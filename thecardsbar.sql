-- Adminer 4.7.8 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `cards`;
CREATE TABLE `cards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `cards` (`id`, `card_id`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1,	'design-1',	'2021-01-22 06:32:20',	NULL,	'2021-01-22 06:32:20'),
(2,	'design-2',	'2021-01-22 06:32:20',	NULL,	'2021-01-22 06:32:20'),
(3,	'design-3',	'2021-01-22 06:32:20',	NULL,	'2021-01-22 06:32:20'),
(4,	'design-4',	'2021-01-22 06:32:20',	NULL,	'2021-01-22 06:32:20'),
(5,	'design-5',	'2021-01-22 06:32:20',	NULL,	'2021-01-22 06:32:20'),
(6,	'design-6',	'2021-01-22 06:32:20',	NULL,	'2021-01-22 06:32:20'),
(7,	'design-7',	'2021-01-22 06:32:20',	NULL,	'2021-01-22 06:32:20'),
(8,	'design-8',	'2021-01-22 06:32:20',	NULL,	'2021-01-22 06:32:20'),
(9,	'design-9',	'2021-01-22 06:32:20',	NULL,	'2021-01-22 06:32:20'),
(10,	'design-10',	'2021-01-22 06:32:20',	NULL,	'2021-01-22 06:32:20'),
(11,	'design-11',	'2021-01-22 06:32:20',	NULL,	'2021-01-22 06:32:20');

DROP TABLE IF EXISTS `cardtypes`;
CREATE TABLE `cardtypes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `cardtypes` (`id`, `name`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1,	'Business',	'2021-01-27 06:54:06',	NULL,	'2021-01-27 06:54:06'),
(2,	'Personal',	'2021-01-27 06:54:37',	NULL,	'2021-01-27 06:54:37'),
(3,	'dsdsad',	'2021-01-27 06:54:40',	NULL,	'2021-01-27 06:54:40');

DROP TABLE IF EXISTS `card_categories`;
CREATE TABLE `card_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `card_categories` (`id`, `name`, `image`, `description`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1,	'IT',	NULL,	'Information Technology',	'2020-12-22 03:57:55',	NULL,	'2021-01-15 08:25:42'),
(2,	'Automobile',	'',	'Automobile',	'2021-01-15 08:01:28',	NULL,	'2021-01-15 08:01:28'),
(3,	'Travel',	'',	'Travel',	'2021-01-15 08:06:03',	NULL,	'2021-01-15 08:06:03'),
(4,	'Building & Construction',	'',	'Building & Construction',	'2021-01-15 08:07:15',	NULL,	'2021-01-15 08:07:15'),
(5,	'Transportation',	'',	'Transportation',	'2021-01-15 08:08:43',	NULL,	'2021-01-15 08:08:43'),
(6,	'Hospitals & Diagnostics',	'',	'Hospitals & Diagnostics',	'2021-01-15 08:15:50',	NULL,	'2021-01-15 08:15:50'),
(8,	'Doctors',	'',	'Doctors',	'2021-01-15 08:20:04',	NULL,	'2021-01-15 08:20:04'),
(9,	'Electronics',	NULL,	'Electronics & Electrical',	'2021-01-15 09:30:02',	NULL,	'2021-01-15 09:30:02'),
(10,	'Party',	NULL,	'Party',	'2021-01-15 09:42:40',	NULL,	'2021-01-15 09:42:40'),
(11,	'Real Estate',	NULL,	'Real Estate',	'2021-01-15 09:49:50',	NULL,	'2021-01-15 09:49:50'),
(12,	'Clothing',	NULL,	'Clothing',	'2021-01-15 09:54:18',	NULL,	'2021-01-15 09:54:18'),
(13,	'Medical',	NULL,	'Medical',	'2021-01-15 10:00:13',	NULL,	'2021-01-15 10:00:13'),
(14,	'Restaurants',	NULL,	'Restaurants',	'2021-01-15 10:03:51',	NULL,	'2021-01-15 10:03:51'),
(15,	'Wedding',	NULL,	'Wedding',	'2021-01-15 10:09:34',	NULL,	'2021-01-15 10:09:34'),
(19,	'Sports Goods',	NULL,	'Sports Goods',	'2021-01-15 10:15:15',	NULL,	'2021-01-15 10:15:15'),
(20,	'AS',	'categories/image//zB03ozTS7LXnek7lfrb0tpqonE5aA16JLBjqSCSs.png',	'aA',	'2021-01-15 12:58:14',	NULL,	'2021-01-15 13:04:04'),
(21,	'dfgfdg',	NULL,	'fsdf',	'2021-01-15 13:07:37',	NULL,	'2021-01-15 13:07:37'),
(22,	'ssssssss',	NULL,	'sss',	'2021-01-15 13:10:21',	NULL,	'2021-01-15 13:10:21'),
(23,	'asdsad',	'categories/image/A7JJ6ZBtHOqGWYVQjf2a4dh7TYipQG1EeA6RN3wW.png',	'adsad',	'2021-01-15 13:52:18',	NULL,	'2021-01-15 13:52:18'),
(24,	'Mobile & Computers',	'categories/image/R6SSDFWQCLYIETByLfQxJCNx1A5eo5U7HV5z2OEd.png',	'Mobile & Computers',	'2021-01-25 02:31:48',	NULL,	'2021-01-25 02:31:48'),
(25,	'Fruits & Vegetables',	'categories/image/N4wkLx117vha368ULLxqPUMB4zz4Iy9nNNayQZxk.png',	'Fruits & Vegetables',	'2021-01-25 02:32:27',	NULL,	'2021-01-25 02:32:27'),
(26,	'Restaurant & Bakery',	'categories/image/ZFs5vdbtBK03l7PKfwXBMYtwuQPzOhj3rfDOexJh.png',	'Restaurant & Bakery',	'2021-01-25 02:33:06',	NULL,	'2021-01-25 02:33:06'),
(27,	'Pharmacy & Healthcare',	'categories/image/BmiIBvPpAba9NQ3XOKo2n8cuTE9My5qiZ4vnJmJc.png',	'Pharmacy & Healthcare',	'2021-01-25 02:33:32',	NULL,	'2021-01-25 02:33:32'),
(28,	'Paan Shop',	'categories/image/SBDlfPbRbQPZ6kXUBdEX6o25mb3IiVrK0F4RNQ62.png',	'Paan Shop',	'2021-01-25 02:35:53',	NULL,	'2021-01-25 02:35:53'),
(29,	'Woman Accessories & Garments',	'categories/image/0cFxCw4zuykArhEo0zy6fNL5acizVuavqm985MSV.png',	'Woman Accessories & Garments',	'2021-01-25 02:36:23',	NULL,	'2021-01-25 02:36:23'),
(30,	'Man Accessories & Garments',	'categories/image/eKdfLk1L6VoWrpJH1LuqJF6YXq1eIZt4vNdKIQc9.png',	'Man Accessories & Garments',	'2021-01-25 02:36:45',	NULL,	'2021-01-25 02:36:45'),
(31,	'Books & Stationary',	'categories/image/sAyhxAei8tbvQbZTvZeV1Mg7gVcHEYx1rx8VyUKh.png',	'Books & Stationary',	'2021-01-25 02:37:11',	NULL,	'2021-01-25 02:37:11'),
(32,	'Fresh Meat & Fish',	'categories/image/oPD9tUFzljion6EyPkvhdcaEeMH5XLRFnwBq4GHn.png',	'Fresh Meat & Fish',	'2021-01-25 02:37:36',	NULL,	'2021-01-25 02:37:36'),
(33,	'Hardware & Tools',	'categories/image/2tFFSaASZZc7TfqWxDaEKMk2kQWBU89zc0wV4k2x.png',	'Hardware & Tools',	'2021-01-25 02:38:00',	NULL,	'2021-01-25 02:38:00'),
(34,	'Jewellery & Gems',	'categories/image/CjDhhofpc67tUEW9p2uypiRIkPLSi4Sz56egJoJX.png',	'Jewellery & Gems',	'2021-01-25 02:38:46',	NULL,	'2021-01-25 02:38:46'),
(35,	'Local Service',	'categories/image/9IlGQqw5uvt3u7MQt5H91miOiWudJfSEPVzw1QMv.png',	'Local Service',	'2021-01-25 02:39:06',	NULL,	'2021-01-25 02:39:06'),
(36,	'Travel & Tourism',	'categories/image/sIgVxHNOU7wmJOC96MM3siZTj72FRZSovY9MC9B2.png',	'Travel & Tourism',	'2021-01-25 02:39:24',	NULL,	'2021-01-25 02:39:24'),
(37,	'Garments Shop',	'categories/image/cpbZvxWl5fbhG4wfVIh9mR59QppempvBSbx3n7Og.png',	'Garments Shop',	'2021-01-25 02:39:58',	NULL,	'2021-01-25 02:39:58'),
(38,	'Cleaning Service',	'categories/image/gY9DPNM9Y7SWUiU8b2jybg73Oxr9z7S9hEq41FeJ.png',	'Cleaning Service',	'2021-01-25 02:41:52',	NULL,	'2021-01-25 02:41:52'),
(39,	'Tailoring Service',	'categories/image/7KRw1UmM2mjC2BVDOJJaHwDlyZzeY8pknSAD8g0X.png',	'Tailoring Service',	'2021-01-25 02:43:05',	NULL,	'2021-01-25 02:43:05'),
(40,	'Plumbing Service',	'categories/image/QBSqYZjCrpZb5Jcp1ow6BIbt9LoyiflBvElgzzE1.png',	'Plumbing Service',	'2021-01-25 02:43:34',	NULL,	'2021-01-25 02:43:34'),
(41,	'Electrician Service',	'categories/image/NM6vmZbmRYIgHpeDi1BEuSdGFhjM2ufEA1dIXQZy.png',	'Electrician Service',	'2021-01-25 02:44:01',	NULL,	'2021-01-25 02:44:01'),
(42,	'Acupressure Service',	'categories/image/NbF0PikbLqXmW2ppx3VK8Sz7wrqkOkkbfF0wkNrT.png',	'Acupressure Service',	'2021-01-25 02:45:25',	NULL,	'2021-01-25 02:45:25'),
(43,	'Services with Image',	'categories/image/kbHkAgwq0zUTIsRjFiz4MjFMDNLr80Jv4lO1vlYG.png',	'Services with Image',	'2021-01-25 02:57:08',	NULL,	'2021-01-25 02:58:16');

DROP TABLE IF EXISTS `card_likes`;
CREATE TABLE `card_likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_template_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `likes` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `card_likes` (`id`, `user_id`, `card_template_id`, `likes`, `created_at`, `deleted_at`, `updated_at`) VALUES
(5,	'1',	'22',	1,	'2021-01-19 05:25:24',	NULL,	'2021-01-20 07:05:46'),
(6,	'28',	'23',	1,	'2021-01-19 07:11:26',	NULL,	'2021-01-20 07:05:46'),
(7,	'44',	'23',	1,	'2021-01-19 13:59:46',	NULL,	'2021-01-20 07:05:46'),
(8,	'44',	'22',	1,	'2021-01-19 13:59:50',	NULL,	'2021-01-20 07:05:46'),
(9,	'40',	'22',	1,	'2021-01-19 14:02:58',	NULL,	'2021-01-20 07:05:46'),
(10,	'40',	'23',	1,	'2021-01-19 14:03:00',	NULL,	'2021-01-20 07:05:46'),
(13,	'39',	'22',	1,	'2021-01-21 07:44:58',	NULL,	'2021-01-21 08:40:51'),
(14,	'39',	'23',	1,	'2021-01-21 09:43:21',	NULL,	'2021-01-21 09:43:21');

DROP TABLE IF EXISTS `card_templates`;
CREATE TABLE `card_templates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` varchar(111) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_category_id` varchar(111) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `picture` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `cardtype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `colortype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `maxsocialmedia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `minsocialmedia` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `logo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `background` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `gallery` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `max_no_gallery` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_no_gallery` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `services` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `max_no_services` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `min_no_services` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `serviceswithimg` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `primaryphone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secondaryphone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `primaryemail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secondaryemail` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `website` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `min_no_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cardtitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `carddescription` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `actualprice` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discounttype` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bottomtitle` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `calltagline` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `openclosetime` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `company` int(11) NOT NULL DEFAULT '0',
  `companyabout` int(11) NOT NULL DEFAULT '0',
  `crosel_image` varchar(222) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `card_templates` (`id`, `category_id`, `sub_category_id`, `price`, `created_at`, `picture`, `updated_at`, `cardtype`, `colortype`, `maxsocialmedia`, `minsocialmedia`, `profile`, `logo`, `background`, `gallery`, `max_no_gallery`, `min_no_gallery`, `services`, `max_no_services`, `min_no_services`, `serviceswithimg`, `username`, `primaryphone`, `secondaryphone`, `primaryemail`, `secondaryemail`, `address`, `location`, `website`, `designation`, `description`, `min_no_description`, `cardtitle`, `carddescription`, `actualprice`, `discounttype`, `discount`, `bottomtitle`, `calltagline`, `openclosetime`, `company`, `companyabout`, `crosel_image`, `deleted_at`) VALUES
(25,	'1',	'1',	'199',	'2021-01-21 12:49:24',	'storage/card/image/20351611233363.png',	'2021-01-21 12:59:43',	'2',	'a:3:{i:0;s:7:\"Primary\";i:1;s:4:\"Text\";i:2;s:7:\"Heading\";}',	'4',	'3',	'1',	'0',	'0',	'0',	NULL,	NULL,	'0',	NULL,	NULL,	'0',	'1',	'1',	'0',	'1',	'0',	'1',	'1',	'1',	'1',	'1',	'1',	'Information card',	'Information card',	'500',	'2',	'301',	'1',	NULL,	NULL,	0,	1,	'card/image/56601611233363.png',	NULL),
(26,	'10',	'79',	'199',	'2021-01-21 13:22:04',	'storage/card/image/61231611235323.png',	'2021-01-21 13:22:04',	'1',	'a:4:{i:0;s:7:\"Primary\";i:1;s:9:\"Secondary\";i:2;s:4:\"Text\";i:3;s:7:\"Heading\";}',	'5',	'4',	'1',	'0',	'1',	'1',	'4',	'3',	'0',	NULL,	NULL,	'0',	'1',	'1',	'0',	'1',	'0',	'1',	'1',	'1',	'1',	'1',	'1',	'Beauty Industry',	'Beauty Industry',	'500',	'2',	'301',	'',	NULL,	NULL,	1,	1,	'card/image/96731611235322.png',	NULL),
(27,	'9',	'61',	'199',	'2021-01-21 13:40:08',	'storage/card/image/85211611236407.png',	'2021-01-21 13:40:08',	'1',	'a:5:{i:0;s:7:\"Primary\";i:1;s:9:\"Secondary\";i:2;s:4:\"Blog\";i:3;s:4:\"Text\";i:4;s:7:\"Heading\";}',	'4',	'3',	'1',	'0',	'1',	'0',	NULL,	NULL,	'1',	'6',	'4',	'0',	'1',	'1',	'0',	'1',	'0',	'1',	'1',	'1',	'1',	'1',	'1',	'Shop Card',	'Shop Card',	'500',	'2',	'301',	'',	NULL,	NULL,	1,	1,	'card/image/12591611236406.png',	NULL),
(28,	'9',	'58',	'199',	'2021-01-21 14:19:08',	'storage/card/image/1731611238747.png',	'2021-01-21 14:19:08',	'2',	'a:3:{i:0;s:7:\"Primary\";i:1;s:7:\"Heading\";i:2;s:5:\"sfsdf\";}',	'4',	'3',	'1',	'0',	'0',	'1',	'3',	'3',	'0',	NULL,	NULL,	'0',	'1',	'1',	'0',	'1',	'0',	'1',	'1',	'1',	'0',	'1',	'1',	'Three Grid Images',	'Three Grid Images',	'500',	'2',	'301',	'',	NULL,	NULL,	0,	1,	'card/image/87041611238747.png',	NULL),
(29,	'35',	NULL,	'799',	'2021-01-25 03:05:16',	'storage/card/image//xYJBR4Pz19jzMOWLfIMfvACOQSzDgIhsgDu4VNNs.png',	'2021-01-25 03:05:16',	'1',	'a:1:{i:0;s:7:\"Primary\";}',	'4',	'3',	'1',	'1',	'0',	'0',	NULL,	NULL,	'1',	'4',	'4',	'1',	'1',	'1',	'1',	'1',	'1',	'1',	'1',	'1',	'0',	'0',	NULL,	'Product & Service Card',	'Product & Service Card',	'1200',	'2',	'401',	'1',	NULL,	NULL,	1,	1,	'storage/card/image//s7xFxYS4wXe0AnOjkGegfM78olFIa6Drw2lRvBNQ.png',	NULL),
(30,	'2',	'2',	'999',	'2021-01-25 03:18:39',	'storage/card/image//uymODAOhDckKKG7tyAvldoc0DddAlKxcYdm3zSWt.png',	'2021-01-25 03:18:39',	'1',	'a:1:{i:0;s:7:\"Primary\";}',	'2',	'-23',	'1',	'1',	'0',	'0',	NULL,	NULL,	'1',	'4',	'4',	'1',	'1',	'1',	'1',	'1',	'1',	'1',	'1',	'1',	'1',	'0',	NULL,	'Business Owner & Services Card',	'Business Owner & Services Card',	'1200',	'2',	'201',	'1',	NULL,	NULL,	1,	1,	'storage/card/image//UQzUIKXBBgpoQv9T1aE3nzdB7KG3kviQMCA3X5QY.png',	NULL),
(31,	'9',	'58',	'999',	'2021-01-25 03:24:41',	'storage/card/image//7nkB4t4ObqtV95h4sC7M2PbMbPfWuTx0aJWY2Mrp.png',	'2021-01-25 03:24:41',	'1',	'a:1:{i:0;s:7:\"Primary\";}',	'4',	'4',	'1',	'1',	'0',	'0',	NULL,	NULL,	'1',	'4',	'4',	'1',	'1',	'1',	'1',	'1',	'1',	'1',	'1',	'1',	'1',	'0',	NULL,	'Professional Services & Profile',	'Professional Services & Profile',	'1200',	'2',	'201',	'1',	NULL,	'1',	1,	1,	'storage/card/image//xzG1uUp4Bo7WNkdBWcXwT5cS2GZVqffFw73gUw4j.png',	NULL),
(32,	'2',	'2',	'77',	'2021-01-27 12:56:51',	'storage/card/image//NKfcIZ2Z4PaW007ScDrbu0utNOVxUK5MenONYXda.png',	'2021-01-27 12:56:51',	'2',	'',	'4',	'3',	'1',	'0',	'0',	'0',	NULL,	NULL,	'1',	'4',	'4',	'1',	'1',	'1',	'1',	'1',	'1',	'1',	'1',	'0',	'0',	'0',	NULL,	'fdfdfd',	'dffdfdfdf',	'100',	'1',	'23',	'',	NULL,	NULL,	0,	0,	'storage/card/image//nC90iILdCvG5wocMOkw7mPbrKuI7jT3YKCBxVfN3.png',	NULL);

DROP TABLE IF EXISTS `card_themes`;
CREATE TABLE `card_themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_template_id` varchar(111) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `default_image` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `card_themes` (`id`, `name`, `card_template_id`, `image`, `default_image`, `created_at`, `deleted_at`, `updated_at`) VALUES
(4,	'blue',	'1',	'storage/card/image/27261610603269.png',	0,	'2021-01-14 00:17:49',	NULL,	'2021-01-14 00:17:49'),
(5,	'brown',	'1',	'storage/card/image/52671610603269.png',	0,	'2021-01-14 00:17:49',	NULL,	'2021-01-14 00:17:49'),
(6,	'white',	'1',	'storage/card/image/93481610603269.png',	0,	'2021-01-14 00:17:50',	NULL,	'2021-01-14 00:17:50'),
(7,	'Default',	'3',	'storage/card/image/76681610611021.png',	1,	'2021-01-14 02:27:01',	NULL,	'2021-01-14 02:27:01'),
(8,	'thme 1',	'3',	'storage/card/image/14711610611021.png',	0,	'2021-01-14 02:27:01',	NULL,	'2021-01-14 02:27:01'),
(9,	'theme 2',	'3',	'storage/card/image/46511610611021.png',	0,	'2021-01-14 02:27:01',	NULL,	'2021-01-14 02:27:01'),
(10,	'theme',	'3',	'storage/card/image/8201610611021.png',	0,	'2021-01-14 02:27:01',	NULL,	'2021-01-14 02:27:01'),
(17,	'Seablue',	'25',	'storage/card/image/51961611233758.png',	0,	'2021-01-21 12:55:58',	NULL,	'2021-01-21 12:55:58'),
(18,	'Blood Red',	'25',	'storage/card/image/96151611233758.png',	0,	'2021-01-21 12:55:58',	NULL,	'2021-01-21 12:55:58'),
(19,	'Grey Green',	'25',	'storage/card/image/55721611233758.png',	0,	'2021-01-21 12:55:59',	NULL,	'2021-01-21 12:55:59'),
(20,	'Purple',	'25',	'storage/card/image/47771611233759.png',	0,	'2021-01-21 12:55:59',	NULL,	'2021-01-21 12:55:59'),
(21,	'brown Orange',	'25',	'storage/card/image/51271611233759.png',	0,	'2021-01-21 12:55:59',	NULL,	'2021-01-21 12:55:59'),
(22,	'Purple Black',	'26',	'storage/card/image/91001611235324.png',	0,	'2021-01-21 13:22:05',	NULL,	'2021-01-21 13:22:05'),
(23,	'Blue Orange',	'26',	'storage/card/image/31851611235325.png',	0,	'2021-01-21 13:22:05',	NULL,	'2021-01-21 13:22:05'),
(24,	'Dark and Royal Blue',	'26',	'storage/card/image/91001611235325.png',	0,	'2021-01-21 13:22:06',	NULL,	'2021-01-21 13:22:06'),
(25,	'Cyan Black',	'26',	'storage/card/image/71801611235326.png',	0,	'2021-01-21 13:22:06',	NULL,	'2021-01-21 13:22:06'),
(26,	'Pink',	'26',	'storage/card/image/89701611235326.png',	0,	'2021-01-21 13:22:07',	NULL,	'2021-01-21 13:22:07'),
(27,	'Purple and Sky Blue',	'27',	'storage/card/image/43601611236408.png',	0,	'2021-01-21 13:40:08',	NULL,	'2021-01-21 13:40:08'),
(28,	'Pink and Purple',	'27',	'storage/card/image/251611236408.png',	0,	'2021-01-21 13:40:09',	NULL,	'2021-01-21 13:40:09'),
(29,	'Grey Dark Cyan',	'27',	'storage/card/image/3431611236409.png',	0,	'2021-01-21 13:40:09',	NULL,	'2021-01-21 13:40:09'),
(30,	'Cyan and blue',	'27',	'storage/card/image/24971611236409.png',	0,	'2021-01-21 13:40:10',	NULL,	'2021-01-21 13:40:10'),
(31,	'Dark Blue',	'27',	'storage/card/image/88771611236410.png',	0,	'2021-01-21 13:40:10',	NULL,	'2021-01-21 13:40:10'),
(32,	'Pink and Blue',	'28',	'storage/card/image/98841611238748.png',	0,	'2021-01-21 14:19:08',	NULL,	'2021-01-21 14:19:08'),
(33,	'Black and Yellow',	'28',	'storage/card/image/48401611238748.png',	0,	'2021-01-21 14:19:08',	NULL,	'2021-01-21 14:19:08'),
(34,	'Cream Oranges',	'28',	'storage/card/image/75261611238748.png',	0,	'2021-01-21 14:19:09',	NULL,	'2021-01-21 14:19:09'),
(35,	'Brown and Dark Green',	'28',	'storage/card/image/12181611238749.png',	0,	'2021-01-21 14:19:09',	NULL,	'2021-01-21 14:19:09'),
(36,	'Light Blue',	'28',	'storage/card/image/46871611238749.png',	0,	'2021-01-21 14:19:09',	NULL,	'2021-01-21 14:19:09'),
(37,	'Purple & Pink',	'29',	'storage/card/image//lZCgqzbyY2HEiPnfjgT6y1tXco0n0468aEwZnPjr.png',	0,	'2021-01-25 03:05:16',	NULL,	'2021-01-25 03:05:16'),
(38,	'Black & Light Blue',	'29',	'storage/card/image//GMV9JaTuIoWYIHoRage8IHUCKHNi8hdra1w2YJ1e.png',	0,	'2021-01-25 03:05:16',	NULL,	'2021-01-25 03:05:16'),
(39,	'Light Cyan',	'29',	'storage/card/image//w1siI4AwwRrEHolA3VFInJUFAL44btJU31g3s1sX.png',	0,	'2021-01-25 03:05:16',	NULL,	'2021-01-25 03:05:16'),
(40,	'Dark Cyan',	'29',	'storage/card/image//1aEduzuNM2WrZSrxOPxqOG4qk8tLYu4m1TZVhopq.png',	0,	'2021-01-25 03:05:16',	NULL,	'2021-01-25 03:05:16'),
(41,	'Dark Blue',	'30',	'storage/card/image//Oj5i94ApGnD48Ea9y6YKwrnbzGlaCHzhGxvkIdkg.png',	0,	'2021-01-25 03:18:39',	NULL,	'2021-01-25 03:18:39'),
(42,	'Purple',	'30',	'storage/card/image//YofDc7k4ehYDQhIG6lMFolY9xeKvWhYHQpqBtY4d.png',	0,	'2021-01-25 03:18:39',	NULL,	'2021-01-25 03:18:39'),
(43,	'Red',	'30',	'storage/card/image//GCZpNBBEaETjyiGDpWtjNcUMkHGWPtH25XjxHfO1.png',	0,	'2021-01-25 03:18:39',	NULL,	'2021-01-25 03:18:39'),
(44,	'Green',	'30',	'storage/card/image//r60Hfw5UA2a5IiEc7tru78cZfZ4XXi9DiDcN83jg.png',	0,	'2021-01-25 03:18:39',	NULL,	'2021-01-25 03:18:39'),
(45,	'Black & Red',	'31',	'storage/card/image//iJU6HulPx6vvd7bcLY9skdJLvbFlpchD7lIDdl3n.png',	0,	'2021-01-25 03:24:41',	NULL,	'2021-01-25 03:24:41'),
(46,	'Bright Turquoise & Black',	'31',	'storage/card/image//eJnx2J8XjdaSYaWUbDGJx2d5hK1APnZaow9NK8Gy.png',	0,	'2021-01-25 03:24:41',	NULL,	'2021-01-25 03:24:41'),
(47,	'Black & Blue',	'31',	'storage/card/image//yxIj1Vh1AROw4QydkpcFQzZwJIzERGTckZbdT0s3.png',	0,	'2021-01-25 03:24:41',	NULL,	'2021-01-25 03:24:41'),
(48,	'Black & Yellow',	'31',	'storage/card/image//XtEbVi8BipWY3geEEG1QMfqd0BBuh8kVJ0aN5Gfo.png',	0,	'2021-01-25 03:24:41',	NULL,	'2021-01-25 03:24:41'),
(49,	'sdfsfsdf',	'32',	'storage/card/image//6IufN4HOAfJuHoRQPlPRpJnOQ1LoDCp4f1UvD5qN.png',	0,	'2021-01-27 12:56:51',	NULL,	'2021-01-27 12:56:51'),
(50,	'sdfsdfsfds',	'32',	'storage/card/image//eIiQWDnHZ6vESqddcQb3QpJ3MG17R6Vq4Fp3p9SK.png',	0,	'2021-01-27 12:56:51',	NULL,	'2021-01-27 12:56:51');

DROP TABLE IF EXISTS `card_views`;
CREATE TABLE `card_views` (
  `id` int(10) unsigned NOT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_template_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `views` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `card_views` (`id`, `user_id`, `card_template_id`, `views`, `created_at`, `deleted_at`, `updated_at`) VALUES
(0,	'44',	'22',	1,	'2021-01-19 13:59:35',	NULL,	'2021-01-19 13:59:35'),
(0,	'28',	'23',	1,	'2021-01-20 06:58:53',	NULL,	'2021-01-20 06:58:53'),
(0,	'48',	'23',	1,	'2021-01-20 11:58:38',	NULL,	'2021-01-20 11:58:38'),
(0,	'28',	'22',	1,	'2021-01-20 14:09:13',	NULL,	'2021-01-20 14:09:13'),
(0,	'39',	'22',	1,	'2021-01-20 14:09:20',	NULL,	'2021-01-20 14:09:20'),
(0,	'39',	'index.html',	1,	'2021-01-20 14:09:25',	NULL,	'2021-01-20 14:09:25'),
(0,	'39',	'23',	1,	'2021-01-20 14:09:50',	NULL,	'2021-01-20 14:09:50'),
(0,	'28',	'25',	1,	'2021-01-21 12:57:08',	NULL,	'2021-01-21 12:57:08'),
(0,	'28',	'index.html',	1,	'2021-01-21 13:22:17',	NULL,	'2021-01-21 13:22:17'),
(0,	'28',	'24',	1,	'2021-01-22 10:38:49',	NULL,	'2021-01-22 10:38:49'),
(0,	'28',	'26',	1,	'2021-01-22 10:39:24',	NULL,	'2021-01-22 10:39:24'),
(0,	'28',	'27',	1,	'2021-01-22 10:40:49',	NULL,	'2021-01-22 10:40:49'),
(0,	'28',	'28',	1,	'2021-01-22 10:41:53',	NULL,	'2021-01-22 10:41:53'),
(0,	'28',	'29',	1,	'2021-01-25 03:05:56',	NULL,	'2021-01-25 03:05:56');

DROP TABLE IF EXISTS `color_types`;
CREATE TABLE `color_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `color_types` (`id`, `name`, `title`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1,	'Primary',	'Primary Color',	'2021-01-05 23:43:39',	NULL,	'2021-01-05 23:43:39'),
(2,	'Secondary',	'Secondary Color',	'2021-01-05 23:43:39',	NULL,	'2021-01-05 23:43:39'),
(3,	'Blog',	'Blog Color',	'2021-01-05 23:43:39',	NULL,	'2021-01-05 23:43:39'),
(4,	'Text',	'Text Color',	'2021-01-05 23:43:39',	NULL,	'2021-01-05 23:43:39'),
(5,	'Heading',	'Heading Color',	'2021-01-05 23:43:39',	NULL,	'2021-01-05 23:43:39'),
(6,	'sdfsdf',	'sdfsdf',	'2021-01-06 02:57:52',	NULL,	'2021-01-06 02:57:52'),
(7,	'sfsdf',	'sfsdf',	'2021-01-06 03:01:39',	NULL,	'2021-01-06 03:01:39'),
(8,	'dasdas',	'sadsad',	'2021-01-06 03:07:08',	NULL,	'2021-01-06 03:07:08'),
(9,	'dasdasd',	'asd',	'2021-01-06 03:48:26',	NULL,	'2021-01-06 03:48:26'),
(10,	'Fsdfsd_s_sdfdsf',	'sdfsdf',	'2021-01-06 04:46:54',	NULL,	'2021-01-06 04:46:54'),
(11,	'Dada',	'asdsad',	'2021-01-06 05:01:59',	NULL,	'2021-01-06 05:01:59'),
(12,	'Fdsf_fdsf_sf',	'fdsf fdsf sf',	'2021-01-06 05:05:41',	NULL,	'2021-01-06 05:05:41'),
(13,	'Ffsdfds_fdfsd_sdfggggg_f',	'ffsdfds fdfsd sdfggggg f',	'2021-01-06 05:06:29',	NULL,	'2021-01-06 05:06:29'),
(14,	'Abc',	'abc',	'2021-01-06 07:44:00',	NULL,	'2021-01-06 07:44:00');

DROP TABLE IF EXISTS `contactuses`;
CREATE TABLE `contactuses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `contactuses` (`id`, `name`, `email`, `phone`, `message`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1,	'Eric Jones',	'ericjonesonline@outlook.com',	'555-555-1212',	'',	'2021-01-19 07:40:26',	NULL,	'2021-01-19 07:40:26'),
(2,	'Eric Jones',	'ericjonesonline@outlook.com',	'555-555-1212',	'',	'2021-01-23 02:42:54',	NULL,	'2021-01-23 02:42:54');

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1,	'2014_10_12_000000_create_users_table',	1),
(2,	'2014_10_12_100000_create_password_resets_table',	1),
(5,	'2020_12_15_075041_create_orders_table',	2),
(6,	'2020_12_15_053838_create_social_media_table',	3),
(8,	'2020_12_15_052703_create_card_templates_table',	4),
(9,	'2020_12_21_095357_create_temp_card_details_table',	5),
(10,	'2020_12_22_072958_create_card_categories_table',	6),
(11,	'2020_12_22_073815_create_card_categories_table',	7),
(12,	'2020_12_22_092212_create_sub_categories_table',	7),
(15,	'2020_12_24_042844_create_order_card_details_table',	8),
(16,	'2020_12_24_051827_create_orderfields_table',	8),
(17,	'2020_12_25_070141_create_contactuses_table',	9),
(18,	'2020_12_29_070559_create_pages_table',	10),
(19,	'2020_12_29_071221_create_page_contents_table',	10);

DROP TABLE IF EXISTS `orderfields`;
CREATE TABLE `orderfields` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `order_id` varchar(111) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` varchar(111) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fieldname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fieldvalue` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subfieldname` varchar(111) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fieldimage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `orderfields` (`id`, `order_id`, `user_id`, `fieldname`, `fieldvalue`, `subfieldname`, `fieldimage`, `created_at`, `deleted_at`, `updated_at`) VALUES
(546,	'42',	'39',	'card_theme_id',	'15',	NULL,	NULL,	'2021-01-14 13:45:44',	NULL,	'2021-01-14 13:45:44'),
(547,	'42',	'39',	'category_id',	'1',	NULL,	NULL,	'2021-01-14 13:45:44',	NULL,	'2021-01-14 13:45:44'),
(548,	'42',	'39',	'sub_category_id',	'1',	NULL,	NULL,	'2021-01-14 13:45:44',	NULL,	'2021-01-14 13:45:44'),
(549,	'42',	'39',	'social_media',	'dsdsad',	'Facebook',	NULL,	'2021-01-14 13:45:44',	NULL,	'2021-01-14 13:45:44'),
(550,	'42',	'39',	'first_name',	'dtstss',	NULL,	NULL,	'2021-01-14 13:45:44',	NULL,	'2021-01-14 13:45:44'),
(551,	'42',	'39',	'last_name',	'sdfsdf',	NULL,	NULL,	'2021-01-14 13:45:44',	NULL,	'2021-01-14 13:45:44'),
(552,	'42',	'39',	'primaryphone',	'4234234',	NULL,	NULL,	'2021-01-14 13:45:44',	NULL,	'2021-01-14 13:45:44'),
(553,	'42',	'39',	'primaryemail',	'das@fsf.fdg',	NULL,	NULL,	'2021-01-14 13:45:44',	NULL,	'2021-01-14 13:45:44'),
(554,	'42',	'39',	'address',	'fdsdfsd',	NULL,	NULL,	'2021-01-14 13:45:44',	NULL,	'2021-01-14 13:45:44'),
(555,	'42',	'39',	'profileimg',	'users/ZU1tQkZaZ3FRSExjVUFJcnp6L3pEUT09/orders/bERrQ1VIRjZsd2FUeXRVNXdGL01KZz09/profile/Bsfn56qbsO5oDZb9c6fiGJHCuMhLqCdxX3mRQWit.jpg',	NULL,	NULL,	'2021-01-14 13:45:44',	NULL,	'2021-01-14 13:45:44'),
(556,	'48',	'28',	'card_theme_id',	'0',	NULL,	NULL,	'2021-01-15 14:52:58',	NULL,	'2021-01-15 14:52:58'),
(557,	'48',	'28',	'category_id',	'1',	NULL,	NULL,	'2021-01-15 14:52:58',	NULL,	'2021-01-15 14:52:58'),
(558,	'48',	'28',	'sub_category_id',	'1',	NULL,	NULL,	'2021-01-15 14:52:58',	NULL,	'2021-01-15 14:52:58'),
(559,	'48',	'28',	'social_media',	'dsadsa',	'Facebook',	NULL,	'2021-01-15 14:52:58',	NULL,	'2021-01-15 14:52:58'),
(560,	'48',	'28',	'first_name',	'parmod',	NULL,	NULL,	'2021-01-15 14:52:58',	NULL,	'2021-01-15 14:52:58'),
(561,	'48',	'28',	'last_name',	'kumar',	NULL,	NULL,	'2021-01-15 14:52:58',	NULL,	'2021-01-15 14:52:58'),
(562,	'48',	'28',	'primaryphone',	'7695845778',	NULL,	NULL,	'2021-01-15 14:52:58',	NULL,	'2021-01-15 14:52:58'),
(563,	'48',	'28',	'primaryemail',	'pk@gmaikl.com',	NULL,	NULL,	'2021-01-15 14:52:58',	NULL,	'2021-01-15 14:52:58'),
(564,	'48',	'28',	'address',	'adasd',	NULL,	NULL,	'2021-01-15 14:52:58',	NULL,	'2021-01-15 14:52:58'),
(565,	'48',	'28',	'profileimg',	'users/elRuZkRtcXcrOXBUTjVBSFVMTGtDUT09/orders/QmtIZk5SUW9oU2VVM09jL0F0dUxnUT09/profile/DckBExWugGEGyhx08PkEWJhK4KWQXtnz8WlYNRT7.png',	NULL,	NULL,	'2021-01-15 14:52:58',	NULL,	'2021-01-15 14:52:58'),
(566,	'49',	'48',	'card_theme_id',	'16',	NULL,	NULL,	'2021-01-20 12:03:30',	NULL,	'2021-01-20 12:03:30'),
(567,	'49',	'48',	'category_id',	'1',	NULL,	NULL,	'2021-01-20 12:03:30',	NULL,	'2021-01-20 12:03:30'),
(568,	'49',	'48',	'sub_category_id',	'1',	NULL,	NULL,	'2021-01-20 12:03:30',	NULL,	'2021-01-20 12:03:30'),
(569,	'49',	'48',	'social_media',	'https://www.facebook.com',	'Facebook',	NULL,	'2021-01-20 12:03:30',	NULL,	'2021-01-20 12:03:30'),
(570,	'49',	'48',	'social_media',	'https://www.skype.com/',	'Skype',	NULL,	'2021-01-20 12:03:30',	NULL,	'2021-01-20 12:03:30'),
(571,	'49',	'48',	'social_media',	'https://twitter.com/',	'Twitter',	NULL,	'2021-01-20 12:03:30',	NULL,	'2021-01-20 12:03:30'),
(572,	'49',	'48',	'first_name',	'Parmod',	NULL,	NULL,	'2021-01-20 12:03:30',	NULL,	'2021-01-20 12:03:30'),
(573,	'49',	'48',	'last_name',	'Kumar',	NULL,	NULL,	'2021-01-20 12:03:30',	NULL,	'2021-01-20 12:03:30'),
(574,	'49',	'48',	'primaryphone',	'76969515',	NULL,	NULL,	'2021-01-20 12:03:30',	NULL,	'2021-01-20 12:03:30'),
(575,	'49',	'48',	'secondaryphone',	'7355088618',	NULL,	NULL,	'2021-01-20 12:03:30',	NULL,	'2021-01-20 12:03:30'),
(576,	'49',	'48',	'companyabout',	'Techno Deviser IT company',	NULL,	NULL,	'2021-01-20 12:03:30',	NULL,	'2021-01-20 12:03:30'),
(577,	'49',	'48',	'designation',	'PHP Developer',	NULL,	NULL,	'2021-01-20 12:03:30',	NULL,	'2021-01-20 12:03:30'),
(578,	'49',	'48',	'primaryemail',	'pk17861000@gmail.com',	NULL,	NULL,	'2021-01-20 12:03:30',	NULL,	'2021-01-20 12:03:30'),
(579,	'49',	'48',	'address',	'mohali punjab',	NULL,	NULL,	'2021-01-20 12:03:30',	NULL,	'2021-01-20 12:03:30'),
(580,	'49',	'48',	'website',	'https://www.td.com/',	NULL,	NULL,	'2021-01-20 12:03:30',	NULL,	'2021-01-20 12:03:30'),
(581,	'49',	'48',	'profileimg',	'users/QmtIZk5SUW9oU2VVM09jL0F0dUxnUT09/orders/b2RwWm9WcDJDelJyN1ZYRXpXQ3pHQT09/profile/egepqcbvnyGdRkNUnaCHb0BayofXpDcu2A6mXjGm.jpg',	NULL,	NULL,	'2021-01-20 12:03:30',	NULL,	'2021-01-20 12:03:30'),
(582,	'49',	'48',	'logo',	'users/QmtIZk5SUW9oU2VVM09jL0F0dUxnUT09/orders/b2RwWm9WcDJDelJyN1ZYRXpXQ3pHQT09/logo/6L5if3psomNsAq6m0wyI1M3ass2zKi5yBwDLPNDP.png',	NULL,	NULL,	'2021-01-20 12:03:30',	NULL,	'2021-01-20 12:03:30'),
(583,	'50',	'39',	'card_theme_id',	'0',	NULL,	NULL,	'2021-01-20 14:11:38',	NULL,	'2021-01-20 14:11:38'),
(584,	'50',	'39',	'category_id',	'1',	NULL,	NULL,	'2021-01-20 14:11:38',	NULL,	'2021-01-20 14:11:38'),
(585,	'50',	'39',	'sub_category_id',	'1',	NULL,	NULL,	'2021-01-20 14:11:38',	NULL,	'2021-01-20 14:11:38'),
(586,	'50',	'39',	'social_media',	'dsdsad',	'Facebook',	NULL,	'2021-01-20 14:11:38',	NULL,	'2021-01-20 14:11:38'),
(587,	'50',	'39',	'social_media',	'www.skype.com',	'Skype',	NULL,	'2021-01-20 14:11:38',	NULL,	'2021-01-20 14:11:38'),
(588,	'50',	'39',	'first_name',	'dtstsss',	NULL,	NULL,	'2021-01-20 14:11:38',	NULL,	'2021-01-20 14:11:38'),
(589,	'50',	'39',	'last_name',	'sdfsdf',	NULL,	NULL,	'2021-01-20 14:11:38',	NULL,	'2021-01-20 14:11:38'),
(590,	'50',	'39',	'primaryphone',	'4234234',	NULL,	NULL,	'2021-01-20 14:11:38',	NULL,	'2021-01-20 14:11:38'),
(591,	'50',	'39',	'primaryemail',	'das@fsf.fdg',	NULL,	NULL,	'2021-01-20 14:11:38',	NULL,	'2021-01-20 14:11:38'),
(592,	'50',	'39',	'address',	'fdsdfsd',	NULL,	NULL,	'2021-01-20 14:11:38',	NULL,	'2021-01-20 14:11:38'),
(593,	'50',	'39',	'profileimg',	'users/ZU1tQkZaZ3FRSExjVUFJcnp6L3pEUT09/orders/dXNhNTZBMWo1WDZOSUF0M3FLSjRUdz09/profile/T1nHKoWc6FhIJrwHwajgPnnXTrYkkwZZdFFF4Ape.jpg',	NULL,	NULL,	'2021-01-20 14:11:38',	NULL,	'2021-01-20 14:11:38'),
(594,	'51',	'39',	'card_theme_id',	'0',	NULL,	NULL,	'2021-01-20 14:13:52',	NULL,	'2021-01-20 14:13:52'),
(595,	'51',	'39',	'category_id',	'1',	NULL,	NULL,	'2021-01-20 14:13:52',	NULL,	'2021-01-20 14:13:52'),
(596,	'51',	'39',	'sub_category_id',	'1',	NULL,	NULL,	'2021-01-20 14:13:52',	NULL,	'2021-01-20 14:13:52'),
(597,	'51',	'39',	'social_media',	'dsdsad',	'Facebook',	NULL,	'2021-01-20 14:13:52',	NULL,	'2021-01-20 14:13:52'),
(598,	'51',	'39',	'social_media',	'www.skype.com',	'Skype',	NULL,	'2021-01-20 14:13:52',	NULL,	'2021-01-20 14:13:52'),
(599,	'51',	'39',	'first_name',	'dtstsss',	NULL,	NULL,	'2021-01-20 14:13:52',	NULL,	'2021-01-20 14:13:52'),
(600,	'51',	'39',	'last_name',	'sdfsdf',	NULL,	NULL,	'2021-01-20 14:13:52',	NULL,	'2021-01-20 14:13:52'),
(601,	'51',	'39',	'primaryphone',	'4234234',	NULL,	NULL,	'2021-01-20 14:13:52',	NULL,	'2021-01-20 14:13:52'),
(602,	'51',	'39',	'primaryemail',	'harjitkumar261@gmail.com',	NULL,	NULL,	'2021-01-20 14:13:52',	NULL,	'2021-01-20 14:13:52'),
(603,	'51',	'39',	'address',	'fdsdfsd',	NULL,	NULL,	'2021-01-20 14:13:52',	NULL,	'2021-01-20 14:13:52'),
(604,	'51',	'39',	'profileimg',	'users/ZU1tQkZaZ3FRSExjVUFJcnp6L3pEUT09/orders/QTJrbitndS9MVm4waDhscm5GOU9Rdz09/profile/Za3MoIeet9i4JWuUFHMmI6lydjFdOWuaFV2rYFj8.jpg',	NULL,	NULL,	'2021-01-20 14:13:52',	NULL,	'2021-01-20 14:13:52'),
(605,	'52',	'28',	'card_theme_id',	'15',	NULL,	NULL,	'2021-01-21 05:21:48',	NULL,	'2021-01-21 05:21:48'),
(606,	'52',	'28',	'category_id',	'1',	NULL,	NULL,	'2021-01-21 05:21:48',	NULL,	'2021-01-21 05:21:48'),
(607,	'52',	'28',	'sub_category_id',	'1',	NULL,	NULL,	'2021-01-21 05:21:48',	NULL,	'2021-01-21 05:21:48'),
(608,	'52',	'28',	'social_media',	'dsadsa',	'Facebook',	NULL,	'2021-01-21 05:21:48',	NULL,	'2021-01-21 05:21:48'),
(609,	'52',	'28',	'social_media',	'ghj',	'WhatsApp',	NULL,	'2021-01-21 05:21:48',	NULL,	'2021-01-21 05:21:48'),
(610,	'52',	'28',	'first_name',	'parmod',	NULL,	NULL,	'2021-01-21 05:21:48',	NULL,	'2021-01-21 05:21:48'),
(611,	'52',	'28',	'last_name',	'kumar',	NULL,	NULL,	'2021-01-21 05:21:48',	NULL,	'2021-01-21 05:21:48'),
(612,	'52',	'28',	'primaryphone',	'7695845778',	NULL,	NULL,	'2021-01-21 05:21:48',	NULL,	'2021-01-21 05:21:48'),
(613,	'52',	'28',	'primaryemail',	'pk@gmaikl.com',	NULL,	NULL,	'2021-01-21 05:21:48',	NULL,	'2021-01-21 05:21:48'),
(614,	'52',	'28',	'address',	'adasd',	NULL,	NULL,	'2021-01-21 05:21:48',	NULL,	'2021-01-21 05:21:48'),
(615,	'52',	'28',	'profileimg',	'users/elRuZkRtcXcrOXBUTjVBSFVMTGtDUT09/orders/UEhkUVkzVVM2UFUwclFKUEk1ZnJSZz09/profile/tpe50bdKeeM470W7U7eApHlrYawIKYVkxNBFJrFT.png',	NULL,	NULL,	'2021-01-21 05:21:48',	NULL,	'2021-01-21 05:21:48');

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `razorpay_order_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `razorpay_signature` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_template_id` varchar(111) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_theme_id` int(10) unsigned NOT NULL,
  `price` double(8,2) NOT NULL,
  `payment_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `coupon` double(8,2) DEFAULT NULL,
  `payment_method` enum('1') COLLATE utf8mb4_unicode_ci DEFAULT NULL COMMENT '1=razorpay',
  `payment_status` enum('1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '1=unpaid',
  `status` enum('0','1','2') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1' COMMENT '0 =cancel , 1=process, 2=completed',
  `cardimage` text COLLATE utf8mb4_unicode_ci,
  `cardqr` varchar(111) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `card_details` text COLLATE utf8mb4_unicode_ci,
  `primary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scondary` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `designation` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `social_media` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vcard` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subfieldname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `purchased_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `orders` (`id`, `razorpay_order_id`, `razorpay_signature`, `user_id`, `card_template_id`, `card_theme_id`, `price`, `payment_id`, `coupon`, `payment_method`, `payment_status`, `status`, `cardimage`, `cardqr`, `card_details`, `primary`, `scondary`, `text`, `code`, `first_name`, `last_name`, `email`, `designation`, `about`, `phone`, `address`, `social_media`, `website`, `vcard`, `subfieldname`, `image`, `created_at`, `purchased_at`, `deleted_at`, `updated_at`) VALUES
(42,	'order_GPCxw0yJ1h5EU4',	'7783a3e530bc20506dbb7a763963b1fcfa841e45add5e982b1bad582c86a2733',	'39',	'22',	15,	489.50,	'pay_GPCyX9Dyov8PDG',	NULL,	NULL,	'2',	'2',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'dtstss',	'sdfsdf',	'das@fsf.fdg',	NULL,	NULL,	NULL,	'fdsdfsd',	NULL,	NULL,	NULL,	NULL,	NULL,	'2021-01-14 13:45:44',	'2021-01-14 13:46:28',	'0000-00-00 00:00:00',	'2021-01-14 13:46:52'),
(48,	'order_GQfrX0rV0oMyGU',	'b1a95f849b9fbe82ee5b40b42db7f73bdb2ef61e0453c4748c04f477476f6307',	'28',	'22',	0,	489.50,	'pay_GQfrduJCTAtgcR',	NULL,	NULL,	'2',	'2',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'parmod',	'kumar',	'pk@gmaikl.com',	NULL,	NULL,	NULL,	'adasd',	NULL,	NULL,	NULL,	NULL,	NULL,	'2021-01-15 14:52:58',	'2021-01-18 06:41:04',	'0000-00-00 00:00:00',	'2021-01-18 06:42:20'),
(49,	NULL,	NULL,	'48',	'23',	16,	200.00,	NULL,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'Parmod',	'Kumar',	'pk17861000@gmail.com',	NULL,	NULL,	NULL,	'mohali punjab',	NULL,	NULL,	NULL,	NULL,	NULL,	'2021-01-20 12:03:30',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'2021-01-20 12:03:30'),
(50,	'order_GRac6huAcZOl84',	'a4fa046bf9ba42e83b109d2da44ccdc2da2637f40f312c8941f9469dfc877c38',	'39',	'22',	0,	489.50,	'pay_GRacE8Fp4rkoRe',	NULL,	NULL,	'2',	'1',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'dtstsss',	'sdfsdf',	'das@fsf.fdg',	NULL,	NULL,	NULL,	'fdsdfsd',	NULL,	NULL,	NULL,	NULL,	NULL,	'2021-01-20 14:11:38',	'2021-01-20 14:12:00',	'0000-00-00 00:00:00',	'2021-01-20 14:12:00'),
(51,	'order_GRaeMAv6GPX3xk',	'72e97211cf38943c5b0cce69288d1ce262511a6ceb30a6e03e28235b49d5237b',	'39',	'22',	0,	489.50,	'pay_GRaeRSJSj7zGRp',	NULL,	NULL,	'2',	'2',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'dtstsss',	'sdfsdf',	'harjitkumar261@gmail.com',	NULL,	NULL,	NULL,	'fdsdfsd',	NULL,	NULL,	NULL,	NULL,	NULL,	'2021-01-20 14:13:52',	'2021-01-20 14:14:06',	'0000-00-00 00:00:00',	'2021-01-20 14:16:29'),
(52,	NULL,	NULL,	'28',	'22',	15,	489.50,	NULL,	NULL,	NULL,	'1',	'1',	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	NULL,	'parmod',	'kumar',	'pk@gmaikl.com',	NULL,	NULL,	NULL,	'adasd',	NULL,	NULL,	NULL,	NULL,	NULL,	'2021-01-21 05:21:48',	'0000-00-00 00:00:00',	'0000-00-00 00:00:00',	'2021-01-21 05:21:48');

DROP TABLE IF EXISTS `order_card_details`;
CREATE TABLE `order_card_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `orderfield_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fieldvalue` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


DROP TABLE IF EXISTS `pages`;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `pages` (`id`, `name`, `description`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1,	'contact-us',	NULL,	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(2,	'policy',	NULL,	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(3,	'terms',	NULL,	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(4,	'about-us',	NULL,	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53');

DROP TABLE IF EXISTS `page_contents`;
CREATE TABLE `page_contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `page_contents` (`id`, `page_id`, `title`, `content`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1,	'1',	'address',	'<p>The Poppy Factory, 20 Petersham Road, Richmond, Surrey, TW10 6UW</p>',	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(2,	'1',	'call-us',	'<p><a href=\"#\">T: +44 (0) 208 652 7787</a></p>\n<p><a href=\"#\">M: +44 (0) 7554 424167</a></p>',	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(3,	'1',	'mail-us',	'<p><a href=\"mailto:admin@gmail.com\">info@streetlife-uk.com</a></p>\n<p><a href=\"mailto:example@gmail.com\">example@gmail.com</a></p>',	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(4,	'2',	'your-privacy-is-important-to-us',	'<h3>Your privacy is important to us: </h3>\n                        <p>Therefore, we guarantee that:</p>\n                        <ul class=\"list-unstyled\">\n                            <li>\n                                <i class=\"fa fa-circle\"></i>\n                                <p>We do not rent or sell your personal information to anyone.</p>\n                            </li>\n                            <li>\n                                <i class=\"fa fa-circle\"></i>\n                                <p>Any personal information you provide will be secured by us.</p>\n                            </li>\n                            <li>\n                                <i class=\"fa fa-circle\"></i>\n                                <p>You will be able to erase all the data we have stored on you at any given\n                                    time. To request data termination, please contact our customer support.</p>\n                            </li>\n                        </ul>',	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(5,	'2',	'third-party-services',	'<h3>Third-party services: </h3>\n                        <p>\n                            We use third-party services in order to operate our website. Please note that these\n                            services may contain links to third-party apps, websites or services that are not\n                            operated by us. We make no representation or warranties\n                            with regard to and are not responsible for the content, functionality, legality,\n                            security, accuracy, or other aspects of such third-party apps, websites or services.\n                            Note that, when accessing and/or using these third-party\n                            services, their own privacy policy may apply.\n                        </p>',	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(6,	'2',	'google-analytics',	'<h3>Google Analytics: </h3>\n                        <p>This website uses Google Analytics, a web analytics service provided by Google, Inc.\n                            (“Google”). Google Analytics uses “cookies”, which are text files placed on your\n                            computer, to help the website analyze how users use the\n                            site. The information generated by the cookie about your use of the website will be\n                            transmitted to and stored by Google on servers in the United States . In case\n                            IP-anonymisation is activated on this website, your IP\n                            address will be truncated within the area of Member States of the European Union or\n                            other parties to the Agreement on the European Economic Area. Only in exceptional\n                            cases the whole IP address will be first transferred\n                            to a Google server in the USA and truncated there. The IP-anonymisation is active on\n                            this website. Google will use this information on behalf of the operator of this\n                            website for the purpose of evaluating your use of\n                            the website, compiling reports on website activity for website operators and\n                            providing them other services relating to website activity and internet usage. The\n                            IP-address, that your Browser conveys within the scope\n                            of Google Analytics, will not be associated with any other data held by Google. You\n                            may refuse the use of cookies by selecting the appropriate settings on your browser,\n                            however please note that if you do this you may\n                            not be able to use the full functionality of www.Tradix.com. You can also opt-out\n                            from being tracked by Google Analytics with effect for the future by downloading and\n                            installing Google Analytics Opt-out Browser Addon\n                        </p>',	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(7,	'2',	'information-we-collect',	'<h3>Information we collect: </h3>\n                        <p>Information we collect: </p>\n                        <ul class=\"list-unstyled\">\n                            <li>\n                                <i class=\"fa fa-circle\"></i>\n                                <p>Google ID (to identify you in our database)</p>\n                            </li>\n                            <li>\n                                <i class=\"fa fa-circle\"></i>\n                                <p>Google First & Last name</p>\n                            </li>\n                            <li>\n                                <i class=\"fa fa-circle\"></i>\n                                <p>Google Email</p>\n                            </li>\n                            <li>\n                                <i class=\"fa fa-circle\"></i>\n                                <p>Google avatar image</p>\n                            </li>\n                        </ul>\n                        <p>We do not collect passwords or any other sensitive information.</p>',	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(8,	'3',	'terms',	'<h3>Your Terms & Conditions is important to us: </h3>\n                        <p>Therefore, we guarantee that:</p>\n                        <ul class=\"list-unstyled\">\n                            <li>\n                                <i class=\"fa fa-circle\"></i>\n                                <p>We do not rent or sell your personal information to anyone.</p>\n                            </li>\n                            <li>\n                                <i class=\"fa fa-circle\"></i>\n                                <p>Any personal information you provide will be secured by us.</p>\n                            </li>\n                            <li>\n                                <i class=\"fa fa-circle\"></i>\n                                <p>You will be able to erase all the data we have stored on you at any given\n                                    time. To request data termination, please contact our customer support.</p>\n                            </li>\n                        </ul>',	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(9,	'3',	'third-party',	'<h3>Third-party services: </h3>\n                        <p>We use third-party services in order to operate our website. Please note that these\n                            services may contain links to third-party apps, websites or services that are not\n                            operated by us. We make no representation or warranties\n                            with regard to and are not responsible for the content, functionality, legality,\n                            security, accuracy, or other aspects of such third-party apps, websites or services.\n                            Note that, when accessing and/or using these third-party\n                            services, their own privacy policy may apply.</p>',	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(10,	'3',	'google-analytics',	'<h3>Google Analytics: </h3>\n                        <p>This website uses Google Analytics, a web analytics service provided by Google, Inc.\n                            (“Google”). Google Analytics uses “cookies”, which are text files placed on your\n                            computer, to help the website analyze how users use the\n                            site. The information generated by the cookie about your use of the website will be\n                            transmitted to and stored by Google on servers in the United States . In case\n                            IP-anonymisation is activated on this website, your IP\n                            address will be truncated within the area of Member States of the European Union or\n                            other parties to the Agreement on the European Economic Area. Only in exceptional\n                            cases the whole IP address will be first transferred\n                            to a Google server in the USA and truncated there. The IP-anonymisation is active on\n                            this website. Google will use this information on behalf of the operator of this\n                            website for the purpose of evaluating your use of\n                            the website, compiling reports on website activity for website operators and\n                            providing them other services relating to website activity and internet usage. The\n                            IP-address, that your Browser conveys within the scope\n                            of Google Analytics, will not be associated with any other data held by Google. You\n                            may refuse the use of cookies by selecting the appropriate settings on your browser,\n                            however please note that if you do this you may\n                            not be able to use the full functionality of www.Tradix.com. You can also opt-out\n                            from being tracked by Google Analytics with effect for the future by downloading and\n                            installing Google Analytics Opt-out Browser Addon\n                        </p>',	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(11,	'3',	'information-we-collect',	'<h3>Information we collect: </h3>\n                        <p>Information we collect: </p>\n                        <ul class=\"list-unstyled\">\n                            <li>\n                                <i class=\"fa fa-circle\"></i>\n                                <p>Google ID (to identify you in our database)</p>\n                            </li>\n                            <li>\n                                <i class=\"fa fa-circle\"></i>\n                                <p>Google First & Last name</p>\n                            </li>\n                            <li>\n                                <i class=\"fa fa-circle\"></i>\n                                <p>Google Email</p>\n                            </li>\n                            <li>\n                                <i class=\"fa fa-circle\"></i>\n                                <p>Google avatar image</p>\n                            </li>\n                        </ul>\n                        <p>We do not collect passwords or any other sensitive information.</p>',	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(12,	'4',	'about-us',	'<p><span style=\"color: #1d2228; font-family: Poppins, sans-serif; font-size: 16px; background-color: #ffffff;\">luctus massa ipsum at tempus eleifend congue lectus bibendum porttito praesent elit sapien venenatis nec urna vitae pulvinar an</span></p>',	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(13,	'4',	'our-goals',	'<p><span style=\"color: #1d2228; font-family: Poppins, sans-serif; font-size: 16px; background-color: #ffffff;\">luctus massa ipsum at tempus eleifend congue lectus bibendum porttito praesent elit sapien venenatis nec urna vitae pulvinar an</span></p>',	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(14,	'4',	'our-values',	'<p><span style=\"color: #1d2228; font-family: Poppins, sans-serif; font-size: 16px; background-color: #ffffff;\">luctus massa ipsum at tempus eleifend congue lectus bibendum porttito praesent elit sapien venenatis nec urna vitae pulvinar an</span></p>',	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(15,	'4',	'our-features',	'<p class=\"wow txt\" style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; color: #1d2228; font-family: Poppins, sans-serif; font-size: 16px; background-color: #ffffff;\">Our creative Ad agency is ranked among the finest in the US. We cultivate smart ideas for start-ups and seasoned players.</p>\n<ul class=\"feat list-unstyled\" style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; padding-left: 0px; list-style: none; color: #1d2228; font-family: Poppins, sans-serif; font-size: 16px; background-color: #ffffff;\">\n<li class=\"wow fadeInUp\" style=\"box-sizing: border-box; animation-name: fadeInUp;\">\n<h6 style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0.5rem; font-weight: 500; line-height: 1.2; font-size: 1rem;\"><span style=\"box-sizing: border-box;\">1</span>&nbsp;Modern Design</h6>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;\">luctus massa ipsum at tempus eleifend congue lectus bibendum</p>\n</li>\n<li class=\"wow fadeInUp\" style=\"box-sizing: border-box; animation-name: fadeInUp;\">\n<h6 style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0.5rem; font-weight: 500; line-height: 1.2; font-size: 1rem;\"><span style=\"box-sizing: border-box;\">2</span>&nbsp;Fully Customization</h6>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;\">luctus massa ipsum at tempus eleifend congue lectus bibendum</p>\n</li>\n<li class=\"wow fadeInUp\" style=\"box-sizing: border-box; animation-name: fadeInUp;\">\n<h6 style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 0.5rem; font-weight: 500; line-height: 1.2; font-size: 1rem;\"><span style=\"box-sizing: border-box;\">3</span>&nbsp;Retina ready</h6>\n<p style=\"box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;\">luctus massa ipsum at tempus eleifend congue lectus bibendum</p>\n</li>\n</ul>',	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53'),
(16,	'4',	'we-are',	'<p><span style=\"color: #1d2228; font-family: Poppins, sans-serif; font-size: 16px; background-color: #ffffff;\">We develop creative solutions for small and big brands alike, build authentic product identities and much more.Lorem ipsum dolor sit amet, consectetur adipiscing elit sit non facilisis vitae eu. Ultrices ut diam morbi risus dui, nec eget at lorem in id tristique in elementum leo nisi eleifend placerat magna lacus elementum ornare vehicula odio posuere quisque ultrices tempus cras id blandit maecenas in ornare quis dolor tempus risus vitae feugiat fames aliquet sed.</span></p>',	'2021-01-28 08:36:53',	NULL,	'2021-01-28 08:36:53');

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('gur.singhk@gmail.com',	'$2y$10$jWo8Q3POG3eROIvi6vlICeSqphXa84dam4DJVwSUVTDQMYjzqivEq',	'2021-01-18 04:23:48');

DROP TABLE IF EXISTS `shared_cards`;
CREATE TABLE `shared_cards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `card_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `shared_with` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `shared_cards` (`id`, `card_id`, `shared_with`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1,	'5',	'39',	NULL,	NULL,	NULL);

DROP TABLE IF EXISTS `social_media`;
CREATE TABLE `social_media` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `social_media` (`id`, `name`, `icon`, `deleted_at`, `created_at`, `updated_at`) VALUES
(1,	'Facebook',	'fa-facebook',	NULL,	'2020-08-30 13:12:02',	NULL),
(2,	'Skype',	'fa-skype',	NULL,	'2020-08-30 13:12:22',	NULL),
(3,	'WhatsApp',	'fa-whatsapp',	NULL,	'2020-08-30 13:12:59',	NULL),
(4,	'LinkedIn',	'fa-linkedin',	NULL,	'2020-08-30 13:13:20',	NULL),
(5,	'Twitter',	'fa-twitter',	NULL,	'2020-08-30 13:13:42',	NULL);

DROP TABLE IF EXISTS `sub_categories`;
CREATE TABLE `sub_categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `sub_categories` (`id`, `name`, `image`, `description`, `category_id`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1,	'PHP Developer',	NULL,	'PHP Developer',	'1',	'2020-12-22 06:10:15',	NULL,	'2021-01-15 08:27:21'),
(2,	'Sale & Purchase Cars',	NULL,	'Sale & Purchase Cars',	'2',	'2021-01-15 08:02:08',	NULL,	'2021-01-15 08:02:08'),
(3,	'Sale & Purchase Two Wheelers',	NULL,	'Sale & Purchase Two Wheelers',	'2',	'2021-01-15 08:04:02',	NULL,	'2021-01-15 08:04:02'),
(4,	'Vehicles Loan',	NULL,	'Vehicles Loan',	'2',	'2021-01-15 08:04:39',	NULL,	'2021-01-15 08:04:39'),
(5,	'Vehicles Insurance',	NULL,	'Vehicles Insurance',	'2',	'2021-01-15 08:04:49',	NULL,	'2021-01-15 08:04:49'),
(6,	'Used Vehicles',	NULL,	'Used Vehicles',	'2',	'2021-01-15 08:04:58',	NULL,	'2021-01-15 08:04:58'),
(7,	'Book a Cab',	NULL,	'Book a Cab',	'3',	'2021-01-15 08:06:16',	NULL,	'2021-01-15 08:06:16'),
(8,	'Car Rentals',	NULL,	'Car Rentals',	'3',	'2021-01-15 08:06:27',	NULL,	'2021-01-15 08:06:27'),
(9,	'Building & Real Estate Developers',	NULL,	'Building & Real Estate Developers',	'4',	'2021-01-15 08:07:48',	NULL,	'2021-01-15 08:07:48'),
(10,	'Explore by Services',	NULL,	'Services',	'4',	'2021-01-15 08:08:00',	NULL,	'2021-01-15 10:37:30'),
(11,	'Animal Transportation',	NULL,	'Animal Transportation',	'5',	'2021-01-15 08:08:56',	NULL,	'2021-01-15 08:08:56'),
(12,	'Household Goods',	NULL,	'Household Goods',	'5',	'2021-01-15 08:09:08',	NULL,	'2021-01-15 08:09:08'),
(13,	'Vehicles Transporters',	NULL,	'Vehicles Transporters',	'5',	'2021-01-15 08:09:15',	NULL,	'2021-01-15 08:09:15'),
(14,	'Business & Industrial Goods',	NULL,	'Business & Industrial Goods',	'5',	'2021-01-15 08:09:36',	NULL,	'2021-01-15 08:09:36'),
(15,	'Miltary Aerospace & Defence',	NULL,	'Miltary Aerospace & Defence',	'5',	'2021-01-15 08:09:49',	NULL,	'2021-01-15 08:09:49'),
(16,	'Food & Agriculture Products',	NULL,	'Food & Agriculture Products',	'5',	'2021-01-15 08:10:28',	NULL,	'2021-01-15 08:10:28'),
(17,	'Medical & Surgical Clothing',	NULL,	'Medical & Surgical Clothing',	'6',	'2021-01-15 08:16:35',	NULL,	'2021-01-15 08:16:35'),
(19,	'Surgical & ICU Equipments',	NULL,	'Surgical & ICU Equipments',	'6',	'2021-01-15 08:17:20',	NULL,	'2021-01-15 08:17:20'),
(20,	'Diagnostic Medical Imaging Equipment',	NULL,	'Diagnostic Medical Imaging Equipment',	'6',	'2021-01-15 08:17:44',	NULL,	'2021-01-15 08:17:44'),
(21,	'Physiotherapy & Rehab Aids',	NULL,	'Physiotherapy & Rehab Aids',	'6',	'2021-01-15 08:18:04',	NULL,	'2021-01-15 08:18:04'),
(22,	'Ayurvedic Doctors',	NULL,	'Ayurvedic Doctors',	'8',	'2021-01-15 08:20:22',	NULL,	'2021-01-15 08:20:22'),
(23,	'Bone & Joints Doctors',	NULL,	'Bone & Joints Doctors',	'8',	'2021-01-15 08:20:29',	NULL,	'2021-01-15 08:20:29'),
(24,	'Cardiologists',	NULL,	'Cardiologists',	'8',	'2021-01-15 08:20:37',	NULL,	'2021-01-15 08:20:37'),
(25,	'Chest Specialists',	NULL,	'Chest Specialists',	'8',	'2021-01-15 08:20:44',	NULL,	'2021-01-15 08:20:44'),
(26,	'Child Specialist Doctors',	NULL,	'Child Specialist Doctors',	'8',	'2021-01-15 08:20:56',	NULL,	'2021-01-15 08:20:56'),
(27,	'Cosmetic Surgeons',	NULL,	'Cosmetic Surgeons',	'8',	'2021-01-15 08:21:02',	NULL,	'2021-01-15 08:21:02'),
(28,	'Dentists',	NULL,	'Dentists',	'8',	'2021-01-15 08:21:35',	NULL,	'2021-01-15 08:21:35'),
(29,	'Diabetologists',	NULL,	'Diabetologists',	'8',	'2021-01-15 08:21:52',	NULL,	'2021-01-15 08:21:52'),
(30,	'Dietitians',	NULL,	'Dietitians',	'8',	'2021-01-15 08:22:14',	NULL,	'2021-01-15 08:22:14'),
(31,	'Front-end Developer',	NULL,	'Front-end Developer',	'1',	'2021-01-15 08:27:55',	NULL,	'2021-01-15 08:27:55'),
(32,	'Backend Developer',	NULL,	'Backend Developer',	'1',	'2021-01-15 08:28:05',	NULL,	'2021-01-15 08:28:05'),
(33,	'Full-stack Developer',	NULL,	'Full-stack Developer',	'1',	'2021-01-15 08:28:15',	NULL,	'2021-01-15 08:28:15'),
(35,	'Web Developer',	NULL,	'Web Developer',	'1',	'2021-01-15 08:28:47',	NULL,	'2021-01-15 08:28:47'),
(36,	'Desktop Developer',	NULL,	'Desktop Developer',	'1',	'2021-01-15 08:28:59',	NULL,	'2021-01-15 08:28:59'),
(37,	'Mobile Developer',	NULL,	'Mobile Developer',	'1',	'2021-01-15 08:29:06',	NULL,	'2021-01-15 08:29:06'),
(38,	'Graphics Developer',	NULL,	'Graphics Developer',	'1',	'2021-01-15 08:29:15',	NULL,	'2021-01-15 08:29:15'),
(39,	'Game Developer',	NULL,	'Game Developer',	'1',	'2021-01-15 08:29:24',	NULL,	'2021-01-15 08:29:24'),
(40,	'Data Scientist',	NULL,	'Data Scientist',	'1',	'2021-01-15 08:29:39',	NULL,	'2021-01-15 08:29:39'),
(41,	'WordPress Developer',	NULL,	'WordPress Developer',	'1',	'2021-01-15 08:29:44',	NULL,	'2021-01-15 08:29:44'),
(42,	'Security Developer',	NULL,	'Security Developer',	'1',	'2021-01-15 08:29:49',	NULL,	'2021-01-15 08:29:49'),
(43,	'IOS Developer',	NULL,	'IOS Developer',	'1',	'2021-01-15 08:30:02',	NULL,	'2021-01-15 08:30:02'),
(44,	'Android Developer',	NULL,	'Android Developer',	'1',	'2021-01-15 08:30:14',	NULL,	'2021-01-15 08:30:14'),
(45,	'Web Designer',	NULL,	'Web Designer',	'1',	'2021-01-15 08:31:45',	NULL,	'2021-01-15 08:31:45'),
(46,	'Programmer',	NULL,	'Programmer',	'1',	'2021-01-15 08:32:03',	NULL,	'2021-01-15 08:32:03'),
(47,	'Network Infrastructure',	NULL,	'Network Infrastructure',	'1',	'2021-01-15 08:33:18',	NULL,	'2021-01-15 08:33:18'),
(48,	'Cyber Security',	NULL,	'Cyber Security',	'1',	'2021-01-15 08:33:23',	NULL,	'2021-01-15 08:33:23'),
(49,	'Cloud Computing',	NULL,	'Cloud Computing',	'1',	'2021-01-15 08:33:35',	NULL,	'2021-01-15 08:33:35'),
(50,	'Skin & Hair Doctors',	NULL,	'Skin & Hair Doctors',	'8',	'2021-01-15 09:23:42',	NULL,	'2021-01-15 09:23:42'),
(51,	'Eye Specialists',	NULL,	'Eye Specialists',	'8',	'2021-01-15 09:24:11',	NULL,	'2021-01-15 09:24:11'),
(52,	'ENT Specialists',	NULL,	'ENT Specialists',	'8',	'2021-01-15 09:24:28',	NULL,	'2021-01-15 09:24:28'),
(53,	'Sexologists',	NULL,	'Sexologists',	'8',	'2021-01-15 09:24:49',	NULL,	'2021-01-15 09:24:49'),
(54,	'Thyroid Doctors',	NULL,	'Thyroid Doctors',	'8',	'2021-01-15 09:25:27',	NULL,	'2021-01-15 09:25:27'),
(55,	'Psychologists',	NULL,	'Psychologists',	'8',	'2021-01-15 09:25:47',	NULL,	'2021-01-15 09:25:47'),
(56,	'Homeopathic Doctors',	NULL,	'Homeopathic Doctors',	'8',	'2021-01-15 09:26:10',	NULL,	'2021-01-15 09:26:10'),
(57,	'Graphics Designers',	NULL,	'Graphics Designers',	'1',	'2021-01-15 09:27:10',	NULL,	'2021-01-15 09:27:10'),
(58,	'LED, LCD, Smart TV and Home Theatre',	NULL,	'LED, LCD, Smart TV and Home Theatre',	'9',	'2021-01-15 09:30:51',	NULL,	'2021-01-15 09:30:51'),
(59,	'Home Appliances & Machines',	NULL,	'Home Appliances & Machines',	'9',	'2021-01-15 09:31:06',	NULL,	'2021-01-15 09:31:06'),
(60,	'Batteries & Charge Storage Devices',	NULL,	'Batteries & Charge Storage Devices',	'9',	'2021-01-15 09:31:20',	NULL,	'2021-01-15 09:31:20'),
(61,	'Indoor Lights & Lighting Accessories',	NULL,	'Indoor Lights & Lighting Accessories',	'9',	'2021-01-15 09:32:15',	NULL,	'2021-01-15 09:32:15'),
(62,	'Solar & Renewable Energy Products',	NULL,	'Solar & Renewable Energy Products',	'9',	'2021-01-15 09:32:24',	NULL,	'2021-01-15 09:32:24'),
(63,	'Office Automation Products & Devices',	NULL,	'Office Automation Products & Devices',	'9',	'2021-01-15 09:33:08',	NULL,	'2021-01-15 09:33:08'),
(64,	'Musical Instruments',	NULL,	'Musical Instruments',	'9',	'2021-01-15 09:34:21',	NULL,	'2021-01-15 09:34:21'),
(65,	'Mobiles & Accessories',	NULL,	'Mobiles & Accessories',	'9',	'2021-01-15 09:35:12',	NULL,	'2021-01-15 09:35:12'),
(66,	'Smart Technology',	NULL,	'Smart Technology',	'9',	'2021-01-15 09:35:56',	NULL,	'2021-01-15 09:35:56'),
(67,	'Children Hospitals',	NULL,	'Children Hospitals',	'6',	'2021-01-15 09:38:16',	NULL,	'2021-01-15 09:38:16'),
(68,	'ENT Hospitals',	NULL,	'ENT Hospitals',	'6',	'2021-01-15 09:38:30',	NULL,	'2021-01-15 09:38:30'),
(69,	'Maternity Hospitals',	NULL,	'Maternity Hospitals',	'6',	'2021-01-15 09:38:46',	NULL,	'2021-01-15 09:38:46'),
(70,	'Mental Hospitals',	NULL,	'Mental Hospitals',	'6',	'2021-01-15 09:39:01',	NULL,	'2021-01-15 09:39:01'),
(71,	'Multispeciality Hospitals',	NULL,	'Multispeciality Hospitals',	'6',	'2021-01-15 09:39:32',	NULL,	'2021-01-15 09:39:32'),
(72,	'Cancer Hospitals',	NULL,	'Cancer Hospitals',	'6',	'2021-01-15 09:39:53',	NULL,	'2021-01-15 09:39:53'),
(73,	'Charitable Hospitals',	NULL,	'Charitable Hospitals',	'6',	'2021-01-15 09:40:10',	NULL,	'2021-01-15 09:40:10'),
(74,	'Kidney Hospitals',	NULL,	'Kidney Hospitals',	'6',	'2021-01-15 09:40:29',	NULL,	'2021-01-15 09:40:29'),
(75,	'Nursing Homes',	NULL,	'Nursing Homes',	'6',	'2021-01-15 09:40:42',	NULL,	'2021-01-15 09:40:42'),
(76,	'Swine Flu Testing Centres',	NULL,	'Swine Flu Testing Centres',	'6',	'2021-01-15 09:40:58',	NULL,	'2021-01-15 09:40:58'),
(77,	'Eye Hospitals',	NULL,	'Eye Hospitals',	'6',	'2021-01-15 09:41:21',	NULL,	'2021-01-15 09:41:21'),
(78,	'Diabetic Centres',	NULL,	'Diabetic Centres',	'6',	'2021-01-15 09:41:37',	NULL,	'2021-01-15 09:41:37'),
(79,	'DJ on Hire',	NULL,	'DJ on Hire',	'10',	'2021-01-15 09:43:04',	NULL,	'2021-01-15 09:43:04'),
(80,	'Event Organisers',	NULL,	'Event Organisers',	'10',	'2021-01-15 09:43:12',	NULL,	'2021-01-15 09:43:12'),
(81,	'Photographers',	NULL,	'Photographers',	'10',	'2021-01-15 09:43:23',	NULL,	'2021-01-15 09:43:23'),
(82,	'Wine Retailers',	NULL,	'Wine Retailers',	'10',	'2021-01-15 09:43:38',	NULL,	'2021-01-15 09:43:38'),
(83,	'Playback Singers',	NULL,	'Playback Singers',	'10',	'2021-01-15 09:43:50',	NULL,	'2021-01-15 09:43:50'),
(84,	'Bouncer Services',	NULL,	'Bouncer Services',	'10',	'2021-01-15 09:44:15',	NULL,	'2021-01-15 09:44:15'),
(85,	'Costumes',	NULL,	'Costumes',	'10',	'2021-01-15 09:44:24',	NULL,	'2021-01-15 09:44:24'),
(86,	'Dance Groups',	NULL,	'Dance Groups',	'10',	'2021-01-15 09:44:39',	NULL,	'2021-01-15 09:44:39'),
(87,	'Decorators',	NULL,	'Decorators',	'10',	'2021-01-15 09:44:49',	NULL,	'2021-01-15 09:44:49'),
(88,	'Fireworks',	NULL,	'Fireworks',	'10',	'2021-01-15 09:45:29',	NULL,	'2021-01-15 09:45:29'),
(89,	'Florists',	NULL,	'Florists',	'10',	'2021-01-15 09:45:51',	NULL,	'2021-01-15 09:45:51'),
(90,	'Generator & Power Backup',	NULL,	'Generator & Power Backup',	'10',	'2021-01-15 09:46:01',	NULL,	'2021-01-15 09:46:01'),
(91,	'Kids Entertainment',	NULL,	'Kids Entertainment',	'10',	'2021-01-15 09:46:14',	NULL,	'2021-01-15 09:46:14'),
(92,	'Tent House',	NULL,	'Tent House',	'10',	'2021-01-15 09:46:32',	NULL,	'2021-01-15 09:46:32'),
(93,	'Sweet Shops',	NULL,	'Sweet Shops',	'10',	'2021-01-15 09:46:42',	NULL,	'2021-01-15 09:46:42'),
(94,	'Sound System On Hire',	NULL,	'Sound System On Hire',	'10',	'2021-01-15 09:46:58',	NULL,	'2021-01-15 09:46:58'),
(95,	'Commercial Land',	NULL,	'Commercial Land',	'11',	'2021-01-15 09:50:28',	NULL,	'2021-01-15 09:50:28'),
(96,	'Commercial Spaces',	NULL,	'Commercial Spaces',	'11',	'2021-01-15 09:50:39',	NULL,	'2021-01-15 09:50:39'),
(97,	'Residence',	NULL,	'Residence',	'11',	'2021-01-15 09:51:02',	NULL,	'2021-01-15 09:51:02'),
(98,	'Commercial Rental',	NULL,	'Commercial Rental',	'11',	'2021-01-15 09:51:38',	NULL,	'2021-01-15 09:51:38'),
(99,	'Independent House Rental',	NULL,	'Independent House Rental',	'11',	'2021-01-15 09:51:49',	NULL,	'2021-01-15 09:51:49'),
(100,	'Rental',	NULL,	'Rental',	'11',	'2021-01-15 09:52:04',	NULL,	'2021-01-15 09:52:04'),
(101,	'Suits, Blazers & Waistcoats',	NULL,	'Suits, Blazers & Waistcoats',	'12',	'2021-01-15 09:54:28',	NULL,	'2021-01-15 09:54:28'),
(102,	'Ties, Socks, Caps & More',	NULL,	'Ties, Socks, Caps & More',	'12',	'2021-01-15 09:54:54',	NULL,	'2021-01-15 09:54:54'),
(103,	'Footwear',	NULL,	'Footwear',	'12',	'2021-01-15 09:55:24',	NULL,	'2021-01-15 09:55:24'),
(104,	'Winter Wear',	NULL,	'Winter Wear',	'12',	'2021-01-15 09:56:01',	NULL,	'2021-01-15 09:56:01'),
(105,	'Ethnic Wear',	NULL,	'Ethnic Wear',	'12',	'2021-01-15 09:56:15',	NULL,	'2021-01-15 09:56:15'),
(106,	'Innerwear & Loungewear',	NULL,	'Innerwear & Loungewear',	'12',	'2021-01-15 09:56:36',	NULL,	'2021-01-15 09:56:36'),
(107,	'Lingerie & Sleepwear',	NULL,	'Lingerie & Sleepwear',	'12',	'2021-01-15 09:57:20',	NULL,	'2021-01-15 09:57:20'),
(108,	'Party Dresses',	NULL,	'Party Dresses',	'12',	'2021-01-15 09:57:42',	NULL,	'2021-01-15 09:57:42'),
(109,	'Sports Wears',	NULL,	'Sports Wears',	'12',	'2021-01-15 09:57:51',	NULL,	'2021-01-15 09:57:51'),
(110,	'Summer Wear',	NULL,	'Summer Wear',	'12',	'2021-01-15 09:58:16',	NULL,	'2021-01-15 09:58:16'),
(111,	'Kid\'s Clothing',	NULL,	'Kid\'s Clothing',	'12',	'2021-01-15 09:59:01',	NULL,	'2021-01-15 09:59:01'),
(112,	'Doctors',	NULL,	'Doctors',	'13',	'2021-01-15 10:00:40',	NULL,	'2021-01-15 10:00:40'),
(113,	'Hospitals',	NULL,	'Hospitals',	'13',	'2021-01-15 10:00:55',	NULL,	'2021-01-15 10:00:55'),
(114,	'Dentists',	NULL,	'Dentists',	'13',	'2021-01-15 10:01:02',	NULL,	'2021-01-15 10:01:02'),
(115,	'Chemists',	NULL,	'Chemists',	'13',	'2021-01-15 10:01:10',	NULL,	'2021-01-15 10:01:10'),
(116,	'Pathology Labs',	NULL,	'Pathology Labs',	'13',	'2021-01-15 10:01:24',	NULL,	'2021-01-15 10:01:24'),
(117,	'Ambulance',	NULL,	'Ambulance',	'13',	'2021-01-15 10:01:33',	NULL,	'2021-01-15 10:01:33'),
(118,	'Opticians',	NULL,	'Opticians',	'13',	'2021-01-15 10:01:41',	NULL,	'2021-01-15 10:01:41'),
(119,	'Blood Banks',	NULL,	'Blood Banks',	'13',	'2021-01-15 10:02:19',	NULL,	'2021-01-15 10:02:19'),
(120,	'Breakfast Restaurants',	NULL,	'Breakfast Restaurants',	'14',	'2021-01-15 10:04:35',	NULL,	'2021-01-15 10:04:35'),
(121,	'British Restaurants',	NULL,	'British Restaurants',	'14',	'2021-01-15 10:04:44',	NULL,	'2021-01-15 10:04:44'),
(122,	'Cake Shops',	NULL,	'Cake Shops',	'14',	'2021-01-15 10:04:55',	NULL,	'2021-01-15 10:04:55'),
(123,	'Desserts',	NULL,	'Desserts',	'14',	'2021-01-15 10:05:08',	NULL,	'2021-01-15 10:05:08'),
(124,	'Dhaba Restaurants',	NULL,	'Dhaba Restaurants',	'14',	'2021-01-15 10:05:29',	NULL,	'2021-01-15 10:05:29'),
(125,	'Indian Restaurants',	NULL,	'Indian Restaurants',	'14',	'2021-01-15 10:05:47',	NULL,	'2021-01-15 10:05:47'),
(126,	'Indian Restaurants',	NULL,	'Indian Restaurants',	'14',	'2021-01-15 10:05:48',	NULL,	'2021-01-15 10:05:48'),
(127,	'Kashmiri Restaurants',	NULL,	'Kashmiri Restaurants',	'14',	'2021-01-15 10:06:06',	NULL,	'2021-01-15 10:06:06'),
(128,	'Lounge Bars',	NULL,	'Lounge Bars',	'14',	'2021-01-15 10:06:19',	NULL,	'2021-01-15 10:06:19'),
(129,	'Lunch Buffet Restaurants',	NULL,	'Lunch Buffet Restaurants',	'14',	'2021-01-15 10:06:28',	NULL,	'2021-01-15 10:06:28'),
(130,	'Malaysian Restaurants',	NULL,	'Malaysian Restaurants',	'14',	'2021-01-15 10:06:38',	NULL,	'2021-01-15 10:06:38'),
(131,	'Modern Indian Restaurants',	NULL,	'Modern Indian Restaurants',	'14',	'2021-01-15 10:06:49',	NULL,	'2021-01-15 10:06:49'),
(132,	'Night Clubs',	NULL,	'Night Clubs',	'14',	'2021-01-15 10:06:59',	NULL,	'2021-01-15 10:06:59'),
(133,	'Non Veg Thali Restaurants',	NULL,	'Non Veg Thali Restaurants',	'14',	'2021-01-15 10:07:11',	NULL,	'2021-01-15 10:07:11'),
(134,	'Pastry Shops',	NULL,	'Pastry Shops',	'14',	'2021-01-15 10:07:26',	NULL,	'2021-01-15 10:07:26'),
(135,	'Punjabi Restaurants',	NULL,	'Punjabi Restaurants',	'14',	'2021-01-15 10:07:37',	NULL,	'2021-01-15 10:07:37'),
(136,	'Pure Veg Restaurants',	NULL,	'Pure Veg Restaurants',	'14',	'2021-01-15 10:07:46',	NULL,	'2021-01-15 10:07:46'),
(137,	'Sea Food Biryani Restaurants',	NULL,	'Sea Food Biryani Restaurants',	'14',	'2021-01-15 10:07:59',	NULL,	'2021-01-15 10:07:59'),
(138,	'Banquet Halls',	NULL,	'Banquet Halls',	'15',	'2021-01-15 10:10:51',	NULL,	'2021-01-15 10:10:51'),
(139,	'Bridal Requisites',	NULL,	'Bridal Requisites',	'15',	'2021-01-15 10:10:59',	NULL,	'2021-01-15 10:10:59'),
(140,	'Jewellery',	NULL,	'Jewellery',	'15',	'2021-01-15 10:11:08',	NULL,	'2021-01-15 10:11:08'),
(141,	'Florists',	NULL,	'Florists',	'15',	'2021-01-15 10:11:19',	NULL,	'2021-01-15 10:11:19'),
(142,	'Photographers',	NULL,	'Photographers',	'15',	'2021-01-15 10:11:28',	NULL,	'2021-01-15 10:11:28'),
(143,	'Mehendi Artists',	NULL,	'Mehendi Artists',	'15',	'2021-01-15 10:11:41',	NULL,	'2021-01-15 10:11:41'),
(144,	'Bridal Makeup',	NULL,	'Bridal Makeup',	'15',	'2021-01-15 10:11:50',	NULL,	'2021-01-15 10:11:50'),
(145,	'Bridal Wear On Hire',	NULL,	'Bridal Wear On Hire',	'15',	'2021-01-15 10:12:00',	NULL,	'2021-01-15 10:12:00'),
(146,	'Doli On Hire',	NULL,	'Doli On Hire',	'15',	'2021-01-15 10:12:12',	NULL,	'2021-01-15 10:12:12'),
(147,	'Fireworks',	NULL,	'Fireworks',	'15',	'2021-01-15 10:12:25',	NULL,	'2021-01-15 10:12:25'),
(148,	'Mandap Decorator',	NULL,	'Mandap Decorator',	'15',	'2021-01-15 10:12:36',	NULL,	'2021-01-15 10:12:36'),
(149,	'Wedding Grocery',	NULL,	'Wedding Grocery',	'15',	'2021-01-15 10:12:50',	NULL,	'2021-01-15 10:12:50'),
(150,	'Wedding Halls',	NULL,	'Wedding Halls',	'15',	'2021-01-15 10:13:00',	NULL,	'2021-01-15 10:13:00'),
(151,	'Bus',	NULL,	'Bus',	'3',	'2021-01-15 10:13:48',	NULL,	'2021-01-15 10:13:48'),
(152,	'Trains',	NULL,	'Trains',	'3',	'2021-01-15 10:13:56',	NULL,	'2021-01-15 10:13:56'),
(153,	'Home Stays',	NULL,	'Home Stays',	'3',	'2021-01-15 10:14:17',	NULL,	'2021-01-15 10:14:17'),
(154,	'Flights',	NULL,	'Flights',	'3',	'2021-01-15 10:14:32',	NULL,	'2021-01-15 10:14:32'),
(155,	'Badminton',	NULL,	'Badminton',	'19',	'2021-01-15 10:15:28',	NULL,	'2021-01-15 10:15:28'),
(156,	'Billiards & Pool',	NULL,	'Billiards & Pool',	'19',	'2021-01-15 10:15:39',	NULL,	'2021-01-15 10:15:39'),
(157,	'Cricket',	NULL,	'Cricket',	'19',	'2021-01-15 10:15:48',	NULL,	'2021-01-15 10:15:48'),
(158,	'Cycling',	NULL,	'Cycling',	'19',	'2021-01-15 10:15:56',	NULL,	'2021-01-15 10:15:56'),
(159,	'Football',	NULL,	'Football',	'19',	'2021-01-15 10:16:09',	NULL,	'2021-01-15 10:16:09'),
(160,	'Golf',	NULL,	'Golf',	'19',	'2021-01-15 10:16:18',	NULL,	'2021-01-15 10:16:18'),
(161,	'Hockey',	NULL,	'Hockey',	'19',	'2021-01-15 10:16:29',	NULL,	'2021-01-15 10:16:29'),
(162,	'Skatings',	NULL,	'Skatings',	'19',	'2021-01-15 10:16:38',	NULL,	'2021-01-15 10:16:38'),
(163,	'Swimming',	NULL,	'Swimming',	'19',	'2021-01-15 10:16:46',	NULL,	'2021-01-15 10:16:46'),
(164,	'Volleyball',	NULL,	'Volleyball',	'19',	'2021-01-15 10:16:58',	NULL,	'2021-01-15 10:16:58'),
(165,	'Tennis',	NULL,	'Tennis',	'19',	'2021-01-15 10:17:07',	NULL,	'2021-01-15 10:17:07');

DROP TABLE IF EXISTS `temp_card_details`;
CREATE TABLE `temp_card_details` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `temp_card_details` (`id`, `user_id`, `card_id`, `card_details`, `created_at`, `deleted_at`, `updated_at`) VALUES
(7,	'35',	'13',	'[{\"name\":\"category_id\",\"value\":\"1\"},{\"name\":\"sub_category_id\",\"value\":\"1\"},{\"name\":\"Primary\",\"value\":\"#000080\"},{\"name\":\"Secondary\",\"value\":\"#808000\"},{\"name\":\"Blog\",\"value\":\"#008000\"},{\"name\":\"Text\",\"value\":\"#008000\"},{\"name\":\"Heading\",\"value\":\"#ff00ff\"},{\"name\":\"social_media[Facebook]\",\"value\":\"dfdf\"},{\"name\":\"social_media[Skype]\",\"value\":\"fgdg\"},{\"name\":\"code\",\"value\":\"+91\"},{\"name\":\"social_media[WhatsApp]\",\"value\":\"44444444444\"},{\"name\":\"social_media[LinkedIn]\",\"value\":\"gdfgfdg\"},{\"name\":\"social_media[Twitter]\",\"value\":\"dfgfdg\"},{\"name\":\"first_name\",\"value\":\"asdasd\"},{\"name\":\"last_name\",\"value\":\"sad\"},{\"name\":\"company\",\"value\":\"dsffd\"},{\"name\":\"companyabout\",\"value\":\"fsdf\"},{\"name\":\"designation\",\"value\":\"sadsad\"},{\"name\":\"description[1]\",\"value\":\"dsad\"},{\"name\":\"description[2]\",\"value\":\"dsadsa\"},{\"name\":\"primaryemail\",\"value\":\"dsfsd@sasdf.fgf\"},{\"name\":\"secondaryemail\",\"value\":\"pkksd@gmail.com\"},{\"name\":\"primaryphone\",\"value\":\"4584587584\"},{\"name\":\"secondaryphone\",\"value\":\"52234234\"},{\"name\":\"address\",\"value\":\"ffdg\"},{\"name\":\"website\",\"value\":\"\"},{\"name\":\"services[1]\",\"value\":\"\"},{\"name\":\"services[2]\",\"value\":\"\"}]',	'2020-12-21 23:12:30',	NULL,	'2020-12-24 02:26:07'),
(8,	'35',	'18',	'[{\"name\":\"category_id\",\"value\":\"1\"},{\"name\":\"sub_category_id\",\"value\":\"1\"},{\"name\":\"Primary\",\"value\":\"#800000\"},{\"name\":\"Secondary\",\"value\":\"#000000\"},{\"name\":\"Blog\",\"value\":\"#008000\"},{\"name\":\"Text\",\"value\":\"#800080\"},{\"name\":\"Heading\",\"value\":\"#c0c0c0\"},{\"name\":\"social_media[]\",\"value\":\"\"},{\"name\":\"first_name\",\"value\":\"\"},{\"name\":\"last_name\",\"value\":\"\"},{\"name\":\"company\",\"value\":\"\"},{\"name\":\"companyabout\",\"value\":\"\"},{\"name\":\"primaryemail\",\"value\":\"\"},{\"name\":\"primaryphone\",\"value\":\"\"},{\"name\":\"address\",\"value\":\"\"},{\"name\":\"website\",\"value\":\"\"},{\"name\":\"services[1]\",\"value\":\"hhhhh\"},{\"name\":\"services[2]\",\"value\":\"hhhhh\"},{\"name\":\"services[3]\",\"value\":\"fffff\"},{\"name\":\"services[4]\",\"value\":\"fff\"}]',	'2020-12-25 05:06:03',	NULL,	'2020-12-25 08:30:02'),
(9,	'28',	'18',	'{\"card_id\":\"18\",\"category_id\":\"1\",\"sub_category_id\":\"1\",\"Primary\":\"#800000\",\"Secondary\":\"#008000\",\"Blog\":\"#800080\",\"Text\":\"#c0c0c0\",\"Heading\":\"#d7d700\",\"social_media\":{\"Facebook\":\"sdfsdf\",\"Skype\":\"fdf\",\"WhatsApp\":\"fgfdgfg\"},\"first_name\":null,\"last_name\":null,\"company\":null,\"companyabout\":null,\"primaryemail\":null,\"primaryphone\":null,\"address\":\"dgfdgfgd\",\"website\":\"dfgfdg\",\"services\":{\"1\":\"ssss\",\"2\":\"ddddd\",\"3\":\"ffff\",\"4\":\"ffff\"}}',	'2020-12-27 23:06:25',	NULL,	'2021-01-05 12:25:34'),
(10,	'28',	'13',	'{\"card_id\":\"13\",\"card_theme_id\":\"0\",\"category_id\":\"1\",\"sub_category_id\":\"1\",\"social_media\":{\"Skype\":null,\"Facebook\":null,\"WhatsApp\":null,\"LinkedIn\":null,\"Twitter\":null},\"first_name\":\"sdsd\",\"last_name\":\"dssd\",\"primaryphone\":\"asdasd\",\"primaryemail\":\"sdsad\",\"address\":\"sadsad\"}',	'2020-12-28 00:06:00',	NULL,	'2021-01-14 13:08:08'),
(11,	'28',	'15',	'{\"card_id\":\"15\",\"category_id\":\"1\",\"sub_category_id\":\"1\",\"Primary\":\"#000000\",\"Secondary\":\"#800000\",\"Blog\":\"#800080\",\"social_media\":{\"Facebook\":\"sfsdfd\",\"Skype\":\"sdfdsf\"},\"first_name\":\"sdfdsf\",\"last_name\":\"dfdsf\",\"primaryemail\":\"fdsf@fsaf.ghfgh\",\"primaryphone\":\"45435435\",\"address\":\"sdfds\",\"website\":\"sfdsdf\"}',	'2021-01-01 11:45:24',	NULL,	'2021-01-01 11:46:06'),
(12,	'28',	'14',	'{\"card_id\":\"14\",\"category_id\":\"1\",\"sub_category_id\":\"1\",\"Primary\":\"#000000\",\"Secondary\":\"#008000\",\"Blog\":\"#00ff00\",\"social_media\":{\"Facebook\":\"dsadas\",\"Skype\":\"sdsadsa\",\"WhatsApp\":\"dsadsa\",\"LinkedIn\":\"sdsadsa\"},\"first_name\":\"asdsad\",\"last_name\":\"asdsad\",\"primaryemail\":\"sdsadsa@aff.fhfgh\",\"primaryphone\":\"5435534\",\"address\":\"fdsfsdf\"}',	'2021-01-01 11:57:56',	NULL,	'2021-01-01 11:58:31'),
(13,	'44',	'13',	'{\"card_id\":\"13\",\"category_id\":\"1\",\"sub_category_id\":\"1\",\"color\":{\"Primary\":\"#ff0000\",\"Secondary\":\"#ffff00\",\"Blog\":\"#ffff00\",\"Text\":\"#0000ff\",\"Heading\":\"#0000ff\"},\"social_media\":{\"Facebook\":\"www.facebook.com\",\"Skype\":\"www.skype.com\",\"WhatsApp\":\"www.whatsapp.com\",\"Twitter\":\"www.twitter.com\",\"LinkedIn\":\"www.linkedin.com\"},\"first_name\":\"Test\",\"last_name\":\"Test\",\"primaryphone\":\"9915004993\",\"primaryemail\":\"harjitkumar261@gmail.com\",\"address\":\"E-287, Industrial Area, Sector 75, Sahibzada Ajit Singh Nagar,\"}',	'2021-01-08 05:07:37',	NULL,	'2021-01-08 05:26:06'),
(14,	'45',	'13',	'{\"card_id\":\"13\",\"category_id\":\"1\",\"sub_category_id\":\"1\",\"color\":{\"Primary\":\"#8fe015\",\"Secondary\":\"#008000\",\"Blog\":\"#800080\",\"Text\":\"#c0c0c0\",\"Heading\":\"#ff0000\"},\"social_media\":{\"Facebook\":\"dsf\",\"WhatsApp\":\"56636346\",\"LinkedIn\":\"gfdgfd\",\"Twitter\":\"dfgfdg\",\"Skype\":\"dgdfgfdg\"},\"first_name\":\"fhfghfgh\",\"last_name\":\"fghfgh\",\"primaryphone\":\"7574554544\",\"primaryemail\":\"sdfdfds@fsd.fgh\",\"address\":\"fsdfsdf\"}',	'2021-01-08 05:18:01',	NULL,	'2021-01-08 07:47:53'),
(15,	'28',	'17',	'{\"card_id\":\"17\",\"category_id\":\"1\",\"sub_category_id\":\"1\",\"color\":{\"Primary\":null,\"Blog\":null,\"Text\":null},\"social_media\":{\"Facebook\":null,\"0\":null},\"first_name\":null,\"last_name\":null,\"primaryphone\":null,\"primaryemail\":null,\"address\":null}',	'2021-01-11 13:07:57',	NULL,	'2021-01-11 13:14:51'),
(16,	'39',	'14',	'{\"card_id\":\"14\",\"category_id\":\"1\",\"sub_category_id\":\"1\",\"color\":{\"Primary\":\"#35AA8A\",\"Secondary\":\"#0E3125\",\"Blog\":\"#FFFFFF\"},\"social_media\":{\"Facebook\":\"facebook.com\",\"Skype\":\"skype.me\",\"WhatsApp\":\"9996888722\",\"LinkedIn\":\"linkedin.com\",\"Twitter\":\"twitter.com\"},\"first_name\":null,\"last_name\":null,\"primaryphone\":null,\"primaryemail\":null,\"address\":null}',	'2021-01-13 04:43:02',	NULL,	'2021-01-13 04:45:14'),
(17,	'39',	'13',	'{\"card_id\":\"13\",\"category_id\":\"1\",\"sub_category_id\":\"1\",\"username\":\"1\",\"social_media\":{\"Facebook\":\"fsfdsf\"},\"first_name\":null,\"last_name\":null,\"primaryphone\":null,\"primaryemail\":null,\"address\":null}',	'2021-01-13 04:47:40',	NULL,	'2021-01-13 06:49:20'),
(18,	'28',	'21',	'{\"card_id\":\"21\",\"card_theme_id\":\"14\",\"category_id\":\"1\",\"sub_category_id\":\"1\",\"social_media\":{\"Facebook\":\"dsdad\"},\"first_name\":null,\"last_name\":null,\"primaryphone\":null,\"primaryemail\":null,\"address\":null}',	'2021-01-14 10:18:07',	NULL,	'2021-01-14 10:18:17'),
(19,	'28',	'22',	'{\"card_id\":\"22\",\"card_theme_id\":\"15\",\"category_id\":\"1\",\"sub_category_id\":\"1\",\"social_media\":{\"Facebook\":\"dsadsa\",\"WhatsApp\":\"ghj\"},\"first_name\":\"parmod\",\"last_name\":\"kumar\",\"primaryphone\":\"7695845778\",\"primaryemail\":\"pk@gmaikl.com\",\"address\":\"adasd\"}',	'2021-01-14 13:29:48',	NULL,	'2021-01-21 05:21:14'),
(20,	'39',	'22',	'{\"card_id\":\"22\",\"card_theme_id\":\"0\",\"category_id\":\"1\",\"sub_category_id\":\"1\",\"social_media\":{\"Facebook\":\"dsdsad\",\"Skype\":\"www.skype.com\"},\"first_name\":\"dtstsss\",\"last_name\":\"sdfsdf\",\"primaryphone\":\"4234234\",\"primaryemail\":\"harjitkumar261@gmail.com\",\"address\":\"fdsdfsd\"}',	'2021-01-14 13:45:16',	NULL,	'2021-01-20 14:13:46'),
(21,	'47',	'22',	'{\"card_id\":\"22\",\"card_theme_id\":\"0\",\"category_id\":\"2\",\"sub_category_id\":\"5\",\"social_media\":{\"Facebook\":\"saSa\",\"Skype\":\"sdsa\"},\"first_name\":\"dsada\",\"last_name\":\"asdsa\",\"primaryphone\":\"4234\",\"primaryemail\":\"fsdf@ada.g\",\"address\":\"fsdf\"}',	'2021-01-15 14:03:52',	NULL,	'2021-01-15 14:23:25'),
(22,	'28',	'23',	'{\"card_id\":\"23\",\"card_theme_id\":\"16\",\"category_id\":\"2\",\"sub_category_id\":\"2\",\"first_name\":\"Gurpreet\",\"last_name\":\"Singh\",\"primaryphone\":\"9888854300\",\"secondaryphone\":\"9888854300\",\"companyabout\":\"Pracoders\",\"designation\":\"Hardware Engineer\",\"primaryemail\":null,\"address\":null,\"website\":null}',	'2021-01-17 17:23:47',	NULL,	'2021-01-28 06:14:24'),
(23,	'48',	'23',	'{\"card_id\":\"23\",\"card_theme_id\":\"16\",\"category_id\":\"1\",\"sub_category_id\":\"1\",\"social_media\":{\"Facebook\":\"https:\\/\\/www.facebook.com\",\"Skype\":\"https:\\/\\/www.skype.com\\/\",\"Twitter\":\"https:\\/\\/twitter.com\\/\"},\"first_name\":\"Parmod\",\"last_name\":\"Kumar\",\"primaryphone\":\"76969515\",\"secondaryphone\":\"7355088618\",\"companyabout\":\"Techno Deviser IT company\",\"designation\":\"PHP Developer\",\"primaryemail\":\"pk17861000@gmail.com\",\"address\":\"mohali punjab\",\"website\":\"https:\\/\\/www.td.com\\/\"}',	'2021-01-20 11:59:03',	NULL,	'2021-01-20 12:03:15'),
(24,	'28',	'25',	'{\"card_id\":\"25\",\"card_theme_id\":\"21\",\"category_id\":\"1\",\"sub_category_id\":\"44\",\"social_media\":{\"Facebook\":null},\"first_name\":null,\"last_name\":null,\"primaryphone\":null,\"bottomtitle\":null,\"companyabout\":null,\"designation\":null,\"dheading\":{\"1\":null},\"description\":{\"1\":null},\"primaryemail\":null,\"address\":null,\"website\":null}',	'2021-01-21 12:58:04',	NULL,	'2021-01-21 13:00:09'),
(25,	'28',	'29',	'{\"card_id\":\"29\",\"card_theme_id\":\"40\",\"category_id\":\"35\",\"first_name\":null,\"last_name\":null,\"primaryphone\":null,\"secondaryphone\":null,\"company\":null,\"bottomtitle\":null,\"companyabout\":null,\"primaryemail\":null,\"secondaryemail\":null,\"address\":null,\"website\":null,\"services\":{\"1\":null,\"2\":null,\"3\":null,\"4\":null}}',	'2021-01-25 03:07:01',	NULL,	'2021-01-25 03:29:43'),
(26,	'28',	'26',	'{\"card_id\":\"26\",\"card_theme_id\":\"22\",\"category_id\":\"10\",\"sub_category_id\":\"87\",\"social_media\":{\"Facebook\":\"www.facebook.com\",\"Skype\":\"www.skype.com\",\"WhatsApp\":\"www.whatsapp.com\",\"LinkedIn\":\"www.linkedin.com\",\"Twitter\":\"www.twitter.com\"},\"first_name\":null,\"last_name\":null,\"primaryphone\":null,\"company\":null,\"companyabout\":null,\"designation\":null,\"dheading\":{\"1\":null},\"description\":{\"1\":null},\"primaryemail\":null,\"address\":null,\"website\":null}',	'2021-01-27 12:55:52',	NULL,	'2021-01-27 12:56:16');

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(111) COLLATE utf8mb4_unicode_ci NOT NULL,
  `otp` varchar(111) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gender` varchar(111) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `profile` varchar(111) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about` varchar(111) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_role` int(11) NOT NULL DEFAULT '0',
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `otp`, `gender`, `profile`, `about`, `user_role`, `email_verified_at`, `password`, `remember_token`, `created_at`, `deleted_at`, `updated_at`) VALUES
(28,	'parmodk',	'admin@td.com',	'7696399515',	'',	NULL,	NULL,	NULL,	1,	'2021-01-04 08:15:22',	'$2y$10$BebiHntxPMfLdHa2qL0Fe.yPEohzUBeb/SgSmnUS/pjRJOnmLCaiS',	'RxQ6SKkUmEQn7nN1CKbKm0H7T7i6sxaJsG35i1WTXSFtnO7fBwef03Dk0vST',	'2020-12-16 05:53:42',	NULL,	'2021-01-04 08:15:22'),
(29,	'asdsa',	'asdsa@afa.ghfgh',	'76965655',	'361739',	NULL,	NULL,	NULL,	0,	NULL,	'$2y$10$1nx8n5.6x8qR8N1CFmR6auj8H063F/lr7D2NKnkvyEcf8nasi9cGS',	NULL,	'2020-12-16 06:32:59',	NULL,	'2020-12-16 06:32:59'),
(30,	'ASas',	'adas@adsa.fgfh',	'65445456',	'',	NULL,	NULL,	NULL,	0,	NULL,	'$2y$10$Fl/FM/EDxBeASajwopYeUeXv62QVJmkqno4dK0OHEuMWkNLz8/RsK',	NULL,	'2020-12-16 06:40:28',	NULL,	'2020-12-16 06:45:35'),
(31,	'saa',	'adas@fsd.fh',	'4554545',	'',	NULL,	NULL,	NULL,	0,	NULL,	'$2y$10$On2irmvI3VUWJ8mW7LI3kufqh/zGdpOdLayOHVAa8je5IteZpapgW',	NULL,	'2020-12-16 06:47:56',	NULL,	'2020-12-16 06:48:15'),
(32,	'SAS',	'ASDSD@SADSF.FGH',	'4544465',	'',	NULL,	NULL,	NULL,	0,	NULL,	'$2y$10$mgtNr63h8mn5Pxb/ohioROssBJxmCpKtKmJcU94QERnhn0mEyxjzy',	NULL,	'2020-12-16 06:56:52',	NULL,	'2020-12-16 06:57:09'),
(35,	'testaa',	'test@test.com',	'76958458758',	'',	NULL,	NULL,	NULL,	0,	NULL,	'$2y$10$eeMKjdOBoUh3rAyf4Qcil.U30boEk9TLrNqp4V6d5eKSo6WEGLQDK',	'DDnJLZcxNwYNG4hKF8XUpoK7TkUblfx3zjSxzyv30lLndLyjIscgoYQASjyR',	'2020-12-17 23:10:05',	NULL,	'2020-12-24 07:20:41'),
(36,	'fsdf',	'sdfds@dasf.fgh',	'4354354352',	NULL,	NULL,	NULL,	NULL,	0,	NULL,	'$2y$10$sopy/34BPdirVxgDlNh9auvMc8.r2Tget5qMCkMvAeCdRRZknAAyi',	NULL,	'2020-12-24 06:10:54',	NULL,	'2020-12-24 06:10:54'),
(37,	'sadasd',	'asdsad@fsf.fgghfg',	'4564455656',	'796531',	NULL,	NULL,	NULL,	0,	NULL,	'$2y$10$IJSUrqG12IQQV9uaLfZwrOovMlV56Of0wypiFd.Uo1ZdYyBTV6QbC',	NULL,	'2020-12-24 08:46:02',	NULL,	'2020-12-24 08:46:02'),
(38,	'gggg',	'test22@test.com',	'45875848758',	'',	NULL,	NULL,	NULL,	0,	NULL,	'$2y$10$9PO5aeYVa25Z.VtvRdRKjOrTKAVvk6kyFsIQJd10xJPxeJTsdVFbq',	NULL,	'2020-12-28 02:59:35',	NULL,	'2020-12-28 03:54:51'),
(39,	'test',	'test11@test.com',	'555588889',	'',	NULL,	NULL,	NULL,	0,	'2021-01-13 04:38:24',	'$2y$10$vkyB7lYMzDqG5reY0UuBEucebmfIa72n3BMYQdt6586XrLun/gVny',	'Fyg6A22vabXwa9jRrobB2cF6wVbIFc5feSVbPMkfC2XoGHBzjypTzfqNkXHh',	'2020-12-29 01:11:58',	NULL,	'2021-01-13 04:38:24'),
(40,	'Gurpreet Singh',	'gur.singhk@gmail.com',	'9888854300',	'',	NULL,	NULL,	NULL,	0,	'2021-01-19 14:02:34',	'$2y$10$E8.N9OK27kFzO/ddgfnt4Ojni.ZR/e6Q9pzJB8Rmc.sJ9mFu.oJSm',	'5EAXvHW7oKPRqKq32NBhBI4CfYND00P4PKIAV8fzyz3rqg8xe22acckZ6i3v',	'2021-01-02 09:44:12',	NULL,	'2021-01-19 14:02:34'),
(41,	'djjjj',	'fdsfd@hhhs.com',	'4232223432',	'569343',	NULL,	NULL,	NULL,	0,	NULL,	'$2y$10$yAM8e/nUmxLCt01iswSApeMHRnJmbLNtIXe1qXWVD3lXE1VHtxuPK',	NULL,	'2021-01-04 05:14:10',	NULL,	'2021-01-04 05:14:10'),
(42,	'djjjj',	'fdsffddsfd@hhhs.com',	'4232223432',	'755368',	NULL,	NULL,	NULL,	0,	NULL,	'$2y$10$hZPecNyz7ZDGKb1ViPaE6e3FRISzad0shFwX7gDOo.OpXIz9HT5HO',	NULL,	'2021-01-04 05:15:18',	NULL,	'2021-01-04 05:15:18'),
(43,	'djjjj',	'fdsffddsdddfd@hhhs.com',	'4232223432',	'',	NULL,	NULL,	NULL,	0,	NULL,	'$2y$10$2HdwhGxELgBfSjdfAD375e5vMZ9S0ssvtGCTF9jqrPpdHuHRlN1Ry',	NULL,	'2021-01-04 05:17:32',	NULL,	'2021-01-04 05:17:45'),
(44,	'Harjit',	'harjitkumar261@gmail.com',	'9915004993',	'',	NULL,	NULL,	NULL,	0,	'2021-01-13 06:43:37',	'$2y$10$uJBwGP/mz1jXwTF7rbY77.zrrEJyzij0pjWHjb60Ccb8OM0PHrZNK',	'p7BrHbcrxNXh3nntpj5lRXJkYiz3a5CtKK17riyu6k8r0qKmlcAGIeUmEZmB',	'2021-01-08 04:48:37',	NULL,	'2021-01-13 06:43:37'),
(45,	'dfsdf',	'sdfd@dfdsf.fdgfdg',	'545435435',	'497188',	NULL,	NULL,	NULL,	0,	NULL,	'$2y$10$EiRU4AlU0U2HCz.xfan18OjuzZjxZL6YNQcjx5WrvwVtRp.2Z3Ni.',	'zpPOspZdGi9MXctgVL41naldXcEJv7481hQPij45mvlK8BB5ZdZJXnMnqB9M',	'2021-01-08 04:54:34',	NULL,	'2021-01-08 04:54:34'),
(46,	'nisha',	'technodeviser05@gmail.com',	'9653261440',	'',	NULL,	NULL,	NULL,	0,	'2021-01-14 07:07:22',	'$2y$10$24iBBku9ydeS8n0tA1rrV.D8Ra11Sv8YmYfxnO21UtpSEQxq79w4i',	NULL,	'2021-01-14 07:06:39',	NULL,	'2021-01-14 07:07:22'),
(47,	'parmod',	'parmod@kumar.com',	'4578548754',	'',	NULL,	NULL,	NULL,	0,	'2021-01-15 14:03:15',	'$2y$10$BnszWiXqB2clQ7oI/plgx.UEFqh.C5W89F2Ha6vszocUjcR2veayS',	NULL,	'2021-01-15 14:03:03',	NULL,	'2021-01-15 14:03:15'),
(48,	'parmod kumar',	'pk7861000@gmail.com',	'7696399515',	'',	NULL,	NULL,	NULL,	0,	'2021-01-20 11:57:39',	'$2y$10$CapuDrP7iXj7/Qs3yrbFh.psgE5kk3evIIo4lb6QcxbrkqQl3kteO',	NULL,	'2021-01-20 11:57:26',	NULL,	'2021-01-20 11:57:39');

DROP TABLE IF EXISTS `users_cards`;
CREATE TABLE `users_cards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_template_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_theme_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cardpdf` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cardimage` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cardqr` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `vcard` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `users_cards` (`id`, `user_id`, `card_template_id`, `card_theme_id`, `order_id`, `cardpdf`, `cardimage`, `cardqr`, `vcard`, `created_at`, `deleted_at`, `updated_at`) VALUES
(4,	'39',	'22',	'0',	'42',	'users/ZU1tQkZaZ3FRSExjVUFJcnp6L3pEUT09/orders/bERrQ1VIRjZsd2FUeXRVNXdGL01KZz09/Q8JNyYssDLCReH9wZty64X0WhqD6JHQjnRJ4I9vJ.png',	NULL,	'users/ZU1tQkZaZ3FRSExjVUFJcnp6L3pEUT09/orders/bERrQ1VIRjZsd2FUeXRVNXdGL01KZz09/qrcode.png',	'users/ZU1tQkZaZ3FRSExjVUFJcnp6L3pEUT09/orders/bERrQ1VIRjZsd2FUeXRVNXdGL01KZz09/contact.vcf',	'2021-01-14 13:46:52',	NULL,	'2021-01-14 13:46:52'),
(5,	'28',	'22',	'0',	'48',	'users/elRuZkRtcXcrOXBUTjVBSFVMTGtDUT09/orders/QmtIZk5SUW9oU2VVM09jL0F0dUxnUT09/6C1EbyzXBt04PvOMAktTBs2H3Cg3LIznJ5ShitsZ.pdf',	'users/elRuZkRtcXcrOXBUTjVBSFVMTGtDUT09/orders/QmtIZk5SUW9oU2VVM09jL0F0dUxnUT09/POHiyrn9HcX0mL7UzO96KKFwZrSQCkrCeCYgdorR.png',	'users/elRuZkRtcXcrOXBUTjVBSFVMTGtDUT09/orders/QmtIZk5SUW9oU2VVM09jL0F0dUxnUT09/qrcode.png',	'users/elRuZkRtcXcrOXBUTjVBSFVMTGtDUT09/orders/QmtIZk5SUW9oU2VVM09jL0F0dUxnUT09/contact.vcf',	'2021-01-18 06:42:20',	NULL,	'2021-01-20 11:38:39'),
(6,	'39',	'22',	'0',	'51',	'users/ZU1tQkZaZ3FRSExjVUFJcnp6L3pEUT09/orders/QTJrbitndS9MVm4waDhscm5GOU9Rdz09/MZobiWL9z0uA2ltXIDGyWXEKJPEguMTCxwRVQdCW.pdf',	'users/ZU1tQkZaZ3FRSExjVUFJcnp6L3pEUT09/orders/QTJrbitndS9MVm4waDhscm5GOU9Rdz09/w9itnFkh0v6iMxp8peDuyXhspSJlgqgUBByo3ZcU.png',	'users/ZU1tQkZaZ3FRSExjVUFJcnp6L3pEUT09/orders/QTJrbitndS9MVm4waDhscm5GOU9Rdz09/qrcode.png',	'users/ZU1tQkZaZ3FRSExjVUFJcnp6L3pEUT09/orders/QTJrbitndS9MVm4waDhscm5GOU9Rdz09/contact.vcf',	'2021-01-20 14:16:29',	NULL,	'2021-01-20 14:16:29');

DROP TABLE IF EXISTS `user_card_likes`;
CREATE TABLE `user_card_likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_card_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_template_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `likes` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `user_card_likes` (`id`, `user_id`, `user_card_id`, `card_template_id`, `likes`, `created_at`, `deleted_at`, `updated_at`) VALUES
(1,	'28',	'5',	'22',	1,	'2021-01-19 14:46:26',	NULL,	'2021-01-21 04:59:31'),
(2,	'48',	'5',	'22',	1,	'2021-01-20 11:58:30',	NULL,	'2021-01-21 04:59:31'),
(3,	'39',	'5',	'22',	0,	'2021-01-20 14:08:14',	NULL,	'2021-01-21 07:54:03'),
(5,	'48',	'5',	'22',	0,	'2021-01-20 14:34:08',	NULL,	'2021-01-21 04:59:31');

DROP TABLE IF EXISTS `user_card_views`;
CREATE TABLE `user_card_views` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_card_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `card_template_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `views` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


-- 2021-01-29 05:18:12
