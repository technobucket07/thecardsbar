
<p>
<h4>Hello,</h4>
<p><span>Card</span> (<span>{{$card_name}}</span>) has been successfully completed for User ({{$name}}) at <span>{{$date}}</span>.</p>
<p>Where UserId is {{$user_id}} and Card id  is {{$card_id}}.</p>
</p>
<p>Please Download Your Card <a href="{{$cardpdf}}"> here</a> or scan <a href="{{$cardqr}}"> QR </a>. </p>
<p>Thanks</p>

