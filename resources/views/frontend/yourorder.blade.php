@extends('frontend.layouts.master')
@section('content')
@if(session()->has('success'))
    <div class="alert alert-success">
        {{ session()->get('success') }}
    </div>
@endif
<div class="container">
<h3 class="recent_order_title">{{$page}}</h3>
<div class="row headings_orders">
  <div class="col my-auto">
    Created at
  </div>
	<div class="col my-auto">
		Preview
	</div>
	<div class="col my-auto">
		Card Name
	</div>
	<div class="col my-auto">
		Card Details
	</div>
	<div class="col my-auto">
		Card Price
	</div>
	<div class="col my-auto">
		Payment Status
	</div>
	<div class="col my-auto">
	
	</div>
</div>
<div class="row">
  <div class="col-md-12">
    @if(count($orders) !=0)
    @foreach($orders as $order)
    <div class="card card-2 recent_order">
      <div class="card-body">
         <div class="media">
            <div class="media-body my-auto text-right">
			<div class="container">
               <div class="row my-auto flex-column flex-md-row">
                  <div class="col my-auto">
                    <h6 class="mb-0">{{date($order->created_at)}}</h6>
                  </div>
                  <div class="col my-auto">
                     <!-- <h6 class="mb-0">Aluminum plan</h6> -->
                     <img src="{{url($order->themeimage ? $order->themeimage :($order->picture?$order->picture:''))}}" style="width: 100px;">
                  </div>
                  <div class="col my-auto">
                     <h6 class="mb-0">{{$order->cardtitle}}</h6>
                  </div>
                  <div class="col my-auto">
                     <h6 class="mb-0">{{$order->carddescription}}</h6>
                  </div>
                  <div class="col my-auto">
                     <h6 class="mb-0">&#8377;{{$order->price}}</h6>
                  </div>
                  <div class="col my-auto">
                     <h6 class="mb-0">
                      @if($order->payment_status =='2')
                      Paid
                      @else
                      Unpaid
                      @endif
                   </h6>
                  </div>
                  
                  <div class="col my-auto">
                    @if($order->payment_status =='1')
                      <a href="{{route('checkout',[$order->id])}}" class="btnCustomStyle2 btn-solid">Checkout</a>
                        <div class="razoreform" style="display: none;">
                          <button id="rzp-button1">Pay</button>
                        </div>
                      @else
                        @if($order->status =='1')
                          <a href="#" class="btnCustomStyle2 btn-solid">Process</a>
                        @elseif($order->status =='2')
                          <a href="{{route('MyCards')}}" class="btnCustomStyle2 btn-solid">Completed</a>
                        @endif
                      @endif
                  </div>
               </div>
            </div>
			</div>
         </div>
      </div>
    </div>
    @endforeach
    @else
    <div style="text-align: center;">
       <span>{{$empty}}</span>
    </div>
    @endif
  </div>
</div>
</div>
@endsection
@section('scripts')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script type="text/javascript">
  $(".razorpay").on("click", function(){
    var order_id =$(this).data('order_id');
    $.ajax({
    url: "{{route('PayNow')}}",
    method: 'post',
    data: {
      order_id:order_id,
      _token:"{{ csrf_token() }}"
    },
    success: function(result){
       eval(result);
       $('#rzp-button1').click();
    }});
  });
</script>
@endsection