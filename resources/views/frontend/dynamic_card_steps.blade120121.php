@extends('frontend.layouts.master')
@section('css')
@section('content')
    <!-- BACK BUTTON STARTS HERE -->
  
 <div class="preloader"></div>
 
    <!--<section>-->
    <!--    <div class="container">-->
    <!--        <div class="row">-->
    <!--            <div class="col-md-12">-->
    <!--                <p class="backBtn">-->
    <!--                    <a class="btnCustomStyle2 btn-solid" href="index.html">-->
    <!--                        <span>Back</span>-->
    <!--                    </a>-->
    <!--                </p>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->
    <!-- BACK BUTTON END HERE -->

    <section class="upperCircles">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="progressbar list-unstyled" style="width: 100%;">
                        <li class="active" style=" width: 33%;"></li>
                        <li id="active1"  style=" width: 33%;"></li>
                        <li id="active2"  style=" width: 33%;"></li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- FORM WIZARD STARTS HERE -->
    <section class="mainFormWizard">
        <div class="container-fluid">
            <span id="theme_name" style="display: none;">sky</span>
            <span id="trans_name" style="display: none;">slide</span>
            <span id="rtl" style="display: none;">0</span>
            <div class="row">
                <div class="col-md-9">
                  <ul>
                    @foreach ($errors->all() as $error) 
                      <li>
                        <div class="alert alert-warning">
                       <strong>Warning!</strong> {{ $error }}
                        </div>
                      </li>
                    @endforeach
                  </ul>
                    <form id="wizard_example_2"  method="post" enctype="multipart/form-data" >
                       <div id="loader1" style="position: relative;">
                          <!--<div id="loader2" class="center1" ></div> </br>-->
                          
                         <h2 style="text-align: center;font-size:14px;">Please Waite ...</h2>
                   </div>
                        @csrf
                        <input type="hidden" name="card_id" id="card_id" value="{{$card->id}}">
                         <fieldset>
                            <legend>Crad type</legend>
                            <div class="row">
                              <div class="col-lg-6">
                                <div class="form-group">
                                  <label>Category</label>
                                  @php
                                    $catselected ='';
                                    if (array_key_exists('category_id',$cardtempdetail)){
                                      $catselected = $cardtempdetail['category_id'];
                                    }
                                  @endphp
                                  {{ Form::select('category_id',$categories,old('category_id') ? old('category_id') :$catselected ,['class'=>'form-control category', 'id'=>'category_id','required'=>true]) }}
                                </div>
                              </div>
                              <div class="col-lg-6">
                                <div class="form-group">
                                  <label>Sub Category</label>
                                  @php
                                    $subcatselected = old('sub_category_id');
                                    if (array_key_exists('sub_category_id',$cardtempdetail)){
                                      $subcatselected = $cardtempdetail['sub_category_id'];
                                    }
                                  @endphp
                                  {{ Form::select('sub_category_id',[],$subcatselected,['class'=>'form-control subcategory','id'=>'sub_category_id']) }}
                                  <input type="hidden"  id="subcategoryid" value="{{$subcatselected}}">
                                </div>
                              </div>
                            </div>
                          </fieldset>
                        <fieldset>
                            <legend>Color</legend>
                            <div class="row">
                              @php
                              $colortypes = unserialize($card->colortype);
                              @endphp
                              @if(!empty($colortypes))
                                @foreach($colortypes as $colortype)

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>{{$colortype}} Color</label>
                                           <select id="{{$colortype}}" name="color[{{$colortype}}]" class='form-control'>
                                               <option value="">Choose Color</option>
                                                 @foreach($color as $colors)
                                                 @php
                                                  $colselected ='';
                                                  $cardcolor =array();
                                                if(!empty($cardtempdetail['color'])){
                                                    $cardcolor = (array)$cardtempdetail['color'];
                                                }
                                                  if (array_key_exists($colortype,$cardcolor)){
                                                    if($cardcolor[$colortype] == $colors->hexString){
                                                      $colselected = 'selected';
                                                    }
                                                  }
                                                @endphp
                                              <option value="{{$colors->hexString}}" {{$colselected}} style="color:{{$colors->hexString}};height:10px;"> &nbsp;<span >{{$colors->name}}</span></option>
                                              @endforeach
                                           </select>
                                           
                                        </div>
                                       
                                    </div>
                                </div>
                                @endforeach
                              @endif
                            </div>
                        </fieldset>
                      
                        <fieldset>
                            <legend>Social Links</legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="col" style="color:red;font-size:16px;"></span>
                                    <ul class="list-unstyled socialLinksboxx">
                                      <div class="socialmedia">
                                        @php
                                        $si=1;
                                        @endphp
                                        @if(!empty($cardtempdetail['social_media']))
                                        @foreach($cardtempdetail['social_media'] as $key => $socialmedia)
                                        <div class="row sociallink">
                                          <div class="col-md-4">
                                            <select class="form-control selectsocialMedia">
                                              <option value="">Select</option>
                                              @foreach($socialMedia as $social)
                                              @php
                                              $soselct ='';
                                                if($social['name'] == $key){
                                                  $soselct="selected";
                                                }
                                              @endphp
                                                <option value="{{$social['name']}}" {{$soselct}}>{{$social['icon']}}</option>
                                                @endforeach
                                              </select>
                                          </div>
                                          <div class="col-md-4">
                                            <input type="text" name="social_media[]" class="form-control socialMedialink" placeholder="" value="{{$socialmedia}}">
                                          </div>
                                          @if($si==1)
                                          <span><button class="btn btn-success btn-add" type="button"><i class="fa fa-plus-square-o" aria-hidden="true"></i> </button></span>
                                          @else
                                          <span><button class="btn btn-danger remove_field" type="button"><i class="fa fa-minus-square-o" aria-hidden="true"></i></button></span>
                                          @endif
                                        </div>
                                          @php
                                          $si++;
                                          @endphp
                                          @endforeach
                                        @endif
                                      </div>
                                    </ul>
                                </div>
                            </div>
                        </fieldset>
                      <fieldset>
                            <legend>Profile </legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        Personal Information
                                                    </a>
                                                </h4>
                                            </div>
                      <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                          
                    <div class="panel-body personalBoxxx">
                      <div class="row">
                          @if($card->username =='1')
                            @php
                            $firstname ='';
                            $lastname  ='';
                              if(array_key_exists('first_name',$cardtempdetail)){
                                $firstname = $cardtempdetail['first_name'];
                              }
                              if(array_key_exists('last_name',$cardtempdetail)){
                                $lastname = $cardtempdetail['last_name'];
                              }
                            @endphp
                        <div class="col-md-12 col-lg-6">
                            <div class="form-group ">
                                <input type="text" class="form-control" name="first_name" placeholder="First Name" value="{{$firstname}}">
                                <small class="text-muted1 first_name"></small>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="form-group ">
                                <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="{{$lastname}}">
                                <small class="text-muted1 last_name"></small>
                            </div>
                        </div>
                      @endif

                    @if($card->primaryphone =='1')
                      @php
                      $primaryphone='';
                        if(array_key_exists('primaryphone',$cardtempdetail)){
                          $primaryphone = $cardtempdetail['primaryphone'];
                        }
                      @endphp
                    <div class="col-md-12 col-lg-6 ">
                        <div class="form-group">
                            <input type="text" name="primaryphone" class="form-control" placeholder="Primary Number" maxlength="10" value="{{$primaryphone}}">
                                <small class="text-muted1 phone"></small>
                        </div>
                    </div>
                    @endif

                      @if($card->username2 =='1')
                            @php
                            $otherfname ='';
                            $otherlname  ='';
                              if(array_key_exists('otherfname',$cardtempdetail)){
                                $otherfname = $cardtempdetail['otherfname'];
                              }
                              if(array_key_exists('otherlname',$cardtempdetail)){
                                $otherlname = $cardtempdetail['otherlname'];
                              }
                            @endphp
                        <div class="col-md-12 col-lg-6">
                            <div class="form-group ">
                                <input type="text" class="form-control" name="otherfname" placeholder="Other First Name" value="{{$otherfname}}">
                                <small class="text-muted1 otherfname"></small>
                            </div>
                        </div>
                        <div class="col-md-12 col-lg-6">
                            <div class="form-group ">
                                <input type="text" class="form-control" placeholder="Other Last Name" name="otherlname" value="{{$otherlname}}">
                                <small class="text-muted1 otherlname"></small>
                            </div>
                        </div>
                      @endif
                      @if($card->secondaryphone =='1')
                        @php
                        $secondaryphone='';
                          if(array_key_exists('secondaryphone',$cardtempdetail)){
                            $secondaryphone = $cardtempdetail['secondaryphone'];
                          }
                        @endphp
                    <div class="col-md-12 col-lg-6 ">
                        <div class="form-group">
                            <input type="text" name="secondaryphone" class="form-control" placeholder="Secondary Number" maxlength="10" value="{{$secondaryphone}}">
                                <small class="text-muted1 phone"></small>
                        </div>
                    </div>
                    @endif


                                       @if($card->company =='1')
                                       @php
                                       $company ='';
                                       if(array_key_exists('company',$cardtempdetail)){
                                            $company = $cardtempdetail['company'];
                                        }
                                       @endphp
                                       <div class="col-md-12 col-lg-6">
                                          <div class="form-group ">
                                              <input type="text" class="form-control" id="company" placeholder="Company Name" name="company" value="{{$company}}">
                                              <small class="text-muted1 company"></small>
                                          </div>
                                      </div>
                                       @endif
                                       @if($card->bottomtitle =='1')
                                        @php
                                        $bottomtitle='';
                                          if(array_key_exists('bottomtitle',$cardtempdetail)){
                                            $bottomtitle = $cardtempdetail['bottomtitle'];
                                          }
                                        @endphp
                                      <div class="col-md-12 col-lg-6">
                                          <div class="form-group ">
                                              <input type="text" class="form-control" placeholder="Bottom title" name="bottomtitle" value="{{$bottomtitle}}">
                                              <small class="text-muted1 bottomtitle"></small>
                                          </div>
                                      </div>
                                    @endif
                                       @if($card->companyabout =='1')
                                       @php
                                       $companyabout='';
                                       if(array_key_exists('companyabout',$cardtempdetail)){
                                            $companyabout = $cardtempdetail['companyabout'];
                                        }
                                       @endphp
                                       <div class="col-md-12 col-lg-6">
                                          <div class="form-group ">
                                              <input type="text" class="form-control" id="companyabout" placeholder="About Your Company" name="companyabout" value="{{$companyabout}}">
                                              <small class="text-muted1 companyabout"></small>
                                          </div>
                                      </div>
                                       @endif

                                        @if($card->designation =='1')
                                        @php
                                        $designation ='';
                                          if(array_key_exists('designation',$cardtempdetail)){
                                            $designation = $cardtempdetail['designation'];
                                          }
                                        @endphp
                                        <div class="col-md-12 col-lg-6">
                                            <div class="form-group ">
                                                <input type="text" class="form-control" placeholder="Designation" name="designation" value="{{$designation}}">
                                                 <small class="text-muted1 designation"></small>
                                            </div>
                                        </div>
                                      @endif
                                @if($card->description =='1')
                                @for($i=1; $i<=$card->min_no_description; $i++)
                                @php
                                $desarr = (array)$cardtempdetail['description'];
                                $dvalue = $desarr[$i];

                                $headingarr = (array)$cardtempdetail['dheading'];
                                $hvalue = $headingarr[$i];
                                @endphp
                                <div class="col-md-12">
                                  <div class="form-group ">
                                      <input type="text" class="form-control DescriptionHeading" placeholder="Heading of Description. Example About,Hobby, Study" data-no="{{$i}}" value="{{$hvalue}}" name="dheading[{{$i}}]">
                                  </div>
                                    <div class="form-group ">
                                        <textarea class="form-control descriptiontext{{$i}}" rows="3" name="description[{{$i}}]" placeholder="Description" maxlength="100">{{$dvalue}}</textarea>
                                        <small class="text-muted1 about"></small>
                                    </div>
                                </div>
                                @endfor
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingfour">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                Contact Information
                            </a>
                        </h4>
                    </div>
                    <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                        <div class="panel-body businessInformation">
                            <div class="row">
                                    @if($card->calltagline =='1')
                                        @php
                                        $calltagline='';
                                          if(array_key_exists('calltagline',$cardtempdetail)){
                                            $calltagline = $cardtempdetail['calltagline'];
                                          }
                                        @endphp
                                      <div class="col-md-12 col-lg-6">
                                          <div class="form-group ">
                                              <input type="text" class="form-control" placeholder="Enter Call Us Tag line" name="calltagline" value="{{$calltagline}}">
                                              <small class="text-muted1 calltagline"></small>
                                          </div>
                                      </div>
                                    @endif
                                    @if($card->openclosetime =='1')
                                        @php
                                        $openclosetime='';

                                          if(array_key_exists('openclosetime',$cardtempdetail)){
                                          $opentime = (array)$cardtempdetail['openclosetime'];
                                          }
                                        @endphp
                                      <div class="col-md-12 col-lg-6">
                                          <div class="form-group ">
                                              <input type="text" class="form-control" placeholder="Enter Opening time (8:00 - 19:00)" name="openclosetime[Time]" value="{{$opentime['Time']}}">
                                          </div>
                                      </div>
                                      <div class="col-md-12 col-lg-6">
                                        <div class="form-group ">
                                            <input type="text" class="form-control" placeholder="Enter Opening days (Mon - Fri)" name="openclosetime[days]" value="{{$opentime['days']}}">
                                        </div>
                                      </div>
                                    @endif

                                                      @if($card->primaryemail =='1')
                                        @php
                                        $primaryemail='';
                                          if(array_key_exists('primaryemail',$cardtempdetail)){
                                            $primaryemail = $cardtempdetail['primaryemail'];
                                          }
                                        @endphp
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="email" class="form-control" placeholder="Email" name="primaryemail" value="{{$primaryemail}}">
                                                                <small class="text-muted1 email"></small>
                                                            </div>
                                                        </div>
                                                      @endif
                                                      @if($card->secondaryemail =='1')
                                      @php
                                        $secondaryemail='';
                                          if(array_key_exists('secondaryemail',$cardtempdetail)){
                                            $secondaryemail = $cardtempdetail['secondaryemail'];
                                          }
                                        @endphp
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="email" class="form-control" placeholder="Email" name="secondaryemail" value="{{$secondaryemail}}">
                                                                <small class="text-muted1 email"></small>
                                                            </div>
                                                        </div>
                                                      @endif
                              
                                  @if($card->address =='1')
                                        @php
                                        $address='';
                                          if(array_key_exists('address',$cardtempdetail)){
                                            $address = $cardtempdetail['address'];
                                          }
                                        @endphp
                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Address" name="address" maxlength="80" value="{{$address}}">
                                                                 <small class="text-muted1 address"></small>
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if($card->website =='1')
                                        @php
                                        $website='';
                                          if(array_key_exists('website',$cardtempdetail)){
                                            $website = $cardtempdetail['website'];
                                          }
                                        @endphp
                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Website" name="website" value="{{$website}}">
                                                                 <small class="text-muted1 website"></small>
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if($card->profile =='1')

                                                          <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Profile Image</label>
                                                                    <input type="file" name="profileimg" id="input-file-now" class="dropify" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if($card->background =='1')
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Background Image</label>
                                                                    <input type="file" name="backgroundimg" id="input-file-background" class="dropify" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if($card->logo =='1')
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Logo Image</label>
                                                                    <input type="file" name="logo" id="input-file-logo" class="dropify" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endif
                                                        @if($card->gallery =='1')
                                                        @for($g=1; $g<= $card->max_no_gallery; $g++)
                                                          <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                  <label>Gallery{{$g}} Image</label>
                                                                  <input type="file" name="gallery[{{$g}}]" id="input-file-gallery{{$g}}" class="dropify" />
                                                                </div>
                                                            </div>
                                                          </div>
                                                        @endfor
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        @if($card->services =='1')
                                        <div class="panel panel-default">
                                          <div class="panel-heading" role="tab" id="headingfour">
                                            <h4 class="panel-title">
                                               <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#Services" aria-expanded="false" aria-controls="collapsefour">
                                               Services Information
                                               </a>
                                            </h4>
                                          </div>
                                         <div id="Services" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                            <div class="panel-body businessInformation">
                                               <div class="row">
                                                @for($s=1; $s <= $card->max_no_services; $s++)
                                          @php
                                          
                                            $srvalue='';
                                            if(!empty($cardtempdetail['services'])){
                                              $servicesarr = (array)$cardtempdetail['services'];
                                              //echo "<pre>";print_r($servicesarr);die('sss');
                                              $srvalue = $servicesarr[$s];
                                            }
                                          @endphp

                                                  @if($card->serviceswithimg =='1')
                                                  <div class="col-md-6">
                                                    <div class="form-group">
                                                        <div class="uploadBackground">
                                                            <label>Service Image 1</label>
                                                            <input type="file" name="serviceimage[{{$s}}]" id="input-file-service{{$s}}" class="dropify" />
                                                        </div>
                                                    </div>
                                                  </div>
                                                  @endif
                                                  <div class="col-md-12 col-lg-6">
                                                     <div class="form-group ">
                                                        <input type="text" class="form-control" placeholder="Enter Services {{$s}}" name="services[{{$s}}]" value="{{$srvalue}}">
                                                        <small class="text-muted1 services{{$s}}"></small>
                                                     </div>
                                                  </div>
                                                @endfor
                                               </div>
                                            </div>
                                         </div>
                                      </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                            <div class="col-md-12">
                                <p class="backBtn">
                                    <a class="btnCustomStyle2 btn-solid" href="javascript:void(0);">
                                        <span>Preview</span>
                                    </a>
                                </p>
                            </div>
                          
                       </form>
                    </div>
                <div class="col-md-3">
                    
                    <div id="frame" style="background-image: url('{{ url("web_assets")}}/img/Phone1.png');background-color: #cccccc;background-position: center;background-repeat: no-repeat;background-size: cover;
  position: relative;width: 263px;height: 535px;border-radius: 30px;"></br>
   <div id="loader" class="center" ></div> 
                         <div id="previewImage" ></div>
                        
                    </div>
                   
      
                    <div id="img">
                         <img class="img-fluid mx-auto d-block "  src="{{ url('web_assets')}}/img/features.png" alt="">
                    </div>
                  
                   
                </div>
                
            </div>
    <style>
        .abtSec {
      background: transparent;
   
    }
    
    .topHeader {
    background: transparent; 
        /*border-radius: 20px 20px 0px 0px;*/
    }
    .wrapper {
   
     background: transparent;
     /*border-radius: 20px 20px 20px 20px;*/
}
p{
    font-size:13px;
}
h6{
    font-size:12px;
}

element.style {
}

.socialMedia li .fa {
    font-size: 22px;
    text-align: center;
    text-decoration: none;
    width: 35px;
    height: 35px;
    line-height: 35px;
    /* color: #c8c8c8; */
}
    </style>
     
            
<div id="html-content-holder" style="width: 333px;">
    <div >
            <section class="wrapper" style="display: block;height: 660px;width: 320px;margin-left: 13px;margin-top: 14.1px;border-radius: 30px;" id="wrapper">

        <!-- HEADER STARTS HERE -->
        <header>
            <section class="topHeader" style="padding: 30px 30px 56px 30px;border-radius: 30px 30px 0px 0px;">
                <div class="firstName" style="font-size:25px;">James</div>
                <div class="lastName" style="font-size:20px;">Anderson</div>
                <div class="designation" style="font-size:14px;">Sales Manager</div>
            </section>
        </header>
        <!-- HEADER END HERE -->

        <!-- ABOUT STARTS HERE -->
        <section class="abtSec" style="padding: 40px 10px 0 20px;">
            <table class="">
                <tr>
                    <p class="userImg"></p>
                </tr>
                <tr>
                    <td style="width:100%;"> <h1 id="h1" style="font-size:13px">About Me</h1></td>
                </tr>
                <tr>
                    <td style="width:100%;"><p class="content" style="text-align: justify;"></p></td>
                </tr>
            </table>
           
                
            </p>
           
            
        </section>
        <!-- ABOUT END HERE -->

        <!-- CONTACT STARTS HERE -->
        <section class="infoSection"  style="padding: 0 20px;margin-top:-20px;">
            <h2 id="h2" style="font-size:13px">contact Me</h2>
          
            <table class="list-unstyled">
                 
                <tr id="location">
                   
                </tr>
                   
               
                <tr id="phone">
                    
                </tr>
                
                <tr id="email">
                 
                </tr>
          </table>
        </section>
        <!-- CONTACT END HERE -->



        <!-- CONTACT STARTS HERE -->
        <section class="searchMe" style="padding: 0 20px;">
            <h2 style="font-size:13px">search Me @</h2>
            <table align="center">
                <tr>
                    <td>
                  <ul class="list-unstyled socialMedia d-flex" style="text-align:center;">
              
                
                   </ul>
                 </td>
            </tr>
            </table>
        </section>
        <!-- CONTACT END HERE -->


        <!-- FOOTER STARTS HERE -->
        <footer>
            <section class="footer">
                <!--<p class="webLink">-->
                   
                <!--</p>-->
                <!--<p class="nameUser"></p>-->
                
                <table>
                 
                     <tr class="webLink1">
                        
                     </tr>
                     
               
             
                 <tr class="nameUser1">
                         
                     </tr>
                 </table>
            </section>
        </footer>
        <!-- FOOTER END HERE -->
     </section>
</div>
 </div>      
        </div>  
    </section>
    <input type="hidden" value="{{$card->maxsocialmedia}}" id="max_no_socialmedia">
    <!-- FORM WIZARD END HERE -->
@endsection

@section('scripts')
    <script src="{{ url('web_assets/js/html2canvas.js') }}"></script>
    <script src="{{ url('web_assets/js/form-wizard.js') }}"></script>
    <script src="{{ url('web_assets/js/jscolor.js') }}"></script>
    <script src="{{ url('web_assets/js/dropify.js') }}"></script>
     
<style>
    canvas {
    height: 510px;
    width: 245px;
    margin-top: -11px;
    border-radius: 25px;
    margin-left: 5px;
    }
</style>
    <script>
        jscolor.presets.default = {
            borderColor:'rgba(236,239,245,0.6)', width:200, height:100,
            buttonColor:'rgba(70,51,255,1)', shadowColor:'rgba(0,0,0,0.05)'
        };
    </script>

    <script>
    
        $(document).ready(function(){
           
            	$('#myModal').hide();
            	 $('#h1').hide();
            	 $('#h2').hide();
            	 $('.searchMe').hide();
            	 $('.btnCustomStyle2').hide();
            	 $('#code').val('+91');
            	 $('#loader').hide(); 
                 $('#loader1').hide(); 
            	 $('#wrapper').hide();
            	 $('.preloader').fadeOut('slow');
            view();
            $('.sf-btn-finish').click(function(){
                    $('.preloader').fadeIn('slow');
                   
            });
            $('select').change(function(){
              $('.btnCustomStyle2').show();
            });
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });
           
            $("[type='file']").focusout(function(){
            
               var files =$('#input-file-now').prop('files')[0];
              if(files[0]!==''){
                  if(files[0].size > (1024*1024)){
                  alert('Please Insert Image Max 2MB.!');
                  $("#input-file-now").val(null);
                  }
              }
            
             
               });
               
             $('.btnCustomStyle2').click(function(){
                  $('.preloader').fadeIn('slow');
                  	$('#loader').show();
                  	
               var  files =$('#input-file-now').prop('files')[0];
            
              
                var form_data = new FormData($('#wizard_example_2')[0]);
                  //form_data.append("file", files);
                    
   
                       $.ajax({
                            type: 'post',
                            url:  "{{route('card')}}",
                            
                            data: form_data,
                            async: false,
                            dataType:'json',
                            success: function (data1) {
                            var    data=data1.va;
                                console.log(data1);
                                
                           
                                $('.contactIcon').css('background',data.scondary);
                                 if(data.img!==undefined){
                                     
                                    
                                 $('.userImg').html('<td style="width:70%;"></td><td style="border:1px solid '+data.primary+';height:80px;border-radius: 40px;" align="right"><img class="img-fluid img-fluid1" src="http://thecardsbar.com/card/image/'+data.img+'" alt="userimage" style="border-radius: 40px;height: 82px;width: 82px; "></td>')
                                 }
                                
                                $('.firstname').text(data.first_name);
                                $('.lastname').text(data.last_name);
                                $('.designation').text(data.designation);
                                 if(data.about!==null){
                                       $('#h1').show();
                                $('.content').text(data.about); 
                                 }
                                  if(data.first_name!==null){
                                $('.nameUser1').html('<td style="width:600px;text-align:center;"><p  style="text-align:center;font-size: 11px;"><span>'+data.first_name+'</span>&nbsp;<span>'+data.last_name+'</span></p></td>'); 
                                  }
                                   if(data.address!==null || data.phone!==null || data.email!==null){
                                    $('#h2').show();
                              }
                                 if(data.address!==null){
                                $('#location').html('<td style="width:40px;height:40px;"><p style="width:40px;font-size:20px;text-align:center;height:40px;border-radius: 20px;background:transparent;;"><i  class="fa fa-map-marker" aria-hidden="true" style="font-size:33px;margin-left:0px;margin-top:1px;margin-top: -3px;color:'+data.scondary+';" ></i></p></td><td><div class="contactInfo" style="margin-left: 5px;line-height: 0.5;"><h6 style="font-weight: 700;font-size: 12px;color:'+data.text+';">Location</h6><p style="line-height: 1.1;height: 30px;margin-top: -5px;color:'+data.scondary+';">'+data.address+'</p></div></td>');
                                 }
                                  if(data.phone!==null){
                                $('#phone').html('<td style="width:40px;height:40px;"><p style="width:40px;font-size:20px;text-align:center;height:40px;border-radius: 20px;background:transparent;"><i class="fa fa-mobile" aria-hidden="true" style="font-size:35px;margin-left:1px;margin-top:3px;color:'+data.scondary+';"></i></p></td><td><div class="contactInfo" style="margin-left: 5px;line-height: 0.5;"><h6 style="font-weight: 700;font-size: 12px;color:'+data.text+';">Phone Number</h6><p style="color:'+data.scondary+';"><a href="#">'+data.phone+'</a></p></div></td>'); 
                                  }
                                   if(data.email!==null){
                                $('#email').html('<td style="width:40px;height:40px;"><p style="width:40px;font-size:20px;text-align:center;height:40px;border-radius: 20px;background:transparent;"><i   class="fa fa-envelope" aria-hidden="true" style="font-size:23px;margin-left:1px;margin-top:6px;color:'+data.scondary+';"></i></p></td><td> <div class="contactInfo" style="margin-left: 5px;line-height: 0.5;"><h6 style="font-weight: 700;font-size: 12px;color:'+data.text+';">Email Address</h6><p style="color:'+data.scondary+';"><a href="#">'+data.email+'</a></p></div></td>');
                                   }
                                    if(data.website!==null){
                                        
                                  $('.webLink1').html('<td style="width:600px;"><p id="p" class="webLink" style="color:'+data.scondary+';font-size: 12px;"><a href="#" style="text-align:center;color:'+data.scondary+'"><i class="fa fa-globe" aria-hidden="true"></i> &nbsp; '+data.website1.substr(8)+'</a>  </p></td>');
                                    }
                                
                                
                                var i;
                                var html='';
                              if(data.social_media[1]!==null || data.social_media[2]!==null || data.social_media[3]!==null){
                                   $('.searchMe').show();
                              }
                                   for(i=1; i<= data1.count; i++){
                                       if(data.social_media[i]!==null)
                                      
                                       html +='<li style=" background:'+data.scondary+'"><a href="'+data.social_media[i]+'" class="fa '+data.socialMediaList[i]+'"></a></li>';
                                       
                                   }
                                 $('.socialMedia').html(html);
                                 $(".content").css("color",data.text);
                                 $("#p").css("color",data.scondary);
                                 $("h1").css("color",data.scondary);
                                 $("h2").css("color",data.scondary);
                                 $("h6").css("color",data.text);
                                 $(".topHeader").css("background",data.scondary);
                                 $(".topHeader").css("color",data.text);
                                 $(".abtSec").css("background",data.primary);
                                 $(".wrapper").css("color",data.text);
                                 $(".wrapper").css("background",data.primary);
                                 $(".wrapper").css("display","block");
                                 $('.preloader').fadeOut('slow');
                               	$('#img').hide(); 
                               		setTimeout(function(){ $('#loader').hide();  }, 4000);
                               	 	dview();
                               		//$('#myModal').show();
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                   
      
         });
           
         $('button').click(function(){
             $('#myModal').hide(); 
         });   
            
            $('.finish-btn').click(function(){
                 $('form').attr('action', "{{url('/placeorder')}}");

                 //$('form').attr('action', "{{url('/generate_pdf')}}");
                //  $('form').attr('target','popup');
                //  $('form').attr(' onclick',"window.open('/generate_pdf','popup','width=800,height=800');");
            });
            
            function view(){
            
                $.ajax({
                    url:"{{route('delete-image')}}",
                    method:"get",
                    success: function(data) {
                        console.log(data);
                    }
                    
                });
            }
            
            
            
// global variable
 
    function dview() {
         var element = $("#html-content-holder"); // global variable
       var getCanvas; 

         html2canvas(element, {
              x: 50, y: 100,
              draggable: true,
         onrendered: function (canvas) {
                 
                $("#previewImage").html(canvas);
                getCanvas = canvas;
                console.log(canvas);
             }
         });
             
          $('#frame').show(); 
         $('#wrapper').hide(); 
        $('#img').hide(); 
       
    }
    $('#frame').hide(); 
  $('#wrapper').hide();  
 
 
      //("#Secondary option[value="+ color +"]").prop('disabled', true);              
    //   $("#text option[value=" + color + "]").prop('disabled', true);
    //     $("#Primary option[value="+ color +"]").prop('disabled', true);
   

    $("#Primary").change(function(){
       var color=$("#Primary").val();
        $("#Primary option[value="+ color +"]").prop('disabled', false);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
    }) ;   
        
  $("#Secondary").change(function(){
       var color=$("#Secondary").val();
       $("#Secondary option[value="+ color +"]").prop('disabled', false);
       $("#Primary option[value="+ color +"]").prop('disabled', true);
       $("#text option[value=" + color + "]").prop('disabled', true);
    }) ;   
    
    $("#text").change(function(){
       var color=$("#text").val();
        $("#text option[value=" + color + "]").prop('disabled', false);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
         $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
        
         $('.sf-controls').css("display","none");
        
        $('select').change(function(){
             var select1=$('#Primary').val();
             var select2=$('#Secondary').val();
             var select3=$('#text').val();
             if(select1!=='Choose Color' && select2!=='Choose Color' && select3!=='Choose Color'){
                 $('.sf-controls').css("display","block");
                   $('#active1').attr('class','active');
             }
            
        });
     
     $('.next-btn').click(function(){
              $('.sf-controls').css("display","block");
        });   
        
        
        //  $('.socialLinksboxx').on("focusout","input",function(){
             
           
        //      var input1=$('input[name="social_media[1]"]').val();
        //      var input2=$('input[name="social_media[2]"]').val();
        //      var input3=$('input[name="social_media[3]"]').val();
        //      var input4=$('input[name="social_media[4]"]').val();
        //      var input5=$('input[name="social_media[5]"]').val();
             
        //      if(input1!=='' && input2!=='' && input3!=='' && input4!=='' && input5!==''){
        //            $('.sf-controls').css("display","block");
        //              $('#active2').attr('class','active');
        //      }else{
        //            $('.sf-controls').css("display","none");
        //            $('.col').text('*Please fill all the boxes');
        //      }
            
        // });
        
        
        
        //  $('.panel-group').on("focusout","input",function(){
            
        //      var first_name=$('input[name="first_name"]').val();
        //      var last_name=$('input[name="last_name"]').val();
        //      var email=$('input[name="email"]').val();
        //      var designation=$('input[name="designation"]').val();
        //      var about=$('textarea').val();
        //      var image=$('input[name="image"]').val();
        //      var phone=$('input[name="phone"]').val();
        //      var address=$('input[name="address"]').val();
             
             
        //                       if(first_name.length <31){
                                
        //                           first_name1=first_name;
        //                       }else{
        //                           $('.first_name').text('First Name max 30 Char');
        //                             // alert('*First Name max 30 Char');
        //                       }
                              
                               
                              
        //                       if(last_name.length <31){
                                 
        //                           last_name1=last_name;
        //                       }else{
        //                            $('.last_name').text('Last Name max 30 Char');
        //                            //alert('*Last Name max 30 Char');
        //                       }
                              
        //                       if(about.length <150){
                                 
        //                           about1=about;
        //                       }else{
        //                            // alert('About max 300 Char');
        //                              $('.about').text('*About max 150 Char');
        //                       }
                              
        //                     if(address.length <100){
                                  
        //                           address1=address;
        //                       }else{
        //                          // alert('About max 255 Char');
        //                           $('.address').text('*Address max 100 Char');
        //                       }
                              
        //                       if(designation.length <30){
                                  
        //                           designation1=designation;
        //                       }else{
        //                           //alert('Designation max 255 Char');
        //                           $('.designation').text('* Designation max 30 Char');
        //                       }
        //                         if(email!==''){
        //                       if(isEmail(email)==true){
                                 
        //                           email1=email;
        //                            $('.email').css('color','green');
        //                           $(".email").text('*Email Valid..!');
        //                       }else{
        //                            //alert('Is not E-mail');
        //                             $('.email').css('color','red');
        //                             $('.email').text('*Is not E-mail');
        //                       }
        //                         }
        //                       if(phone!==''){
        //                       if(validatePhone(phone)==true){
                                 
        //                           phone1=phone;
        //                           $('.phone').css('color','green');
        //                           $(".phone").text('*Phone Valid..!');
        //                       }else{
        //                            //alert('Is not Valid..!');
        //                              $('.phone').css('color','red');
        //                            $('.phone').text('*Is not Valid Phone No... only no!');
        //                       }
        //                       }
                             
        //      if(first_name1!=='' && last_name1!=='' && email1!=='' && designation1!==''  && about1!=='' && address1!=='' && phone1!==''){
        //          // alert(first_name+" "+last_name+" "+email+" "+designation+"  "+about+" "+phone+" "+address+" "+website);
        //          $('.sf-controls').css("display","block");
        //      }else{
        //           $('.sf-controls').css("display","none");
        //      }
            
        // });
        
        
        function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
          if (regex.test(email)) {
                 return true;
               }
            else {
                  return false;
             }
        }
        
        
     function validatePhone(Phone) {
    
    var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (filter.test(Phone)) {
        return true;
    }
    else {
        return false;
    }
}
                    
});


$(document).on("focusout","#wizard_example_2 input,select,textarea,password",function(){
  // var formdata =$('#wizard_example_2').serializeArray();
  // formdata.splice(0, 2);
 var formdata = new FormData($('#wizard_example_2')[0])
  $.ajax({
    url: "{{route('TempCardDetails')}}",
    method:'post',
    data: formdata,
    processData: false,
    contentType: false,
    success: function(result){
      
    }
  });
});

// $(document).on("focusout",".DescriptionHeading",function(){
//   var no = $(this).data('no');
//   $('.descriptiontext'+no).attr('name','description['+$(this).val()+']');

// });

// $( document ).ready(function() {
//    $.ajax({
//     url: "{{route('getTempForm')}}",
//     method:'get',
//     data:{
//       card_id:$('#card_id').val()
//     },
//     success: function(result){
//       if(result.status){
//         var obj = JSON.parse(result.data.card_details);
//         console.log(obj);
//         obj.forEach(function( item,key ) {
//           $("input:text[name='"+item.name+"']").val(item.value);
//           //$("input:select[name='"+item.name+"']").val(item.value);
//           $("input:[name='"+item.name+"']").val(item.value);
//         });
//       }
//     }
//   });
// });


$('.category').change(function() {
    var cate = $(this).val();
      $.ajax({
        url:"{{route('getSubcategory')}}",
        method:'get',
        data:{
          category_id:cate
        },
        success: function(res){
          var html = "";
          $.each(res, function (key, value) {
            html += "<option value="+key+">"+value+"</option>";
          });
          $(".subcategory").html(html);
        }
      });
  });

$( document ).ready(function() {
    var cate = $('.category').val();
    var subcate = $('#subcategoryid').val();
    $.ajax({
        url:"{{route('getSubcategory')}}",
        method:'get',
        data:{
          category_id:cate
        },
        success: function(res){
          var html = "";
          $.each(res, function (key, value) {
            var sect ='';
            if(subcate == key){
              sect='selected';
            }
            html += "<option value="+key+" "+sect+">"+value+"</option>";
          });
          $(".subcategory").html(html);
        }
      });
  });

$(document).ready(function(){
      $( ".sf-controls.clearfix" ).css('display','block');
       $('.btn-add').click(function(e){
           e.preventDefault();
           var max_fields_limit =$('#max_no_socialmedia').val();
           var sociallink2 = $('.socialmedia .sociallink').length;
           if(sociallink2 < max_fields_limit){
               $('.socialmedia').append('<div class="row sociallink"><div class="col-md-4"><select class="form-control selectsocialMedia"><option value="">Select</option><option value="Facebook">fa-facebook</option><option value="Skype">fa-skype</option><option value="WhatsApp">fa-whatsapp</option><option value="LinkedIn">fa-linkedin</option><option value="Twitter">fa-twitter</option></select></div><div class="col-md-4"><input type="text" name="social_media[]" class="form-control socialMedialink" placeholder="" value=""></div><span><button class="btn btn-danger remove_field" type="button"><i class="fa fa-minus-square-o" aria-hidden="true"></i></button></span></div>');
           }
       });  
       $('.socialmedia').on("click",".remove_field", function(e){
           e.preventDefault(); $(this).parents('.sociallink').remove(); x--;
       });
});

$(document).on('change', '.selectsocialMedia', function (e) {
  var media = $(this).val();
  $(this).parents('.sociallink').find('.col-md-4 .socialMedialink').attr('name','social_media['+media+']');
  $(this).parents('.sociallink').find('.col-md-4 .socialMedialink').attr('placeholder',media);
  
});

</script>
    
   
    
    
@endsection