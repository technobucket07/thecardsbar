@extends('frontend.layouts.master')
@section('content')
<link href="https://chart.googleapis.com/chart?">
<section class="purchased_pro">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 col-12">
				<div class="product_img_part">
					<a target="_blank" href="{{route('GetQRcode',[endeCrypt(auth()->id()),endeCrypt($order->id)])}}"><img class="img-fluid" src="{{ url('storage/'. ($order->cardimage ? $order->cardimage : ($order->themeimage ? $order->themeimage :($order->picture ? $order->picture:''))))}}" alt=""></a>
				</div>
{{--				<a href="{{route('GetQRcode',[endeCrypt(auth()->id()),endeCrypt($order->id)])}}">info</a>--}}
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-12">
				<div class="product_content">
					<div class="card_div card_div_one">
						<p class="sec_title">Card Info</p>
						<p class="pro_title">
							<!-- Card Name with Theme Name -->
							{{$order->cardtitle}}
						</p>
						<p class="sm_discr">
						<!-- Sdiscription about themes and card for various services for various servicesand card for various services -->
						{{$order->carddescription}}
						</p>
					</div>
					<div class="card_div_two" data-test="{{$order->cardid}}" data-ll="{{route('DownloadCardWithQR',[endeCrypt($order->cardid)])}}">
						<!-- <a class="qr_page" href="{{url('storage/'.$order->cardqr)}}"><img src="{{url('storage/'.$order->cardqr)}}" alt=""></a> -->
						<img src="data:image/png;base64, {{base64_encode(QrCode::format('png')->merge(url('/').'/web_assets/img/new_logo.png', 0.5, true)->size(200)->errorCorrection('H')->generate(route('DownloadCardWithQR',[endeCrypt($order->cardid)])))}}">
					{{--	{!! QrCode::size(150)->generate(route('DownloadCardWithQR',[endeCrypt($order->cardid)])); !!}
					--}}
					</div>
					<div class="card_div">
						<p class="sec_title">Purchase Details</p>
						<p class="price"><span class="currency" id="currency">&#8377;</span> {{$order->price}}</p>
						<p class="stat">Purchased on: <span class="stat_data">{{date($order->purchased_at)}}</p>
						<!-- <p class="stat">Payment Method: <span class="stat_data">Visa Card 2342 XXXX XXXX</p> -->
						<p class="stat">Payment ID: <span class="stat_data">
							<!-- TRANS488451b78 --> 
							{{$order->payment_id}}
						</p>
					</div>
					<div class="card_div">
						<p class="sec_title">Download</p>
						<div class="download">
							<a href="{{route('DownloadCard',[endeCrypt($order->cardid),'pdf'])}}" class="pdf"><span class="icon"><img class="img-fluid" src="http://thecardsbar.com/web_assets/img/download.png"></span>PDF</a>
							<a href="{{route('DownloadCard',[endeCrypt($order->cardid),'vcf'])}}" class="vcf"><span class="icon"><img class="img-fluid" src="http://thecardsbar.com/web_assets/img/download.png"></span>VCF</a>
						</div>
						
					</div>
					<div class="card_div">
						<p class="sec_title">Share</p>
						<div class="share_icons">
							<!-- <a href="#" class="wp"><i class="fa fa-whatsapp" aria-hidden="true"></i> Whatsapp</a> -->
							<a href="#" class="pdf share" data-toggle="modal" data-target="#exampleModal" data-file="{{route('DownloadCard',[endeCrypt($order->cardid),'pdf'])}}"><i class="fa fa-file-pdf-o" aria-hidden="true"></i> PDF</a>
							<a href="#" class="vcf share" data-toggle="modal" data-target="#exampleModal" data-file="{{route('DownloadCard',[endeCrypt($order->cardid),'vcf'])}}"><i class="fa fa-address-card-o" aria-hidden="true"></i> V Card</a>
							<!-- <a href="#" class="copy"><i class="fa fa-clone" aria-hidden="true"></i> Copy URL link</a> -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content share_popup col-12">
            <div class="modal-header">
                <h5 class="modal-title">Share</h5> <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">&times;</span> </button>
            </div>
            <div class="modal-body">
                <div class="icon-container1 d-flex">
                    <div class="smd"> 
						<a class="whatsapp" href="#" target=_blank>
							<i class="img-thumbnail fa fa-whatsapp" style="color: #25D366;background-color: #cef5dc;"></i>
							<span>Whatsapp</span>
						</a>
                    </div>
                </div>
            </div>
            <div class="bottom"> 
            	<label>
				Copy Link
            	
				</label>
                <div class="link_copy"> 
					<input class="ur" type="url" placeholder="link" id="myInput"> 
					<button class="cpy" onclick="myFunction()"><i class="fa fa-clone"></i></button> 
					<span class="message"></span>
				</div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('css')
@endsection
@section('scripts')
<script type="text/javascript">
	function myFunction() {
		var textBox = document.getElementById("myInput");
    	textBox.select();
    	document.execCommand("copy");
		$(".message").text("link copied");
	}
	$('.share').on('click',function(){
		var what = 'https://api.whatsapp.com/send?text=';
		$('#myInput').val($(this).data('file'));
		$('.whatsapp').attr('href',what+$(this).data('file'));
	});
</script>
@endsection