@extends('frontend.layouts.master')
@section('content')

        <!-- SLIDER STARTS HERE -->
<section class="sliderOwl customSlide">
    <div id="owl-demo" class="owl-carousel owl-theme">	
		<div class="item header_banners first">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-sm-6">
						<div class="banner_context">
							<p class="banner_title">Grow with trend<br>Create Your Own Digital Card</p>
							<p class="banner_bio">Go with flow by using trendy cards to grow your business. Get custome made digital business cards for your business.</p>
							<a href="javascript:void(0)" class="red_btn">View Cards</a>
						</div>
					</div>
					<div class="col-lg-6 col-sm-6">
						<div class="img_part">
							<img src="{{ url('web_assets')}}/img/banner_vector_1.png" class="img-fluid">
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="item header_banners second">
			<div class="container">
				<div class="row">
					<div class="col-lg-6 col-sm-6">
						<div class="banner_context">
							<p class="banner_title">Custom<br><span class="red">qr code cards</span></p>
							<p class="banner_bio">Go with flow by using trendy cards to grow your business. Get custome made digital business cards for your business.</p>
							<a href="javascript:void(0)" class="red_btn">View Cards</a>
						</div>
					</div>
					<div class="col-lg-6 col-sm-6">
						<div class="img_part">
							<img src="{{ url('web_assets')}}/img/banner_vector_2.png" class="img-fluid">
						</div>
					</div>
				</div>
			</div>
		</div>
        <div class="item">
            <div class="innerBannerCont">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="gpsTop">Discover the world’s top</p>
                            <p class="gpshead">Beautiful & Creatives Digital Cards</p>
                            <p class="gpsCont">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            <p class="contBtn">
                                <a href="#" class="btnCustomStyle2 btn-solid">Contact us</a>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <img src="{{ url('web_assets')}}/img/banner-social.png" class="mx-auto d-block">
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="item">

            <div class="innerBannerCont">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="gpsTop">Discover the world’s top</p>
                            <p class="gpshead">Beautiful & Creatives Digital Cards</p>
                            <p class="gpsCont">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</p>
                            <p class="contBtn">
                                <a href="#" class="btnCustomStyle2 btn-solid">Contact us</a>
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <img src="{{ url('web_assets')}}/img/banner-social.png" class="mx-auto d-block">
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>


    </div>
</section>
<!-- SLIDER END HERE -->

<!-- COUNTERS STARTS HERE -->
<section class="mainCounters">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="flash_on_boxx stats_site">
                    <h3 class="text-center">Join our community</h3>
                    <div class="row text-center">
                        <div class="col-md-4 col-6">
                            <div class="counter">
                                <!--<img class="d-block mx-auto" src="{{ url('web_assets')}}/img/counter-1.png" alt="">-->
								<img class="d-block mx-auto" src="{{ url('web_assets')}}/img/ttl_customer.png" alt="">
								<div class="stats_data">
									<h2 class="timer count-title count-number" data-to="{{customerCount()}}" data-speed="3000"></h2>
									<p class="count-text ">CUSTOMERS</p>
								</div>
                            </div>
                        </div>
                        <div class="col-md-4 col-6">
                            <div class="counter">
                                <!--<img class="d-block mx-auto" src="{{ url('web_assets')}}/img/counter-2.png" alt="">-->
								<img class="d-block mx-auto" src="{{ url('web_assets')}}/img/card_view.png" alt="">
								<div class="stats_data">
									<h2 class="timer count-title count-number" data-to="{{rand(1,1000)}}" data-speed="3000"></h2>
									<p class="count-text ">CARD VIEWS</p>
								</div>
                            </div>
                        </div>
                        <div class="col-md-4 col-12">
                            <div class="counter">
                                <!--<img class="d-block mx-auto" src="{{ url('web_assets')}}/img/counter-3.png" alt="">-->
								<img class="d-block mx-auto" src="{{ url('web_assets')}}/img/tree.png" alt="">
								<div class="stats_data">
									<h2 class="count-title count-number">Millions</h2>
									<p class="count-text ">OF TREES TO BE SAVED</p>
								</div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<!-- COUNTERS END HERE -->
	
<!-- new cat -->
<section class="PopularCategories">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading text-center">Popular Category</h2>
                <p class="discription text-center">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration</p>
            </div>
        </div>

        <!-- <div class="row">
             <div class="col-md-12">
                <div class="tab-slider--nav flexible_tabs">
                    <ul class="tab-slider--tabs">
                        <li class="tab-slider--trigger active" rel="tab1">BUSINESS</li>
                        <li class="tab-slider--trigger" rel="tab1">STUDENT</li>
                        <li class="tab-slider--trigger" rel="tab1">MUCH MORE</li>
                    </ul>
                </div>
            </div> 
		</div> -->
		<div class="row">
            <div class="col-md-12">
                <div class="tab-slider--container">
                    <div id="tab1" class="tab-slider--body">
                        <div class="row row--5 slidelayout_02">
                            @foreach($cards as $card)
							<div class="col-lg-4 col-md-6 col-12">
								<div class="texture_bg">
                                <div class="portfolio-style--3">
                                    <div class="topLikeandview">
                                        <ul class="list-unstyled">
                                            @php
                                            //dd($card->is_like);
                                            if($card->is_liked){
                                                $like ='like';
                                            }else{
                                                $like ='dislike';
                                            }
                                            @endphp
                                            <li>
                                                <a href="javascript:void(0);">
												<div class="heartbutton {{$like}}" data-type="like" data-templateid="{{$card->id}}" data-post_id="102" data-post_animation="bloom">
                                                    @if($card->is_liked)
                                                    <img src="{{ url('web_assets')}}/img/heart.png" class="heart-icon-outline heartsize heart-icon-outline heartsize liked">
                                                    <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-filled heartsize stop-animation-bloom">
                                                    <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-animation heartsize start-animation-bloom">
                                                    @else
                                                    <img src="{{ url('web_assets')}}/img/heart.png" class="heart-icon-outline heartsize">
                                                    <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-filled heartsize">
                                                    <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-animation heartsize">
                                                    @endif
													
												</div>
												<span class="numberlikes">{{$card->total_likes}}</span>
												</a>
                                            </li>
                                            <li>
                                                <a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                                <span>{{$card->total_views}}</span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="thumbnail">
                                        <!-- <a href="#" data-fancybox="gallery"></a> -->
                                        <a href="{{route('dynamicCardSteps',$card->id)}}">
											<img src="{{ url($card->picture)}}" class="img-fluid">
                                        </a>
                                    </div>
                                    <div class="content">
                                        <p class="portfoliotype"><span class="buy_icon"><i class="fa fa-shopping-bag" aria-hidden="true"></i></span> <span class="buy_amt">&#8377;{{$card->price}}</span></p>
                                        <h4 class="title">
                                            <a href="#">{{$card->cardtitle}}</a>
                                        </h4>
                                        <div class="portfolio-btn clearfix">
											<a class="rn-btn text-white pull-right price_amt" href="#">&#8377;{{$card->price}}</a>
                                            <a class="rn-btn text-white pull-left" href="{{route('dynamicCardSteps',$card->id)}}">Order</a>
                                        </div>
                                    </div>
                                </div>
								</div>
							</div>
                            @endforeach
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="view-more-btn text-center">
                                    <!-- <a class="btnCustomStyle2 btn-solid" href="#">
                                        <span>View More Cards</span>
                                    </a> -->
                                    <a class="btnCustomStyle2 btn-solid" href="{{route('ecards')}}">
                                        <span>View All</span>
                                    </a>
                                   {{--

                                    @if($cards->currentPage() > 1)
                                    <a class="btnCustomStyle2 btn-solid" href="{{ $cards->previousPageUrl()}}">
                                        <span>Previous</span>
                                    </a>
                                    @endif
                                    <a class="btnCustomStyle2 btn-solid" href="{{ $cards->nextPageUrl()}}">
                                        <span>Next</span>
                                    </a>

                                    --}}
                                    {{-- {!! $cards->links() !!}  --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
<!-- new cat -->

<!-- POPULAR STARTS HERE -->
<section class="popular">
    <div class="container">
		<div class="row">
			<div class="col-lg-12">
				<div class="d-sm-flex align-items-sm-center populer_owl">
                    <h2>Popular</h2>
                    <p class="mt-sm-0 ml-sm-4 border-sm-left pl-sm-3">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore...</p>
                </div>
			</div>
		</div>
        <div class="row">            
            <div class="col-lg-9">             
                <div class="innerMillion">
                    <div id="news-slider2" class="owl-carousel">
						<div class="slide_one">
							<a href="#"><img src="{{ url('web_assets')}}/img/owlbanners/owl_banner4.jpg" alt="" class="img-fluid"></a>
						</div>
						<div class="slide_one">
							<a href="#"><img src="{{ url('web_assets')}}/img/owlbanners/owl_banner2.jpg" alt="" class="img-fluid"></a>
						</div>
						<div class="slide_one">
							<a href="#"><img src="{{ url('web_assets')}}/img/owlbanners/owl_banner5.jpg" alt="" class="img-fluid"></a>
						</div>
					</div>
				</div>
            </div>
			<div class="col-lg-3 populer_slider_home">
				<p class="owl_heading">Make<br>Your Own <span class="red">Card</span>
				<hr class="sm_divider">
				<p class="subtext">This <span class="red">card</span> can be yours or you can <span class="red">make a new</span> one for yourself.
			<!--
                <p class="sideImg">
                    <img src="{{ url('web_assets')}}/img/popularsidepic.png" class="img-fluid" alt="">
                </p>
				-->
            </div>
        </div>
    </div>
</section>
<!-- POPULAR END HERE -->

<!-- STEPS STARTS HERE -->
<section class="StepsBox">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2 class="heading text-center">Steps</h2>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-5 col-sm-6">
                <img src="{{ url('web_assets')}}/img/about-bg-1.png" alt="" class="img-fluid mx-auto">
            </div>
            <div class="col-lg-6 offset-lg-1 col-sm-6">
                <div class="about-text">
                    <ul class="list-unstyled clearfix">
                        <li>
                            <span>01</span>
                        </li>
                        <li>
                            <h2>Login/Sign</h2>
                            <p>Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="height-70"></div>

        <div class="row">
            <div class="col-lg-6 order-12 order-sm-1 col-sm-6">
                <div class="about-text">
                    <ul class="list-unstyled clearfix">
                        <li>
                            <span>02</span>
                        </li>
                        <li>
                            <h2>Choose you category</h2>
                            <p>Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna.</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-5 offset-lg-1 order-1 order-sm-12 col-sm-6">
                <img src="{{ url('web_assets')}}/img/about-bg-2.png" alt="" class="img-fluid mx-auto">
            </div>
        </div>

        <div class="height-70"></div>

        <div class="row">
            <div class="col-lg-5 col-sm-6">
                <img src="{{ url('web_assets')}}/img/about-bg-3.png" alt="" class="img-fluid mx-auto">
            </div>
            <div class="col-lg-6 offset-lg-1 col-sm-6">
                <div class="about-text">
                    <ul class="list-unstyled clearfix">
                        <li>
                            <span>04</span>
                        </li>
                        <li>
                            <h2>Fill in Color,theam,pics</h2>
                            <p>Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna.</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="height-70"></div>

        <div class="row">
            <div class="col-lg-6 order-12 order-sm-1 col-sm-6">
                <div class="about-text">
                    <ul class="list-unstyled clearfix">
                        <li>
                            <span>04</span>
                        </li>
                        <li>
                            <h2>Card Ready/Download Card</h2>
                            <p>Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna.</p>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-5 offset-lg-1 order-1 order-sm-12 col-sm-6">
                <img src="{{ url('web_assets')}}/img/about-bg-4.png" alt="" class="img-fluid mx-auto">
            </div>
        </div>
    </div>
</section>
<!-- STEPS END HERE -->


<!-- NEW FEATURE STARTS HERE -->
<section class="newFeature">
    <!-- CONTAINER -->
    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <h2 class="heading text-center">New Features View</h2>
                <p class="discription text-center">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration</p>
            </div>
        </div>


        <!-- ROW -->
        <div class="row">
            <!-- FEATURES LEFT -->
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 features-left">
                <!-- FEATURES ICON -->
                <div class="features">
                    <!-- ICON -->
                    <div class="icon-container">
                        <div class="icon">
                            <img class="img-fluid" src="http://thecardsbar.com/web_assets/img/ph.png" alt="">
                        </div>
                    </div>
                    <!-- HANDING AND DETAILS -->
                    <div class="fetaure-details">
                        <h3>Click, Call and Business! </h3>
                        <p>Your recipients can use CLICK TO CALL feature to make a call. Isn't it a business opportunity when your customers can easily reach you? </p>
                        <!-- <h3>No need to carry business cards ! </h3>
                        <p>Share your digital business card using our vCard Sharing App. Easily send your contact information via SMS, Email or Whatsapp (Without saving their number).</p> -->
                    </div>
                </div>
                <!-- /END FEATURES ICON -->

                <!-- FEATURES ICON -->
                <div class="features">
                    <!-- ICON -->
                    <div class="icon-container">
                        <div class="icon">
                            <img class="img-fluid" src="http://thecardsbar.com/web_assets/img/wp_wh.png" alt="">
                        </div>
                    </div>
                    <!-- HANDING AND DETAILS -->
                    <div class="fetaure-details">
                        <h3>They can Whatsapp you. Without saving your number! </h3>
                        <p>Yup! Recipients can tap "Click to Whatsapp" feature on your Digital vCard and they can intiate a chat with you. Convert more leads into customers by answering their queries about your products and services by a quick Whatsapp Chat. It builds trust! </p>
                    </div>
                </div>
                <!-- /END FEATURES ICON -->

                <!-- FEATURES ICON -->
                <div class="features">
                    <!-- ICON -->
                    <div class="icon-container">
                        <div class="icon">
                            <img class="img-fluid" src="http://thecardsbar.com/web_assets/img/location.png" alt="">
                        </div>
                    </div>
                    <!-- HANDING AND DETAILS -->
                    <div class="fetaure-details">
                        <h3>Navigation to your Store or Physical Location.</h3>
                        <p>
                            If you are running a Retail Store or have an actual office, where youw ant your prospects to physically visit your site, then our Digital Business vCard provide CLICK TO NAVIGATE feature. 
                            People can get turn by turn navigation to your business location using Google Map. This will ensure that your prospects are not distracted with other sites, stores or location.
                        </p>
                        <!-- <h3>Lead Capture Form.</h3>
                        <p>If you are into businesses like Education, Real Estate, Marketing Agency, Tours and Travel, Finance and Insurance or any other B2B segment, where you want people to fill up enquiry form, then our Digital Business Card can help you capture leads with contact form. You will get email notification for each lead submission.</p> -->
                    </div>
                </div>
                <!-- /END FEATURES ICON -->
            </div>
            <!-- /END FEATURES LEFT -->

            <!-- FEATURES CENTER IMAGE -->
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 rwd-device">
                <!-- IMAGE -->
                <div class="feature-phone-image">
                    <img class="img-fluid mx-auto d-block" src="{{ url('web_assets')}}/img/features.png" alt="">
                </div>
            </div>
            <!-- /END FEATURES CENTER IMAGE -->

            <!-- FEATURES RIGHT -->
            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 features-right">
                <!-- FEATURES ICON -->
                <div class="features">
                    <!-- ICON -->
                    <div class="icon-container">
                        <div class="icon">
                            <img class="img-fluid" src="http://thecardsbar.com/web_assets/img/id.png" alt="">
                        </div>
                    </div>
                    <!-- HANDING AND DETAILS -->
                    <div class="fetaure-details">
                        <h3>Send vCard by Whatsapp and Email.  </h3>
                        <p>When you meet people, just ask for their name and phone number and you can send your digital business card on whatsapp, without even saving their number. Similarly, you can also ask for name and email, and you can send your Digital vCard to your prospects with just couple of clicks. </p>
                    </div>
                </div>
                <!-- /END FEATURES ICON -->

                <!-- FEATURES ICON -->
                <div class="features">
                    <!-- ICON -->
                    <div class="icon-container">
                        <div class="icon">
                            <img class="img-fluid" src="http://thecardsbar.com/web_assets/img/qr_code.png" alt="">
                        </div>
                    </div>
                    <!-- HANDING AND DETAILS -->
                    <div class="fetaure-details">
                        <h3>Business Card QR Code.</h3>
                        <p>Whenever you meet people in person, you can always ask them to scan your digital business card QR code, so that they can instantly access and store your contact information in their mobile device.</p>
                    </div>
                </div>
                <!-- /END FEATURES ICON -->

                <!-- FEATURES ICON -->
                <div class="features">
                    <!-- ICON -->
                    <div class="icon-container">
                        <div class="icon">
                            <img class="img-fluid" src="http://thecardsbar.com/web_assets/img/touch.png" alt="">
                        </div>
                    </div>
                    <!-- HANDING AND DETAILS -->
                    <div class="fetaure-details">
                        <h3>Click to Website</h3>
                        <p>57% of the customers who contact vendors have already made up their mind to buy a product. Customers these days are smart and gather information from all over the internet. And hence we have Click To Website feature on our digtial business vcard, so that your prospects can visit your website to gather more information about your products. </p>
                    </div>
                </div>
                <!-- /END FEATURES ICON -->
            </div>
        </div>
        <!-- /END ROW -->
    </div>
    <!-- /END CONTAINER -->
</section>
<!-- NEW FEATURE ENDS HERE -->


@endsection

@section('scripts')
    <script>
        $(document).ready(function() {

            $("#owl-demo").owlCarousel({

                navigation : false, // Show next and prev buttons
                autoPlay : false,
                slideSpeed : 200,
                nav : false,
                paginationSpeed : 400,

                items : 1,
                itemsDesktop : false,
                itemsDesktopSmall : false,
                itemsTablet: false,
                itemsMobile : false

            });

        });
    </script>

    <script>
        $(document).ready(function() {
            $("#news-slider2").owlCarousel({
                items : 1,				
                /*itemsDesktop:[1199,2],
                itemsDesktopSmall:[980,2],
                itemsMobile : [700,1],*/
                pagination:true,
				margin:0,
				loop:true,
                navigation:false,
                navigationText:["",""],
                autoPlay:true
            });
        });
    </script>

    <script>/*
        $("document").ready(function(){
            $(".tab-slider--body").hide();
            $(".tab-slider--body:first").show();
        });*/

        $(".tab-slider--nav li").click(function() {
            $(".tab-slider--body").hide();
            var activeTab = $(this).attr("rel");
            $("#"+activeTab).fadeIn();
            $('.tab-slider--tabs').removeClass('slide');
            if($(this).attr("rel") == "tab2"){
                $('.tab-slider--tabs').addClass('slide');
            }else if($(this).attr("rel") == "tab3"){
                $('.tab-slider--tabs').addClass('slide');
            }else if($(this).attr("rel") == "tab4"){
                $('.tab-slider--tabs').addClass('slide');
            }

            $(".tab-slider--nav li").removeClass("active");
            $(this).addClass("active");
        });
    $(document).on('click', '.heartbutton', function(e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
              'X-CSRF-Token': "{{csrf_token()}}"
            }
        });
        var totallike = parseInt($(this).next('.numberlikes').text());
        var like=0;
        if($(this).hasClass('dislike')){
            $(this).removeClass('dislike');
            like=1;
            totallike++;
        }else{
            $(this).addClass('dislike')
            like=0;
            totallike--;
        }
        $(this).next('.numberlikes').text(totallike);
        $.ajax({
            type: "POST",
            url: "{{route('CardReviews')}}",
            data:{
                'action':$(this).data('type'),
                'templateid':$(this).data('templateid'),
                'like':like,
            },
            success:function(data) {
            }
        });
    });
    </script>
	<!-------heart icon animation------------>
	<script>
		$(document).ready(function () {
  $(".heartbutton").click(function() {
    var currentAnimation = $(this).data("post_animation");
        $(this).find(".heart-icon-outline").toggleClass("liked");
        $(this).find(".heart-icon-animation").toggleClass("start-animation-"+currentAnimation);
        $(this).find(".heart-icon-filled").toggleClass("stop-animation-"+currentAnimation);
    });
});
	</script>
@endsection