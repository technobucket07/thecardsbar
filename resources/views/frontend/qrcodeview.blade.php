@extends('frontend.layouts.master')
@section('content')
<section>
	<div class="outer_card">
		<div class="img_part">

			@if(!empty($card->serviceManProfile))
				<a href="#"><img class="img-fluid" src="{{url('storage/'.$card->serviceManProfile)}}"></a>
			@else
				<a href="#"><img src="{{ url('web_assets')}}/onlyLogo.png" class="img-fluid"></a>
			@endif
{{--			<a href="#"><img class="img-fluid" src="{{url($card->serviceManProfile?$card->serviceManProfile:'web_assets/img/cardsbar.jpg')}}"></a>--}}
		</div>
		<div class="discriptions">
			<p class="title">{{$card->serviceMan}}</p>
			<p class="context">{{$card->categoryName}}</p>
				<img src="data:image/png;base64, {{base64_encode(QrCode::format('png')->merge(url('/').'/web_assets/img/black_logo.png', 0.5, true)->size(200)->errorCorrection('H')->generate(route('DownloadCardWithQR',[endeCrypt($card->id)])))}}">
				<!--img class="img-fluid" src="http://thecardsbar.com/web_assets/img/black_download.png" alt=""-->
		
			<div class="download_btn">
				<a href="data:image/png;base64, {{base64_encode(QrCode::format('png')->merge(url('/').'/web_assets/img/black_logo.png', 0.5, true)->size(200)->errorCorrection('H')->generate(route('DownloadCardWithQR',[endeCrypt($card->id)])))}}" download>
					Download QR
					<img style="display: none;" src="data:image/png;base64, {{base64_encode(QrCode::format('png')->merge(url('/').'/web_assets/img/black_logo.png', 0.5, true)->size(200)->errorCorrection('H')->generate(route('DownloadCardWithQR',[endeCrypt($card->id)])))}}" alt="W3Schools" width="104" height="142">
				</a>
{{--				<a href="javascript:void(0);">Download QR</a>--}}
			</div>
			<div class="QR_content">
				<span>share QR</span>   
			</div>   
			{!!
				Share::page(url('storage/'.$card->cardqr), 'Please Scan This QR Code')
				->facebook()
				->twitter()
				->linkedin()
				->whatsapp()
				!!}
				<div class="QR_content">
					<span>save contact</span> 
				</div>
				<div class="save_btn">
					<a href="{{route('DownloadCard',[endeCrypt($card->cardid),'vcf'])}}"><img src="http://thecardsbar.com/web_assets/img/id-card.svg">save</a>
				</div>
				
		</div>
		<div class="foot_interact">
			<!--a href="#"><span class="icon"><img class="img-fluid" src="http://thecardsbar.com/web_assets/img/share.png" alt=""></span> Share my code</a>
			<a href="{{url('storage/'.$card->cardqr)}}" target="_blank"><span class="icon"><img class="img-fluid" src="http://thecardsbar.com/web_assets/img/download.png" alt=""></span> Save to gallery</a-->
		</div>
	</div>
<section>
@endsection
@section('css')
<style>
@media (max-width:450px) {
footer{display:none;}  
}
</style>
@endsection
@section('scripts')
<script type="text/javascript">
    $('.footerBox').css('display','none');   
</script>
@endsection