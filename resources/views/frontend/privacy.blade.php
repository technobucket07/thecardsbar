@extends('frontend.layouts.master')
@section('content')
<!-- BREADCRUMBS STARTS HERE -->
<section class="mainbreadCrumbs">
    <div class="container">
       <div class="row">
          <div class="col-lg-12">
             <div class="breadcrumbInner">
                <h2 class="title">Privacy Policy</h2>
                <ul class="page-list list-unstyled">
                   <li class="breadcrumbItem"><a href="index.html">Home</a></li>
                   <li class="breadcrumbItem active">Privacy Policy</li>
                </ul>
             </div>
          </div>
       </div>
    </div>
</section>
<!-- BREADCRUMBS END HERE -->
<!-- PRIVACY POLICY STARTS HERE -->
<section class="terms_condition">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-12">
                <div class="terms_condition-content">
                    <div class="terms_condition-text">
                        {!!$content[str_slug('Your privacy is important to us')]!!}
                    </div>

{{--                    <div class="terms_condition-text">--}}
{{--                        {!!$content[str_slug('Third-party services')]!!}--}}
{{--                    </div>--}}

{{--                    <div class="terms_condition-text">--}}
{{--                        {!!$content[str_slug('Google Analytics')]!!}--}}
{{--                    </div>--}}

{{--                    <div class="terms_condition-text">--}}
{{--                        {!!$content[str_slug('Information we collect')]!!}--}}
{{--                    </div>--}}
                </div>
            </div>
        </div>
    </div>
</section>
<!-- PRIVACY POLICY END HERE -->
@endsection
@section('scripts')
@endsection