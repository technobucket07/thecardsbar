@extends('frontend.layouts.master')
@section('content')
<!-- BREADCRUMBS STARTS HERE -->
<section class="mainbreadCrumbs">
    <div class="container">
       <div class="row">
          <div class="col-lg-12">
             <div class="breadcrumbInner">
                <h2 class="title">All Cards</h2>
                <ul class="page-list list-unstyled">
                   <li class="breadcrumbItem"><a href="{{route('home')}}">Home</a></li>
                   <li class="breadcrumbItem active">All cards</li>
                </ul>
             </div>
          </div>
       </div>
    </div>
</section>
<!-- BREADCRUMBS END HERE -->

<!-- ABOUT INTRO STARTS HERE -->
<section class="intro-section section-padding">
  <div class="container">
    <div class="row">
		 <div class="col-md-12">
			<div class="action_tabs">
				<!-- <button class="btn-custom btn btn-primary  collapsed" type="button" data-toggle="collapse" data-target="#content" aria-expanded="false" aria-controls="content">
					<span class="filter-text">Sort &amp; Filter</span>
				</button> -->
				
				<!-- <div class="content collapse bg-white shadow-lg py-4" id="content">
					<div class="interact_sec">
						<p class="title">Sort by:</p>
						<ul class="filter">
							<li><span class="name">Category</span><span class="action_on"><input type="checkbox"></span></li>
							<li><span class="name">Price</span><span class="action_on"><input type="checkbox"></span></li>
							<li><span class="name">Likes</span><span class="action_on"><input type="checkbox"></span></li>
							<li><span class="name">Latest</span><span class="action_on"><input type="checkbox"></span></li>
						</ul>
					</div>
					<div class="interact_sec">
						<p class="title">Filter by:</p>
						<ul class="sorting">
							<li> 
								<p class="sub_title">Price</p>
								<ul>
									<li><span class="name">Low-High</span><span class="action_on"><input type="checkbox"></span></li>
									<li><span class="name">High-Low</span><span class="action_on"><input type="checkbox"></span></li>
									<li><span class="name">Below 500</span><span class="action_on"><input type="checkbox"></span></li>
									<li><span class="name">Below 1000</span><span class="action_on"><input type="checkbox"></span></li>
								</ul>
							</li>
							<li> 
								<p class="sub_title">Popularity</p>
								<ul>
									<li><span class="name">Most Liked</span><span class="action_on"><input type="checkbox"></span></li>
									<li><span class="name">Most Viewed</span><span class="action_on"><input type="checkbox"></span></li>
									<li><span class="name">Trending</span><span class="action_on"><input type="checkbox"></span></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="interact_sec_btns">
						<a href="#" class="apply">Apply</a>
					</div>
							
				</div> -->
			</div>
			<div class="tab-slider--nav flexible_tabs">
				<ul class="tab-slider--tabs">
					@foreach($categories as $id=>$category)
					<a href="{{route('ecards',['category'=>$id])}}">
						<li class="tab-slider--trigger @if(request()->get('category')){{ request()->get('category') == $id ? 'active' :''}} @endif" rel="tab1">{{$category}}</li>
					</a>
					@endforeach
					<!-- <li class="tab-slider--trigger active" rel="tab1">BUSINESS</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">MUCH MORE</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li> -->
				</ul>
			</div>
		</div> 
	</div>
	<div class="tab-slider--container spacing">
    	<div id="tab1" class="tab-slider--body">
            <div class="row row--5 slidelayout_02">
                @foreach($cards as $card)
				<div class="col-lg-4 col-md-6 col-12">
					<div class="texture_bg">
                    <div class="portfolio-style--3">
                        <div class="topLikeandview">
                            <ul class="list-unstyled">
                                @php
                                //dd($card->is_like);
                                if($card->is_liked){
                                    $like ='like';
                                }else{
                                    $like ='dislike';
                                }
                                @endphp
                                <li>
                                    <a href="javascript:void(0);">
									<div class="heartbutton {{$like}}" data-type="like" data-templateid="{{$card->id}}" data-post_id="102" data-post_animation="bloom">
                                        @if($card->is_liked)
                                        <img src="{{ url('web_assets')}}/img/heart.png" class=" heart-icon-outline heartsize heart-icon-outline heartsize liked">
                                        <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-filled heartsize stop-animation-bloom">
                                        <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-animation heartsize start-animation-bloom">
                                        @else
                                        <img src="{{ url('web_assets')}}/img/heart.png" class="heart-icon-outline heartsize">
                                        <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-filled heartsize">
                                        <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-animation heartsize">
                                        @endif
									</div>
									<span class="numberlikes">{{$card->total_likes}}</span>
									</a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <span>{{$card->total_views}}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="thumbnail">
                            <!-- <a href="#" data-fancybox="gallery"></a> -->
                            <a href="{{route('dynamicCardSteps',$card->id)}}">
								<img src="{{ url($card->picture)}}" class="img-fluid">
                            </a>
                        </div>
                        <div class="content">
                            <p class="portfoliotype"><span class="buy_icon"><i class="fa fa-shopping-bag" aria-hidden="true"></i></span> <span class="buy_amt">&#8377;{{$card->price}}</span></p>
                            <h4 class="title">
                                <a href="#">{{$card->cardtitle}}</a>
                            </h4>
                            <div class="portfolio-btn clearfix">
								<a class="rn-btn text-white pull-right price_amt" href="#">&#8377;{{$card->price}}</a>
                                <a class="rn-btn text-white pull-left" href="{{route('dynamicCardSteps',$card->id)}}">Order</a>
                            </div>
                        </div>
                    </div>
					</div>
				</div>
                @endforeach
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="view-more-btn text-center">

                        @if($cards->currentPage() > 1 || $cards->currentPage() == $cards->lastPage())
                            <a class="btnCustomStyle2 btn-solid" href="{{ $cards->previousPageUrl()}}">
                                <span>Previous</span>
                            </a>
                        @endif
                        @if($cards->currentPage()!= $cards->lastPage())
                            <a class="btnCustomStyle2 btn-solid" href="{{ $cards->nextPageUrl()}}">
                                <span>Next</span>
                            </a>
                        @endif

                        {{-- {!! $cards->links() !!}  --}}
                    </div>
                </div>
            </div>
		</div>
  </div>
</section>
<!-- ABOUT INTRO END HERE -->
@endsection
@section('scripts')
<script type="text/javascript">
	// $(document).ready(function(){
	//   $(".tab-slider--trigger").click(function(){
	//   	$('.tab-slider--trigger').removeClass('active');
	//   	$(this).removeClass('active');
	//   });
	// });
</script>

<script>
    $(document).on('click', '.heartbutton', function(e) {
        e.preventDefault();
        var isuser= "{{ auth()->id() }}";
        if(!isuser){
            $('#loginSignup').modal('show');
            return false;
        }
        
        $.ajaxSetup({
            headers: {
              'X-CSRF-Token': "{{csrf_token()}}"
            }
        });
        var totallike = parseInt($(this).next('.numberlikes').text());
        var like=0;
        if($(this).hasClass('dislike')){
            $(this).removeClass('dislike');
            like=1;
            totallike++;
        }else{
            $(this).addClass('dislike')
            like=0;
            totallike--;
        }
        $(this).next('.numberlikes').text(totallike);
        $.ajax({
            type: "POST",
            url: "{{route('CardReviews')}}",
            data:{
                'action':$(this).data('type'),
                'templateid':$(this).data('templateid'),
                'like':like,
            },
            success:function(data) {
            }
        });
    });
    </script>
	<!-------heart icon animation------------>
	<script>
		$(document).ready(function () {
  $(".heartbutton").click(function() {
    var currentAnimation = $(this).data("post_animation");
        $(this).find(".heart-icon-outline").toggleClass("liked");
        $(this).find(".heart-icon-animation").toggleClass("start-animation-"+currentAnimation);
        $(this).find(".heart-icon-filled").toggleClass("stop-animation-"+currentAnimation);
    });
});
	</script>
@endsection