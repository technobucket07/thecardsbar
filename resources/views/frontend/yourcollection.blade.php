@extends('frontend.layouts.master')
@section('content')
<!-- BREADCRUMBS STARTS HERE -->
<section class="mainbreadCrumbs">
    <div class="container">
       <div class="row">
          <div class="col-lg-12">
             <div class="breadcrumbInner">
                <h2 class="title">{{$page}}</h2>
                <ul class="page-list list-unstyled">
                   <li class="breadcrumbItem"><a href="{{route('home')}}">Home</a></li>
                   <li class="breadcrumbItem active">Friends Cards</li>
                </ul>
             </div>
          </div>
       </div>
    </div>
</section>
<!-- BREADCRUMBS END HERE -->

<!-- my friends STARTS HERE -->
<section class="PopularCategories">
    <div class="container">
      <!-- <div class="row">
        <div class="col-md-12">
          <h2 class="heading text-center">My Friend Collection</h2>
          <p class="discription text-center">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration</p>
        </div>
      </div>
      <div class="row">
         <div class="col-md-12">
            <div class="tab-slider--nav flexible_tabs">
                <ul class="tab-slider--tabs">
                    <li class="tab-slider--trigger active" rel="tab1">BUSINESS</li>
                    <li class="tab-slider--trigger" rel="tab1">STUDENT</li>
                    <li class="tab-slider--trigger" rel="tab1">MUCH MORE</li>
                </ul>
            </div>
        </div> 
      </div> -->
    <div class="row">
      <div class="col-md-12">
        <div class="tab-slider--container">
          <div id="tab1" class="tab-slider--body">
            <div class="row row--5 slidelayout_02 slidelayout_03">
            @if(count($cards) >0)
              @foreach($cards as $card)
              <div class="col-lg-4 col-md-6 col-12">
                <div class="texture_bg">
                  <div class="portfolio-style--3">                    
                    <div class="thumbnail">
                      <a href="" data-fancybox="gallery">
                        <img src="{{ url('storage/'.$card->cardimage)}}" class="img-fluid">
                      </a>
                    </div>
                  </div>
                  <div class="after_card_interact">
                      <ul class="list-unstyled">
                        @php
                          if(auth()->id() == $card->userisliked){
                              $like ='like';
                          }else{
                              $like ='dislike';
                          }
                        @endphp
                        <li>
                          <a href="javascript:void(0);">
                          <div class="heartbutton {{$like}}"  data-type="like" data-usercardid="{{$card->id}}" data-templateid="{{$card->card_template_id}}" data-post_id="102" data-post_animation="bloom">
                            @if(auth()->id() == $card->userisliked)
                              <img src="{{ url('web_assets')}}/img/heart.png" class="heart-icon-outline heartsize heart-icon-outline heartsize liked">
                              <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-filled heartsize stop-animation-bloom">
                              <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-animation heartsize start-animation-bloom">
                              @else
                              <img src="{{ url('web_assets')}}/img/heart.png" class="heart-icon-outline heartsize">
                              <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-filled heartsize">
                              <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-animation heartsize">
                              @endif
                            <!-- <img src="{{ url('web_assets')}}/img/heart_black.png" class="img-fluid heart-icon-outline heartsize">
                            <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-filled heartsize">
                            <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-animation heartsize"> -->
                          </div>
                          <span class="numberlikes">{{$card->likes_count}}</span>
                          </a>
                        </li>
                        <li>
                          <a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                          <span>{{$card->views_count}}</span>
                        </li>
                      </ul>
                  </div>
                  <div class="card_bio">
                    <div class="img_part">
                      <img src="{{ url($card->serviceManProfile ? 'storage/'.$card->serviceManProfile:'web_assets/img/user.png')}}" class="img-fluid">
                    </div>
                    <div class="context">
                      <p class="user_name">{{$card->serviceMan}}</p>
                      <p class="cat_name">{{$card->categoryName}}</p>
                    </div>
                  </div>                  
                </div>
              </div>
              @endforeach
            @else
            <div class="col-lg-12" style="text-align: center;">
               <span>{{$empty}}</span>
            </div>
            @endif   
            </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</section>
@endsection
@section('scripts')
<script>
    $(document).on('click', '.heartbutton', function(e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
              'X-CSRF-Token': "{{csrf_token()}}"
            }
        });
        var totallike = parseInt($(this).next('.numberlikes').text());
        var like=0;
        if($(this).hasClass('dislike')){
            $(this).removeClass('dislike');
            like=1;
            totallike++;
        }else{
            $(this).addClass('dislike')
            like=0;
            totallike--;
        }
        $(this).next('.numberlikes').text(totallike);
        $.ajax({
            type: "POST",
            url: "{{route('UserCardReviews')}}",
            data:{
                'action':$(this).data('type'),
                'usercardid':$(this).data('usercardid'),
                'templateid':$(this).data('templateid'),
                'like':like,
            },
            success:function(data) {
            }
        });
    });
    </script>
  <!-------heart icon animation------------>
  <script>
    $(document).ready(function () {
  $(".heartbutton").click(function() {
    var currentAnimation = $(this).data("post_animation");
        $(this).find(".heart-icon-outline").toggleClass("liked");
        $(this).find(".heart-icon-animation").toggleClass("start-animation-"+currentAnimation);
        $(this).find(".heart-icon-filled").toggleClass("stop-animation-"+currentAnimation);
    });
});
  </script>
@endsection