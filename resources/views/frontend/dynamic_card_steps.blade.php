@extends('frontend.layouts.master')
@section('css')
@section('content')
    <!-- BACK BUTTON STARTS HERE -->
  
 <div class="preloader"></div>
 
    <!--<section>-->
    <!--    <div class="container">-->
    <!--        <div class="row">-->
    <!--            <div class="col-md-12">-->
    <!--                <p class="backBtn">-->
    <!--                    <a class="btnCustomStyle2 btn-solid" href="index.html">-->
    <!--                        <span>Back</span>-->
    <!--                    </a>-->
    <!--                </p>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->
    <!-- BACK BUTTON END HERE -->
<!--
    <section class="upperCircles">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="progressbar list-unstyled" style="width: 100%;">
                        <li class="active" style=" width: 33%;"></li>
                        <li id="active1"  style=" width: 33%;"></li>
                        <li id="active2"  style=" width: 33%;"></li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </section>
-->
    <!-- FORM WIZARD STARTS HERE -->
    <section class="mainFormWizard">
        <div class="container-fluid">
            <span id="theme_name" style="display: none;">sky</span>
            <span id="trans_name" style="display: none;">slide</span>
            <span id="rtl" style="display: none;">0</span>
            <div class="row left_tabs_parent">
                <div class="col-md-9">
					<ul>
                    @foreach ($errors->all() as $error) 
                      <li>
                        <div class="alert alert-warning">
                       <strong>Warning!</strong> {{ $error }}
                        </div>
                      </li>
                    @endforeach
                    </ul>
                    <form id="wizard_example_2" action ="{{url('/placeorder')}}"  method="post" enctype="multipart/form-data" novalidate>
						<div class="upper_controls">							
							<a class="prev-btn" href="#"><i class="fa fa-angle-left" aria-hidden="true"></i></a>
							<a class="next-btn" href="#"><i class="fa fa-angle-right" aria-hidden="true"></i></a>
						</div>
                       <div id="loader1" style="position: relative;">
                          <!--<div id="loader2" class="center1" ></div> </br>-->
                          
                         <h2 style="text-align: center;font-size:14px;">Please Waite ...</h2>
                   </div>
                        @csrf
                        <input type="hidden" name="temp_card_id" id="temp_card_id" value="{{$card->id}}">
      						<fieldset>
                    <legend>Theme</legend>
      							<div class="preset">
                      @php
                      $themeid='';
                      if(isset($cardtempdetail['card_theme_id'])){
                          $themeid =$cardtempdetail['card_theme_id'];
                      }
                      @endphp
      								<div class="form-group form-check form-check-inline">
      									<div class="chiller_cb">
      										<input id="maintheme" class="form-check-input themeclass" name="card_theme_id" type="checkbox" value="0" @if($themeid==0) checked @endif data-image="{{url($card->picture)}}">
      										<label for="maintheme">Main</label>
      										<span></span>
      									</div>
      								</div>
                      @if(count($card->themes) > 0)
                        @foreach($card->themes as $theme)
        								<div class="form-group form-check form-check-inline">
                          <div class="chiller_cb">
                            <input id="theme{{$theme->id}}" class="form-check-input themeclass" name="card_theme_id" type="checkbox" value="{{$theme->id}}" data-image="{{url($theme->image)}}"@if($themeid==$theme->id) checked @endif>
                            <label for="theme{{$theme->id}}" >{{$theme->name}}</label>
                            <span></span>
                          </div>
                        </div>
                        @endforeach
                      @endif
      							</div>
							<!--
                            <div class="row">
                              @php
                              $colortypes = unserialize($card->colortype);
                              $cardcolor =array();
                              if(!empty($cardtempdetail['color'])){
                                  $cardcolor = (array)$cardtempdetail['color'];
                              }
                              @endphp
                              @if(!empty($colortypes))
                                @foreach($colortypes as $colortype)

                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="firstColor">
                                      @php
                                      $colselected ='';
                                      if (array_key_exists($colortype,$cardcolor)){
                                        $colselected =$cardcolor[$colortype];
                                      }
                                      @endphp
                                            <label>{{$colortype}} Color</label>
                                            <input data-jscolor="{value:{{$colselected}}}" value="{{$colselected}}" id="{{$colortype}}" name="color[{{$colortype}}]" class='form-control'>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                              @endif
                            </div>-->
                        </fieldset>
                        <fieldset>
                            <legend>Card type</legend>
                            <div class="row">
                              <div class="col-lg-6">
                                <div class="form-group">
                                  <label>Category</label>
                                  @php
                                    $catselected ='';
                                    if (array_key_exists('category_id',$cardtempdetail)){
                                      $catselected = $cardtempdetail['category_id'];
                                    }
                                  @endphp
                                  {{ Form::select('category_id',$categories,old('category_id') ? old('category_id') :$catselected ,['class'=>'form-control category style_2', 'id'=>'category_id','required'=>true]) }}
                                </div>
                              </div>
                              <div class="col-lg-6">
                                <div class="form-group">
                                  <label>Sub Category</label>
                                  @php
                                    $subcatselected = old('sub_category_id');
                                    if (array_key_exists('sub_category_id',$cardtempdetail)){
                                      $subcatselected = $cardtempdetail['sub_category_id'];
                                    }
                                  @endphp
                                  {{ Form::select('sub_category_id',[],$subcatselected,['class'=>'form-control subcategory style_2','id'=>'sub_category_id']) }}
                                  <input type="hidden"  id="subcategoryid" value="{{$subcatselected}}">
                                </div>
                              </div>
                            </div>
                          </fieldset>
                                              
                        <fieldset>
                            <legend>Social Links</legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <!-------choose social media---------->
                                  <ul class="multi_media">
                                    @php
                                    $social_media =array();
                                    if(!empty($cardtempdetail['social_media'])){
                                      $social_media = (array)$cardtempdetail['social_media'];
                                    }
                                    @endphp
                                    @foreach($socialMedia as $social)
                                    <li>
                                    <input type="checkbox"class="checkicon"data-name="{{$social['name']}}" id="{{$social['name']}}icon"@if(array_key_exists($social['name'],$social_media)) checked @endif />
                                    <label for="{{$social['name']}}icon">
                                      <!-- <img src="{{url($social['icon'])}}" /> -->
                                      <i class="fa {{$social['icon']}}" aria-hidden="true"></i>
                                    </label>
                                    </li>
                                    @endforeach
									               <span class="sm_condition">Min. {{$card->minsocialmedia}} required</span>
                                  </ul>
                                  <!-------choose social media end-------------------->
                                    <ul class="list-unstyled socialLinksboxx">
                                    @if(!empty($cardtempdetail['social_media']))
                                    @foreach($cardtempdetail['social_media'] as $name=>$link)
                                    <li class="clearfix" id="{{$name}}">
                                      <span class="titleSocial">
                                        <label>
                                          {{$name}}
                                        </label>
                                      </span>
                                      <span class="btnLinkright linkup">
                                        <input type="text" class="form-control link" name="social_media[{{$name}}]" placeholder="Enter profle link..." value="{{$link}}" readonly="1">
                                        <span class="msg">
                                        </span>
                                        <button type="button" class="btnCustomStyle2 btn-solid">
                                        Link
                                        </button>
                                        <span class="delete deletelink"><i class="fa fa-trash" aria-hidden="true"></i></span>
                                      </span>
                                    </li>
                                    @endforeach
                                    @endif
                                    </ul>
                                </div>
                            </div>
                        </fieldset>
						            <fieldset>
                            <legend>Profile </legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-group accordian_parent" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        Personal Information
                                                    </a>
                                                </h4>
                                            </div>
											<div id="collapseOne" class="panel-collapse collapse in show" role="tabpanel" aria-labelledby="headingOne">
												<div class="panel-body personalBoxxx">
													<div class="row">	
												  @if($card->username =='1')
													@php
													$firstname ='';
													$lastname  ='';
													  if(array_key_exists('first_name',$cardtempdetail)){
														$firstname = $cardtempdetail['first_name'];
													  }
													  if(array_key_exists('last_name',$cardtempdetail)){
														$lastname = $cardtempdetail['last_name'];
													  }
													@endphp
												<div class="col-md-12 col-lg-6">
													<div class="form-group ">
														<input type="text" class="form-control" name="first_name" placeholder="First Name" value="{{$firstname}}">
														<small class="text-muted1 first_name"></small>
													</div>
												</div>
												<div class="col-md-12 col-lg-6">
													<div class="form-group ">
														<input type="text" class="form-control" placeholder="Last Name" name="last_name" value="{{$lastname}}">
														<small class="text-muted1 last_name"></small>
													</div>
												</div>
											  @endif

											@if($card->primaryphone =='1')
											  @php
											  $primaryphone='';
												if(array_key_exists('primaryphone',$cardtempdetail)){
												  $primaryphone = $cardtempdetail['primaryphone'];
												}
											  @endphp
											<div class="col-md-12 col-lg-6 ">
												<div class="form-group">
													<input type="text" name="primaryphone" class="form-control" placeholder="Primary Number" maxlength="10" value="{{$primaryphone}}">
														<small class="text-muted1 phone"></small>
												</div>
											</div>
											@endif

											  @if($card->username2 =='1')
													@php
													$otherfname ='';
													$otherlname  ='';
													  if(array_key_exists('otherfname',$cardtempdetail)){
														$otherfname = $cardtempdetail['otherfname'];
													  }
													  if(array_key_exists('otherlname',$cardtempdetail)){
														$otherlname = $cardtempdetail['otherlname'];
													  }
													@endphp
												<div class="col-md-12 col-lg-6">
													<div class="form-group ">
														<input type="text" class="form-control" name="otherfname" placeholder="Other First Name" value="{{$otherfname}}">
														<small class="text-muted1 otherfname"></small>
													</div>
												</div>
												<div class="col-md-12 col-lg-6">
													<div class="form-group ">
														<input type="text" class="form-control" placeholder="Other Last Name" name="otherlname" value="{{$otherlname}}">
														<small class="text-muted1 otherlname"></small>
													</div>
												</div>
											  @endif
											  @if($card->secondaryphone =='1')
												@php
												$secondaryphone='';
												  if(array_key_exists('secondaryphone',$cardtempdetail)){
													$secondaryphone = $cardtempdetail['secondaryphone'];
												  }
												@endphp
											<div class="col-md-12 col-lg-6 ">
												<div class="form-group">
													<input type="text" name="secondaryphone" class="form-control" placeholder="Secondary Number" maxlength="10" value="{{$secondaryphone}}">
														<small class="text-muted1 phone"></small>
												</div>
											</div>
											@endif


															   @if($card->company =='1')
															   @php
															   $company ='';
															   if(array_key_exists('company',$cardtempdetail)){
																	$company = $cardtempdetail['company'];
																}
															   @endphp
															   <div class="col-md-12 col-lg-6">
																  <div class="form-group ">
																	  <input type="text" class="form-control" id="company" placeholder="Company Name" name="company" value="{{$company}}">
																	  <small class="text-muted1 company"></small>
																  </div>
															  </div>
															   @endif
															   @if($card->bottomtitle =='1')
																@php
																$bottomtitle='';
																  if(array_key_exists('bottomtitle',$cardtempdetail)){
																	$bottomtitle = $cardtempdetail['bottomtitle'];
																  }
																@endphp
															  <div class="col-md-12 col-lg-6">
																  <div class="form-group ">
																	  <input type="text" class="form-control" placeholder="Bottom title" name="bottomtitle" value="{{$bottomtitle}}">
																	  <small class="text-muted1 bottomtitle"></small>
																  </div>
															  </div>
															@endif
															   @if($card->companyabout =='1')
															   @php
															   $companyabout='';
															   if(array_key_exists('companyabout',$cardtempdetail)){
																	$companyabout = $cardtempdetail['companyabout'];
																}
															   @endphp
															   <div class="col-md-12 col-lg-6">
																  <div class="form-group ">
																	  <input type="text" class="form-control" id="companyabout" placeholder="About Your Company" name="companyabout" value="{{$companyabout}}">
																	  <small class="text-muted1 companyabout"></small>
																  </div>
															  </div>
															   @endif
                              @if($card->designation =='1')
                              @php
                              $designation ='';
                                if(array_key_exists('designation',$cardtempdetail)){
                                  $designation = $cardtempdetail['designation'];
                                }
                              @endphp
                                        <div class="col-md-12 col-lg-6">
                                            <div class="form-group ">
                                                <input type="text" class="form-control" placeholder="Designation" name="designation" value="{{$designation}}">
                                                 <small class="text-muted1 designation"></small>
                                            </div>
                                        </div>
                                      @endif
                                @if($card->description =='1')
                                @for($i=1; $i<=$card->min_no_description; $i++)
                                @php
                                $hvalue='';
                                 $dvalue='';
                                if(!empty($cardtempdetail)){
                                	if(array_key_exists('description',$cardtempdetail)){
                                		$desarr = (array) $cardtempdetail['description'];
                                  		$dvalue = $desarr[$i];
                                	}
                                  
                                	if(array_key_exists('dheading',$cardtempdetail)){
                                		$headingarr = (array)$cardtempdetail['dheading'];
                                  		$hvalue = $headingarr[$i];
                                	}
                                }
                                @endphp
                                <div class="col-md-12">
                                  <div class="form-group ">
                                      <input type="text" class="form-control DescriptionHeading" placeholder="Heading of Description. Example About,Hobby, Study" data-no="{{$i}}" value="{{$hvalue}}" name="dheading[{{$i}}]">
                                  </div>
                                    <div class="form-group ">
                                        <textarea class="form-control descriptiontext{{$i}}" rows="3" name="description[{{$i}}]" placeholder="Description" maxlength="100">{{$dvalue}}</textarea>
                                        <small class="text-muted1 about"></small>
                                    </div>
                                </div>
                                @endfor
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingfour">
                        <h4 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                Contact Information
                            </a>
                        </h4>
                    </div>
                    <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                        <div class="panel-body businessInformation">
                            <div class="row">
                                    @if($card->calltagline =='1')
                                        @php
                                        $calltagline='';
                                          if(array_key_exists('calltagline',$cardtempdetail)){
                                            $calltagline = $cardtempdetail['calltagline'];
                                          }
                                        @endphp
                                      <div class="col-md-12 col-lg-6">
                                          <div class="form-group ">
                                              <input type="text" class="form-control" placeholder="Enter Call Us Tag line" name="calltagline" value="{{$calltagline}}">
                                              <small class="text-muted1 calltagline"></small>
                                          </div>
                                      </div>
                                    @endif
                                    @if($card->openclosetime =='1')
                                        @php
                                        $openclosetime='';
                                        $opentime['Time']='';
                                        $opentime['days']='';
                                          if(array_key_exists('openclosetime',$cardtempdetail)){
                                          $opentime = (array)$cardtempdetail['openclosetime'];
                                          }
                                        @endphp
                                      <div class="col-md-12 col-lg-6">
                                          <div class="form-group ">
                                              <input type="text" class="form-control" placeholder="Enter Opening time (8:00 - 19:00)" name="openclosetime[Time]" value="{{$opentime['Time']}}">
                                          </div>
                                      </div>
                                      <div class="col-md-12 col-lg-6">
                                        <div class="form-group ">
                                            <input type="text" class="form-control" placeholder="Enter Opening days (Mon - Fri)" name="openclosetime[days]" value="{{$opentime['days']}}">
                                        </div>
                                      </div>
                                    @endif
																@if($card->primaryemail =='1')
																@php
																$primaryemail='';
																  if(array_key_exists('primaryemail',$cardtempdetail)){
																	$primaryemail = $cardtempdetail['primaryemail'];
																  }
																@endphp
																				<div class="col-md-12 col-lg-6">
																					<div class="form-group ">
																						<input type="email" class="form-control" placeholder="Email" name="primaryemail" value="{{$primaryemail}}">
																						<small class="text-muted1 email"></small>
																					</div>
																				</div>
																			  @endif
																			  @if($card->secondaryemail =='1')
															  @php
																$secondaryemail='';
																  if(array_key_exists('secondaryemail',$cardtempdetail)){
																	$secondaryemail = $cardtempdetail['secondaryemail'];
																  }
																@endphp
																				<div class="col-md-12 col-lg-6">
																					<div class="form-group ">
																						<input type="email" class="form-control" placeholder="Email" name="secondaryemail" value="{{$secondaryemail}}">
																						<small class="text-muted1 email"></small>
																					</div>
																				</div>
																			  @endif
													  
														  @if($card->address =='1')
																@php
																$address='';
                                $location='';
																  if(array_key_exists('address',$cardtempdetail)){
																	$address = $cardtempdetail['address'];
																  }
                                  if(array_key_exists('location',$cardtempdetail)){
                                  $location = $cardtempdetail['location'];
                                  }
																@endphp
																	<div class="col-md-12 col-lg-6 ">
																		<div class="form-group ">
																			<input type="text" class="form-control" placeholder="Enter your Official address" name="address" value="{{$address}}" >
																			 <small class="text-muted1 address"></small>
																		</div>
																	</div>
                                  <div class="col-md-12 col-lg-6 ">
                                    <div class="form-group ">
                                      <input type="text" id="autocomplete" name="official_location" class="form-control" placeholder="Search your location" value="" onFocus="geolocate()">
                                       <small class="text-muted1 address"></small>
                                      <input type="hidden" name="location" id="latlong" value="{{$location}}">
                                    </div>
                                  </div>
															@endif
															@if($card->website =='1')
																@php
																$website='';
																  if(array_key_exists('website',$cardtempdetail)){
																	$website = $cardtempdetail['website'];
																  }
																@endphp
																				<div class="col-md-12 col-lg-6 ">
																					<div class="form-group">
																						<input type="text" class="form-control" placeholder="Website" name="website" value="{{$website}}">
																						 <small class="text-muted1 website"></small>
																					</div>
																				</div>
																				@endif
																				@if($card->profile =='1')

																				  <div class="col-md-12 col-lg-12">
																					<div class="form-group">
																						<div class="uploadBackground">
																							<label>Profile Image</label>
																							<input type="file" name="profileimg" id="input-file-now" class="dropify" />
																						</div>
																					</div>
																				</div>
																				@endif
																				@if($card->background =='1')
																				<div class="col-md-12 col-lg-6">
																					<div class="form-group">
																						<div class="uploadBackground">
																							<label>Background Image</label>
																							<input type="file" name="backgroundimg" id="input-file-background" class="dropify" />
																						</div>
																					</div>
																				</div>
																				@endif
																				@if($card->logo =='1')
																				<div class="col-md-12 col-lg-6">
																					<div class="form-group">
																						<div class="uploadBackground">
																							<label>Logo Image</label>
																							<input type="file" name="logo" id="input-file-logo" class="dropify" />
																						</div>
																					</div>
																				</div>
																				@endif
																				@if($card->gallery =='1')
																				@for($g=1; $g<= $card->max_no_gallery; $g++)
																				  <div class="col-md-12 col-lg-6">
																					<div class="form-group">
																						<div class="uploadBackground">
																						  <label>Gallery{{$g}} Image</label>
																						  <input type="file" name="gallery[{{$g}}]" id="input-file-gallery{{$g}}" class="dropify" />
																						</div>
																					</div>
																				  </div>
																				@endfor
																				@endif
																			</div>
																		</div>
																	</div>
										</div>
																@if($card->services =='1')
																<div class="panel panel-default">
																  <div class="panel-heading" role="tab" id="headingfour">
																	<h4 class="panel-title">
																	   <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#Services" aria-expanded="false" aria-controls="collapsefour">
																	   Services Information
																	   </a>
																	</h4>
																  </div>
																 <div id="Services" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
																	<div class="panel-body businessInformation">
																	   <div class="row">
																		@for($s=1; $s <= $card->max_no_services; $s++)
																  @php
																  
																	$title='';
                                    $desc='';
                                    if(!empty($cardtempdetail['services'])){

                                        $servicesarr = (array)$cardtempdetail['services'];
                                        if($servicesarr){
                                            $services = $servicesarr[$s];
                                        }
                                        if(isset($services->title)){
                                            $title = $services->title;
                                        }

                                        if(isset($services->description)){
                                            $desc = $services->description;
                                        }

                                    }
																  @endphp
                                  <div class="col-lg-12 mainlabel"><label>Service {{$s}}</label> </div>
                                    <div class="col-lg-6">
                                     <div class="form-group ">
                                      <input type="text" class="form-control" placeholder="Enter Services {{$s}}" name="services[{{$s}}][title]" value="{{$title}}">
                                      <small class="text-muted1 services{{$s}}"></small>
                                     </div>
                                    </div>

                               @if($card->serviceswithimg =='1')
                                  <div class="col-md-6">
                                    <div class="form-group">
                                        <div class="uploadBackground">
                                            <input type="file" name="services[{{$s}}][image]" id="input-file-service{{$s}}" class="dropify" />
                                        </div>
                                    </div>
                                  </div>

                                  @endif
                                    @if($card->servicedescription =='1')
                                    <div class="col-md-6">
                                    <div class="form-group">
                                      <div class="uploadBackground">
                                        <input type="text" class="form-control" name="services[{{$s}}][description]" id="input-description-service{{$s}}" placeholder="Enter description {{$s}}" value="{{$desc}}" />
                                      </div>
                                    </div>
                                    </div>
                                    @endif
																		@endfor
																	   </div>
																	</div>
																 </div>
															  </div>
																@endif
															</div>
														</div>
													</div>
												</fieldset><!--
                            <div class="col-md-12">
                                <p class="backBtn">
                                    <a class="btnCustomStyle2 btn-solid" href="javascript:void(0);">
                                        <span>Preview</span>
                                    </a>
                                </p>
                            </div>-->
                          
                    </form>
                    </div>
                <div class="col-md-3">
                    
                    <div id="frame" style="background-image: url('{{ url("web_assets")}}/img/Phone1.png');background-color: #cccccc;background-position: center;background-repeat: no-repeat;background-size: cover;
  position: relative;width: 263px;height: 535px;border-radius: 30px;"></br>
   <div id="loader" class="center" ></div> 
                         <div id="previewImage" ></div>
                        
                    </div>
                   
      
                    <div id="img">
                         <img class="img-fluid mx-auto d-block "  src="" alt="">
                    </div>
                  
                   
                </div>
                
            </div>
    <style>
        .abtSec {
      background: transparent;
   
    }
    
    .topHeader {
    background: transparent; 
        /*border-radius: 20px 20px 0px 0px;*/
    }
    .wrapper {
   
     background: transparent;
     /*border-radius: 20px 20px 20px 20px;*/
}
p{
    font-size:13px;
}
h6{
    font-size:12px;
}

element.style {
}

.socialMedia li .fa {
    font-size: 22px;
    text-align: center;
    text-decoration: none;
    width: 35px;
    height: 35px;
    line-height: 35px;
    /* color: #c8c8c8; */
}

.mainlabel{
  color: #000;
}
    </style>
     
            
<div id="html-content-holder" style="width: 333px;">
    <div >
            <section class="wrapper" style="display: block;height: 660px;width: 320px;margin-left: 13px;margin-top: 14.1px;border-radius: 30px;" id="wrapper">

        <!-- HEADER STARTS HERE -->
        <header>
            <section class="topHeader" style="padding: 30px 30px 56px 30px;border-radius: 30px 30px 0px 0px;">
                <div class="firstName" style="font-size:25px;">James</div>
                <div class="lastName" style="font-size:20px;">Anderson</div>
                <div class="designation" style="font-size:14px;">Sales Manager</div>
            </section>
        </header>
        <!-- HEADER END HERE -->

        <!-- ABOUT STARTS HERE -->
        <section class="abtSec" style="padding: 40px 10px 0 20px;">
            <table class="">
                <tr>
                    <p class="userImg"></p>
                </tr>
                <tr>
                    <td style="width:100%;"> <h1 id="h1" style="font-size:13px">About Me</h1></td>
                </tr>
                <tr>
                    <td style="width:100%;"><p class="content" style="text-align: justify;"></p></td>
                </tr>
            </table>
           
                
            </p>
           
            
        </section>
        <!-- ABOUT END HERE -->

        <!-- CONTACT STARTS HERE -->
        <section class="infoSection"  style="padding: 0 20px;margin-top:-20px;">
            <h2 id="h2" style="font-size:13px">contact Me</h2>
          
            <table class="list-unstyled">
                 
                <tr id="location">
                   
                </tr>
                   
               
                <tr id="phone">
                    
                </tr>
                
                <tr id="email">
                 
                </tr>
          </table>
        </section>
        <!-- CONTACT END HERE -->



        <!-- CONTACT STARTS HERE -->
        <section class="searchMe" style="padding: 0 20px;">
            <h2 style="font-size:13px">search Me @</h2>
            <table align="center">
                <tr>
                    <td>
                  <ul class="list-unstyled socialMedia d-flex" style="text-align:center;">
              
                
                   </ul>
                 </td>
            </tr>
            </table>
        </section>
        <!-- CONTACT END HERE -->
                <div style="display: none;" id="map"></div>

        <!-- FOOTER STARTS HERE -->
        <footer>
            <section class="footer">
                <!--<p class="webLink">-->
                   
                <!--</p>-->
                <!--<p class="nameUser"></p>-->
                
                <table>
                 
                     <tr class="webLink1">
                        
                     </tr>
                     
               
             
                 <tr class="nameUser1">
                         
                     </tr>
                 </table>
            </section>
        </footer>
        <!-- FOOTER END HERE -->
     </section>
</div>
 </div>      
        </div>  
    </section>
    <input type="hidden" value="{{$card->maxsocialmedia}}" id="max_no_socialmedia">
    <!-- FORM WIZARD END HERE -->
@endsection
@section('css')
<style type="text/css">
  
</style>
@endsection

@section('scripts')
    <script src="{{ url('web_assets/js/html2canvas.js') }}"></script>
    <script src="{{ url('web_assets/js/form-wizard.js') }}"></script>
    <script src="{{ url('web_assets/js/jscolor.js') }}"></script>
    <script src="{{ url('web_assets/js/dropify.js') }}"></script>
     
<style>
    canvas {
    height: 510px;
    width: 245px;
    margin-top: -11px;
    border-radius: 25px;
    margin-left: 5px;
    }
</style>
    <script>
        jscolor.presets.default = {
            borderColor:'rgba(236,239,245,0.6)', width:200, height:100,
            buttonColor:'rgba(70,51,255,1)', shadowColor:'rgba(0,0,0,0.05)'
        };
    </script>

    <script>
   
$(document).ready(function(){
            	$('#myModal').hide();
            	 $('#h1').hide();
            	 $('#h2').hide();
            	 $('.searchMe').hide();
            	 $('.btnCustomStyle2').hide();
            	 $('#code').val('+91');
            	 $('#loader').hide(); 
                 $('#loader1').hide(); 
            	 $('#wrapper').hide();
            	 $('.preloader').fadeOut('slow');
            // view();
            $('.sf-btn-finish').click(function(){
                    $('.preloader').fadeIn('slow');
                   
            });
            $('select').change(function(){
              $('.btnCustomStyle2').show();
            });
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });
           
            $("[type='file']").focusout(function(){
            
               var files =$('#input-file-now').prop('files')[0];
              if(files[0]!==''){
                  if(files[0].size > (1024*1024)){
                  alert('Please Insert Image Max 2MB.!');
                  $("#input-file-now").val(null);
                  }
              }
            
             
               });
               
        // $('.btnCustomStyle2').click(function(){
        //           $('.preloader').fadeIn('slow');
        //           	$('#loader').show();                  	
        //             var  files =$('#input-file-now').prop('files')[0];
        //             var form_data = new FormData($('#wizard_example_2')[0]);
        //             //form_data.append("file", files);
        //             $.ajax({
        //               type: 'post',
        //               url:  "route('')",
                      
        //               data: form_data,
        //               async: false,
        //               dataType:'json',
        //               success: function (data1) {
        //                 var data=data1.va;
        //                 $('.contactIcon').css('background',data.scondary);
        //                  if(data.img!==undefined){
        //                  $('.userImg').html('<td style="width:70%;"></td><td style="border:1px solid '+data.primary+';height:80px;border-radius: 40px;" align="right"><img class="img-fluid img-fluid1" src="http://thecardsbar.com/card/image/'+data.img+'" alt="userimage" style="border-radius: 40px;height: 82px;width: 82px; "></td>')
        //                  }
                                
        //                 $('.firstname').text(data.first_name);
        //                 $('.lastname').text(data.last_name);
        //                 $('.designation').text(data.designation);
        //                  if(data.about!==null){
        //                        $('#h1').show();
        //                 $('.content').text(data.about); 
        //                  }
        //                   if(data.first_name!==null){
        //                 $('.nameUser1').html('<td style="width:600px;text-align:center;"><p  style="text-align:center;font-size: 11px;"><span>'+data.first_name+'</span>&nbsp;<span>'+data.last_name+'</span></p></td>'); 
        //                   }
        //                   if(data.address!==null || data.phone!==null || data.email!==null){
        //                     $('#h2').show();
        //                   }
        //                  if(data.address!==null){
        //                   $('#location').html('<td style="width:40px;height:40px;"><p style="width:40px;font-size:20px;text-align:center;height:40px;border-radius: 20px;background:transparent;;"><i  class="fa fa-map-marker" aria-hidden="true" style="font-size:33px;margin-left:0px;margin-top:1px;margin-top: -3px;color:'+data.scondary+';" ></i></p></td><td><div class="contactInfo" style="margin-left: 5px;line-height: 0.5;"><h6 style="font-weight: 700;font-size: 12px;color:'+data.text+';">Location</h6><p style="line-height: 1.1;height: 30px;margin-top: -5px;color:'+data.scondary+';">'+data.address+'</p></div></td>');
        //                  }
        //                 if(data.phone!==null){
        //                 $('#phone').html('<td style="width:40px;height:40px;"><p style="width:40px;font-size:20px;text-align:center;height:40px;border-radius: 20px;background:transparent;"><i class="fa fa-mobile" aria-hidden="true" style="font-size:35px;margin-left:1px;margin-top:3px;color:'+data.scondary+';"></i></p></td><td><div class="contactInfo" style="margin-left: 5px;line-height: 0.5;"><h6 style="font-weight: 700;font-size: 12px;color:'+data.text+';">Phone Number</h6><p style="color:'+data.scondary+';"><a href="#">'+data.phone+'</a></p></div></td>'); 
        //                 }
        //                 if(data.email!==null){
        //                 $('#email').html('<td style="width:40px;height:40px;"><p style="width:40px;font-size:20px;text-align:center;height:40px;border-radius: 20px;background:transparent;"><i   class="fa fa-envelope" aria-hidden="true" style="font-size:23px;margin-left:1px;margin-top:6px;color:'+data.scondary+';"></i></p></td><td> <div class="contactInfo" style="margin-left: 5px;line-height: 0.5;"><h6 style="font-weight: 700;font-size: 12px;color:'+data.text+';">Email Address</h6><p style="color:'+data.scondary+';"><a href="#">'+data.email+'</a></p></div></td>');
        //                  }
        //                 if(data.website!==null){  
        //                 $('.webLink1').html('<td style="width:600px;"><p id="p" class="webLink" style="color:'+data.scondary+';font-size: 12px;"><a href="#" style="text-align:center;color:'+data.scondary+'"><i class="fa fa-globe" aria-hidden="true"></i> &nbsp; '+data.website1.substr(8)+'</a>  </p></td>');
        //                 }     
        //                 var i;
        //                 var html='';
        //                 if(data.social_media[1]!==null || data.social_media[2]!==null || data.social_media[3]!==null){
        //                    $('.searchMe').show();
        //                 }
        //                 for(i=1; i<= data1.count; i++){
        //                    if(data.social_media[i]!==null)
                          
        //                    html +='<li style=" background:'+data.scondary+'"><a href="'+data.social_media[i]+'" class="fa '+data.socialMediaList[i]+'"></a></li>'; 
        //                 }
        //                $('.socialMedia').html(html);
        //                $(".content").css("color",data.text);
        //                $("#p").css("color",data.scondary);
        //                $("h1").css("color",data.scondary);
        //                $("h2").css("color",data.scondary);
        //                $("h6").css("color",data.text);
        //                $(".topHeader").css("background",data.scondary);
        //                $(".topHeader").css("color",data.text);
        //                $(".abtSec").css("background",data.primary);
        //                $(".wrapper").css("color",data.text);
        //                $(".wrapper").css("background",data.primary);
        //                $(".wrapper").css("display","block");
        //                $('.preloader').fadeOut('slow');
        //                	$('#img').hide(); 
        //                		setTimeout(function(){ $('#loader').hide();  }, 4000);
        //                	 	dview();
        //                   //$('#myModal').show();
        //               },
        //                     cache: false,
        //                     contentType: false,
        //                     processData: false
        //         });
        // });
           
         $('button').click(function(){
             $('#myModal').hide(); 
         });   
            
            // $('.finish-btn').click(function(){
            //      $('form').attr('action', "{{url('/placeorder')}}");

                 //$('form').attr('action', "{{url('/generate_pdf')}}");
                //  $('form').attr('target','popup');
                //  $('form').attr(' onclick',"window.open('/generate_pdf','popup','width=800,height=800');");
            // });
            
            // function view(){
            
            //     $.ajax({
            //         url:"route('delete-image')",
            //         method:"get",
            //         success: function(data) {
            //             console.log(data);
            //         }
                    
            //     });
            // }
            
            
            
// global variable
 
    function dview() {
         var element = $("#html-content-holder"); // global variable
       var getCanvas; 

         html2canvas(element, {
              x: 50, y: 100,
              draggable: true,
         onrendered: function (canvas) {
                 
                $("#previewImage").html(canvas);
                getCanvas = canvas;
                console.log(canvas);
             }
         });
             
          $('#frame').show(); 
         $('#wrapper').hide(); 
        $('#img').hide(); 
       
    }
    $('#frame').hide(); 
  $('#wrapper').hide();  
 
 
      //("#Secondary option[value="+ color +"]").prop('disabled', true);              
    //   $("#text option[value=" + color + "]").prop('disabled', true);
    //     $("#Primary option[value="+ color +"]").prop('disabled', true);
   

    $("#Primary").change(function(){
       var color=$("#Primary").val();
        $("#Primary option[value="+ color +"]").prop('disabled', false);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
    }) ;   
        
  $("#Secondary").change(function(){
       var color=$("#Secondary").val();
       $("#Secondary option[value="+ color +"]").prop('disabled', false);
       $("#Primary option[value="+ color +"]").prop('disabled', true);
       $("#text option[value=" + color + "]").prop('disabled', true);
    }) ;   
    
    $("#text").change(function(){
       var color=$("#text").val();
        $("#text option[value=" + color + "]").prop('disabled', false);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
         $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
        
         $('.sf-controls').css("display","none");
        
        $('select').change(function(){
             var select1=$('#Primary').val();
             var select2=$('#Secondary').val();
             var select3=$('#text').val();
             if(select1!=='Choose Color' && select2!=='Choose Color' && select3!=='Choose Color'){
                 $('.sf-controls').css("display","block");
                   $('#active1').attr('class','active');
             }
            
        });
     
     $('.next-btn').click(function(){
              $('.sf-controls').css("display","block");
        });   
        
        function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
          if (regex.test(email)) {
                 return true;
               }
            else {
                  return false;
             }
        }
        
        
     function validatePhone(Phone) {
    
    var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (filter.test(Phone)) {
        return true;
    }
    else {
        return false;
    }
}
                    
});

$('body').on('submit', '#wizard_example_2', function(e) {
  e.preventDefault();
  var formId = $(this).attr('id');
  var formdataId = $(this).data("id");
  $('#'+formId).removeClass('border-red-error');
  $('#'+formId).find(".error").remove();
  $('#'+formId).find(".border-red").removeClass('border-red');
  var form = $('#'+formId);
  $.ajax({
      type: $(this).attr('method'),
      url: $(this).attr('action'),
      data: new FormData(this),
      processData: false,
      contentType: false,
      // beforeSend: function() {
      //     showLoader();
      // },
      success: function(data) {
        $('.preloader').css('display','none');
          if (data.success)
          {
              $(this).find("button[type='submit']").prop('disabled', true);
              //hideLoader();
              if (data.message != '') {
                $('.ajaxtoast').css('display','block');
                $('.ajaxtoast').find('.toast-body').text(data.message);
                // $('.image-upload-wrap').css('display','block');
                // $('.file-upload-content').css('display','none');
                // $('.themerow').html();
              }
              //$("#vistingcards").trigger("reset");
              //refreshDiv();
               if (data.extra.redirect)
               {
                setTimeout(function(){ window.location.href = data.extra.redirect; }, 1500);
               }
          } else {
             // hideLoader();
              $.each(data.errors, function(i, v) {
                  var error = '<div class="error">' + v + '</div>';
                  var split = i.split('.');
                  if (split[2]) {
                      var ind = split[0] + '[' + split[1] + ']' + '[' + split[2] + ']';
                      form.find("[name='" + ind + "']").addClass('border-red');
                      form.find("[name='" + ind + "']").parent().append(error);
                  } else if (split[1]) {
                      var ind = split[0] + '[' + split[1] + ']';
                      form.find("[name='" + ind + "']").addClass('border-red');
                      form.find("[name='" + ind + "']").parent().append(error);
                  } else {
                      form.find("[name='" + i + "']").addClass('border-red');
                      form.find("[name='" + i + "']").parent().append(error);
                      if(i=='location'){
                        form.find("[id='autocomplete']").addClass('border-red');
                        form.find("[id='autocomplete']").parent().append(error);
                      }
                  }
              });
              $('#'+formId).addClass('border-red-error');
              $(".border-red").first().focus();
          }
      },
      error: function(data) {
          console.log('An error occurred.');
      }
  });
});

$(document).on("focusout","#wizard_example_2 input,select,textarea,password",function(){
// $(document).on("click","#wizard_example_2 .sf-btn-next",function(){
  // var formdata =$('#wizard_example_2').serializeArray();
  // formdata.splice(0, 2);
 var formdata = new FormData($('#wizard_example_2')[0])
  $.ajax({
    url: "{{route('TempCardDetails')}}",
    method:'post',
    data: formdata,
    processData: false,
    contentType: false,
    success: function(result){
      
    }
  });
});

// $(document).on("focusout",".DescriptionHeading",function(){
//   var no = $(this).data('no');
//   $('.descriptiontext'+no).attr('name','description['+$(this).val()+']');

// });

// $( document ).ready(function() {
//    $.ajax({
//     url: "{{route('getTempForm')}}",
//     method:'get',
//     data:{
//       card_id:$('#card_id').val()
//     },
//     success: function(result){
//       if(result.status){
//         var obj = JSON.parse(result.data.card_details);
//         console.log(obj);
//         obj.forEach(function( item,key ) {
//           $("input:text[name='"+item.name+"']").val(item.value);
//           //$("input:select[name='"+item.name+"']").val(item.value);
//           $("input:[name='"+item.name+"']").val(item.value);
//         });
//       }
//     }
//   });
// });


$('.category').change(function() {
    var cate = $(this).val();
      $.ajax({
        url:"{{route('getSubcategory')}}",
        method:'get',
        data:{
          category_id:cate
        },
        success: function(res){
          var html = "";
          $.each(res, function (key, value) {
            html += "<option value="+key+">"+value+"</option>";
          });
          $(".subcategory").html(html);
        }
      });
  });

$( document ).ready(function() {
    var cate = $('.category').val();
    var subcate = $('#subcategoryid').val();
    $.ajax({
        url:"{{route('getSubcategory')}}",
        method:'get',
        data:{
          category_id:cate
        },
        success: function(res){
          var html = "";
          $.each(res, function (key, value) {
            var sect ='';
            if(subcate == key){
              sect='selected';
            }
            html += "<option value="+key+" "+sect+">"+value+"</option>";
          });
          $(".subcategory").html(html);
        }
      });
  });

  $(document).on('click', '.deletelink', function (e) {
    $(this).parent('.btnLinkright').find('input').removeAttr('readonly');
    $(this).parent('.btnLinkright').find('.btnCustomStyle2').css('display','block');
    $(this).css('display','none');
  });

$(document).ready(function(){
  $( ".sf-controls.clearfix" ).css('display','block');

   $('.btn-add').click(function(e){
       e.preventDefault();
       var max_fields_limit =$('#max_no_socialmedia').val();
       var sociallink2 = $('.socialmedia .sociallink').length;
       if(sociallink2 < max_fields_limit){
           $('.socialmedia').append('<div class="row sociallink"><div class="col-md-4"><select class="form-control selectsocialMedia"><option value="">Select</option><option value="Facebook">fa-facebook</option><option value="Skype">fa-skype</option><option value="WhatsApp">fa-whatsapp</option><option value="LinkedIn">fa-linkedin</option><option value="Twitter">fa-twitter</option></select></div><div class="col-md-4"><input type="text" name="social_media[]" class="form-control socialMedialink" placeholder="" value=""></div><span><button class="btn btn-danger remove_field" type="button"><i class="fa fa-minus-square-o" aria-hidden="true"></i></button></span></div>');
       }
   });  
   $('.socialmedia').on("click",".remove_field", function(e){
       e.preventDefault(); $(this).parents('.sociallink').remove(); x--;
   });

  $('.checkicon').change(function(e) {
      var linkname = $(this).data('name');
      var max_fields_limit =$('#max_no_socialmedia').val();
      var sociall = $('.socialLinksboxx li').length;
      if($(this).is(":checked")) {
        if(sociall < max_fields_limit){
        $('.socialLinksboxx').append('<li class="clearfix" id="'+linkname+'"><span class="titleSocial"><label>'+linkname+'</label></span><span class="btnLinkright linkup"><input type="text" class="form-control" name=social_media['+linkname+'] placeholder="Enter profle link..."><span class="msg"></span><button type="button" class="btnCustomStyle2 btn-solid">Link</button><span class="delete deletelink" style="display:none;"><i class="fa fa-trash" aria-hidden="true"></i></span></span></li>');
        }else{
          alert('You can added Maximum '+max_fields_limit+' social media');
          this.checked = false;
        }
      }else{
        $('.socialLinksboxx li#'+linkname).remove();
      }      
    });

  if($('.themeclass').is(':checked')){
     var img = $('.themeclass:checked').data('image');
    $("#img .img-fluid").attr('src',img);
  }

  $(".themeclass").change(function(e){
    $(".themeclass").prop('checked',false);
    $(this).prop('checked',true);

    var img = $(this).data('image');
    $("#img .img-fluid").attr('src',img);
  });

});

$(document).on('click', '.btnCustomStyle2', function (e) {
  var input = $(this).parents('li').find('input');
  var inputvalue = input.val();
  if(inputvalue!=''){
    input.attr('readonly','readonly');
    $(this).css('display','none');
    $(this).parents('li').find('.msg').css('display','none');
    $(this).parents('li').find('.deletelink').css('display','block');
  }else{
    $(this).parents('li').find('.msg').text('fill profile link');
  }
});

$(document).on('change', '.selectsocialMedia', function (e) {
  var media = $(this).val();
  $(this).parents('.sociallink').find('.col-md-4 .socialMedialink').attr('name','social_media['+media+']');
  $(this).parents('.sociallink').find('.col-md-4 .socialMedialink').attr('placeholder',media);
});

</script>
<script
      src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADlk166150RMLLGby78Ayq9kUKyAdHtp0&callback=initAutocomplete&libraries=places&v=weekly"
      async defer
    ></script>
<script> 
  let placeSearch;
  let autocomplete;
  // const componentForm = {
  //   street_number: "short_name",
  //   route: "long_name",
  //   locality: "long_name",
  //   administrative_area_level_1: "short_name",
  //   country: "long_name",
  //   postal_code: "short_name",
  // };
  const center = $('#latlong').val();
//   const defaultBounds = {
//   north: center.lat + 0.1,
//   south: center.lat - 0.1,
//   east: center.lng + 0.1,
//   west: center.lng - 0.1,
// };
  const options = {
    // bounds: defaultBounds,
    //componentRestrictions: { country: "us" },
    fields: ["address_components", "geometry", "icon", "name"],
    origin: center,
    //strictBounds: false,
    types: ["establishment"],
  };

  function initAutocomplete() {

      const map = new google.maps.Map(document.getElementById("map"), {
          center: { lat: -33.8688, lng: 151.2195 },
          zoom: 13,
          mapTypeId: "roadmap",
      });
      const input = document.getElementById("autocomplete");
      const searchBox = new google.maps.places.SearchBox(input);
      map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);



      // Create the autocomplete object, restricting the search predictions to
    // geographical location types.
    autocomplete = new google.maps.places.Autocomplete(
      document.getElementById("autocomplete"),
      options
    );
    // Avoid paying for data that you don't need by restricting the set of
    // place fields that are returned to just the address components.
    autocomplete.setFields(["address_component","geometry"]);
    // When the user selects an address from the drop-down, populate the
    // address fields in the form.
    autocomplete.addListener("place_changed", fillInAddress);
  }

  function fillInAddress() {
    var place = autocomplete.getPlace();
    // var lat = place.geometry.location.lat();
    // var lng = place.geometry.location.lng();

    var latlong = place.geometry.location.toJSON();
    $('#latlong').val(JSON.stringify(latlong));
    // // Get the place details from the autocomplete object.
    // const place = autocomplete.getPlace();

    // for (const component in componentForm) {
    //   document.getElementById(component).value = "";
    //   document.getElementById(component).disabled = false;
    // }

    // // Get each component of the address from the place details,
    // // and then fill-in the corresponding field on the form.
    // for (const component of place.address_components) {
    //   const addressType = component.types[0];

    //   if (componentForm[addressType]) {
    //     const val = component[componentForm[addressType]];
    //     document.getElementById(addressType).value = val;
    //   }
    // }
  }

  // Bias the autocomplete object to the user's geographical location,
  // as supplied by the browser's 'navigator.geolocation' object.
  function geolocate() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition((position) => {
        const geolocation = {
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        };
        const circle = new google.maps.Circle({
          center: geolocation,
          radius: position.coords.accuracy,
        });
        autocomplete.setBounds(circle.getBounds());
      });
    }
  }
</script>   
@endsection