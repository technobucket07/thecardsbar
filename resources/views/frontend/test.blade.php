<html>
        <!--Core CSS -->
      <link href="{{ url('bootstrap-4/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{ url('bootstrap-4/css/font-awesome.min.css')}}" rel="stylesheet">
    
     
     
     
        <style>
            


/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #3055c6; }
::selection { color: #f1f1f1;background: #3055c6; }



body  {font-family: 'Poppins', sans-serif;
/*background: #2a53c1;*/
}
.wrapper {width: 400px;margin: 0 auto;height: 800px;background: #ecf2fb;}


.profileboxx {text-align: center;
/*background-image: url(web_assets/img/bgimg.jpg);*/
height: 315px;
color: #fff;
    
}
.profileboxx .innerbox {padding: 40px 0 0 0;}
.profileboxx .innerbox .companyName {    text-transform: uppercase;
    font-size: 24px;
    font-weight: 600;
    letter-spacing: 1px;}
.profileboxx .innerbox .userimg  {     border: 3px solid #fff;
    width: 114px;
    height: 114px;
    margin: 15px auto;
    border-radius: 100px;}
.profileboxx .innerbox .userimg img {    border-radius: 100px;
    width: 100%;}

.profileboxx .innerbox .name  {    font-size: 20px;
    font-weight: 600;}



.information {margin: -40px 0 0 0;}
.information ul {display: table;margin: 0 auto;
/*background: #fff;*/
width: 80%;border-radius: 10px;padding: 30px;}
.information ul li {margin: 0 0 10px 0;}
.information ul li:last-child {margin: 0;}
.information ul li:after {display: block;clear: both;content: "";}
.information ul li .ledtname{float: left;width: 30%;    margin: 0;    font-size: 14px;color: #6e7179;}
.information ul li .rightname{float: left;width: 60%;margin: 0;font-size: 14px;color: #000;font-weight: 600;margin-left: 10%;}


.servicesBoxx {margin: 15px 0 15px 0;}
.servicesBoxx h4{    width: 80%;
    display: table;
    margin: 0 auto 15px;
    font-size: 14px;
    color: #6e7179;}
.servicesBoxx ul {    display: table;
    margin: 0 auto;
    /* background: #fff; */
    width: 80%;
    border-radius: 10px;
    /* padding: 30px; */
    text-align: center;}
.servicesBoxx ul li {margin: 0 0 10px 0;}
.servicesBoxx ul li:last-child {margin: 0;}
.servicesBoxx ul li:after {display: block;clear: both;content: "";}
.servicesBoxx ul li .ledtname{    float: left;
    width: 47%;
    margin: 0;
    font-size: 14px;
    color: #fff;
    background: #3055c6;
    border-radius: 50px;
    padding: 10px 0;}
.servicesBoxx ul li .rightname{    float: right;
    width: 47%;
    margin: 0;
    font-size: 14px;
    color: #fff;
    /* margin-left: 10%; */
    background: #3055c6;
    padding: 10px 0;
    border-radius: 50px;}


.socialMediaIcons {    display: table;
    margin:20px auto;
    /* background: #fff; */
    width: 85%;}
.socialMediaIcons ul{margin: 0;}
.socialMediaIcons ul li {display: inline-block;margin: 0 7px;}
.socialMediaIcons ul li a {  
 
    font-size: 30px;
    width: 50px;
    height: 50px;
    line-height: 50px;
    text-align: center;
    text-decoration: none;
    border-radius: 50%;
    display: inline-block;
    background: #ecf2fb;
    color: #2d6ff7;

}
.socialMediaIcons ul li a .fa {
    font-size: 26px;
}

.socialMediaIcons ul li a .fa:hover {
    opacity: 0.7;
}

.address {color: #2b53c2;font-size: 13px;text-align: center;}






















/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


}


        </style>
        
         <body  style="padding-bottom:-150px;">


       <section class="wrapper">
           
           <table style="width:100%;">
               <tr>
                   <th style="height:400px" >
                        <img src="{{url('web_assets/img/bgimg.jpg')}}" style="width:400px;height:100%">
                       
                        <div class="profileboxx" style="margin-top:-250px;">
                            
                            <div class="innerbox" style="margin-top:-150px;">
                             
                                <h2 class="companyName">Global Computers</h2>
                                <p style="border: 3px solid #fff;width: 98px;height: 98px;margin: 15px auto;border-radius: 50px;">
                                    <img class="" src="{{ url('web_assets/img/profilepix.jpg')}}" style="width:100px; height:100px;border-radius: 50px;margin-top:-1px;" alt="profilepix">
                                     
                                </p>
                                <p class="name">Steve Smith</p>
                            </div>
                            
                            <div style="width:100%;height:140px;margin-top:0px;">
                                 <div style="width:100%;background:transparent;height:80px;"></div>
                                  <div style="width:100%;background:#ecf2fb;height:80px;"></div>
                            </div>
                      
                        <div class="information" style="border-radius:20px;margin-left:30px;width:340px;background: #fff;margin-top:0px;margin-top:-100px;" >
                            <ul class="list-unstyled"  style="margin: 0 auto;border-radius: 10px;list-style: none;text-align:center;margin-top:-30px" >
                                <li>
                                    <p class="ledtname">Contact</p>
                                    <p class="rightname">+91-750851980</p>
                                </li>
                                  <li>
                                    <p class="ledtname">Whats App</p>
                                    <p class="rightname">+12 125 124 8574</p>
                                </li>
                                 <li>
                                    <p class="ledtname">Website</p>
                                    <p class="rightname">www.globals.com</p>
                                </li>
                                <li>
                                    <p class="ledtname">Email</p>
                                    <p class="rightname">info@example.com</p>
                                </li>
                               
                              
                                
                            </ul>
                        </div>
                     </div>
                   </th>
               </tr>
               
               <tr>
                   <th style="height:250px;">
                       <div style="background:#ecf2fb;height:100px;margin-top:-150px;">
                            <div class="servicesBoxx" style="width:340px;margin-left:30px;" >
                                <h4>Services</h4>
                              
                                <ul class="list-unstyled">
                                     <li>
                                        <p class="ledtname"  style="height:20px;width:150px;border-radius:20px;">Software</p>
                                        <p class="rightname"  style="height:20px;width:150px;border-radius:20px;">Tablets</p>
                                    </li>
                                      <li>
                                        <p class="ledtname"  style="height:20px;width:150px;border-radius:20px;">Network Devices</p>
                                        <p class="rightname"  style="height:20px;width:150px;border-radius:20px;">Peripherals</p>
                                    </li>
                                   
                                    <li>
                                        <p class="ledtname" style="height:20px;width:150px;border-radius:20px;">Desktops</p>
                                        <p class="rightname"  style="height:20px;width:150px;border-radius:20px;">Laptops</p>
                                    </li>
                                  
                                </ul>
                            </div>
                           
                       </div>
                       
                   </th>
               </tr>
               
               
               <tr>
                   <th style="height:100px">
                         <div style="margin-top:-120px;">
                        <div class="socialMediaIcons" >
                            <ul class="list-unstyled" style="margin-left:35px;">
                                <li style="margin-left:10px;">
                                    <a href="#">
                                        <i class="fa fa-facebook" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li style="margin-left:22px;">
                                    <a href="#">
                                        <i class="fa fa-skype" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li style="margin-left:22px;">
                                    <a href="#">
                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li style="margin-left:22px;">
                                    <a href="#">
                                        <i class="fa fa-linkedin" aria-hidden="true"></i>
                                    </a>
                                </li>
                                <li style="margin-left:22px;">
                                    <a href="#">
                                        <i class="fa fa-twitter" aria-hidden="true"></i>
                                    </a>
                                </li>
                            </ul>
                        </div>
                        <div class="address">
                           92-93, Lawrence Road, Amritsar, Punjab
                       </div>
                        </div>
                        
                   </th>
               </tr>
               
               
               
           </table>



        </section>



       
   


    </body>
    
    </html>