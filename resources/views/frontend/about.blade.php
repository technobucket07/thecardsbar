@extends('frontend.layouts.master')
@section('content')
<!-- BREADCRUMBS STARTS HERE -->
<section class="mainbreadCrumbs">
    <div class="container">
       <div class="row">
          <div class="col-lg-12">
             <div class="breadcrumbInner">
                <ul class="page-list list-unstyled">
                   <li class="breadcrumbItem"><a href="{{route('home')}}">Home</a></li>
                   <li class="breadcrumbItem active">About Us</li>
                </ul>
             </div>
          </div>
       </div>
    </div>
</section>
<!-- BREADCRUMBS END HERE -->
<!-- ABOUT INTRO STARTS HERE -->
<section class="intro-section section-padding">
  <div class="container about_bio">

{{--      <div class="row">--}}
{{--      <div class="col-lg-12 col-md-4">--}}
{{--        <div class="htit">--}}
{{--          <h4 class="first_title">Who We Are ?</h4>--}}
{{--          {!!$content[str_slug('We Are')]!!}--}}
{{--        </div>--}}
{{--      </div>--}}
{{--	</div>--}}

	<div class="row">
      <div class="col-lg-12">
        <div class="item mt-30">
          <h6>About Us</h6>
          {!!$content[str_slug('About Us')]!!}
        </div>
      </div>
	</div>

{{--	<div class="row">--}}
{{--      <div class="col-lg-12">--}}
{{--        <div class="item mt-30">--}}
{{--          <h6>Our Goals</h6>--}}
{{--          {!!$content[str_slug('Our Goals')]!!}--}}
{{--        </div>--}}
{{--      </div>--}}
{{--	</div>--}}
{{--	<div class="row">--}}
{{--      <div class="col-lg-12">--}}
{{--        <div class="item mt-30">--}}
{{--          <h6>Our Values</h6>--}}
{{--          {!!$content[str_slug('Our Values')]!!}--}}
{{--        </div>--}}
{{--      </div>--}}
{{--    </div>--}}

  </div>
</section>
<!-- ABOUT INTRO END HERE -->



@endsection
@section('scripts')

@endsection