@extends('frontend.layouts.master')
@section('content')
<link href="https://chart.googleapis.com/chart?">
<section class="purchased_pro">
	<div class="container">
		<div class="row">
			<div class="col-lg-3 col-md-3 col-sm-3 col-12">
				<div class="product_img_part">
					<a href="#"><img class="img-fluid" src="{{url($order->picture)}}" alt="logo-metri"></a>

				</div>
			</div>
			<div class="col-lg-9 col-md-9 col-sm-9 col-12">
				<div class="product_content">
					<p class="pro_title">{{$order->cardtitle}}</p>
					<p class="sm_discr">{{$order->carddescription}}</p>
					<p class="price"><span class="currency" id="currency">&#36;</span> {{$order->price}}</p>
					<p class="stat">Purchased on: <span class="stat_data">Jan 25, 2035</p>
					<p class="stat">Payment Method: <span class="stat_data">Visa Card 2342 XXXX XXXX</p>
					<p class="stat">Payment ID: <span class="stat_data">{{$order->payment_id}}</p>
					<div class="sharing">
						<a href="#" class="pdf"><span class="icon"><i class="fa fa-file-pdf-o" aria-hidden="true"></i></span>PDF</a>
						<a href="#" class="vcf"><span class="icon"><i class="fa fa-address-card-o" aria-hidden="true"></i></span>VCF</a>
						<a href="#" class="whtsapp"><span class="icon"><i class="fa fa-whatsapp" aria-hidden="true"></i></span>Whatsapp</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endsection
@section('scripts')
<script type="text/javascript">
</script>
@endsection