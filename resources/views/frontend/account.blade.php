@extends('frontend.layouts.master')
@section('content')
<!-- BREADCRUMBS STARTS HERE -->
<section class="mainbreadCrumbs">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <div class="breadcrumbInner">
               <h2 class="title">My Account</h2>
               <ul class="page-list list-unstyled">
                  <li class="breadcrumbItem"><a href="index.html">Home</a></li>
                  <li class="breadcrumbItem active">My Account</li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- BREADCRUMBS END HERE -->
<!-- Begin HorizontalTab style 1 -->

<section class="section-full other-bg">
   <div class="container">
      <!-------------------------------new set------------------------------->
      <div class="row">
         <div class="col-lg-3">
            <div class="nav flex-column nav-pills acc_tab_nav" id="v-pills-tab" role="tablist" aria-orientation="vertical">
               <a class="nav-link @if(!(Session::has('editprofile') || Session::has('changepass'))) active @endif" id="v-pills-dashboard-tab" data-toggle="pill" href="#v-pills-dashboard" role="tab" aria-controls="v-pills-dashboard" aria-selected="true"><i class="fc_icons fa fa-tachometer"></i> My Dashboard</a>
               <a class="nav-link @if(Session::has('changepass'))active @endif" id="v-pills-passsword_change-tab" data-toggle="pill" href="#v-pills-passsword_change" role="tab" aria-controls="v-pills-passsword_change" aria-selected="false"><i class="fa fa-key" aria-hidden="true"></i> Change Password</a>
               <a class="nav-link @if(Session::has('editprofile'))active @endif" id="v-pills-edit_profile-tab" data-toggle="pill" href="#v-pills-edit_profile" role="tab" aria-controls="v-pills-edit_profile" aria-selected="false"><i class="fc_icons fa fa-pencil-square-o"></i> Edit Profile</a>

               <!-- <a class="nav-link" id="v-pills-address-tab" data-toggle="pill" href="#v-pills-address" role="tab" aria-controls="v-pills-address" aria-selected="false"><i class="fc_icons fa fa-map-marker"></i> Addresses</a> -->

               <a class="nav-link" id="v-pills-logout-tab" data-toggle="pill" href="#v-pills-settings" role="tab" aria-controls="v-pills-settings" aria-selected="false"><i class="fc_icons fa fa-sign-out"></i> Sign Out</a>
            </div>
         </div>
         <div class="col-lg-9">
            <div class="tab-content" id="v-pills-tabContent">
               <div class="base_card dash tab-pane fade @if(!(Session::has('editprofile') || Session::has('changepass'))) active show @endif" id="v-pills-dashboard" role="tabpanel" aria-labelledby="v-pills-dashboard-tab">
                  <h3 class="acc_card_title"><i class="fa fa-tachometer"></i>DASHBOARD</h3>
                  <div class="base_card_body">
                     <img class="img-fluid user_img" src="{{url('storage/'.$user->profile)}}" alt="">
                     <h3 class="user_welcome">{{$user->name}}</h3>
                     <p class="text_discr">{{$user->about}}</p>
                     <!-- <div class="server_stats">
                        <ul>
                           <li><span class="icon likes"><i class="fa fa-heart" aria-hidden="true"></i></span><span class="counter">487</span></li>
                           <li><span class="icon shares"><i class="fa fa-share-alt" aria-hidden="true"></i></span><span class="counter">398</span></li>
                           <li><span class="icon downloads"><i class="fa fa-download" aria-hidden="true"></i></span><span class="counter">6869</span></li>
                           <li><span class="icon views"><i class="fa fa-eye" aria-hidden="true"></i></span><span class="counter"></span>2546</li>
                        </ul>
                     </div> -->
                     <div class="div_acc">
                        <p class="d_tail">User Info</p>
                        <table class="table table-striped">
                           <thead>
                              <tr>
                                 <td scope="col">User Name</td>
                                 <td scope="col">{{$user->name}}</td>
                              </tr>
                           </thead>
                           <tbody>
                              <tr>
                                 <td scope="row">Email</td>
                                 <td>{{$user->email}}</td>
                              </tr>
                              <tr>
                                 <td scope="row">User From</td>
                                 <td>{{date("d M Y", strtotime($user->created_at))}}</td>
                              </tr>
                              <tr>
                                 <td scope="row">Cards Created</td>
                                 <td>14</td>
                              </tr>
                              <tr>
                                 <td scope="row">Cards Purhcased</td>
                                 <td>74</td>
                              </tr>
                           </tbody>
                        </table>
                     </div>
                  </div>
               </div>
               <!------passsword_change tabview------->
               <div class="base_card tab-pane fade @if(Session::has('changepass'))active show @endif" id="v-pills-passsword_change" role="tabpanel" aria-labelledby="v-pills-passsword_change-tab">
                  <h3 class="acc_card_title"><i class="fa fa-key" aria-hidden="true"></i>CHANGE PASSOWRD</h3>
                  <ul>
                     @if(Session::has('changepass'))
                        @if($message = Session::get('errors'))
                           @foreach ($message->all() as $error) 
                            <li>
                              <div class="alert alert-warning">
                             <strong>Warning!</strong> {{ $error }}
                              </div>
                            </li>
                           @endforeach
                        @endif
                        @if(Session::has('success'))
                           <li>
                              <div class="alert alert-success">
                             <strong>Success!</strong> {{Session::get('success')}}
                              </div>
                            </li>
                        @endif
                     @endif
                  </ul>
                  <div class="base_card_body">
                  <form id="changePassword" method="post" action="{{route('changePassword')}}">
                     @csrf
                     <div class="row passcode_change">
                        <div class="col-lg-12 col-sm-12 form-six">
                           <div class="form-group{{ $errors->has('current-password') ? ' has-error' : '' }}">
                              <input type="password" name="current-password" id="current-password" class="form-control" placeholder="Enter Current Password" value="">
                           </div>
                        </div>
                        <div class="col-lg-12 col-sm-12 form-six">
                           <div class="form-group{{ $errors->has('new-password') ? ' has-error' : '' }}">
                              <input type="password" name="new-password" id="new-password" class="form-control" placeholder="Enter New Password" value="">
                           </div>
                        </div>
                        <div class="col-lg-12 col-sm-12 form-six">
                           <div class="form-group">
                              <input type="password" name="new-password_confirmation" id="new-password-confirm" class="form-control" placeholder="Confirm New Password" value="">
                           </div>
                        </div>
                        <div class="col-lg-12 col-sm-12 form-six">
                           <div class="update_btn">
                              <button type="submit" class="btnCustomStyle2 btn-solid">Submit</button>
                           </div>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
            <!------edit_profile tabview------->
            <div class="base_card tab-pane fade @if(Session::has('editprofile'))active show @endif" id="v-pills-edit_profile" role="tabpanel" aria-labelledby="v-pills-edit_profile-tab">
               <h3 class="acc_card_title"><i class="fa fa-pencil-square-o"></i>EDIT PROFILE</h3>
               <div class="base_card_body">
                  <div class="div_acct">
                     <div class="edit_profile">
                     <ul>
                        @if(Session::has('editprofile'))
                           @if($message = Session::get('errors'))
                              @foreach ($message->all() as $error) 
                               <li>
                                 <div class="alert alert-warning">
                                <strong>Warning!</strong> {{ $error }}
                                 </div>
                               </li>
                              @endforeach
                           @endif
                           @if(Session::has('success'))
                              <li>
                                 <div class="alert alert-success">
                                <strong>Success!</strong> {{Session::get('success')}}
                                 </div>
                               </li>
                           @endif
                        @endif
                     </ul>
                        <form id="Editprofile" method="post" action="{{route('UpdateAccount')}}" enctype="multipart/form-data">
                           @csrf
                           <div class="row">
                              <div class="col-lg-12">
                                 <div class="outer_uploader">
                                    <div class="circle">
                                       <!-- User Profile Image -->
                                       <img class="profile-pic" src="{{url('storage/'.$user->profile)}}">
                                       <!-- Default Image -->
                                       <!-- <i class="fa fa-user fa-5x"></i> -->
                                    </div>
                                    <div class="p-image">
                                       <i class="fa fa-camera upload-button"></i>
                                       <input class="file-upload" name="profile" type="file" accept="image/*"/>
                                    </div>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-sm-12 form-six">
                                 <div class="form-group">
                                    <input type="text" name="name" class="form-control" placeholder="Enter Name" value="{{$user->name}}">
                                 </div>
                              </div>
                              <div class="col-lg-6 col-sm-12 form-six">
                                 <div class="form-group">
                                    <input type="email" name="email" class="form-control" placeholder="Enter Email" value="{{$user->email}}">
                                 </div>
                              </div>
                              <div class="col-lg-6 col-sm-6 form-six">
                                 <div class="form-group">
                                    <input id="phoneorder" name="phone" class="form-control" placeholder="Enter Phone" value="{{$user->phone}}">
                                 </div>
                              </div>
                              <div class="col-lg-6 col-sm-6 form-six">
                                 <div class="form-group">
                                    <select class="form-control" name="gender" value= "{{old('gender')}}">
                                        <option>Gender</option>
                                            <option value="1" @if($user->gender =='1') selected @endif>Male</option>
                                            <option value="2"@if($user->gender =='2') selected @endif>Female</option>
                                            <option value="3"@if($user->gender =='3') selected @endif>Other</option>
                                    </select>
                                 </div>
                              </div>
                              <div class="col-lg-6 col-sm-12 form-six">
                                 <div class="form-group">
                                    <textarea name="about" class="form-control" placeholder="About">{{$user->about}}</textarea>
                                 </div>
                              </div>
                              <div class="col-lg-12 col-sm-12 form-six">
                                 <div class="update_btn">
                                    <button type="submit" class="btnCustomStyle2 btn-solid">Submit</button>
                                 </div>
                              </div>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
            <!------address tabview------->
            <!-- <div class="base_card tab-pane fade" id="v-pills-address" role="tabpanel" aria-labelledby="v-pills-address-tab">
               <h3 class="acc_card_title"><i class="fa fa-map-marker"></i>ADDRESSES</h3>
               <div class="base_card_body">
                  <div class="div_acc">
                     <ul>
                        <li>
                           <div class="dashboard-text">
                              <h4>James Anderson</h4>
                              <p>14301 Peacock Springs Trl Orlando FL</p>
                              <a href="#" data-toggle="modal" data-target="#EditAddress"><small>Edit Address</small></a>
                           </div>
                        </li>
                        <li>
                           <a href="#" class="text-red" data-toggle="modal" data-target="#AddDashboardAddress"><i class="fa fa-plus"></i> Add Address</a>
                        </li>
                     </ul>
                  </div>
               </div>
            </div> -->
         </div>
      </div>
   </div>
</section>
<!-- End HorizontalTab style 1 -->
@endsection