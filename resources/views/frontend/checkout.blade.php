@extends('frontend.layouts.master')
<style>
  p.noticeBeforePay {
    color: #777; background: none; font-size: 14px; text-align: left; word-break: break-word; }
  span.highlightspecific { color: black; font-size: 14px; font-weight: 500; }

  a:hover.textunderline {
    background-color: yellow;
  }

</style>
@section('content')
<!-- CART STARTS HERE --> 
<section class="cartMainBoxx">
  <div class="container">
    <!-- BREADCRUMBS STARTS HERE -->
    <div class="row">
      <div class="col-lg-12">
         <div class="breadcrumbInner">
            <ul class="page-list list-unstyled">
               <li class="breadcrumbItem"><a href="index.html">Home</a></li>
               <li class="breadcrumbItem active">Checkout</li>
            </ul>
         </div>
      </div>
    </div>
    <!-- BREADCRUMBS END HERE -->
    <!-- Cart Table Start -->
    <table class="ct-responsive-table">
      <thead>
        <tr>
          <th class="remove-item">Action</th>
          <th>Product</th>
          <th>Quantity</th>
          <th>Total</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="remove">
            <button type="button" class="close-btn close-danger remove-from-cart">
              <span></span>
              <span></span>
            </button>
          </td>
          <td data-title="Product">
            <div class="cartProductWrapper">
              <img src="{{url($order->themeimage ? $order->themeimage :($order->picture?$order->picture:''))}}" style="width:50px;">
              <div class="cartProductBody">
                <h6> <a href="#">{{$order->cardtitle}}</a> </h6>
                <p>{{$order->carddescription}}</p>
              </div>
            </div>
          </td>
          <td data-title="Quantity"> <strong>1</strong> </td>
          <td data-title="Price Total"> <strong>&#8377;{{$order->price}}</strong> </td>
        </tr>
      </tbody>
    </table>
    <!-- Cart Table End -->

    <!-- Coupon Code Start -->
    <div class="row promoCode">
      <div class="col-lg-6 left_aside">
        <div class="form-group mb-0">
          <div class="input-group mb-0">
            <input type="text" class="form-control" placeholder="Enter Coupon Code" aria-label="Coupon Code">
            <div class="input-group-append">
              <button class="btn-custom" type="button">Apply</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Coupon Code End -->

    <!-- Cart form Start -->
    <div class="row cartForm">
      <div class="offset-lg-6 col-lg-6">
        <h4>Total Amount</h4>
        <table>
          <tbody>
            <tr>
              <th>Actual Price</th>
              <td>&#8377;{{$order->actualprice}}</td>
            </tr>
			     <tr>
              <th>Discount</th>
              <td>
                @if($order->discounttype==1)
                {{$order->discount}}% 
                @else
                &#8377;{{$order->discount}}
                @endif 
                <!-- <span class="small"></span> --> </td>
            </tr>
            <tr>
              <th>Tax</th>
              <td>00 <span class="small">(0%)</span> </td>
            </tr>
            <tr>
              <th>Total</th>
              <td> <b>&#8377;{{$order->price}}</b> </td>
            </tr>
          </tbody>
        </table>

        <p class="noticeBeforePay" >By proceeding, you accept the latest versions of our
          <span class="highlightspecific" > <a class="textunderline" href="{{route('termsConditions')}}" target="_blank" > T&Cs </a> </span>,
          <span class="highlightspecific" > <a class="textunderline" href="{{route('privacy-policy')}}" target="_blank" > Privacy policy </a>  </span> and
          <span class="highlightspecific" > <a class="textunderline" href="{{route('refund-policy')}}" target="_blank" > Refund policy </a> </span>.</p>

        <a class="btnCustomStyle2 btn-solid btn-block text-center razorpay"  data-order_id="{{$order->id}}">Pay Now</a>
        <div class="razoreform" style="display: none;">
          <button id="rzp-button1">Pay</button>
        </div>
      </div>
    </div>
    <!-- Cart form End -->

  </div>

</section>
<!-- CART END HERE -->
@endsection

@section('scripts')
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script type="text/javascript">
  // $(".razorpay").on("click", function(){
  $(document).on('click', '.razorpay', function(e) {
    e.preventDefault();
    var order_id =$(this).data('order_id');
    $.ajax({
    url: "{{route('PayNow')}}",
    method: 'post',
    data: {
      order_id:order_id,
      _token:"{{ csrf_token() }}"
    },
    success: function(result){
       eval(result);
       $('#rzp-button1').click();
    }});
  });

  $(function()
  {
    $('[name="checkbox-01"]').change(function()
    {
      if ($(this).is(':checked')) {
        $('.payNowAfterConfirmation').attr('disabled',false);
        $(".red").css("background-color", "red");
      }else{
        $('.payNowAfterConfirmation').attr('disabled',true);
        $(".red").css("background-color", "green");
      }
    });
  });

</script>
@endsection