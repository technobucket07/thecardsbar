@extends('frontend.layouts.master')
@section('content')
<!-- BREADCRUMBS STARTS HERE -->
<section class="mainbreadCrumbs">
    <div class="container">
       <div class="row">
          <div class="col-lg-12">
             <div class="breadcrumbInner">
                <h2 class="title">eCards</h2>
                <ul class="page-list list-unstyled">
                   <li class="breadcrumbItem"><a href="{{route('home')}}">Home</a></li>
                   <li class="breadcrumbItem active">eCards</li>
                </ul>
             </div>
          </div>
       </div>
    </div>
</section>
<!-- BREADCRUMBS END HERE -->

<!-- ABOUT INTRO STARTS HERE -->
<section class="intro-section section-padding">
  <div class="container">
    <div class="row">
		 <div class="col-md-12">
			<div class="action_tabs">
				<!-- <button class="btn-custom btn btn-primary  collapsed" type="button" data-toggle="collapse" data-target="#content" aria-expanded="false" aria-controls="content">
					<span class="filter-text">Sort &amp; Filter</span>
				</button> -->
				
				<!-- <div class="content collapse bg-white shadow-lg py-4" id="content">
					<div class="interact_sec">
						<p class="title">Sort by:</p>
						<ul class="filter">
							<li><span class="name">Category</span><span class="action_on"><input type="checkbox"></span></li>
							<li><span class="name">Price</span><span class="action_on"><input type="checkbox"></span></li>
							<li><span class="name">Likes</span><span class="action_on"><input type="checkbox"></span></li>
							<li><span class="name">Latest</span><span class="action_on"><input type="checkbox"></span></li>
						</ul>
					</div>
					<div class="interact_sec">
						<p class="title">Filter by:</p>
						<ul class="sorting">
							<li> 
								<p class="sub_title">Price</p>
								<ul>
									<li><span class="name">Low-High</span><span class="action_on"><input type="checkbox"></span></li>
									<li><span class="name">High-Low</span><span class="action_on"><input type="checkbox"></span></li>
									<li><span class="name">Below 500</span><span class="action_on"><input type="checkbox"></span></li>
									<li><span class="name">Below 1000</span><span class="action_on"><input type="checkbox"></span></li>
								</ul>
							</li>
							<li> 
								<p class="sub_title">Popularity</p>
								<ul>
									<li><span class="name">Most Liked</span><span class="action_on"><input type="checkbox"></span></li>
									<li><span class="name">Most Viewed</span><span class="action_on"><input type="checkbox"></span></li>
									<li><span class="name">Trending</span><span class="action_on"><input type="checkbox"></span></li>
								</ul>
							</li>
						</ul>
					</div>
					<div class="interact_sec_btns">
						<a href="#" class="apply">Apply</a>
					</div>
							
				</div> -->
			</div>
			<div class="tab-slider--nav flexible_tabs">
				<ul class="tab-slider--tabs">
					@foreach($categories as $id=>$category)
					<a href="{{route('ecards',['category'=>$id])}}">
						<li class="tab-slider--trigger @if(request()->get('category')){{ request()->get('category') == $id ? 'active' :''}} @endif" rel="tab1">{{$category}}</li>
					</a>
					@endforeach
					<!-- <li class="tab-slider--trigger active" rel="tab1">BUSINESS</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">MUCH MORE</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li>
					<li class="tab-slider--trigger" rel="tab1">STUDENT</li> -->
				</ul>
			</div>
		</div> 
	</div>
	<div class="tab-slider--container spacing">
      <div id="tab1" class="tab-slider--body">
		<div class="row row--5">
		@foreach($cards as $card)
		<div class="col-lg-3 col-md-6 col-12">
            <div class="portfolio-style--3">
                <div class="topLikeandview">
                    <ul class="list-unstyled">
                        <li>
                            <a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a>
                            <span>159</span>
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                            <span>350</span>
                        </li>
                    </ul>
                </div>
                <div class="thumbnail">
                    <a href="{{ url($card->picture)}}" data-fancybox="gallery">
                        <img class="img-fluid" src="{{ url($card->picture)}}" alt="">
                    </a>
                </div>
                <div class="content">
                    <p class="portfoliotype"><span class="buy_icon"><i class="fa fa-shopping-bag" aria-hidden="true"></i></span> <span class="buy_amt">${{$card->price}}</span></p>
                    <h4 class="title">
                        <a href="#"><!--<span><img src="{{ url('web_assets')}}/img/user-img.png"></span>-->{{$card->cardtitle}}</a>
                    </h4>
                    <div class="portfolio-btn clearfix">
                        <a class="rn-btn text-white pull-left" href="{{route('dynamicCardSteps',$card->id)}}">Order</a>
                        <!-- <a class="rn-btn text-white pull-right" href="#">Know more</a> -->
                    </div>
                </div>
            </div>
        </div>
     	@endforeach
		</div>
		<!-----pageination-------->
			<nav aria-label="..." class="custom_paging">
			  <ul class="pagination">
			  	@if($cards->currentPage() > 1)
				<li class="page-item">
				  <a class="page-link" href="{{$cards->previousPageUrl()}}" tabindex="-1" aria-disabled="true">Previous</a>
				</li>
				@endif
				<!-- <li class="page-item"><a class="page-link" href="#">1</a></li>
				<li class="page-item active" aria-current="page">
				  <a class="page-link" href="#">2 <span class="sr-only">(current)</span></a>
				</li>
				<li class="page-item"><a class="page-link" href="#">3</a></li> -->
				<li class="page-item">
				  <a class="page-link" href="{{ $cards->nextPageUrl()}}">Next</a>
				</li>
			  </ul>
			</nav>
		<!-----pagination end ------>
	  </div>
	</div>
  </div>
</section>
<!-- ABOUT INTRO END HERE -->
@endsection
@section('scripts')
<script type="text/javascript">
	// $(document).ready(function(){
	//   $(".tab-slider--trigger").click(function(){
	//   	$('.tab-slider--trigger').removeClass('active');
	//   	$(this).removeClass('active');
	//   });
	// });
</script>
@endsection