<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>The CardBar</title>
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('web_assets/img/new_logo.png') }}">
  <link rel="icon" type="image/png" href="{{ asset('web_assets/img/new_logo.png') }}">
    <!--Core CSS -->
    <link href="{{ url('web_assets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ url('web_assets/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ url('web_assets/css/main.css') }}" rel="stylesheet">
    <link href="{{ url('web_assets/css/demo.css') }}" rel="stylesheet">
    <link href="{{ url('web_assets/css/webslidemenu.css') }}" rel="stylesheet">
    <link href="{{ url('web_assets/css/form-wizard.css') }}" rel="stylesheet">
    <link href="{{ url('web_assets/css/dropify.css') }}" rel="stylesheet">

    <link href="{{ url('web_assets/css/tabs.css') }}" rel="stylesheet">
    <link href="{{ url('web_assets/css/color_1.css') }}" rel="stylesheet">
    <link href="{{ url('web_assets/css/easy-responsive-tabs.css') }}" rel="stylesheet">
    <link href="{{ url('web_assets/css/animate.css') }}" rel="stylesheet">
    <link href="{{ url('web_assets/css/jquery.fancybox.min.css') }}" rel="stylesheet">
    <link href="{{ url('web_assets/css/owl.carousel.css') }}" rel="stylesheet">
    <link href="{{ url('web_assets/css/owl.theme.css') }}" rel="stylesheet">
    <link href="{{ url('web_assets/css/custom.css') }}" rel="stylesheet">
    <link href="{{ url('web_assets/css/responsive.css') }}" rel="stylesheet">
    <style>
        img.img-fluid.mx-auto.d-block.only-applyto-this {
            border: 1px solid #f00;
            border-radius: 17px;
            padding: 0px;

        }
        .facebook{
            background-color: #007bff;
        }
    </style>
    @yield('css')
</head>
<body>

<!-- HEADER STARTS HERE -->
<header>
    <div class="preloader" style="display: none;"></div>
    <!-- <div id="loader" class="center" ></div> 
    <div id="loader1" style="position: relative;">
        <h2 style="text-align: center;font-size:14px;">Please Waite ...</h2>
   </div> -->

    <div class="wsmenucontainer clearfix">
        <div id="overlapblackbg"></div>
        <section class="top-bar">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="contact-info-con">
                           <a href="mailto:support@thecardsbar.com"> <i class="fa fa-envelope-o"></i>&nbsp;&nbsp;support@thecardsbar.com
                           </a>
                        </div>
                    </div>

                    <div class="col-xs-12 col-sm-6">

                        <ul class="list-unstyled social-part">
                            <li><a href="https://www.facebook.com/The-Cards-Bar-335687521162464" target="_blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                            <!-- <li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li> -->
                            <li><a href="https://www.instagram.com/thecardsbar7/" target="_blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="https://twitter.com/bar_cards" target="_blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                        </ul>

                    </div>
                </div>
            </div>
        </section>
        <div class="wsmobileheader clearfix">
            <a id="wsnavtoggle" class="animated-arrow"><span></span></a>
{{--            <a href="index.html" class="smallogo"><img src="{{ url('web_assets/img/logo-cardbar.png')}}"></a>--}}
            <a href="{{url('/')}}" class="smallogo"><img src="{{ url('web_assets/img/new_logo.png')}}"></a>

            <a href="#" class="mobileshopIcon" data-toggle="modal" data-target="#loginSignup">
                <img class="d-block mx-auto" src="{{ url('web_assets')}}/img/counter-1.png" alt="">
            </a>
        </div>
        <div class="headerfull pm_buttoncolor02">
            <!--Main Menu HTML Code-->
            <div class="container whitebg">
                <div class="row">
                    <div class="wsmain">
                        <div class="smllogo">
                            <a href="{{url('/')}}"><img src="{{ url('web_assets')}}/img/new_logo.png" alt="logo-metri"></a>
                        </div>
                        <nav class="wsmenu clearfix">
                            <ul class="mobile-sub wsmenu-list clearfix">
                                <li>
                                    <a href="{{route('home')}}" class="navtext">
                                        Home
                                    </a>
                                </li>

                                <li>
                                    <a href="{{route('ecards')}}" class="navtext">
                                        All cards
                                    </a>
                                </li>

{{--                                <li>--}}
{{--                                    <a href="{{route('YourCollection')}}" class="navtext">--}}
{{--                                        Friends cards--}}
{{--                                    </a>--}}
{{--                                </li>--}}

                                <!-- <li>
                                    <a href="{{route('home')}}" class="navtext">
                                        Pricing
                                    </a>
                                </li>
                                <li>
                                    <a href="{{url('steps')}}" class="navtext">
                                        How it works?
                                    </a>
                                </li> -->

                                <li>
                                    <a href="{{route('MyCards')}}" class="navtext">
                                        My cards
                                    </a>
                                </li>

                                <li>
                                    <a href="{{route('contact-us')}}" class="navtext">
                                        Contact
                                    </a>
                                </li>

                                <li>
                                    <a href="{{route('about-us')}}" class="navtext">
                                        About us
                                    </a>
                                </li>


                                <li>
                                    @php
                                    use App\Models\Order;
                                    $cart=0;
                                    if(auth()->user()){
                                        $cart = Order::where('user_id',auth()->id())
                                        ->where('payment_status','1')->count();
                                    }
                                    @endphp
                                    
                                    <a href="{{route('MyCart')}}" class="navtext cart_icon">Cart <span class="cart-item-count counter_cart">({{$cart}})</span>
                                    </a>
                                </li>
                                <li class="loginSign after_logged_in">
                                    <ul class="list-unstyled">
                                        @guest
                                        <li>
                                            <a href="{{url('login')}}" id="loginSignupbtn" class="btnCustomStyle2 btn-solid" data-toggle="modal" data-target="#loginSignup">Sign In</a>
                                        </li>
                                        @else
                                        <li class="profileDropDown new_prof_dropdown">

                                            @if(!empty(Auth::user()->profile))
                                                <img class="img-fluid mx-auto only-applyto-this d-block" src="{{url('storage/'.Auth::user()->profile)}}" alt="">
                                            @else
                                                <img class="img-fluid mx-auto only-applyto-this d-block" src="{{ url('web_assets')}}/user.png" alt="">
                                            @endif

                                            <a href="#" class="navtext">
                                                <span>{{ Auth::user()->name }} <i class="fa fa-sort-desc" aria-hidden="true"></i></span>
                                            </a>
                                            <div class="megamenu clearfix">
                                                <ul class="list-unstyled dropBox">
                                                    <li><a href="{{route('MyOrders')}}">Orders</a></li>
{{--                                                    <li><a href="{{route('MyCards')}}">My Cards</a></li>--}}
                                                    <li><a href="{{route('account')}}">My Account</a></li>
                                                    <li>
                                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                                           onclick="event.preventDefault();
                                                         document.getElementById('logout-form').submit();">
                                                            {{ __('Logout') }}
                                                        </a>
                                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                        @csrf
                                                    </form>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                         @endguest
                                    </ul>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
            <!--Menu HTML Code-->
        </div>
    </div>
</header>
<!-- HEADER END HERE -->

@yield('content')

<!-- FOOTER STARTS HERE -->
<footer class="footerBox">
    <div class="footer-wrapper">
        <div class="row align-items-end row--0">
            <div class="col-lg-6">
                <div class="footerLeft">
                    <div class="inner">
                        <!-- <span>Lorem Ipsum</span> -->
                        <h2>Make a First Impression great</h2>
                        <a class="btnCustomStyle2" href="{{route('contact-us')}}"><span>Contact Us</span></a>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="footerRight">
                    <div class="row">
                        <div class="col-lg-6 col-sm-6 col-12">
                            <div class="footerLink">
                                <h4>Our policies</h4>
                                <ul class="ftLink list-unstyled">

{{--                                    <li><a href="{{url('/')}}">Home</a></li>--}}
                                    <li><a href="{{route('about-us')}}">About us</a></li>
                                    <li><a href="{{route('contact-us')}}">Contact us</a></li>
                                    <li><a href="{{route('privacy-policy')}}">Privacy policy</a></li>
                                    <li><a href="{{route('refund-policy')}}">Refundable policy</a></li>
                                    <li><a href="{{route('termsConditions')}}">Terms & conditions</a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-lg-6 col-sm-6 col-12">
                            <div class="footerLink">
                                <h4>Say Hello</h4>
                                <ul class="ftLink">
                                    <li><a href="mailto:support@thecardsbar.com">support@thecardsbar.com</a></li>
                                    <!-- <li><a href="#">hr@example.com</a></li> -->
                                </ul>
                                <div class="social-share-inner">
                                    <ul class="socialShare d-flex justify-content-start list-unstyled">
                                        <li>
                                            <a href="https://www.facebook.com/The-Cards-Bar-335687521162464" target="_blank">
                                                <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 320 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"></path>
                                                </svg>
                                            </a>
                                        </li>
                                        <!-- <li>
                                            <a href="#" target="_blank">
                                                <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 448 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M100.28 448H7.4V148.9h92.88zM53.79 108.1C24.09 108.1 0 83.5 0 53.8a53.79 53.79 0 0 1 107.58 0c0 29.7-24.1 54.3-53.79 54.3zM447.9 448h-92.68V302.4c0-34.7-.7-79.2-48.29-79.2-48.29 0-55.69 37.7-55.69 76.7V448h-92.78V148.9h89.08v40.8h1.3c12.4-23.5 42.69-48.3 87.88-48.3 94 0 111.28 61.9 111.28 142.3V448z"></path>
                                                </svg>
                                            </a>
                                        </li> -->
                                        <li>
                                            <a href="https://www.instagram.com/thecardsbar7/" target="_blank">
                                                <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 448 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M224.1 141c-63.6 0-114.9 51.3-114.9 114.9s51.3 114.9 114.9 114.9S339 319.5 339 255.9 287.7 141 224.1 141zm0 189.6c-41.1 0-74.7-33.5-74.7-74.7s33.5-74.7 74.7-74.7 74.7 33.5 74.7 74.7-33.6 74.7-74.7 74.7zm146.4-194.3c0 14.9-12 26.8-26.8 26.8-14.9 0-26.8-12-26.8-26.8s12-26.8 26.8-26.8 26.8 12 26.8 26.8zm76.1 27.2c-1.7-35.9-9.9-67.7-36.2-93.9-26.2-26.2-58-34.4-93.9-36.2-37-2.1-147.9-2.1-184.9 0-35.8 1.7-67.6 9.9-93.9 36.1s-34.4 58-36.2 93.9c-2.1 37-2.1 147.9 0 184.9 1.7 35.9 9.9 67.7 36.2 93.9s58 34.4 93.9 36.2c37 2.1 147.9 2.1 184.9 0 35.9-1.7 67.7-9.9 93.9-36.2 26.2-26.2 34.4-58 36.2-93.9 2.1-37 2.1-147.8 0-184.8zM398.8 388c-7.8 19.6-22.9 34.7-42.6 42.6-29.5 11.7-99.5 9-132.1 9s-102.7 2.6-132.1-9c-19.6-7.8-34.7-22.9-42.6-42.6-11.7-29.5-9-99.5-9-132.1s-2.6-102.7 9-132.1c7.8-19.6 22.9-34.7 42.6-42.6 29.5-11.7 99.5-9 132.1-9s102.7-2.6 132.1 9c19.6 7.8 34.7 22.9 42.6 42.6 11.7 29.5 9 99.5 9 132.1s2.7 102.7-9 132.1z"></path>
                                                </svg>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="https://twitter.com/bar_cards" target="_blank">
                                                <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 512 512" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M459.37 151.716c.325 4.548.325 9.097.325 13.645 0 138.72-105.583 298.558-298.558 298.558-59.452 0-114.68-17.219-161.137-47.106 8.447.974 16.568 1.299 25.34 1.299 49.055 0 94.213-16.568 130.274-44.832-46.132-.975-84.792-31.188-98.112-72.772 6.498.974 12.995 1.624 19.818 1.624 9.421 0 18.843-1.3 27.614-3.573-48.081-9.747-84.143-51.98-84.143-102.985v-1.299c13.969 7.797 30.214 12.67 47.431 13.319-28.264-18.843-46.781-51.005-46.781-87.391 0-19.492 5.197-37.36 14.294-52.954 51.655 63.675 129.3 105.258 216.365 109.807-1.624-7.797-2.599-15.918-2.599-24.04 0-57.828 46.782-104.934 104.934-104.934 30.213 0 57.502 12.67 76.67 33.137 23.715-4.548 46.456-13.32 66.599-25.34-7.798 24.366-24.366 44.833-46.132 57.827 21.117-2.273 41.584-8.122 60.426-16.243-14.292 20.791-32.161 39.308-52.628 54.253z"></path>
                                                </svg>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-12 col-12">
                            <div class="copyright">
                                <p>Copyright © 2021 <a href="{{url('/')}}"><strong class="text-red">The Cards Bar.</strong></a> All Rights Reserved.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- socialNotEmail -->
@if (session('socialLogin'))
<div class="modal fade show" id="socialNotEmail" tabindex="-1" role="dialog" style="display: block;" aria-labelledby="socialNotEmailLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="socialNotEmailLabel">Please add Your email</h5>
      </div>
      <div class="alert alert-warning socialLogin" role="alert">
            {{session('socialLogin')}}
        </div>
        <form id="socialLoginEmailVerify" method="POST" action="{{ route('socialAddEnail') }}" aria-label="{{ __('UserVerify') }}">
            @csrf
          <div class="modal-body">
           <div class="content">
                <div class="form-group">
                    <input type="email" class="form-control" id="useremail" name="email" placeholder="Enter Email" value="">
                </div>
                @foreach(session('loginData') as $name=>$va)
                <input type="hidden" name="{{$name}}" value="{{$va}}">
                @endforeach
                <!-- <div class="form-group">
                    <input type="text" class="form-control" id="userphone" name="email" value="" placeholder="Enter phone">
                </div> -->
           </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btnCustomStyle2 btn-solid">Add email</button>
          </div>
        </form>
    </div>
  </div>
</div>
@endif
<!-- End -->
<!-- FOOTER ENDS HERE -->
<!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#otpverifymodel">
  otp verify
</button> -->
<div class="modal fade" id="otpverifymodel" tabindex="-1" role="dialog" aria-labelledby="otpverifymodelLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="otpverifymodelLabel">Please verify your email</h5>
        <!-- <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button> -->
      </div>
        <form id="verifyotpform" method="POST" action="{{ route('UserVerify') }}" aria-label="{{ __('UserVerify') }}">
            @csrf
          <div class="modal-body">
           <div class="content">
                <div class="form-group">
                    <input type="hidden" class="form-control" id="verifyemail" name="email" value="">
                    <input type="text" class="form-control" id="verifyotp" name="otp" value="" placeholder="Enter otp">
                    <small id="verifyotperror" style="display: none;" class="form-text invalid-feedback"role="alert"></small>
                </div>
                <a href="#" class="anchar" id="sendagainotp">Resend OTP</a>
           </div>
          </div>
          <div class="modal-footer">
            <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
            <button type="submit" class="btnCustomStyle2 btn-solid">Verify</button>
          </div>
        </form>
    </div>
  </div>
</div>

<!-- MODALS STARTS HERE -->
<div class="modal fade" id="loginSignup" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">


            <button type="button" class="close-btn" data-dismiss="modal" aria-label="Close">
                <span></span>
                <span></span>
            </button>



            <div class="content">
                <!-- Nav pills -->
                <ul class="nav nav-pills" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="pill" href="#login">Sign In</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="pill" href="#regis">Sign Up</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div id="login" class="container tab-pane active">
                        <h2 class="signup-form-title">Sign in to your account!</h2>
                         <!-- alert massage -->
                        <div class="alert alert-success loginsuccessmsg" style="display: none;" role="alert">
                        </div>
                        <div class="alert alert-danger loginerrormsg"style="display: none;" role="alert">
                        </div>
                        <div class="alert alert-warning loginwarningmsg"style="display: none;" role="alert">
                        </div>
                        <!-- alert massage end -->
                        <form id="formLogin" method="POST" action="{{ route('UserLoginajax') }}" aria-label="{{ __('UserLoginajax') }}">
                            @csrf
                            <div class="form-group">
                                <!-- <label for="exampleFormControlInput1">Email address</label> -->
                                <input type="email" class="form-control" id="loginemail" placeholder="name@example.com" name="email" value="" required="1">
                                <!-- <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small> -->
                            </div>
                            <div class="form-group">
                                <!-- <label for="exampleInputPassword1">Password</label> -->
                                <input type="password" class="form-control" id="loginpassword" placeholder="Password" name="password" value=""required="1">
                                <!-- <small id="emailHelp" class="form-text text-muted">Password incorrect.</small> -->
                            </div>

                            <div class="checkboxes d-flex">
                                <label class="privacy-policy"> Remember Me
                                    <input type="checkbox"name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <span class="checkmark"></span>
                                </label>
                                <a href="{{ route('password.request') }}">Forgot password?</a>
                            </div>
                            <button type="submit" class="btnCustomStyle2 btn-solid">Sign In</button>
                        </form>
                        <div class="connector text-center"><span>or</span></div>

                        <div class="signup-form-signin d-flex">
                            <a href="{{route('social.oauth','google')}}" class="theme-button google" type="submit">Google
                                <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 488 512" class="icon" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M488 261.8C488 403.3 391.1 504 248 504 110.8 504 0 393.2 0 256S110.8 8 248 8c66.8 0 123 24.5 166.3 64.9l-67.5 64.9C258.5 52.6 94.3 116.6 94.3 256c0 86.5 69.1 156.6 153.7 156.6 98.2 0 135-70.4 140.8-106.9H248v-85.3h236.1c2.3 12.7 3.9 24.9 3.9 41.4z"></path>
                                </svg>
                            </a>
                            <a href="{{route('social.oauth','facebook')}}" class="theme-button facebook" type="submit">
                                Facebook
                                <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 320 512" class="icon" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"></path>
                                </svg>
                            </a>
                        </div>
                
                    </div>
                    <div id="regis" class="container tab-pane fade">
                        <h2 class="signup-form-title">Create an account!</h2>
                        <form id ="formRegister" method="POST" action="{{ route('registerUser') }}" aria-label="{{ __('registerUser') }}" >
                            @csrf
                            <!-- <div class="form-group">
                                <label for="InputName">Full Name</label>
                                <input type="text" class="form-control" id="InputName" placeholder="Full Name">
                                 <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
                            </div> -->
                            <div class="form-group">
                                <!-- <label for="InputUsername">Username</label> -->
                                <input type="text" class="form-control" id="name" placeholder="name"name="name" value=""required="1">
                                <small id="rnameerror" style="display: none;" class="form-text invalid-feedback"role="alert"></small>
                            </div>
                            <div class="form-group">
                                <!-- <label for="exampleFormControlInput2">Email address</label> -->
                                <input type="email" class="form-control" id="remail" placeholder="name@example.com" name="email"value=""required="1">
                                <small id="remailerror" style="display: none;" class="form-text invalid-feedback"role="alert"></small>
                            </div>
                            <div class="form-group">
                                <input type="text" class="form-control" id="phone" placeholder="phone number" name="phone"value=""required="1">
                                <small id="rphoneerror" style="display: none;" class="form-text invalid-feedback"role="alert"></small>
                            </div>
                            <div class="form-group">
                                <!-- <label for="exampleInputPassword2">Password</label> -->
                                <input type="password" class="form-control" id="rpassword" placeholder="Password" name="password" value="">
                                <small id="rpassworderror" style="display: none;" class="form-text invalid-feedback"role="alert"></small>
                            </div>
                            <div class="form-group">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" placeholder="password confirm" required>
                            </div>
                            <!--  <div class="form-group">
                                 <label for="exampleInputPasswordVer">Verify Password</label>
                                 <input type="password" class="form-control" id="exampleInputPasswordVer" placeholder="Password">
                             </div> -->
                            <button type="submit" class="btnCustomStyle2 btn-solid">Sign Up</button>
                        </form>
                        <!-- <div class="connector text-center"><span>or</span></div>

                        <div class="signup-form-signin d-flex">
                            <button class="theme-button google" type="submit">Google
                                <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 488 512" class="icon" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M488 261.8C488 403.3 391.1 504 248 504 110.8 504 0 393.2 0 256S110.8 8 248 8c66.8 0 123 24.5 166.3 64.9l-67.5 64.9C258.5 52.6 94.3 116.6 94.3 256c0 86.5 69.1 156.6 153.7 156.6 98.2 0 135-70.4 140.8-106.9H248v-85.3h236.1c2.3 12.7 3.9 24.9 3.9 41.4z"></path>
                                </svg>
                            </button>
                            <button class="theme-button facebook" type="submit">
                                Facebook
                                <svg stroke="currentColor" fill="currentColor" stroke-width="0" viewBox="0 0 320 512" class="icon" height="1em" width="1em" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M279.14 288l14.22-92.66h-88.91v-60.13c0-25.35 12.42-50.06 52.24-50.06h40.42V6.26S260.43 0 225.36 0c-73.22 0-121.08 44.38-121.08 124.72v70.62H22.89V288h81.39v224h100.17V288z"></path>
                                </svg>
                            </button>
                        </div> -->
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- MODALS ENDS HERE -->

<!-- jQuery -->
<script class="jsbin" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script src="{{ url('web_assets/js/jquery.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ url('web_assets/js/proper.js') }}"></script>
<script src="{{ url('web_assets/js/bootstrap.min.js') }}"></script>
<script src="{{ url('web_assets/js/webslidemenu.js') }}"></script>
<script src="{{ url('web_assets/js/main.js') }}"></script>
<!-- <script src="{{ url('web_assets/js/form-wizard.js') }}"></script> -->
<script src="{{ url('web_assets/js/jscolor.js') }}"></script>
<script src="{{ url('web_assets/js/dropify.js') }}"></script>
<script src="{{ url('web_assets/js/jquery.fancybox.min.js') }}"></script>
<script src="{{ url('web_assets/js/owl.carousel.min.js') }}"></script>
<script src="{{ asset('js/share.js') }}"></script>
<script type="text/javascript">

$(document).on('submit', '#formRegister', function(e) {
    e.preventDefault();
    $('.preloader').css('display','block');
    $.ajax({
     method: $(this).attr('method'),
     url: $(this).attr('action'),
     data: $(this).serialize(),
     dataType: "json",
         success: function(response) {
            $('.preloader').css('display','none');
            if(response.status){
                $('#loginSignup').modal('hide');
                $('#otpverifymodel').modal('show');
                $('#verifyemail').val(response.email);
                $('.invalid-feedback').css('display','none');
                $('#formRegister')[0].reset();
                console.log('email',response.email);
                console.log('otp',response.otp); 
            }else{
                var myerrors = response.errors;
                //var myerrors = JSON.parse(response.errors);
                if(myerrors.password != undefined && myerrors.password.length >0){

                    $('#rpassworderror').css('display','block').text(response.errors.password[0]);
                }
                if(myerrors.name != undefined && myerrors.name.length >0){
                    $('#rnameerror').css('display','block').text(response.errors.name[0]);
                }
                
                if(myerrors.email != undefined && myerrors.email.length >0){
                    $('#remailerror').css('display','block').text(response.errors.email[0]);
                }
                if(myerrors.phone != undefined && myerrors.phone.length >0){
                    $('#rphoneerror').css('display','block').text(response.errors.phone[0]);
                }
                console.log('errrr',response.errors.password);
            }
            
         }
    });
});


$(document).on('submit', '#socialLoginEmailVerify', function(e) {
    e.preventDefault();
   $('.preloader').css('display','block');
    $.ajax({
     method: $(this).attr('method'),
     url:   $(this).attr('action'),
     data: $(this).serialize(),
     dataType: "json",
         success: function(response) {
            $('.preloader').css('display','none');
            if(response.status){
                $('#socialNotEmail').css('display','none');
                $('#otpverifymodel').modal('show');
                $('#verifyemail').val(response.email);
                $('#verifyotperror').css('display','block').text(response.massage);
            }else{
                $('#verifyotperror').css('display','block').text(response.massage);
            }
         }
    });
});

$(document).on('submit', '#verifyotpform', function(e) {
    e.preventDefault();
   $('.preloader').css('display','block');
    $.ajax({
     method: $(this).attr('method'),
     url:   $(this).attr('action'),
     data: $(this).serialize(),
     dataType: "json",
         success: function(response) {
            $('.preloader').css('display','none');
            if(response.status){
                $('#otpverifymodel').modal('hide');
                $('#loginSignup').modal('show');
                $('#regis').removeClass('active');
                $('#login').addClass('active');
                $('.loginsuccessmsg').css('display','block').text(response.massage);
                $('.loginerrormsg').css('display','none');
                //window.location.href = "{{route('UserLoginajax')}}";
                location.reload();
            }else{
                $('#verifyotperror').css('display','block').text(response.massage);
            }
         }
    });
});

$(document).on('submit', '#formLogin', function(e) {
    e.preventDefault();
    $('.preloader').css('display','block');
    $.ajax({
     method: $(this).attr('method'),
     url:   $(this).attr('action'),
     data: $(this).serialize(),
     dataType: "json",
         success: function(response) {
            $('.preloader').css('display','none');
            if(response.status){
                if(response.isadmin){
                    window.location.href = "{{route('admin.home')}}";   
                }else{
                    window.location.href = "{{route('home')}}";
                }
                
            }else{
                if(response.notverified){
                    $('#verifyemail').val(response.email);
                    $('#otpverifymodel').modal('show');
                    $('#loginSignup').modal('hide');
                    $('#verifyotperror').css('display','block').text(response.massage);
                }else{
                    $('.loginerrormsg').css('display','block').text(response.massage);
                    $('.loginsuccessmsg').css('display','none');
                }
            }
         }
    });
});

$(document).on('click', '#sendagainotp', function(e) {
    $.ajax({
     method:'post',
     url: "{{route('reSendOtp')}}",
     data: {
        'email':$('#verifyemail').val(),
        '_token':"{{ csrf_token() }}"
     },
     dataType: "json",
        success: function(response) {
            $('#verifyotperror').css('display','block').text(response.massage);
            console.log(response.otp);
        }
    });
});

// $(document).on('click','.nav-item.dropdown', function(e){
//     if($('.dropdown-menu.dropdown-menu-right').hasClass('show')){
//         $('.dropdown-menu.dropdown-menu-right').css({'position':'relative','transform':''});
//     }
// });

</script>

<script>
$(document).ready(function() {

    
    var readURL = function(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('.profile-pic').attr('src', e.target.result);
            }
    
            reader.readAsDataURL(input.files[0]);
        }
    }
    

    $(".file-upload").on('change', function(){
        readURL(this);
    });
    
    $(".upload-button").on('click', function() {
       $(".file-upload").click();
    });
});
</script>


@if (session('Notlogin'))
<script type="text/javascript">
    $('#loginSignup').modal('show');
    $('.loginwarningmsg').css('display','block').text("{{session('Notlogin')}}");  
</script>
@endif


@yield('scripts')

</body>
</html>