@extends('frontend.layouts.master')
@section('content')
        <!-- BREADCRUMBS STARTS HERE -->
        <section class="mainbreadCrumbs">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="breadcrumbInner">
                        <h2 class="title">{{$page}}</h2>
                        <ul class="page-list list-unstyled">
                           <li class="breadcrumbItem"><a href="{{route('home')}}">Home</a></li>
                           <li class="breadcrumbItem active">mycards</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
        </section>
        <!-- BREADCRUMBS END HERE -->

        <!-- ABOUT INTRO STARTS HERE -->
        <section class="intro-section section-padding">
          <div class="container">
              @if(count($orders) !=0)
              <div class="row row--5 slidelayout_02">
                @foreach($orders as $card)
                <div class="col-lg-4 col-md-6 col-12">
                  <div class="texture_bg">
                    <div class="portfolio-style--3">
                        <div class="topLikeandview">
                            <ul class="list-unstyled">
                                @php
                                if(auth()->id() == Isuserlike($card->cardid)){
                                    $like ='like';
                                }else{
                                    $like ='dislike';
                                }
                                @endphp
                                <li>
                                    <a href="javascript:void(0);">
                                      <div class="heartbutton {{$like}}"  data-type="like" data-usercardid="{{$card->cardid}}" data-templateid="{{$card->card_template_id}}" data-post_id="102" data-post_animation="bloom">
                                        @if(auth()->id() == Isuserlike($card->cardid))
                                        <img src="{{ url('web_assets')}}/img/heart.png" class="heart-icon-outline heartsize heart-icon-outline heartsize liked">
                                        <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-filled heartsize stop-animation-bloom">
                                        <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-animation heartsize start-animation-bloom">
                                        @else
                                        <img src="{{ url('web_assets')}}/img/heart.png" class="heart-icon-outline heartsize">
                                        <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-filled heartsize">
                                        <img src="{{ url('web_assets')}}/img/red_heart.png" class="heart-icon-animation heartsize">
                                        @endif
                                      </div>
                                      <span class="numberlikes">{{countUserCardLikes($card->cardid)}}</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                                    <span>{{countUserCardViews($card->cardid)}}</span>
                                </li>
                            </ul>
                        </div>
                        <div class="thumbnail">
                          <!-- <a href="#" data-fancybox="gallery"> -->
                            <a href="{{route('Mycard',[$card->cardid])}}">
                            <img src="{{url('storage/'. ($card->cardimage ? $card->cardimage : ($card->themeimage ? $card->themeimage :($card->picture ? $card->picture:''))))}}" class="img-fluid">
                            </a>
                        </div>
                        <div class="content">
                            <div class="portfolio-btn clearfix">
                                <a class="rn-btn text-white pull-left" href="{{route('Mycard',[$card->cardid])}}">Know more</a>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
                @endforeach
            </div>
            {{--
              @foreach($orders as $card)
              <div class="col-lg-4 col-md-6 col-12">
                <div class="portfolio-style--3">
                  <div class="topLikeandview">
                    <ul class="list-unstyled">
                      <li>
                        <a href="#"><i class="fa fa-heart" aria-hidden="true"></i></a>
                        <span>159</span>
                      </li>
                      <li>
                        <a href="#"><i class="fa fa-eye" aria-hidden="true"></i></a>
                        <span>350</span>
                      </li>
                    </ul>
                  </div>
                  <div class="thumbnail">
                    <a href="img/portfolio-1.jpg" data-fancybox="gallery">
                      <img class="w-100" src="{{url($card->themeimage ? $card->themeimage :$card->picture)}}" >
                    </a>
                  </div>
                  <div class="content">
                    <div class="portfolio-btn clearfix">
                      <!-- <a class="rn-btn text-white pull-left" href="#">ORDER</a> -->
                      <a class="rn-btn text-white pull-right" href="{{route('Mycard',[$card->cardid])}}">Know more</a>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
              --}}
              @else
              <div class="col-lg-12" style="text-align: center;">
                 <span>{{$empty}}</span>
              </div>
              @endif
          </div>
        </section>
        <!-- ABOUT INTRO END HERE -->
@endsection
@section('scripts')
<script>
    $(document).on('click', '.heartbutton', function(e) {
        e.preventDefault();
        $.ajaxSetup({
            headers: {
              'X-CSRF-Token': "{{csrf_token()}}"
            }
        });
        var totallike = parseInt($(this).next('.numberlikes').text());
        var like=0;
        if($(this).hasClass('dislike')){
            $(this).removeClass('dislike');
            like=1;
            totallike++;
        }else{
            $(this).addClass('dislike')
            like=0;
            totallike--;
        }
        $(this).next('.numberlikes').text(totallike);
        $.ajax({
            type: "POST",
            url: "{{route('UserCardReviews')}}",
            data:{
                'action':$(this).data('type'),
                'usercardid':$(this).data('usercardid'),
                'templateid':$(this).data('templateid'),
                'like':like,
            },
            success:function(data) {
            }
        });
    });
    </script>

    <script>
    $(document).ready(function () {
  $(".heartbutton").click(function() {
    var currentAnimation = $(this).data("post_animation");
        $(this).find(".heart-icon-outline").toggleClass("liked");
        $(this).find(".heart-icon-animation").toggleClass("start-animation-"+currentAnimation);
        $(this).find(".heart-icon-filled").toggleClass("stop-animation-"+currentAnimation);
    });
});
  </script>
@endsection