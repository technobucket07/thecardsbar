@extends('frontend.layouts.master')
@section('content')
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif


        <!-- BREADCRUMBS STARTS HERE -->
        <section class="mainbreadCrumbs">
            <div class="container">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="breadcrumbInner">
                        <ul class="page-list list-unstyled">
                           <li class="breadcrumbItem"><a href="index.html">Home</a></li>
                           <li class="breadcrumbItem active">Contact Us</li>
                        </ul>
                     </div>
                  </div>
               </div>
            </div>
        </section>
        <!-- BREADCRUMBS END HERE -->
		
<!-- CONTACT FORM STARTS HERE -->
<section class="contactPage">
    <div class="container">
      <div class="row">
      <div class="col-lg-12" style="display:none;">
					<h3 class="acc_card_title"><img class="d-block mx-auto" src="{{ url('web_assets')}}/img/customer-support.png" alt="">Contact Us</h3>
			</div>
      </div>
                <div class="row innerBoxcon main_outer gap_upper">
                    <div class="col-md-4 left_aside">
                        <!--<img src="{{url('img/sendarrow.jpg')}}" alt="sendarrow">-->
						<img class="img-fluid" src="{{ url('web_assets')}}/img/email.png" alt="">
						<div class="context">
							<h3 class="font-bold">Get In Touch</h3>
							<p class="formPara">We all know how important your information is. They are always safe with us.</p>
						</div>

                    </div>
                    <div class="col-md-8">
					<!--
                        <form class="modalFormPhone contact_form" id="modalFormPhone" action="{{route('PostContactUs')}}" method="post" >
                          @csrf
                            <label>
                                <p class="label-txt">Name</p>
                                <input type="text" class="input" name="name" id="username" placeholder="User name">
                                <div class="line-box">
                                    <div class="line"></div>
                                </div>
                            </label>
                            <label>
                                <p class="label-txt">Email</p>
                                <input type="text" class="input" name="email" id="useremail" placeholder="E-mail">
                                <div class="line-box">
                                    <div class="line"></div>
                                </div>
                            </label>
                            <label>
                                <p class="label-txt">Phone</p>
                                <input type="text" class="input" name="phone" id="userphone" placeholder="Ph. number">
                                <div class="line-box">
                                    <div class="line"></div>
                                </div>
                            </label>
                            <label>
                                <p class="label-txt">Message</p>
                                <textarea class="input" name="message" id="usermessage" rows="2"></textarea>
                                <div class="line-box">
                                    <div class="line"></div>
                                </div>
                            </label>
                            <p class="expBtn">
                                <button href="#" type="submit" class="btnCustomStyle2 btn-solid">Send Message</button>
                            </p>
                        </form>
                        -->
						<form class="modalFormPhone contact_form" id="modalFormPhone" action="{{route('PostContactUs')}}" method="post" >
                          @csrf
                @php
                  $user = auth()->user();
                @endphp
              @if($user)
							<div class="row">
								<div class="col-lg-4 col-md-4 col-12 col-xs-12 col-sm-12">
									<input type="text" class="input" name="name" value="{{$user->name}}" id="username" placeholder="User name">
								</div>
								<div class="col-lg-4 col-md-4 col-12 col-xs-12 col-sm-12">
									<input type="mail" class="input" name="email" value="{{$user->email}}" id="useremail" placeholder="E-mail">
								</div>
								<div class="col-lg-4 col-md-4 col-12 col-xs-12 col-sm-12">
									<input type="number" class="input" name="phone" value="{{$user->phone}}" id="userphone" placeholder="Ph. number">
								</div>
							</div>
              @else
              <div class="row">
                <div class="col-lg-4 col-md-4 col-12 col-xs-12 col-sm-12">
                  <input type="text" class="input" name="name" id="username" placeholder="User name">
                </div>
                <div class="col-lg-4 col-md-4 col-12 col-xs-12 col-sm-12">
                  <input type="mail" class="input" name="email" id="useremail" placeholder="E-mail">
                </div>
                <div class="col-lg-4 col-md-4 col-12 col-xs-12 col-sm-12">
                  <input type="number" class="input" name="phone" id="userphone" placeholder="Ph. number">
                </div>
              </div>
              @endif
							<div class="row">
								<div class="col-lg-12 col-md-12 col-12 col-xs-12 col-sm-12">
									<textarea class="input" name="message" id="usermessage" rows="6" placeholder="Write us a message..."></textarea>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-12 col-md-12 col-12 col-xs-12 col-sm-12">
									<button href="#" type="submit" class="btnCustomStyle2 btn-solid">Send Message</button>
								</div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


        <div id="map">
        </div>
     

        <section class="contactArea">
            <div class="container">
				<div class="row">
					<div class="col-lg-12">
						<ul class="contact_opts">

							<li class="col-lg-6" style="width: 48%;" >
								<div class="mainAddress">
									<div class="icon">
										<img src="{{ url('web_assets')}}/img/placeholder.png" class="img-fluid">
									</div>
									<div class="inner">
										<h4 class="title">Address</h4>

                                            {!!$content[str_slug('Address')]!!}

										<!-- <p>The Poppy Factory, 20 Petersham Road,<br>Richmond, Surrey, TW10 6UW</p> -->
									</div>
								</div>
							</li>

{{--							<li>--}}
{{--								<div class="mainAddress">--}}
{{--									<div class="icon">--}}
{{--										<img src="{{ url('web_assets')}}/img/headphones.png" class="img-fluid">--}}
{{--									</div>--}}
{{--									<div class="inner">--}}
{{--										<h4 class="title">Call us</h4>--}}
{{--                    {!!$content[str_slug('Call us')]!!}--}}
{{--										<!-- <a href="#">T: +44 (0) 208 652 7787</a>--}}
{{--										<a href="#">M: +44 (0) 7554 424167</a> -->--}}
{{--									</div>--}}
{{--								</div>--}}
{{--							</li>--}}

                            <li class="col-lg-6" style="width: 48%;" >
								<div class="mainAddress">
									<div class="icon">
										<img src="{{ url('web_assets')}}/img/mail.png" class="img-fluid">
									</div>
									<div class="inner">
										<h4 class="title">Mail us</h4>

                                        {!!$content[str_slug('Mail us')]!!}

										<!-- <a href="mailto:admin@gmail.com">info@streetlife-uk.com</a>
										<a href="mailto:example@gmail.com">example@gmail.com</a> -->
									</div>
								</div>
							</li>

						</ul>
					</div>
				<div>

                <!--<div class="row">
                    <div class="col-lg-4 col-md-6 col-12">
                        <div class="mainAddress">
                            <div class="icon">
                                <svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" height="1em" width="1em">
                                <path d="M21 10c0 7-9 13-9 13s-9-6-9-13a9 9 0 0 1 18 0z"></path>
                                <circle cx="12" cy="10" r="3"></circle>

								</svg>
							</div>
							<div class="inner">
								<h4 class="title">Location</h4>
								<p>The Poppy Factory, 20 Petersham Road, Richmond, Surrey, TW10 6UW</p>
							</div>
						</div>
					</div>
					<div class="col-lg-4 col-md-6 col-12">
						<div class="mainAddress">
							<div class="icon">
								<svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" height="1em" width="1em">
								<path d="M3 18v-6a9 9 0 0 1 18 0v6"></path>
							<path d="M21 19a2 2 0 0 1-2 2h-1a2 2 0 0 1-2-2v-3a2 2 0 0 1 2-2h3zM3 19a2 2 0 0 0 2 2h1a2 2 0 0 0 2-2v-3a2 2 0 0 0-2-2H3z"></path>
							</svg>
							</div>
							<div class="inner">
								<h4 class="title">Contact With Phone Number</h4>
								<p><a href="#">T: +44 (0) 208 652 7787</a></p>
								<p><a href="#">M: +44 (0) 7554 424167</a></p>
							</div>
					</div>
					</div>
					<div class="col-lg-4 col-md-6 col-12">
						<div class="mainAddress">
							<div class="icon">
								<svg stroke="currentColor" fill="none" stroke-width="2" viewBox="0 0 24 24" stroke-linecap="round" stroke-linejoin="round" height="1em" width="1em">
								<path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path>
								<polyline points="22,6 12,13 2,6"></polyline>
								</svg>
							</div>
							<div class="inner">
								<h4 class="title">Email Address</h4>
								<p><a href="mailto:admin@gmail.com">info@streetlife-uk.com</a></p>
								<p><a href="mailto:example@gmail.com">example@gmail.com</a></p>
							</div>
						</div>
					</div>
				</div>
			--></div>
		</section>
@endsection
@section('scripts')
<script type="text/javascript">
  jscolor.presets.default = {
      borderColor:'rgba(236,239,245,0.6)', width:200, height:100, 
      buttonColor:'rgba(70,51,255,1)', shadowColor:'rgba(0,0,0,0.05)'
  };

  $(document).ready(function(){
      // Basic
      $('.dropify').dropify();

      // Translated
      $('.dropify-fr').dropify({
          messages: {
              default: 'Glissez-déposez un fichier ici ou cliquez',
              replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
              remove:  'Supprimer',
              error:   'Désolé, le fichier trop volumineux'
          }
      });

      // Used events
      var drEvent = $('#input-file-events').dropify();

      drEvent.on('dropify.beforeClear', function(event, element){
          return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
      });

      drEvent.on('dropify.afterClear', function(event, element){
          alert('File deleted');
      });

      drEvent.on('dropify.errors', function(event, element){
          console.log('Has Errors');
      });

      var drDestroy = $('#input-file-to-destroy').dropify();
      drDestroy = drDestroy.data('dropify')
      $('#toggleDropify').on('click', function(e){
          e.preventDefault();
          if (drDestroy.isDropified()) {
              drDestroy.destroy();
          } else {
              drDestroy.init();
          }
      })
  });
  

   $(document).ready(function(){

      $('.input').focus(function(){
        $(this).parent().find(".label-txt").addClass('label-active');
      });

      $(".input").focusout(function(){
        if ($(this).val() == '') {
          $(this).parent().find(".label-txt").removeClass('label-active');
        };
      });

    });
</script>
@endsection