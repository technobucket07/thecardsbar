<!DOCTYPE html>
<html lang="en" >
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Profile</title>
        <!--Core CSS -->
        <link href="{{ url('web_assets/card-css/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{ url('web_assets/card-css/css/font-awesome.min.css')}}" rel="stylesheet">
        

        <style>
        
            
/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #f0394d; }
::selection { color: #f1f1f1;background: #f0394d; }



body  {font-family: 'Poppins', sans-serif;background:transparent;}
.wrapper {width: 400px;margin: 0 auto;height: 800px;background: {{$val["primary"]}};  }


.topHeader .firstName {font-size: 38px;line-height: 30px;}
.topHeader .lastName {font-size: 27px;}
.topHeader .designation 
{
    font-size: 17px;
/*color: #c8c8c8;*/
    
}

.abtSec {background:{{$val["primary"]}};padding: 40px 40px 0 40px;border-radius: 36px 36px 0 0;margin: -36px 0 0 0;}
.abtSec .userImg {text-align: right;margin: -92px 0 0 0;}
.abtSec .userImg img {width: 100px;}
.abtSec h1 {
    color: {{$val["text"]}};
    text-transform: uppercase;font-size: 14px;font-weight: 700;}
.abtSec .content {
    color: {{$val["text"]}};
       font-size: 13px;}


.infoSection {  padding: 0 40px;}
.infoSection h2 {
    color:  {{$val["text"]}};
text-transform: uppercase;font-size: 14px;font-weight: 700;margin: 0 0 15px 0;}
.infoSection ul {}
.infoSection ul li {margin: 10px 0;}
.infoSection ul li .contactIcon {float: left;width: 35px;height: 36px;text-align: center;border-radius: 50px;margin: 0 10px 0 0;background:{{$val["scondary"]}}; }
.infoSection ul li .contactIcon i {font-size: 36px;}
.infoSection ul li .contactIcon i.messageIcon {font-size: 24px;line-height: 33px;}
.infoSection ul li .contactInfo {float: left;width: 81%;}
.infoSection ul li .contactInfo h6 {margin: 0 0 4px 0;
             font-weight: 700;font-size: 14px; color:  {{$val["text"]}};
              }
.infoSection ul li .contactInfo p {font-weight: 700;font-size: 16px;margin: 0;}
.infoSection ul li .contactInfo p a {
    /*color: #1e1c26;*/
    
}

.searchMe {padding: 0 40px;}
.searchMe h2 {
    color:  {{$val["text"]}};
    text-transform: uppercase;font-size: 14px;font-weight: 700;margin: 0 0 15px 0;}

.socialMedia {overflow: auto;white-space: nowrap;margin: 0 0 10px 0;}
.socialMedia li {margin: 5px;  background: #1e1c26;border-radius: 8px;}
.socialMedia li .fa {font-size: 30px;text-align: center;text-decoration: none;width: 50px;height: 50px;line-height: 50px;
     color:  {{$val["text"]}};
    
}
.socialMedia li .fa:hover {opacity: 0.7;}

.footer {padding: 0 40px;} 
.footer .webLink {text-align: center;font-size: 14px;font-weight: 600;margin: 0 0 5px 0;}
.footer .webLink a {
    /*color: #1e1c26;*/
    
}
.footer .nameUser {
    /*color: #b9b711;*/
    text-align: center;font-size: 13px;letter-spacing: 7px;}



/* Media Queries */
a{
   color:  {{$val["text"]}}; 
}
p{
    color:  {{$val["text"]}};
}


@media (min-width: 992px) and (max-width: 1199px) {
}

@media (min-width: 768px) and (max-width: 991px) {
}

@media (min-width: 480px) and (max-width: 767px) {
.footer .webLink, .footer .nameUser {text-align: left;}

}
@media (min-width: 150px) and (max-width: 479px) {
}
        </style>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
 <script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script>
    </head>
    <body  >
       <div id="chane">
            <a id="btn-Convert-Html2Image" href="#" class="btn btn-success">Image Download</a>
    <br/>
    <h3>Image Show :</h3>
    <div id="previewImage">
    </div>
       </div>    
   
    
<div id="html-content-holder" >
      <section class="wrapper canvas_div_pdf">

        <!-- HEADER STARTS HERE -->
        <header>
            <section class="topHeader" style="background:{{$val["scondary"]}}; color:{{$val["text"]}};padding: 40px 40px 76px 40px;">
                <div class="firstName">{{$val['first_name']}}</div>
                <div class="lastName">{{$val['last_name']}}</div>
                <div class="designation">{{$val['designation']}}</div>
            </section>
        </header>
        <!-- HEADER END HERE -->

        <!-- ABOUT STARTS HERE -->
        <section class="abtSec">
              @if(isset($val['img']))
            <p class="userImg"><img class="img-fluid" src="{{ url('card/image/'.$val["img"])}}" alt="userimage" style="border-radius: 48px;border:5px solid {{$val['text']}};width:90px;height:90px"></p>
             @endif
             @if(!empty($val['about']))
            <h1>About Me</h1>
            <p class="content">{{$val['about']}}</p>
            @endif
        </section>
        <!-- ABOUT END HERE -->

        <!-- CONTACT STARTS HERE -->
        <section class="infoSection">
                @if(!empty($val['address']))
            <h2>contact Me</h2>
            @endif
            <table>
                 @if(!empty($val['address']))
                <tr>
                    <td style="width:40px;height:40px;">
                        <p style="width:38px;font-size:20px;text-align:center;height:38px;border-radius: 20px;background:{{$val["scondary"]}}">
                        <i class="fa fa-map-marker" aria-hidden="true" style="font-size:30px;margin-left:0px;margin-top:5px;" ></i>
                    </p>
                     </td>
                     <td>
                      <div class="contactInfo">
                          <h6 style="font-weight: 700;font-size: 14px;color:{{$val["text"]}}">Location</h6>
                          <p style="font-weight: 700;font-size: 16px;">{{$val['address']}}</p>
                     </div>
                  </td>
                </tr>
                   @endif
                     @if(!empty($val['phone']))
               
                    <tr>
                    <td style="width:40px;height:40px;">
                        <p style="width:38px;font-size:20px;text-align:center;height:38px;border-radius: 20px;background:{{$val["scondary"]}}">
                        <i class="fa fa-mobile" aria-hidden="true" style="font-size:30px;margin-left:1px;margin-top:5px;"></i>
                    </p>
                    </td>
                    <td>
                      <div class="contactInfo">
                        <h6 style="font-weight: 700;font-size: 14px;color:{{$val["text"]}}">Phone Number</h6>
                        <p style="font-weight: 700;font-size: 16px;"><a href="#">{{$val['phone']}}</a></p>
                      </div>
                   </td>
                </tr>
                  @endif
                   @if(!empty($val['email']))
                  <tr>
                    <td style="width:40px;height:40px;">
                        <p style="width:38px;font-size:20px;text-align:center;height:38px;border-radius: 20px;background:{{$val["scondary"]}}">
                        <i class="fa fa-envelope" aria-hidden="true" style="font-size:20px;margin-left:1px;margin-top:6px;"></i>
                    </p>
                   </td>
                   <td> 
                    <div class="contactInfo">
                        <h6 style="font-weight: 700;font-size: 14px;color:{{$val["text"]}}">Email Address</h6>
                        <p style="font-weight: 700;font-size: 16px;"><a href="#">{{$val['email']}}</a></p>
                    </div>
                  </td>
                </tr>
                @endif
            </table>
        </section>
        <!-- CONTACT END HERE -->



        <!-- CONTACT STARTS HERE -->
        @if(isset($val['social_media']))
        <section class="searchMe">
             @if(!empty($val['social_media'][1]))
            <h2>search Me @</h2>
            @endif
            <table colspan=>
                <tr>
                 @foreach($val['social_media'] as $key=>$media)
                  @if(!empty($media))
                 <td style="width:65px;height:40px;">
                     <p style="width:50px;font-size:20px;background:black;text-align:center;height:40px;border-radius: 10px;padding-top:5px;">
                     <a href="{{$media}}" class="fa {{$val['socialMediaList'][$key]}}" style="text-align: center;font-size: 31px;top:20px;margin-left:5px;"></a></p>
                     </td>
                   @endif
                @endforeach
               </tr>
            </table>
        </section>
        @endif
        <!-- CONTACT END HERE -->


        <!-- FOOTER STARTS HERE -->
        <footer>
            <section class="footer" >
                 <table>
                 @if(!empty($val['website']))
              
                     <tr>
                         <td></td>
                          <td></td>
                         <td style="width:600px;"><p class="webLink" >
                            <a href="#" style="text-align:center;font-weight: 700;font-size: 14px;height:200px;"><i class="fa fa-globe" aria-hidden="true"></i> &nbsp; {{$val['website']}}</a>  </p>
                        </td>
                     </tr>
                     
                @endif
             
                 <tr>
                         <td></td>
                          <td></td>
                         <td style="width:600px;text-align:center;">   <p class="nameUser;font-weight: 700;font-size: 10px;" >{{$val['first_name']}} {{$val['last_name']}}</p>
                        </td>
                     </tr>
                 </table>
            </section>
            
        </footer>
        <!-- FOOTER END HERE -->
</section>
</div>

   
    
   
     

<script>
$(document).ready(function(){
	$('#chane').hide();
	view();
var element = $("#html-content-holder"); // global variable
var getCanvas; // global variable
 
    function view() {
         html2canvas(element, {
         onrendered: function (canvas) {
                $("#previewImage").append(canvas);
                getCanvas = canvas;
             }
         });
         	$('#chane').show();
    }

	$("#btn-Convert-Html2Image").on('click', function () {
         var imgageData = getCanvas.toDataURL("image/png");
       // Now browser starts downloading it instead of just showing it
        var newData = imgageData.replace(/^data:image\/png/, "data:application/octet-stream");
        $("#btn-Convert-Html2Image").attr("download", "Download-cards.png").attr("href", newData);
	});

    $('.wrapper').hide();
});

</script>

</body>
</html>