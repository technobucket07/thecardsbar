@extends('frontend.layouts.master')
@section('css')
    <link href="{{ url('web_assets/css/form-wizard.css') }}" rel="stylesheet">
    <link href="{{ url('web_assets/css/dropify.css') }}" rel="stylesheet">
@endsection
@section('content')
    <!-- BACK BUTTON STARTS HERE -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="backBtn">
                        <a class="btnCustomStyle2 btn-solid" href="index.html">
                            <span>Back</span>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- BACK BUTTON END HERE -->

    <section class="upperCircles">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="progressbar list-unstyled">
                        <li class="active"></li>
                        <li></li>
                        <li></li>
                        <li></li>
                        <li></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- FORM WIZARD STARTS HERE -->
    <section class="mainFormWizard">
        <div class="container-fluid">
            <span id="theme_name" style="display: none;">sky</span>
            <span id="trans_name" style="display: none;">slide</span>
            <span id="rtl" style="display: none;">0</span>
            <div class="row">
                <div class="col-md-9">

                    <form id="wizard_example_2" action="{{url('api/convert/card')}}" method="post" enctype="multipart/form-data">
                        <fieldset>
                            <legend>Color</legend>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>First Color</label>
                                            <input data-jscolor="{value:'#f9004d'}" name="color1">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Second Color</label>
                                            <input data-jscolor="{value:'#7367f0'}" name="color2">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Third Color</label>
                                            <input data-jscolor="{value:'#28c76f'}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Fourth Color</label>
                                            <input data-jscolor="{value:'#B7295C'}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Fifth Color</label>
                                            <input data-jscolor="{value:'#ea5455'}">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Sixth Color</label>
                                            <input data-jscolor="{value:'#ff9f43'}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Background</legend>
                            <div class="row">
                                <div class="col-md-12 col-lg-6">
                                    <div class="form-group">
                                        <div class="uploadBackground">
                                            <label>First Background</label>
                                            <input type="file" id="input-file-now" class="dropify" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-6">
                                    <div class="form-group">
                                        <div class="uploadBackground">
                                            <label>Second Background</label>
                                            <input type="file" id="input-file-now" class="dropify" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-6">
                                    <div class="form-group">
                                        <div class="uploadBackground">
                                            <label>Third Background</label>
                                            <input type="file" id="input-file-now" class="dropify" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-lg-6">
                                    <div class="form-group">
                                        <div class="uploadBackground">
                                            <label>Fourth Background</label>
                                            <input type="file" id="input-file-now" class="dropify" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Social Links</legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <ul class="list-unstyled socialLinksboxx">
                                        @foreach($socialMedia as $social)
                                            <li class="clearfix">
                                                 <span class="titleSocial">
                                                     <i class="fa {{$social['icon']}}" aria-hidden="true"></i>{{$social['name']}}
                                                 </span>
                                                 <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link...">
                                                 </span>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Profile </legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        Personal Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                <div class="panel-body personalBoxxx">
                                                    <div class="row">
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                <input type="email" class="form-control" name="first_name" placeholder="First Name">
                                                                <small class="text-muted">Max 30 words</small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                <input type="email" class="form-control" placeholder="Last Name" name="last_name">
                                                                <small class="text-muted">Max 30 words</small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                <input type="email" class="form-control" placeholder="Email" name="email">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Designation" name="designation">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <textarea class="form-control" rows="3" name="about" placeholder="About me Description"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <p class="backBtn">
                                                                <a class="btnCustomStyle2 btn-solid" href="#">
                                                                    <span>Save</span>
                                                                </a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingTwo">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                        Business Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                                <div class="panel-body businessInformation">
                                                    <div class="row">

                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                {{Form::text('business_name', null, array('class'=>'form-control', 'placeholder'=>'Buniness Name'))}}
                                                                <small class="text-muted">Max 30 words</small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                {{Form::text('tagline', null, array('class'=>'form-control', 'placeholder'=>'Tagline'))}}
                                                                <small class="text-muted">Max 30 words</small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                <input type="email" class="form-control" placeholder="Email">

                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                {{Form::text('website', null, array('class'=>'form-control', 'placeholder'=>'Website'))}}
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Logo</label>
                                                                    <input type="file" name="logo" id="input-file-now" class="dropify" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="addressBoxxxMain">
                                                                    <label>Address (Default)</label>
                                                                    <ul class="list-unstyled">
                                                                        <li class="clearfix">
                                                                            <span>Markfed Entrance, PAP Complex, Jalandhar, Punjab 144008, India</span>
                                                                                        <span>
                                                                                            <a href="#" data-toggle="modal" data-target="#addressBar">Edit Address</a>
                                                                                        </span>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <p class="backBtn">
                                                                <a class="btnCustomStyle2 btn-solid" href="#">
                                                                    <span>Save</span>
                                                                </a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingThree">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                                        Products & Services
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                                <div class="panel-body businessInformation">

                                                    <div class="row">
                                                        @for($i=1; $i<=6; $i++)
                                                        <div class="col-md-12">
                                                            <h6 class="text-red">Service {{$i}}</h6>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                {{Form::text('service_name[]', null, array('class'=>'form-control', 'placeholder'=>'Product Title'))}}
                                                                 <small class="text-muted">Max 30 words</small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                {{Form::text('description[]', null, array('class'=>'form-control', 'placeholder'=>'Description'))}}
                                                                <small class="text-muted">Max 30 words</small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Logo</label>
                                                                    <input type="file" name="service_logo[]" id="input-file-now" class="dropify" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        @endfor



                                                        <div class="col-md-12">
                                                            <p class="backBtn">
                                                                <a class="btnCustomStyle2 btn-solid" href="#">
                                                                    <span>Save</span>
                                                                </a>
                                                            </p>
                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingfour">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                                        Contact Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                                <div class="panel-body businessInformation">

                                                    <div class="row">

                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                <input type="text" name="phone" class="form-control" placeholder="Primary Number">

                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                <input type="email" class="form-control" placeholder="Secondary Number">

                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                <input type="email" class="form-control" placeholder="Whats app Number">

                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                <input type="email" class="form-control" placeholder="Landline Number">

                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Address" name="address">

                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Website" name="website">

                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <p class="backBtn">
                                                                <a class="btnCustomStyle2 btn-solid" href="#">
                                                                    <span>Save</span>
                                                                </a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingfive">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefive" aria-expanded="false" aria-controls="collapsefive">
                                                        Other Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapsefive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfive">
                                                <div class="panel-body personalBoxxx">

                                                    <div class="row">

                                                        <div class="col-md-12 col-lg-4">
                                                            <div class="form-group">
                                                                <input type="email" class="form-control" placeholder="Qualification">

                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-4">
                                                            <div class="form-group">
                                                                <input type="email" class="form-control" placeholder="Streams">

                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 col-lg-4">
                                                            <div class="form-group">
                                                                <input type="email" class="form-control" placeholder="Year">

                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <textarea class="form-control" rows="3" placeholder="Hobbies"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <textarea class="form-control" rows="3" placeholder="About me Description"></textarea>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <p class="backBtn">
                                                                <a class="btnCustomStyle2 btn-solid" href="#">
                                                                    <span>Save</span>
                                                                </a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
                <div class="col-md-3">
                    <div class="feature-phone-image">
                        <img class="img-fluid mx-auto d-block" src="{{ url('web_assets')}}/img/features.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- FORM WIZARD END HERE -->
@endsection

@section('scripts')
    <script src="{{ url('web_assets/js/form-wizard.js') }}"></script>
    <script src="{{ url('web_assets/js/jscolor.js') }}"></script>
    <script src="{{ url('web_assets/js/dropify.js') }}"></script>
    <script>
        jscolor.presets.default = {
            borderColor:'rgba(236,239,245,0.6)', width:200, height:100,
            buttonColor:'rgba(70,51,255,1)', shadowColor:'rgba(0,0,0,0.05)'
        };
    </script>

    <script>
        $(document).ready(function(){
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            })
        });
    </script>

    <script>

    </script>
@endsection