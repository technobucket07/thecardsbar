<div class="row">
<div class="col-md-12 col-lg-6">
    <div class="form-group ">
        <input type="number" class="form-control" name="orderid" placeholder="Enter Order ID" value="@if(request()->get('oid')){{request()->get('oid')}}@endif">
        <small class="text-muted1 orderid"></small>
    </div>
</div>
<div class="col-md-12 col-lg-6">
    <div class="form-group ">
        <input type="number" class="form-control" name="userid" placeholder="Enter User ID" value="@if(request()->get('uid')){{request()->get('uid')}}@endif">
        <small class="text-muted1 userid"></small>
    </div>
</div>
</div>