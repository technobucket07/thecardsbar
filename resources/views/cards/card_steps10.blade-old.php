@extends('cards.layouts.master')
@section('css')
         <link href="{{ url('web_assets/css/form-wizard.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/css/dropify.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/bootstrap.min.css')}}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/font-awesome.min.css')}}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/main.css')}}" rel="stylesheet">
         <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/demo/prism.css" />
         <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/bower_components/sweetalert/dist/sweetalert.css" />
	     <link rel="Stylesheet" type="text/css" href="{{ url('web_assets/js/style1.css?abc')}}" />
	     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	     <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
		 <script src="https://foliotek.github.io/Croppie/croppie.js"></script>
		 <script src="{{ url('web_assets/js/crop_js/script10.js')}}"></script>
       
       
    <style>
        .text-muted1 {
     color: red;
        }
        .progressbar li {
         
        }
        
        #loader { 
      border: 12px solid #f3f3f3; 
      border-radius: 50%; 
      border-top: 12px solid #444444; 
      width: 70px; 
      height: 70px; 
      animation: spin 1s linear infinite; 
    } 
    
    @keyframes spin { 
      100% { 
        transform: rotate(360deg); 
      } 
    } 
    
    .center { 
      position: absolute; 
      top: 0; 
      bottom: 0; 
      left: 0; 
      right: 0; 
      margin: auto; 
    } 
    

    </style>     
    
       
@section('content')
    <!-- BACK BUTTON STARTS HERE -->


    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="backBtn">
                        <a class="btnCustomStyle2 btn-solid" href="index.html">
                            <span>Back</span>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- BACK BUTTON END HERE -->

    <section class="upperCircles">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="progressbar list-unstyled" style="width: 100%;">
                        <li class="active" style=" width: 33%;"></li>
                        <li id="active1"  style=" width: 33%;"></li>
                        <li id="active2"  style=" width: 33%;"></li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- FORM WIZARD STARTS HERE -->
    <section class="mainFormWizard">
        <div class="container-fluid">
            <span id="theme_name" style="display: none;">sky</span>
            <span id="trans_name" style="display: none;">slide</span>
            <span id="rtl" style="display: none;">0</span>
            <div class="row">
                <div class="col-md-9">
    <ul>
            @foreach ($errors->all() as $error)
           
              <li> <div class="alert alert-warning">
               <strong>Warning!</strong> {{ $error }}
              </div></li>
            @endforeach
        </ul>
                    <form id="wizard_example_2"  method="post" enctype="multipart/form-data" >
                       
                        @csrf
                        <fieldset>
                            <legend>Color</legend>
                            <div class="row">
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Background color 1</label>
                                             <input type="color" class="form-control" id="Primary" name="color1" style="height:80px;width:200px;" value="#ffffff">

                                           
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Social Media Background Color</label>
                                             <input type="color" class="form-control" id="Secondary" name="color2" style="height:80px;width:200px;" value="#ecf2fb">
                                        
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Background color 2</label>
                                               <input type="color" class="form-control" id="text" name="color3" style="height:80px;width:200px;" value="#121520">
                                         
                                        </div>
                                    </div>
                                </div>
                                
                                
                               
                                
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Blog background Color</label>
                                             <input type="color" class="form-control" id="heading2" name="color4" style="height:80px;width:200px;" value="#cbccd0">
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Social Media Color</label>
                                             <input type="color" class="form-control" id="heading1" name="color5" style="height:80px;width:200px;" value="#2d6ff7">
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>2 Background Font color</label>
                                             <input type="color" class="form-control" id="heading1" name="color6" style="height:80px;width:200px;" value="#afd41e">
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                
                               <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Name Font color</label>
                                             <input type="color" class="form-control" id="heading1" name="color7" style="height:80px;width:200px;" value="#121520">
                                        </div>
                                       
                                    </div>
                                </div>
                               
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Designation Color</label>
                                             <input type="color" class="form-control" id="heading1" name="color8" style="height:80px;width:200px;" value="#81869a">
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                
                               
                            </div>
                        </fieldset>
                      
                        <fieldset>
                            <legend>Social Links</legend>
                            @include('cards.socialmedia')
                            <!-- <div class="row">
                                <div class="col-md-12">
                                    <span class="col" style="color:red;font-size:16px;"></span>
                                    <ul class="list-unstyled socialLinksboxx">
                                        @foreach($socialMedia as $social)
                                            <li class="clearfix">
                                                 <span class="titleSocial" >
                                                     <i class="fa {{$social['icon']}}" aria-hidden="true"></i>{{$social['name']}}
                                                 </span>
                                                 @if($social['icon']=='fa-whatsapp')
                                              
                                                 <select class="form-control" name="code" style="width: 78px; height: 29px;float: left;margin-top: 0px;font-size: 12px;margin-left: 63px;" id="code">
                                                    @foreach($va as $va1)
                                                   <option value="{{$va1->callingCode}}">{{$va1->callingCode}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$va1->country}} </option>
                                                    @endforeach
                                                </select>
                                                 <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="7484564564">
                                                 </span>
                                                   @elseif($social['icon']=='fa-facebook')
                                                  <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.facebook.com">
                                                 </span>
                                                 
                                                 @elseif($social['icon']=='fa-skype')
                                                  <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.skype.com">
                                                 </span>
                                                 
                                                
                                                  
                                                   @elseif($social['icon']=='fa-linkedin')
                                                    <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.linkedin.com">
                                                 </span>
                                                    @elseif($social['icon']=='fa-twitter')
                                                   <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://twitter.com">
                                                 </span>
                                                   @endif
                                                
                                            </li>
                                             
                                        @endforeach
                                    </ul>
                                </div>
                            </div> -->
                        </fieldset>
                        
                        
                      <fieldset>
                            <legend>Profile </legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        
                                              <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne1">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        Image Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne1">
                                                
                                                <div class="panel-body personalBoxxx">
                                                    <div class="row">
                                                        
                                                        
                                                          <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Profile Image</label>
                                                                    <!--<input type="file" name="profile_image" id="input-file-now" class="dropify" />-->
                                                                    
                                                                     <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload1"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="profile_image" id="base64image1">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap1">
                                                                         <div id="upload-demo1"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>logo Image</label>
                                                                    <!--<input type="file" name="logo" id="input-file-now1" class="dropify" />-->
                                                                    
                                                                <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload2"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="logo" id="base64image2">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap2">
                                                                         <div id="upload-demo2"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                    
                                                                    
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                          
                                                        <!-- <div class="col-md-12 col-lg-12 ">-->
                                                        <!--    <div class="form-group">-->
                                                        <!--        <input type="text" name="bottom_title" class="form-control" placeholder="Bottom Title " maxlength="25">-->
                                                        <!--            <small class="text-muted1 bottom_title"></small>-->
                                                        <!--    </div>-->
                                                        <!--</div>-->
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
                                                        Personal Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                
                                                <div class="panel-body personalBoxxx">
                                                  @include('cards.createfor')
                                                    <div class="row">
                                                        
                                                        
                                                          
                                                        
                                                        
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" name="first_name" placeholder="Enter First Name " value="JACKIE">
                                                                <small class="text-muted1 first_name1"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Enter Last Name" name="last_name" value="SIRONADAMS">
                                                                <small class="text-muted1 last_name"></small>
                                                            </div>
                                                        </div>
                                                        
                                                       
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Designation" name="designation" maxlength="40" value="Chartered Accountant">
                                                                 <small class="text-muted1 designation"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" name="phone" class="form-control" placeholder="Phone Number " maxlength="10" value="7345738457">
                                                                    <small class="text-muted1 phone1"></small>
                                                            </div>
                                                        </div>
                                                       
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" name="email" class="form-control" placeholder="Enter E-mail" value="developertest.nomail@gmail.com">
                                                                    <small class="text-muted1 email"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                         
                                                         
                                                         
                                                          
                                                         
                                                      
                                                       <div class="col-md-12 col-lg-12 ">
                                                            <div class="form-group">
                                                                <textarea type="text" name="address" id="address" class="form-control" placeholder="Enter Address " maxlength="78">14 Sti Kilda Road, New York, USA, 14 Sti Kilda Road, New York, USA,14 Sti</textarea>
                                                                    <small class="text-muted1 address"></small>
                                                            </div> 
                                                        </div>
                                                      <div class="col-md-12 col-lg-12">
                                                        <div class="form-group ">
                                                          <input type="text" id="autocomplete" class="form-control" placeholder="Search your location" value="" onFocus="geolocate()">
                                                           <small class="text-muted1 address"></small>
                                                          <input type="hidden" name="location" id="latlong" value="">
                                                        </div>
                                                      </div>
                                                     
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
           
                       
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingfour">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                                        Service Name
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                                <div class="panel-body businessInformation">

                                                    <div class="row">

                                                       
                                                        
                                                     
                                                        
                                                         <div class="col-md-12 col-lg-12">
                                                            <div class="form-group">
                                                                <label>Service Name 1</label>
                                                                  <!--<input type="file" name="service-image-1" id="input-file-now2" class="dropify" />-->
                                                                  
                                                                         
                                                                <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload3"  value="{{url('web_assets/design-image/design-10/profilepix.jpg')}}" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="serviceimage1" id="base64image3">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap3">
                                                                         <div id="upload-demo3"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                  
                                                                  
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 1" name="service-name-1" maxlength="25" value="Income Tax">
                                                                 <small class="text-muted1 service-name-1"></small>
                                                            </div>
                                                        </div>
                                                      
                                                        
                                                         <div class="col-md-12 col-lg-12">
                                                            <div class="form-group">
                                                                <label>Service Name 2</label>
                                                                 <!--<input type="file" name="service-image-2" id="input-file-now3" class="dropify" />-->
                                                                  <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload4"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="serviceimage2" id="base64image4">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap4">
                                                                         <div id="upload-demo4"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                 
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 2" name="service-name-2" maxlength="20" value="Corporate Tax">
                                                                 <small class="text-muted1 service-name-2"></small>
                                                            </div>
                                                        </div>
                                                       
                                                         <div class="col-md-12 col-lg-12">
                                                            <div class="form-group">
                                                                <label>Service Name 3</label>
                                                                 <!--<input type="file" name="service-image-3" id="input-file-now4" class="dropify" />-->
                                                                 
                                                                   <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload5"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="serviceimage3" id="base64image5">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap5">
                                                                         <div id="upload-demo5"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                 
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 3" name="service-name-3" maxlength="20" value="Accounting Services">
                                                                 <small class="text-muted1 service-name-3"></small>
                                                            </div>
                                                        </div>
                                                       
                                                        
                                                         <div class="col-md-12 col-lg-12">
                                                            <div class="form-group">
                                                                <label>Service Name 4</label>
                                                                 <!--<input type="file" name="service-image-4" id="input-file-now5" class="dropify" />-->
                                                                 
                                                                 <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload6"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="serviceimage4" id="base64image6">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap6">
                                                                         <div id="upload-demo6"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                 
                                                                 
                                                                 
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 4" name="service-name-4" maxlength="20" value="Audit & Assurance">
                                                                 <small class="text-muted1 service-name-4"></small>
                                                            </div>
                                                        </div>
                                                        
                                                            <div class="col-md-12 col-lg-12">
                                                            <div class="form-group">
                                                                <label>Service Name 5</label>
                                                                 <!--<input type="file" name="service-image-5" id="input-file-now6" class="dropify" />-->
                                                                 
                                                                  
                                                                 <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload7"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="serviceimage5" id="base64image7">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap7">
                                                                         <div id="upload-demo7"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                 
                                                                 
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 5" name="service-name-5" maxlength="20" value="Project Financing">
                                                                 <small class="text-muted1 service-name-5"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                           <div class="col-md-12 col-lg-12 ">
                                                            <div class="form-group">
                                                                <label>Service Name 6</label>
                                                                 <!--<input type="file" name="service-image-6" id="input-file-now7" class="dropify" />-->
                                                                 
                                                                   <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload8"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="serviceimage6" id="base64image8">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap8">
                                                                         <div id="upload-demo8"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                 
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 6" name="service-name-6" maxlength="20" value="Payroll Accounting">
                                                                 <small class="text-muted1 service-name-6"></small>
                                                            </div>
                                                        </div>
                                                        
                                                       
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                             
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                            <div class="col-md-12">
                                <p class="backBtn">
                                    <a class="btnCustomStyle2 btn-solid Preview1 Preview2 Preview3 Preview4 Preview5 Preview6 Preview7 Preview8" href="javascript:void(0);">
                                        <span>Preview</span>
                                    </a>
                                </p>
                            </div>
                          
                       </form>
                    </div>
                <div class="col-md-3">
                    
                    <div id="frame" style="background-image: url('{{ url("web_assets")}}/img/Phone1.png');background-color: #cccccc;background-position: center;background-repeat: no-repeat;background-size: cover;
  position: relative;width: 263px;height: 528px;border-radius: 30px;"></br>
                         <div id="previewImage" ></div>
                         <div id="loader" class="center" ></div> 
                          <div id="img" style="width: 235px;border-radius: 30px;margin-left: 15px;margin-top: -9px;">
                         <img class="img-fluid mx-auto d-block "  src="{{ url('web_assets')}}/img/desine-10.png" alt="" style="border-radius: 15px;height:500px;">
                    </div>
                  
                    </div>
                   
      
                   
                   
                </div>
                
    <style>

/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #f0394d; }
::selection { color: #f1f1f1;background: #f0394d; }



body  {font-family: 'Poppins', sans-serif;background: #000;}
.wrapper {width: 400px;margin: 0 auto;height: 810px;background: #fff;}

.topBar {padding: 30px;height: 387px;}
.logoWithinfo .logo {}
.logoWithinfo .logo img {}

.logoWithinfo .rightTop {}
.logoWithinfo .rightTop .callUs{font-size: 12px;
color: #2d6ff7;
margin: 0;
font-weight: 600;}
.logoWithinfo .rightTop .mobileNo{font-size: 16px;
font-weight: 700;
margin: 0;
line-height: 16px;color: #121520;}


.profileBoxxx {    margin: 30px 0;}
.profileBoxxx img{border-radius: 10px;}
.profileBoxxx h1{    color: #121520;
    font-size: 18px;
    font-weight: 700;
    margin: 15px 0 5px 0;}
.profileBoxxx p{    font-size: 12px;
    color: #81869a;
    margin: 0;}


.socialMediaIcons {}
.socialMediaIcons ul{margin: 0;}
.socialMediaIcons ul li {display: inline-block;margin: 0 7px;}
.socialMediaIcons ul li a {  
 
    font-size: 30px;
    width: 50px;
    height: 50px;
    line-height: 50px;
    text-align: center;
    text-decoration: none;
    border-radius: 25px;
    display: inline-block;
    background: #ecf2fb;
    color: #2d6ff7;

}
.socialMediaIcons ul li a .fa {
    font-size: 26px;
    margin-top: 12px;
}

.socialMediaIcons ul li a .fa:hover {
    opacity: 0.7;
}




.bottomBar {
    background: #121520;
    padding: 20px 40px 0 40px;
    border-radius: 36px 36px 0 0;
        height: 424px;
}
.bottomBar .servicesBoxx {}
.bottomBar .servicesBoxx .col-md-4{    padding-right: 7.5px;
    padding-left: 7.5px;}



.bottomBar .servicesBoxx h2 {    color: #f5f6f7;
    font-size: 16px;
    font-weight: 600;
    margin: 0 0 15px 0;}
.user-card {       background: #1e2234;
    border-radius: 10px;
        width: 100px;
        margin: 0 0 15px 0;}
.user-card p {    background: #2d6ff7;
    border-radius: 10px;
    margin: 0;
    padding: 10px;}    
.user-card h4 {     color: #cbccd0;
    font-size: 12px;
    padding: 10px;}


.footerLinks {}
.footerLinks ul {}
.footerLinks ul li {display: inline-block;
    float: left;}
.footerLinks ul li.first {width: 16%;}
.footerLinks ul li.first a {
     font-size: 30px;
    width: 50px;
    height: 50px;
    line-height: 50px;
    text-align: center;
    text-decoration: none;
    border-radius: 25px;
    display: inline-block;
    background: #2d6ff7;
    color: #ecf2fb;}
.footerLinks ul li.first a .fa {  font-size: 26px; margin-top:13px;}

.footerLinks ul li.second{width: 67%;}
.footerLinks ul li.second p {    margin: 0;}
.footerLinks ul li.second p a {color: #f5f6f7;font-size: 11px;}
.footerLinks ul li.second p a i {     margin: 0 5px 0 0;
    width: 18px;
    font-size: 15px;
    text-align: center;}

.footerLinks ul li.third{width: 16%;text-align: right;}
.footerLinks ul li.third a {
    font-size: 30px;
    width: 50px;
    height: 50px;
    line-height: 50px;
    text-align: center;
    text-decoration: none;
    border-radius: 25px;
    display: inline-block;
    background: #2d6ff7;
    color: #ecf2fb;}
.footerLinks ul li.third a .fa {  font-size: 26px; margin-top:13px;}















/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


}

        </style>
     
    <div style="opacity:0;">      
<div id="html-content-holder">
 <section class="wrapper" id="wrapper">

        
                <div class="topBar">

                    <div class="logoWithinfo">
                        <ul class="list-unstyled clearfix m-0">
                            <li class="logo w-50 pull-left">
                               
                            </li>
                            <li class="rightTop w-50 pull-left text-right">
                                
                            </li>
                        </ul>
                    </div>

                    <div class="profileBoxxx">
                         
                    </div>

                    <div class="socialMediaIcons">
                        <ul class="list-unstyled" align="center">
                           
                            
                        </ul>
                    </div>

                </div>
        
                <div class="bottomBar">
                    <div class="servicesBoxx">
                        <h2 class="text-center">Our Services</h2>
                        <div class="row">
                       
                        </div>


                        <div class="footerLinks">
                            <ul class="list-unstyled">
                                <li class="first">
                                    
                                </li>
                                <li class="second">
                                    
                                </li>
                                <li class="third">
                                    
                                </li>
                            </ul>
                        </div>






                    </div>
                </div>




        </section>

         
  
    </div> 
  </div>            
            
            
            
        </div>
        

        
<script>
demoUpload1();
demoUpload2();
demoUpload3();
demoUpload4();
demoUpload5();
demoUpload6();
demoUpload7();
demoUpload8();
</script>
        
        
    </section>
    <!-- FORM WIZARD END HERE -->
@endsection

@section('scripts')
    <script src="{{ url('web_assets/js/html2canvas.js') }}"></script>
    <script src="{{ url('web_assets/js/form-wizard.js') }}"></script>
    <script src="{{ url('web_assets/js/jscolor.js') }}"></script>
    <script src="{{ url('web_assets/js/dropify.js') }}"></script>
     
<style>
    canvas {
    height: 502px;
    width: 238px;
    margin-top: -10px;
    border-radius: 27px;
    margin-left: 13px;

    }
</style>
    <script>
        jscolor.presets.default = {
            borderColor:'rgba(236,239,245,0.6)', width:200, height:100,
            buttonColor:'rgba(70,51,255,1)', shadowColor:'rgba(0,0,0,0.05)'
        };
    </script>

    <script>
        $(document).ready(function(){
            	$('#myModal').hide();
            	
            	
            // 	 $('.btnCustomStyle2').hide();
            	 $('#code').val('+91');
            	 $('#loader').hide(); 
            	 $('#wrapper').hide();
                 $('.Address').hide();
                 $('.servicesBoxx h2').hide();
            view();
            $('input[type="color"]').change(function(){
              $('.btnCustomStyle2').show();
            });
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });
           
          
               
             $('.btnCustomStyle2').click(function(){
                     $('#wrapper').show();
                  	$('#loader').show();
              
            
              
                var form_data = new FormData($('#wizard_example_2')[0]);
                 
                    
   
                       $.ajax({
                            type: 'post',
                            url:  "{{route('admin.Cards.cards10')}}",
                            data: form_data,
                            async: false,
                            dataType:'json',
                            success: function (data1) {
                            var    data=data1.va;
                                console.log(data);
                                if(data.profile_image!==null && data.first_name!==null && data.last_name!==null) {
                                   
                                     $('.profileBoxxx').html('<img class="img-fluid d-block mx-auto" src="'+data.profile_image+'" alt="ca-profile"><h1 class="text-center" style="color:'+data.color7+';">'+data.first_name+' '+data.last_name+'</h1><p class="text-center" style="color:'+data.color8+';">'+data.designation+'</p>');
                                    }else{
                                    var img1 ="{{url('/web_assets/design-image/design-10/ca-profile.png')}}";
                                     $('.profileBoxxx').html('<img class="img-fluid d-block mx-auto" src="'+img1+'" alt="ca-profile"><h1 class="text-center" style="color:'+data.color7+';">'+data.first_name+' '+data.last_name+'</h1><p class="text-center" style="color:'+data.color8+';">'+data.designation+'</p>');
                                    }
                                if(data.logo!==null){
                                   
                                     $('.logo').html(' <img class="img-fluid" src="'+data.logo+'" alt="ca-logo">');
                                 }else{
                                    var img2 = "{{url('/web_assets/design-image/design-10/ca-logo.png')}}";
                                     $('.logo').html(' <img class="img-fluid" src="'+img2+'" alt="ca-logo">');
                                 }
                                 
                                if(data.phone!==null){
                                 
                                    $('.rightTop').html('<p class="callUs" style="color:'+data.color5+';">Call Us</p><p class="mobileNo" style="color:'+data.color7+';">'+data.code+data.phone+'</p>');
                                    $('.footerLinks .list-unstyled .first').html('<a href="#" style="background:'+data.color5+';color:'+data.color6+';"><i class="fa fa-phone" aria-hidden="true"></i></a>');
                                }
                            var html1='';
                                if(data.servicename1!==null  && data.serviceimage1!==null){
                                 $('.servicesBoxx h2').show();
                                 $('.servicesBoxx h2').css('color',data.color6);
                                  
                                 html1+='<div class="col-md-4"><div class="user-card" style="background:'+data.color4+';"><p style="background:'+data.color5+';"><img src="'+data.serviceimage1+'" alt="ser-1" class="img-fluid d-block mx-auto" style="height:55px"/></p><h4 style="color:'+data.color6+';">'+data.servicename1+'</h4></div></div>';
                                }else{
                                 $('.servicesBoxx h2').show();
                                 $('.servicesBoxx h2').css('color',data.color6);

                                  var img3 = "{{url('/web_assets/design-image/design-10/ser-1.png')}}";

                                 html1+='<div class="col-md-4"><div class="user-card" style="background:'+data.color4+';"><p style="background:'+data.color5+';"><img src="'+img3+'" alt="ser-1" class="img-fluid d-block mx-auto" style="height:55px"/></p><h4 style="color:'+data.color6+';">'+data.servicename1+'</h4></div></div>';
                                }
                                 
                                  if(data.servicename2!==null && data.serviceimage2!==null){
                                   html1+='<div class="col-md-4"><div class="user-card" style="background:'+data.color4+'"><p style="background:'+data.color5+';"><img src="'+data.serviceimage2+'" alt="ser-1" class="img-fluid d-block mx-auto" style="height:55px"/></p><h4 style="color:'+data.color6+';">'+data.servicename2+'</h4></div></div>';
                                  
                                }else{
                                  var img4 = "{{url('/web_assets/design-image/design-10/ser-2.png')}}";

                                   html1+='<div class="col-md-4"><div class="user-card" style="background:'+data.color4+'"><p style="background:'+data.color5+';"><img src="'+img4+'" alt="ser-1" class="img-fluid d-block mx-auto" style="height:55px"/></p><h4 style="color:'+data.color6+';">'+data.servicename2+'</h4></div></div>';
                                  
                                }
                                 
                                  if(data.servicename3!==null && data.serviceimage3!==null){

                                   html1+='<div class="col-md-4"><div class="user-card" style="background:'+data.color4+'"><p style="background:'+data.color5+';"><img src="'+data.serviceimage3+'" alt="ser-1" class="img-fluid d-block mx-auto" style="height:55px"/></p><h4 style="color:'+data.color6+';">'+data.servicename3+'</h4></div></div>';
                                }else{
                                  var img5 = "{{url('/web_assets/design-image/design-10/ser-3.png')}}";

                                   html1+='<div class="col-md-4"><div class="user-card" style="background:'+data.color4+'"><p style="background:'+data.color5+';"><img src="'+img5+'" alt="ser-1" class="img-fluid d-block mx-auto" style="height:55px"/></p><h4 style="color:'+data.color6+';">'+data.servicename3+'</h4></div></div>';
                                }
                                
                                 if(data.servicename4!==null && data.serviceimage4!==null){

                                   html1+='<div class="col-md-4"><div class="user-card" style="background:'+data.color4+'"><p style="background:'+data.color5+';"><img src="'+data.serviceimage4+'" alt="ser-1" class="img-fluid d-block mx-auto" style="height:55px"/></p><h4 style="color:'+data.color6+';">'+data.servicename4+'</h4></div></div>';
                                }else{
                                  var img6 = "{{url('/web_assets/design-image/design-10/ser-4.png')}}";
                                   html1+='<div class="col-md-4"><div class="user-card" style="background:'+data.color4+'"><p style="background:'+data.color5+';"><img src="'+img6+'" alt="ser-1" class="img-fluid d-block mx-auto" style="height:55px"/></p><h4 style="color:'+data.color6+';">'+data.servicename4+'</h4></div></div>';
                                }
                                
                                 if(data.servicename5!==null && data.serviceimage5!==null){

                                   html1+='<div class="col-md-4"><div class="user-card" style="background:'+data.color4+'"><p style="background:'+data.color5+';"><img src="'+data.serviceimage5+'" alt="ser-1" class="img-fluid d-block mx-auto" style="height:55px"/></p><h4 style="color:'+data.color6+';">'+data.servicename5+'</h4></div></div>';
                                }else{
                                  var img7 = "{{url('/web_assets/design-image/design-10/ser-5.png')}}";
                                   html1+='<div class="col-md-4"><div class="user-card" style="background:'+data.color4+'"><p style="background:'+data.color5+';"><img src="'+img7+'" alt="ser-1" class="img-fluid d-block mx-auto" style="height:55px"/></p><h4 style="color:'+data.color6+';">'+data.servicename5+'</h4></div></div>';
                                }
                                
                                 if(data.servicename6!==null && data.serviceimage6!==null){

                                   html1+='<div class="col-md-4"><div class="user-card" style="background:'+data.color4+'"><p style="background:'+data.color5+';"><img src="'+data.serviceimage6+'" alt="ser-1" class="img-fluid d-block mx-auto" style="height:55px"/></p><h4 style="color:'+data.color6+';">'+data.servicename6+'</h4></div></div>';
                                }else{
                                  var img8 = "{{url('/web_assets/design-image/design-10/ser-6.png')}}";
                                   html1+='<div class="col-md-4"><div class="user-card" style="background:'+data.color4+'"><p style="background:'+data.color5+';"><img src="'+img8+'" alt="ser-1" class="img-fluid d-block mx-auto" style="height:55px"/></p><h4 style="color:'+data.color6+';">'+data.servicename6+'</h4></div></div>';
                                }
                                
                                $('.bottomBar .servicesBoxx .row').html(html1);
                                
                                 if(data.address!==null && data.email!==null){
                                   $('.footerLinks .list-unstyled .second').html('<p><a href="#" style="color:'+data.color6+';"><i class="fa fa-envelope-o" aria-hidden="true"></i>'+data.email+'</a></p>'
                                   +'<p><a href="#" style="color:'+data.color6+';"><i class="fa fa-map-marker" aria-hidden="true"></i><div style="width:90%;float:right;margin-top: -20px;font-size:11px;">'+data.address+'</div></a></p>');
                                }
                                 
                                 
                                 
                                  var i;
                                var html3='';
                              
                                  for(var i in data.social_media){
                                      if(data.social_media[i]!==null)
                                  
                                      
                                   
                                      html3+='<li><a href="#" style="background:'+data.color2+';"><i class="fa '+data.socialMediaList[i]+'" aria-hidden="true" style="color:'+data.color5+';"></i></a></li>';
                                  }
                                 
                                $('.socialMediaIcons .list-unstyled').html(html3);
                                  var html='';
                                 
                                      if(data.socialMediaList[3]=='fa-whatsapp'){
                                        if(data.social_media[3]!==null){
                                     
                                             html+='<a href="#" style="background:'+data.color5+';color:'+data.color6+';"><i class="fa '+data.socialMediaList[3]+'" aria-hidden="true"></i></a>';    
                                        } 
                                   }
                                       
                                $('.footerLinks .list-unstyled .third').html(html);
                               
                             
                                 $(".bottomBar").css("background",data.color3);
                                 $(".wrapper").css("background",data.color1);
                                 
                         
                                
                              	$('#img').hide(); 
                              		setTimeout(function(){ $('#loader').hide();  }, 4000);
                              	 	dview();
                               		//$('#myModal').show();
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                   
      
         });
           
         $('button').click(function(){
             $('#myModal').hide(); 
         });   
            
            $('.finish-btn').click(function(){
                 $('form').attr('action', "{{route('admin.Cards.generate_pdf10')}}");
                 $('form').attr('target','popup');
                 var urrl="{{route('admin.Cards.generate_pdf10')}}";
                 $('form').attr(' onclick',"window.open("+urrl+",'popup','width=800,height=800');");
            });
            
            function view(){
            
                $.ajax({
                    url:"{{route('admin.Cards.delete-image')}}",
                    method:"get",
                    success: function(data) {
                        console.log(data);
                    }
                    
                });
            }
            
            
            
// global variable
 
    function dview() {
         var element = $("#html-content-holder"); // global variable
       var getCanvas; 

         html2canvas(element, {
              x: 50, y: 100,
              draggable: true,
         onrendered: function (canvas) {
                 
                $("#previewImage").html(canvas);
                getCanvas = canvas;
             
             }
         });
             
          $('#frame').show(); 
        $('#wrapper').hide(); 
        $('#img').hide(); 
       
    }
    //$('#img').hide(); 
   // $('#frame').hide(); 
  //$('#wrapper').hide();  
 
 
     
        
        //  $('.sf-controls').css("display","none");
        
          $('input[type="color"]').change(function(){
             var select1=$('#Primary').val();
             var select2=$('#Secondary').val();
             var select3=$('#text').val();
             var select4=$('#heading').val();
             var select5=$('#button_color').val();
             if(select1!=='Choose Color' && select2!=='Choose Color' && select3!=='Choose Color' && select4!=='Choose Color' && select5!=='Choose Color'){
                 $('.sf-controls').css("display","block");
                   $('#active1').attr('class','active');
             }
            
        });
     
    //  $('.next-btn').click(function(){
    //           $('.sf-controls').css("display","none");
    //     });   
        
        
         $('.socialLinksboxx').on("focusout","input",function(){
             
           
             var input1=$('input[name="social_media[1]"]').val();
             var input2=$('input[name="social_media[2]"]').val();
             var input3=$('input[name="social_media[3]"]').val();
             var input4=$('input[name="social_media[4]"]').val();
             var input5=$('input[name="social_media[5]"]').val();
             
             if(input1!=='' && input2!=='' && input3!=='' && input4!=='' && input5!==''){
                   $('.sf-controls').css("display","block");
                     $('#active2').attr('class','active');
             }else{
                   $('.sf-controls').css("display","none");
                   $('.col').text('*Please fill all the boxes');
             }
            
        });
        
        
        
         $('.panel-group').on("focusout","input",function(){
            
             var first_name=$('input[name="first_name"]').val();
             var last_name=$('input[name="last_name"]').val();
             var phone=$('input[name="phone"]').val();
             var designation=$('input[name="designation"]').val();
             var email=$('input[name="email"]').val();
             var bottom_title=$('input[name="bottom_title"]').val();
             var address=$('#address').val();
             var about=$('#about').val();
             var service_name_1=$('input[name="service-name-1"]').val();
             var service_name_2=$('input[name="service-name-2"]').val();
             var service_name_3=$('input[name="service-name-3"]').val();
             var service_name_4=$('input[name="service-name-4"]').val();
             var service_name_5=$('input[name="service-name-5"]').val();
             var service_name_6=$('input[name="service-name-6"]').val();
             
                              if(first_name.length <31){
                                
                                  first_name=first_name;
                              }else{
                                  $('.first_name').text('First Name  max 30 Char');
                                  $('.first_name').css('color','red');
                                    // alert('*First Name max 30 Char');
                              }
                              
                            
                              
                               
                              
                              if(last_name.length <31){
                                 
                                  last_name=last_name;
                              }else{
                                   $('.last_name').text('Last Name max 30 Char');
                                    $('.last_name').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              
                            
                              
                              if(designation.length <31){
                                 
                                  designation=designation;
                              }else{
                                   $('.designation').text('Designation max 30 Char');
                                    $('.designation').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              
                            //   if(about.length <100){
                                 
                            //       about1=about;
                            //   }else{
                            //       $('.about').text('About max 100 Char');
                            //       $('.about').css('color','red');
                            //       //alert('*Last Name max 30 Char');
                            //   }
                           
                              
                               if(address.length <81){
                                 
                                  address1=address;
                              }else{
                                   $('.address').text('Address max 80 Char');
                                   $('.address').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                           
                              
                               if(service_name_1.length <21){
                                 
                                   service_name_1= service_name_1;
                              }else{
                                  
                                   $('.service-name-1').text('Service Name 1 max 20 Char');
                                   $('.service-name-1').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_2.length <21){
                                 
                                   service_name_2= service_name_2;
                              }else{
                                   service_name_2='';
                                   $('.service-name-2').text('Service Name 2 max 20 Char');
                                   $('.service-name-2').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_3.length <21){
                                 
                                   service_name_3= service_name_3;
                              }else{
                                   $('.service-name-3').text('Service Name 3 max 20 Char');
                                   $('.service-name-3').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              if(service_name_4.length <21){
                                 
                                   service_name_4= service_name_4;
                              }else{
                                   $('.service-name-4').text('Service Name 4 max 20 Char');
                                   $('.service-name-4').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                            //   if(service_name_5.length <26){
                                 
                            //       service_name_5= service_name_5;
                            //   }else{
                            //       $('.service-name-5').text('Service Name 5 max 25 Char');
                            //       $('.service-name-5').css('color','red');
                            //       //alert('*Last Name max 30 Char');
                            //   }
                              
                            //   if(service_name_6.length <26){
                                 
                            //       service_name_6= service_name_6;
                            //   }else{
                            //       $('.service-name-6').text('Service Name 6 max 25 Char');
                            //          $('.service-name-6').css('color','red');
                            //       //alert('*Last Name max 30 Char');
                            //   }
                              
                              
                              
                            
                             
                              if(phone!==''){
                              if(validatePhone(phone)==true){
                                 
                                  phone=phone;
                                   $('.phone1').css('color','green');
                                   $(".phone1").text('*Phone Valid..!');
                              }else{
                                   //alert('Is not Valid..!');
                                   $('.phone1').css('color','red');
                                   $('.phone1').text('*Is not Valid Phone No... only no!');
                              }
                              }
                              
                              if(email!==''){
                              if(isEmail(email)==true){
                                 
                                  email1=email;
                                  $('.email').css('color','green');
                                  $(".email").text('* E-mail Valid..!');
                              }else{
                                  //alert('Is not Valid..!');
                                     $('.email').css('color','red');
                                  $('.email').text('* Is not Valid E-mail..!');
                              }
                              }
                             
             if(first_name!=='' && last_name!==''  && phone!=='' && email!=='' && designation!=='' && address1!=='' && service_name_1!=='' && service_name_2!=='' && service_name_3!=='' && service_name_4!==''){
                   $('.sf-controls').css("display","block");
             }else{
                  $('.sf-controls').css("display","none");
             }
            
        });
        
        
        function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
          if (regex.test(email)) {
                 return true;
               }
            else {
                  return false;
             }
        }
        
        
     function validatePhone(Phone) {
    
    var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (filter.test(Phone)) {
        return true;
    }
    else {
        return false;
    }
}
                    
});
    </script>
    
   
    
    
@endsection