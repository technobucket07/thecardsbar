
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <meta content="telephone=yes" name="format-detection">
        <title>Profile</title>
        <!--Core CSS -->
        <link href="{{ url('web_assets/card-css/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{ url('web_assets/card-css/css/font-awesome.min.css')}}" rel="stylesheet">
        
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <style>
            
/* Aurthor: Hardeep Singh */
@import url("https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap");

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #f0394d; }
::selection { color: #f1f1f1;background: #f0394d; }

j

body  {font-family: 'Poppins', sans-serif; }
.wrapper {width: 400px;margin: 0 auto;height: 850px;background: {{$val["primary"]}};}


.topHeader .firstName {font-size: 38px;line-height: 30px;}
.topHeader .lastName {font-size: 27px;}
.topHeader .designation 
{
    font-size: 17px;
/*color: #c8c8c8;*/
    
}

.abtSec {padding: 40px 40px 0 40px;border-radius: 36px 36px 0 0;margin: -36px 0 0 0;background:{{$val["primary"]}};}
.abtSec .userImg {text-align: right;margin: -92px 0 0 0;}
.abtSec .userImg img {width: 100px;}
.abtSec h1 {
    color: {{$val["text"]}};
    text-transform: uppercase;font-size: 14px;font-weight: 700;}
.abtSec .content {
    color: {{$val["text"]}};
       font-size: 13px;}


.infoSection {  padding: 0 40px;}
.infoSection h2 {
    color:  {{$val["text"]}};
text-transform: uppercase;font-size: 14px;font-weight: 700;margin: 0 0 15px 0;}
.infoSection ul {}
.infoSection ul li {margin: 10px 0;}
.infoSection ul li .contactIcon {float: left;width: 35px;height: 36px;text-align: center;border-radius: 50px;margin: 0 10px 0 0;background: {{$val["scondary"]}}; }
.infoSection ul li .contactIcon i {font-size: 36px;}
.infoSection ul li .contactIcon i.messageIcon {font-size: 24px;line-height: 33px;}
.infoSection ul li .contactInfo {float: left; width: 81%;}
.infoSection ul li .contactInfo h6 {margin: 0 0 4px 0;
             font-weight: 700;font-size: 14px;
              }
.infoSection ul li .contactInfo p {font-weight: 700;font-size: 16px;margin: 0;}
.infoSection ul li .contactInfo p a {
    /*color: #1e1c26;*/
    
}

.searchMe {padding: 0 40px;}
.searchMe h2 {
    color:  {{$val["text"]}};
    text-transform: uppercase;font-size: 14px;font-weight: 700;margin: 0 0 15px 0;}

.socialMedia {overflow: auto;white-space: nowrap;margin: 0 0 10px 0;}
.socialMedia li {margin: 5px;  background: #1e1c26;border-radius: 8px;}
.socialMedia li .fa {font-size: 30px;text-align: center;text-decoration: none;width: 50px;height: 50px;line-height: 50px;
     color:  {{$val["text"]}};
    
}
.socialMedia li .fa:hover {opacity: 0.7;}

.footer {padding: 0 40px;} 
.footer .webLink {text-align: center;font-size: 14px;font-weight: 600;margin: 0 0 5px 0;}
.footer .webLink a {
    /*color: #1e1c26;*/
    
}
.footer .nameUser {
    /*color: #b9b711;*/
    text-align: center;font-size: 13px;letter-spacing: 7px;}



/* Media Queries */
a{
   color:  {{$val["text"]}}; 
}
p{
    color:  {{$val["text"]}};
}


@media (min-width: 992px) and (max-width: 1199px) {
}

@media (min-width: 768px) and (max-width: 991px) {
}

@media (min-width: 480px) and (max-width: 767px) {
.footer .webLink, .footer .nameUser {text-align: left;}

}
@media (min-width: 150px) and (max-width: 479px) {
}

footer {
                position: fixed; 
                bottom: 0cm; 
                left: 0cm; 
                right: 0cm;
                height: 2cm;

                /** Extra personal styles **/
                background-color: #03a9f4;
                color: white;
                text-align: center;
                line-height: 1.5cm;
            }
        </style>
<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://html2canvas.hertzen.com/dist/html2canvas.js"></script>
    </head>
    <body style="padding-bottom:-150px;background: {{$val['primary']}};">

     <section class="wrapper" >

        <!-- HEADER STARTS HERE -->
        <header>
            <section class="topHeader" style="background:{{$val["scondary"]}}; color:{{$val["text"]}};padding: 40px 40px 76px 40px;">
                <div class="firstName">{{$val['first_name']}}</div>
                <div class="lastName">{{$val['last_name']}}</div>
                <div class="designation">{{$val['designation']}}</div>
            </section>
        </header>
        <!-- HEADER END HERE -->

        <!-- ABOUT STARTS HERE -->
        <section class="abtSec">
              @if(isset($val['image']))
               <p class="userImg"><img class="img-fluid" src="{{ $val['image']}}" alt="userimage" style="border-radius: 50px;border:4px solid {{$val['primary']}};width:95px;height:95px;padding:-1px"></p>
               @else
                <p class="userImg"><img class="img-fluid" src="{{ url('web_assets/design-image/design-1/userimage.png')}}" alt="userimage" style="border-radius: 50px;border:4px solid {{$val['primary']}};width:95px;height:95px;padding:-1px"></p>
              @endif
             @if(!empty($val['about']))
            <h1 style="color:{{$val["text"]}}">About Me</h1>
            <p class="content" style="text-align: justify;">{{$val['about']}}</p>
            @endif
        </section>
        <!-- ABOUT END HERE -->

        <!-- CONTACT STARTS HERE -->
        <section class="infoSection" style="margin-top:20px;">
                @if(!empty($val['address']))
            <h2 style="color:{{$val["text"]}}">contact Me</h2>
            @endif
            <table>
                 @if(!empty($val['address']))
                <tr>
                    <td style="width:42px;height:42px;">
                        <p style="width:40px;font-size:20px;text-align:center;height:40px;border-radius: 20px;background:transparent;background-blend-mode: overlay;margin-top:-60px;">
                        <i class="fa fa-map-marker" aria-hidden="true" style="font-size:38px;margin-left:9px;margin-top:33px;color:{{$val['text']}};" ></i></p>
                    </td>
                    
                    <td><div class="contactInfo" style="margin-left: 10px;line-height: 0.5;">
                        <h6 style="margin-bottom: 10px; font-weight: 700;font-size: 14px;color:{{$val['heading']}}">Location</h6>
                       <a href="{{$val['location']}}"> <p style="color:{{$val['text']}};line-height: 1.1;height:20px;margin-top:-8px;">{{$val['address']}} </p> </a>
                     </div>
                     </td>
                </tr>
                   @endif
                     @if(!empty($val['phone']))
               
                    <tr style="margin-top:10px;">
                    <td style="width:40px;height:40px;">
                        <p style="width:40px;font-size:20px;text-align:center;height:40px;border-radius: 20px;background:transparent;background-blend-mode: overlay;margin-top:4px;">
                        <i class="fa fa-mobile" aria-hidden="true" style="font-size:42px;margin-left:10px;margin-top:4px;color:{{$val['text']}};"></i>
                    </p></td>
                   <td> <div class="contactInfo" style="margin-left: 10px;line-height: 0.5;">
                        <h6 style="font-weight: 700;font-size: 14px;color:{{$val['heading']}}">Phone Number</h6>
                        <p style="color:{{$val['text']}};text-align: justify;"><a class="external" target="_system" href="tel:+91{{$val['phone']}}" style="color:{{$val['text']}};">{{$val['phone']}}</a></p>
                      </div>
               </td>
                </tr>
                  @endif
                   @if(!empty($val['email']))
                  <tr>
                    <td style="width:40px;height:40px;">
                    <p style="width:40px;font-size:20px;text-align:center;height:40px;border-radius: 20px;background:transparent;background-blend-mode: overlay;margin-top:-4px;">
                        <i class="fa fa-envelope" aria-hidden="true" style="font-size:27px;margin-left:6px;margin-top:6px;color:{{$val['text']}};"></i>
                    </p>
                    </td>
                   <td> 
                   <div class="contactInfo" style="margin-left: 10px;line-height: 0.5;">
                        <h6 style="font-weight: 700;font-size: 14px;color:{{$val['heading']}}">Email Address</h6>
                        <p style="color:{{$val['text']}};text-align: justify;"><a href="mailto:{{$val['email']}}" style="color:{{$val['text']}}">{{$val['email']}}</a></p>
                    </div>
                </td>
                </tr>
                @endif
            </table>
        </section>
        <!-- CONTACT END HERE -->



        <!-- CONTACT STARTS HERE -->
        @if(isset($val['social_media1']))
        <section class="searchMe" style="margin-top:20px;">
            @if(!empty($val['social_media1'][1]))
            <h2 style="color:{{$val["scondary"]}}">search Me @</h2>
            @endif
            <table align="center">
                <tr>
            @foreach($val['social_media1'] as $key=>$media)
         
             @if(!empty($media))
              @if($val['socialMediaList'][$key]=='fa-facebook')
               <td style="width:65px;height:40px; margin-left:10px;"><p style="width:45px;font-size:20px;background:{{$val["scondary"]}};height:40px;border-radius: 10px; padding:5px 5px 5px 5px; margin-left:8px;">
                   <a href="{{$media}}" class="fa {{$val['socialMediaList'][$key]}}" style="font-size: 31px; margin-left:14px;margin-top:4px;" target="_blank"></a></p></td>
                   @else
                    <td style="width:65px;height:40px; margin-left:10px;"><p style="width:45px;font-size:20px;background:{{$val["scondary"]}};height:40px;border-radius: 10px; padding:5px 5px 5px 5px; margin-left:8px;text-align: center;">
                   <a href="{{$media}}" class="fa {{$val['socialMediaList'][$key]}}" style="font-size: 31px; margin-left:10px;margin-top:4px;" target="_blank"></a></p></td>
                 @endif
               @endif
            @endforeach
               </tr>
            </table>
        </section>
        @endif
        <!-- CONTACT END HERE -->


        <!-- FOOTER STARTS HERE -->

            <section class="footer"  style="margin-top:5px;width:100%;background:{{$val['primary']}};"></br>
            <table  align="center" >
                 @if(!empty($val['website']))
                 
              
                     <tr>
                           @if(substr($val['website1'],0,5)=='http:')
                      
                                   <td style="border-top:0px;text-align:center;width:100%;">
                                         
                                           <a href="{{$val['website1']}}" target="_blank" >&nbsp;
                                           <i class="fa fa-globe" aria-hidden="true" style="color:{{$val["text"]}};margin-top:4px;float:left;margin-left:-6px;"></i>&nbsp; &nbsp;  <span style="color:{{$val["text"]}};">{{substr($val['website1'],7)}}</span></a>  
                                       
                                   </td>
                                     @else
                                     
                                      <td style="border-top:0px;text-align:center;width:100%;">
                                         
                                           <a href="{{$val['website1']}}" target="_blank" >&nbsp;
                                           <i class="fa fa-globe" aria-hidden="true" style="color:{{$val["text"]}};margin-top:4px;float:left;margin-left:-6px;"></i>&nbsp;&nbsp; <span style="color:{{$val["text"]}};">{{substr($val['website1'],8)}}</span></a>  
                                       
                                   </td>
                                   
                                   @endif
                     </tr>
                     
                @endif
                </table>
                
                 <table  style="margin-left:20px;" >
                 <tr >
                   <td  style="border-top:0px;" >
                      <p class="nameUser" style="font-weight: 700;font-size: 12px;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$val['first_name']}}&nbsp;{{$val['last_name']}}</p>
                      
                      
                   </td>
                 </tr>
             </table>
               
            </section>
            

        <!-- FOOTER END HERE -->

</section>
    

</body>
</html>