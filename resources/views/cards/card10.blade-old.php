<html>
        <!--Core CSS -->
      <link href="{{ url('bootstrap-4/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{ url('bootstrap-4/css/font-awesome.min.css')}}" rel="stylesheet">
    
     
     
     
        <style>
        

/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #f0394d; }
::selection { color: #f1f1f1;background: #f0394d; }



body  {font-family: 'Poppins', sans-serif;background: #000;}
.wrapper {width: 400px;margin: 0 auto;height: 850px;background: #fff;}

.topBar {padding: 30px;}
.logoWithinfo .logo {}
.logoWithinfo .logo img {height:30px;}

.logoWithinfo .rightTop {}
.logoWithinfo .rightTop .callUs{font-size: 12px;
color: #2d6ff7;
margin: 0;
/*font-weight: 600;*/
font-style:bold;
    
}
.logoWithinfo .rightTop .mobileNo{font-size: 16px;
/*font-weight: 700;*/
font-style:bold;
margin: 0;
line-height: 16px;color: #121520;}


.profileBoxxx {    margin: 30px 0;}
.profileBoxxx img{ border-radius:10px;}
.profileBoxxx h1{    color: #121520;
    font-size: 18px;
    /*font-weight: 700;*/
    font-style:bold;
    margin: 15px 0 5px 0;}
.profileBoxxx p{    font-size: 12px;
    color: #81869a;
    margin: 0;}


.socialMediaIcons {}
.socialMediaIcons ul{margin: 0; margin-left:10px; margin-top:20px;}
.socialMediaIcons ul li {display: inline-block;margin: 0 10px 0px 0px; width:50px;height:50px; }
.socialMediaIcons ul li a {  
 
    font-size: 30px;
    width: 50px;
    height: 50px;
    line-height: 50px;
    text-align: center;
    text-decoration: none;
    border-radius: 25px;
    display: inline-block;
    background: #ecf2fb;
    color: #2d6ff7;
    margin-left:5px;

}
.socialMediaIcons ul li a .fa {
    font-size: 26px;
    margin-left:14px;
}

.socialMediaIcons ul li a .fa:hover {
    opacity: 0.7;
}




.bottomBar {
    background: #121520;
    padding: 20px 40px 0 40px;
    border-radius: 36px 36px 0 0;
        height: 433px;
}
.bottomBar .servicesBoxx {}
.bottomBar .servicesBoxx .col-md-4{    padding-right: 7.5px;
    padding-left: 7.5px;}



.bottomBar .servicesBoxx h2 {    
        color: #f5f6f7;
        font-size: 16px;
        /*font-weight: 600;*/
        font-style:bold;
        margin: 0 0 15px 0;
    
}
.user-card {       
        background: #1e2234;
        border-radius: 10px;
        margin: 0 0 15px 0;
        width:100px;
        margin-left:10px;
}
.user-card p {    
        background: #2d6ff7;
        border-radius: 10px;
        text-align:center;
        margin: 0;
        padding: 10px;
    
}    
.user-card h4 {     color: #cbccd0;
    font-size: 11px;
   font-style:bold;
    padding: 10px;}


.footerLinks {}
.footerLinks ul {}
.footerLinks ul li {display: inline-block;
    float: left; margin-top:20px;}
.footerLinks ul li.first {width: 16%;}
.footerLinks ul li.first a {
     font-size: 30px;
    width: 50px;
    height: 50px;
    line-height: 50px;
    text-align: center;
    text-decoration: none;
    border-radius: 25px;
    display: inline-block;
    background: #2d6ff7;
    color: #ecf2fb;}
.footerLinks ul li.first a .fa {  font-size: 26px;
     margin-left:14px;
}

.footerLinks ul li.second{width: 67%; margin-top:15px;}
.footerLinks ul li.second p {    margin: 0;}
.footerLinks ul li.second p a {color: #f5f6f7;font-size: 11px; font-style:bold;}
.footerLinks ul li.second p a i {     margin: 0 5px 0 0;
    width: 19px;
    text-align: center; font-size:17px; margin-top:5px;}

.footerLinks ul li.third{width: 16%;text-align: right;margin-top:20px;}
.footerLinks ul li.third a {
    font-size: 30px;
    width: 50px;
    height: 50px;
    line-height: 50px;
    text-align: center;
    text-decoration: none;
    border-radius: 25px;
    display: inline-block;
    background: #2d6ff7;
    color: #ecf2fb;}
.footerLinks ul li.third a .fa {  font-size: 26px;
    margin-left:14px;
}















/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


}

        </style>
        <body style="padding-bottom:-150px;">

           <section class="wrapper" style="background:{{$val['color1']}};">

        
                <div class="topBar">

                    <div class="logoWithinfo">
                        <ul class="list-unstyled clearfix m-0">
                            <li class="logo w-50 pull-left">
                                @if($val['logo']!==null)
                                 <img class="img-fluid" src="{{$val['logo']}}" alt="ca-logo">
                                @else
                                  <img class="img-fluid" src="{{url('web_assets/design-image/design-10/ca-logo.png')}}" alt="ca-logo">
                                @endif
                            </li>
                            <li class="rightTop w-50 pull-left text-right">
                                <p class="callUs" style="color:{{$val['color5']}};">Call Us</p>
                                <p class="mobileNo"><a href="tel:{{$val['code']}}{{$val['phone']}}" style="color:{{$val['color7']}};float:right;">{{$val['code']}}-{{$val['phone']}}</a></p>
                            </li>
                        </ul>
                    </div>

                    <div class="profileBoxxx" align="center">
                         @if($val['profile_image']!==null)
                            <img src="{{$val['profile_image']}}" alt="ca-profile" style="height:100px;width:100px;" >

                         @else
                            <img src="{{url('web_assets/design-image/design-10/ca-profile.png')}}" alt="ca-profile" style="height:100px;width:100px;" >

                         @endif
                         <h1 class="text-center" style="color:{{$val['color7']}};">{{$val['first_name']}} {{$val['last_name']}}</h1>
                         <p class="text-center" style="color:{{$val['color8']}};">{{$val['designation']}}</p>
                    </div>

                    <div class="socialMediaIcons">
                        <ul class="list-unstyled">
                        @if(isset($val['social_media']))   
                        @foreach($val['social_media1'] as $key=>$media)
                        @if($val['socialMediaList'][$key]=='fa-facebook')
                        @if($media!=='')
                            <li>
                                <a href="{{$media}}" style="background:{{$val['color2']}};">
                                    <i class="fa {{$val['socialMediaList'][$key]}}" aria-hidden="true" style="color:{{$val['color5']}}; margin-left:16px;"></i>
                                </a>
                            </li>
                        
                        @endif
                        @else
                        
                         @if($media!=='')
                            <li>
                                <a href="{{$media}}" style="background:{{$val['color2']}};">
                                    <i class="fa {{$val['socialMediaList'][$key]}}" aria-hidden="true" style="color:{{$val['color5']}};"></i>
                                </a>
                            </li>
                        
                        @endif
                        
                        @endif
                        @endforeach
                        @endif
                           
                        </ul>
                    </div>

                </div>
        
                <div class="bottomBar" style="background:{{$val['color3']}};">
                    <div class="servicesBoxx">
                        <h2 class="text-center" style="color:{{$val['color6']}};">Our Services</h2>
                        <table style="margin-left:-10px;">
                            <tr>
                            <th>
                                 @if($val['profile_image']!==null)
                                <div class="user-card" style="background:{{$val['color4']}};">
                                    <p style="background:{{$val['color5']}};">
                                        <img src="{{$val['serviceimage1']}}" alt="ser-1" style="height:55px">
                                    </p>
                                    <h4 style="color:{{$val['color6']}};">{{$val['service-name-1']}} </h4>
                                </div>
                                @else
                                
                                <div class="user-card" style="background:{{$val['color4']}};">
                                    <p style="background:{{$val['color5']}};">
                                        <img src="{{url('web_assets/design-image/design-10/ser-1.png')}}" alt="ser-1" style="height:55px">
                                    </p>
                                    <h4 style="color:{{$val['color6']}};">{{$val['service-name-1']}} </h4>
                                </div>
                                
                                @endif
                            </th>
                            <th>
                                   @if($val['profile_image']!==null)
                                <div class="user-card" style="background:{{$val['color4']}};">
                                    <p style="background:{{$val['color5']}};">
                                        <img src="{{$val['serviceimage2']}}" alt="ser-2" style="height:55px">
                                    </p>
                                    <h4 style="color:{{$val['color6']}};">{{$val['service-name-2']}}</h4>
                                </div>
                                @else
                                  <div class="user-card" style="background:{{$val['color4']}};">
                                    <p style="background:{{$val['color5']}};">
                                        <img src="{{url('web_assets/design-image/design-10/ser-2.png')}}" alt="ser-2" style="height:55px">
                                    </p>
                                    <h4 style="color:{{$val['color6']}};">{{$val['service-name-2']}}</h4>
                                </div>
                                
                                @endif
                            </th>
                            <th>
                                  @if($val['profile_image']!==null)
                                <div class="user-card" style="background:{{$val['color4']}};">
                                    <p style="background:{{$val['color5']}};">
                                        <img src="{{$val['serviceimage3']}}" alt="ser-3" style="height:55px">
                                    </p>
                                     <h4 style="color:{{$val['color6']}};">{{$val['service-name-3']}} </h4>
                                </div>
                                @else
                                
                                  <div class="user-card" style="background:{{$val['color4']}};">
                                    <p style="background:{{$val['color5']}};">
                                        <img src="{{url('web_assets/design-image/design-10/ser-3.png')}}" alt="ser-3" style="height:55px">
                                    </p>
                                     <h4 style="color:{{$val['color6']}};">{{$val['service-name-3']}} </h4>
                                </div>
                                
                                @endif
                            </th>
                        </tr>
                        <tr>
                            <th>
                                  @if($val['profile_image']!==null)
                                <div class="user-card" style="background:{{$val['color4']}};">
                                    <p style="background:{{$val['color5']}};">
                                        <img src="{{$val['serviceimage4']}}" alt="ser-4" style="height:55px">
                                    </p>
                                    <h4 style="color:{{$val['color6']}};">{{$val['service-name-4']}} </h4>
                                </div>
                                @else
                                
                                 <div class="user-card" style="background:{{$val['color4']}};">
                                    <p style="background:{{$val['color5']}};">
                                        <img src="{{url('web_assets/design-image/design-10/ser-4.png')}}" alt="ser-4" style="height:55px">
                                    </p>
                                    <h4 style="color:{{$val['color6']}};">{{$val['service-name-4']}} </h4>
                                </div>
                                @endif
                                
                            </th>
                            <th>
                                  @if($val['profile_image']!==null)
                                <div class="user-card" style="background:{{$val['color4']}};">
                                    <p style="background:{{$val['color5']}};">
                                        <img src="{{$val['serviceimage5']}}" alt="ser-5" style="height:55px">
                                    </p>
                                    <h4 style="color:{{$val['color6']}};">{{$val['service-name-5']}} </h4>
                                </div>
                                @else
                                <div class="user-card" style="background:{{$val['color4']}};">
                                    <p style="background:{{$val['color5']}};">
                                        <img src="{{url('web_assets/design-image/design-10/ser-5.png')}}" alt="ser-5" style="height:55px">
                                    </p>
                                    <h4 style="color:{{$val['color6']}};">{{$val['service-name-5']}} </h4>
                                </div>
                                
                                @endif
                            </th>
                            <th>
                                  @if($val['profile_image']!==null)
                                <div class="user-card" style="background:{{$val['color4']}};">
                                    <p style="background:{{$val['color5']}};">
                                        <img src="{{$val['serviceimage6']}}" alt="ser-6" style="height:55px">
                                    </p>
                                    <h4 style="color:{{$val['color6']}};">{{$val['service-name-6']}}</h4>
                                </div>
                                @else
                                
                                   <div class="user-card" style="background:{{$val['color4']}};">
                                    <p style="background:{{$val['color5']}};">
                                        <img src="{{url('web_assets/design-image/design-10/ser-6.png')}}" alt="ser-6" style="height:55px">
                                    </p>
                                    <h4 style="color:{{$val['color6']}};">{{$val['service-name-6']}}</h4>
                                </div>
                                
                                @endif
                            </th>
                            </tr>
                        </table>


                        <div class="footerLinks">
                            <ul class="list-unstyled">
                                @if($val['phone']!==null)
                                <li class="first">
                                    <a href="tel:{{$val['code']}}{{$val['phone']}}" style="background:{{$val['color5']}};">
                                       <i class="fa fa-phone" aria-hidden="true" style="color:{{$val['color6']}}"></i>
                                    </a>
                                </li>
                                @endif
                                <li class="second">
                                    <p>
                                        <a href="mailto:{{$val['email']}}" style="color:{{$val['color6']}};">
                                            <i class="fa fa-envelope-o" aria-hidden="true"></i>{{$val['email']}}
                                        </a>
                                    </p>
                                    <p>
                                        <a href="#" style="color:{{$val['color6']}};">
                                            <i class="fa fa-map-marker" aria-hidden="true" style="font-size:17px;"></i><div style="float:right;width:91%;font-size:12px;margin-top:-20px;">{{$val['address']}}</div>
                                        </a>
                                    </p>
                                </li>
                                <li class="third">
                             @if(isset($val['social_media']))   
                             @foreach($val['social_media1'] as $key=>$media)
                             @if($val['socialMediaList'][$key]=='fa-whatsapp')
                             @if($media!=='')
                                    <a href="{{$media}}" style="background:{{$val['color5']}};">
                                       <i class="fa {{$val['socialMediaList'][$key]}}" aria-hidden="true" style="color:{{$val['color6']}}"></i>
                                    </a>
                              @endif
                              @endif
                              @endforeach
                              @endif
                                </li>
                            </ul>
                        </div>






                    </div>
                </div>




        </section>

 

            
    </body>
    
    </html>