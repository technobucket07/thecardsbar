@extends('cards.layouts.master')
@section('css')
         <link href="{{ url('web_assets/css/form-wizard.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/css/dropify.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/bootstrap.min.css')}}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/font-awesome.min.css')}}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/main.css')}}" rel="stylesheet">
         <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/demo/prism.css" />
         <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/bower_components/sweetalert/dist/sweetalert.css" />
	     <link rel="Stylesheet" type="text/css" href="{{ url('web_assets/js/style1.css?abc')}}" />
	     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
		 <script src="https://foliotek.github.io/Croppie/croppie.js"></script>
		 <script src="{{ url('web_assets/js/crop_js/script11.js')}}"></script>
	    
	
		
    <style>
        .text-muted1 {
     color: red;
        }
        .progressbar li {
         
        }
        
        #loader { 
      border: 12px solid #f3f3f3; 
      border-radius: 50%; 
      border-top: 12px solid #444444; 
      width: 70px; 
      height: 70px; 
      animation: spin 1s linear infinite; 
    } 
    
    @keyframes spin { 
      100% { 
        transform: rotate(360deg); 
      } 
    } 
    
    .center { 
      position: absolute; 
      top: 0; 
      bottom: 0; 
      left: 0; 
      right: 0; 
      margin: auto; 
    } 
    
   
    </style>     
   
       
@section('content')
    <!-- BACK BUTTON STARTS HERE -->
  <!--<script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script> -->

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="backBtn">
                        <a class="btnCustomStyle2 btn-solid" href="index.html">
                            <span>Back</span>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- BACK BUTTON END HERE -->

    <section class="upperCircles">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="progressbar list-unstyled" style="width: 100%;">
                        <li class="active" style=" width: 33%;"></li>
                        <li id="active1"  style=" width: 33%;"></li>
                        <li id="active2"  style=" width: 33%;"></li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- FORM WIZARD STARTS HERE -->
    <section class="mainFormWizard">
        <div class="container-fluid">
            <span id="theme_name" style="display: none;">sky</span>
            <span id="trans_name" style="display: none;">slide</span>
            <span id="rtl" style="display: none;">0</span>
            <div class="row">
                <div class="col-md-9">
    <ul>
            @foreach ($errors->all() as $error)
           
              <li> <div class="alert alert-warning">
               <strong>Warning!</strong> {{ $error }}
              </div></li>
            @endforeach
        </ul>
                    <form id="wizard_example_2"  method="post" enctype="multipart/form-data" >
                       
                        @csrf
                        <fieldset>
                            <legend>Color</legend>
                            <div class="row">
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Primary Color</label>
                                             <input type="color" class="form-control" id="Primary" name="primary" style="height:80px;width:200px;"  value="#352210">
                                           
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Secondary Color</label>
                                             <input type="color" class="form-control" id="Secondary" name="scondary" style="height:80px;width:200px;" value="#ffffff">
                                        
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Text Color</label>
                                               <input type="color" class="form-control" id="text" name="text" style="height:80px;width:200px;" value="#0000">
                                         
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Fa Fa-icon Color</label>
                                        <input type="color" class="form-control" id="heading" name="blog_color" style="height:80px;width:200px;" value="#ee9f40">
                                          
                                           
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                
                                
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Button Color</label>
                                             <input type="color" class="form-control" id="heading1" name="button_color" style="height:80px;width:200px;" value="#e8ecee">
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                   <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Website Color</label>
                                             <input type="color" class="form-control" id="heading1" name="website_color" style="height:80px;width:200px;" value="#fff2e3">
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                
                                
                               
                            </div>
                        </fieldset>
                      
                        <fieldset>
                            <legend>Social Links</legend>
                            @include('cards.socialmedia')
                            <!-- <div class="row">
                                <div class="col-md-12">
                                    <span class="col" style="color:red;font-size:16px;"></span>
                                    <ul class="list-unstyled socialLinksboxx">
                                        @foreach($socialMedia as $social)
                                          @if($social['icon']=='fa-whatsapp')
                                            <li class="clearfix">
                                                 <span class="titleSocial" >
                                                     <i class="fa {{$social['icon']}}" aria-hidden="true"></i>{{$social['name']}}
                                                 </span>
                                               
                                              
                                                 <select class="form-control" name="code" style="width: 78px; height: 29px;float: left;margin-top: 0px;font-size: 12px;margin-left: 63px;" id="code">
                                                    @foreach($va as $va1)
                                                    
                                                   <option value="{{$va1->callingCode}}">{{$va1->callingCode}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$va1->country}} </option>
                                                    @endforeach
                                                </select>
                                                 
                                                 <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="9564846264">
                                                 </span>
                                            </li>
                                              @endif
                                             
                                        @endforeach
                                    </ul>
                                </div>
                            </div> -->
                        </fieldset>
                        
                        
                      <fieldset>
                            <legend>Profile </legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        
                                              <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne1">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        Company Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne1">
                                                
                                                <div class="panel-body personalBoxxx">
                                                    <div class="row">
                                                      <div class="col-md-12">
                                                        <div class="form-group">
                                                         <div class="uploadBackground">
                                                          <label>Logo</label>
                                                           <div class="row">
                                                            <div class="col-md-6">
                                                              <input type="file" id="upload1"  value="Choose Picture" accept="image/*" class="dropify">
                                                              <input type="hidden" name="logo" id="base64image1">
                                                           </div>
                                                        <div class="col-md-6">
                                                          <div class="upload-demo-wrap1">
                                                            <div id="upload-demo1"></div>
                                                          </div>
                                                        </div>
                                                       </div>
                                                      </div>
                                                     </div>
                                                </div>
                                                        
                                                        
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <label>Company Phone</label>
                                                                <input type="text" class="form-control" placeholder="Enter Company Phone No." name="c_phone" maxlength="10" value="1212485741">
                                                                 <small class="text-muted1 c_phone"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <label>Company Email</label>
                                                                <input type="text" class="form-control" placeholder="Enter Company E-mail" name="c_email" value="info@example.com">
                                                                 <small class="text-muted1 c_email"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <label>Website </label>
                                                                <input type="text" class="form-control" placeholder="Enter website" name="website" value="www.example.com">
                                                                 <small class="text-muted1 website"></small>
                                                            </div>
                                                        </div>
                                                      <div class="col-md-12 col-lg-6 ">
                                                        <div class="form-group ">
                                                          <input type="text" class="form-control" placeholder="Enter your Official address" name="address" value="14 Sti Kilda Road, USA">
                                                          <small class="text-muted1 address"></small>
                                                        </div>
                                                      </div>
                                                      <div class="col-md-12 col-lg-6">
                                                        <div class="form-group ">
                                                          <input type="text" id="autocomplete" class="form-control" placeholder="Search your location" value="" onFocus="geolocate()">
                                                           <small class="text-muted1 address"></small>
                                                          <input type="hidden" name="location" id="latlong" value="">
                                                        </div>
                                                      </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
                                                        Personal Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                
                                                <div class="panel-body personalBoxxx">
                                                  @include('cards.createfor')
                                                    <div class="row">
                                                        
                                                        
                                                          
                                                        
                                                        
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" name="first_name1" placeholder="First Name 1" maxlength="10" value="Gurpreet">
                                                                <small class="text-muted1 first_name1"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Enter Last Name1" name="last_name1" maxlength="10" value="Singh ">
                                                                <small class="text-muted1 last_name1"></small>
                                                            </div>
                                                        </div>
                                                        
                                                      
                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" name="phone1" class="form-control" placeholder="Phone Number 1" maxlength="10" value="1234567890">
                                                                    <small class="text-muted1 phone1"></small>
                                                            </div>
                                                        </div>
                                                       <div class="col-md-12 col-lg-6 "></div>
                                                       
                                                       <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" name="first_name2" placeholder="Enter First Name 2" maxlength="10" value="Harpreet">
                                                                <small class="text-muted1 first_name2"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Enter Last Name2" name="last_name2" maxlength="10" value="Singh">
                                                                <small class="text-muted1 last_name2"></small>
                                                            </div>
                                                        </div>
                                                        
                                                      
                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" name="phone2" class="form-control" placeholder="Enter Phone Number 2" maxlength="10" value="1251248574">
                                                                    <small class="text-muted1 phone2"></small>
                                                            </div>
                                                        </div>
                                                     
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
           
                       
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingfour">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                                        Service Offers
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                                <div class="panel-body businessInformation">

                                                    <div class="row">

                                                      
                                                        
                                                      <div class="col-md-12">
                                                          <label style="color:black">Service Image 1</label>
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                  
                                                                    <!--<input type="file" name="service-image-1" id="input-file-now1" class="dropify" />-->
                                                                    <div class="col-md-6" style="float:left;">
                                                                      <input type="file" id="upload2"  value="Choose Picture" accept="image/*" class="dropify">
                                                                     </div>
                                                                    <div class="col-md-6" style="float:left;">
                                                                    <input type="hidden" name="serviceimage1" id="base64image2">
                                                                  <div class="upload-demo-wrap2">
                                                                <div id="upload-demo2"></div>
                                                                
                                                              </div>
                                                             <!--<div class="actions">-->
                                                             
                                                             <!--   <input type="number" class="basic-width2" placeholder="width" style="width:100px;"> X-->
                                                             <!--   <input type="number" class="basic-height2" placeholder="height" style="width:100px;">-->
                                                             <!-- </div>-->
                                                              </div>
                                                              </div>
                                                            </div>
                                                       
                                                        </div>
                                                        
                                                        
                                                         <div class="col-md-12 col-lg-12 ">
                                                            <div class="form-group">
                                                                <label>Service Name 1</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service 1" name="servicename1" maxlength="15" value="Living Room">
                                                                  <input type="text" class="form-control" placeholder="Enter Product 1" name="product1" maxlength="15" value="Floor Tiles">
                                                                 <small class="text-muted1 service-name-1"></small>
                                                            </div>
                                                        </div>
                                                        
                                                          
                                                      <div class="col-md-12">
                                                          <label style="color:black">Service Image 2</label>
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                  
                                                                    <!--<input type="file" name="service-image-1" id="input-file-now1" class="dropify" />-->
                                                                    <div class="col-md-6" style="float:left;">
                                                                      <input type="file" id="upload3"  value="Choose Picture" accept="image/*" class="dropify">
                                                                     </div>
                                                                    <div class="col-md-6" style="float:left;">
                                                                    <input type="hidden" name="serviceimage2" id="base64image3">
                                                                  <div class="upload-demo-wrap3">
                                                                <div id="upload-demo3"></div>
                                                              </div>
                                                              <!-- <div class="actions">-->
                                                              <!--  <input type="number" class="basic-width3" placeholder="width" style="width:100px;"> X-->
                                                              <!--  <input type="number" class="basic-height3" placeholder="height" style="width:100px;">-->
                                                              <!--</div>-->
                                                              
                                                              </div>
                                                              </div>
                                                            </div>
                                                       
                                                        </div>
                                                        
                                                        
                                                         <div class="col-md-12 col-lg-12 ">
                                                            <div class="form-group">
                                                                <label>Service Name 2</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service 2" name="servicename2" maxlength="15" value="Kitchen">
                                                                  <input type="text" class="form-control" placeholder="Enter Product 2" name="product2" maxlength="15" value="Wall Tiles">
                                                                 <small class="text-muted1 service-name-2"></small>
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="col-md-12">
                                                          <label style="color:black">Service Image 3</label>
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                  
                                                                    <!--<input type="file" name="service-image-1" id="input-file-now1" class="dropify" />-->
                                                                    <div class="col-md-6" style="float:left;">
                                                                      <input type="file" id="upload4"  value="Choose Picture" accept="image/*" class="dropify">
                                                                     </div>
                                                                    <div class="col-md-6" style="float:left;">
                                                                    <input type="hidden" name="serviceimage3" id="base64image4">
                                                                  <div class="upload-demo-wrap4">
                                                                <div id="upload-demo4"></div>
                                                              </div>
                                                              <!--<div class="actions">-->
                                                              <!--  <input type="number" class="basic-width4" placeholder="width" style="width:100px;"> X-->
                                                              <!--  <input type="number" class="basic-height4" placeholder="height" style="width:100px;">-->
                                                              <!--</div>-->
                                                              
                                                              </div>
                                                              </div>
                                                            </div>
                                                       
                                                        </div>
                                                        
                                                        
                                                         <div class="col-md-12 col-lg-12 ">
                                                            <div class="form-group">
                                                                <label>Service Name 3</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 3" name="servicename3" maxlength="15" value="Bathroom">
                                                                  <input type="text" class="form-control" placeholder="Enter Product 3" name="product3" maxlength="15" value="Wall Tiles">
                                                                 <small class="text-muted1 service-name-3"></small>
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="col-md-12">
                                                          <label style="color:black">Service Image 4</label>
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                  
                                                                    <!--<input type="file" name="service-image-1" id="input-file-now1" class="dropify" />-->
                                                                    <div class="col-md-6" style="float:left;">
                                                                      <input type="file" id="upload5"  value="Choose Picture" accept="image/*" class="dropify">
                                                                     </div>
                                                                    <div class="col-md-6" style="float:left;">
                                                                    <input type="hidden" name="serviceimage4" id="base64image5">
                                                                  <div class="upload-demo-wrap5">
                                                                <div id="upload-demo5"></div>
                                                              </div>
                                                              
                                                              <!-- <div class="actions">-->
                                                              <!--  <input type="number" class="basic-width5" placeholder="width" style="width:100px;"> X-->
                                                              <!--  <input type="number" class="basic-height5" placeholder="height" style="width:100px;">-->
                                                              <!--</div>-->
                                                              
                                                              </div>
                                                              </div>
                                                            </div>
                                                       
                                                        </div>
                                                        
                                                        
                                                         <div class="col-md-12 col-lg-12">
                                                            <div class="form-group">
                                                                <label>Service Name 4</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 4" name="servicename4" maxlength="15" value="Living Room">
                                                                  <input type="text" class="form-control" placeholder="Enter Product 4" name="product4" maxlength="15" value="Floor Tiles">
                                                                 <small class="text-muted1 service-name-4"></small>
                                                            </div>
                                                        </div>
                                                       
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                             
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                            <div class="col-md-12">
                                <p class="backBtn">
                                    <a class="btnCustomStyle2 btn-solid Preview1 Preview2 Preview3 Preview4 Preview5 Preview6" href="javascript:void(0);">
                                        <span>Preview</span>
                                    </a>
                                </p>
                            </div>
                          
                       </form>
                    </div>
                <div class="col-md-3">
                    
                    <div id="frame" style="background-image: url('{{ url("web_assets")}}/img/Phone1.png');background-color: #cccccc;background-position: center;background-repeat: no-repeat;background-size: cover;
  position: relative;width: 263px;height: 520px;border-radius: 30px;"></br>
                         <div id="previewImage" ></div>
                         <div id="loader" class="center" ></div> 
                          <div id="img" style="width: 235px;border-radius: 30px;margin-left: 15px;margin-top: -9px;">
                         <img class="img-fluid mx-auto d-block "  src="{{ url('web_assets')}}/img/desine-11.png" alt="" style="border-radius: 15px;height:495px;">
                    </div>
                  
                    </div>
                   
      
                   
                   
                </div>
                
    <style>

/* Aurthor: Hardeep Singh */
/*@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');*/

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #eb9d3f; }
::selection { color: #f1f1f1;}



body  {font-family: 'Poppins', sans-serif;}
.wrapper {width: 400px;margin: 0 auto;height: 900px;background: #fff;}


.mainBoxx {background: #352210;overflow: hidden;height: 100%;}

.topContact {    overflow: hidden;
    margin: 25px 0;
    height: 55px;
}
.topContact ul {  margin: 0; }
.topContact ul:after {
    display: block;
    clear: both;
    content: "";
}
.topContact ul .leftSide {float: left;
        width: auto;
}
.topContact ul .leftSide a {display: block; text-decoration: none;    border-radius: 0 10px 10px 0; font-size: 12px;
    font-weight: 600;  background: #fff;
    color: #000333;
    padding:10px;
    
}
.topContact ul .leftSide a i {   
    font-size: 16px;
    background: #ee9f41;
    color: #fff;
    width: 26px;
    height: 26px;
    line-height: 23px;
    float: right;
    border-radius: 14px;
    padding: 2px;
    text-align: center;
    margin: -4px -5px 0px 0px;
    
}

.topContact ul .rightSide {float: right;
        width: auto;
}
.topContact ul .rightSide a { 
    display: block;
    text-decoration: none;    
    border-radius: 10px 0 0 10px;
    font-size: 12px;
    font-weight: 600;  background: #fff;
    color: #000333;
    padding:10px;
    text-align: right;
    height:20px;

}
.topContact ul .rightSide a i { font-size: 16px;
    background: #ee9f41;
    color: #fff;
    width: 26px;
    height: 26px;
    line-height: 23px;
    border-radius: 14px;
    padding:2px;
    float: left;
    text-align: center;
    /*margin: 0 5px 0 0 ;*/
     margin: -4px 5px 0px -5px;
}


.logoBoxx {}
.logoBoxx .userImg {    background: transparent;
    /*padding: 10px;*/
    display: table;
    margin: 0 auto;
    border-radius: 10px;}
.logoBoxx .userImg img {border-radius:10px;}


.middlwBoxx {    background: #fff;
    border-radius: 40px;
    overflow: hidden;
    margin: 25px 0 0 0;
    padding: 30px;
    height: 550px;
}

.infoSection {     overflow: hidden;
    margin: 0 0 15px 0;
    border-bottom: 1px solid #e8ecee;
    padding: 0 0 20px 0;}
.infoSection h2 {color: #1e1c26;text-transform: uppercase;font-size: 14px;font-weight: 700;margin: 0 0 15px 0;}
.infoSection ul {margin: 0;}
.infoSection ul li {margin: 0 0 15px 0;}
.infoSection ul li:last-child {
    margin: 0;
}
.infoSection ul li:after {
    display: block;
    clear: both;
    content: "";
}

.infoSection ul li .contactIcon {    float: left;
       margin: 0 20px 0 0;
    width: 25px;
    background:transparent;
    text-align: center;}
.infoSection ul li .contactIcon i {font-size: 28px;line-height: 21px;    color: #eb9d3f;}
.infoSection ul li .contactIcon i.messageIcon {    font-size: 20px;
    line-height: 21px;}
.infoSection ul li .contactInfo {float: left;width: 81%;}
.infoSection ul li .contactInfo p {margin: 0;color: #000333;}
.infoSection ul li .contactInfo a {display: block;color: #000333;font-size: 16px;text-decoration: none;}




.serviceOffers {}
.serviceOffers h4 {  text-transform: uppercase;  color: #000333;
    font-size: 16px;
    text-align: center;
    font-weight: 600;
    margin: 0 0 15px 0;}
.serviceOffers ul {}
.serviceOffers ul:after {
    display: block;
    clear: both;
    content: "";
}
.serviceOffers ul li {    float: left;
width: 170px;
border: 1px solid #e8ecee;}

.serviceOffers ul li.bordeBottomRight {    border-bottom: 0;
    border-right: 0;}
.serviceOffers ul li.bordeBottomRight.borderDown {border-bottom: 1px solid #e8ecee !important;}
.serviceOffers ul li.bordeBottom {border-bottom: 0;}





.serviceOffers ul li .innerserviceBoxx {text-align: center;    padding: 10px 0;}
.serviceOffers ul li .innerserviceBoxx .iconImg {    display: table;
    margin: 0 auto;}
.serviceOffers ul li .innerserviceBoxx .iconImg img{
    width: 70px;
    height: 60px;
}
.serviceOffers ul li .innerserviceBoxx .title {    margin: 0;
    font-size: 14px;
    font-weight: 600;
    color: #ee9f40; }
.serviceOffers ul li .innerserviceBoxx .tagline {margin: 0;}
.serviceOffers ul li .innerserviceBoxx .tagline small { 
    background: #e8ecee;
    padding: 3px 10px;
    border-radius: 11px;
    display: inline-block;
    font-size: 11px;
    font-weight: 600;
    color: #000333;}



.bottomIcons {padding: 0 40px;
    margin: 60px 0 0 0;margin: 20px 0 0 0;}
.bottomIcons ul{margin: 0;}
.bottomIcons ul li.phone {display: inline-block;width: 15%;   float: left;}
.bottomIcons ul li.phone a {font-size: 30px;width: 60px;height: 60px;line-height: 65px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;    background: #ee9f40;color: #fff;}
.bottomIcons ul li.phone a .fa {font-size: 32px; margin-top: 15px;
}
.bottomIcons ul li.phone a .fa:hover {opacity: 0.7;}

.bottomIcons ul li.middelLine {    float: left;     margin: 7px 0 0 0;   width: 70%;}
.bottomIcons ul li.middelLine .moreInfo {     color: #fff2e3;
    font-size: 12px;
    text-align: center;
    margin: 0;}
.bottomIcons ul li.middelLine .linkweb { text-align: center;margin: 0;}
.bottomIcons ul li.middelLine .linkweb a {     text-decoration: none;
    color: #fff2e3;
    font-size: 12px;
    text-align: center;}

.bottomIcons ul{margin: 0;}
.bottomIcons ul:after {display: block;clear: both;content: "";}
.bottomIcons ul li.whatsApp {display: inline-block;width: 15%;      text-align: right; float: right;}
.bottomIcons ul li.whatsApp a {  font-size: 30px;width: 60px;height: 60px;line-height: 60px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;    background: #ee9f40;color: #fff;}
.bottomIcons ul li.whatsApp a .fa {font-size: 32px; margin-top: 15px;}
.bottomIcons ul li.whatsApp a .fa:hover {opacity: 0.7;}













/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


}

        </style>
     
    <div style="opacity:0;">      
<div id="html-content-holder">
    
           <section class="wrapper" id="wrapper" >

            <div class="mainBoxx">
                <div class="topContact">
                    <ul class="list-unstyled">
                      

                        
                    </ul>
                </div>

                <div class="logoBoxx">
                    <p class="userImg">
                        
                        
                    </p>
                </div>


                <div class="middlwBoxx">


                    <div class="infoSection">
                        <ul class="list-unstyled">
                           
                        </ul>
                    </div>

                    <div class="serviceOffers">
                        <h4>Service Offers</h4>
                        <ul class="list-unstyled">
                           
                            
                          
                            
                            
                        </ul>
                    </div>

                </div>

                <div class="bottomIcons">
                    <ul class="list-unstyled">
                        <li class="phone">
                           
                        </li>
                        <li class="middelLine">
                           
                        </li>
                        <li class="whatsApp">
                           
                        </li>
                    </ul>
                </div>
            </div>

        </section>
           
 
 </div> 

</div>           
            
        </div>


	


<script>


demoUpload1();
demoUpload2();
demoUpload3();
demoUpload4();
demoUpload5();
</script>	
            
          
        



        
        
    </section>
    <!-- FORM WIZARD END HERE -->
@endsection

@section('scripts')
    <script src="{{ url('web_assets/js/html2canvas.js') }}"></script>
    <script src="{{ url('web_assets/js/form-wizard.js') }}"></script>
    <script src="{{ url('web_assets/js/jscolor.js') }}"></script>
    <script src="{{ url('web_assets/js/dropify.js') }}"></script>
    
    

     
<style>
    canvas {
    height: 504px;
    width: 238px;
    margin-top: -18px;
    border-radius: 27px;
    margin-left: 13px;

    }
</style>

    <script>
        jscolor.presets.default = {
            borderColor:'rgba(236,239,245,0.6)', width:200, height:100,
            buttonColor:'rgba(70,51,255,1)', shadowColor:'rgba(0,0,0,0.05)'
        };
    </script>

    <script>
        $(document).ready(function(){
              
            	 $('#h1').hide();
            	 $('#h2').hide();
            	 $('.serviceOffers h4').hide();
            	  $('.logoBoxx .userImg').hide();
            // 	 $('.btnCustomStyle2').hide();
            	 $('#code').val('+91');
            	 $('#loader').hide(); 
            	 $('#wrapper').hide();
             $('.companyPic').hide();
             $('.proprietorName').hide();
            view();
            $('input[type="color"]').change(function(){
              $('.btnCustomStyle2').show();
            });
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });
           
            
             $('.btnCustomStyle2').click(function(){
                  	$('#loader').show();
                  	$('#wrapper').show();
                  	
            //   var  files =$('#base64image').val();
      
              
                var form_data = new FormData($('#wizard_example_2')[0]);
                //   form_data.append("file", files);
                        //   console.log(form_data);
   
                       $.ajax({
                            type: 'post',
                            url:  "{{route('admin.Cards.cards11')}}",
                            
                            data: form_data,
                            async: false,
                            dataType:'json',
                            success: function (data1) {
                            var    data=data1.va;
                                console.log(data1);
                                var html1='';
                                  var html2='';
                                var html3='';
                                if(data.first_name1!==null && data.last_name1!==null && data.phone1!==null){
                                    html1+='<li class="leftSide"><a href="'+data.code+data.phone1+'" style="background:'+data.scondary+';color:'+data.text+';height:35px;">&nbsp;<i class="fa fa-phone" aria-hidden="true" style="color:'+data.scondary+';background-color:'+data.blog_color+';"></i> '+data.first_name1+' '+data.last_name1+'&nbsp;&nbsp;</a></li>'
                                }
                                
                                if(data.first_name2!==null && data.last_name2!==null && data.phone2!==null){
                                    html1+='<li class="rightSide"><a href="'+data.code+data.phone2+'" style="background:'+data.scondary+';color:'+data.text+';height:35px;"><i class="fa fa-phone" aria-hidden="true" style="color:'+data.scondary+';background-color:'+data.blog_color+';"></i> '+data.first_name2+' '+data.last_name2+' </a></li>';
                                }
                                $('.topContact .list-unstyled').html(html1);
                                 if(data.logo!==null){
                                      $('.logoBoxx .userImg').show();
                                    $('.logoBoxx .userImg').html('<img src="'+data.logo+'" alt="userimg" class="img-fluid d-block" style="height:100px;width:auto;">');
                                } else {
                                     $('.logoBoxx .userImg').show();
                                     var img1 ="{{url('/web_assets/design-image/design-11/userimg.png')}}";
                                    $('.logoBoxx .userImg').html('<img src="'+img1+'" alt="userimg" class="img-fluid d-block" style="height:100px;width:auto;">');
                                }
                                 if(data.c_phone!==null){
                                      $('.infoSection h4').show();
                    html2+='<li><p class="contactIcon"><i class="fa fa-mobile" aria-hidden="true" style="color:'+data.blog_color+';"></i></p><div class="contactInfo"><a href="'+data.code+data.c_phone+'" style="color:'+data.text+'">'+data.code+'-'+data.c_phone+'</a></div></li>'; 
                    $('.phone').html('<a href="'+data.code+data.c_phone+'" style="color:'+data.scondary+';background-color:'+data.blog_color+';"><i class="fa fa-phone" aria-hidden="true"></i></a>');
                                }
                                 if(data.c_email!==null){
                                   
                  
                    html2+='<li><p class="contactIcon"><i class="fa fa-envelope messageIcon" aria-hidden="true" style="color:'+data.blog_color+';"></i></p><div class="contactInfo" ><a href="#" style="color:'+data.text+'">'+data.c_email+'</a></div></li>';
                   
                                }
                                
                                
                                if(data.address!==null){
                                     $('.infoSection h4').show();
                    html2+='<li><p class="contactIcon"><i class="fa fa-map-marker" aria-hidden="true" style="color:'+data.blog_color+';"></i></p><div class="contactInfo"><p style="color:'+data.text+';font-weight: inherit;">'+data.address+'</p></div></li>';
                                }
                                
                                
                                 if(data.website!==null){
                                     $('.middelLine').html('<p class="moreInfo"  style="color:'+data.website_color+';">for more information</p><p class="linkweb"><a href="#" style="color:'+data.website_color+';">'+data.website3+'</a></p>');
                                }
                                  $('.infoSection .list-unstyled').html(html2);
                                  if(data.servicename1!==null && data.serviceimage1!==null){
                                        $('.serviceOffers h4').show();
                                         $('.serviceOffers h4').css('color',data.text);
                        html3+='<li class="bordeBottomRight" style="border:1px solid '+data.button_color+';"><div class="innerserviceBoxx"><p class="iconImg"><img src="'+data.serviceimage1+'" alt="ser-1" class="img-fluid d-block"></p><p class="title" style="color:'+data.blog_color+';">'+data.servicename1+'</p><p class="tagline"><small style="background:'+data.button_color+';color:'+data.text+';">'+data.product1+'</small></p></div></li>';
                                      }else {
                                        $('.serviceOffers h4').show();
                                         $('.serviceOffers h4').css('color',data.text);
                                var img2 ="{{url('/web_assets/design-image/design-11/ser-1.jpg')}}";
                        html3+='<li class="bordeBottomRight" style="border:1px solid '+data.button_color+';"><div class="innerserviceBoxx"><p class="iconImg"><img src="'+img2+'" alt="ser-1" class="img-fluid d-block"></p><p class="title" style="color:'+data.blog_color+';">'+data.servicename1+'</p><p class="tagline"><small style="background:'+data.button_color+';color:'+data.text+';">'+data.product1+'</small></p></div></li>';
                                      }
                                      
                                  if(data.servicename2!==null && data.serviceimage2!==null){
                       html3+='<li class="bordeBottom" style="border:1px solid '+data.button_color+';"><div class="innerserviceBoxx"><p class="iconImg"><img src="'+data.serviceimage2+'" alt="ser-2" class="img-fluid d-block"></p><p class="title" style="color:'+data.blog_color+';">'+data.servicename2+'</p><p class="tagline"><small style="background:'+data.button_color+';color:'+data.text+';">'+data.product2+'</small></p></div></li>';
                                      }else{
                            var img3 ="{{url('/web_assets/design-image/design-11/ser-2.jpg')}}";
                       html3+='<li class="bordeBottom" style="border:1px solid '+data.button_color+';"><div class="innerserviceBoxx"><p class="iconImg"><img src="'+img3+'" alt="ser-2" class="img-fluid d-block"></p><p class="title" style="color:'+data.blog_color+';">'+data.servicename2+'</p><p class="tagline"><small style="background:'+data.button_color+';color:'+data.text+';">'+data.product2+'</small></p></div></li>';
                                      }
                                  if(data.servicename3!==null && data.serviceimage3!==null){ 
                      html3+='<li class="bordeBottomRight" style="border:1px solid '+data.button_color+';"><div class="innerserviceBoxx"><p class="iconImg"><img src="'+data.serviceimage3+'" alt="ser-3" class="img-fluid d-block"></p><p class="title" style="color:'+data.blog_color+';">'+data.servicename3+'</p><p class="tagline"><small style="background:'+data.button_color+';color:'+data.text+';">'+data.product3+'</small></p></div></li>';
                                  }else{ 
                                var img4 ="{{url('/web_assets/design-image/design-11/ser-3.jpg')}}";
                      html3+='<li class="bordeBottomRight" style="border:1px solid '+data.button_color+';"><div class="innerserviceBoxx"><p class="iconImg"><img src="'+img4+'" alt="ser-3" class="img-fluid d-block"></p><p class="title" style="color:'+data.blog_color+';">'+data.servicename3+'</p><p class="tagline"><small style="background:'+data.button_color+';color:'+data.text+';">'+data.product3+'</small></p></div></li>';
                                  }
                                  if(data.servicename4!==null && data.serviceimage4!==null){
                    html3+='<li style="border:1px solid '+data.button_color+';"><div class="innerserviceBoxx" ><p class="iconImg"><img src="'+data.serviceimage4+'" alt="ser-4" class="img-fluid d-block"></p><p class="title" style="color:'+data.blog_color+';">'+data.servicename4+'</p><p class="tagline"><small style="background:'+data.button_color+';color:'+data.text+';">'+data.product4+'</small></p></div></li>';                  
                                      
                                  }else{
                                    var img5 ="{{url('/web_assets/design-image/design-11/ser-4.jpg')}}";
                    html3+='<li style="border:1px solid '+data.button_color+';"><div class="innerserviceBoxx" ><p class="iconImg"><img src="'+img5+'" alt="ser-4" class="img-fluid d-block"></p><p class="title" style="color:'+data.blog_color+';">'+data.servicename4+'</p><p class="tagline"><small style="background:'+data.button_color+';color:'+data.text+';">'+data.product4+'</small></p></div></li>';                  
                                      
                                  }
                                   $('.serviceOffers .list-unstyled').html(html3)
                                  
                                  var html='';
                                 
                                      if(data.socialMediaList[3]=='fa-whatsapp'){
                                        if(data.social_media[3]!==null){
                                     
                                           
                                            html+='<a href="#" style="color:'+data.scondary+';background-color:'+data.blog_color+';"><i class="fa '+data.socialMediaList[3]+'" aria-hidden="true"></i></a>';
                                        } 
                                   }
                                 $('.whatsApp').html(html);
                                 $(".middlwBoxx").css("background",data.scondary);
                                 $('.infoSection').css('border-bottom','1px solid'+data.button_color)
                                 $(".mainBoxx").css("background",data.primary);
                         
                                
                              	$('#img').hide(); 
                              		setTimeout(function(){ $('#loader').hide();  }, 4000);
                              	 	dview();
                               		//$('#myModal').show();
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                   
      
         });
           
        
            $('.finish-btn').click(function(){
                 $('form').attr('action', "{{route('admin.Cards.generate_pdf11')}}");
                 $('form').attr('target','popup');
                 var urrl = "{{route('admin.Cards.generate_pdf11')}}";
                 $('form').attr(' onclick',"window.open("+urrl+",'popup','width=800,height=800');");
            });
            
            function view(){
            
                $.ajax({
                    url:"{{route('admin.Cards.delete-image')}}",
                    method:"get",
                    success: function(data) {
                        console.log(data);
                    }
                    
                });
            }
            
            
            
// global variable
 
    function dview() {
         var element = $("#html-content-holder"); // global variable
       var getCanvas; 

         html2canvas(element, {
              x: 50, y: 100,
              draggable: true,
         onrendered: function (canvas) {
                 
                $("#previewImage").html(canvas);
                getCanvas = canvas;
                console.log(canvas);
             }
         });
             
          //$('#frame').show(); 
        $('#wrapper').hide(); 
        $('#img').hide(); 
       
    }
   
 
 
      //("#Secondary option[value="+ color +"]").prop('disabled', true);              
    //   $("#text option[value=" + color + "]").prop('disabled', true);
    //     $("#Primary option[value="+ color +"]").prop('disabled', true);
   
    $("#Primary").change(function(){
       var color=$("#Primary").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', true);
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', false);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
    }) ;   
        
  $("#Secondary").change(function(){
       var color=$("#Secondary").val();
       $("#heading1 option[value=" + color + "]").prop('disabled', true);
       $("#heading option[value=" + color + "]").prop('disabled', true);
       $("#Secondary option[value="+ color +"]").prop('disabled', false);
       $("#Primary option[value="+ color +"]").prop('disabled', true);
       $("#text option[value=" + color + "]").prop('disabled', true);
    }) ;   
    
    $("#text").change(function(){
       var color=$("#text").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', true);
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', false);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
         $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
    
     $("#heading").change(function(){
       var color=$("#heading").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', true);
        $("#heading option[value=" + color + "]").prop('disabled', false);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
        
        
         $("#heading1").change(function(){
       var color=$("#heading1").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', false);
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
         $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
         
        
        
        //  $('.sf-controls').css("display","none");
        
          $('input[type="color"]').change(function(){
             var select1=$('#Primary').val();
             var select2=$('#Secondary').val();
             var select3=$('#text').val();
             var select4=$('#heading').val();
             var select5=$('#button_color').val();
             if(select1!=='Choose Color' && select2!=='Choose Color' && select3!=='Choose Color' && select4!=='Choose Color' && select5!=='Choose Color'){
                 $('.sf-controls').css("display","block");
                   $('#active1').attr('class','active');
             }
            
        });
     
    //  $('.next-btn').click(function(){
    //           $('.sf-controls').css("display","none");
    //     });   
        
        
         $('.socialLinksboxx').on("focusout","input",function(){
             
           
            //  var input1=$('input[name="social_media[1]"]').val();
            //  var input2=$('input[name="social_media[2]"]').val();
             var input3=$('input[name="social_media[3]"]').val();
            //  var input4=$('input[name="social_media[4]"]').val();
            //  var input5=$('input[name="social_media[5]"]').val();
             
             if(input3!==''){
                   $('.sf-controls').css("display","block");
                     $('#active2').attr('class','active');
             }else{
                   $('.sf-controls').css("display","none");
                   $('.col').text('*Please fill all the boxes');
             }
            
        });
        
        
        
         $('.panel-group').on("focusout","input",function(){
            
             var first_name1=$('input[name="first_name1"]').val();
             var last_name1=$('input[name="last_name1"]').val();
             var phone1=$('input[name="phone1"]').val();
             var c_phone=$('input[name="c_phone"]').val();
             
             var c_email=$('input[name="c_email"]').val();
             var first_name2=$('input[name="first_name2"]').val();
             var last_name2=$('input[name="last_name2"]').val();
             var phone2=$('input[name="phone2"]').val();
             var service_name_1=$('input[name="servicename1"]').val();
             var service_name_2=$('input[name="servicename2"]').val();
             var service_name_3=$('input[name="servicename3"]').val();
             var service_name_4=$('input[name="servicename4"]').val();
             
                              if(first_name1.length <11){
                                
                                  first_name1=first_name1;
                              }else{
                                  $('.first_name1').text('First Name 1 max 10 Char');
                                    // alert('*First Name max 30 Char');
                              }
                              
                              if(first_name2.length <11){
                                
                                  first_name2=first_name2;
                              }else{
                                  $('.first_name2').text('First Name 2 max 10 Char');
                                    // alert('*First Name max 30 Char');
                              }
                              
                               
                              
                              if(last_name1.length <11){
                                 
                                  last_name1=last_name1;
                              }else{
                                   $('.last_name1').text('Last Name2 max 10 Char');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              if(last_name2.length <11){
                                 
                                  last_name2=last_name2;
                              }else{
                                   $('.last_name2').text('Last Name 2 max 10 Char');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_1.length <16){
                                 
                                   service_name_1= service_name_1;
                              }else{
                                   $('.service-name-1').text('Service Name 1 max 15 Char');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_2.length <16){
                                 
                                   service_name_2= service_name_2;
                              }else{
                                   $('.service-name-2').text('Service Name 2 max 15 Char');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_3.length <16){
                                 
                                   service_name_3= service_name_3;
                              }else{
                                   $('.service-name-3').text('Service Name 3 max 15 Char');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              if(service_name_4.length <16){
                                 
                                   service_name_4= service_name_4;
                              }else{
                                   $('.service-name-4').text('Service Name 4 max 15 Char');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              
                              
                            
                             
                              if(phone1!==''){
                              if(validatePhone(phone1)==true){
                                 
                                  phone1=phone1;
                                  $('.phone1').css('color','green');
                                  $(".phone1").text('*Phone Valid..!');
                              }else{
                                   //alert('Is not Valid..!');
                                     $('.phone1').css('color','red');
                                   $('.phone1').text('*Is not Valid Phone No... only no!');
                              }
                              }
                              
                              if(phone2!==''){
                              if(validatePhone(phone2)==true){
                                 
                                  phone2=phone2;
                                  $('.phone2').css('color','green');
                                  $(".phone2").text('*Phone Valid..!');
                              }else{
                                   //alert('Is not Valid..!');
                                     $('.phone2').css('color','red');
                                   $('.phone2').text('*Is not Valid Phone No... only no!');
                              }
                              }
                              
                               if(c_phone!==''){
                              if(validatePhone(c_phone)==true){
                                 
                                  c_phone=c_phone;
                                  $('.c_phone').css('color','green');
                                  $(".c_phone").text('*Phone Valid..!');
                              }else{
                                   //alert('Is not Valid..!');
                                     $('.c_phone').css('color','red');
                                   $('.c_phone').text('*Is not Valid Phone No... only no!');
                              }
                              }
                              
                               if(c_email!==''){
                              if(isEmail(c_email)==true){
                                 
                                  c_email=c_email;
                                   $('.c_email').css('color','green');
                                   $(".c_email").text('*Email Valid..!');
                              }else{
                                   //alert('Is not E-mail');
                                    $('.c_email').css('color','red');
                                    $('.c_email').text('*Is not E-mail');
                              }
                                }
                             
             if(first_name1!=='' && last_name1!==''  && phone1!==' '&& first_name2!=='' && last_name2!==''  && phone2!==''  && c_phone!=='' && c_email!=='' && service_name_1!=='' && service_name_2!=='' && service_name_3!=='' && service_name_4!==''){
                 // alert(first_name+" "+last_name+" "+email+" "+designation+"  "+about+" "+phone+" "+address+" "+website);
                 $('.sf-controls').css("display","block");
             }else{
                  $('.sf-controls').css("display","none");
             }
            
        });
        
        
        function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
          if (regex.test(email)) {
                 return true;
               }
            else {
                  return false;
             }
        }
        
        
     function validatePhone(Phone) {
    
    var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (filter.test(Phone)) {
        return true;
    }
    else {
        return false;
    }
}



        
});
    </script>
    
   
  
@endsection