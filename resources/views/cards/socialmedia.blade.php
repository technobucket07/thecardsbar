
<div class="row">
	<div class="col-md-12">
		<span class="col" style="color:red;font-size:16px;"></span>
		<ul class="multi_media">
			@php $j=1;@endphp
		   @foreach($socialMedia as $social)
		      <li>
		      <input type="checkbox" class="checkicon" data-id="{{$social['id']}}" data-name="{{$social['name']}}" id="{{$social['name']}}icon" @if($j <=3){{'checked'}} @endif/>
		      <label for="{{$social['name']}}icon">
		        <!-- <img src="{{url($social['icon'])}}" /> -->
		        <i class="fa {{$social['icon']}}" aria-hidden="true"></i>
		      </label>
		      </li>
		      @php $j++; @endphp
		   @endforeach
		</ul>
		<!-------choose social media end-------------------->
        <ul class="list-unstyled socialLinksboxx">
        	@php $i=1;@endphp
        	@foreach($socialMedia as $social)
	        	<li class="clearfix" id="{{$social['name']}}">
	              <span class="titleSocial"><label>{{$social['name']}}</label></span>
	              <span class="btnLinkright linkup">
	              	@if($social['name']=='WhatsApp')
		              	<select class="form-control" name="code" id="code">
	                        @foreach($va as $va1)
	                       <option value="{{$va1->callingCode}}">{{$va1->callingCode}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$va1->country}} </option>
	                        @endforeach
	                   	</select>
	                   	<input type="text" class="form-control link" name="social_media[{{$social['id']}}]" value="7695875847">
	                @else
		                <input type="text" class="form-control link" name="social_media[{{$social['id']}}]" value="https://www.{{$social['name']}}.com">
		            	</span>
	            	@endif
	            </li>
	            @if($i==3)
	            @break;
	            @endif
	            @php $i++; @endphp
            @endforeach
        </ul>
	</div>
</div>
<div class="c_code" style="display: none;">
        @foreach($va as $va1)
       <option value="{{$va1->callingCode}}">{{$va1->callingCode}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$va1->country}} </option>
        @endforeach
   	</select>
</div>
<script type="text/javascript">
function convertToSlug(Text)
{
    return Text
        .toLowerCase()
        .replace(/ /g,'-')
        .replace(/[^\w-]+/g,'')
        ;
}
	$(document).ready(function(){
	  $('.checkicon').change(function() {
	      var linkname = convertToSlug($(this).data('name'));
	      console.log(linkname);
	      var linkid = $(this).data('id');
	      if($(this).is(":checked")) {
	        //var icon =  $(this). parent('li').find('img').attr('src');
	        if(linkname=='whatsapp'){
	        	$('.socialLinksboxx').append('<li class="clearfix" id="'+linkname+'"><span class="titleSocial"><label>'+linkname+'</label></span><span class="btnLinkright linkup"><select class="form-control" name="code" id="code">'+$('.c_code').html()+'<input type="text" class="form-control" name=social_media['+linkid+'] value="7698574875"></span></li>');
	        }else{
	        	$('.socialLinksboxx').append('<li class="clearfix" id="'+linkname+'"><span class="titleSocial"><label>'+linkname+'</label></span><span class="btnLinkright linkup"><input type="text" class="form-control" name=social_media['+linkid+'] value="https://www.'+linkname+'.com"></span></li>');
	        }
	      }else{
	        $('.socialLinksboxx li#'+linkname).remove();
	      }      
	    });
	});
</script>