<html>
        <!--Core CSS -->
      <link href="{{ url('bootstrap-4/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{ url('bootstrap-4/css/font-awesome.min.css')}}" rel="stylesheet">
    
     
     
     
        <style>
  
/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #c18f59; }
::selection { color: #f1f1f1;background: #c18f59; }



body  {font-family: 'Poppins', sans-serif;background: #131311;}
.wrapper {width: 400px;margin: 0 auto;height: 830px;background: #f7f9fb;}



.profileboxx {height: 270px;color: #fff;}
.profileboxx:after {display: block;clear: both;content: "";}
.profileboxx .innerbox {float: left;width: 40%;text-align: center;margin-left:30px;}
.profileboxx .innerbox .userimg  {width: 114px;height: 114px;margin:0 auto;border-radius: 58px;border: 3px solid {{$val['text']}};}
.profileboxx .innerbox .userimg img {border-radius: 56px;width: 100%;height:115px;}
.profileboxx .innerbox .designation  {font-size: 12px;text-transform: uppercase;margin: 10px 0 5px 0; color: {{$val['text']}};}
.profileboxx .innerbox .name  {font-size: 16px;font-style:bold; color: {{$val['text']}};}
.profileboxx .rightInfo {float: right;width: 43%;text-align: right; margin-left:-20px;}
.profileboxx .rightInfo ul {}
.profileboxx .rightInfo ul .first {}

.profileboxx .rightInfo ul .first .timing {font-size: 17px;margin: 0;  margin-right:0px;  color: {{$val['text']}};}
.profileboxx .rightInfo ul .first .timing i {margin: 0px 0px 0px 0px; padding-top:15px; color: {{$val['social_media_color']}};}
.profileboxx .rightInfo ul .first .days {font-size: 12px; color: {{$val['text']}};}

.profileboxx .rightInfo ul .third {margin-top:10px;}
.socialMediaIcons {}
.socialMediaIcons ul{margin: 0; float:right; margin-right:-5px;}
.socialMediaIcons ul li {display: inline-block;margin: 0 1px; width: 36px;height: 36px;}
.socialMediaIcons ul li a {  font-size: 30px;width: 36px;height: 36px;line-height: 28px;text-align: center;text-decoration: none;border-radius: 18px;display: inline-block;color: #fff;background: {{$val['social_media_color']}};}
.socialMediaIcons ul li a .fa {font-size: 18px; margin-left:10px;margin-top:5px; color: {{$val['text']}}; }
.socialMediaIcons ul li a .fa:hover {opacity: 0.7;}

.address {color: #2b53c2;font-size: 13px;text-align: center;}



.serviceOffers {    padding: 0 30px;margin: -35px 0 0 0;}
.serviceOffers table {    margin: 0;}
.serviceOffers table:after {display: block;clear: both;content: "";}
.serviceOffers  table tr th {float: left;width: 130px;margin: 0 0 15px 0;padding: 0 7.5px;}
.serviceOffers  table tr th.marginMinustop {margin: -40px 0 15px 0;}







.serviceOffers  table tr th .innerserviceBoxx {text-align: left;border-radius: 15px;padding: 15px;margin: 20px 0 0px 0;background: {{$val['scondary']}};}
.serviceOffers  table tr th .innerserviceBoxx .iconImg {padding: 30px 0;margin: 0;}
.serviceOffers  table tr th .innerserviceBoxx .iconImg img{width: 125px; height: 85px;}
.serviceOffers  table tr th .innerserviceBoxx .title {margin: 0;font-size: 14px;font-style:blod;color:{{$val['heading_color']}}; }
.serviceOffers  table tr th .innerserviceBoxx .tagline {margin: 0;font-size: 12px;color: {{$val['blogtext_color']}};}


.bottomBoxx {padding: 0 30px;}
.bottomBoxx .Address{font-size: 12px;font-style:bold;margin: 0;color:{{$val['blogtext_color']}};}
.bottomBoxx .infoAdd{font-size: 14px;font-style: bold;margin: 0;color: {{$val['heading_color']}};}
.bottomBoxx .professional{font-size: 18px;font-style:bold;text-align: center;color:{{$val['social_media_color']}};}










/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


}      

        </style>
        <body style="padding-bottom:-150px;">
           <section class="wrapper" style="background:{{$val['primary']}}">

            <div class="profileboxx">
                @if($val['bg_image']!==null)
                <img src="{{$val['bg_image']}}" style="height:100%;width:100%;float:left;"/>
                   @else
                    <img src="{{ url('web_assets/design-image/design-9/bgimg.jpg')}}" style="height:100%;width:100%;float:left;"/>
                @endif
                <div class="innerbox" style="padding-right:-20px;margin-top:20px;" >
                    <p class="userimg">
                        @if($val['profile_image']!==null)
                        <img  src="{{$val['profile_image']}}" alt="profilepic">
                        @else
                          <img  src="{{ url('web_assets/design-image/design-9/profilepic.jpg')}}" alt="profilepic">
                        @endif
                    </p>
                    <p class="designation">{{$val['designation']}}</p>
                    <p class="name">{{$val['first_name']}} {{$val['last_name']}}</p>

                </div>
                <div class="rightInfo"  style="padding-left:-20px;margin-top:20px;margin-right:20px;">
                    <ul class="list-unstyled">
                        <li class="first">
                             @if($val['time']!==null)
                            <p class="timing" style="text-align:right;"><i class="fa fa-history" aria-hidden="true" style="height:15px;"></i>{{$val['time']}}</p>
                            <p class="days" style="text-align:right;">{{$val['days']}}</p>
                              @endif
                        </li>
                        <li class="first">
                             @if($val['phone']!==null)
                            <p class="timing" style="text-align:right;"><i class="fa fa-phone" aria-hidden="true" style="height:15px;"></i><a href="tel:{{$val['phone']}}" style="color:{{$val['text']}};">{{$val['phone']}}</a></p>
                             @endif
                             @if($val['call_us']!==null)
                            <p class="days" style="text-align:right;">{{$val['call_us']}}</p>
                            @endif
                        </li>
                        <li class="third">
                            <div class="socialMediaIcons" >
                                <ul class="list-unstyled">
                                      @if(isset($val['social_media']))   
                                       @foreach($val['social_media1'] as $key=>$media)

                                        <li>
                                          <a href="{{$media}}">
                                              <i class="fa {{$val['socialMediaList'][$key]}}" aria-hidden="true"></i>
                                          </a>
                                        </li>                                        
                                      @endforeach
                                      @endif
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="serviceOffers">
                <table>
                    <tr style="">
                    <th class="marginMinustop" style="height:200px;">
                          @if($val['serviceimage1']!==null && $val['case1']!==null)
                        <div class="innerserviceBoxx" style="margin-top:-20px;">
                            <p class="iconImg">
                                <img src="{{$val['serviceimage1']}}" alt="ser-1" class="">
                            </p>
                            <p class="title">{{$val['service-name-1']}}</p>
                            <p class="tagline">{{$val['case1']}}</p>
                        </div>
                        
                        @else
                        <div class="innerserviceBoxx" style="margin-top:-20px;">
                            <p class="iconImg">
                                <img src="{{url('web_assets/design-image/design-9/ser-1.jpg')}}" alt="ser-1" class="">
                            </p>
                            <p class="title">{{$val['service-name-1']}}</p>
                            <p class="tagline">{{$val['case1']}}</p>
                        </div>
                        
                        @endif
                    </th>
                    <th>
                       @if($val['serviceimage2']!==null && $val['case2']!==null)
                        <div class="innerserviceBoxx" style="margin-top:20px;">
                            <p class="iconImg">
                                <img src="{{$val['serviceimage2']}}" alt="ser-2" class="">
                            </p>
                            <p class="title">{{$val['service-name-2']}}</p>
                            <p class="tagline">{{$val['case2']}}</p>
                        </div>
                        @else
                        <div class="innerserviceBoxx" style="margin-top:20px;">
                            <p class="iconImg">
                                <img src="{{url('web_assets/design-image/design-9/ser-2.jpg')}}" alt="ser-2" class="">
                            </p>
                            <p class="title">{{$val['service-name-2']}}</p>
                            <p class="tagline">{{$val['case2']}}</p>
                        </div>
                        
                        @endif
                    </th>
                    </tr>
                    <tr>
                    <th class="marginMinustop" style="height:200px;">
                        @if($val['serviceimage3']!==null && $val['case3']!==null)
                        <div class="innerserviceBoxx" style="margin-top:-20px;">
                            <p class="iconImg">
                                <img src="{{$val['serviceimage3']}}" alt="ser-3" class="">
                            </p>
                           <p class="title">{{$val['service-name-3']}}</p>
                            <p class="tagline">{{$val['case3']}}</p>
                        </div>
                          @else
                           <div class="innerserviceBoxx" style="margin-top:-20px;">
                            <p class="iconImg">
                                <img src="{{url('web_assets/design-image/design-9/ser-3.jpg')}}" alt="ser-3" class="">
                            </p>
                           <p class="title">{{$val['service-name-3']}}</p>
                            <p class="tagline">{{$val['case3']}}</p>
                        </div>
                          
                        @endif
                    </th>
                    <th>
                        <div class="innerserviceBoxx" style="margin-top:20px;">
                             @if($val['serviceimage4']!==null && $val['case4']!==null)
                            <p class="iconImg">
                                <img src="{{$val['serviceimage4']}}" alt="ser-4" class="">
                            </p>
                             <p class="title">{{$val['service-name-4']}}</p>
                            <p class="tagline">{{$val['case4']}}</p>
                            @else
                             <p class="iconImg">
                                <img src="{{url('web_assets/design-image/design-9/ser-4.jpg')}}" alt="ser-4" class="">
                            </p>
                             <p class="title">{{$val['service-name-4']}}</p>
                            <p class="tagline">{{$val['case4']}}</p>
                            
                             @endif
                        </div>
                    </th>
                    </tr>
                </table>
            </div>


            <div class="bottomBoxx">
            @if($val['address']!==null)
                <h6 class="Address">Address</h6>
                <a href="{{$val['location']}}"><p class="infoAdd">{{$val['address']}}</p></a>
            @endif
            @if($val['bottom_title']!==null)
                <p class="professional">{{$val['bottom_title']}} </p>
            @endif        
            </div>








        </section>
            
    </body>
    
    </html>