@extends('cards.layouts.master')
@section('css')
<link href="{{ url('cardslayout/css/form-wizard.css') }}" rel="stylesheet">
<link href="{{ url('cardslayout/css/dropify.css') }}" rel="stylesheet">
<link href="{{ url('cardslayout/card-css/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{ url('cardslayout/card-css/css/font-awesome.min.css')}}" rel="stylesheet">
<link href="{{ url('cardslayout/card-css/css/main.css')}}" rel="stylesheet">
<link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/demo/prism.css" />
<link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/bower_components/sweetalert/dist/sweetalert.css" />
<link rel="Stylesheet" type="text/css" href="{{ url('cardslayout/js/style1.css?abc')}}" />
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
<script src="https://foliotek.github.io/Croppie/croppie.js"></script>
<script src="{{ url('cardslayout/js/crop_js/script1.js')}}"></script>
<style>
   .text-muted1 {
   color: red;
   }
   .progressbar li {
   }
   #loader { 
   border: 12px solid #f3f3f3; 
   border-radius: 50%; 
   border-top: 12px solid #444444; 
   width: 70px; 
   height: 70px; 
   animation: spin 1s linear infinite; 
   } 
   /*  #loader2 { */
   /*  border: 12px solid #f3f3f3; */
   /*  border-radius: 50%; */
   /*  border-top: 12px solid #444444; */
   /*  width: 70px; */
   /*  height: 70px; */
   /*  animation: spin 1s linear infinite; */
   /*} */
   .preloader {
   position: absolute;
   top: 0;
   left: 0;
   width: 100%;
   height: 122%;
   z-index: 9999;
   background-image: url('../IMAGE/default.gif');
   background-repeat: no-repeat; 
   background-color: #fff9;
   background-position: center;
   }
   @keyframes spin { 
   100% { 
   transform: rotate(360deg); 
   } 
   } 
   .center { 
   position: absolute; 
   top: 0; 
   bottom: 0; 
   left: 0; 
   right: 0; 
   margin: auto; 
   } 
   .center1 { 
   position: absolute; 
   top: 0; 
   bottom: 0; 
   left: 0; 
   right: 0; 
   margin: auto; 
   } 
</style>
@section('content')
<!-- BACK BUTTON STARTS HERE -->
<div class="preloader"></div>
<!--<section>-->
<!--    <div class="container">-->
<!--        <div class="row">-->
<!--            <div class="col-md-12">-->
<!--                <p class="backBtn">-->
<!--                    <a class="btnCustomStyle2 btn-solid" href="index.html">-->
<!--                        <span>Back</span>-->
<!--                    </a>-->
<!--                </p>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--</section>-->
<!-- BACK BUTTON END HERE -->
<section class="upperCircles">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <ul class="progressbar list-unstyled" style="width: 100%;">
               <li class="active" style=" width: 33%;"></li>
               <li id="active1"  style=" width: 33%;"></li>
               <li id="active2"  style=" width: 33%;"></li>
            </ul>
         </div>
      </div>
   </div>
</section>
<!-- FORM WIZARD STARTS HERE -->
<section class="mainFormWizard">
   <div class="container-fluid">
      <span id="theme_name" style="display: none;">sky</span>
      <span id="trans_name" style="display: none;">slide</span>
      <span id="rtl" style="display: none;">0</span>
      <div class="row">
         <div class="col-md-9">
            <ul>
               @foreach ($errors->all() as $error)
               <li>
                  <div class="alert alert-warning">
                     <strong>Warning!</strong> {{ $error }}
                  </div>
               </li>
               @endforeach
            </ul>
            <form id="wizard_example_2"  method="post" enctype="multipart/form-data" >
               <div id="loader1" style="position: relative;">
                  <!--<div id="loader2" class="center1" ></div> </br>-->
                  <h2 style="text-align: center;font-size:14px;">Please Waite ...</h2>
               </div>
               @csrf
               <fieldset>
                  <legend>Color</legend>
                  <div class="row">
                     <div class="col-lg-4">
                        <div class="form-group">
                           <div class="firstColor">
                              <label>Primary Color</label>
                              <input type="color" class="form-control"   id="Primary" name="primary"  style="height:80px;width:200px;" value="#f9004d">
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="form-group">
                           <div class="firstColor">
                              <label>Secondary Color</label>
                              <input type="color" class="form-control"   id="Secondary" name="scondary"  style="height:80px;width:200px;" value="#1e1c26">
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="form-group">
                           <div class="firstColor">
                              <label>Heading Color</label>
                              <input type="color" class="form-control"   id="text" name="heading" style="height:80px;width:200px;" value="#e17a7a">
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4">
                        <div class="form-group">
                           <div class="firstColor">
                              <label>Text Color</label>
                              <input type="color" class="form-control"   id="text" name="text" style="height:80px;width:200px;" value="#ffffff">
                           </div>
                        </div>
                     </div>
                  </div>
               </fieldset>
               <fieldset>
                  <legend>Social Links</legend>
                  @include('cards.socialmedia')
                  <!-- <div class="row">
                     <div class="col-md-12">
                        <span class="col" style="color:red;font-size:16px;"></span>
                        <ul class="list-unstyled socialLinksboxx">
                           @foreach($socialMedia as $social)
                           <li class="clearfix">
                              <span class="titleSocial" >
                              <i class="fa {{$social['icon']}}" aria-hidden="true"></i>{{$social['name']}}
                              </span>
                              @if($social['icon']=='fa-whatsapp')
                              <select class="form-control" name="code" style="width: 78px; height: 29px;float: left;margin-top: 0px;font-size: 12px;margin-left: 63px;" id="code">
                                 @foreach($va as $va1)
                                 <option value="{{$va1->callingCode}}">{{$va1->callingCode}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$va1->country}} </option>
                                 @endforeach
                              </select>
                              <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                              <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="7484564564">
                              </span>
                              @elseif($social['icon']=='fa-facebook')
                              <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                              <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.facebook.com">
                              </span>
                              @elseif($social['icon']=='fa-skype')
                              <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                              <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.skype.com">
                              </span>
                              @elseif($social['icon']=='fa-linkedin')
                              <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                              <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.linkedin.com">
                              </span>
                              @elseif($social['icon']=='fa-twitter')
                              <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                              <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://twitter.com">
                              </span>
                              @endif
                           </li>
                           @endforeach
                        </ul>
                     </div>
                  </div> -->
               </fieldset>
               <fieldset>
                  <legend>Profile </legend>
                  <div class="row">
                     <div class="col-md-12">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                           <div class="panel panel-default">
                              <div class="panel-heading" role="tab" id="headingOne">
                                 <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Personal Information
                                    </a>
                                 </h4>
                              </div>
                              <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                 <div class="panel-body personalBoxxx">
                                    @include('cards.createfor')
                                    <div class="row">
                                       <div class="col-md-12 col-lg-6">
                                          <div class="form-group ">
                                             <input type="text" class="form-control" name="first_name" placeholder="First Name" value="James">
                                             <small class="text-muted1 first_name"></small>
                                          </div>
                                       </div>
                                       <div class="col-md-12 col-lg-6">
                                          <div class="form-group ">
                                             <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="Anderson">
                                             <small class="text-muted1 last_name"></small>
                                          </div>
                                       </div>
                                       <div class="col-md-12 col-lg-6">
                                          <div class="form-group ">
                                             <input type="email" class="form-control" placeholder="Email" name="email" value="info@example.com">
                                             <small class="text-muted1 email"></small>
                                          </div>
                                       </div>
                                       <div class="col-md-12 col-lg-6">
                                          <div class="form-group ">
                                             <input type="text" class="form-control" placeholder="Designation" name="designation" value="Sales Manager">
                                             <small class="text-muted1 designation"></small>
                                          </div>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="form-group ">
                                             <textarea class="form-control" rows="3" name="about" placeholder="About me Description" maxlength="100">Lorem Ipsum is simply dummy text of the printing and typesetting.</textarea>
                                             <small class="text-muted1 about"></small>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="panel panel-default">
                              <div class="panel-heading" role="tab" id="headingfour">
                                 <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                    Contact Information
                                    </a>
                                 </h4>
                              </div>
                              <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                 <div class="panel-body businessInformation">
                                    <div class="row">
                                       <div class="col-md-12 col-lg-6 ">
                                          <div class="form-group">
                                             <input type="text" name="phone" class="form-control" placeholder="Primary Number" maxlength="10" value="+121251248574">
                                             <small class="text-muted1 phone"></small>
                                          </div>
                                       </div>
                                       <div class="col-md-12 col-lg-6 ">
                                          <div class="form-group ">
                                             <input type="text" class="form-control" placeholder="Enter your Official" name="address" maxlength="80" value="14 Sti Kilda Road, USA">
                                             <small class="text-muted1 address"></small>
                                          </div>
                                       </div>
                                       <div class="col-md-12 col-lg-6 ">
                                          <div class="form-group ">
                                            <input type="text" id="autocomplete" class="form-control" placeholder="Search your location" value="" onFocus="geolocate()">
                                             <small class="text-muted1 address"></small>
                                            <input type="hidden" name="location" id="latlong" value="">
                                          </div>
                                        </div>
                                       <div class="col-md-12 col-lg-6 ">
                                          <div class="form-group">
                                             <input type="text" class="form-control" placeholder="Website" name="website" value="www.example.com"> 
                                             <small class="text-muted1 website"></small>
                                          </div>
                                       </div>
                                       <div class="col-md-12">
                                          <div class="form-group">
                                             <div class="uploadBackground">
                                                <label>Image</label>
                                                <div class="row">
                                                   <div class="col-md-6">
                                                      <input type="file" id="upload1"  value="Choose Picture" accept="image/*" class="dropify" >
                                                      <input type="hidden" name="image" id="base64image1">
                                                   </div>
                                                   <div class="col-md-6">
                                                      <div class="upload-demo-wrap1">
                                                         <div id="upload-demo1"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </fieldset>
               <div class="col-md-12">
                  <p class="backBtn">
                     <a class="btnCustomStyle2 btn-solid" href="javascript:void(0);">
                     <span>Preview</span>
                     </a>
                  </p>
               </div>
            </form>
         </div>
         <div class="col-md-3">
            <div id="frame" style="background-image: url('{{ url("cardslayout")}}/img/Phone1.png');background-color: #cccccc;background-position: center;background-repeat: no-repeat;background-size: cover;
            position: relative;width: 263px;height: 535px;border-radius: 30px;"></br>
            <div id="loader" class="center" ></div>
            <div id="previewImage" ></div>
         </div>
         <div id="img">
            <img class="img-fluid mx-auto d-block "  src="{{ url('cardslayout')}}/img/features.png" alt="">
         </div>
      </div>
   </div>
   <style>
      .abtSec {
      background: transparent;
      }
      .topHeader {
      background: transparent; 
      /*border-radius: 20px 20px 0px 0px;*/
      }
      .wrapper {
      background: transparent;
      /*border-radius: 20px 20px 20px 20px;*/
      }
      p{
      font-size:13px;
      }
      h6{
      font-size:12px;
      }
      element.style {
      }
      .socialMedia li .fa {
      font-size: 22px;
      text-align: center;
      text-decoration: none;
      width: 35px;
      height: 35px;
      line-height: 35px;
      /* color: #c8c8c8; */
      }
   </style>
   <div id="html-content-holder" style="width: 333px;">
      <div >
         <section class="wrapper" style="display: block;height: 660px;width: 320px;margin-left: 13px;margin-top: 14.1px;border-radius: 30px;" id="wrapper">
            <!-- HEADER STARTS HERE -->
            <header>
               <section class="topHeader" style="padding: 30px 30px 56px 30px;border-radius: 30px 30px 0px 0px;">
                  <div class="firstName" style="font-size:25px;">James</div>
                  <div class="lastName" style="font-size:20px;">Anderson</div>
                  <div class="designation" style="font-size:14px;">Sales Manager</div>
               </section>
            </header>
            <!-- HEADER END HERE -->
            <!-- ABOUT STARTS HERE -->
            <section class="abtSec" style="padding: 40px 10px 0 20px;">
               <table class="">
                  <tr>
                     <p class="userImg"></p>
                  </tr>
                  <tr>
                     <td style="width:100%;">
                        <h1 id="h1" style="font-size:13px">About Me</h1>
                     </td>
                  </tr>
                  <tr>
                     <td style="width:100%;">
                        <p class="content" style="text-align: justify;"></p>
                     </td>
                  </tr>
               </table>
               </p>
            </section>
            <!-- ABOUT END HERE -->
            <!-- CONTACT STARTS HERE -->
            <section class="infoSection"  style="padding: 0 20px;margin-top:-20px;">
               <h2 id="h2" style="font-size:13px"><span>contact Me</span></h2>
               <table class="list-unstyled">
                  <tr id="location">
                  </tr>
                  <tr id="phone">
                  </tr>
                  <tr id="email">
                  </tr>
               </table>
            </section>
            <!-- CONTACT END HERE -->
            <!-- CONTACT STARTS HERE -->
            <section class="searchMe" style="padding: 0 20px;">
               <h2 style="font-size:13px">search Me @</h2>
               <table align="center">
                  <tr>
                     <td>
                        <ul class="list-unstyled socialMedia d-flex" style="text-align:center;">
                        </ul>
                     </td>
                  </tr>
               </table>
            </section>
            <!-- CONTACT END HERE -->
            <!-- FOOTER STARTS HERE -->
            <footer>
               <section class="footer">
                  <!--<p class="webLink">-->
                  <!--</p>-->
                  <!--<p class="nameUser"></p>-->
                  <table>
                     <tr class="webLink1">
                     </tr>
                     <tr class="nameUser1">
                     </tr>
                  </table>
               </section>
            </footer>
            <!-- FOOTER END HERE -->
         </section>
      </div>
   </div>
   </div>
   <script>
      demoUpload1();
      
   </script>
</section>
<!-- FORM WIZARD END HERE -->
@endsection
@section('scripts')
<script src="{{ url('cardslayout/js/html2canvas.js') }}"></script>
<script src="{{ url('cardslayout/js/form-wizard.js') }}"></script>
<script src="{{ url('cardslayout/js/jscolor.js') }}"></script>
<script src="{{ url('cardslayout/js/dropify.js') }}"></script>
<style>
   canvas {
   /*height: 582px;*/
   height: 507px;
   width: 245px;
   /*margin-top: -84px;*/
   margin-top: -7px;
   border-radius: 25px;
   margin-left: 5px;
   }
</style>
<script>
   jscolor.presets.default = {
       borderColor:'rgba(236,239,245,0.6)', width:200, height:100,
       buttonColor:'rgba(70,51,255,1)', shadowColor:'rgba(0,0,0,0.05)'
   };
</script>
<script>
   $(document).ready(function(){
      
        $('#myModal').hide();
         $('#h1').hide();
         $('#h2').hide();
         $('.searchMe').hide();
       //    $('.btnCustomStyle2').hide();
         $('#code').val('+91');
         $('#loader').hide(); 
            $('#loader1').hide(); 
         $('#wrapper').hide();
         $('.preloader').fadeOut('slow');
       view();
       // $('.sf-btn-finish').click(function(){
       //         $('.preloader').fadeIn('slow');
       //           setTimeout(function(){ $('.preloader').fadeOut('slow');  }, 4000);
              
       // });
       // $('input[type="color"]').change(function(){
       //   $('.btnCustomStyle2').show();
       // });
       // Basic
       $('.dropify').dropify();
   
       // Translated
       $('.dropify-fr').dropify({
           messages: {
               default: 'Glissez-déposez un fichier ici ou cliquez',
               replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
               remove:  'Supprimer',
               error:   'Désolé, le fichier trop volumineux'
           }
       });
   
       // Used events
       var drEvent = $('#input-file-events').dropify();
   
       drEvent.on('dropify.beforeClear', function(event, element){
           return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
       });
   
       drEvent.on('dropify.afterClear', function(event, element){
           alert('File deleted');
       });
   
       drEvent.on('dropify.errors', function(event, element){
           console.log('Has Errors');
       });
   
       var drDestroy = $('#input-file-to-destroy').dropify();
       drDestroy = drDestroy.data('dropify')
       $('#toggleDropify').on('click', function(e){
           e.preventDefault();
           if (drDestroy.isDropified()) {
               drDestroy.destroy();
           } else {
               drDestroy.init();
           }
       });
      
   
          
        $('.btnCustomStyle2').click(function(){
             $('.preloader').fadeIn('slow');
              $('#loader').show();
              
       //   var  files =$('#input-file-now').prop('files')[0];
       
         
           var form_data = new FormData($('#wizard_example_2')[0]);
           //   form_data.append("file", files);
               
   
                  $.ajax({
                       type: 'post',
                       url:  "{{route('admin.Cards.card')}}",
                       
                       data: form_data,
                       async: false,
                       dataType:'json',
                       success: function (data1) {
                       var    data=data1.va;
                           console.log(data);
                           
                      
                           $('.contactIcon').css('background',data.scondary);
                            if(data.image!==null){
                                
                               
                            $('.userImg').html('<td style="width:70%;"></td><td style="border:3px solid '+data.primary+';width:84px;border-radius: 42px;" align="right"><img class="img-fluid img-fluid1" src="'+data.image+'" alt="userimage" ></td>');
                            }else{
                               var userimg ="{{url('/cardslayout/design-image/design-1/userimage.png')}}";
                                 $('.userImg').html('<td style="width:70%;"></td><td style="border:3px solid '+data.primary+';width:84px;border-radius: 42px;" align="right"><img class="img-fluid img-fluid1" src="'+userimg+'" alt="userimage" ></td>') 
                            }
                           
                           $('.firstname').text(data.first_name);
                           $('.lastname').text(data.last_name);
                           $('.designation').text(data.designation);
                            if(data.about!==null){
                                  $('#h1').show();
                                   $('#h1').css('color',data.text);
                           $('.content').text(data.about); 
                            }
                             if(data.first_name!==null){
                           $('.nameUser1').html('<td style="width:600px;text-align:center;"><p  style="text-align:center;font-size: 11px;color:'+data.text+'"><span>'+data.first_name+'</span>&nbsp;<span>'+data.last_name+'</span></p></td>'); 
                             }
                              if(data.address!==null || data.phone!==null || data.email!==null){
                               $('#h2').show();
                              
                         }
                            if(data.address!==null){
                           $('#location').html('<td style="width:40px;height:40px;"><p style="width:40px;font-size:20px;text-align:center;height:40px;border-radius: 20px;background:transparent;;"><i  class="fa fa-map-marker" aria-hidden="true" style="font-size:33px;margin-left:0px;margin-top:1px;margin-top: -3px;color:'+data.text+';" ></i></p></td><td><div class="contactInfo" style="margin-left: 5px;line-height: 0.5;"><h6 style="font-weight: 700;font-size: 12px;color:'+data.heading+';">Location</h6><p style="line-height: 1.1;height: 30px;margin-top: -5px;color:'+data.text+';">'+data.address+'</p></div></td>');
                            }
                             if(data.phone!==null){
                           $('#phone').html('<td style="width:40px;height:40px;"><p style="width:40px;font-size:20px;text-align:center;height:40px;border-radius: 20px;background:transparent;"><i class="fa fa-mobile" aria-hidden="true" style="font-size:35px;margin-left:1px;margin-top:3px;color:'+data.text+';"></i></p></td><td><div class="contactInfo" style="margin-left: 5px;line-height: 0.5;"><h6 style="font-weight: 700;font-size: 12px;color:'+data.heading+';">Phone Number</h6><p style="color:'+data.text+';"><a href="#">'+data.phone+'</a></p></div></td>'); 
                             }
                              if(data.email!==null){
                           $('#email').html('<td style="width:40px;height:40px;"><p style="width:40px;font-size:20px;text-align:center;height:40px;border-radius: 20px;background:transparent;"><i   class="fa fa-envelope" aria-hidden="true" style="font-size:23px;margin-left:1px;margin-top:6px;color:'+data.text+';"></i></p></td><td> <div class="contactInfo" style="margin-left: 5px;line-height: 0.5;"><h6 style="font-weight: 700;font-size: 12px;color:'+data.heading+';">Email Address</h6><p style="color:'+data.text+';"><a href="#">'+data.email+'</a></p></div></td>');
                              }
                               if(data.website!==null){
                                   
                             $('.webLink1').html('<td style="width:600px;"><p id="p" class="webLink" style="color:'+data.scondary+';font-size: 12px;"><a href="#" style="text-align:center;color:'+data.text+'"><i class="fa fa-globe" aria-hidden="true"></i> &nbsp; '+data.website1.substr(8)+'</a>  </p></td>');
                               }
                           
                           
                           var i;
                           var html='';
                         if(data.social_media[1]!==null || data.social_media[2]!==null || data.social_media[3]!==null){
                              $('.searchMe').show();
                         }
                         //var social_media = JSON.parse(data.social_media);
                         console.log('>>>>>>>>>>>>>>>>>>',data.social_media.length);

                              for(var i in data.social_media){
                                  if(data.social_media[i]!==null)
                                  html +='<li style=" background:'+data.scondary+'"><a href="'+data.social_media[i]+'" class="fa '+data.socialMediaList[i]+'"></a></li>';
                              }
                            $('.socialMedia').html(html);
                            $(".content").css("color",data.text);
                            $("#p").css("color",data.scondary);
                            $("h1").css("color",data.scondary);
                            $('#h2 span').css('color',data.text);
                            $('#h1').css('color',data.text);
                            $("h2").css("color",data.scondary);
                       
                            $(".topHeader").css("background",data.scondary);
                            $(".topHeader").css("color",data.text);
                            $(".abtSec").css("background",data.primary);
                            $(".wrapper").css("color",data.text);
                            $(".wrapper").css("background",data.primary);
                            $(".wrapper").css("display","block");
                            $('.preloader').fadeOut('slow');
                            $('#img').hide(); 
                              setTimeout(function(){ $('#loader').hide();  }, 4000);
                              dview();
                              //$('#myModal').show();
                       },
                       cache: false,
                       contentType: false,
                       processData: false
                   });
              
   
    });
      
    $('button').click(function(){
        $('#myModal').hide(); 
    });   
       
       $('.finish-btn').click(function(){
            $('form').attr('action', "{{route('admin.Cards.generate_pdf')}}");
          
           var pdfgen ="{{route('admin.Cards.generate_pdf')}}";
   
            $('form').attr('target','popup');
            $('form').attr(' onclick',"window.open("+pdfgen+",'popup','width=800,height=800');");
           
       });
       
       function view(){
       
           $.ajax({
               url:"{{route('admin.Cards.delete-image')}}",
               method:"get",
               success: function(data) {
                   console.log(data);
               }
               
           });
       }
       
       
       
   // global variable
   
   function dview() {
    var element = $("#html-content-holder"); // global variable
   var getCanvas; 
   
    html2canvas(element, {
         x: 50, y: 100,
         draggable: true,
    onrendered: function (canvas) {
            
           $("#previewImage").html(canvas);
           getCanvas = canvas;
           console.log(canvas);
        }
    });
        
     $('#frame').show(); 
    $('#wrapper').hide(); 
   $('#img').hide(); 
   
   }
   $('#frame').hide(); 
   $('#wrapper').hide();  
   
   
   //("#Secondary option[value="+ color +"]").prop('disabled', true);              
   //   $("#text option[value=" + color + "]").prop('disabled', true);
   //     $("#Primary option[value="+ color +"]").prop('disabled', true);
   
   
   $("#Primary").change(function(){
   var color=$("#Primary").val();
   $("#Primary option[value="+ color +"]").prop('disabled', false);
   $("#Secondary option[value="+ color +"]").prop('disabled', true);
   $("#text option[value=" + color + "]").prop('disabled', true);
   }) ;   
   
   $("#Secondary").change(function(){
   var color=$("#Secondary").val();
   $("#Secondary option[value="+ color +"]").prop('disabled', false);
   $("#Primary option[value="+ color +"]").prop('disabled', true);
   $("#text option[value=" + color + "]").prop('disabled', true);
   }) ;   
   
   $("#text").change(function(){
   var color=$("#text").val();
   $("#text option[value=" + color + "]").prop('disabled', false);
   $("#Primary option[value="+ color +"]").prop('disabled', true);
    $("#Secondary option[value="+ color +"]").prop('disabled', true);
   }) ;   
   
   //  $('.sf-controls').css("display","none");
   
   $('input[type="color"]').change(function(){
        var select1=$('#Primary').val();
        var select2=$('#Secondary').val();
        var select3=$('#text').val();
        if(select1!=='Choose Color' && select2!=='Choose Color' && select3!=='Choose Color'){
            $('.sf-controls').css("display","block");
              $('#active1').attr('class','active');
        }
       
   });
   
   $('.next-btn').click(function(){
         $('.sf-controls').css("display","none");
   });   
   
   
    $('.socialLinksboxx').on("focusout","input",function(){
        
      
        var input1=$('input[name="social_media[1]"]').val();
        var input2=$('input[name="social_media[2]"]').val();
        var input3=$('input[name="social_media[3]"]').val();
        var input4=$('input[name="social_media[4]"]').val();
        var input5=$('input[name="social_media[5]"]').val();
        
        if(input1!=='' && input2!=='' && input3!=='' && input4!=='' && input5!==''){
              $('.sf-controls').css("display","block");
                $('#active2').attr('class','active');
        }else{
              $('.sf-controls').css("display","none");
              $('.col').text('*Please fill all the boxes');
        }
       
   });
   
   
   
    $('.panel-group').on("focusout","input",function(){
       
        var first_name=$('input[name="first_name"]').val();
        var last_name=$('input[name="last_name"]').val();
        var email=$('input[name="email"]').val();
        var designation=$('input[name="designation"]').val();
        var about=$('textarea').val();
        var image=$('input[name="image"]').val();
        var phone=$('input[name="phone"]').val();
        var address=$('input[name="address"]').val();
        
        
                         if(first_name.length <31){
                           
                             first_name1=first_name;
                         }else{
                             $('.first_name').text('First Name max 30 Char');
                               // alert('*First Name max 30 Char');
                         }
                         
                          
                         
                         if(last_name.length <31){
                            
                             last_name1=last_name;
                         }else{
                              $('.last_name').text('Last Name max 30 Char');
                              //alert('*Last Name max 30 Char');
                         }
                         
                         if(about.length <150){
                            
                             about1=about;
                         }else{
                              // alert('About max 300 Char');
                                $('.about').text('*About max 150 Char');
                         }
                         
                       if(address.length <100){
                             
                             address1=address;
                         }else{
                            // alert('About max 255 Char');
                             $('.address').text('*Address max 100 Char');
                         }
                         
                         if(designation.length <30){
                             
                             designation1=designation;
                         }else{
                             //alert('Designation max 255 Char');
                             $('.designation').text('* Designation max 30 Char');
                         }
                           if(email!==''){
                         if(isEmail(email)==true){
                            
                             email1=email;
                              $('.email').css('color','green');
                             $(".email").text('*Email Valid..!');
                         }else{
                              //alert('Is not E-mail');
                               $('.email').css('color','red');
                               $('.email').text('*Is not E-mail');
                         }
                           }
                         if(phone!==''){
                         if(validatePhone(phone)==true){
                            
                             phone1=phone;
                             $('.phone').css('color','green');
                             $(".phone").text('*Phone Valid..!');
                         }else{
                              //alert('Is not Valid..!');
                                $('.phone').css('color','red');
                              $('.phone').text('*Is not Valid Phone No... only no!');
                         }
                         }
                        
        if(first_name1!=='' && last_name1!=='' && email1!=='' && designation1!==''  && about1!=='' && address1!=='' && phone1!==''){
            // alert(first_name+" "+last_name+" "+email+" "+designation+"  "+about+" "+phone+" "+address+" "+website);
            $('.sf-controls').css("display","block");
        }else{
             $('.sf-controls').css("display","none");
        }
       
   });
   
   
   function isEmail(email) {
     var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
     return regex.test(email);
     if (regex.test(email)) {
            return true;
          }
       else {
             return false;
        }
   }
   
   
   function validatePhone(Phone) {
   
   var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
   if (filter.test(Phone)) {
   return true;
   }
   else {
   return false;
   }
   }
               
   });
</script>
@endsection