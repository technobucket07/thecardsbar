@extends('cards.layouts.master')
@section('css')
         <link href="{{ url('web_assets/css/form-wizard.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/css/dropify.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/bootstrap.min.css')}}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/font-awesome.min.css')}}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/main.css')}}" rel="stylesheet">
                
         <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/demo/prism.css" />
         <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/bower_components/sweetalert/dist/sweetalert.css" />
	     <link rel="Stylesheet" type="text/css" href="{{ url('web_assets/js/style1.css?abc')}}" />
	     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	     <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
		 <script src="https://foliotek.github.io/Croppie/croppie.js"></script>
		 <script src="{{ url('web_assets/js/crop_js/script7.js')}}"></script>
    <style>
        .text-muted1 {
     color: red;
        }
        .progressbar li {
         
        }
        
        #loader { 
      border: 12px solid #f3f3f3; 
      border-radius: 50%; 
      border-top: 12px solid #444444; 
      width: 70px; 
      height: 70px; 
      animation: spin 1s linear infinite; 
    } 
    
    @keyframes spin { 
      100% { 
        transform: rotate(360deg); 
      } 
    } 
    
    .center { 
      position: absolute; 
      top: 0; 
      bottom: 0; 
      left: 0; 
      right: 0; 
      margin: auto; 
    } 
    

    </style>     
    
       
@section('content')
    <!-- BACK BUTTON STARTS HERE -->
 

    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="backBtn">
                        <a class="btnCustomStyle2 btn-solid" href="index.html">
                            <span>Back</span>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- BACK BUTTON END HERE -->

    <section class="upperCircles">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="progressbar list-unstyled" style="width: 100%;">
                        <li class="active" style=" width: 33%;"></li>
                        <li id="active1"  style=" width: 33%;"></li>
                        <li id="active2"  style=" width: 33%;"></li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- FORM WIZARD STARTS HERE -->
    <section class="mainFormWizard">
        <div class="container-fluid">
            <span id="theme_name" style="display: none;">sky</span>
            <span id="trans_name" style="display: none;">slide</span>
            <span id="rtl" style="display: none;">0</span>
            <div class="row">
                <div class="col-md-9">
    <ul>
            @foreach ($errors->all() as $error)
           
              <li> <div class="alert alert-warning">
               <strong>Warning!</strong> {{ $error }}
              </div></li>
            @endforeach
        </ul>
                    <form id="wizard_example_2"  method="post" enctype="multipart/form-data" >
                       
                        @csrf
                        <fieldset>
                            <legend>Color</legend>
                            <div class="row">
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Primary Color</label>
                                             <input type="color" class="form-control" id="Primary" name="primary" style="height:80px;width:200px;" value="#ffffff">
                                           
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Secondary Color</label>
                                             <input type="color" class="form-control" id="Secondary" name="scondary" style="height:80px;width:200px;" value="#f5f5f5">
                                        
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Text Color</label>
                                               <input type="color" class="form-control" id="text" name="text" style="height:80px;width:200px;" value="#212529">
                                         
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Heading Color</label>
                                        <input type="color" class="form-control" id="heading" name="blog_color" style="height:80px;width:200px;" value="#b3b3b3">
                                          
                                           
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                
                                
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Button Color</label>
                                             <input type="color" class="form-control" id="heading1" name="button_color" style="height:80px;width:200px;" value="#ffbd00">
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Contact Color</label>
                                             <input type="color" class="form-control" id="heading2" name="contact_color" style="height:80px;width:200px;" value="#bf1c1d">
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                
                                
                               
                            </div>
                        </fieldset>
                      
                        <fieldset>
                            <legend>Social Links</legend>
                            @include('cards.socialmedia')
                            <!-- <div class="row">
                                <div class="col-md-12">
                                    <span class="col" style="color:red;font-size:16px;"></span>
                                    <ul class="list-unstyled socialLinksboxx">
                                        @foreach($socialMedia as $social)
                                           @if($social['icon']=='fa-whatsapp')
                                            <li class="clearfix">
                                                 <span class="titleSocial" >
                                                     <i class="fa {{$social['icon']}}" aria-hidden="true"></i>{{$social['name']}}
                                                 </span>
                                              
                                              
                                                 <select class="form-control" name="code" style="width: 78px; height: 29px;float: left;margin-top: 0px;font-size: 12px;margin-left: 63px;" id="code">
                                                    @foreach($va as $va1)
                                                   <option value="{{$va1->callingCode}}">{{$va1->callingCode}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$va1->country}} </option>
                                                    @endforeach
                                                </select>
                                                
                                                 <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="7654764389">
                                                 </span>
                                            </li>
                                          @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div> -->
                        </fieldset>
                        
                        
                      <fieldset>
                            <legend>Profile </legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        
                                              <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne1">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        Image Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne1">
                                                
                                                <div class="panel-body personalBoxxx">
                                                    <div class="row">
                                                        
                                                        
                                                          <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Profile Image</label>
                                                                    <!--<input type="file" name="profile_image" id="input-file-now" class="dropify" />-->
                                                                    
                                                                    <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload1"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="profile_image" id="base64image1">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap1">
                                                                         <div id="upload-demo1"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Service Image</label>
                                                                    <!--<input type="file" name="service_image" id="input-file-now1" class="dropify" />-->
                                                                    
                                                                     <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload2"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="service_image" id="base64image2">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap2">
                                                                         <div id="upload-demo2"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
                                                        Personal Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                
                                                <div class="panel-body personalBoxxx">
                                                  @include('cards.createfor')
                                                    <div class="row">
                                                        
                                                        
                                                          
                                                        
                                                        
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" name="first_name" placeholder="Enter First Name" value="Michael">
                                                                <small class="text-muted1 first_name1"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Enter Last Name" name="last_name" value="Christophar">
                                                                <small class="text-muted1 last_name"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" name="email" class="form-control" placeholder="Phone Email" value="information@gmail.com">
                                                                    <small class="text-muted1 email"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Designation" name="designation" maxlength="40" value="Plumber">
                                                                 <small class="text-muted1 designation"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" name="phone" class="form-control" placeholder="Phone Number " maxlength="10" value="1212512480">
                                                                    <small class="text-muted1 phone1"></small>
                                                            </div>
                                                        </div>
                                                        
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" name="bottom_title" class="form-control" placeholder="Bottom Title " maxlength="12" value="Christophar">
                                                                    <small class="text-muted1 bottom_title"></small>
                                                            </div>
                                                        </div>
                                                      <div class="col-md-12 col-lg-6 ">
                                                        <div class="form-group ">
                                                          <input type="text" class="form-control" placeholder="Enter your Official Address" name="address" value="14 Sti Kilda Road, New York, USA">
                                                          <small class="text-muted1 address"></small>
                                                        </div>
                                                      </div>
                                                        <div class="col-md-12 col-lg-6">
                                                        <div class="form-group ">
                                                          <input type="text" id="autocomplete" class="form-control" placeholder="Search your location" value="" onFocus="geolocate()">
                                                           <small class="text-muted1 address"></small>
                                                          <input type="hidden" name="location" id="latlong" value="">
                                                        </div>
                                                      </div>
                                                      
                                                     
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
           
                       
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingfour">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                                        Service Name
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                                <div class="panel-body businessInformation">

                                                    <div class="row">

                                                       
                                                        
                                                     
                                                        
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <label>Service Name 1</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 1" name="service-name-1" maxlength="18"  value="Leak">
                                                                 <small class="text-muted1 service-name-1"></small>
                                                            </div>
                                                        </div>
                                                      
                                                        
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <label>Service Name 2</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 2" name="service-name-2" maxlength="18" value="Uncorking">
                                                                 <small class="text-muted1 service-name-2"></small>
                                                            </div>
                                                        </div>
                                                       
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <label>Service Name 3</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 3" name="service-name-3" maxlength="18" value="Maintance">
                                                                 <small class="text-muted1 service-name-3"></small>
                                                            </div>
                                                        </div>
                                                       
                                                        
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <label>Service Name 4</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 4" name="service-name-4" maxlength="18" value="Installation">
                                                                 <small class="text-muted1 service-name-4"></small>
                                                            </div>
                                                        </div>
                                                        
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <label>Service Name 5</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 5" name="service-name-5" maxlength="18" value="Reparation">
                                                                 <small class="text-muted1 service-name-5"></small>
                                                            </div>
                                                        </div>
                                                       
                                                        
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <label>Service Name 6</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 6" name="service-name-6" maxlength="18" value="Outdoor">
                                                                 <small class="text-muted1 service-name-6"></small>
                                                            </div>
                                                        </div>
                                                       
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                             
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                            <div class="col-md-12">
                                <p class="backBtn">
                                    <a class="btnCustomStyle2 btn-solid Preview1 Preview2" href="javascript:void(0);">
                                        <span>Preview</span>
                                    </a>
                                </p>
                            </div>
                          
                       </form>
                    </div>
                <div class="col-md-3">
                    
                    <div id="frame" style="background-image: url('{{ url("web_assets")}}/img/Phone1.png');background-color: #cccccc;background-position: center;background-repeat: no-repeat;background-size: cover;
  position: relative;width: 263px;height: 528px;border-radius: 30px;"></br>
                         <div id="previewImage" ></div>
                         <div id="loader" class="center" ></div> 
                          <div id="img" style="width: 235px;border-radius: 30px;margin-left: 15px;margin-top: -9px;">
                         <img class="img-fluid mx-auto d-block "  src="{{ url('web_assets')}}/img/desine-7.png" alt="" style="border-radius: 15px;height:500px;">
                    </div>
                  
                    </div>
                   
      
                   
                   
                </div>
                
    <style>


/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #3055c6; }
::selection { color: #f1f1f1;background:; }



body  {font-family: 'Poppins', sans-serif;}
.wrapper {width: 420px;margin: 0 auto;height: 825px;background: #fff;}

.profilePicBoxx {padding:30px;margin: 0 0 15px 0;background: #f5f5f5;border-radius: 0 0 30px 30px;}
.profilePicBoxx:after {display: block;clear: both;content: "";}
.profilePicBoxx .userImg {width: 115px;height: 115px;
/*border: 3px solid #d8d8e7;*/
float: right;border-radius: 57px;}
.profilePicBoxx .userImg img {}
.profilePicBoxx .propNameDesignation {padding: 10px 125px 0 0;text-align: left;}
.profilePicBoxx .propNameDesignation h3 {font-size: 30px;font-weight: 600;margin: 0;}
.profilePicBoxx .propNameDesignation h4 {font-size: 30px;font-weight: 600;margin: 0;}
.profilePicBoxx .propNameDesignation p {padding: 0 0 0 2px; color:#bf1c1d;font-size: 20px;}


.infoSection {padding: 0 30px;margin: 30px 0;}
.infoSection ul {}
.infoSection ul li {margin: 20px 0;}
.infoSection ul li .contactIcon {float: left;width: 35px;height: 36px;background: #fffd3e;text-align: center;border-radius: 50px;margin: 0 10px 0 0;}
.infoSection ul li .contactIcon i {font-size: 36px;}
.infoSection ul li .contactIcon i.messageIcon {font-size: 24px;line-height: 33px;}
.infoSection ul li .contactInfo {margin-top: 10px;}
.infoSection ul li .contactInfo h6 {margin: 0 0 4px 0;color: #b3b3b3;font-size: 14px;}
.infoSection ul li .contactInfo p {font-weight:600;font-size: 16px;margin: 0;}
.infoSection ul li .contactInfo p a {color: #1e1c26;}



.services {padding: 0 30px;}
.services ul {}
.services ul:after {display: block;clear: both;content: "";}
.services ul .leftSide {float: left;width: 47%;padding: 20px 10px 0 0;}
.services ul .leftSide .boxxService {}
.services ul .leftSide .boxxService li {background: #ffbd00;font-size: 16px;font-weight: 500;text-align: center;border-radius: 18px;padding: 5px 0;margin: 0 0 15px 0;}

.services ul .leftSide .boxxService li:last-child {margin: 0;}




.services ul .rightSide {float: right;width: 53%;}
.services ul .rightSide .wrokerimg {text-align: right;}
.services ul .rightSide .wrokerimg img {height: 270px;width: 100%;     margin-top: 18px;}





.bottomIcons {padding: 0 40px;margin: 25px 0 0 0;}
.bottomIcons ul{margin: 0;}
.bottomIcons ul li.phone {display: inline-block;width: 20%;   float: left;}
.bottomIcons ul li.phone a {font-size: 30px;width: 60px;height: 60px;line-height: 65px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;    background: #bf1c1d;color: #fff;}
.bottomIcons ul li.phone a .fa {font-size: 32px; margin-top:14px;}
.bottomIcons ul li.phone a .fa:hover {opacity: 0.7;}

.bottomIcons ul li.middelLine {    float: left; padding: 0 5px;    margin: 4px 0 0 0;   width: 60%;}
.bottomIcons ul li.middelLine h6 {     margin: 0;  font-size: 14px;text-align: right;font-weight: 600;}
.bottomIcons ul li.middelLine p {   line-height: 36px;   margin: 0; font-size: 30px;text-align: right;font-weight: 600;}

.bottomIcons ul{margin: 0;}
.bottomIcons ul:after {display: block;clear: both;content: "";}
.bottomIcons ul li.whatsApp {display: inline-block;width: 20%;      text-align: right; float: right;}
.bottomIcons ul li.whatsApp a {  font-size: 30px;width: 60px;height: 60px;line-height: 60px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;    background: #bf1c1d;color: #fff;}
.bottomIcons ul li.whatsApp a .fa {font-size: 32px; margin-top:14px;}
.bottomIcons ul li.whatsApp a .fa:hover {opacity: 0.7;}










/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


}


        </style>
     
    <div style="opacity: 0;">      
<div id="html-content-holder">
    
    <section class="wrapper" id="wrapper">

            <div class="profilePicBoxx">
                <div class="userImg">
                    
                </div>
                <div class="propNameDesignation">
                    <h3></h3>
                    <h4></h4>
                    <p></p>
                </div>
            </div>

            <div class="infoSection">
                <ul class="list-unstyled">
                    <li>
                        <div class="contactInfo contactInfo1">
                      
                        </div>
                    </li>
                    <li>
                        <div class="contactInfo contactInfo2">

                        </div>
                    </li>
                    <li>
                        <div class="contactInfo contactInfo3">
                           
                        </div>
                    </li>
                </ul>
            </div>


            <div class="services">
                <ul class="list-unstyled">
                    <li class="leftSide">
                        <ul class="list-unstyled boxxService">
                            
                        </ul>
                    </li>
                    <li class="rightSide">
                        <p class="wrokerimg">
                            
                        </p>
                    </li>
                </ul>
            </div>

            <div class="bottomIcons">
                    <ul class="list-unstyled">
                        <li class="phone">
                           
                        </li>
                        <li class="middelLine">
                          
                        </li>
                        <li class="whatsApp">
                           
                        </li>
                    </ul>
                </div>

        
        </section>

           
 
 </div> 
  </div>            
            
            
            
        </div>
        

        
<script>
demoUpload1();
demoUpload2();

</script>
        
        
    </section>
    <!-- FORM WIZARD END HERE -->
@endsection

@section('scripts')
    <script src="{{ url('web_assets/js/html2canvas.js') }}"></script>
    <script src="{{ url('web_assets/js/form-wizard.js') }}"></script>
    <script src="{{ url('web_assets/js/jscolor.js') }}"></script>
    <script src="{{ url('web_assets/js/dropify.js') }}"></script>
     
<style>
    canvas {
    height: 502px;
    width: 238px;
    margin-top: -10px;
    border-radius: 27px;
    margin-left: 13px;

    }
</style>
    <script>
        jscolor.presets.default = {
            borderColor:'rgba(236,239,245,0.6)', width:200, height:100,
            buttonColor:'rgba(70,51,255,1)', shadowColor:'rgba(0,0,0,0.05)'
        };
    </script>

    <script>
        $(document).ready(function(){
            	$('#myModal').hide();
            	 $('#h1').hide();
            	 $('#h2').hide();
            	 $('.searchMe').hide();
            	// $('.btnCustomStyle2').hide();
            	 $('#code').val('+91');
            	 $('#loader').hide(); 
            	 $('#wrapper').hide();
             $('.companyPic').hide();
             $('.proprietorName').hide();
            view();
            $('input[type="color"]').change(function(){
              $('.btnCustomStyle2').show();
            });
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });
           
            
               
             $('.btnCustomStyle2').click(function(){
                     $('#wrapper').show();
                  	$('#loader').show();
            //   var  files =$('#input-file-now').prop('files')[0];
            
              
                var form_data = new FormData($('#wizard_example_2')[0]);
                //   form_data.append("file", files);
                    
   
                       $.ajax({
                            type: 'post',
                            url:  "{{route('admin.Cards.cards7')}}",
                            
                            data: form_data,
                            async: false,
                            dataType:'json',
                            success: function (data1) {
                            var    data=data1.va;
                                console.log(data);
                                if(data.profile_image!==null){
                                      $('.userImg').html('<img class="img-fluid" src="'+data.profile_image+'" alt="profilepic" >');
                                      $('.userImg').css('border','3px solid '+data.blog_color);
                                }else{
                                  var img1 ="{{url('/web_assets/design-image/design-7/profilepic.jpg')}}";
                                     $('.userImg').html('<img class="img-fluid" src="'+img1+'" alt="profilepic" >');
                                      $('.userImg').css('border','3px solid '+data.blog_color);
                                }
                                if(data.service_image!==null){
                                   
                                     $('.wrokerimg').html('<img class="img-fluid" src="'+data.service_image+'" alt="workerpic">');
                                 }else{
                                  var img2 ="{{url('/web_assets/design-image/design-7/workerpic.jpg')}}";
                                     $('.wrokerimg').html('<img class="img-fluid" src="'+img2+'" alt="workerpic">');
                                 }
                                 
                                
                             
                                if(data.first_name!==null && data.last_name!==null){
                                    $('.propNameDesignation h3').css('color',data.text);
                                    $('.propNameDesignation h4').css('color',data.text);
                                    $('.propNameDesignation h3').text(data.first_name);
                                    $('.propNameDesignation h4').text(data.last_name);
                                }
                                
                                if(data.designation!==null){
                                   $('.propNameDesignation p').text(data.designation);
                                   $('.propNameDesignation p').css('color',data.contact_color);
                                }
                                
                                if(data.phone!==null){
                                    $('.phone').html('<a href="'+data.code+data.phone+'" style="background:'+data.contact_color+';"><i class="fa fa-phone" aria-hidden="true" style="color:'+data.primary+';"></i></a>');
                                }
                            var html1='';
                                if(data.servicename1!==null){
                                     html1+='<li style="background:'+data.button_color+';color:'+data.text+';">'+data.servicename1+'</li>';
                                  
                                }
                                
                                 if(data.servicename2!==null){
                                     html1+='<li style="background:'+data.button_color+';color:'+data.text+';">'+data.servicename2+'</li>';
                                  
                                }
                                 if(data.servicename3!==null){
                                     html1+='<li style="background:'+data.button_color+';color:'+data.text+';">'+data.servicename3+'</li>';
                                  
                                }
                                 if(data.servicename4!==null){
                                     html1+='<li style="background:'+data.button_color+';color:'+data.text+';">'+data.servicename4+'</li>';
                                  
                                }
                                 if(data.servicename5!==null){
                                     html1+='<li style="background:'+data.button_color+';color:'+data.text+';">'+data.servicename5+'</li>';
                                  
                                }
                                 if(data.servicename6!==null){
                                     html1+='<li style="background:'+data.button_color+';color:'+data.text+';">'+data.servicename6+'</li>';
                                  
                                }
                                $('.boxxService').html(html1);
                                if(data.last_name!==null){
                                   $('.middelLine'). html('<h6 style="color:'+data.text+';">contact to</h6><p style="color:'+data.text+'">'+data.bottom_title+'</p>');
                                }
                                
                                 if(data.address!==null){
                                   $('.contactInfo1').html('<h6 style="color:'+data.blog_color+';">Address</h6><p style="color:'+data.text+';">'+data.address+'</p>');
                                }
                                
                                 if(data.email!==null){
                                   $('.contactInfo2').html('<h6 style="color:'+data.blog_color+';">Email</h6><p style="color:'+data.text+';">'+data.email+'</p>');
                                }
                                
                                 if(data.phone!==null){
                                   $('.contactInfo3').html('<h6 style="color:'+data.blog_color+';">Contact</h6><p style="color:'+data.text+';">'+data.code+'-'+data.phone+'</p>');
                                }
                                  
                                var html='';
                                var info='';
                                  for(var i in data.social_media){
                                      if(data.social_media[i]!==null)
                                      
                                   if(data.socialMediaList[i]=='fa-whatsapp'){
                                        html+='<a href="#" style="background:'+data.contact_color+';"><i class="fa '+data.socialMediaList[i]+'" aria-hidden="true" style="color:'+data.primary+';"></i></a>';
                                   }

                                   if(data.socialMediaList[i]=='fa-info'){
                                        info+='<a href="#"><i class="fa '+data.socialMediaList[i]+'" aria-hidden="true"></i></a>';
                                   }
                                      
                                  }
                                 $('.whatsApp').html(html);
                                  $('.info').html(info);
                                
                               
                                 $(".calling").css("color",data.text);
                                 $(".profilePicBoxx").css("background",data.scondary);
                                 $(".wrapper").css("background",data.primary);
                         
                                
                              	$('#img').hide(); 
                              		setTimeout(function(){ $('#loader').hide();  }, 4000);
                              	 	dview();
                               		//$('#myModal').show();
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                   
      
         });
           
         $('button').click(function(){
             $('#myModal').hide(); 
         });   
            
            $('.finish-btn').click(function(){
                 $('form').attr('action', "{{route('admin.Cards.generate_pdf7')}}");
                 $('form').attr('target','popup');
                 var urrl="{{route('admin.Cards.generate_pdf7')}}";
                 $('form').attr(' onclick',"window.open("+urrl+",'popup','width=800,height=800');");
            });
            
            function view(){
            
                $.ajax({
                    url:"{{route('admin.Cards.delete-image')}}",
                    method:"get",
                    success: function(data) {
                        console.log(data);
                    }
                    
                });
            }
            
            
            
// global variable
 
    function dview() {
         var element = $("#html-content-holder"); // global variable
       var getCanvas; 

         html2canvas(element, {
              x: 50, y: 100,
              draggable: true,
         onrendered: function (canvas) {
                 
                $("#previewImage").html(canvas);
                getCanvas = canvas;
             
             }
         });
             
          $('#frame').show(); 
        $('#wrapper').hide(); 
        $('#img').hide(); 
       
    }
    //$('#img').hide(); 
   // $('#frame').hide(); 
  //$('#wrapper').hide();  
 
 
      //("#Secondary option[value="+ color +"]").prop('disabled', true);              
    //   $("#text option[value=" + color + "]").prop('disabled', true);
    //     $("#Primary option[value="+ color +"]").prop('disabled', true);
   
    $("#Primary").change(function(){
       var color=$("#Primary").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', true);
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', false);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
    }) ;   
        
  $("#Secondary").change(function(){
       var color=$("#Secondary").val();
       $("#heading1 option[value=" + color + "]").prop('disabled', true);
       $("#heading option[value=" + color + "]").prop('disabled', true);
       $("#Secondary option[value="+ color +"]").prop('disabled', false);
       $("#Primary option[value="+ color +"]").prop('disabled', true);
       $("#text option[value=" + color + "]").prop('disabled', true);
    }) ;   
    
    $("#text").change(function(){
       var color=$("#text").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', true);
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', false);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
         $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
    
     $("#heading").change(function(){
       var color=$("#heading").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', true);
        $("#heading option[value=" + color + "]").prop('disabled', false);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
        
        
         $("#heading1").change(function(){
       var color=$("#heading1").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', false);
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
         $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
         
        
        
        //  $('.sf-controls').css("display","none");
        
          $('input[type="color"]').change(function(){
             var select1=$('#Primary').val();
             var select2=$('#Secondary').val();
             var select3=$('#text').val();
             var select4=$('#heading').val();
             var select5=$('#button_color').val();
             if(select1!=='Choose Color' && select2!=='Choose Color' && select3!=='Choose Color' && select4!=='Choose Color' && select5!=='Choose Color'){
                 $('.sf-controls').css("display","block");
                   $('#active1').attr('class','active');
             }
            
        });
     
    //  $('.next-btn').click(function(){
    //           $('.sf-controls').css("display","none");
    //     });   
        
        
         $('.socialLinksboxx').on("focusout","input",function(){
             
           
            //  var input1=$('input[name="social_media[1]"]').val();
            //  var input2=$('input[name="social_media[2]"]').val();
             var input3=$('input[name="social_media[3]"]').val();
            //  var input4=$('input[name="social_media[4]"]').val();
            //  var input5=$('input[name="social_media[5]"]').val();
             
             if(input3!==''){
                   $('.sf-controls').css("display","block");
                     $('#active2').attr('class','active');
             }else{
                   $('.sf-controls').css("display","none");
                   $('.col').text('*Please fill all the boxes');
             }
            
        });
        
        
        
         $('.panel-group').on("focusout","input",function(){
            
             var first_name=$('input[name="first_name"]').val();
             var last_name=$('input[name="last_name"]').val();
             var phone=$('input[name="phone"]').val();
             var designation=$('input[name="designation"]').val();
             var email=$('input[name="email"]').val();
             var bottom_title=$('input[name="bottom_title"]').val();
             var address=$('#address').val();
             var service_name_1=$('input[name="service-name-1"]').val();
             var service_name_2=$('input[name="service-name-2"]').val();
             var service_name_3=$('input[name="service-name-3"]').val();
             var service_name_4=$('input[name="service-name-4"]').val();
             var service_name_5=$('input[name="service-name-5"]').val();
             var service_name_6=$('input[name="service-name-6"]').val();
             
                              if(first_name.length <31){
                                
                                  first_name=first_name;
                              }else{
                                  $('.first_name').text('First Name  max 30 Char');
                                  $('.first_name').css('color','red');
                                    // alert('*First Name max 30 Char');
                              }
                              
                            
                              
                               
                              
                              if(last_name.length <31){
                                 
                                  last_name=last_name;
                              }else{
                                   $('.last_name').text('Last Name max 30 Char');
                                    $('.last_name').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              
                               if(bottom_title.length <31){
                                 
                                  bottom_title1=bottom_title;
                              }else{
                                   $('.bottom_title').text('Designation max 30 Char');
                                   $('.bottom_title').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              
                              if(designation.length <31){
                                 
                                  designation=designation;
                              }else{
                                   $('.designation').text('Designation max 30 Char');
                                    $('.designation').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              
                               if(address.length <81){
                                 
                                  address1=address;
                              }else{
                                   $('.address').text('Address max 80 Char');
                                   $('.address').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                           
                              
                               if(service_name_1.length <19){
                                 
                                   service_name_1= service_name_1;
                              }else{
                                  
                                   $('.service-name-1').text('Service Name 1 max 18 Char');
                                   $('.service-name-1').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_2.length <19){
                                 
                                   service_name_2= service_name_2;
                              }else{
                                   service_name_2='';
                                   $('.service-name-2').text('Service Name 2 max 18 Char');
                                   $('.service-name-2').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_3.length <19){
                                 
                                   service_name_3= service_name_3;
                              }else{
                                   $('.service-name-3').text('Service Name 3 max 18 Char');
                                   $('.service-name-3').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              if(service_name_4.length <19){
                                 
                                   service_name_4= service_name_4;
                              }else{
                                   $('.service-name-4').text('Service Name 4 max 18 Char');
                                   $('.service-name-4').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_5.length <18){
                                 
                                   service_name_5= service_name_5;
                              }else{
                                   $('.service-name-5').text('Service Name 5 max 18 Char');
                                   $('.service-name-5').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_6.length <18){
                                 
                                   service_name_6= service_name_6;
                              }else{
                                   $('.service-name-6').text('Service Name 6 max 18 Char');
                                     $('.service-name-6').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              
                              
                            
                             
                              if(phone!==''){
                              if(validatePhone(phone)==true){
                                 
                                  phone=phone;
                                   $('.phone1').css('color','green');
                                   $(".phone1").text('*Phone Valid..!');
                              }else{
                                   //alert('Is not Valid..!');
                                   $('.phone1').css('color','red');
                                   $('.phone1').text('*Is not Valid Phone No... only no!');
                              }
                              }
                              
                              if(email!==''){
                              if(isEmail(email)==true){
                                 
                                  email1=email;
                                  $('.email').css('color','green');
                                  $(".email").text('* E-mail Valid..!');
                              }else{
                                   //alert('Is not Valid..!');
                                     $('.email').css('color','red');
                                   $('.email').text('* Is not Valid E-mail..!');
                              }
                              }
                             
             if(first_name!=='' && last_name!==''  && phone!=='' && designation!=='' && bottom_title!=='' && email!=='' && address1!=='' && service_name_1!=='' && service_name_2!=='' && service_name_3!=='' && service_name_4!==''){
                   $('.sf-controls').css("display","block");
             }else{
                  $('.sf-controls').css("display","none");
             }
            
        });
        
        
        function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
          if (regex.test(email)) {
                 return true;
               }
            else {
                  return false;
             }
        }
        
        
     function validatePhone(Phone) {
    
    var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (filter.test(Phone)) {
        return true;
    }
    else {
        return false;
    }
}
                    
});
    </script>
    
   
    
    
@endsection