@extends('cards.layouts.master')
@section('css')
         <link href="{{ url('web_assets/css/form-wizard.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/css/dropify.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/bootstrap.min.css')}}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/font-awesome.min.css')}}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/main.css')}}" rel="stylesheet">
         <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/demo/prism.css" />
         <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/bower_components/sweetalert/dist/sweetalert.css" />
	     <link rel="Stylesheet" type="text/css" href="{{ url('web_assets/js/style1.css?abc')}}" />
	     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	     <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
		 <script src="https://foliotek.github.io/Croppie/croppie.js"></script>
		 <script src="{{ url('web_assets/js/crop_js/script5.js')}}"></script>
       
    <style>
        .text-muted1 {
     color: red;
        }
        .progressbar li {
         
        }
        
        #loader { 
      border: 12px solid #f3f3f3; 
      border-radius: 50%; 
      border-top: 12px solid #444444; 
      width: 70px; 
      height: 70px; 
      animation: spin 1s linear infinite; 
    } 
    /*  #loader2 { */
    /*  border: 12px solid #f3f3f3; */
    /*  border-radius: 50%; */
    /*  border-top: 12px solid #444444; */
    /*  width: 70px; */
    /*  height: 70px; */
    /*  animation: spin 1s linear infinite; */
    /*} */
    
.preloader {
   position: absolute;
   top: 0;
   left: 0;
   width: 100%;
   height: 122%;
   z-index: 9999;
   background-image: url('../IMAGE/default.gif');
   background-repeat: no-repeat; 
   background-color: #fff9;
   background-position: center;
}
    
    @keyframes spin { 
      100% { 
        transform: rotate(360deg); 
      } 
    } 
    
    .center { 
      position: absolute; 
      top: 0; 
      bottom: 0; 
      left: 0; 
      right: 0; 
      margin: auto; 
    } 
    
    
    .center1 { 
      position: absolute; 
      top: 0; 
      bottom: 0; 
      left: 0; 
      right: 0; 
      margin: auto; 
    } 
    

    </style>     
    
       
@section('content')
    <!-- BACK BUTTON STARTS HERE -->

 <div class="preloader"></div>
 
    <!--<section>-->
    <!--    <div class="container">-->
    <!--        <div class="row">-->
    <!--            <div class="col-md-12">-->
    <!--                <p class="backBtn">-->
    <!--                    <a class="btnCustomStyle2 btn-solid" href="index.html">-->
    <!--                        <span>Back</span>-->
    <!--                    </a>-->
    <!--                </p>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->
    <!-- BACK BUTTON END HERE -->

    <section class="upperCircles">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="progressbar list-unstyled" style="width: 100%;">
                        <li class="active" style=" width: 33%;"></li>
                        <li id="active1"  style=" width: 33%;"></li>
                        <li id="active2"  style=" width: 33%;"></li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- FORM WIZARD STARTS HERE -->
    <section class="mainFormWizard">
        <div class="container-fluid">
            <span id="theme_name" style="display: none;">sky</span>
            <span id="trans_name" style="display: none;">slide</span>
            <span id="rtl" style="display: none;">0</span>
            <div class="row">
                <div class="col-md-9">
    <ul>
            @foreach ($errors->all() as $error)
           
              <li> <div class="alert alert-warning">
               <strong>Warning!</strong> {{ $error }}
              </div></li>
            @endforeach
            
                 
        </ul>
                    <form id="wizard_example_2"  method="post" enctype="multipart/form-data" >
                       <div id="loader1" style="position: relative;">
                          <!--<div id="loader2" class="center1" ></div> </br>-->
                          
                         <h2 style="text-align: center;font-size:14px;">Please Waite ...</h2>
                   </div>
                        @csrf
                        <fieldset>
                            <legend>Color</legend>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Primary Color</label>
                                            <input type="color" class="form-control" id="Primary" name="primary"  style="height:80px;width:200px;" value="#ffffff">
                                           
                                        </div>
                                       
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Heading Color</label>
                                               <input type="color" class="form-control" id="Secondary" name="scondary" style="height:80px;width:200px;" value="#ef544a">
                                        </div>
                                    </div>
                                </div>
                                
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Icon Background  Color</label>
                                             <input type="color" class="form-control" id="blog_color" name="blog_color" style="height:80px;width:200px;" value="#ffdcda">
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Text Color</label>
                                            <input type="color" class="form-control"  id="text" name="text"  style="height:80px;width:200px;" value="#3f455a">
                                            
                                        </div>
                                    </div>
                                </div>
                               
                            </div>
                        </fieldset>
                      
                        <fieldset>
                            <legend>Social Links</legend>
                            @include('cards.socialmedia')
                            <!-- <div class="row">
                                <div class="col-md-12">
                                    <span class="col" style="color:red;font-size:16px;"></span>
                                    <ul class="list-unstyled socialLinksboxx">
                                        @foreach($socialMedia as $social)
                                            <li class="clearfix">
                                                 <span class="titleSocial" >
                                                     <i class="fa {{$social['icon']}}" aria-hidden="true"></i>{{$social['name']}}
                                                 </span>
                                                 @if($social['icon']=='fa-whatsapp')
                                              
                                                 <select class="form-control" name="code" style="width: 78px; height: 29px;float: left;margin-top: 0px;font-size: 12px;margin-left: 63px;" id="code">
                                                    @foreach($va as $va1)
                                                   <option value="{{$va1->callingCode}}">{{$va1->callingCode}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$va1->country}} </option>
                                                    @endforeach
                                                </select>
                                                 <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="7484564564">
                                                 </span>
                                                   @elseif($social['icon']=='fa-facebook')
                                                  <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.facebook.com">
                                                 </span>
                                                 
                                                 @elseif($social['icon']=='fa-skype')
                                                  <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.skype.com">
                                                 </span>
                                                 
                                                
                                                  
                                                   @elseif($social['icon']=='fa-linkedin')
                                                    <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.linkedin.com">
                                                 </span>
                                                    @elseif($social['icon']=='fa-twitter')
                                                   <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://twitter.com">
                                                 </span>
                                                   @endif
                                                
                                            </li>
                                             
                                        @endforeach
                                    </ul>
                                </div>
                            </div> -->
                        </fieldset>
                      <fieldset>
                            <legend>Profile </legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        
                                                      <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingfour">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                                        Image Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                                <div class="panel-body businessInformation">

                                                    <div class="row">

                                                      
                                                         <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Image 1</label>
                                                                    <!--<input type="file" name="image1" id="input-file-now" class="dropify" />-->
                                                                     <div class="row">
                                                                      <div class="col-md-6">
                                                                        <!--<img src="../web_assets/design-image/design-5/{{$image[0]}}" style="width:100px;height:100px;" />-->
                                                                        <input type="file" id="upload1"  value="" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="image1" id="base64image1">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap1">
                                                                         <div id="upload-demo1"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                         <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Image 2</label>
                                                                    <!--<input type="file" name="image2" id="input-file-now1" class="dropify" />-->
                                                                    
                                                                     <div class="row">
                                                                      <div class="col-md-6">
                                                                            <!--<img src="../web_assets/design-image/design-5/{{$image[1]}}" style="width:100px;height:100px;" />-->
                                                                        <input type="file" id="upload2"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="image2" id="base64image2">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap2">
                                                                         <div id="upload-demo2"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                         <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Image 3</label>
                                                                    <!--<input type="file" name="image3" id="input-file-now2" class="dropify" />-->
                                                                    
                                                                     <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload3"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="image3" id="base64image3">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                          <!--<img src="../web_assets/design-image/design-5/{{$image[2]}}" style="width:100px;height:100px;" />-->
                                                                         <div class="upload-demo-wrap3">
                                                                         <div id="upload-demo3"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        Personal Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                
                                                <div class="panel-body personalBoxxx">
                                                  @include('cards.createfor')
                                                    <div class="row">
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" name="first_name" placeholder="First Name" value="Chetan">
                                                                <small class="text-muted1 first_name"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="Bhagat">
                                                                <small class="text-muted1 last_name"></small>
                                                            </div>
                                                        </div>
                                                     <div class="col-md-12 col-lg-6">
                                                        <div class="form-group ">
                                                            <input type="text" class="form-control" placeholder="Enter Phone" name="phone" value="9123123126">
                                                            <small class="text-muted1 phone1"></small>
                                                        </div>
                                                    </div>
                                                  <div class="col-md-12 col-lg-6 ">
                                                    <div class="form-group ">
                                                      <input type="text" id="autocomplete" class="form-control" placeholder="Search your location" value="" onFocus="geolocate()">
                                                       <small class="text-muted1 address"></small>
                                                      <input type="hidden" name="location" id="latlong" value="">
                                                    </div>
                                                  </div>
                                                  <div class="col-md-12 col-lg-12 ">
                                                    <div class="form-group ">
                                                      <input type="text" class="form-control" placeholder="Enter your Official Address" name="address" value="Disney Springs, Buena Vista Drive, Lake Buena Vista, FL, USA">
                                                      <small class="text-muted1 address"></small>
                                                    </div>
                                                  </div>
                                                         <div class="col-md-12 col-lg-6"></div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Enter Heading1" name="heading1" maxlength="8" value="Study">
                                                                <small class="text-muted1 heading1"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-12">
                                                            <div class="form-group ">
                                                                <textarea class="form-control" rows="3" placeholder="Enter Study as Example(Bachelor of Arts, Major - Literature & Language, JULY 2020 )" name="study" id="study" maxlength="90">Bachelor of Arts, Major - Literature & Language, JULY 2020</textarea>
                                                                <small class="text-muted1 study"></small>
                                                            </div>
                                                        </div>
                                                         <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Enter Heading2" name="heading2" maxlength="8" value="About">
                                                                <small class="text-muted1 heading2"></small>
                                                            </div>
                                                        </div>
                                                        
                                                       
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <textarea class="form-control" rows="3" name="about"  id="about" placeholder="About me Description" maxlength="100">Lorem Ipsum is simply dummy text of the printing and Love typesetting.</textarea>
                                                                <small class="text-muted1 about"></small>
                                                            </div>
                                                        </div>
                                                         <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Enter Heading3" name="heading3" maxlength="8" value="Hobby">
                                                                <small class="text-muted1 heading3"></small>
                                                            </div>
                                                        </div>
                                                         <div class="col-md-12 col-lg-12">
                                                            <div class="form-group ">
                                                                 <textarea class="form-control" rows="3" placeholder="Enter Hobby as Example( Reading novels, Writing or blogginge, Playing Cricket )" name="hobby" id="hobby" maxlength="90">Reading novels, Writing or blogging, Playing Cricket</textarea>
                                                                <small class="text-muted1 hobby"></small>
                                                            </div>
                                                        </div>
                                                     
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
           
                       
                          
                                        
                                        
                                  
                                        
                                        
                                        
                                        
                             
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                            <div class="col-md-12">
                                <p class="backBtn">
                                    <a class="btnCustomStyle2 btn-solid Preview1 Preview2 Preview3" href="javascript:void(0);">
                                        <span>Preview</span>
                                    </a>
                                </p>
                            </div>
                          
                       </form>
                    </div>
                <div class="col-md-3">
                    
                   <div id="frame" style="background-image: url('{{ url("web_assets")}}/img/Phone1.png');background-color: #cccccc;background-position: center;background-repeat: no-repeat;background-size: cover;position: relative;width: 259px;height: 512px;border-radius: 30px;"></br>
   <div id="loader" class="center" ></div> 
                         <div id="previewImage" ></div>
                        
                      <div id="img" style="width: 224px;border-radius: 40px;margin-left: 18px;margin-top: -10px;">
                         <img class="img-fluid mx-auto d-block "  src="{{ url('web_assets')}}/img/desine-5.png" alt="" style="border-radius: 22px;height: 487px;margin-top:1px;">
                    </div>
                  
                    </div>
                  
                   
                </div>
                
            </div>
    <style>
       
/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #3055c6; }
::selection { color: #f1f1f1;background: #3055c6; }



body  {font-family: 'Poppins', sans-serif;background: #eae7f0;}
.wrapper {width: 400px;margin: 0 auto;height: 830px;background: #fff;}


.mainBoxx {padding: 30px;}






.galleryImages {}
.galleryImages ul {}

.galleryImages ul:after {display: block;clear: both;content: "";}


.galleryImages ul li {display: inline-block;float: left;width: 45%;margin: 0 5% 0 0;}
.galleryImages ul li:last-child {margin:0;}
.galleryImages ul li .pic-1{margin: 0;border-radius: 15px;}
.galleryImages ul li .pic-1 img {border-radius: 15px;width: 100%;height: 273px;}
.galleryImages ul li .pic-2 {    margin: 0 0 12% 0;border-radius: 15px;}
.galleryImages ul li .pic-2 img {border-radius: 15px;width: 100%; height: 125px;}
.galleryImages ul li .pic-3{margin: 0;border-radius: 15px;}
.galleryImages ul li .pic-3 img {border-radius: 15px;width: 100%;height: 125px;}

.StudentName {}
.StudentName h1 {color: #3f455a; font-size: 36px;font-weight: 700;}

.information {  margin: 40px 0 0 0;}
.information ul {margin: 0}
.information ul .mainli {    margin: 0 0 20px 0;}
.information ul .mainli:after {display: block;clear: both;content: "";}
.information ul .mainli h4 { width: 36%;  float: left;color: #ef544a;font-weight: 600;font-size: 21px;}
.information ul .rightInfo {  width: 64%;  float: right;}
.information ul .rightInfo ul { }
.information ul .rightInfo ul li{font-size: 14px;color: #3f455a;    margin: 0 0 5px 0; }
.information ul .rightInfo ul li.qualification{    font-size: 18px;font-weight: 600;color: #3f455a; }
.information ul .rightInfo .abtText {font-size: 14px;color: #3f455a;margin: 0;line-height: 24px;}



.socialMediaIcons {}
.socialMediaIcons ul{margin: 0;}
.socialMediaIcons ul li.phone {display: inline-block;width: 20%;   float: left;}
.socialMediaIcons ul li.phone a {font-size: 30px;width: 60px;height: 60px;line-height: 65px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;    background: #ef544a;color: #fff;}
.socialMediaIcons ul li.phone a .fa {font-size: 32px; margin-top: 14px;}
.socialMediaIcons ul li.phone a .fa:hover {opacity: 0.7;}

.socialMediaIcons ul li.middelLine {    float: left;     margin: 7px 0 0 0;   width: 60%;}
.socialMediaIcons ul li.middelLine .innerMediaBoxx {  }
.socialMediaIcons ul li.middelLine  .innerMediaBoxx ul{    margin: 0 auto;
    display: table;
    /*background: #ffdcda;*/
    padding: 5px 10px;
    border-radius: 25px;}
.socialMediaIcons ul li.middelLine  .innerMediaBoxx ul li {display: inline-block;margin: 0 3px;}
.socialMediaIcons ul li.middelLine  .innerMediaBoxx ul li a {  font-size: 30px;width: 36px;height: 36px;line-height: 30px;text-align: center;text-decoration: none;border-radius: 18px;display: inline-block;    background: #ef544a;color: #fff;}
.socialMediaIcons ul:after {display: block;clear: both;content: "";}
.socialMediaIcons ul li.middelLine  .innerMediaBoxx ul li a .fa {font-size: 18px; margin-top: 9px;}
.socialMediaIcons ul li.middelLine  .innerMediaBoxx ul li a .fa:hover {opacity: 0.7;}

.socialMediaIcons {}
.socialMediaIcons ul{margin: 0;}
.socialMediaIcons ul li.whatsApp {display: inline-block;width: 20%;      text-align: right; float: right;}
.socialMediaIcons ul li.whatsApp a {  font-size: 30px;width: 60px;height: 60px;line-height: 60px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;    background: #ef544a;color: #fff;}
.socialMediaIcons ul li.whatsApp a .fa {font-size: 32px; margin-top: 14px;}
.socialMediaIcons ul li.whatsApp a .fa:hover {opacity: 0.7;}




/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


}
    </style>
     
    <div style="opacity: 0;">    
<div id="html-content-holder" style="width: 400px;">
     <section class="wrapper" id="wrapper">

            <div class="mainBoxx">
                <div class="galleryImages">
                    <ul class="list-unstyled">

                        <li>
                            <p class="pic-1">
                               
                            </p>
                        </li>

                        <li>
                            <p class="pic-2">
                                
                            </p>
                            <p class="pic-3">
                               
                            </p>
                        </li>

                    </ul>
                </div>
                <div class="StudentName">
                   
                </div>

                <div class="information">
                    <ul class="list-unstyled">
                        <li class="mainli">
                            <h4 id="study1"></h4>
                            <div class="rightInfo">
                                <ul class="list-unstyled list-unstyled1">
                                  
                                </ul>
                            </div>
                        </li>
                        <li class="mainli">
                            <h4 id="about1"></h4>
                            <div class="rightInfo">
                               <p class="abtText"></p>
                            </div>
                        </li>
                        <li class="mainli">
                            <h4 id="hobby1"></h4>
                            <div class="rightInfo">
                                <ul class="list-unstyled list-unstyled2">
                                   
                                    
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="socialMediaIcons">
                        <ul class="list-unstyled">
                            <li class="phone">
                                
                            </li>
                            <li class="middelLine">
                                <div class="innerMediaBoxx">
                                    <ul class="list-unstyled list-unstyled3">
                                        
                                    </ul>
                                </div>
                            </li>
                            <li class="whatsApp">
                                
                            </li>
                        </ul>
                    </div>


            </div>
        
        </section>
      </div> 
  </div>              
            
            
            
        </div>
        

        
<script>
demoUpload1();
demoUpload2();
demoUpload3();

</script>
        
        
    </section>
    <!-- FORM WIZARD END HERE -->
@endsection

@section('scripts')
    <script src="{{ url('web_assets/js/html2canvas.js') }}"></script>
    <script src="{{ url('web_assets/js/form-wizard.js') }}"></script>
    <script src="{{ url('web_assets/js/jscolor.js') }}"></script>
    <script src="{{ url('web_assets/js/dropify.js') }}"></script>
     
<style>
     canvas {
    height: 492px;
    width: 225px;
    margin-top: -13px;
    border-radius: 21px;
    margin-left: 17px;
    }
</style>
    <script>
        jscolor.presets.default = {
            borderColor:'rgba(236,239,245,0.6)', width:200, height:100,
            buttonColor:'rgba(70,51,255,1)', shadowColor:'rgba(0,0,0,0.05)'
        };
    </script>

    <script>
    
        $(document).ready(function(){
           
            	$('#myModal').hide();
            	 $('#h1').hide();
            	 $('#h2').hide();
            	 $('.searchMe').hide();
            // 	 $('.btnCustomStyle2').hide();
            	 $('#code').val('+91');
            	 $('#loader').hide(); 
                 $('#loader1').hide(); 
            	 $('#wrapper').hide();
            	 $('#study1').hide();
            	 $('#about1').hide();
            	 $('#hobby1').hide();
            	 $('.list-unstyled3').hide();
            	 $('.preloader').fadeOut('slow');
            view();
            $('.sf-btn-finish').click(function(){
                    // $('.preloader').fadeIn('slow');
                    // 	setTimeout(function(){ $('.preloader').fadeOut('slow');  }, 4000);
                   
            });
            $('input[type="color"]').change(function(){
              $('.btnCustomStyle2').show();
            });
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });
           
            // $("[type='file']").focusout(function(){
            
            //   var files =$('#input-file-now').prop('files')[0];
            //   if(files[0]!==''){
            //       if(files[0].size > (1024*1024)){
            //       alert('Please Insert Image Max 2MB.!');
            //       $("#input-file-now").val(null);
            //       }
            //   }
            
             
            //   });
               
             $('.btnCustomStyle2').click(function(){
                 	 $('#wrapper').show();
                //   $('.preloader').fadeIn('slow');
                  	$('#loader').show();
                  	
            //   var  files =$('#input-file-now').prop('files')[0];
            //   var  files1 =$('#input-file-now').prop('files')[0];
            //   var  files2 =$('#input-file-now').prop('files')[0];
            
              
                var form_data = new FormData($('#wizard_example_2')[0]);
                //   form_data.append("file", files);
                //   form_data.append("file", files1);
                //   form_data.append("file", files2);
                    
   
                       $.ajax({
                            type: 'post',
                            url:  "{{route('admin.Cards.cards5')}}",
                            
                            data: form_data,
                            async: false,
                            dataType:'json',
                            success: function (data1) {
                            var    data=data1.va;
                                console.log(data);
                                $('.wrapper').css('background',data.primary);
                                 $('.wrapper').css('color',data.primary);
                                 $('.list-unstyled3').css('background',data.blog_color);
                                if(data.image1!==null){
                                    $('.pic-1').html('<img src="'+data.image1+'" alt="pic-1" class="img-fluid d-block">');
                                }else{
                                  var img1 ="{{url('/web_assets/design-image/design-5/pic-1.jpg')}}";
                                    $('.pic-1').html('<img src="'+img1+'" alt="pic-1" class="img-fluid d-block">');
                                }
                                
                                if(data.image2!==null){
                                    $('.pic-2').html('<img src="'+data.image2+'" alt="pic-2" class="img-fluid d-block">');
                                }else{
                                  var img2 ="{{url('/web_assets/design-image/design-5/pic-2.jpg')}}";
                                     $('.pic-2').html('<img src="'+img2+'" alt="pic-2" class="img-fluid d-block">');
                                }
                                
                                 if(data.image3!==null){
                                    $('.pic-3').html('<img src="'+data.image3+'" alt="pic-3" class="img-fluid d-block">');
                                }else{
                                  var img3 ="{{url('/web_assets/design-image/design-5/pic-3.jpg')}}";
                                     $('.pic-3').html('<img src="'+img3+'" alt="pic-3" class="img-fluid d-block">');
                                }
                                
                                if(data.hobby!==null){
                                     $('#hobby1').show();
                                     $('#hobby1').text(data.heading3);
                                     $('#hobby1').css('color',data.scondary);
                                     var hobby=data.hobby.split(",");
                                     var hobby1='';
                                     var i;
                                         for(i=0; i< hobby.length; i++){
                                            hobby1+='<li style="color:'+data.text+'">'+hobby[i]+'</li>'; 
                                         }
                                    $('.list-unstyled2').html(hobby1);
                                }
                                
                                 if(data.about!==null){
                                     $('#about1').show();
                                     $('#about1').text(data.heading2);
                                     $('#about1').css('color',data.scondary);
                                     $('.abtText').text(data.about);
                                     $('.abtText').css('color',data.text);
                                }
                                 if(data.study!==null){
                                     $('#study1').show();
                                     $('#study1').text(data.heading1);
                                     $('#study1').css('color',data.scondary);
                                     var study=data.study.split(",");
                                     var study1='';
                                     var i;
                                         for(i=0; i< study.length; i++){
                                             if(i==0){
                                                  study1+='<li class="qualification" style="color:'+data.text+'">'+study[i]+'</li>'; 
                                             }else if(i==1){
                                                  study1+='<li style="color:'+data.text+'">'+study[i]+'</li>'; 
                                             }else if(i==2){
                                                  study1+='<li style="color:'+data.text+'"><small>'+study[i]+'</small></li>'; 
                                             }else{
                                                  study1+='<li style="color:'+data.text+'"><small>'+study[i]+'</small></li>'; 
                                             }
                                               
                                           
                                         }
                                    
                                    $('.list-unstyled1').html(study1);
                                }
                                
                                if(data.phone!==null){
                                   
                                    $('.phone').html('<a href="'+data.phone+'" style="background:'+data.scondary+';color:'+data.primary+';"><i class="fa fa-phone" aria-hidden="true"></i></a>');
                                }
                                
                                if(data.first_name!==null && data.last_name!==null){
                                    $('.StudentName').html('<h1>'+data.first_name+' '+data.last_name+'</h1>');
                                    $('h1').css('color',data.text);
                                }
                                
                               var i;
                                var html='';
                                 var html1='';
                          
                                   for(var i in data.social_media){
                                       if(data.social_media[i]!==null){
                                        $('.list-unstyled3').show();
                                      
                                       //html +='<li style=" background:'+data.scondary+'"><a href="'+data.social_media[i]+'" class="fa '+data.socialMediaList[i]+'"></a></li>';
                                       if(data.socialMediaList[i]=='fa-whatsapp'){
                                           
                                          html1+='<a href="'+data.social_media[i]+'" style="background:'+data.scondary+';color:'+data.primary+';"><i class="fa '+data.socialMediaList[i]+'" aria-hidden="true"></i></a>';
                                            $('.whatsApp').html(html1);
                                       }else{
                                           html+='<li><a href="'+data.social_media[i]+'" style="background:'+data.scondary+';color:'+data.primary+';"><i class="fa '+data.socialMediaList[i]+'" aria-hidden="true"></i></a></li>';
                                       }
                                       }
                                   }
                                 
                                     
                                    $('.list-unstyled3').html(html);
                                 
                                 $('.preloader').fadeOut('slow');
                               	$('#img').hide(); 
                               		setTimeout(function(){ $('#loader').hide();  }, 4000);
                               	 	dview();
                               		//$('#myModal').show();
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                   
      
         });
           
         $('button').click(function(){
             $('#myModal').hide(); 
         });   
            
            $('.finish-btn').click(function(){
                 $('form').attr('action', "{{route('admin.Cards.generate_pdf5')}}");
                 $('form').attr('target','popup');
                 var urrl ="{{route('admin.Cards.generate_pdf5')}}";
                 $('form').attr(' onclick',"window.open("+urrl+",'popup','width=800,height=800');");
            });
            
            function view(){
            
                $.ajax({
                    url:"{{route('admin.Cards.delete-image')}}",
                    method:"get",
                    success: function(data) {
                        console.log(data);
                    }
                    
                });
            }
            
            
            
// global variable
 
    function dview() {
         var element = $("#html-content-holder"); // global variable
       var getCanvas; 

         html2canvas(element, {
              x: 50, y: 100,
              draggable: true,
         onrendered: function (canvas) {
                 
                $("#previewImage").html(canvas);
                getCanvas = canvas;
                console.log(canvas);
             }
         });
             
          //$('#frame').show(); 
         $('#wrapper').hide(); 
        $('#img').hide(); 
       
    }
    //$('#frame').hide(); 
 // $('#wrapper').hide();  
 
 
      //("#Secondary option[value="+ color +"]").prop('disabled', true);              
    //   $("#text option[value=" + color + "]").prop('disabled', true);
    //     $("#Primary option[value="+ color +"]").prop('disabled', true);
   

    $("#Primary").change(function(){blog_color
       var color=$("#Primary").val();
        $("#Primary option[value="+ color +"]").prop('disabled', false);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#blog_color option[value=" + color + "]").prop('disabled', true);
    }) ;   
        
  $("#Secondary").change(function(){
       var color=$("#Secondary").val();
       $("#Secondary option[value="+ color +"]").prop('disabled', false);
       $("#Primary option[value="+ color +"]").prop('disabled', true);
       $("#text option[value=" + color + "]").prop('disabled', true);
        $("#blog_color option[value=" + color + "]").prop('disabled', true);
    }) ;   
    
    $("#text").change(function(){
       var color=$("#text").val();
        $("#text option[value=" + color + "]").prop('disabled', false);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
         $("#Secondary option[value="+ color +"]").prop('disabled', true);
          $("#blog_color option[value=" + color + "]").prop('disabled', true);
    }) ;   
    
     $("#blog_color").change(function(){
       var color=$("#blog_color").val();
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
        $("#blog_color option[value=" + color + "]").prop('disabled', false);
    }) ;   
        
        //  $('.sf-controls').css("display","none");
        
        $('input[type="color"]').change(function(){
             var select1=$('#Primary').val();
             var select2=$('#Secondary').val();
             var select3=$('#text').val();
             if(select1!=='Choose Color' && select2!=='Choose Color' && select3!=='Choose Color'){
                 $('.sf-controls').css("display","block");
                   $('#active1').attr('class','active');
             }
            
        });
     
    //  $('.next-btn').click(function(){
    //           $('.sf-controls').css("display","none");
    //     });   
        
        
         $('.socialLinksboxx').on("focusout","input",function(){
             
           
             var input1=$('input[name="social_media[1]"]').val();
             var input2=$('input[name="social_media[2]"]').val();
             var input3=$('input[name="social_media[3]"]').val();
             var input4=$('input[name="social_media[4]"]').val();
             var input5=$('input[name="social_media[5]"]').val();
             
             if(input1!=='' && input2!=='' && input3!=='' && input4!=='' && input5!==''){
                   $('.sf-controls').css("display","block");
                     $('#active2').attr('class','active');
             }else{
                   $('.sf-controls').css("display","none");
                   $('.col').text('*Please fill all the boxes');
             }
            
        });
        
        
        
         $('.panel-group').on("focusout","textarea",function(){
            
             var first_name=$('input[name="first_name"]').val();
             var last_name=$('input[name="last_name"]').val();
             var phone=$('input[name="phone"]').val();
             var heading1=$('input[name="heading1"]').val();
             var heading2=$('input[name="heading2"]').val();
             var heading3=$('input[name="heading3"]').val();
             var about=$('#about').val();
             var study=$('#study').val();
             var hobby=$('#hobby').val();
           
             
             
                              if(first_name.length <31){
                                
                                  first_name1=first_name;
                              }else{
                                  $('.first_name').text('First Name max 30 Char');
                                    // alert('*First Name max 30 Char');
                              }
                              
                               
                              
                              if(last_name.length <31){
                                 
                                  last_name1=last_name;
                              }else{
                                   $('.last_name').text('Last Name max 30 Char');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              if(about.length <91){
                                 
                                  about1=about;
                              }else{
                                   // alert('About max 300 Char');
                                     $('.about').text('*About max 90 Char');
                              }
                              
                            if(hobby.length <91){
                                  
                                  hobby1=hobby;
                              }else{
                                 // alert('About max 255 Char');
                                  $('.hobby').text('*Hobby max 90 Char');
                              }
                              
                              if(study.length <91){
                                  study1=study;
                              }else{
                                  //alert('Designation max 255 Char');
                                  $('.study').text('* Study max 90 Char');
                              }
                              
                              if(heading1.length <9){
                                  heading11=heading1;
                              }else{
                                  //alert('Designation max 255 Char');
                                  $('.heading1').text('* Heading1 max 8 Char');
                              }
                              
                               if(heading2.length <9){
                                  heading12=heading2;
                              }else{
                                  //alert('Designation max 255 Char');
                                  $('.heading2').text('* Heading2 max 8 Char');
                              }
                              
                               if(heading3.length <9){
                                  heading13=heading3;
                              }else{
                                  //alert('Designation max 255 Char');
                                  $('.heading3').text('* Heading3 max 8 Char');
                              }
                                
                              if(phone!==''){
                              if(validatePhone(phone)==true){
                                 
                                  phone1=phone;
                                  $('.phone1').css('color','green');
                                  $(".phone1").text('*Phone Valid..!');
                              }else{
                                   //alert('Is not Valid..!');
                                     $('.phone1').css('color','red');
                                   $('.phone1').text('*Is not Valid Phone No... only no!');
                              }
                              }
                             
             if(first_name1!=='' && last_name1!=='' && about1!=='' && hobby1!=='' && study1!=='' && heading11!=='' && heading12!=='' && heading13!==''){
                
                 $('.sf-controls').css("display","block");
             }else{
                  $('.sf-controls').css("display","none");
             }
            
        });
        
        
        function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
          if (regex.test(email)) {
                 return true;
               }
            else {
                  return false;
             }
        }
        
        
     function validatePhone(Phone) {
    
    var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (filter.test(Phone)) {
        return true;
    }
    else {
        return false;
    }
}
                    
});
    </script>
    
   
    
    
@endsection