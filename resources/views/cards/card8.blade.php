<html>
        <!--Core CSS -->
      <link href="{{ url('bootstrap-4/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{ url('bootstrap-4/css/font-awesome.min.css')}}" rel="stylesheet">
    
     
     
     
        <style>
       
/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #3055c6; }
::selection { color: #f1f1f1;background: #3055c6; }



body  {font-family: 'Poppins', sans-serif;background: #eae7f0;padding-bottom:-150px;}
.wrapper {width: 400px;margin: 0 auto;height: 850px;background: #fff;}

.mainboxx {background: #317ef8;height: 850px;overflow: hidden;background-image: url(../img/bg-white.png);color: #fff;}



.logo {margin: 15px 0 0 0px;}


.profileboxx {padding: 0 40px;margin: 30px 0 0 0;}
.galleryImages ul:after {display: block;clear: both;content: "";}
.galleryImages > ul > li {display: inline-block;float: left;width: 40%;margin: 0 5% 0 0;}
.galleryImages > ul > li:last-child {margin:0;width: 55%;}
.galleryImages > ul > li .pic-1{margin: 0;border-radius: 15px;}
.galleryImages > ul > li .pic-1 img {border-radius: 15px;width: 100%;height: 180px;}
.galleryImages > ul > li .pic-2 {text-align: left;margin: 40px -90px 45px 4px; }
.galleryImages > ul > li .pic-2 p { margin: 0;font-size: 20px; font-style:bold;}
.galleryImages > ul > li .pic-2 small {font-size: 12px;color: #bed6fd;margin-top:-10px;}
.galleryImages > ul > li .pic-3{margin-top:-10px;}

.socialMediaIcons { }
.socialMediaIcons ul{}
.socialMediaIcons ul li {display: inline-block;margin: 0 2px; width: 50px;height: 50px;}
.socialMediaIcons ul li a {text-align: center;font-size: 30px;width: 50px;height: 50px;line-height: 50px;text-decoration: none;border-radius: 15px;display: block;}
.socialMediaIcons ul li a.yellowColor {background: #fff4e2;color: #ffbc4e;}
.socialMediaIcons ul li a.redColor {background: #ffebef;color: #ff3b62;}
.socialMediaIcons ul li a.greenColor {background: #e6fff4;color: #39de96;}
.socialMediaIcons ul li a .fa {font-size: 26px; margin-left:15px; }
.socialMediaIcons ul li a .fa:hover {opacity: 0.7;}



.abtEmailboxx {
    color: #3f455a;
    padding: 0 40px;
    margin: 25px 0;
}
.abtEmailboxx .aboutsec {}
.abtEmailboxx .aboutsec h4 {font-size: 12px;
    color: #abb1c7; font-style:bold;}
.abtEmailboxx .aboutsec p {font-size: 14px;
    color: #3f455a;    font-style:bold;}

.abtEmailboxx .emailsec {}
.abtEmailboxx .emailsec h4 {font-size: 12px;
    color: #abb1c7; font-style:bold;}
.abtEmailboxx .emailsec p {font-size: 14px;
    color: #3f455a;   }


.servicesBoxx {    margin: 15px 0;
    padding: 0 40px;}
.servicesBoxx table {}
.servicesBoxx table tr th {margin: 0 0 10px 0;}
.servicesBoxx table tr th:last-child {margin: 0;}
.servicesBoxx table tr th:after {display: block;clear: both;content: "";}
.servicesBoxx table tr th .ledtname{
    float: left;
    width: 130px;
    margin: 0;
    font-size: 14px;
    color: #fff;
   
    background: #317ef8;
    border-radius: 15px;
    padding: 10px 10px;}
.servicesBoxx table tr th .ledtname h4 {font-size: 12px;    margin: 0; font-style:bold; }
.servicesBoxx table tr th .ledtname p {font-size: 18px;   margin: 0; font-style:bold; }

.servicesBoxx table tr th .rightname{    
    float: right;
    width: 130px;
   
    margin: 0;
    font-size: 14px;
    color: #fff;
    background: #317ef8;
    padding: 10px 10px;
    border-radius: 15px;}
.servicesBoxx table tr th .rightname h4 {font-size: 12px; margin: 0; font-style:bold;}
.servicesBoxx table tr th .rightname p {font-size: 18px;    margin: 0; font-style:bold;}




.bottomIcons {padding: 0 40px;
    margin: 60px 0 0 0;}
.bottomIcons ul{margin: 0;}
.bottomIcons ul li.phone {display: inline-block;width: 20%;   float: left;}
.bottomIcons ul li.phone a {font-size: 30px;width: 60px;height: 60px;line-height: 65px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;    background: #003c9c;color: #fff;}
.bottomIcons ul li.phone a .fa {font-size: 32px; margin-left:16px; margin-top:-2px;}
.bottomIcons ul li.phone a .fa:hover {opacity: 0.7;}

.bottomIcons ul li.middelLine {    float: left;     margin: 7px 0 0 0;   width: 60%;}
.bottomIcons ul li.middelLine p {     font-size: 14px; text-align: center;}

.bottomIcons ul{margin: 0;}
.bottomIcons ul:after {display: block;clear: both;content: "";}
.bottomIcons ul li.whatsApp {display: inline-block;width: 20%;      text-align: right; float: right;}
.bottomIcons ul li.whatsApp a {  font-size: 30px;width: 60px;height: 60px;line-height: 60px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;    background: #003c9c;color: #fff;}
.bottomIcons ul li.whatsApp a .fa {font-size: 32px; margin-left:15px;}
.bottomIcons ul li.whatsApp a .fa:hover {opacity: 0.7;}














/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


} 

        </style>
        <body>
           {{--dd($val)--}}
               <section class="wrapper">

            <div class="mainboxx" style="background:{{url($val['primary'])}}">
                <img src="{{url('web_assets/img/bg-white.png')}}" style="height:800px;float:bottom;"/>
                <div class="logo" align="center">
                    @if($val['logo']!==null)
                     <img  src="{{$val['logo']}}" alt="logo" style="height:60px">
                    @else
                     <img  src="{{url('web_assets/design-image/design-8/logo.png')}}" alt="logo" style="height:60px">
                    @endif
                   
                </div>

                <div class="profileboxx" style="position: relative;">
                    <div class="galleryImages" >
                        <ul class="list-unstyled">
                            <li>
                                <p class="pic-1">
                                       @if($val['profile_image']!==null)
                                        <img src="{{$val['profile_image']}}" alt="profilepic" class="img-fluid d-block">
                                     @else
                                        <img src="{{url('web_assets/design-image/design-8/profilepic.jpg')}}" alt="profilepic" class="img-fluid d-block">
                                      @endif
                                </p>
                            </li>
                            <li>
                                <div class="pic-2">
                                    <p style="color:{{$val['blogtext_color']}};">{{$val['first_name']}} {{$val['last_name']}}</p>
                                    <small style="color:{{$val['blogtext_color']}};">{{$val['designation']}}</small>
                                </div>
                                <div class="pic-3">
                                    <div class="socialMediaIcons">
                                        <ul class="list-unstyled">
                                           
                                        @if(isset($val['social_media'])) 
                                         @php $i=0; @endphp
                                             @foreach($val['social_media1'] as $key=>$media)
                                             @if($val['socialMediaList'][$key]!='fa-whatsapp')
                                             <li>
                                                <a href="{{$media}}" class="yellowColor" style="background-color:rgba({{$val['mediacolor1'][$i]}},0.1);color:{{$val['mediacolor'][$i]}};">
                                                    <i class="fa {{$val['socialMediaList'][$key]}}" aria-hidden="true"></i>
                                                </a>
                                            </li>
                                             @php $i++; @endphp
                                            @endif
                                            @endforeach
                                        @endif
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="abtEmailboxx" style="margin-top:-520px;position: relative;">
                    <div class="aboutsec">
                        @if($val['about']!=='')
                        <h4 style="color:{{$val['heading_color']}}">About</h4>
                        <p style="text-align:justify;color:{{$val['text']}};">{{$val['about']}}</p>
                        @endif
                    </div>
                    <div class="emailsec">
                         @if($val['email']!=='')
                        <h4 style="color:{{$val['heading_color']}}">Email</h4>
                        <p ><a href="mailto:{{$val['email']}}" style="color:{{$val['text']}};">{{$val['email']}}</a></p>
                         @endif
                    </div>
                </div>

                <div class="servicesBoxx" style="margin-top:-20px;position: relative;">
                    <table>
                        <tr>
                        <th style="width:120px;">
                            @if($val['service-name-1']!==null)
                            <div class="ledtname" style="background:{{$val['primary']}};color:{{$val['blogtext_color']}};margin-top:5px;">
                                <h4>{{$val['service-name-1']}}</h4>
                                <p>{{$val['name1']}}</p>
                            </div>
                            @endif
                            </th>
                            <th style="width:120px;">
                          @if($val['service-name-2']!==null)
                            <div class="rightname" style="background:{{$val['primary']}};color:{{$val['blogtext_color']}};margin-left:10px;margin-top:5px;">
                                <h4>{{$val['service-name-2']}}</h4>
                                <p>{{$val['name2']}}</p>
                            </div>
                            @endif
                        </th>
                        </tr>
                        <tr>
                        <th style="width:120px;">
                             @if($val['service-name-3']!==null)
                            <div class="ledtname" style="background:{{$val['primary']}};color:{{$val['blogtext_color']}};margin-top:5px;">
                                <h4>{{$val['service-name-3']}}</h4>
                                <p>{{$val['name3']}}</p>
                            </div>
                             @endif
                            </th>
                            <th style="width:120px;" >
                            @if($val['service-name-4']!==null)
                            <div class="rightname" style="background:{{$val['primary']}};color:{{$val['blogtext_color']}};margin-top:5px;"> 
                                <h4>{{$val['service-name-4']}}</h4>
                                <p>{{$val['name4']}}</p>
                            </div>
                            @endif
                        </th>
                        </tr>
                        <tr>
                        <th style="width:120px;">
                             @if($val['service-name-5']!==null)
                            <div class="ledtname" style="background:{{$val['primary']}};color:{{$val['blogtext_color']}};margin-top:5px;">
                               <h4>{{$val['service-name-5']}}</h4>
                                <p>{{$val['name5']}}</p>
                            </div>
                             @endif
                            </th>
                            <th style="width:120px;">
                                @if($val['service-name-6']!==null)
                            <div class="rightname" style="background:{{$val['primary']}};color:{{$val['blogtext_color']}};margin-top:5px;">
                                 <h4>{{$val['service-name-6']}}</h4>
                                <p>{{$val['name6']}}</p>
                            </div>
                                @endif
                        </th>
                        </tr>
                    </table>
                </div>

                <div class="bottomIcons">
                    <ul class="list-unstyled">
                        <li class="phone">
                            <a href="tel:{{$val['phone']}}" style="background:{{$val['scondary']}};color:{{$val['blogtext_color']}};">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="middelLine">
                           <a href="{{$val['location']}}">
                            <p style="color:{{$val['blogtext_color']}}">{{$val['address']}}</p>
                        </a>
                        </li>
                        @if(isset($val['social_media']))   
                             @foreach($val['social_media1'] as $key=>$media)
                             @if($val['socialMediaList'][$key]=='fa-whatsapp')
                             @if($media!=='')
                        <li class="whatsApp">
                            <a href="{{$media}}" style="background:{{$val['scondary']}};color:{{$val['blogtext_color']}};">
                                <i class="fa {{$val['socialMediaList'][$key]}}" aria-hidden="true"></i>
                            </a>
                        </li>
                          @endif
                          @endif
                          @endforeach
                           @endif
                    </ul>
                </div>



            </div>
        
        </section>

            
    </body>
    <script>
        function ColorLuminance(hex, lum) {

	// validate hex string
	hex = String(hex).replace(/[^0-9a-f]/gi, '');
	if (hex.length < 6) {
		hex = hex[0]+hex[0]+hex[1]+hex[1]+hex[2]+hex[2];
	}
	lum = lum || 0;

	// convert to decimal and change luminosity
	var rgb = "#", c, i;
	for (i = 0; i < 3; i++) {
		c = parseInt(hex.substr(i*2,2), 16);
		c = Math.round(Math.min(Math.max(0, c + (c * lum)), 255)).toString(16);
		rgb += ("00"+c).substr(c.length);
	}

	return rgb;
}
    </script>
    
    </html>