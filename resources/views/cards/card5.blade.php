<html>
        <!--Core CSS -->
      <link href="{{ url('bootstrap-4/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{ url('bootstrap-4/css/font-awesome.min.css')}}" rel="stylesheet">
    
     
     
     
        <style>

/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #3055c6; }
::selection { color: #f1f1f1;background: #3055c6; }



body  {font-family: 'Poppins', sans-serif;background: #eae7f0;  padding-bottom:-150px;}
.wrapper {width: 400px;margin: 0 auto;height: 900px;background: #fff;}


.mainBoxx {padding: 30px; height: 800px;}






.galleryImages {}
.galleryImages ul {}

.galleryImages ul:after {display: block;clear: both;content: "";}


.galleryImages ul li {display: inline-block;float: left;width: 45%;margin: 0 5% 0 0;}
.galleryImages ul li:last-child {margin:0;}
.galleryImages ul li .pic-1{margin: 0;border-radius: 15px;}
.galleryImages ul li .pic-1 img {border-radius: 15px;width: 100%}
.galleryImages ul li .pic-2 {    margin: 0 0 12% 0;border-radius: 15px;}
.galleryImages ul li .pic-2 img {border-radius: 15px;width: 100%}
.galleryImages ul li .pic-3{margin: -90px 0px 0px 0px;border-radius: 15px;}
.galleryImages ul li .pic-3 img {border-radius: 15px;width: 100%}

.StudentName {}
.StudentName h1 {color: #3f455a; font-size: 36px;}

.information {  margin: 40px 0 0 0;}
.information ul {margin: 0}
.information ul .mainli {    margin: 0 0 20px 0;}
.information ul .mainli:after {display: block;clear: both;content: "";}
.information ul .mainli h4 { width: 36%;  float: left;color: #ef544a;font-size: 21px;}
.information ul .rightInfo {  width: 64%;  float: right;}
.information ul .rightInfo ul { }
.information ul .rightInfo ul li{font-size: 14px;color: #3f455a;    margin: 0 0 5px 0; }
.information ul .rightInfo ul li.qualification{    font-size: 18px;color: #3f455a; }
.information ul .rightInfo .abtText {font-size: 14px;color: #3f455a;margin: 0;line-height: 24px;}



.socialMediaIcons {}
.socialMediaIcons ul{margin: 0;}
.socialMediaIcons ul li.phone {display: inline-block;width: 20%;   float: left;}
.socialMediaIcons ul li.phone a {font-size: 30px;width: 60px;height: 60px;line-height: 65px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;background: #ef544a;color: #fff;margin-top:5px;}
.socialMediaIcons ul li.phone a .fa {font-size: 32px;margin-left:16px;margin-top:-2px;}
.socialMediaIcons ul li.phone a .fa:hover {opacity: 0.7;}

.socialMediaIcons ul li.middelLine {    float: left;     margin: 7px 0 0 0;   width: 200px;}
.socialMediaIcons ul li.middelLine .innerMediaBoxx {  }
.socialMediaIcons ul li.middelLine  .innerMediaBoxx ul{    
    margin: 0 auto;
    /*display: table;*/
    width: 132px;
    height:30px;
    background: #ffdcda;
    padding: 10px 10px;
    border-radius: 22px;
   
    
    
}
.socialMediaIcons ul li.middelLine  .innerMediaBoxx ul li {display: inline-block;margin: 0 0px; width:30px; height:30px;  }
.socialMediaIcons ul li.middelLine  .innerMediaBoxx ul li a {  font-size: 30px;width: 30px;height: 30px;line-height: 28px;text-align: center;text-decoration: none;border-radius: 15px;display: inline-block; margin-top:2px;background: #ef544a;color: #fff;}
.socialMediaIcons ul:after {display: block;clear: both;content: "";}
.socialMediaIcons ul li.middelLine  .innerMediaBoxx ul li a .fa {font-size: 18px; margin-left:7px;margin-top:6px;}
.socialMediaIcons ul li.middelLine  .innerMediaBoxx ul li a .fa:hover {opacity: 0.7;}

.socialMediaIcons {}
.socialMediaIcons ul{margin: 0;}
.socialMediaIcons ul li.whatsApp {display: inline-block;width: 20%;      text-align: right; float: right;}
.socialMediaIcons ul li.whatsApp a {  font-size: 30px;width: 60px;height: 60px;line-height: 60px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;    background: #ef544a;color: #fff;margin-top:5px;}
.socialMediaIcons ul li.whatsApp a .fa {font-size: 32px; margin-left:15px;}
.socialMediaIcons ul li.whatsApp a .fa:hover {opacity: 0.7;}




/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


}      </style>
        <body style="padding-bottom:-150px;">
   <section class="wrapper">

            <div class="mainBoxx" style="background:{{$val['primary']}}">
                <div class="galleryImages">
                    <ul class="list-unstyled">

                        <li>
                            <p class="pic-1">
                                @if($val['image1']!==null)
                                <img src="{{$val['image1']}}" alt="pic-1" class="img-fluid d-block" style="height:280px;width:170px;">
                                @else
                                   <img src="{{url('web_assets/design-image/design-5/pic-1.jpg')}}" alt="pic-1" class="img-fluid d-block" style="height:280px;width:170px;">
                                @endif
                            </p>
                        </li>

                        <li>
                            <p class="pic-2">
                                
                              @if($val['image2']!==null)
                                 <img src="{{$val['image2']}}" alt="pic-2" class="img-fluid d-block" style="height:134px;width:150px;">
                               @else
                                <img src="{{url('web_assets/design-image/design-5/pic-2.jpg')}}" alt="pic-2" class="img-fluid d-block" style="height:134px;width:150px;">
                              @endif
                                
                            </p>
                            <p class="pic-3">
                                
                                 @if($val['image3']!==null)
                                 <img src="{{$val['image3']}}" alt="pic-2" class="img-fluid d-block" style="height:134px;width:150px;">
                                   @else
                            
                                <img src="{{url('web_assets/design-image/design-5/pic-3.jpg')}}" alt="pic-2" class="img-fluid d-block" style="height:134px;width:150px;">
                                  @endif
                              </p>  
                        </li>

                    </ul>
                </div>
                <div class="StudentName">
                    <h1 style="color:{{$val['text']}};font-style:bold;">{{$val['first_name']}} {{$val['last_name']}}</h1>
                </div>

                <div class="information">
                    <ul class="list-unstyled">
                        <li class="mainli">
                            <h4 style="color:{{$val['scondary']}};font-style:bold;">{{$val['heading1']}}</h4>
                            <div class="rightInfo">
                                <ul class="list-unstyled" >
                                    @foreach($val['study1'] as $key=>$value)
                                    
                                    @if($key==0)
                                    <li class="qualification" style="color:{{$val['text']}};font-style:bold;">{{$value}}</li>
                                    @elseif($key==1)
                                    <li style="color:{{$val['text']}}">{{$value}}</li>
                                    @elseif($key==2)
                                    <li style="color:{{$val['text']}}"><small>{{$value}}</small></li>
                                    @else
                                     <li style="color:{{$val['text']}}">{{$value}}</li>
                                    @endif
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                        <li class="mainli">
                            <h4 style="color:{{$val['scondary']}};font-style:bold;">{{$val['heading2']}}</h4>
                            <div class="rightInfo">
                               <p class="abtText" style="color:{{$val['text']}};">{{$val['about']}}</p>
                            </div>
                        </li>
                        <li class="mainli">
                            <h4 style="color:{{$val['scondary']}};font-style:bold;">{{$val['heading3']}}</h4>
                            <div class="rightInfo">
                                <p class="abtText" style="color:{{$val['text']}};">{{$val['hobby']}}</p>
                                {{--
                                    <ul class="list-unstyled" style="color:{{$val['text']}}">
                                    @foreach($val['hobby1'] as $key=>$value)
                                    <li style="color:{{$val['text']}}">{{$value}}</li>
                                    
                                    @endforeach
                                </ul>
                                    --}}
                                
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="socialMediaIcons">
                        <ul class="list-unstyled">
                            <li class="phone">
                                <a href="tel:{{$val['phone']}}" style="background:{{$val['scondary']}};">
                                    <i class="fa fa-phone" aria-hidden="true" style="color:{{$val['primary']}}"></i>
                                </a>
                            </li>
                            <li class="middelLine">
                                <div class="innerMediaBoxx" >
                                    <ul class="list-unstyled" style="background:{{$val['blog_color']}};padding-top:10px;padding-bottom:3px;">
                                         @if(isset($val['social_media']))   
                               @foreach($val['social_media1'] as $key=>$media)
                               @if($val['socialMediaList'][$key]!=='fa-whatsapp' && $val['socialMediaList'][$key]!=='fa-facebook')
                                        <li>
                                            <a href="{{$media}}" style="background:{{$val['scondary']}};">
                                                <i class="fa {{$val['socialMediaList'][$key]}}" aria-hidden="true" style="color:{{$val['primary']}}"></i>
                                            </a>
                                        </li>
                                        
                                        
                                         @endif
                                         
                                          @if($val['socialMediaList'][$key]=='fa-facebook')
                                        <li>
                                            <a href="{{$media}}" style="background:{{$val['scondary']}};">
                                                <i class="fa {{$val['socialMediaList'][$key]}}" aria-hidden="true" style="color:{{$val['primary']}};margin-top:3px;padding-left:2px;"></i>
                                            </a>
                                        </li>
                                        
                                        
                                         @endif
                                          @endforeach
                                      @endif
                                    </ul>
                                </div>
                            </li>
                             @if(isset($val['social_media']))   
                               @foreach($val['social_media1'] as $key=>$media)
                               @if($val['socialMediaList'][$key]=='fa-whatsapp')
                            <li class="whatsApp">
                                <a href="{{$media}}" style="background:{{$val['scondary']}};">
                                    <i class="fa {{$val['socialMediaList'][$key]}}" aria-hidden="true" style="color:{{$val['primary']}}"></i>
                                </a>
                            </li>
                            @endif
                            @endforeach
                            @endif
                            
                        </ul>
                    </div>


            </div>
        
        </section>

    </body>
    
    </html>