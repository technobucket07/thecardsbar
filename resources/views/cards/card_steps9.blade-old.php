@extends('cards.layouts.master')
@section('css')
         <link href="{{ url('web_assets/css/form-wizard.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/css/dropify.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/bootstrap.min.css')}}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/font-awesome.min.css')}}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/main.css')}}" rel="stylesheet">
         
          <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/demo/prism.css" />
         <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/bower_components/sweetalert/dist/sweetalert.css" />
	     <link rel="Stylesheet" type="text/css" href="{{ url('web_assets/js/style1.css?abc')}}" />
	     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	     <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
		 <script src="https://foliotek.github.io/Croppie/croppie.js"></script>
		 <script src="{{ url('web_assets/js/crop_js/script9.js')}}"></script>
       
    <style>
        .text-muted1 {
     color: red;
        }
        .progressbar li {
         
        }
        
        #loader { 
      border: 12px solid #f3f3f3; 
      border-radius: 50%; 
      border-top: 12px solid #444444; 
      width: 70px; 
      height: 70px; 
      animation: spin 1s linear infinite; 
    } 
    
    @keyframes spin { 
      100% { 
        transform: rotate(360deg); 
      } 
    } 
    
    .center { 
      position: absolute; 
      top: 0; 
      bottom: 0; 
      left: 0; 
      right: 0; 
      margin: auto; 
    } 
    

    </style>     
    
       
@section('content')
    <!-- BACK BUTTON STARTS HERE -->


    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="backBtn">
                        <a class="btnCustomStyle2 btn-solid" href="index.html">
                            <span>Back</span>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- BACK BUTTON END HERE -->

    <section class="upperCircles">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="progressbar list-unstyled" style="width: 100%;">
                        <li class="active" style=" width: 33%;"></li>
                        <li id="active1"  style=" width: 33%;"></li>
                        <li id="active2"  style=" width: 33%;"></li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- FORM WIZARD STARTS HERE -->
    <section class="mainFormWizard">
        <div class="container-fluid">
            <span id="theme_name" style="display: none;">sky</span>
            <span id="trans_name" style="display: none;">slide</span>
            <span id="rtl" style="display: none;">0</span>
            <div class="row">
                <div class="col-md-9">
    <ul>
            @foreach ($errors->all() as $error)
           
              <li> <div class="alert alert-warning">
               <strong>Warning!</strong> {{ $error }}
              </div></li>
            @endforeach
        </ul>
                    <form id="wizard_example_2"  method="post" enctype="multipart/form-data" >
                       
                        @csrf
                        <fieldset>
                            <legend>Color</legend>
                            <div class="row">
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Primary Color</label>
                                             <input type="color" class="form-control" id="Primary" name="primary" style="height:80px;width:200px;" value="#f7f9fb">
                                           
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Secondary Color</label>
                                             <input type="color" class="form-control" id="Secondary" name="scondary" style="height:80px;width:200px;" value="#fff">
                                        
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Text Color</label>
                                               <input type="color" class="form-control" id="text" name="text" style="height:80px;width:200px;" value="#fff">
                                         
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Heading Color</label>
                                        <input type="color" class="form-control" id="heading" name="heading_color" style="height:80px;width:200px;" value="#374569">
                                          
                                           
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Blog Text Color</label>
                                             <input type="color" class="form-control" id="heading2" name="blogtext_color" style="height:80px;width:200px;" value="#8890a6">
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Social Media Color</label>
                                             <input type="color" class="form-control" id="heading1" name="social_media_color" style="height:80px;width:200px;" value="#c18f59">
                                        </div>
                                       
                                    </div>
                                </div>
                                
                              
                               
                                
                                
                                
                               
                            </div>
                        </fieldset>
                      
                        <fieldset>
                            <legend>Social Links</legend>
                            @include('cards.socialmedia')
                           <!--  <div class="row">
                                <div class="col-md-12">
                                    <span class="col" style="color:red;font-size:16px;"></span>
                                    <ul class="list-unstyled socialLinksboxx">
                                        @foreach($socialMedia as $social)
                                         @if($social['icon']!=='fa-linkedin')
                                            <li class="clearfix">
                                                 <span class="titleSocial" >
                                                     <i class="fa {{$social['icon']}}" aria-hidden="true"></i>{{$social['name']}}
                                                 </span>
                                                  @if($social['icon']=='fa-whatsapp')
                                              
                                                 <select class="form-control" name="code" style="width: 78px; height: 29px;float: left;margin-top: 0px;font-size: 12px;margin-left: 63px;" id="code">
                                                    @foreach($va as $va1)
                                                   <option value="{{$va1->callingCode}}">{{$va1->callingCode}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$va1->country}} </option>
                                                    @endforeach
                                                </select>
                                                 <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="7484564564">
                                                 </span>
                                                   @elseif($social['icon']=='fa-facebook')
                                                  <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.facebook.com">
                                                 </span>
                                                 
                                                 @elseif($social['icon']=='fa-skype')
                                                  <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.skype.com">
                                                 </span>
                                                 
                                                
                                                  
                                                   
                                                    @elseif($social['icon']=='fa-twitter')
                                                   <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://twitter.com">
                                                 </span>
                                                   @endif
                                                
                                            </li>
                                              @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div> -->
                        </fieldset>
                        
                        
                      <fieldset>
                            <legend>Profile </legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        
                                              <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne1">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        Image Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne1">
                                                
                                                <div class="panel-body personalBoxxx">
                                                    <div class="row">
                                                        
                                                        
                                                          <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Profile Image</label>
                                                                    <!--<input type="file" name="profile_image" id="input-file-now" class="dropify" />-->
                                                                    
                                                                    <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload1"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="profile_image" id="base64image1">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap1">
                                                                         <div id="upload-demo1"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Background Image</label>
                                                                    <!--<input type="file" name="bg_image" id="input-file-now1" class="dropify" />-->
                                                                    
                                                                     <div class="row">
                                                                      <div class="col-md-12">
                                                                        <input type="file" id="upload2"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="bg_image" id="base64image2">
                                                                      </div>
                                                                      <div class="col-md-12">
                                                                         <div class="upload-demo-wrap2">
                                                                         <div id="upload-demo2"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                          
                                                         <div class="col-md-12 col-lg-12 ">
                                                            <div class="form-group">
                                                                <input type="text" name="bottom_title" class="form-control" placeholder="Bottom Title " maxlength="25" value="Professional Law Firm">
                                                                    <small class="text-muted1 bottom_title"></small>
                                                            </div>
                                                        </div>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
                                                        Personal Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                
                                                <div class="panel-body personalBoxxx">
                                                  @include('cards.createfor')
                                                    <div class="row">
                                                        
                                                        
                                                          
                                                        
                                                        
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" name="first_name" placeholder="Enter First Name" value="Paul">
                                                                <small class="text-muted1 first_name"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Enter Last Name" name="last_name" value="Goodman">
                                                                <small class="text-muted1 last_name"></small>
                                                            </div>
                                                        </div>
                                                        
                                                       
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Enter Designation" name="designation" maxlength="40" value="YOUR ATTORNEY">
                                                                 <small class="text-muted1 designation"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" name="phone" class="form-control" placeholder="Enter Phone Number" maxlength="10" value="975.79.098">
                                                                    <small class="text-muted1 phone1"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" name="call_us" class="form-control" placeholder="Enter Call Us Tag line" maxlength="30" value="Call Us For Free Consultation">
                                                                    <small class="text-muted1 call_us"></small>
                                                            </div>
                                                        </div>
                                                       
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" name="time" class="form-control" placeholder="Enter Opening time (8:00 - 19:00)" value="8:00 - 19:00">
                                                                    <small class="text-muted1 time"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                         
                                                         
                                                          <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" name="days" class="form-control" placeholder="Enter Opening days (Mon - Fri)" value="Opening Hours Mon - Fri">
                                                                    <small class="text-muted1 days"></small>
                                                            </div>
                                                        </div>
                                                       <div class="col-md-12 col-lg-12 ">
                                                            <div class="form-group">
                                                                <textarea type="text" name="address" id="address" class="form-control" placeholder="Enter Address" maxlength="78">12 King Street, Firs Avenue, Melbourne , Australia</textarea>
                                                                    <small class="text-muted1 address"></small>
                                                            </div> 
                                                        </div>
                                                        <div class="col-md-12 col-lg-12 ">
                                                        <div class="form-group ">
                                                          <input type="text" id="autocomplete" class="form-control" placeholder="Search your location" value="" onFocus="geolocate()">
                                                           <small class="text-muted1 address"></small>
                                                          <input type="hidden" name="location" id="latlong" value="">
                                                        </div>
                                                      </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
           
                       
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingfour">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                                        Service Name
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                                <div class="panel-body businessInformation">

                                                    <div class="row">

                                                       
                                                        
                                                     
                                                        
                                                         <div class="col-md-12 col-lg-12 ">
                                                            <div class="form-group">
                                                                <label>Service Name 1</label>
                                                                  <!--<input type="file" name="service-image-1" id="input-file-now2" class="dropify" />-->
                                                                  
                                                                  <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload3"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="serviceimage1" id="base64image3">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap3">
                                                                         <div id="upload-demo3"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                  
                                                                  
                                                                  
                                                                <input type="text" class="form-control" placeholder="Enter Service 1" name="service-name-1" maxlength="15" value="Family Law">
                                                                 <input type="text" class="form-control" placeholder="Enter Cases 1" name="case1" maxlength="15" value="50 Cases">
                                                                 <small class="text-muted1 service-name-1"></small>
                                                            </div>
                                                        </div>
                                                      
                                                        
                                                         <div class="col-md-12 col-lg-12 ">
                                                            <div class="form-group">
                                                                <label>Service Name 2</label>
                                                                 <!--<input type="file" name="service-image-2" id="input-file-now3" class="dropify" />-->
                                                                 
                                                                  <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload4"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="serviceimage2" id="base64image4">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap4">
                                                                         <div id="upload-demo4"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                 
                                                                 
                                                                <input type="text" class="form-control" placeholder="Enter Service 2" name="service-name-2" maxlength="15" value="Business Law">
                                                                  <input type="text" class="form-control" placeholder="Enter Cases 2" name="case2" maxlength="15" value="35 Cases">
                                                                 <small class="text-muted1 service-name-2"></small>
                                                            </div>
                                                        </div>
                                                       
                                                         <div class="col-md-12 col-lg-12">
                                                            <div class="form-group">
                                                                <label>Service Name 3</label>
                                                                 <!--<input type="file" name="service-image-3" id="input-file-now4" class="dropify" />-->
                                                                 
                                                                   <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload5"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="serviceimage3" id="base64image5">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap5">
                                                                         <div id="upload-demo5"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                 
                                                                 
                                                                <input type="text" class="form-control" placeholder="Enter Service 3" name="service-name-3" maxlength="15" value="Real Estates">
                                                                  <input type="text" class="form-control" placeholder="Enter Cases 3" name="case3" maxlength="15" value="20 Cases">
                                                                 <small class="text-muted1 service-name-3"></small>
                                                            </div>
                                                        </div>
                                                       
                                                        
                                                         <div class="col-md-12 col-lg-12 ">
                                                            <div class="form-group">
                                                                <label>Service Name 4</label>
                                                                 <!--<input type="file" name="service-image-4" id="input-file-now5" class="dropify" />-->
                                                                 
                                                                 <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload6"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="serviceimage4" id="base64image6">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap6">
                                                                         <div id="upload-demo6"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                 
                                                                <input type="text" class="form-control" placeholder="Enter Service 4" name="service-name-4" maxlength="15" value="Civil Litigation">
                                                                  <input type="text" class="form-control" placeholder="Enter Cases 4" name="case4" maxlength="15" value="30 Cases">
                                                                 <small class="text-muted1 service-name-4"></small>
                                                            </div>
                                                        </div>
                                                        
                                                         
                                                       
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                             
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                            <div class="col-md-12">
                                <p class="backBtn">
                                    <a class="btnCustomStyle2 btn-solid Preview1 Preview2 Preview3 Preview4 Preview5 Preview6" href="javascript:void(0);">
                                        <span>Preview</span>
                                    </a>
                                </p>
                            </div>
                          
                       </form>
                    </div>
                <div class="col-md-3">
                    
                    <div id="frame" style="background-image: url('{{ url("web_assets")}}/img/Phone1.png');background-color: #cccccc;background-position: center;background-repeat: no-repeat;background-size: cover;
  position: relative;width: 263px;height: 528px;border-radius: 30px;"></br>
                         <div id="previewImage" ></div>
                         <div id="loader" class="center" ></div> 
                          <div id="img" style="width: 235px;border-radius: 30px;margin-left: 15px;margin-top: -9px;">
                         <img class="img-fluid mx-auto d-block "  src="{{ url('web_assets')}}/img/desine-9.png" alt="" style="border-radius: 15px;height:500px;">
                    </div>
                  
                    </div>
                   
      
                   
                   
                </div>
                
    <style>


/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #c18f59; }
::selection { color: #f1f1f1;background: #c18f59; }



body  {font-family: 'Poppins', sans-serif;background: #131311;}
.wrapper {width: 400px;margin: 0 auto;height: 860px;background: #f7f9fb;}



.profileboxx {height: 290px;color: #fff;padding: 30px 30px 0 30px;}
.profileboxx:after {display: block;clear: both;content: "";}
.profileboxx .innerbox {float: left;width: 43%;text-align: center;}
.profileboxx .innerbox .userimg  {width: 114px;height: 114px;margin:0 auto;border-radius: 58px;}
.profileboxx .innerbox .userimg img {border-radius: 58px;width: 100%; height: 108px;}
.profileboxx .innerbox .designation  {font-size: 12px;text-transform: uppercase;margin: 10px 0 5px 0;}
.profileboxx .innerbox .name  {font-size: 16px;font-weight: 600;}
.profileboxx .rightInfo {float: right;width: 57%;text-align: right;}
.profileboxx .rightInfo ul {}
.profileboxx .rightInfo ul .first {}

.profileboxx .rightInfo ul .first .timing {font-size: 20px;margin: 0;}
.profileboxx .rightInfo ul .first .timing i {color: #c18f59;margin: 0 10px 0 0;}
.profileboxx .rightInfo ul .first .days {font-size: 12px;}

.profileboxx .rightInfo ul .third {}
.socialMediaIcons {}
.socialMediaIcons ul{margin: 0;}
.socialMediaIcons ul li {display: inline-block;margin: 0 2px;}
.socialMediaIcons ul li a {  font-size: 30px;width: 36px;height: 36px;line-height: 28px;text-align: center;text-decoration: none;border-radius: 18px;display: inline-block;background: #c18f59;color: #fff;}
.socialMediaIcons ul li a .fa {font-size: 18px; margin-top: 9px;}
.socialMediaIcons ul li a .fa:hover {opacity: 0.7;}

.address {color: #2b53c2;font-size: 13px;text-align: left;}



.serviceOffers {    padding: 0 30px;margin: -35px 0 0 0;}
.serviceOffers ul {    margin: 0;}
.serviceOffers ul:after {display: block;clear: both;content: "";}
.serviceOffers ul li {float: left;width: 170px;margin: 0 0 15px 0;padding: 0 7.5px;}
.serviceOffers ul li.marginMinustop {margin: -20px 0 15px 0;}







.serviceOffers ul li .innerserviceBoxx {text-align: left;background: #fff;border-radius: 15px;padding: 15px;}
.serviceOffers ul li .innerserviceBoxx .iconImg {padding: 30px 0;margin: 0;}
.serviceOffers ul li .innerserviceBoxx .iconImg img{width: 100%;height:110px;}
.serviceOffers ul li .innerserviceBoxx .title {margin: 0;font-size: 14px;font-weight: 600;color: #374569; }
.serviceOffers ul li .innerserviceBoxx .tagline {margin: 0;font-size: 12px;color: #8890a6;}


.bottomBoxx {padding: 0 30px;}
.bottomBoxx .Address{color: #8890a6;font-size: 12px;font-weight: 500;margin: 0;}
.bottomBoxx .infoAdd{color: #374569;font-size: 14px;font-weight: 500;margin: 0;}
.bottomBoxx .professional{color: #c18f59;font-size: 18px;font-weight: 700;text-align: center;}










/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


}



        </style>
     
    <div style="opacity:0;">      
<div id="html-content-holder">
     <section class="wrapper" id="wrapper">

            <div class="profileboxx">
                <div class="innerbox">
                    <p class="userimg">
                       
                    </p>
                    <p class="designation"></p>
                    <p class="name"></p>

                </div>
                <div class="rightInfo">
                    <ul class="list-unstyled">
                        <li class="first first1">
                           
                        </li>
                        <li class="first first2">
                           
                        </li>
                        <li class="third">
                            <div class="socialMediaIcons">
                                <ul class="list-unstyled">
                                  
                                    
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="serviceOffers">
                <ul class="list-unstyled">
                    
                   
                </ul>
            </div>


            <div class="bottomBoxx">
                <h6 class="Address">Address</h6>
                <p class="infoAdd"></p>
                <p class="professional" id="professional"> </p>
            </div>








        </section>

         
  
    </div> 
  </div>            
            
            
            
        </div>
        

        

        <script>
demoUpload1();
demoUpload2();
demoUpload3();
demoUpload4();
demoUpload5();
demoUpload6();

</script>
        
    </section>
    <!-- FORM WIZARD END HERE -->
@endsection

@section('scripts')
    <script src="{{ url('web_assets/js/html2canvas.js') }}"></script>
    <script src="{{ url('web_assets/js/form-wizard.js') }}"></script>
    <script src="{{ url('web_assets/js/jscolor.js') }}"></script>
    <script src="{{ url('web_assets/js/dropify.js') }}"></script>
     
<style>
    canvas {
    height: 502px;
    width: 238px;
    margin-top: -10px;
    border-radius: 27px;
    margin-left: 13px;

    }
</style>
    <script>
        jscolor.presets.default = {
            borderColor:'rgba(236,239,245,0.6)', width:200, height:100,
            buttonColor:'rgba(70,51,255,1)', shadowColor:'rgba(0,0,0,0.05)'
        };
    </script>

    <script>
        $(document).ready(function(){
            	$('#myModal').hide();
            	
            	
            // 	 $('.btnCustomStyle2').hide();
            	 $('#code').val('+91');
            	 $('#loader').hide(); 
            	 $('#wrapper').hide();
                 $('.Address').hide();
            view();
            $('input[type="color"]').change(function(){
              $('.btnCustomStyle2').show();
            });
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });
           
            $("[type='file']").focusout(function(){
            
            //      var files =$('#input-file-now').prop('files')[0];
            //   if(files[0]!==''){
            //       if(files[0].size > (1024*1024)){
            //       alert('Please Insert Image Max 2MB.!');
            //       $("#input-file-now").val(null);
            //       }
            //   }
                  
            //       var files1 =$('#input-file-now1').prop('files')[0];
            //   if(files1[0]!==''){
            //       if(files1[0].size > (1024*1024)){
            //       alert('Please Insert Image Max 2MB.!');
            //       $("#input-file-now1").val(null);
            //       }
            //   }
                  
             
               });
               
             $('.btnCustomStyle2').click(function(){
                     $('#wrapper').show();
                  	$('#loader').show();
            //   var  files =$('#input-file-now').prop('files')[0];
            
              
                var form_data = new FormData($('#wizard_example_2')[0]);
                //   form_data.append("file", files);
                    
   
                       $.ajax({
                            type: 'post',
                            url:  "{{route('admin.Cards.cards9')}}",
                            
                            data: form_data,
                            async: false,
                            dataType:'json',
                            success: function (data1) {
                            var    data=data1.va;
                                console.log(data1);
                                if(data.profile_image!==null) {
                                   
                                     $('.userimg').html('<img class="img-fluid d-block mx-auto" src="'+data.profile_image+'" alt="profilepic">');
                                     $('.userimg').css('border','3px solid '+data.text);
                                      
                                }else{
                                  var img1 ="{{url('/web_assets/design-image/design-9/profilepic.jpg')}}";
                                    $('.userimg').html('<img class="img-fluid d-block mx-auto" src="'+img1+'" alt="profilepic">');
                                    $('.userimg').css('border','3px solid '+data.text);
                                }
                                if(data.bg_image!==null){
                                   
                                       $('.profileboxx').css('background-image','url('+data.bg_image+')');
                                 }else{
                                       $('.profileboxx').css('background-image',"url({{url('/web_assets/design-image/design-9/bgimg.jpg')}})");
                                 }
                                 
                                if(data.time){
                                    $('.first1').html('<p class="timing" style="color:'+data.text+';"><i class="fa fa-history" aria-hidden="true" style="color:'+data.social_media_color+';"></i>'+data.time+'</p><p class="days" style="color:'+data.text+';">'+data.days+'</p>')
                                }
                             
                                if(data.first_name!==null && data.last_name!==null){
                                    $('.name').css('color',data.text);
                                   
                                    $('.name').text(data.first_name+' '+data.last_name);
                                   
                                }
                                
                                if(data.designation!==null){
                                   $('.designation').text(data.designation);
                                   $('.designation').css('color',data.text);
                                }
                                
                                if(data.phone!==null){
                                 
                                    $('.first2').html('<p class="timing" style="color:'+data.text+';"><i class="fa fa-phone" aria-hidden="true" style="color:'+data.social_media_color+';"></i>'+data.code+'-'+data.phone+'</p><p class="days" style="color:'+data.text+';">'+data.call_us+'</p>');
                                }
                            var html1='';
                                if(data.servicename1!==null && data.case1!==null    && data.serviceimage1!==null){

                                   html1+='<li class="marginMinustop"><div class="innerserviceBoxx" style="background:'+data.scondary+';"><p class="iconImg"><img src="'+data.serviceimage1+'" alt="ser-1" class="img-fluid d-block"></p><p class="title" style="color:'+data.heading_color+';">'+data.servicename1+'</p><p class="tagline" style="color:'+data.blogtext_color+';">'+data.case1+'</p></div></li>';
                                }else{
                                  var img2 ="{{url('/web_assets/design-image/design-9/ser-1.jpg')}}";
                                     html1+='<li class="marginMinustop"><div class="innerserviceBoxx" style="background:'+data.scondary+';"><p class="iconImg"><img src="'+img2+'" alt="ser-1" class="img-fluid d-block"></p><p class="title" style="color:'+data.heading_color+';">'+data.servicename1+'</p><p class="tagline" style="color:'+data.blogtext_color+';">'+data.case1+'</p></div></li>';
                                }
                                 
                                  if(data.servicename2!==null && data.case2!==null  && data.serviceimage2!==null){

                                   html1+='<li ><div class="innerserviceBoxx" style="background:'+data.scondary+';"><p class="iconImg"><img src="'+data.serviceimage2+'" alt="ser-1" class="img-fluid d-block"></p><p class="title" style="color:'+data.heading_color+';">'+data.servicename2+'</p><p class="tagline" style="color:'+data.blogtext_color+';">'+data.case2+'</p></div></li>';
                                }else{
                                  var img3 ="{{url('/web_assets/design-image/design-9/ser-2.jpg')}}";
                                    html1+='<li ><div class="innerserviceBoxx" style="background:'+data.scondary+';"><p class="iconImg"><img src="'+img3+'" alt="ser-1" class="img-fluid d-block"></p><p class="title" style="color:'+data.heading_color+';">'+data.servicename2+'</p><p class="tagline" style="color:'+data.blogtext_color+';">'+data.case2+'</p></div></li>';
                                }
                                 
                                  if(data.servicename3!==null && data.case3!==null && data.serviceimage3!==null){

                                   html1+='<li class="marginMinustop"><div class="innerserviceBoxx" style="background:'+data.scondary+';"><p class="iconImg"><img src="'+data.serviceimage3+'" alt="ser-1" class="img-fluid d-block"></p><p class="title" style="color:'+data.heading_color+';">'+data.servicename3+'</p><p class="tagline" style="color:'+data.blogtext_color+';">'+data.case3+'</p></div></li>';
                                }else{
                                  var img4 ="{{url('/web_assets/design-image/design-9/ser-3.jpg')}}";
                                     html1+='<li class="marginMinustop"><div class="innerserviceBoxx" style="background:'+data.scondary+';"><p class="iconImg"><img src="'+img4+'" alt="ser-1" class="img-fluid d-block"></p><p class="title" style="color:'+data.heading_color+';">'+data.servicename3+'</p><p class="tagline" style="color:'+data.blogtext_color+';">'+data.case3+'</p></div></li>';
                                }
                                
                                 if(data.servicename4!==null && data.case4!==null && data.serviceimage4!==null){

                                   html1+='<li><div class="innerserviceBoxx" style="background:'+data.scondary+';"><p class="iconImg"><img src="'+data.serviceimage4+'" alt="ser-1" class="img-fluid d-block"></p><p class="title" style="color:'+data.heading_color+';">'+data.servicename4+'</p><p class="tagline" style="color:'+data.blogtext_color+';">'+data.case4+'</p></div></li>';
                                }else{
                                  var img5 ="{{url('/web_assets/design-image/design-9/ser-4.jpg')}}";
                                    html1+='<li><div class="innerserviceBoxx" style="background:'+data.scondary+';"><p class="iconImg"><img src="'+img5+'" alt="ser-1" class="img-fluid d-block"></p><p class="title" style="color:'+data.heading_color+';">'+data.servicename4+'</p><p class="tagline" style="color:'+data.blogtext_color+';">'+data.case4+'</p></div></li>';
                                }
                                
                                $('.serviceOffers .list-unstyled').html(html1);
                                
                                 if(data.address!==null){
                                    $('.Address').show();
                                   $('.infoAdd').text(data.address);
                                   $('.infoAdd').css('color',data.heading_color);
                                   $('.Address').css('color',data.blogtext_color);
                                }
                                 if(data.bottom_title!==null){
                                     $('.professional').text(data.bottom_title);
                                     $('.professional').css('color',data.social_media_color);
                                 }
                                  var i;
                                  var html='';
                                 
                                 
                                      if(data.socialMediaList[1]=='fa-facebook'){
                                      if(data.social_media[1]!==null){

                                          html+='<li><a href="#" style="background:'+data.social_media_color+';color:'+data.text+';"><i class="fa '+data.socialMediaList[1]+'" aria-hidden="true"></i></a></li>';
                                       } 
                                       }
                                       
                                        if(data.socialMediaList[2]=='fa-skype'){
                                       if(data.social_media[2]!==null){
                                      
                                          html+='<li><a href="#" style="background:'+data.social_media_color+';color:'+data.text+';"><i class="fa '+data.socialMediaList[2]+'" aria-hidden="true"></i></a></li>';
                                        }
                                        }
                                      
                                      if(data.socialMediaList[3]=='fa-whatsapp'){
                               if(data.social_media[3]!==null){
                                      
                                          html+='<li><a href="#" style="background:'+data.social_media_color+';color:'+data.text+';"><i class="fa '+data.socialMediaList[3]+'" aria-hidden="true"></i></a></li>';
                                       } 
                                      }
                                      
                                      
                                   if(data.socialMediaList[5]=='fa-twitter'){
                                        if(data.social_media[5]!==null){
                                     
                                         html+='<li><a href="#" style="background:'+data.social_media_color+';color:'+data.text+';"><i class="fa '+data.socialMediaList[5]+'" aria-hidden="true"></i></a></li>';
                                        } 
                                   }
                                      
                                       
                                       
                                           
                                    
                                  
                                   
                               
                                $('.socialMediaIcons .list-unstyled').html(html);
                               
                             
                                 $(".profilePicBoxx").css("background",data.scondary);
                                 $(".wrapper").css("background",data.primary);
                                 
                         
                                
                              	$('#img').hide(); 
                              		setTimeout(function(){ $('#loader').hide();  }, 4000);
                              	 	dview();
                               		//$('#myModal').show();
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                   
      
         });
           
         $('button').click(function(){
             $('#myModal').hide(); 
         });   
            
            $('.finish-btn').click(function(){
                 $('form').attr('action', "{{route('admin.Cards.generate_pdf9')}}");
                 $('form').attr('target','popup');
                 var urrl = "{{route('admin.Cards.generate_pdf9')}}";
                 $('form').attr(' onclick',"window.open("+urrl+",'popup','width=800,height=800');");
            });
            
            function view(){
            
                $.ajax({
                    url:"{{route('admin.Cards.delete-image')}}",
                    method:"get",
                    success: function(data) {
                        console.log(data);
                    }
                    
                });
            }
            
            
            
// global variable
 
    function dview() {
         var element = $("#html-content-holder"); // global variable
       var getCanvas; 

         html2canvas(element, {
              x: 50, y: 100,
              draggable: true,
         onrendered: function (canvas) {
                 
                $("#previewImage").html(canvas);
                getCanvas = canvas;
             
             }
         });
             
          $('#frame').show(); 
        $('#wrapper').hide(); 
        $('#img').hide(); 
       
    }
    //$('#img').hide(); 
   // $('#frame').hide(); 
  //$('#wrapper').hide();  
 
 
      //("#Secondary option[value="+ color +"]").prop('disabled', true);              
    //   $("#text option[value=" + color + "]").prop('disabled', true);
    //     $("#Primary option[value="+ color +"]").prop('disabled', true);
   
    $("#Primary").change(function(){
       var color=$("#Primary").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', true);
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', false);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
    }) ;   
        
  $("#Secondary").change(function(){
       var color=$("#Secondary").val();
       $("#heading1 option[value=" + color + "]").prop('disabled', true);
       $("#heading option[value=" + color + "]").prop('disabled', true);
       $("#Secondary option[value="+ color +"]").prop('disabled', false);
       $("#Primary option[value="+ color +"]").prop('disabled', true);
       $("#text option[value=" + color + "]").prop('disabled', true);
    }) ;   
    
    $("#text").change(function(){
       var color=$("#text").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', true);
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', false);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
         $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
    
     $("#heading").change(function(){
       var color=$("#heading").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', true);
        $("#heading option[value=" + color + "]").prop('disabled', false);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
        
        
         $("#heading1").change(function(){
       var color=$("#heading1").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', false);
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
         $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
         
        
        
        //  $('.sf-controls').css("display","none");
        
          $('input[type="color"]').change(function(){
             var select1=$('#Primary').val();
             var select2=$('#Secondary').val();
             var select3=$('#text').val();
             var select4=$('#heading').val();
             var select5=$('#button_color').val();
             if(select1!=='Choose Color' && select2!=='Choose Color' && select3!=='Choose Color' && select4!=='Choose Color' && select5!=='Choose Color'){
                 $('.sf-controls').css("display","block");
                   $('#active1').attr('class','active');
             }
            
        });
     
    //  $('.next-btn').click(function(){
    //           $('.sf-controls').css("display","none");
    //     });   
        
        
         $('.socialLinksboxx').on("focusout","input",function(){
             
           
             var input1=$('input[name="social_media[1]"]').val();
             var input2=$('input[name="social_media[2]"]').val();
             var input3=$('input[name="social_media[3]"]').val();
            //  var input4=$('input[name="social_media[4]"]').val();
             var input5=$('input[name="social_media[5]"]').val();
             
             if(input1!=='' && input2!=='' && input3!==''  && input5!==''){
                   $('.sf-controls').css("display","block");
                     $('#active2').attr('class','active');
             }else{
                   $('.sf-controls').css("display","none");
                   $('.col').text('*Please fill all the boxes');
             }
            
        });
        
        
        
         $('.panel-group').on("focusout","input",function(){
            
             var first_name=$('input[name="first_name"]').val();
             var last_name=$('input[name="last_name"]').val();
             var phone=$('input[name="phone"]').val();
             var designation=$('input[name="designation"]').val();
             var email=$('input[name="email"]').val();
             var bottom_title=$('input[name="bottom_title"]').val();
             var address=$('#address').val();
             var about=$('#about').val();
             var service_name_1=$('input[name="service-name-1"]').val();
             var service_name_2=$('input[name="service-name-2"]').val();
             var service_name_3=$('input[name="service-name-3"]').val();
             var service_name_4=$('input[name="service-name-4"]').val();
             var service_name_5=$('input[name="service-name-5"]').val();
             var service_name_6=$('input[name="service-name-6"]').val();
             
                              if(first_name.length <31){
                                
                                  first_name=first_name;
                              }else{
                                  $('.first_name').text('First Name  max 30 Char');
                                  $('.first_name').css('color','red');
                                    // alert('*First Name max 30 Char');
                              }
                              
                            
                              
                               
                              
                              if(last_name.length <31){
                                 
                                  last_name=last_name;
                              }else{
                                   $('.last_name').text('Last Name max 30 Char');
                                    $('.last_name').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              
                            
                              
                              if(designation.length <31){
                                 
                                  designation=designation;
                              }else{
                                   $('.designation').text('Designation max 30 Char');
                                    $('.designation').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              
                            //   if(about.length <100){
                                 
                            //       about1=about;
                            //   }else{
                            //       $('.about').text('About max 100 Char');
                            //       $('.about').css('color','red');
                            //       //alert('*Last Name max 30 Char');
                            //   }
                           
                              
                               if(address.length <81){
                                 
                                  address1=address;
                              }else{
                                   $('.address').text('Address max 80 Char');
                                   $('.address').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                           
                              
                               if(service_name_1.length <26){
                                 
                                   service_name_1= service_name_1;
                              }else{
                                  
                                   $('.service-name-1').text('Service Name 1 max 25 Char');
                                   $('.service-name-1').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_2.length <26){
                                 
                                   service_name_2= service_name_2;
                              }else{
                                   service_name_2='';
                                   $('.service-name-2').text('Service Name 2 max 25 Char');
                                   $('.service-name-2').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_3.length <26){
                                 
                                   service_name_3= service_name_3;
                              }else{
                                   $('.service-name-3').text('Service Name 3 max 25 Char');
                                   $('.service-name-3').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              if(service_name_4.length <26){
                                 
                                   service_name_4= service_name_4;
                              }else{
                                   $('.service-name-4').text('Service Name 4 max 25 Char');
                                   $('.service-name-4').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                            //   if(service_name_5.length <26){
                                 
                            //       service_name_5= service_name_5;
                            //   }else{
                            //       $('.service-name-5').text('Service Name 5 max 25 Char');
                            //       $('.service-name-5').css('color','red');
                            //       //alert('*Last Name max 30 Char');
                            //   }
                              
                            //   if(service_name_6.length <26){
                                 
                            //       service_name_6= service_name_6;
                            //   }else{
                            //       $('.service-name-6').text('Service Name 6 max 25 Char');
                            //          $('.service-name-6').css('color','red');
                            //       //alert('*Last Name max 30 Char');
                            //   }
                              
                              
                              
                            
                             
                              if(phone!==''){
                              if(validatePhone(phone)==true){
                                 
                                  phone=phone;
                                   $('.phone1').css('color','green');
                                   $(".phone1").text('*Phone Valid..!');
                              }else{
                                   //alert('Is not Valid..!');
                                   $('.phone1').css('color','red');
                                   $('.phone1').text('*Is not Valid Phone No... only no!');
                              }
                              }
                              
                            //   if(email!==''){
                            //   if(isEmail(email)==true){
                                 
                            //       email1=email;
                            //       $('.email').css('color','green');
                            //       $(".email").text('* E-mail Valid..!');
                            //   }else{
                            //       //alert('Is not Valid..!');
                            //          $('.email').css('color','red');
                            //       $('.email').text('* Is not Valid E-mail..!');
                            //   }
                            //   }
                             
             if(first_name!=='' && last_name!==''  && phone!=='' && designation!=='' && address1!=='' && service_name_1!=='' && service_name_2!=='' && service_name_3!=='' && service_name_4!==''){
                   $('.sf-controls').css("display","block");
             }else{
                  $('.sf-controls').css("display","none");
             }
            
        });
        
        
        function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
          if (regex.test(email)) {
                 return true;
               }
            else {
                  return false;
             }
        }
        
        
     function validatePhone(Phone) {
    
    var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (filter.test(Phone)) {
        return true;
    }
    else {
        return false;
    }
}
                    
});
    </script>
    
   
    
    
@endsection