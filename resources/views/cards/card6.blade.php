<html>
        <!--Core CSS -->
      <link href="{{ url('bootstrap-4/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{ url('bootstrap-4/css/font-awesome.min.css')}}" rel="stylesheet">
        
    
     
     
     
        <style>
            
           
/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #f0394d; }
::selection { color: #f1f1f1;background: #f0394d; }



body  {font-family: 'Poppins', sans-serif;}
.wrapper {margin: 0 auto;background: #fff;}

.TopBlankbox {    background: #0163d2;
    /* visibility: hidden; */
    padding: 40px;
    height: 110px;
    
}




.mainbody {
    background: #fff;
    padding: 20px 40px 0 40px;
    border-radius: 36px 36px 0 0;
    height: 100%;
        margin: -30px 0 0 0;
}


.logoMainBox {}
.logoMainBox .companyPic {    width: 250px;
    margin: -70px auto 20px auto;
    padding: 20px;
    border-radius: 20px;
    /*border: 1px solid #eff6fc;*/
    /*box-shadow: 0 10px 26px rgb(167 167 167 / 10%);*/
    border-bottom:3px solid #6b5c5c40;
     border-right:2px solid #6b5c5c40;
      border-left:0.6px solid #6b5c5c40;
    background: #fff;}
.logoMainBox .companyPic img {}
.logoMainBox h1 {    font-size: 24px;
    color: #000333;
    margin: 0;
    }
.logoMainBox p {    font-size: 16px;
    color: #babdc4;
    margin: 0;}




.proprietorName {    margin: 20px 0 0 0;}
.proprietorName ul {       padding: 20px; margin: 0;
    background: #eff6fc;
    border-radius: 10px;}
.proprietorName ul li {margin: 0 0 15px 0;}
.proprietorName ul li:last-child {margin: 0;}

.proprietorName ul li .titleName {    margin: 0;
    display: inline-block;
    width: 70%;
    height: 34px;
    line-height: 36px;
    float: left;
    font-size: 18px;
   
    color: #000333;}
.proprietorName ul li .calling { margin: 0;   display: inline-block;
    width: 30%;
    float: left;     text-align: right; }
.proprietorName ul li .calling a {    display: inline-block;
    padding: 5px 12px;
    background: #f7d807;
    color: #000333;
    
    border-radius: 10px;
}
.proprietorName ul li .calling a i {}




.serviceoffersBox {    margin: 20px 0 0 0;}
.serviceoffersBox .row {    margin-right: -7.5px;
    margin-left: -7.5px;}
.serviceoffersBox .row .col-md-6 {padding-right: 7.5px;
    padding-left: 7.5px;}
.serviceoffersBox h2 {font-size: 16px;color: #000333;}
.serviceoffersBox .user-card {    background: #eff6fc;
    border-radius: 10px;
    padding: 15px 0;
    margin: 0 0 15px 0;}
.serviceoffersBox .user-card p {}
.serviceoffersBox .user-card p img {}
.serviceoffersBox .user-card h4 {font-size: 12px;text-align: center;color: #000333;}



.socialMediaIcons {}
.socialMediaIcons ul{    margin: 0 auto;
    display: table;}
.socialMediaIcons ul li {    display: inline-block;
    margin: 0 5px;
    padding: 2px;
    background: #f7d807;
    border-radius: 50px;}
.socialMediaIcons ul li a {  
    font-size: 30px;
    width: 50px;
    height: 50px;
    line-height: 50px;
    text-align: center;
    text-decoration: none;
    border-radius: 50%;
    display: inline-block;
    background: #f7d807;
    color: #000333;
    border: 2px solid #ffffff;
}
.socialMediaIcons ul li a .fa {
    font-size: 26px;
}

.socialMediaIcons ul li a .fa:hover {
    opacity: 0.7;
}

.servicediv{
  padding:30px 0px 0px 0px;
}
.serviceimg{
  width:70px;
  height:70px;
}








/* Media Queries */


        </style>
    {{--dd($val)--}}

        <body style="padding-bottom:-150px;">
 <section class="wrapper" style="width:440px;background:red;height:930px;">     
      

             
         
                    <div class="TopBlankbox" style="background:{{$val['scondary']}}"></div>
                     <div style="">
                   <table  class="mainbody" style="width:100%;height:850px;background:{{$val['primary']}};" align="center">
                       <tr>
                          <td colspan="2">
                               <div class="logoMainBox">
                                  <p class="companyPic" style="height:50px;background:{{$val['primary']}};" align="center">
                                     @if($val['logo']!==null)
                                     <img src="{{$val['logo']}}" alt="logo-pic"  style="height:50px:">
                                      @else
                                     <img src="{{url('web_assets/design-image/design-6/logo-pic.png')}}" alt="logo-pic"  style="height:50px:">
                                    @endif
                                  </p>
                                <h1 class="text-center text-uppercase" style="color: {{$val['text']}};font-style:bold;">{{$val['company_name']}}</h1>
                             <p class="text-center" style="color: {{$val['text']}};">{{$val['quotation']}}</p>
                           </div>
                       </td>
                   </tr>
                   <tr>
                       <td colspan="2">
                             <div class="proprietorName" >
                                <ul class="list-unstyled" style="background:{{$val['blog_color']}}">
                                    <li class="clearfix">
                                        <p class="titleName" style="font-size: 18px;color:{{$val['text']}};font-style:none;">{{$val['first_name1']}} {{$val['last_name1']}}</p>
                                        <p class="calling" style="margin-top:10px;">
                                            <a href="tel:{{$val['phone1']}}" style="color:{{$val['text']}};background:{{$val['button_color']}};font-style:none;"> Call <i class="fa fa-phone" aria-hidden="true" style="margin-top:4px;"></i></a>
                                        </p>
                                    </li>
                                    <li class="clearfix">
                                        <p class="titleName"  style="font-size: 18px;color:{{$val['text']}};font-style:none;">{{$val['first_name2']}} {{$val['last_name2']}}</p>
                                        <p class="calling" style="margin-top:10px;">
                                            <a href="tel:{{$val['phone2']}}" style="color:{{$val['text']}};background:{{$val['button_color']}};font-style:none;"> Call <i class="fa fa-phone" aria-hidden="true" style="margin-top:4px;"></i></a>
                                        </p>
                                    </li>
                                </ul>
                            </div>
                           
                       </td>
                     </tr>
                     <tr>
                         <td colspan="2" style="height:30px;"></br>
                             <h2 style="font-size: 16px;color: {{$val['text']}};font-style:bold;">Service Offers</h2>
                         </td>
                     </tr>
                     <tr>
                         <td style="height:150px;width:150px;" align="center">
                             <div style="padding:5px;">
                                   <div class="servicediv" style="background:{{$val['blog_color']}};">
                                       @if($val['serviceimage1']!==null)
                                      <img src="{{$val['serviceimage1']}}" alt="ser-1" class="serviceimg"> <h4 style="font-size: 13px;text-align: center;color: {{$val['text']}};width:160px;height:50px;text-align:center;font-style:bold;">{{$val['service-name-1']}}</h4>
                                    @else
                                     <img src="{{url('web_assets/design-image/design-6/ser-1.png')}}" alt="ser-1" class="serviceimg"> <h4 style="font-size: 13px;text-align: center;color: {{$val['text']}};width:160px;height:50px;text-align:center;font-style:bold;">{{$val['service-name-1']}}</h4>
                                    @endif
                                   </div> 
                           </div>
                        </td> 
                         <td style="height:150px;width:150px" align="center">
                            
                             <div style="padding:5px;">
                                   <div class="servicediv" style="background:{{$val['blog_color']}};">
                                      @if($val['serviceimage2']!==null)
                                       <img src="{{$val['serviceimage2']}}" alt="ser-1" class="serviceimg"> <h4 style="font-size: 13px;text-align: center;color: {{$val['text']}};width:160px;height:50px;text-align:center;font-style:bold;">{{$val['service-name-2']}}</h4>
                                        @else
                                          <img src="{{url('web_assets/design-image/design-6/ser-2.png')}}" alt="ser-1" class="serviceimg"> <h4 style="font-size: 13px;text-align: center;color: {{$val['text']}};width:160px;height:50px;text-align:center;font-style:bold;">{{$val['service-name-2']}}</h4>
                                     @endif
                                   </div> 
                           </div>
                             
                        </td>
                     </tr>
                      
                       <tr>
                         <td style="height:150px;width:150px;" align="center">
                             <div style="padding:5px;">
                                   <div class="servicediv" style="background:{{$val['blog_color']}};">
                                           @if($val['serviceimage3']!==null)
                                            <img src="{{$val['serviceimage3']}}" alt="ser-1" class="serviceimg"> <h4 style="font-size: 13px;text-align: center;color: {{$val['text']}};width:160px;height:50px;text-align:center;font-style:bold;">{{$val['service-name-3']}}</h4>
                                           @else
                                            <img src="{{url('web_assets/design-image/design-6/ser-3.png')}}" alt="ser-1"  class="serviceimg"> <h4 style="font-size: 13px;text-align: center;color: {{$val['text']}};width:160px;height:50px;text-align:center;font-style:bold;">{{$val['service-name-3']}}</h4>
                                           @endif
                                      
                                   </div> 
                           </div>
                           </td>
                         <td style="height:150px;width:150px;" align="center">
                             <div style="padding:5px;">
                                   <div class="servicediv" style="background:{{$val['blog_color']}};">
                                       @if($val['serviceimage4']!==null)
                                        <img src="{{$val['serviceimage4']}}" alt="ser-1"class="serviceimg"> <h4 style="font-size: 13px;text-align: center;color: {{$val['text']}};width:160px;height:50px;text-align:center;font-style:bold;">{{$val['service-name-4']}}</h4>
                                           @else
                                        <img src="{{url('web_assets/design-image/design-6/ser-4.png')}}" alt="ser-1" class="serviceimg"> <h4 style="font-size: 13px;text-align: center;color: {{$val['text']}};width:160px;height:50px;text-align:center;font-style:bold;">{{$val['service-name-4']}}</h4>
                                           @endif
                                      
                                   </div> 
                           </div></td>
                     </tr>
                     <tr>
                         <td colspan="2" style="height:30px;">
                                
                         </td>
                     </tr>
                     
                     <tr>
                         <td colspan="2">      
                         <table style="width:100%;margin-top:30px;" >
                        <tr>
                          
                             @if(isset($val['social_media']))   
                             @foreach($val['social_media1'] as $key=>$media)
                             <td>
                                <a href="{{$media}}" style="font-size: 30px;width: 47px;height: 47px;line-height: 50px;text-align: center;text-decoration: none;border-radius: 155%;display: inline-block;background: {{$val['button_color']}};color: {{$val['text']}};;border: 2px solid {{$val['scondary']}};">
                                    <i class="fa {{$val['socialMediaList'][$key]}}" aria-hidden="true" style="margin-left:9px;margin-top:4px;font-size:35px;"></i>
                                </a>
                            </td>
                          @endforeach
                           @endif
                        </tr>
                    </table>
                     </td>
                     </tr>
                   </table>
                       
              </div>


                
           
                    
                   
                    
            
           
                       
      </section>           
           
        </body>
 

  

</html>
