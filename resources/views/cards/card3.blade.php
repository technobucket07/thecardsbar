<html>
        <!--Core CSS -->
      <link href="{{ url('bootstrap-4/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{ url('bootstrap-4/css/font-awesome.min.css')}}" rel="stylesheet">
    
     
     
     
        <style>

/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #3055c6; }
::selection { color: #f1f1f1;background: #3055c6; }



body  {font-family: 'Poppins', sans-serif;background: #120c2a; padding-bottom:-150px;}
.wrapper {width: 400px;margin: 0 auto;height:780px;background: #ecf2fb;}




.mainDiv {position: relative;overflow: hidden;background: black;}

.bgTopboxx {text-align: center;height: 278px;color: #fff;opacity: 0.9;}
.bodyBoxBg {text-align: center;height: 614px;color: #fff;opacity: 0.9;}

.innerbodyMain {position: absolute;top: 0;}

.innerbox {padding: 40px 0 0 0;text-align: center;}
.innerbox .userimg  {width: 110px;height: 110px;margin: 15px auto;border-radius: 58px;}
.innerbox .userimg img {width: 111px;height: 111px;margin-top:-0.5px;}

.innerbox .name  {color: #fff; font-size: 20px;
/*font-weight: 600;*/
margin: 0;
font-style:bold;
    
}
.innerbox .designation {color: #a49ea8; font-size:16px;
font-style:bold;
/*font-weight: 600;*/
    
}

.abtSec {text-align: right;padding: 0 30px;margin: 65px 0 45px 0;}
.abtSec h4 {color: #fff; font-size: 16px;font-weight: 700;text-transform: uppercase;}
.abtSec p {color: #a49ea8; font-size:14px;
/*font-weight: 500;*/
    
}

.socialInformation {margin-top:-30px;}
.socialInformation ul {margin: 0;}
.socialInformation ul:after {display: block;clear: both;content: "";}
.socialInformation ul li {margin: 0 0 30px 0;}
.socialInformation ul li:after {display: block;clear: both;content: "";}

/*.socialInformation ul li .innerBoxx {position: relative;display: inline-block;text-align: center;width:40%;}*/
/*.socialInformation ul li .innerBoxx:after {display: block;clear: both;content: "";}*/
/*.socialInformation ul li .innerBoxx .nameSocial {background: #5328fe;color: #fff;float: left;padding: 10px 20px;width: 120px;font-size: 16px;font-weight: 600;}*/
/*.socialInformation ul li .innerBoxx .nameSocial.pink, .socialInformation ul li .innerBoxx .iconSocial a.pink {background: #d100bb;}*/
/*.socialInformation ul li .innerBoxx .iconSocial {display: block;margin: 0 5px;padding: 2px;border-radius: 50px;position: absolute;right:-57px;}*/

/*.socialInformation ul li .innerBoxx .iconSocial a {font-size: 30px;width: 50px;height: 50px;text-align: center;text-decoration: none;border-radius: 27px;display: inline-block;background: #5328fe;color: #fff;border: 4px solid #181520;}*/
/*.socialInformation ul li .innerBoxx .iconSocial a .fa {font-size: 28px;  :-20px;}*/
.socialInformation ul li .innerBoxx {position: relative;display: inline-block;float: left; width:130px;height:50px;}
.socialInformation ul li .innerBoxx:after {display: block;clear: both;content: "";}
.socialInformation ul li .innerBoxx .nameSocial {background: #5328fe;color: #fff;float: left;padding: 10px 20px;width: 100px;font-size: 16px;height:25px;
/*font-weight: 600;*/ font-style:bolder;
text-align:center;}

.socialInformation ul li .innerBoxx .nameSocial.pink, .socialInformation ul li .innerBoxx .iconSocial a.pink {background: #d100bb;}*
.socialInformation ul li .innerBoxx .iconSocial {display: block;margin: 0 5px;padding: 1px;border-radius: 55px;position: absolute;right:unset;top: -10px;right: -57px;}
.socialInformation ul li .innerBoxx .iconSocial a {font-size: 30px;width: 49px;height: 49px;line-height: 50px;text-align: center;text-decoration: none;border-radius: 26.5px;display: inline-block;background: #5328fe;margin-top:8px;margin-left:15px;}
.socialInformation ul li .innerBoxx .iconSocial a .fa {font-size: 28px; margin-left:11px;margin-top:1px;}
.socialInformation ul li.rightSide {}

.socialInformation ul li.rightSide .innerBoxx {position: relative;display: inline-block;float: right; width:120px;height:50px;}
.socialInformation ul li.rightSide .innerBoxx:after {display: block;clear: both;content: "";}
.socialInformation ul li.rightSide .innerBoxx .nameSocial {background: #5328fe;color: #fff;float: left;padding: 10px 20px;width: 100px;font-size: 16px;height:25px;
/*font-weight: 600;*/
    
}
.socialInformation ul li.rightSide .innerBoxx .nameSocial.yellow, .socialInformation ul li.rightSide .innerBoxx .iconSocial a.yellow {background: #e18f00;}

.socialInformation ul li.rightSide .innerBoxx .nameSocial.orange, .socialInformation ul li.rightSide .innerBoxx .iconSocial a.orange {background: #ff3600;}
.socialInformation ul li.rightSide .innerBoxx .iconSocial {display: block;margin: 0 5px;padding: 1px;border-radius: 55px;position: absolute;right:unset;top: -10px;left: -55px;}
.socialInformation ul li.rightSide .innerBoxx .iconSocial a {font-size: 30px;width: 49px;height: 49px;line-height: 50px;text-align: center;text-decoration: none;border-radius: 26.5px;display: inline-block;background: #5328fe;margin-top:2px;}
.socialInformation ul li.rightSide .innerBoxx .iconSocial a .fa {font-size: 28px; margin-left:11px;margin-top:10px;}




.footerLastinfo {text-align: center; margin-top:-5px;}
.footerLastinfo h4{color: #a49ea8;font-size: 14px;
font-style:bolder;
/*font-weight: 500;*/
    
}








/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


}
     </style>
        <body>
 <section class="wrapper" >

{{--dd($val)--}}
            <div class="mainDiv">
                <div class="bgTopboxx">
                    @if($val['background_image1']!==null)
                     <img src="{{$val['background_image1']}}" style="width:100%;height:100%"/>
                      @else
                     <img src="{{url('web_assets/design-image/design-3/bgtop.jpg')}}" style="width:100%;height:100%"/>
                    @endif
                   
                </div>
                <div class="bodyBoxBg">
                    @if($val['background_image2']!==null)
                     <img src="{{$val['background_image2']}}" style="width:100%;height:100%"/>
                      @else
                     <img src="{{url('web_assets/design-image/design-3/bgbody.jpg')}}" style="width:100%;height:100%"/>
                    @endif
                </div>


                <div class="innerbodyMain">

                    <div class="innerbox">
                   
                            @if($val['profile_image']!==null)
                                 <p class="userimg" style="border: 3px solid {{($val['heading_color'])}};">
                                  <img  src="{{$val['profile_image']}}" alt="profilepix">
                                 </p>
                            @else
                              <p class="userimg" style="border: 3px solid {{($val['heading_color'])}};">
                                <img  src="{{url('web_assets/design-image/design-3/profilepix.jpg')}}" alt="profilepix" style="border-radius: 58px;">
                              </p>
                            @endif
                            
                           
                       
                        <p class="name" style="color:{{($val['heading_color'])}}">{{$val['first_name']}} {{$val['last_name']}}</p>
                        <p class="designation" style="color:{{($val['text'])}}">{{$val['designation']}}</p>
                    </div>

                    <div class="abtSec">
                        <h4 style="color:{{($val['heading_color'])}}">Address</h4>
                        <a href="{{$val['location']}}">
                          <p style="color:{{($val['text'])}}">{{($val['address'])}}</p>
                        </a>
                    </div>
                     @if(isset($val['social_media1']))   
                    <div class="socialInformation">
                        <ul class="list-unstyled">
                          @php $number =1;@endphp
                          @foreach($val['social_media1'] as $key=>$media)
                            @if ($number % 2 != 0)
                            <li >
                            <div class="innerBoxx" >
                                <div class="nameSocial yellow" style="color:{{($val['heading_color'])}};background:{{($val['mediacolor'][$number])}};">{{ucfirst(explode('-',$val['socialMediaList'][$key])[1])}}</div>
                                <div class="iconSocial">
                                    <a href="{{$media}}" class="yellow" style="background:{{($val['mediacolor'][$number])}};border: 3px solid black;">
                                        <i class="fa {{$val['socialMediaList'][$key]}}" aria-hidden="true" style="color:{{($val['heading_color'])}};margin-left:15px"></i>
                                    </a>
                                </div>
                            </div>
                          </li>
                          @else
                              <li class="rightSide">
                                  
                                 <div class="innerBoxx">
                                    <div class="nameSocial" style="color:{{($val['heading_color'])}};background:{{($val['mediacolor'][$number])}};">{{ucfirst(explode('-',$val['socialMediaList'][$key])[1])}}</div>
                                    <div class="iconSocial" >
                                        <a href="{{$media}}" style="background:{{($val['mediacolor'][$number])}};border: 3px solid black;">
                                           <i class="fa {{$val['socialMediaList'][$key]}}" aria-hidden="true" style="color:{{($val['heading_color'])}};"></i>

                                        </a>
                                    </div>
                                </div>
                            </li>
                          @endif
                           @php $number++;@endphp
                        @endforeach
                        </ul>
                    </div>
                     @endif

                    <div class="footerLastinfo">
                        <h4 style="color:{{($val['text'])}}">Phone Number</h4>
                        <p style="margin-top:-10px;">
                          {{--
                          <a href="{{$val['location']}}">
                           <i class="fa fa-map-marker" aria-hidden="true" style="color:{{($val['heading_color'])}};"></i>
                          </a>
                          --}}
                          <a href="tel:{{($val['country_code'])}}{{($val['phone'])}}" style="color:{{($val['heading_color'])}};width:120px;height:20px;margin-left:140px;"><b style="width:120px;height:20px">{{($val['country_code'])}}-{{($val['phone'])}}</b></a></p>
                    </div>

                </div>

            </div>





        </section>

    </body>
    
    </html>