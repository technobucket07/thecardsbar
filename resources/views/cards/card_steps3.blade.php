@extends('cards.layouts.master')
@section('css')
         <link href="{{ url('web_assets/css/form-wizard.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/css/dropify.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/bootstrap.min.css')}}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/font-awesome.min.css')}}" rel="stylesheet">
         <!--<link href="{{ url('web_assets/card-css/css/main.css')}}" rel="stylesheet">-->
         <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/demo/prism.css" />
         <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/bower_components/sweetalert/dist/sweetalert.css" />
	     <link rel="Stylesheet" type="text/css" href="{{ url('web_assets/js/style1.css?abc')}}" />
	     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	     <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
		 <script src="https://foliotek.github.io/Croppie/croppie.js"></script>
		 <script src="{{ url('web_assets/js/crop_js/script3.js')}}"></script>
       
    <style>
        .text-muted1 {
     color: red;
        }
        .progressbar li {
         
        }
        
        #loader { 
      border: 12px solid #f3f3f3; 
      border-radius: 50%; 
      border-top: 12px solid #444444; 
      width: 70px; 
      height: 70px; 
      animation: spin 1s linear infinite; 
    } 
    /*  #loader2 { */
    /*  border: 12px solid #f3f3f3; */
    /*  border-radius: 50%; */
    /*  border-top: 12px solid #444444; */
    /*  width: 70px; */
    /*  height: 70px; */
    /*  animation: spin 1s linear infinite; */
    /*} */
    
.preloader {
   position: absolute;
   top: 0;
   left: 0;
   width: 100%;
   height: 122%;
   z-index: 9999;
   background-image: url('../../IMAGE/default.gif');
   background-repeat: no-repeat; 
   background-color: #fff9;
   background-position: center;
}
    
    @keyframes spin { 
      100% { 
        transform: rotate(360deg); 
      } 
    } 
    
    .center { 
      position: absolute; 
      top: 0; 
      bottom: 0; 
      left: 0; 
      right: 0; 
      margin: auto; 
    } 
    
    
    .center1 { 
      position: absolute; 
      top: 0; 
      bottom: 0; 
      left: 0; 
      right: 0; 
      margin: auto; 
    } 
    

    </style>     
    
       
@section('content')
    <!-- BACK BUTTON STARTS HERE -->
  
 <div class="preloader"></div>
 
    <!--<section>-->
    <!--    <div class="container">-->
    <!--        <div class="row">-->
    <!--            <div class="col-md-12">-->
    <!--                <p class="backBtn">-->
    <!--                    <a class="btnCustomStyle2 btn-solid" href="index.html">-->
    <!--                        <span>Back</span>-->
    <!--                    </a>-->
    <!--                </p>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->
    <!-- BACK BUTTON END HERE -->

    <section class="upperCircles">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="progressbar list-unstyled" style="width: 100%;">
                        <li class="active" style=" width: 33%;"></li>
                        <li id="active1"  style=" width: 33%;"></li>
                        <li id="active2"  style=" width: 33%;"></li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- FORM WIZARD STARTS HERE -->
    <section class="mainFormWizard">
        <div class="container-fluid">
            <span id="theme_name" style="display: none;">sky</span>
            <span id="trans_name" style="display: none;">slide</span>
            <span id="rtl" style="display: none;">0</span>
            <div class="row">
                <div class="col-md-9">
    <ul>
            @foreach ($errors->all() as $error)
           
              <li> <div class="alert alert-warning">
               <strong>Warning!</strong> {{ $error }}
              </div></li>
            @endforeach
        </ul>
                    <form id="wizard_example_2"  method="post" enctype="multipart/form-data" >
                       <div id="loader1" style="position: relative;">
                          <!--<div id="loader2" class="center1" ></div> </br>-->
                          
                         <h2 style="text-align: center;font-size:14px;">Please Waite ...</h2>
                   </div>
                        @csrf
                        <fieldset>
                            <legend>Color</legend>
                            <div class="row">
                                
                                       
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Heading Color</label>
                                            <input type="color" class="form-control"  id="heading_color" name="heading_color" style="height:80px;width:200px;" value="#ffffff">
                                           
                                            
                                        </div>
                                    </div>
                                </div>
                                
                               
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Text Color</label>
                                             <input type="color" class="form-control"  id="text" name="text" style="height:80px;width:200px;" value="#a49ea8">
                                           
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>WhatsApp Color</label>
                                              <input type="color" class="form-control"  id="mediacolor1" name="mediacolor[]" style="height:80px;width:200px;"  value="#e18f00">
                                           
                                           
                                        </div>
                                       
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Facebook Color</label>
                                             <input type="color" class="form-control"  id="mediacolor2" name="mediacolor[]" style="height:80px;width:200px;" value="#5328fe">
                                          
                                        </div>
                                    </div>
                                </div>
                                
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Skype Color</label>
                                             <input type="color" class="form-control"  id="mediacolor3" name="mediacolor[]" style="height:80px;width:200px;"  value="#d100bb">
                                           
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Twitter Color</label>
                                             <input type="color" class="form-control"  id="mediacolor4" name="mediacolor[]" style="height:80px;width:200px;" value="#ff3600">
                                           
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Linkedin Color</label>
                                            <input type="color" class="form-control"  id="mediacolor5" name="mediacolor[]" style="height:80px;width:200px;" value="#33b5cc">
                                            
                                        </div>
                                    </div>
                                </div>     
                            </div>
                        </fieldset>
                      
                        <fieldset>
                            <legend>Social Links</legend>
                             @include('cards.socialmedia')
                            <!-- <div class="row">
                                <div class="col-md-12">
                                    <span class="col" style="color:red;font-size:16px;"></span>
                                    <ul class="list-unstyled socialLinksboxx">
                                        @foreach($socialMedia as $social)
                                            <li class="clearfix">
                                                 <span class="titleSocial" >
                                                     <i class="fa {{$social['icon']}}" aria-hidden="true"></i>{{$social['name']}}
                                                 </span>
                                                  @if($social['icon']=='fa-whatsapp')
                                              
                                                 <select class="form-control" name="code" style="width: 78px; height: 29px;float: left;margin-top: 0px;font-size: 12px;margin-left: 63px;" id="code">
                                                    @foreach($va as $va1)
                                                   <option value="{{$va1->callingCode}}">{{$va1->callingCode}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$va1->country}} </option>
                                                    @endforeach
                                                </select>
                                                 <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="7484564564">
                                                 </span>
                                                   @elseif($social['icon']=='fa-facebook')
                                                  <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.facebook.com">
                                                 </span>
                                                 
                                                 @elseif($social['icon']=='fa-skype')
                                                  <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.skype.com">
                                                 </span>
                                                 
                                                
                                                  
                                                   @elseif($social['icon']=='fa-linkedin')
                                                    <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.linkedin.com">
                                                 </span>
                                                    @elseif($social['icon']=='fa-twitter')
                                                   <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://twitter.com">
                                                 </span>
                                                   @endif
                                                
                                            </li>
                                             
                                        @endforeach
                                    </ul>
                                </div>
                            </div> -->
                        </fieldset>
                      <fieldset>
                            <legend>Profile </legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        Background Image                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                
                                                <div class="panel-body personalBoxxx">
                                                    <div class="row">
                                                        
                                                        
                                                         <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Background Image 1</label>
                                                                    <!--<input type="file" name="background_image1" id="input-file-now" class="dropify" />-->
                                                                    
                                                                     <div class="row">
                                                                      <div class="col-md-4">
                                                                        <input type="file" id="upload1"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="background_image1" id="base64image1">
                                                                      </div>
                                                                      <div class="col-md-8">
                                                                         <div class="upload-demo-wrap1">
                                                                         <div id="upload-demo1"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                         
                                                         <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Background Image 2</label>
                                                                    <!--<input type="file" name="background_image2" id="input-file-now1" class="dropify" />-->
                                                                    
                                                                    <div class="row">
                                                                      <div class="col-md-4">
                                                                        <input type="file" id="upload2"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="background_image2" id="base64image2">
                                                                      </div>
                                                                      <div class="col-md-8">
                                                                         <div class="upload-demo-wrap2">
                                                                         <div id="upload-demo2"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                          <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Profile Image</label>
                                                                    <!--<input type="file" name="profile_image" id="input-file-now2" class="dropify" />-->
                                                                    
                                                                      <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload3"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="profile_image" id="base64image3">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap3">
                                                                         <div id="upload-demo3"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
           
                       
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingfour">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                                        Personal Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                                <div class="panel-body businessInformation">
                                                    @include('cards.createfor')
                                                     <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" name="first_name" placeholder="First Name" value="Prince">
                                                                <small class="text-muted1 first_name"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="Charles">
                                                                <small class="text-muted1 last_name"></small>
                                                            </div>
                                                        </div>
                                                        
                                                         <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <textarea class="form-control" rows="3" id="about" name="about" placeholder="About me Description" maxlength="100" style="height:auto;">Lorem Ipsum is simply dummy text of the printing and typesetting.</textarea>
                                                                <small class="text-muted1 about"></small>
                                                            </div>
                                                        </div>
                                                        
                                    <div class="col-md-6">
                                        <div class="form-group ">
                                            <input type="text" class="form-control" placeholder="Designation" name="designation" maxlength="40" value="Photographer">
                                             <small class="text-muted1 designation"></small>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-6 ">
                                      <div class="form-group ">
                                        <input type="text" id="autocomplete" class="form-control" placeholder="Search your location" value="" onFocus="geolocate()">
                                         <small class="text-muted1 address"></small>
                                        <input type="hidden" name="location" id="latlong" value="">
                                      </div>
                                    </div>
                                    <div class="col-md-12 col-lg-6 ">
                                      <div class="form-group ">
                                        <input type="text" class="form-control" placeholder="Enter your Official Address" name="address" value="Grand Canyon National Park, Arizona, USA">
                                        <small class="text-muted1 address"></small>
                                      </div>
                                    </div>                
                                    <div class="col-md-6">
                                        <div class="form-group">    
                                        <select class="form-control" name="country_code" style="width: 60px; height: 50px;float: left;margin-top: 0px;font-size: 17px;padding:0px;" id="country_code">
                                            @foreach($va as $va1)
                                           <option value="{{$va1->callingCode}}">{{$va1->callingCode}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$va1->country}} </option>
                                            @endforeach
                                        </select>
                                            
                                            <input type="text" name="phone" class="form-control" placeholder="Contact Number" maxlength="10" style="width:230px;float: right;" value="1251248574">
                                                <small class="text-muted1 phone"></small>
                                        </div>
                                    </div>
                                                         
                                                        </div>
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                          </fieldset>
                            <div class="col-md-12">
                                <p class="backBtn">
                                    <a class="btnCustomStyle2 btn-solid Preview1 Preview2 Preview3" href="javascript:void(0);">
                                        <span>Preview</span>
                                    </a>
                                </p>
                            </div>
                          
                       </form>
                    </div>
                <div class="col-md-3">
                    
                    <div id="frame" style="background-image: url('{{ url("web_assets")}}/img/Phone1.png');background-color: #cccccc;background-position: center;background-repeat: no-repeat;background-size: cover;position: relative;width: 259px;height: 512px;border-radius: 30px;"></br>
   <div id="loader" class="center" ></div> 
                         <div id="previewImage" ></div>
                        
                      <div id="img" style="width: 226px;border-radius: 40px;margin-left: 17px;margin-top: -10px;">
                         <img class="img-fluid mx-auto d-block "  src="{{ url('web_assets')}}/img/design-3.png" alt="" style="border-radius: 22px;height: 489px;margin-top:0px;">
                    </div>
                  
                    </div>
                </div>
                
            </div>
    <style>
       

/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #3055c6; }
::selection { color: #f1f1f1;background: #3055c6; }



body  {font-family: 'Poppins', sans-serif;background: #120c2a;}
.wrapper {width: 400px;margin: 0 auto;height: 876px;background: black;}




.mainDiv {position: relative;overflow: hidden;}

.bgTopboxx {text-align: center;height: 278px;color: #fff; background-repeat: no-repeat;
  background-size: cover;}
.bodyBoxBg {text-align: center;height: 597px;color: #fff; background-repeat: no-repeat;
  background-size: cover;}

.innerbodyMain {position: absolute;top: 0;     width: 100%;}

.innerbox {padding: 40px 0 0 0;text-align: center;}
.innerbox .userimg  {width: 114px;height: 114px;margin: 15px auto;border-radius: 55px;    position: relative;}
.innerbox .userimg img {border-radius: 55px;width: 110px;height: 110px;overflow: visible;margin: -1px 0px 0px -1px;}

.innerbox .name  {color: #fff; font-size: 20px;font-weight: 600;margin: 0;}
.innerbox .designation {color: #a49ea8; font-size:16px;font-weight: 600;}

.abtSec {text-align: right;padding: 0 30px;margin: 65px 0 45px 0;}
.abtSec h4 {color: #fff; font-size: 16px;font-weight: 700;text-transform: uppercase;}
.abtSec p {color: #a49ea8; font-size:14px;font-weight: 500;}

.socialInformation {}
.socialInformation ul {margin: 0;}
.socialInformation ul:after {display: block;clear: both;content: "";}
.socialInformation ul li {margin: 0 0 30px 0;}
.socialInformation ul li:after {display: block;clear: both;content: "";}

.socialInformation ul li .innerBoxx {position: relative;display: inline-block;text-align: center;}
.socialInformation ul li .innerBoxx:after {display: block;clear: both;content: "";}
.socialInformation ul li .innerBoxx .nameSocial {background: #5328fe;color: #fff;float: left;padding: 10px 20px;width: 120px;font-size: 16px;font-weight: 600;}
.socialInformation ul li .innerBoxx .nameSocial.pink, .socialInformation ul li .innerBoxx .iconSocial a.pink {background: #d100bb;}
.socialInformation ul li .innerBoxx .iconSocial {display: block;margin: 0 5px;padding: 2px;border-radius: 50px;position: absolute;right: -57px;top: -10px;}
.socialInformation ul li .innerBoxx .iconSocial a {font-size: 30px;width: 60px;height: 60px;line-height: 50px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;background: #5328fe;color: #fff;border: 4px solid #181520;}
.socialInformation ul li .innerBoxx .iconSocial a .fa {font-size: 26px; margin-top: 13px;}


.socialInformation ul li.rightSide {}

.socialInformation ul li.rightSide .innerBoxx {position: relative;display: inline-block;float: right;}
.socialInformation ul li.rightSide .innerBoxx:after {display: block;clear: both;content: "";}
.socialInformation ul li.rightSide .innerBoxx .nameSocial {background: #5328fe;color: #fff;float: left;padding: 10px 20px;width: 140px;font-size: 16px;font-weight: 600;}
.socialInformation ul li.rightSide .innerBoxx .nameSocial.yellow, .socialInformation ul li.rightSide .innerBoxx .iconSocial a.yellow {background: #e18f00;}

.socialInformation ul li.rightSide .innerBoxx .nameSocial.orange, .socialInformation ul li.rightSide .innerBoxx .iconSocial a.orange {background: #ff3600;}
.socialInformation ul li.rightSide .innerBoxx .iconSocial {display: block;margin: 0 5px;padding: 2px;border-radius: 50px;position: absolute;right:unset;top: -10px;left: -55px;}
.socialInformation ul li.rightSide .innerBoxx .iconSocial a {font-size: 30px;width: 60px;height: 60px;line-height: 50px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;background: #5328fe;color: #fff;border: 4px solid #181520;}
.socialInformation ul li.rightSide .innerBoxx .iconSocial a .fa {font-size: 26px; margin-top: 13px;}




.footerLastinfo {text-align: center; margin-top: -8px;}
.footerLastinfo h4{color: #a49ea8;font-size: 14px;font-weight: 500;}
.footerLastinfo p{color: #fff;font-size: 16px;font-weight: 600;}







/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


}
    </style>
     
     <div style="opacity: 0;">        
<div id="html-content-holder" style="width: 400px;">
   
        
       <section class="wrapper" id="wrapper">


            <div class="mainDiv">
                <div class="bgTopboxx"></div>
                <div class="bodyBoxBg"></div>


                <div class="innerbodyMain">

                    <div class="innerbox">
                        <p class="userimg">
                            
                        </p>
                        <p class="name"></p>
                        <p class="designation"></p>
                    </div>

                    <div class="abtSec">
                        
                    </div>

                    <div class="socialInformation">
                        <ul class="list-unstyled list-unstyled1">
                            
                          
                           
                            
                        </ul>
                    </div>


                    <div class="footerLastinfo">
                       
                    </div>

                </div>

            </div>





        </section>
            
    </div>
</div> 
            
            
            
            
        </div>
        

        
<script>
demoUpload1();
demoUpload2();
demoUpload3();

</script>
        
        
    </section>
    <!-- FORM WIZARD END HERE -->
@endsection

@section('scripts')
<script src="{{ url('web_assets/js/html2canvas.js') }}"></script> 
    <script src="{{ url('web_assets/js/form-wizard.js') }}"></script>
    <script src="{{ url('web_assets/js/jscolor.js') }}"></script>
    <script src="{{ url('web_assets/js/dropify.js') }}"></script>
     
<style>
    canvas {
    height: 492px;
    width: 225px;
    margin-top: -13px;
    border-radius: 21px;
    margin-left: 17px;
    }
</style>
    <script>
        jscolor.presets.default = {
            borderColor:'rgba(236,239,245,0.6)', width:200, height:100,
            buttonColor:'rgba(70,51,255,1)', shadowColor:'rgba(0,0,0,0.05)'
        };
    </script>

    <script>
    
        $(document).ready(function(){
           
            	$('#myModal').hide();
            	 $('#h1').hide();
            	 $('#h2').hide();
            	 $('.searchMe').hide();
            	// $('.btnCustomStyle2').hide();
            	 $('#code').val('+91');
            	 $('#country_code').val('+91');
            	 $('#loader').hide(); 
                 $('#loader1').hide(); 
            	 $('#wrapper').hide();
            	 $('.information1').hide(); 
            	 $('.servicesBoxx1').hide();
            	 $('#p').hide();
                 $('.preloader').fadeOut('slow');
            view();
          
            // $('input[type="color"]').change(function(){
            //   $('.btnCustomStyle2').show();
            // });
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });
           
              
               
             $('.btnCustomStyle2').click(function(){
                 	 $('#wrapper').show();
                
                  	$('#loader').show();
                  	var form_data = new FormData($('#wizard_example_2')[0]);
              
   
                       $.ajax({
                            type: 'post',
                            url:  "{{route('admin.Cards.cards3')}}",
                            
                            data: form_data,
                            async: false,
                            dataType:'json',
                            success: function (data1) {
                            var    data=data1.va;
                                console.log(data);
                                if(data.background_image1!==null){
                                     $('.bgTopboxx').css('background-image',"url("+data.background_image1+")");
                                }else{
                                    var img1 ="{{url('/web_assets/design-image/design-3/bgtop.jpg')}}";
                                     $('.bgTopboxx').css('background-image','url('+img1+')');
                                }
                                if(data.background_image2!==null){
                                     $('.bodyBoxBg').css('background-image',"url("+data.background_image2+")");
                                }else{
                                    var img2 ="{{url('/web_assets/design-image/design-3/bgbody.jpg')}}";
                                    $('.bodyBoxBg').css('background-image','url('+img2+')');
                                }
                               
                                // $('.mainDiv').css('background-color',data.background_color);
                                if(data.address!==null){
                              $('.abtSec').html('<h4 style="color:'+data.heading_color+'">Address</h4><p style="color:'+data.text+'">'+data.address+'</p>');
                                } 
                                
                            if(data.first_name!==null && data.last_name!==null){
                                $('.name').text(data.first_name+' '+data.last_name);
                                $('.name').css('color',data.heading_color);
                            
                              
                                } 
                                
                            if(data.designation!==null){
                              $('.designation').text(data.designation);
                              $('.designation').css('color',data.text);
                             
                                } 
                            if(data.profile_image!==null){
                                $('.userimg').css('border','3px solid '+data.heading_color);
                                    $('.userimg').html('<img  src="'+data.profile_image+'" alt="profilepix">');
                                }else{
                                    $('.userimg').css('border','3px solid '+data.heading_color);
                                    var img3 ="{{url('/web_assets/design-image/design-3/profilepix.jpg')}}";
                                    $('.userimg').html('<img  src='+img3+' alt="profilepix">');
                                }
                                    
                                
                                
                           if(data.phone!==null){
                               $('.footerLastinfo').html('<h4 style="color:'+data.text+'">Phone Number</h4><p style="color:'+data.heading_color+'"> <a href="'+data.location+'"> <i class="fa fa-map-marker" aria-hidden="true"></i> </a> '+data.country_code+'-'+data.phone+'</p>');
                           }        
                                
                                var number =1;
                                var html='';
                                var color =0;
                                   for(var i in data.social_media){

                                       if(data.social_media[i]!==null){
                                        var media = data.socialMediaList[i];
                                        var media1 = media.split("-");
                                            if(number % 2 != 0) {
                                                var mediacolor= data.mediacolor;
                                            html+='<li><div class="innerBoxx"><div class="nameSocial" style="background-color:'+mediacolor[color]+';color:'+data.heading_color+';">'+media1[1]+'</div><div class="iconSocial"><a href="#" style="background-color:'+mediacolor[color]+';color:'+data.heading_color+';border: 4px solid black;"><i class="fa '+data.socialMediaList[i]+'" aria-hidden="true"></i></a></div></div></li>';
                                            }else{

                                                html+='<li class="rightSide"><div class="innerBoxx"><div class="nameSocial" style="background-color:'+mediacolor[color]+';color:'+data.heading_color+';">'+media1[1]+'</div><div class="iconSocial"><a href="#" style="background-color:'+mediacolor[color]+';color:'+data.heading_color+';border: 4px solid black;"><i class="fa '+data.socialMediaList[i]+'" aria-hidden="true"></i></a></div></div></li>';

                                            }
                                            number++;
                                            color++;
                                       }
                                        
                                        
                                       //  if(i==1){
                                       // html+='<li><div class="innerBoxx"><div class="nameSocial" style="background-color:'+data.mediacolor1+';color:'+data.heading_color+';">Facebook</div><div class="iconSocial"><a href="#" style="background-color:'+data.mediacolor1+';color:'+data.heading_color+';border: 4px solid black;"><i class="fa '+data.socialMediaList[i]+'" aria-hidden="true"></i></a></div></div></li>';
                                       //  }else if(i==2){
                                       //        html+='<li class="rightSide"><div class="innerBoxx"><div class="nameSocial" style="background-color:'+data.mediacolor2+';color:'+data.heading_color+';">Whats app</div><div class="iconSocial"><a href="#" style="background-color:'+data.mediacolor2+';color:'+data.heading_color+';border: 4px solid black;"><i class="fa '+data.socialMediaList[i]+'" aria-hidden="true"></i></a></div></div></li>';
                                       //  }else if(i==3){
                                       //        html+='<li><div class="innerBoxx"><div class="nameSocial" style="background-color:'+data.mediacolor3+';color:'+data.heading_color+';">Skype</div><div class="iconSocial"><a href="#" style="background-color:'+data.mediacolor3+';color:'+data.heading_color+';border: 4px solid black;"><i class="fa '+data.socialMediaList[i]+'" aria-hidden="true"></i></a></div></div></li>';
                                       //  }else if(i==4){
                                       //        html+='<li class="rightSide"><div class="innerBoxx"><div class="nameSocial" style="background-color:'+data.mediacolor4+';color:'+data.heading_color+';">Linked In</div><div class="iconSocial"><a href="#" style="background-color:'+data.mediacolor4+';color:'+data.heading_color+';border: 4px solid black;"><i class="fa '+data.socialMediaList[i]+'" aria-hidden="true"></i></a></div></div></li>';
                                       //  }else if(i==5){
                                       //        html+='<li><div class="innerBoxx"><div class="nameSocial" style="background-color:'+data.mediacolor5+';color:'+data.heading_color+';"> Twitter</div><div class="iconSocial"><a href="#" style="background-color:'+data.mediacolor5+';color:'+data.heading_color+';border: 4px solid black;"><i class="fa '+data.socialMediaList[i]+'" aria-hidden="true"></i></a></div></div></li>';
                                       //  }
                                   }
                               $('.list-unstyled1').html(html);
                             
                               	$('#img').hide(); 
                               		dview();
                               		setTimeout(function(){ $('#loader').hide();  }, 4000);
                               	 
                               	
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                   
      
         });
           
         $('button').click(function(){
             $('#myModal').hide(); 
         });   
            
            $('.finish-btn').click(function(){
                 $('form').attr('action', "{{route('admin.Cards.generate_pdf3')}}");
                 var pdfgen ="{{route('admin.Cards.generate_pdf3')}}";
                 $('form').attr('target','popup');
                 $('form').attr(' onclick',"window.open("+pdfgen+",'popup','width=800,height=800');");
            });
            
            function view(){            
                $.ajax({
                    url:"{{route('admin.Cards.delete-image')}}",
                    method:"get",
                    success: function(data) {
                        console.log(data);
                    }
                    
                });
            }
            
            
            
// global variable
 
    function dview() {
         var element = $("#html-content-holder"); // global variable
       var getCanvas; 

         html2canvas(element, {
              
         onrendered: function (canvas) {
                 
                $("#previewImage").html(canvas);
                getCanvas = canvas;
                
             }
         });
             
       //   $('#frame').show(); 
         $('#wrapper').hide(); 
        $('#img').hide(); 
       
    }
   // $('#frame').hide(); 

    
     $("#mediacolor1").change(function(){
       var color=$("#mediacolor1").val();
       
        $("#mediacolor1 option[value="+ color +"]").prop('disabled', false);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor3 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor5 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor4 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor2 option[value=" + color + "]").prop('disabled', true);
        $("#heading_color option[value=" + color + "]").prop('disabled', true);
    }) ;   
    
     $("#text").change(function(){
       var color=$("#text").val();
       
        $("#mediacolor1 option[value="+ color +"]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', false);
        $("#mediacolor3 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor5 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor4 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor2 option[value=" + color + "]").prop('disabled', true);
         $("#heading_color option[value=" + color + "]").prop('disabled', true);
    }) ;   
    
     $("#mediacolor1").change(function(){
       var color=$("#mediacolor1").val();
       
        $("#mediacolor1 option[value="+ color +"]").prop('disabled', false);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor3 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor5 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor4 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor2 option[value=" + color + "]").prop('disabled', true);
         $("#heading_color option[value=" + color + "]").prop('disabled', true);
    }) ;   
    
     $("#mediacolor3").change(function(){
       var color=$("#mediacolor3").val();
        $("#mediacolor1 option[value="+ color +"]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor3 option[value=" + color + "]").prop('disabled', false);
        $("#mediacolor5 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor4 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor2 option[value=" + color + "]").prop('disabled', true);
         $("#heading_color option[value=" + color + "]").prop('disabled', true);
    }) ;   
        
  
   $("#mediacolor5").change(function(){
       var color=$("#mediacolor5").val();
        $("#mediacolor1 option[value="+ color +"]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor3 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor5 option[value=" + color + "]").prop('disabled', false);
        $("#mediacolor4 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor2 option[value=" + color + "]").prop('disabled', true);
         $("#heading_color option[value=" + color + "]").prop('disabled', true);
    }) ;   
    
     $("#mediacolor4").change(function(){
       var color=$("#mediacolor4").val();
        $("#mediacolor1 option[value="+ color +"]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor3 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor5 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor4 option[value=" + color + "]").prop('disabled', false);
        $("#mediacolor2 option[value=" + color + "]").prop('disabled', true);
         $("#heading_color option[value=" + color + "]").prop('disabled', true);
    }) ;   
    
     $("#mediacolor2").change(function(){
       var color=$("#mediacolor2").val();
        $("#mediacolor1 option[value="+ color +"]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor3 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor5 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor4 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor2 option[value=" + color + "]").prop('disabled', false);
        $("#heading_color option[value=" + color + "]").prop('disabled', true);
    }) ;   
        
   $("#heading_color").change(function(){
       var color=$("#heading_color").val();
        $("#mediacolor1 option[value="+ color +"]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor3 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor5 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor4 option[value=" + color + "]").prop('disabled', true);
        $("#mediacolor2 option[value=" + color + "]").prop('disabled', true);
        $("#heading_color option[value=" + color + "]").prop('disabled', false);
    }) ;   
         ///$('.sf-controls').css("display","none");
        
        $('input[type="color"]').change(function(){
             var select1=$('#background_color').val();
             var select2=$('#mediacolor1').val();
             var select3=$('#text').val();
             var select4=$('#mediacolor3').val();
             var select5=$('#mediacolor5').val();
             var select6=$('#mediacolor4').val();
             var select7=$('#mediacolor2').val();
             var select8=$('#heading_color').val();
             if(select1!=='Choose Color' && select2!=='Choose Color' && select3!=='Choose Color' && select4!=='Choose Color' && select5!=='Choose Color' && select6!=='Choose Color' && select7!=='Choose Color' && select8!=='Choose Color'){
                 $('.sf-controls').css("display","block");
                   $('#active1').attr('class','active');
             }
            
        });
     
     $('.next-btn').click(function(){
              $('.sf-controls').css("display","none");
        });   
        
        
         $('.socialLinksboxx').on("focusout","input",function(){
             
           
             var input1=$('input[name="social_media[1]"]').val();
             var input2=$('input[name="social_media[2]"]').val();
             var input3=$('input[name="social_media[3]"]').val();
             var input4=$('input[name="social_media[4]"]').val();
             var input5=$('input[name="social_media[5]"]').val();
             
             if(input1!=='' && input2!=='' && input3!=='' && input4!=='' && input5!==''){
                   $('.sf-controls').css("display","block");
                     $('#active2').attr('class','active');
             }else{
                   $('.sf-controls').css("display","none");
                   $('.col').text('*Please fill all the boxes');
             }
            
        });
        
        
        
         $('.panel-group').on("focusout","input",function(){
            
             var first_name=$('input[name="first_name"]').val();
             var last_name=$('input[name="last_name"]').val();
             var designation=$('input[name="designation"]').val();
             var about=$('#about').val();
             var phone=$('input[name="phone"]').val();
           
          
             
                              if(first_name.length <31){
                                
                                  first_name1=first_name;
                              }else{
                                  $('.first_name').text('First Name max 30 Char');
                                    
                              }
                              
                               
                              
                              if(last_name.length <31){
                                 
                                  last_name1=last_name;
                              }else{
                                   $('.last_name').text('Last Name max 30 Char');
                                   
                              }
                              
                              
                             
                              
                              
                              if(designation.length <41){
                                 
                                  designation1=designation;
                              }else{
                                   $('.designation').text('designation max 40 Char');
                                   
                              }
                               if(about.length <100){
                                 
                                  about1=about;
                              }else{
                                   $('.about').text('About max 100 Char');
                                   
                              }
                              
                             
                            
                               
                              
                              if(phone!==''){
                              if(validatePhone(phone)==true){
                                 
                                  phone1=phone;
                                  $('.phone').css('color','green');
                                  $(".phone").text('*Phone Valid..!');
                              }else{
                                   //alert('Is not Valid..!');
                                     $('.phone').css('color','red');
                                   $('.phone').text('*Is not Valid Phone No... only no!');
                              }
                              }
                              
                              
                             
                             
             if(first_name1!=='' && last_name1!=='' && designation1!=='' && phone1!=='' && about1!==''){
                
                 $('.sf-controls').css("display","block");
             }else{
                  $('.sf-controls').css("display","none");
             }
            
        });
        
        
        function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
          if (regex.test(email)) {
                 return true;
               }
            else {
                  return false;
             }
        }
        
        
     function validatePhone(Phone) {
    
    var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (filter.test(Phone)) {
        return true;
    }
    else {
        return false;
    }
}
                    
});
    </script>
    
   
    
    
@endsection