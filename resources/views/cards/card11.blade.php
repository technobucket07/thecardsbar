<html>
        <!--Core CSS -->
      <link href="{{ url('bootstrap-4/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{ url('bootstrap-4/css/font-awesome.min.css')}}" rel="stylesheet">
    
     
     
     
        <style>
        
/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #eb9d3f; }
::selection { color: #f1f1f1;background: #eb9d3f; }



body  {font-family: 'Poppins', sans-serif;background: #eb9d3f;}
.wrapper {width: 400px;margin: 0 auto;background: #fff;}


.mainBoxx {
    background: #352210;
    overflow: hidden;
    height:930px;
    
}

.topContact {    
    overflow: hidden;
    margin: 25px 0;
    
}
.topContact ul {  margin: 0; }
.topContact ul:after {
    display: block;
    clear: both;
    content: "";
}
.topContact ul .leftSide {float: left;   width:auto; }
.topContact ul .leftSide a {
    display: block;
    text-decoration: none; 
    border-radius: 0px 10px 10px 0px;
    font-size: 14px;
    /*font-weight: 600;  */
    font-style:bold;
    background: #fff;
    color: #000333;
    padding:-2px 0px 7px 4px;
    width:100%;
    float:left;
    text-align: left;
    
    
}
.topContact ul .leftSide a i {    
    font-size: 16px;
    background: #ee9f41;
    color: #fff;
    width: 24px;
    height: 24px;
    padding:2px;
    line-height: 21px;
    border-radius: 14px;
    /*float:left;*/
    text-align: center;
    margin: 8px 0 -2px 5px;
     
}

.topContact ul .rightSide {float: right;  width:auto; }
.topContact ul .rightSide a { 
    display: block;
    text-decoration: none;    
    border-radius: 10px 0 0 10px;
    font-size: 14px;
    /*font-weight: 600; */
    font-style:bold;
    background: #fff;
       width:100%;
      
    color: #000333;
    padding:7px 0px 10px 0px;
    /*text-align: right;*/
   
   
   
}
.topContact ul .rightSide a i { 
    font-size: 16px;
    background: #ee9f41;
    color: #fff;
    width: 24px;
    height: 24px;
     float:left;
     padding:2px;
   
    line-height: 21px;
    border-radius: 14px;
    text-align: center;
    margin: -2px 0 0 5px;
    /*margin: -2px 5px 0 0 ;*/
    
}


.logoBoxx {}
.logoBoxx .userImg { background: #fff;
    /*padding: 10px;*/
    display: table;
    margin: 0 auto;
    border-radius: 10px;}
.logoBoxx .userImg img {
    margin-left:10px;
    border-radius:10px;
    }

.middlwBoxx {   
    background: #fff;
    border-radius: 40px;
    overflow: hidden;
    margin:30px 0 0 0;
    padding: 30px;
    margin-top:10px;
}

.infoSection {     overflow: hidden;
    margin: 0 0 15px 0;
    border-bottom: 1px solid #e8ecee;
    padding: 0 0 20px 0;}
.infoSection h2 {color: #1e1c26;text-transform: uppercase;font-size: 14px;
/*font-weight: 700;*/
 font-style:bold;
margin: 0 0 15px 0;}
.infoSection ul {margin: 0;}
.infoSection ul li {margin: 0 0 15px 0;}
.infoSection ul li:last-child {
    margin: 0;
}
.infoSection ul li:after {
    display: block;
    clear: both;
    content: "";
}

.infoSection ul li .contactIcon {    float: left;
       margin: 0 10px 0 0;
    width: 25px;
    text-align: center;}
.infoSection ul li .contactIcon i {font-size: 28px;line-height: 21px;    color: #eb9d3f;}
.infoSection ul li .contactIcon i.messageIcon {    font-size: 20px;
    line-height: 21px;}
.infoSection ul li .contactInfo {float: left;width: 81%;}
.infoSection ul li .contactInfo p {margin: 0;color: #000333;}
.infoSection ul li .contactInfo a {display: block;color: #000333;font-size: 16px;text-decoration: none; margin-top:-10px;}




.serviceOffers {}
.serviceOffers h4 {  text-transform: uppercase;  color: #000333;
    font-size: 16px;
    text-align: center;
    /*font-weight: 600;*/
     font-style:bold;
    margin: 0 0 15px 0;}
.serviceOffers ul {}
.serviceOffers ul:after {
    display: block;
    clear: both;
    content: "";
}
.serviceOffers table tr th {    float: left;
width: 170px;
border: 1px solid #e8ecee;}

.serviceOffers table tr th.bordeBottomRight { 
    border-right: 0;}
.serviceOffers table tr th .bordeBottomRight.borderDown {border-bottom: 1px solid #e8ecee !important;}
.serviceOffers table tr th .bordeBottom {border-bottom: 0;}





.serviceOffers table tr th .innerserviceBoxx {text-align: center;    padding: 10px 0;margin-top:20px;}
.serviceOffers table tr th .innerserviceBoxx .iconImg {    display: table;
    margin: 0 auto;}
.serviceOffers table tr th .innerserviceBoxx .iconImg img{ height:50px;  width:60px;}
.serviceOffers table tr th .innerserviceBoxx .title {    margin: 0;
    font-size: 14px;
    /*font-weight: 600;*/
     font-style:bold;
    color: #ee9f40; }
.serviceOffers table tr th .innerserviceBoxx .tagline {
    margin: 0; 
width:100%; 
/*padding-left:30px; */
/*padding-right:30px;*/
    
}
.serviceOffers table tr th .innerserviceBoxx .tagline small {       
    background: #e8ecee;
    padding: 3px 10px;
    border-radius: 10px;
    display: inline-block;
    font-size: 11px;
  
    /*font-weight: 600;*/
     font-style:bold;
   
    text-align:center;
    color: #000333;
    
}



.bottomIcons {padding: 0 40px;
    margin: 60px 0 0 0;margin: 20px 0 0 0;}
.bottomIcons ul{margin: 0;}
.bottomIcons ul li.phone {display: inline-block;width: 15%;   float: left;}
.bottomIcons ul li.phone a {font-size: 30px;width: 60px;height: 60px;line-height: 65px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;    background: #ee9f40;color: #fff;}
.bottomIcons ul li.phone a .fa {font-size: 32px;  margin-left:15px; margin-top:14px;}
.bottomIcons ul li.phone a .fa:hover {opacity: 0.7;}

.bottomIcons ul li.middelLine {    float: left;     margin: 7px 0 0 0;   width: 70%;}
.bottomIcons ul li.middelLine .moreInfo {     color: #fff2e3;
    font-size: 12px;
    text-align: center;
    margin: 0;}
.bottomIcons ul li.middelLine .linkweb { text-align: center;margin: 0;  margin-left:8px;}
.bottomIcons ul li.middelLine .linkweb a {     text-decoration: none;
    color: #fff2e3;
    font-size: 14px;
   
    text-align: center;}

.bottomIcons ul{margin: 0;}
.bottomIcons ul:after {display: block;clear: both;content: "";}
.bottomIcons ul li.whatsApp {display: inline-block;width: 15%;      text-align: right; float: right;}
.bottomIcons ul li.whatsApp a {  font-size: 30px;width: 60px;height: 60px;line-height: 60px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;    background: #ee9f40;color: #fff;}
.bottomIcons ul li.whatsApp a .fa {font-size: 32px; margin-left:15px; margin-top:14px;}
.bottomIcons ul li.whatsApp a .fa:hover {opacity: 0.7;}













/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


}

        </style>
      
        <body style="padding-bottom:-150px;">
            <section class="wrapper">

            <div class="mainBoxx" style="background:{{$val['primary']}};">
                <div class="topContact">
                    <ul class="list-unstyled">
                        <li class="leftSide">
                            <a href="tel:{{$val['code']}}{{$val['phone1']}}" style="background:{{$val['scondary']}};color:{{$val['text']}};">{{$val['first_name1']}} {{$val['last_name1']}} <i class="fa fa-phone" aria-hidden="true" style="color:{{$val['scondary']}};background-color:{{$val['blog_color']}};"></i>&nbsp;</a>
                        </li>

                        <li class="rightSide" >
                            <a href="tel:{{$val['code']}}{{$val['phone2']}}" style="background:{{$val['scondary']}};color:{{$val['text']}};"><i class="fa fa-phone" aria-hidden="true" style="color:{{$val['scondary']}};background-color:{{$val['blog_color']}};"></i> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$val['first_name2']}} {{$val['last_name2']}} </a>
                        </li>
                    </ul>
                </div>

                <div class="logoBoxx" align="center">
                    <p class="userImg"  >
                        @if($val['logo'])
                          <img src="{{$val['logo']}}" alt="userimg" class="img-fluid d-block" style="height:80px;width:auto;">
                           @else
                          <img src="{{url('web_assets/design-image/design-11/userimg.png')}}" alt="userimg" class="img-fluid d-block" style="height:80px;width:auto;">
                        @endif
                       
                    </p>
                </div>


                <div class="middlwBoxx" style="background:{{$val['scondary']}};">


                    <div class="infoSection" style="border-bottom:1px solid {{$val['button_color']}};">
                        <ul class="list-unstyled" style="margin-top:5px;">
                            <li>
                                <p class="contactIcon">
                                    <i class="fa fa-mobile" aria-hidden="true" style="color:{{$val['blog_color']}};"></i>
                                </p>
                                <div class="contactInfo">
                                   <a href="tel:{{$val['code']}}{{$val['c_phone']}}" style="color:{{$val['text']}}">{{$val['code']}}-{{$val['c_phone']}}</a>
                                   <a href="{{$val['info']}}" style="float: right; margin-top: -20px;">
                                    <p><i class="fa fa-info-circle" aria-hidden="true"></i></p>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <p class="contactIcon">
                                    <i class="fa fa-envelope messageIcon" aria-hidden="true" style="color:{{$val['blog_color']}};font-size:16px;margin-top:-5px;"></i>
                                </p>
                                <div class="contactInfo">
                                    <a href="mailto:{{$val['c_email']}}" style="color:{{$val['text']}}">{{$val['c_email']}}</a>
                                </div>
                            </li>
                            <li>
                                <p class="contactIcon">
                                    <i class="fa fa-map-marker" aria-hidden="true" style="color:{{$val['blog_color']}};"></i>
                                </p>
                                <div class="contactInfo">
                                    <a href="{{$val['location']}}">
                                        <p style="margin-top:-7px;color:{{$val['text']}}">{{$val['address']}}</p>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <div class="serviceOffers" >
                        <h4 style="color:{{$val['text']}}">Service Offers</h4>
                       
                        <table align="center">
                            <tr>
                            <th class="bordeBottomRight" style="border:1px solid {{$val['button_color']}};" >
                                <div class="innerserviceBoxx" >
                                    <p class="iconImg">
                                        @if($val['serviceimage1']!==null)
                                        <img src="{{$val['serviceimage1']}}" alt="ser-1" class="">
                                        @else
                                        <img src="{{url('web_assets/design-image/design-11/ser-1.jpg')}}" alt="ser-1" class="">
                                        @endif
                                        
                                    </p>
                                    <p class="title" style="color:{{$val['blog_color']}};">{{$val['servicename1']}}</p>
                                    <table align="center" style="background:{{$val['button_color']}};color:{{$val['text']}};text-align:center;font-weight:bold;border-radius:10px;font-size: 11px;height:15px;" >
                                       
                                        <tr>
                                          <td style="padding:5px;">{{$val['product1']}}</td>
                                        </tr>
                                    </table>
                                    
                                </div>
                            </th>
                            <th class="bordeBottom" style="border:1px solid {{$val['button_color']}};" >
                                <div class="innerserviceBoxx">
                                    <p class="iconImg">
                                        @if($val['serviceimage2']!==null)
                                          <img src="{{$val['serviceimage2']}}" alt="ser-1" class="">
                                          @else
                                          <img src="{{url('web_assets/design-image/design-11/ser-2.jpg')}}" alt="ser-1" class="">
                                        @endif
                                    </p>
                                    <p class="title" style="color:{{$val['blog_color']}};">{{$val['servicename2']}}</p>
                                   <table align="center" style="background:{{$val['button_color']}};color:{{$val['text']}};text-align:center;font-weight:bold;border-radius:10px;font-size: 11px;height:15px;" >
                                        <tr>
                                          <td style="padding:5px;">{{$val['product2']}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </th>
                            </tr>
                            <tr>
                            <th class="bordeBottomRight" style="border:1px solid {{$val['button_color']}};">
                                <div class="innerserviceBoxx">
                                    <p class="iconImg">
                                       @if($val['serviceimage3']!==null)
                                          <img src="{{$val['serviceimage3']}}" alt="ser-1" class="">
                                          @else
                                          <img src="{{url('web_assets/design-image/design-11/ser-3.jpg')}}" alt="ser-1" class="">
                                        @endif
                                    </p>
                                    <p class="title" style="color:{{$val['blog_color']}};">{{$val['servicename3']}}</p>
                                    <table align="center" style="background:{{$val['button_color']}};color:{{$val['text']}};text-align:center;font-weight:bold;border-radius:10px;font-size: 11px;height:15px;" >
                                        <tr>
                                          <td style="padding:5px;">{{$val['product3']}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </th>
                            <th style="border:1px solid {{$val['button_color']}};">
                                <div class="innerserviceBoxx" >
                                    <p class="iconImg">
                                        @if($val['serviceimage4']!==null)
                                          <img src="{{$val['serviceimage4']}}" alt="ser-1" class="">
                                          @else
                                          <img src="{{url('web_assets/design-image/design-11/ser-4.jpg')}}" alt="ser-1" class="">
                                        @endif
                                    </p>
                                    <p class="title" style="color:{{$val['blog_color']}};">{{$val['servicename4']}}</p>
                                     <table align="center" style="background:{{$val['button_color']}};color:{{$val['text']}};text-align:center;font-weight:bold;border-radius:10px;font-size: 11px;height:15px;" >
                                        <tr>
                                          <td style="padding:5px;">{{$val['product4']}}</td>
                                        </tr>
                                    </table>
                                </div>
                            </th>
                            </tr>
                        </table>
                        
                    </div>

                </div>

                <div class="bottomIcons">
                    <ul class="list-unstyled">
                        <li class="phone">
                            <a href="tel:{{$val['code']}}{{$val['c_phone']}}" style="color:{{$val['scondary']}};background-color:{{$val['blog_color']}};">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="middelLine">
                            <p class="moreInfo" style="color:{{$val['website_color']}};">for more information</p>
                            <p class="linkweb">
                                <a href="{{$val['website1']}}" style="color:{{$val['website_color']}};">{{$val['website3']}}</a>
                            </p>
                        </li>
                        <li class="whatsApp">
                             @if(isset($val['social_media']))   
                             @foreach($val['social_media1'] as $key=>$media)
                             @if($val['socialMediaList'][$key]=='fa-whatsapp')
                             @if($media!=='')
                            <a href="{{$media}}" style="color:{{$val['scondary']}};background:{{$val['blog_color']}};">
                                <i class="fa {{$val['socialMediaList'][$key]}}" aria-hidden="true"></i>
                            </a>
                           @endif
                           @endif
                           @endforeach
                           @endif
                        
                    </ul>
                </div>













            </div>

        </section>

            
    </body>
    
    </html>