<html>
        <!--Core CSS -->
      <link href="{{ url('bootstrap-4/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{ url('bootstrap-4/css/font-awesome.min.css')}}" rel="stylesheet">
    
     
     
     
        <style>
        

/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #3055c6; }
::selection { color: #f1f1f1;background: #3055c6; }



body  {font-family: 'Poppins', sans-serif;background: #ffbf06;}
.wrapper {width: 400px;margin: 0 auto;height: 890px;}

.profilePicBoxx {padding:30px;margin: 0 0 15px 0;border-radius: 0 0 30px 30px;}
.profilePicBoxx:after {display: block;clear: both;content: "";}
.profilePicBoxx .userImg {width: 115px;height: 115px;float: right;border-radius: 60px;border: 3px solid {{$val['blog_color']}};}
.profilePicBoxx .userImg img {border-radius: 55px;height:100%; width:100%;}
.profilePicBoxx .propNameDesignation {padding: 10px 125px 0 0;text-align: left;}
.profilePicBoxx .propNameDesignation h3 {font-size: 30px;margin: 0;font-style:bold;color: {{$val['text']}};}
.profilePicBoxx .propNameDesignation h4 {font-size: 30px;margin: 0;font-style:bold; color: {{$val['text']}};}
.profilePicBoxx .propNameDesignation p {padding: 0 0 0 2px; color:#bf1c1d;font-size: 20px;}


.infoSection {padding: 0 30px;margin: 30px 0;}
.infoSection ul {}
.infoSection ul li {margin: 10px 0;}
.infoSection ul li .contactIcon {float: left;width: 35px;height: 36px;background: #fffd3e;text-align: center;border-radius: 50px;margin: 0 10px 0 0;}
.infoSection ul li .contactIcon i {font-size: 36px;}
.infoSection ul li .contactIcon i.messageIcon {font-size: 24px;line-height: 33px;}
.infoSection ul li .contactInfo {}
.infoSection ul li .contactInfo h6 {margin: 0 0 4px 0;font-size: 14px;font-style:bold;color: {{$val['blog_color']}};}
.infoSection ul li .contactInfo p {font-size: 16px;margin: 0; color: {{$val['text']}};}
.infoSection ul li .contactInfo p a {color: #1e1c26; color: {{$val['text']}};}



.services {padding: 0 20px;}
.services ul {}
.services ul:after {display: block;clear: both;content: "";}
.services ul .leftSide {float: left;width: 45%;padding: 0 10px 0 0;}
.services ul .leftSide .boxxService {}
.services ul .leftSide .boxxService li {background: #ffbd00;font-size: 16px;text-align: center;border-radius: 15px;padding: 5px 0;margin: 0 0 15px 0;}

.services ul .leftSide .boxxService li:last-child {margin: 0;}




.services ul .rightSide {float: right;width: 55%;}
.services ul .rightSide .wrokerimg {text-align: right;}
.services ul .rightSide .wrokerimg img {height: 280px;width: 96%; float:right;}





.bottomIcons {padding: 0 40px;margin: -25px 0 0 0;}
.bottomIcons ul{margin: 0;}
.bottomIcons ul li.phone {display: inline-block;width: 20%;   float: left;}
.bottomIcons ul li.phone a {font-size: 30px;width: 60px;height: 60px;line-height: 65px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;background: #bf1c1d;color: #fff;}
.bottomIcons ul li.phone a .fa {font-size: 32px; margin-left:17px;margin-top:-2px;}
.bottomIcons ul li.phone a .fa:hover {opacity: 0.7;}

.bottomIcons ul li.middelLine {    float: left; padding: 0 5px;    margin: 4px 0 0 0;   width: 52%;}
.bottomIcons ul li.middelLine h6 {  margin: 0;  font-size: 14px;text-align: right;font-style:bold;  }
.bottomIcons ul li.middelLine p {   line-height: 36px;   margin: 0; font-size: 30px;text-align: right;font-style:bold; }

.bottomIcons ul{margin: 0; margin-top:50px;}
.bottomIcons ul:after {display: block;clear: both;content: "";}
.bottomIcons ul li.whatsApp {display: inline-block;width: 20%;      text-align: right; float: right;}
.bottomIcons ul li.whatsApp a {  font-size: 30px;width: 60px;height: 60px;line-height: 60px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;    background: #bf1c1d;color: #fff;}
.bottomIcons ul li.whatsApp a .fa {font-size: 32px; margin-left:15px;}
.bottomIcons ul li.whatsApp a .fa:hover {opacity: 0.7;}










/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


}
        </style>
        <body style="padding-bottom:-150px;">
            <section class="wrapper" style="background:{{$val['primary']}}">

            <div class="profilePicBoxx" style="background:{{$val['scondary']}}">
                <div class="userImg">
                    @if($val['profile_image']!==null)
                     <img  src="{{$val['profile_image']}}" alt="profilepic">
                    @else
                     <img  src="{{url('web_assets/design-image/design-7/profilepic.jpg')}}" alt="profilepic">
                    @endif
                   
                </div>
                <div class="propNameDesignation">
                    <h3>{{$val['first_name']}}</h3>
                    <h4>{{$val['last_name']}}</h4>
                    <p style="color:{{$val['contact_color']}};">{{$val['designation']}}</p>
                </div>
            </div>

            <div class="infoSection">
                <ul class="list-unstyled">
                    <li>
                        <div class="contactInfo">
                            <p style="float: right;">
                                <a href="{{$val['info']}}"> <i class="fa fa-info-circle" aria-hidden="true"></i></a>
                            </p>
                            <h6>Address</h6>
                            <p><a href="{{$val['location']}}">{{$val['address']}}</a></p>

                        </div>
                    </li>
                    <li>
                        <div class="contactInfo">
                            <h6>Email</h6>
                            <p><a href="mailto:{{$val['email']}}">{{$val['email']}}</a></p>
                        </div>
                    </li>
                    <li>
                        <div class="contactInfo">
                            <h6>Contact</h6>
                            <p><a href="tel:{{$val['phone']}}">{{$val['phone']}}</a></p>
                        </div>
                    </li>
                </ul>
            </div>


            <div class="services">
                <ul class="list-unstyled">
                    <li class="leftSide">
                        <ul class="list-unstyled boxxService">
                            <li style="background:{{$val['button_color']}};color:{{$val['text']}};">{{$val['service-name-1']}}</li>
                            <li style="background:{{$val['button_color']}};color:{{$val['text']}}">{{$val['service-name-2']}}</li>
                            <li style="background:{{$val['button_color']}};color:{{$val['text']}}">{{$val['service-name-3']}}</li>
                            <li style="background:{{$val['button_color']}};color:{{$val['text']}}">{{$val['service-name-4']}} </li>
                            <li style="background:{{$val['button_color']}};color:{{$val['text']}}">{{$val['service-name-5']}}</li>
                            <li style="background:{{$val['button_color']}};color:{{$val['text']}}">{{$val['service-name-6']}}</li>
                        </ul>
                    </li>
                    <li class="rightSide">
                        <p class="wrokerimg">
                              @if($val['service_image']!==null)
                            <img  src="{{$val['service_image']}}" alt="workerpic">
                            @else
                            <img  src="{{url('web_assets/design-image/design-7/workerpic.jpg')}}" alt="workerpic">
                            @endif
                        </p>
                    </li>
                </ul>
            </div>

            <div class="bottomIcons">
                    <ul class="list-unstyled">
                        <li class="phone">
                            <a href="tel:{{$val['phone']}}" style="background:{{$val['contact_color']}};color:{{$val['primary']}};">
                                <i class="fa fa-phone" aria-hidden="true"></i>
                            </a>
                        </li>
                        <li class="middelLine">
                            <h6 style="color:{{$val['text']}};">contact to</h6>
                            <p style="font-size: 25px;color:{{$val['text']}};">{{$val['bottom_title']}}</p>
                        </li>
                          @if(isset($val['social_media']))   
                             @foreach($val['social_media1'] as $key=>$media)
                             @if($val['socialMediaList'][$key]=='fa-whatsapp')
                             @if($media!=='')
                        <li class="whatsApp">
                            <a href="{{$media}}" style="background:{{$val['contact_color']}};color:{{$val['primary']}};">
                                <i class="fa {{$val['socialMediaList'][$key]}}" aria-hidden="true"></i>
                            </a>
                        </li>
                          @endif
                          @endif
                          @endforeach
                           @endif
                    </ul>
                </div>

        
        </section>

    </body>
    
    </html>