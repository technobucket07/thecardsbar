<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Profile</title>
        <!--Core CSS -->
        <!--<link href="{{ url('bootstrap-4/css/bootstrap.min.css')}}" rel="stylesheet">-->
        <link href="{{ url('bootstrap-4/css/font-awesome.min.css')}}" rel="stylesheet">
        <link href="{{ url('bootstrap-4/css/main.css" rel="stylesheet')}}">
    </head>
    <style>
        
/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #f0394d; }
::selection { color: #f1f1f1;background: #f0394d; }




.wrapper {width: 422px;margin: 0 auto;height: 930px;border-radius:0px 0px 0px 0px;background:{{$val['scondary']}};}

.leftSide {    width: 75%;
    float: left;
    height: 100%;
    background: #070709;}
.leftSide .mainBgimg {}
.leftSide .mainBgimg img {width: 100%;}

.profilePicBoxx {padding: 0 15px;    margin: 15px 0;}
.profilePicBoxx:after {display: block;
    clear: both;
    content: "";}
.profilePicBoxx .userImg {    width: 75px;
    height: 75px;
    border: 4px solid #505050;
    float: right;
    border-radius: 100px;}
.profilePicBoxx .userImg img {border-radius: 100px;}
.profilePicBoxx .propNameDesignation {padding: 15px 90px 0 0;text-align: right;}
.profilePicBoxx .propNameDesignation h3 {color: #fff;font-size: 16px;}
.profilePicBoxx .propNameDesignation p {color: #797979;font-size: 14px;}


.abtSec {    padding: 0 15px;
    text-align: right;}
.abtSec h4 {    color: #ff3b3c;
    font-size: 16px;
    text-transform: uppercase;}
.abtSec p {    margin: 0;
    font-size: 13px;
    color: #797979;}

.galleryBoxx {text-align: right;padding: 0 15px;    margin: 15px 0;}
.galleryBoxx h4{color: #ff3b3c;
    font-size: 16px;
    text-transform: uppercase;}
.galleryBoxx ul {    margin: 0;}
.galleryBoxx ul li {margin: 0 4px;display: inline-block;border-radius: 8px;}
.galleryBoxx ul li:last-child {margin-right: 0;}
.galleryBoxx ul li img{border-radius: 8px;width: 100%;}

.contactBoxx {text-align: right;padding: 0 15px;}
.contactBoxx h4 {color: #ff3b3c;
    font-size: 16px;
    text-transform: uppercase;}
.contactBoxx a {color: #797979;}



.rightSide {    width: 25%;
    float: left;
    position: relative;
    height: 100%;}
.socialMediaIcons {text-align: center;margin: 75px 0 0 0;}
.socialMediaIcons h5 {    font-size: 15px;
    color: #797979;
    padding: 0 10px;}
.socialMediaIcons ul{    margin: 0 auto;
    display: table;}
.socialMediaIcons ul li {    display: inline-block;
    margin: 5px;
    background: #f7d807;
    border-radius: 50px;}
.socialMediaIcons ul li a {  
    font-size: 30px;
    width: 50px;
    height: 50px;
    line-height: 50px;
    text-align: center;
    text-decoration: none;
    border-radius: 50%;
    display: inline-block;
background: #151419;
    color: #c8c8c8;
    border: 2px solid #ff3b3c;
}
.socialMediaIcons ul li a .fa {
    font-size: 26px;
}

.socialMediaIcons ul li a .fa:hover {
    opacity: 0.7;
}

.webLink {  
    background-color: white;
    -ms-transform: rotate(-90deg);
    transform: rotate(-90deg);
    margin: 0;
    position: absolute;
    bottom: 140px;
    left: 0;
    right: 0;}












/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


}
    </style>

<body  style="font-family: 'Poppins', sans-serif;padding-bottom:-150px;">
	<section class="wrapper">

      <table width="100%" cellspacing="0" height="910px"  >
      	<tr>
      		<th>
               
                    <img src="{{ $val['image1']}}" alt="girlbg" class="img-fluid d-block" style="width: 320px;height:510px;margin-left:-1px;padding-right:-2px;padding-bottom:-20px;">
               
            </th> 
      		<th rowspan="1" style="width:150px;">
      		           @if(isset($val['social_media1']))   
                <div style="text-align: center;margin: 100px 0px 0 0;">
                        <h4 style="margin-left:-95px;color:{{$val['text']}};">Search <p>me @</p></h4>
                        <ul style="list-style: none;margin-left:0px;">
                             @foreach($val['social_media1'] as $key=>$media)
                             @if($val['socialMediaList'][$key]=='fa-facebook')
                            <li>
                                <a href="{{$media}}" style="font-size: 30px;width: 50px;height: 50px;line-height: 50px;text-align: center;margin-left:-20px;text-decoration: none;border-radius: 26px;display: inline-block;border: 2px solid {{$val['heading']}};background:{{$val['primary']}};">
                                    <i class="fa {{$val['socialMediaList'][$key]}}" aria-hidden="true" style="font-size: 31px;margin-top:10px;margin-left:16px;color:{{$val['text']}};" ></i>
                                </a>
                            </li>
                             @else
                              <li style="margin-top:12px;">
                                <a href="{{$media}}" style="font-size: 30px;width: 50px;height: 50px;line-height: 50px;text-align: center;text-decoration: none;margin-left:-20px;border-radius: 26px;display: inline-block;border: 2px solid {{$val['heading']}};background:{{$val['primary']}};">
                                    <i class="fa {{$val['socialMediaList'][$key]}}" aria-hidden="true" style="font-size: 31px;margin-top:9px;margin-left:12px;color:{{$val['text']}};" ></i>
                                </a>
                            </li>
                            @endif
                            @endforeach
                        </ul>
                    </div>
                    @endif
                  
      		</th>
      	</tr>
        <tr >
      		<th style="background:{{$val['primary']}};margin-left:-2px;" > 
      	    	<div class="profilePicBoxx" style="padding: 0px 10px ; margin: 10px -5px;">
                    <div style="border-radius:70px;height:100px;width:70px;float:right">
                        <img  src="{{ $val['image2']}}" alt="profilepix" style="width:70px;height:70px;border:2px solid {{$val['text']}};border-radius:36px;margin-right:10px;" >
                    </div>
                    <div class="propNameDesignation" style="padding: 0px 90px 0 0;margin-top:15px;">
                        <h3 style="color:{{$val['heading']}}">{{$val['first_name']}} {{$val['last_name']}}</h3>
                        <p style="color:{{$val['text']}};margin-top: -15px;font-size:12px;" >{{$val['designation']}}</p>
                    </div>
                </div>
                 <div class="abtSec" style="margin-top:10px;">
      		        @if($val['about']!=='')
                    <h4 style="color:{{$val['heading']}}">About Me</h4>
                    <p style=" color:{{$val['text']}};text-align: right;margin-top: -21px;">{{$val['about']}} </p>
                    @endif
                </div>

                </th>
                <th rowspan="2">
                    @if($val['website']) 
                    @if(substr($val['website1'],0,5)=='http:')
                    <div style="ms-transform: rotate(-90deg);transform: rotate(-90deg);">
               <a href="{{$val['website1']}}" style="margin-left:-100px;text-decoration: none;color:{{$val['text']}};cursor: pointer;height:390px;width:290px;" > <span style="width:290px;height:100px;"></br></br><span style="margin-left:100px;font-family: 'Poppins', sans-serif;">{{substr($val['website1'],7)}}</span></span></a>
                 </div>
                 @else
                <div style="ms-transform: rotate(-90deg);transform: rotate(-90deg);">
               <a href="{{$val['website1']}}" style="margin-left:-130px;text-decoration: none;color:{{$val['text']}};cursor: pointer;height:390px;width:290px;" > <span style="width:290px;height:100px;"></br></br><span style="margin-left:100px;font-family: 'Poppins', sans-serif;">{{substr($val['website1'],8)}}</span></span></a>
                 </div>
                 @endif
                    @endif
                    
                </th>
            </tr>

      	<tr>
      		<th style="background:{{$val['primary']}};margin-left:-2px;">
      		    
                <div class="galleryBoxx" style="margin-top:10px;">
                    @if($val['gallery1']!=='' || $val['gallery2']=='' ||  $val['gallery3']=='')
                    <h4 style="color:{{$val['heading']}}">Gallery</h4>
                    @endif
                @if($val['gallery1']!=='' || $val['gallery2']=='' ||  $val['gallery3']=='')
                    <ul class="list-unstyled" style="margin-top:0px;margin-left:-20px;">
                        <li><img class="img-fluid" src="{{ $val['gallery1']}}" alt="ser-1" style="width:80px;height:70px;"></li>
                        <li><img class="img-fluid" src="{{ $val['gallery2']}}" alt="ser-2" style="width:80px;height:70px;"></li>
                        <li><img class="img-fluid" src="{{ $val['gallery3']}}" alt="ser-3" style="width:80px;height:70px;"></li>
                    </ul>
                @endif
                </div>

                <div class="contactBoxx" style="text-align: right;padding: 0 15px;margin-top:-30px;">
                     @if($val['phone']!=='')
                      <h4 style="color:{{$val['heading']}};margin-bottom: 3px;">Contact Me</h4>
                    <a href="tel:{{$val['phone']}}" style=" color:{{$val['text']}};text-decoration: none;">{{$val['phone']}}</a>
                    @endif
                </div>
      		</th>
      		
      	
      		</tr>
      		<tr>
      		    <td style="background:{{$val['primary']}};height:60px;">
      		        
      		    </td>
      		</tr>
      


      </table>

	</section>





        <!-- jQuery -->
        <script src="{{ url('bootstrap-4/js/jquery.js')}}"></script>
        <!-- Bootstrap Core JavaScript -->
        <script src="{{ url('bootstrap-4/js/bootstrap.min.js')}}"></script>
        <script src="{{ url('bootstrap-4/js/proper.js')}}"></script>
        
   <script>
       function onclick_url(){
           window.location.href = 'http://www.google.com';
       }
   </script>


    </body>
</html>