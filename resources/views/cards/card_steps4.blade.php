@extends('cards.layouts.master')
@section('css')
         <link href="{{ url('web_assets/css/form-wizard.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/css/dropify.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/bootstrap.min.css')}}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/font-awesome.min.css')}}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/main.css')}}" rel="stylesheet">
         <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/demo/prism.css" />
         <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/bower_components/sweetalert/dist/sweetalert.css" />
	     <link rel="Stylesheet" type="text/css" href="{{ url('web_assets/js/style1.css?abc')}}" />
	     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	     <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
		 <script src="https://foliotek.github.io/Croppie/croppie.js"></script>
		 <script src="{{ url('web_assets/js/crop_js/script4.js')}}"></script>
       
    <style>
        .text-muted1 {
     color: red;
        }
        .progressbar li {
         
        }
        
        #loader { 
      border: 12px solid #f3f3f3; 
      border-radius: 50%; 
      border-top: 12px solid #444444; 
      width: 70px; 
      height: 70px; 
      animation: spin 1s linear infinite; 
    } 
    /*  #loader2 { */
    /*  border: 12px solid #f3f3f3; */
    /*  border-radius: 50%; */
    /*  border-top: 12px solid #444444; */
    /*  width: 70px; */
    /*  height: 70px; */
    /*  animation: spin 1s linear infinite; */
    /*} */
    
.preloader {
   position: absolute;
   top: 0;
   left: 0;
   width: 100%;
   height: 122%;
   z-index: 9999;
   background-image: url('../IMAGE/default.gif');
   background-repeat: no-repeat; 
   background-color: #fff9;
   background-position: center;
}
    
    @keyframes spin { 
      100% { 
        transform: rotate(360deg); 
      } 
    } 
    
    .center { 
      position: absolute; 
      top: 0; 
      bottom: 0; 
      left: 0; 
      right: 0; 
      margin: auto; 
    } 
    
    
    .center1 { 
      position: absolute; 
      top: 0; 
      bottom: 0; 
      left: 0; 
      right: 0; 
      margin: auto; 
    } 
    

    </style>     
    
       
@section('content')
    <!-- BACK BUTTON STARTS HERE -->
  
 <div class="preloader"></div>
 
    <!--<section>-->
    <!--    <div class="container">-->
    <!--        <div class="row">-->
    <!--            <div class="col-md-12">-->
    <!--                <p class="backBtn">-->
    <!--                    <a class="btnCustomStyle2 btn-solid" href="index.html">-->
    <!--                        <span>Back</span>-->
    <!--                    </a>-->
    <!--                </p>-->
    <!--            </div>-->
    <!--        </div>-->
    <!--    </div>-->
    <!--</section>-->
    <!-- BACK BUTTON END HERE -->

    <section class="upperCircles">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="progressbar list-unstyled" style="width: 100%;">
                        <li class="active" style=" width: 33%;"></li>
                        <li id="active1"  style=" width: 33%;"></li>
                        <li id="active2"  style=" width: 33%;"></li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- FORM WIZARD STARTS HERE -->
    <section class="mainFormWizard">
        <div class="container-fluid">
            <span id="theme_name" style="display: none;">sky</span>
            <span id="trans_name" style="display: none;">slide</span>
            <span id="rtl" style="display: none;">0</span>
            <div class="row">
                <div class="col-md-9">
    <ul>
            @foreach ($errors->all() as $error)
           
              <li> <div class="alert alert-warning">
               <strong>Warning!</strong> {{ $error }}
              </div></li>
            @endforeach
        </ul>
                    <form id="wizard_example_2"  method="post" enctype="multipart/form-data" >
                       <div id="loader1" style="position: relative;">
                          <!--<div id="loader2" class="center1" ></div> </br>-->
                          
                         <h2 style="text-align: center;font-size:14px;">Please Waite ...</h2>
                   </div>
                        @csrf
                        <fieldset>
                            <legend>Color</legend>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Primary Color</label>
                                               <input type="color" class="form-control"  id="Primary" name="primary" style="height:80px;width:200px;" value="#ecf2fb">
                                           
                                           
                                        </div>
                                       
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Secondary Color</label>
                                            <input type="color" class="form-control"  id="Secondary" name="scondary" style="height:80px;width:200px;" value="#ffffff">
                                         
                                          
                                        </div>
                                    </div>
                                </div>
                                
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Blog Color</label>
                                            <input type="color" class="form-control"  id="blog_color" name="blog_color" style="height:80px;width:200px;" value="#3055c6">
                                           
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Text Color</label>
                                            <input type="color" class="form-control"  id="text" name="text" style="height:80px;width:200px;" value="#000">
                                           
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Heading Color</label>
                                             <input type="color" class="form-control"  id="heading" name="heading" style="height:80px;width:200px;" value="#6e7179">
                                        </div>
                                    </div>
                                </div>
                                
                               
                            </div>
                        </fieldset>
                      
                        <fieldset>
                            <legend>Social Links</legend>
                            @include('cards.socialmedia')
                            <!-- <div class="row">
                                <div class="col-md-12">
                                    <span class="col" style="color:red;font-size:16px;"></span>
                                    <ul class="list-unstyled socialLinksboxx">
                                        @foreach($socialMedia as $social)
                                            <li class="clearfix">
                                                 <span class="titleSocial" >
                                                     <i class="fa {{$social['icon']}}" aria-hidden="true"></i>{{$social['name']}}
                                                 </span>
                                                  @if($social['icon']=='fa-whatsapp')
                                              
                                                 <select class="form-control" name="code" style="width: 78px; height: 29px;float: left;margin-top: 0px;font-size: 12px;margin-left: 63px;" id="code">
                                                    @foreach($va as $va1)
                                                   <option value="{{$va1->callingCode}}">{{$va1->callingCode}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$va1->country}} </option>
                                                    @endforeach
                                                </select>
                                                 <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="7484564564">
                                                 </span>
                                                   @elseif($social['icon']=='fa-facebook')
                                                  <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.facebook.com">
                                                 </span>
                                                 
                                                 @elseif($social['icon']=='fa-skype')
                                                  <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.skype.com">
                                                 </span>
                                                 
                                                
                                                  
                                                   @elseif($social['icon']=='fa-linkedin')
                                                    <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.linkedin.com">
                                                 </span>
                                                    @elseif($social['icon']=='fa-twitter')
                                                   <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://twitter.com">
                                                 </span>
                                                   @endif
                                                
                                            </li>
                                             
                                        @endforeach
                                    </ul>
                                </div>
                            </div> -->
                        </fieldset>
                      <fieldset>
                            <legend>Profile </legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        Company Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                
                                                <div class="panel-body personalBoxxx">
                                                    <div class="row">
                                                        
                                                        
                                                         <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Background Image</label>
                                                                    <!--<input type="file" name="background_image" id="input-file-now" class="dropify" />-->
                                                                    
                                                                     <div class="row">
                                                                      <div class="col-md-4">
                                                                        <input type="file" id="upload1"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="background_image" id="base64image1">
                                                                      </div>
                                                                      <div class="col-md-8">
                                                                         <div class="upload-demo-wrap1">
                                                                         <div id="upload-demo1"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                          <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Profile Image</label>
                                                                    <!--<input type="file" name="profile_image" id="input-file-now1" class="dropify" />-->
                                                                    
                                                                     <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload2"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="profile_image" id="base64image2">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap2">
                                                                         <div id="upload-demo2"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-12 col-lg-12">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" name="company_name" placeholder="Enter Company Name" value="GLOBAL COMPUTERS">
                                                                <small class="text-muted1 company_name"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" name="first_name" placeholder="First Name" value="Steve">
                                                                <small class="text-muted1 first_name"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="Smith">
                                                                <small class="text-muted1 last_name"></small>
                                                            </div>
                                                        </div>
                                                        
                                                       
                                                       
                                                        
                                                     
                                                        
                                                    </div>
                                                    @include('cards.createfor')
                                                </div>
                                            </div>
                                        </div>
           
                       
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingfour">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                                        Contact Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                                <div class="panel-body businessInformation">

                                                    <div class="row">
                                                         <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="email" class="form-control" placeholder="Email" name="email" maxlength="30" value="info@example.com">
                                                                <small class="text-muted1 email"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Enter What's App No." name="whats_no" maxlength="10" value="125124857">
                                                                 <small class="text-muted1 whats_no"></small>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" name="phone" class="form-control" placeholder="Contact Number" maxlength="10" value="1251248574">
                                                                    <small class="text-muted1 phone"></small>
                                                            </div>
                                                        </div>
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Website" name="website" maxlength="35" value="www.example.com">
                                                            </div>
                                                        </div>
                                                        
                                                       <div class="col-md-12 col-lg-12 ">
                                                            <div class="form-group">
                                                                <textarea type="text" class="form-control" placeholder="Enter your Official Address" id="address" name="address" rowspan="2" maxlength="40" >92-93, Lawrence Road, Amritsar, Punjab</textarea>
                                                                 <small class="text-muted1 address"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-12">
                                                        <div class="form-group ">
                                                          <input type="text" id="autocomplete" class="form-control" placeholder="Search your location" value="" onFocus="geolocate()">
                                                           <small class="text-muted1 address"></small>
                                                          <input type="hidden" name="location" id="latlong" value="">
                                                        </div>
                                                      </div>
                                                       
                                                       
                                                       
                                                          
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                             <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingfour">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#Services" aria-expanded="false" aria-controls="collapsefour">
                                                        Services Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="Services" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                                <div class="panel-body businessInformation">

                                                    <div class="row">
                                                         <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="email" class="form-control" placeholder="Enter Services 1" name="services1" maxlength="15" value="Desktops">
                                                                <small class="text-muted1 services1"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Enter Services 2" name="services2" maxlength="15" value="Laptops">
                                                                 <small class="text-muted1 services2"></small>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Enter Services 3" name="services3" maxlength="15" value="Network Devices">
                                                                    <small class="text-muted1 services3"></small>
                                                            </div>
                                                        </div>
                                                       

                                                       
                                                       
                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Enter Services 4" name="services4" maxlength="15" value="Peripherals">
                                                                 <small class="text-muted1 services3"></small>
                                                            </div>
                                                        </div>
                                                          
                                                           <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text"  class="form-control" placeholder="Enter Services 5" name="services5" maxlength="15" value="Software">
                                                                    <small class="text-muted1 services5"></small>
                                                            </div>
                                                        </div>
                                                       

                                                       
                                                       
                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Enter Services 6" name="services6" maxlength="15" value="Tablets">
                                                                 <small class="text-muted1 services6"></small>
                                                            </div>
                                                        </div>
                                                          
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                             
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                            <div class="col-md-12">
                                <p class="backBtn">
                                    <a class="btnCustomStyle2 btn-solid Preview1 Preview2" href="javascript:void(0);">
                                        <span>Preview</span>
                                    </a>
                                </p>
                            </div>
                          
                       </form>
                    </div>
                <div class="col-md-3">
                    
                    <div id="frame" style="background-image: url('{{ url("web_assets")}}/img/Phone1.png');background-color: #cccccc;background-position: center;background-repeat: no-repeat;background-size: cover;
  position: relative;width: 259px;height: 512px;border-radius: 30px;"></br>
   <div id="loader" class="center" ></div> 
                         <div id="previewImage" ></div>
                        
                      <div id="img" style="width: 235px;border-radius: 40px;margin-left: 15px;margin-top: -10px;">
                         <img class="img-fluid mx-auto d-block "  src="{{ url('web_assets')}}/img/desine-4.png" alt="" style="border-radius: 22px;height: 490px;margin-top: -10px;">
                    </div>
                  
                    </div>
                </div>
                
            </div>
    <style>
       
/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #3055c6; }
::selection { color: #f1f1f1;background: #3055c6; }



body  {font-family: 'Poppins', sans-serif;background: #2a53c1;}
.wrapper {width: 400px;margin: 0 auto;height: 800px;background: #ecf2fb;}


.profileboxx {text-align: center;height: 315px;color: #fff; background-repeat: no-repeat; background-size: cover; background-position: center;}
.profileboxx .innerbox {padding: 40px 0 0 0;}
.profileboxx .innerbox .companyName {    text-transform: uppercase;
    font-size: 24px;
    font-weight: 600;
    letter-spacing: 1px;}
.profileboxx .innerbox .userimg  {     border: 3px solid #fff;
    width: 114px;
    height: 114px;
    margin: 15px auto;
    border-radius: 100px;}
.profileboxx .innerbox .userimg img {    border-radius: 100px;
    width: 100%;}

.profileboxx .innerbox .name  {    font-size: 20px;
    font-weight: 600;}



.information {margin: -40px 0 0 0;}
.information ul {display: table;margin: 0 auto;background: #fff;width: 88%;border-radius: 10px;padding: 30px;}
.information ul li {margin: 0 0 10px 0;}
.information ul li:last-child {margin: 0;}
.information ul li:after {display: block;clear: both;content: "";}
.information ul li .ledtname{float: left;width: 30%;    margin: 0;    font-size: 14px;color: #6e7179;}
.information ul li .rightname{float: left;width: 70%;margin: 0;font-size: 13px;color: #000;font-weight: 600;margin-left: 0%;}


.servicesBoxx {margin: 15px 0 15px 0;}
.servicesBoxx h4{    width: 80%;
    display: table;
    margin: 0 auto 15px;
    font-size: 14px;
    color: #6e7179;}
.servicesBoxx ul {    display: table;
    margin: 0 auto;
    /* background: #fff; */
    width: 80%;
    border-radius: 10px;
    /* padding: 30px; */
    text-align: center;}
.servicesBoxx ul li {margin: 0 0 10px 0;}
.servicesBoxx ul li:last-child {margin: 0;}
.servicesBoxx ul li:after {display: block;clear: both;content: "";}
.servicesBoxx ul li .ledtname{    float: left;
    width: 47%;
    margin: 0;
    font-size: 14px;
    color: #fff;
    background:transparent;
    border-radius: 50px;
    padding: 10px 0;}
.servicesBoxx ul li .rightname{    float: right;
    width: 47%;
    margin: 0;
    font-size: 14px;
    color: #fff;
    /* margin-left: 10%; */
     background:transparent;
    padding: 10px 0;
    border-radius: 50px;}


.socialMediaIcons {    display: table;
    margin:20px auto;
    /* background: #fff; */
    width: 85%;}
.socialMediaIcons ul{margin: 0;
    text-align:center;
}
.socialMediaIcons ul li {display: inline-block;margin: 0 7px;}
.socialMediaIcons ul li a {  
 
    font-size: 30px;
    width: 50px;
    height: 50px;
    line-height: 50px;
    text-align: center;
    text-decoration: none;
    border-radius: 50%;
    display: inline-block;
    background: #ecf2fb;
    color: #2d6ff7;

}
.socialMediaIcons ul li a .fa {
    font-size: 26px;
        margin-top: 12px;
}

.socialMediaIcons ul li a .fa:hover {
    opacity: 0.7;
}

.address {color: #2b53c2;font-size: 13px;text-align: center; }

/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


}
    </style>
     
     <div >        
<div id="html-content-holder" style="width: 400px;">
   
        
        <section class="wrapper" id="wrapper" >

            <div class="profileboxx">
                <div class="innerbox">
                    <h2 class="companyName"></h2>
                    <p id="p" style="width: 114px;height: 106px;margin: 15px auto;border-radius: 55px;">
                        
                    </p>
                    <p class="name"></p>
                </div>
            </div>

            <div class="information information1">
                <ul class="list-unstyled list-unstyled2">
                    <li>
                        <p class="ledtname ledtname1"></p>
                        <p class="rightname rightname1"></p>
                    </li>
                    <li>
                        <p class="ledtname ledtname2"></p>
                        <p class="rightname rightname2"></p>
                    </li>
                    <li>
                        <p class="ledtname ledtname3"></p>
                        <p class="rightname rightname3"></p>
                    </li>
                    <li>
                        <p class="ledtname ledtname4"></p>
                        <p class="rightname rightname4"></p>
                    </li>
                </ul>
            </div>


            <div class="servicesBoxx servicesBoxx1">
                <h4 id="h">Services</h4>
                <ul class="list-unstyled">
                    <li>
                        <p class="ledtname ledtname5" style="border-radius: 20px;">Desktops</p>
                        <p class="rightname ledtname6" style="border-radius: 20px;">Laptops</p>
                    </li>
                    <li>
                        <p class="ledtname ledtname7" style="border-radius: 20px;">Network Devices</p>
                        <p class="rightname ledtname8" style="border-radius: 20px;">Peripherals</p>
                    </li>
                    <li>
                        <p class="ledtname ledtname9" style="border-radius: 20px;">Software</p>
                        <p class="rightname ledtname10" style="border-radius: 20px;">Tablets</p>
                    </li>
                </ul>
            </div>

            <div class="socialMediaIcons">
                <ul class="list-unstyled list-unstyled1">
                   
                </ul>
            </div>

            <div class="address" style="text-align:center;">
               
            </div>


        </section>
            
    </div>
</div> 
            
            
            
            
        </div>
        

        
<script>
demoUpload1();
demoUpload2();

</script>
        
        
        
    </section>
    <!-- FORM WIZARD END HERE -->
@endsection

@section('scripts')
<script src="{{ url('web_assets/js/html2canvas.js') }}"></script> 
    <script src="{{ url('web_assets/js/form-wizard.js') }}"></script>
    <script src="{{ url('web_assets/js/jscolor.js') }}"></script>
    <script src="{{ url('web_assets/js/dropify.js') }}"></script>
     
<style>
    canvas {
   height: 492px;
    width: 225px;
    margin-top: -13px;
    border-radius: 21px;
    margin-left: 17px;
    }
</style>
    <script>
        jscolor.presets.default = {
            borderColor:'rgba(236,239,245,0.6)', width:200, height:100,
            buttonColor:'rgba(70,51,255,1)', shadowColor:'rgba(0,0,0,0.05)'
        };
    </script>

    <script>
    
        $(document).ready(function(){
           
            	$('#myModal').hide();
            	 $('#h1').hide();
            	 $('#h2').hide();
            	 $('.searchMe').hide();
            	
            	 $('#code').val('+91');
            	 $('#loader').hide(); 
                 $('#loader1').hide(); 
            	 $('#wrapper').hide();
            	 $('.information1').hide(); 
            	 $('.servicesBoxx1').hide();
            	 $('#p').hide();
            	 $('.preloader').fadeOut('slow');
            view();
            // $('.sf-btn-finish').click(function(){
            //         $('.preloader').fadeIn('slow');
                   
            // });
            // $('input[type="color"]').change(function(){
            //   $('.btnCustomStyle2').show();
            // });
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });
           
             
               
             $('.btnCustomStyle2').click(function(){
                 	 $('#wrapper').show();
                //   $('.preloader').fadeIn('slow');
                  	$('#loader').show();
                  	 //var files =$('#input-file-now').prop('files')[0];
                    
                  
            
             
            
              
                var form_data = new FormData($('#wizard_example_2')[0]);
                 
                
                    
   
                       $.ajax({
                            type: 'post',
                            url:  "{{route('admin.Cards.cards4')}}",
                            
                            data: form_data,
                            async: false,
                            dataType:'json',
                            success: function (data1) {
                            var    data=data1.va;
                                console.log(data);
                                $('.wrapper').css('background',data.primary);
                                $('.list-unstyled2').css('background',data.scondary);
                                $('.ledtname5').css('background',data.blog_color);
                                $('.ledtname6').css('background',data.blog_color);
                                $('.ledtname7').css('background',data.blog_color);
                                $('.ledtname8').css('background',data.blog_color);
                                $('.ledtname9').css('background',data.blog_color);
                                $('.ledtname10').css('background',data.blog_color);
                                $('.ledtname5').css('color',data.scondary);
                                $('.ledtname6').css('color',data.scondary);
                                $('.ledtname7').css('color',data.scondary);
                                $('.ledtname8').css('color',data.scondary);
                                $('.ledtname9').css('color',data.scondary);
                                $('.ledtname10').css('color',data.scondary);
                                $('.name').css('color',data.scondary);
                                $('.companyName').css('color',data.scondary);
                                $('#p').css('border', '3px solid '+data.scondary);
                                
                                $('.address').css('color',data.blog_color);
                                $('.ledtname1').css('color',data.heading);
                                $('.ledtname2').css('color',data.heading);
                                $('.ledtname3').css('color',data.heading); 
                                $('.ledtname4').css('color',data.heading);
                                $('#h').css('color',data.heading);
                                $('.rightname1').css('color',data.text);   
                                $('.rightname2').css('color',data.text);   
                                $('.rightname3').css('color',data.text);   
                                $('.rightname4').css('color',data.text);   
                                  
                                 
                                if(data.company_name!==null){
                                   $('.companyName').text(data.company_name);  
                                }
                               
                               if(data.first_name!==null && data.last_name!==null){
                                   $('.name').text(data.first_name+' '+data.last_name);
                               }
                               if(data.background_image!==null){
                               $('.profileboxx').css('background-image',"url("+data.background_image+")");
                               }else{
                                  $('.profileboxx').css('background-image',"url({{url('/web_assets/design-image/design-4/bgimg.jpg')}})"); 
                               }
                               if(data.profile_image!==null){
                                    $('#p').show();
                                   $("#p").html('<img class="" id="img" src="'+data.profile_image+'" alt="profilepix" style="border-radius: 50px;width: 108px;height: 100px;margin: 0 auto;">');
                               }else{
                                    $('#p').show();
                                    var img1 ="{{url('/web_assets/design-image/design-4/profilepix.jpg')}}";
                                   $("#p").html('<img class="" id="img" src="'+img1+'" alt="profilepix" style="border-radius: 50px;width: 108px;height: 100px;margin: 0 auto;">');
                               }
                              
                               $('.address').text(data.address);
                             if(data.email!==null){
                                 $('.ledtname1').text("Email");
                                 $('.rightname1').text(data.email);
                             }
                             
                             if(data.website3!==''){
                                 $('.ledtname2').text("Website");
                                 $('.rightname2').text(data.website3);
                             }
                             
                             if(data.whats_no!==null){
                                 $('.ledtname3').text("WhatsApp");
                                 $('.rightname3').text(data.code+'-'+data.whats_no);
                             }
                                
                                if(data.phone!==null){
                                 $('.ledtname4').text("Contact");
                                 $('.rightname4').text(data.code+'-'+data.phone);
                             }
                                if(data.phone!==null || data.whats_no!==null || data.website!==null || data.phone!==null){
                                    $('.information1').show(); 
                                }
                                
                                 
                                 
                                 if(data.services1!==null){
                                 $('.ledtname5').text(data.services1);
                                 }
                                 if(data.services2!==null){
                                 $('.ledtname6').text(data.services2);
                                 }
                                 if(data.services3!==null){
                                 $('.ledtname7').text(data.services3);
                                 }
                                 if(data.services4!==null){
                                 $('.ledtname8').text(data.services4);
                                 }
                                 if(data.services5!==null){
                                 $('.ledtname9').text(data.services5);
                                 }
                                 if(data.services6!==null){
                                 $('.ledtname10').text(data.services6);
                                 }
                             
                            
                                if(data.services1!==null || data.services2!==null || data.services3!==null || data.services4!==null || data.services5!==null || data.services6!==null){
                                    $('.servicesBoxx1').show();
                                }
                                
                                var i;
                                var html='';
                            //   if(data.social_media[1]!==null || data.social_media[2]!==null || data.social_media[3]!==null){
                            //       $('.searchMe').show();
                            //   }
                                   for(var i in data.social_media){
                                       if(data.social_media[i]!==null)
                                        html+=' <li><a href="'+data.social_media[i]+'" style="font-size: 30px;width: 50px;height: 50px;line-height: 50px;text-align: center;text-decoration: none;border-radius: 25px;display: inline-block;background:transparent;;color: #2d6ff7;border: 1px solid '+data.scondary+';"><i class="fa '+data.socialMediaList[i]+'" aria-hidden="true" style="color:'+data.blog_color+'"></i></a></li>';
                                   }
                               $('.list-unstyled1').html(html);
                                 $('.preloader').fadeOut('slow');
                               	$('#img').hide(); 
                               		dview();
                               		setTimeout(function(){ $('#loader').hide();  }, 4000);
                               	 
                               	
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                   
      
         });
           
         $('button').click(function(){
             $('#myModal').hide(); 
         });   
            
            $('.finish-btn').click(function(){
                 $('form').attr('action', "{{route('admin.Cards.generate_pdf4')}}");
                 $('form').attr('target','popup');
                 var urrl ="{{route('admin.Cards.generate_pdf4')}}";
                 $('form').attr(' onclick',"window.open("+urrl+",'popup','width=800,height=800');");
            });
            
            function view(){
            
                $.ajax({
                    url:"{{route('admin.Cards.delete-image')}}",
                    method:"get",
                    success: function(data) {
                        console.log(data);
                    }
                    
                });
            }
            
            
            
// global variable
 
    function dview() {
         var element = $("#html-content-holder"); // global variable
       var getCanvas; 

         html2canvas(element, {
              
         onrendered: function (canvas) {
                 
                $("#previewImage").html(canvas);
                getCanvas = canvas;
                console.log(canvas);
             }
         });
             
       //   $('#frame').show(); 
         $('#wrapper').hide(); 
        $('#img').hide(); 
       
    }
   // $('#frame').hide(); 
//   $('#wrapper').hide();  

    $("#Primary").change(function(){
       var color=$("#Primary").val();
        $("#Primary option[value="+ color +"]").prop('disabled', false);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#blog_color option[value=" + color + "]").prop('disabled', true);
        $("#heading option[value=" + color + "]").prop('disabled', true);
    }) ;   
        
  $("#Secondary").change(function(){
       var color=$("#Secondary").val();
       $("#Secondary option[value="+ color +"]").prop('disabled', false);
       $("#Primary option[value="+ color +"]").prop('disabled', true);
       $("#text option[value=" + color + "]").prop('disabled', true);
       $("#blog_color option[value=" + color + "]").prop('disabled', true);
       $("#heading option[value=" + color + "]").prop('disabled', true);
    }) ;   
    
    $("#text").change(function(){
       var color=$("#text").val();
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', false);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
        $("#blog_color option[value=" + color + "]").prop('disabled', true);
    }) ;   
    
     $("#blog_color").change(function(){
       var color=$("#blog_color").val();
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#blog_color option[value=" + color + "]").prop('disabled', false);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
    
    $("#heading").change(function(){
        var color=$("#heading").val();
        $("#heading option[value=" + color + "]").prop('disabled', false);
        $("#blog_color option[value=" + color + "]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
        
        //  $('.sf-controls').css("display","none");
        
        $('input[type="color"]').change(function(){
             var select1=$('#Primary').val();
             var select2=$('#Secondary').val();
             var select3=$('#text').val();
             var select4=$('#heading').val();
             var select5=$('#blog_color').val();
             if(select1!=='Choose Color' && select2!=='Choose Color' && select3!=='Choose Color' && select4!=='Choose Color' && select5!=='Choose Color'){
                 $('.sf-controls').css("display","block");
                   $('#active1').attr('class','active');
             }
            
        });
     
     $('.next-btn').click(function(){
              $('.sf-controls').css("display","none");
        });   
        
        
         $('.socialLinksboxx').on("focusout","input",function(){
             
           
             var input1=$('input[name="social_media[1]"]').val();
             var input2=$('input[name="social_media[2]"]').val();
             var input3=$('input[name="social_media[3]"]').val();
             var input4=$('input[name="social_media[4]"]').val();
             var input5=$('input[name="social_media[5]"]').val();
             
             if(input1!=='' && input2!=='' && input3!=='' && input4!=='' && input5!==''){
                   $('.sf-controls').css("display","block");
                     $('#active2').attr('class','active');
             }else{
                   $('.sf-controls').css("display","none");
                   $('.col').text('*Please fill all the boxes');
             }
            
        });
        
        
        
         $('.panel-group').on("focusout","input",function(){
            
             var first_name=$('input[name="first_name"]').val();
             var last_name=$('input[name="last_name"]').val();
             var company_name=$('input[name="company_name"]').val();
             var email=$('input[name="email"]').val();
             var services1=$('input[name="services1"]').val();
             var services2=$('input[name="services2"]').val();
             var services3=$('input[name="services3"]').val();
             var services4=$('input[name="services4"]').val();
             var services5=$('input[name="services5"]').val();
             var services6=$('input[name="services6"]').val();
             var phone=$('input[name="phone"]').val();
              var address=$('#address').val();
             var whats_no=$('input[name="whats_no"]').val();
            
             
             
                              if(first_name.length <31){
                                
                                  first_name1=first_name;
                              }else{
                                  $('.first_name').text('First Name max 30 Char');
                                    
                              }
                              
                               
                              
                              if(last_name.length <31){
                                 
                                  last_name1=last_name;
                              }else{
                                   $('.last_name').text('Last Name max 30 Char');
                                   
                              }
                              
                              
                              if(company_name.length <35){
                                 
                                  company_name1=company_name;
                              }else{
                                   $('.company_name').text('Company Name max 35 Char');
                                   
                              }
                              
                              
                              if(address.length <41){
                                 
                                  address1=address;
                              }else{
                                   $('.address').text('Address max 40 Char');
                                   
                              }
                              
                             
                            
                                if(services1.length <16){
                                  
                                  services11=services1;
                              }else{
                                  
                                  $('.services1').text('* Services 1 max 15 Char');
                              }
                              
                                if(services2.length <16){
                                  
                                  services21=services6;
                              }else{
                                  
                                  $('.services2').text('* Services 2 max 15 Char');
                              }
                              
                                if(services3.length <16){
                                  
                                  services31=services3;
                              }else{
                                  
                                  $('.services3').text('* Services 3 max 15 Char');
                              }
                              
                                if(services4.length <16){
                                  
                                  services41=services4;
                              }else{
                                  
                                  $('.services4').text('* Services 1 max 15 Char');
                              }
                              
                                if(services5.length <16){
                                  
                                  services51=services5;
                              }else{
                                  
                                  $('.services5').text('* Services 5 max 10 Char');
                              }
                              
                                
                              
                              if(services6.length <16){
                                  
                                  services61=services6;
                              }else{
                                  
                                  $('.services6').text('* Services 6 max 10 Char');
                              }
                              
                              
                                if(email!==''){
                              if(isEmail(email)==true){
                                 
                                  email=email;
                                   $('.email').css('color','green');
                                   $(".email").text('*Email Valid..!');
                              }else{
                                   //alert('Is not E-mail');
                                    $('.email').css('color','red');
                                    $('.email').text('*Is not E-mail');
                              }
                                }
                              if(phone!==''){
                              if(validatePhone(phone)==true){
                                 
                                  phone1=phone;
                                  $('.phone').css('color','green');
                                  $(".phone").text('*Phone Valid..!');
                              }else{
                                   //alert('Is not Valid..!');
                                     $('.phone').css('color','red');
                                   $('.phone').text('*Is not Valid Phone No... only no!');
                              }
                              }
                              
                              
                             
                              if(whats_no!==''){
                              if(validatePhone(whats_no)==true){
                                 
                                  whats_no=whats_no;
                                  $('.whats_no').css('color','green');
                                  $(".whats_no").text('* WhatsApp No Valid..!');
                              }else{
                                   
                                     $('.whats_no').css('color','red');
                                  $('.whats_no').text('*Is not Valid  Whats App No... only no!');
                              }
                              }
             if(first_name1!=='' && last_name1!=='' && email!=='' && company_name1!=='' && whats_no!=='' && services61!=='' && services51!=='' && services41!=='' && services31!=='' && services21!=='' && services11!==''  && address1!==''){
                
                 $('.sf-controls').css("display","block");
             }else{
                  $('.sf-controls').css("display","none");
             }
            
        });
        
        
        function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
          if (regex.test(email)) {
                 return true;
               }
            else {
                  return false;
             }
        }
        
        
     function validatePhone(Phone) {
    
    var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (filter.test(Phone)) {
        return true;
    }
    else {
        return false;
    }
}
                    
});
    </script>
    
   
    
    
@endsection