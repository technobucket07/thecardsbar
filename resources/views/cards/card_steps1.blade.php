@extends('cards.layouts.master')
@section('css')
         <link href="{{ url('web_assets/css/form-wizard.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/css/dropify.css') }}" rel="stylesheet">
         <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/demo/prism.css" />
         <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/bower_components/sweetalert/dist/sweetalert.css" />
	     <link rel="Stylesheet" type="text/css" href="{{ url('web_assets/js/style1.css?abc')}}" />
	     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	     <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
		 <script src="https://foliotek.github.io/Croppie/croppie.js"></script>
		 <script src="{{ url('web_assets/js/crop_js/script2.js')}}"></script>
        
       
      
    
       
@section('content')
    <!-- BACK BUTTON STARTS HERE -->
  <style>
        .text-muted1 {
     color: red;
        }
        
        .progressbar li {
         width: 33%;
            }
  canvas {
  
         width: 233.5px;
         margin-top: 16px;
         border-radius: 24px;
         margin-left: 15.2px;
         height: 510px;
}
/*.footer-wrapper{*/
/*    top: 256px;*/
/*}*/
/*.footerLeft{*/
/*      top: -35px;  */
/*}*/
   .footerBox{
      margin-top:-195px; 
   }
    #loader { 
      border: 12px solid #f3f3f3; 
      border-radius: 50%; 
      border-top: 12px solid #444444; 
      width: 70px; 
      height: 70px; 
      animation: spin 1s linear infinite; 
    } 
    
    @keyframes spin { 
      100% { 
        transform: rotate(360deg); 
      } 
    } 
    
    .center { 
      position: absolute; 
      top: 0; 
      bottom: 0; 
      left: 0; 
      right: 0; 
      margin: auto; 
    } 
    .mainFormWizard {
    padding: 0px 0px 0px 0px;
    background: #f9f8fd;
    }

  .footerBox {
    margin-top: -463px;
   }
  </style> 
 <script src="https://files.codepedia.info/files/uploads/iScripts/html2canvas.js"></script> 
 
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="backBtn">
                        <a class="btnCustomStyle2 btn-solid" href="index.html">
                            <span>Back</span>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- BACK BUTTON END HERE -->

    <section class="upperCircles">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="progressbar list-unstyled" style="width: 100%;">
                        <li class="active"></li>
                        <li id="active1"></li>
                        <li id="active2"></li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- FORM WIZARD STARTS HERE -->
    <section class="mainFormWizard">
        <div class="container-fluid">
            <span id="theme_name" style="display: none;">sky</span>
            <span id="trans_name" style="display: none;">slide</span>
            <span id="rtl" style="display: none;">0</span>
            <div class="row">
                <div class="col-md-9">
    <ul>
            @foreach ($errors->all() as $error)
           
              <li> <div class="alert alert-warning">
               <strong>Warning!</strong> {{ $error }}
              </div></li>
            @endforeach
        </ul>
                    <form id="wizard_example_2"  method="post" enctype="multipart/form-data" >
                       
                        @csrf
                        <fieldset>
                            <legend>Color</legend>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Primary Color</label>
                                             <input type="color" class="form-control"   id="Primary" name="primary"  style="height:80px;width:200px;" value="#070709">
                                          
                                           
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Heading Color</label>
                                             <input type="color" class="form-control"  id="heading" name="heading"  style="height:80px;width:200px;" value="#ff3b3c">
                                          
                                           
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Secondary Color</label>
                                             <input type="color" class="form-control"  id="Secondary" name="scondary" style="height:80px;width:200px;" value="#ffffff">
                                         
                                          
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Text Color</label>
                                             <input type="color" class="form-control"  id="text" name="text" style="height:80px;width:200px;" value="#797979">
                                           
                                            
                                        </div>
                                    </div>
                                </div>
                                
                                
                                
                                 
                               
                            </div>
                        </fieldset>
                      
                        <fieldset>
                            <legend>Social Links</legend>
                            @include('cards.socialmedia')
                            <!-- <div class="row">
                                <div class="col-md-12">
                                    <span class="col" style="color:red;font-size:16px;"></span>
                                    <ul class="list-unstyled socialLinksboxx">
                                        @foreach($socialMedia as $social)
                                            <li class="clearfix">
                                                 <span class="titleSocial">
                                                     <i class="fa {{$social['icon']}}" aria-hidden="true"></i>{{$social['name']}}
                                                 </span>
                                                 
                                                   @if($social['icon']=='fa-whatsapp')
                                              
                                                 <select class="form-control" name="code" style="width: 78px; height: 29px;float: left;margin-top: 0px;font-size: 12px;margin-left: 63px;" id="code">
                                                    @foreach($va as $va1)
                                                   <option value="{{$va1->callingCode}}">{{$va1->callingCode}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$va1->country}} </option>
                                                    @endforeach
                                                </select>
                                                 <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="7484564564">
                                                 </span>
                                                   @elseif($social['icon']=='fa-facebook')
                                                  <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.facebook.com">
                                                 </span>
                                                 
                                                 @elseif($social['icon']=='fa-skype')
                                                  <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.skype.com">
                                                 </span>
                                                 
                                                
                                                  
                                                   @elseif($social['icon']=='fa-linkedin')
                                                    <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.linkedin.com">
                                                 </span>
                                                    @elseif($social['icon']=='fa-twitter')
                                                   <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://twitter.com">
                                                 </span>
                                                   @endif
                                                
                                            </li>
                                             
                                        @endforeach
                                    </ul>
                                </div>
                            </div> -->
                        </fieldset>
                      <fieldset>
                            <legend>Profile </legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        Personal Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                
                                                <div class="panel-body personalBoxxx">
                                                  @include('cards.createfor')
                                                    <div class="row">
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" name="first_name" placeholder="First Name" maxlength="30" value="Amaira">
                                                                <small class="text-muted1 first_name"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Last Name" name="last_name" maxlength="30" value="Taylor">
                                                                <small class="text-muted1 last_name"></small>
                                                            </div>
                                                        </div>
                                                        <!--<div class="col-md-12 col-lg-6">-->
                                                        <!--    <div class="form-group ">-->
                                                        <!--        <input type="email" class="form-control" placeholder="Email" name="email">-->
                                                        <!--        <small class="text-muted1 email"></small>-->
                                                        <!--    </div>-->
                                                        <!--</div>-->
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Designation" name="designation" maxlength="50" value="Makeup Artist">
                                                                 <small class="text-muted1 designation"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12">
                                                            <div class="form-group ">
                                                                <textarea class="form-control" rows="3" id="about" name="about" placeholder="About me Description" maxlength="100">Lorem Ipsum is simply dummy text of the printing and typesetting.</textarea>
                                                                <small class="text-muted1 about"></small>
                                                            </div>
                                                        </div>
                                                        
                                                     
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
           
                       
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingfour">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                                        Contact Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                                <div class="panel-body businessInformation">

                                                    <div class="row">

                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" name="phone" class="form-control" placeholder="Primary Number" maxlength="10" value="2125124857">
                                                                    <small class="text-muted1 phone"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6 ">
                                                          <div class="form-group ">
                                                            <input type="text" id="autocomplete" class="form-control" placeholder="Search your location" value="" onFocus="geolocate()">
                                                             <small class="text-muted1 address"></small>
                                                            <input type="hidden" name="location" id="latlong" value="">
                                                          </div>
                                                        </div>
                                                        
                                                        <div class="col-md-12 col-lg-6 ">
                                                          <div class="form-group ">
                                                            <input type="text" class="form-control" placeholder="Enter your Official Address" name="address" value="Disney Springs, Buena Vista Drive, Lake Buena Vista, FL, USA">
                                                            <small class="text-muted1 address"></small>
                                                          </div>
                                                        </div>

                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" class="form-control" placeholder="Website" name="website" value="www.example.com">
                                                                 <small class="text-muted1 website"></small>
                                                            </div>
                                                        </div>
                                                          <div class="col-md-12 col-lg-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>BackgroundImage</label>
                                                                    <!--<input type="file" name="image1" id="input-file-now" class="dropify" />-->
                                                                    
                                                                      <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload1"  value="Choose Picture" accept="image/*" class="dropify" style="height:400px;">
                                                                       <input type="hidden" name="image1" id="base64image1">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap1">
                                                                         <div id="upload-demo1"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                    
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                         <div class="col-md-12 col-lg-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Profile Image</label>
                                                                    <!--<input type="file" name="image2" id="input-file-now1" class="dropify" />-->
                                                                      <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload2"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="image2" id="base64image2">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap2">
                                                                         <div id="upload-demo2"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                         <div class="col-md-12 col-lg-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Gallery1 Image</label>
                                                                    <!--<input type="file" name="gallery1" id="input-file-now2" class="dropify" />-->
                                                                      <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload3"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="gallery1" id="base64image3">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap3">
                                                                         <div id="upload-demo3"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                         <div class="col-md-12 col-lg-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Gallery2 Image</label>
                                                                    <!--<input type="file" name="gallery2" id="input-file-now3" class="dropify" />-->
                                                                    <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload4"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="gallery2" id="base64image4">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap4">
                                                                         <div id="upload-demo4"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                         <div class="col-md-12 col-lg-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Gallery3 Image</label>
                                                                    <!--<input type="file" name="gallery3" id="input-file-now4" class="dropify" />-->
                                                                    
                                                                    <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload5"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="gallery3" id="base64image5">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap5">
                                                                         <div id="upload-demo5"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                             
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                            <div class="col-md-12">
                                <p class="backBtn">
                                    <a class="btnCustomStyle2 btn-solid Preview1 Preview2 Preview3 Preview4 Preview5" href="javascript:void(0);">
                                        <span>Preview</span>
                                    </a>
                                </p>
                            </div>
                          
                       </form>
                    </div>
                <div class="col-md-3">
                  
                    <div style="background-image: url('{{ url("web_assets")}}/img/Phone1.png');background-color: #cccccc;background-position: center;background-repeat: no-repeat;background-size: cover;position: relative;padding: 0px 1px;width: 265px;height: 536px;border-radius: 31px;">
                        
                         <div id="previewImage" ></div>
                          <div id="img" style="width: 275px;margin-left: 8px;">
                           <img   src="{{ url('web_assets')}}/img/desine-2.png" alt="" style="border-radius: 29px;height: 511px;width: 238px;margin-left: 6px;margin-top: 15px;">
                       </div>
                        <div id="loader" class="center" ></div> 
                    </div>
                   
      
                      
                  
                   
                </div>
                
                
                
                <div style="margin-top: -97px;;opacity:0;"> 
                   
<div id="html-content-holder1" style="margin-top:0px;" >

<section id="wrapper" style="font-size: 12px;width:322px;height:741px;border-radius:40px 40px 40px 40px;">
   <table width="100%" cellspacing="0" border="0">
      	<tr >
      		<th id="primary4" style="padding: 0px;height: 391px;">
            </th> 
      		<th id="scondary" rowspan="2"  style="width:86px;">
      		         
                <table align="center" style="text-align: center;">
                        <h5 class="searchMe" style="text-align: center;font-size: 15px;margin-top: 36px;">Search <p>me @</p></h5>
                        <tbody class="socialMedia">
                       </tbody>
                </table>
            </th>
      	</tr>
        <tr>
      		<th id="primary" height="50px;"> 
      	    
                    </th>
                </tr>
                <tr  >
                <th id="primary1"  style="height:100px">
                 <div class="abtSec">
      		     </div>

                </th>
                <th rowspan="4" id="url1">
                     <div id="url"> 
                 
                </div>
                </th>
               
            </tr>

      	<tr>
      		<th  id="primary2" style="height:110px">
      		    
                <div class="galleryBoxx">

            
                </div>
            </th>
         </tr>
          <tr>
              <th id="primary3" style="height:70px;">
                <div class="contactBoxx" style="text-align: right;padding-bottom:15px">
                </div>
      		</th>
  		   </tr>
  		   
  		   <tr><th id="primary5" style="height:20px;"></th></tr>
         </table>

	</section>
	</div>
 </div> 
                
                
                
                
                
                
            </div>
          </div>
   <script>
demoUpload1();
demoUpload2();
demoUpload3();
demoUpload4();
demoUpload5();

</script>
    

</section>

    <!-- FORM WIZARD END HERE -->
@endsection

@section('scripts')
    <script src="{{ url('web_assets/js/html2canvas.js') }}"></script>
    <script src="{{ url('web_assets/js/form-wizard.js') }}"></script>
    <script src="{{ url('web_assets/js/jscolor.js') }}"></script>
    <script src="{{ url('web_assets/js/dropify.js') }}"></script>
           

    <script>
   
        jscolor.presets.default = {
            borderColor:'rgba(236,239,245,0.6)', width:200, height:100,
            buttonColor:'rgba(70,51,255,1)', shadowColor:'rgba(0,0,0,0.05)'
        };
    </script>

    <script>
        $(document).ready(function(){
            	 $('#loader').hide();
                 $('#h1').hide();
            	 $('#h2').hide();
            	 $('.searchMe').hide();
            	
            	 $('#code').val('+91');
            	 $('#img1').hide(); 
            	
            view();
            
            $('input[type="color"]').change(function(){
              $('.btnCustomStyle2').show();
            });
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });
           
           
               
             $('.btnCustomStyle2').click(function(){
             	$('#img1').show(); 
             	$('#loader').show();
              
            
              
                var form_data = new FormData($('#wizard_example_2')[0]);
                 
                    
   
                       $.ajax({
                            type: 'post',
                            url:  "{{route('admin.Cards.cards1')}}",
                            
                            data: form_data,
                            async: false,
                            dataType:'json',
                            success: function (data1) {
                            var    data=data1.va;
                                console.log(data);
                               
                               
                               
                              
                            
                              if(data.website1!==null){
                                    $('#url').html('<div style="-ms-transform: rotate(-90deg);transform: rotate(-90deg);width:50px;margin-top: 165px;margin-left: 14px;"><a href="#" style="text-decoration: none;color:'+data.text+';cursor: pointer;font-size: 14px;" ><span style="font-family: "Poppins", sans-serif;">'+data.website+'</a></div>');
                              }
                                
                              if(data.first_name!==null && data.designation!==null && data.image2!==null){   
                                    $('#primary').html('<div class="profilePicBoxx" style="padding: 6px;position: relative;"><div style="float: right;border: 2px solid '+data.text+';border-radius: 35px;height: 71px;width:71px;"><img  src="'+data.image2+'" alt="profilepix" style="width:67px;border-radius: 35px;height:67px;" ></div><div class="propNameDesignation"><h3 style="color:'+data.heading+';float: right;margin-top: 15px;margin-right: 10px;font-size: 15px;">'+data.first_name+' '+data.last_name+'</h3><p style="color:'+data.text+';margin-top: -2px;float: right;font-size: 12px;margin-right: 7px;">'+data.designation+'</p></div></div>');
                                  }else{
                                    var img1 ="{{url('/web_assets/design-image/design-2/profilepix.jpg')}}";
                                  $('#primary').html('<div class="profilePicBoxx" style="padding: 6px;position: relative;"><div style="float: right;border: 2px solid '+data.text+';border-radius: 35px;height: 71px;width:71px;"><img  src="'+img1+'" alt="profilepix" style="width:67px;border-radius: 35px;height:67px;" ></div><div class="propNameDesignation"><h3 style="color:'+data.heading+';float: right;margin-top: 15px;margin-right: 10px;font-size: 15px;">'+data.first_name+' '+data.last_name+'</h3><p style="color:'+data.text+';margin-top: -2px;float: right;font-size: 12px;margin-right: 7px;">'+data.designation+'</p></div></div>');
                                  }
                              if(data.about!==null){
                                   $('.abtSec').html('<h4 style="color:'+data.heading+';text-align: right;margin-right: 5px;font-size: 16px;">About Me</h4><p style=" color:'+data.text+';text-align: right;margin-right: 5px;font-size: 12px;">'+data.about+'</p>'); 
                                 }
                                if(data.phone!==null){
                                   $('.contactBoxx').html('<h4 style="color:'+data.heading+';margin-right: 10px;font-size: 16px;line-height: 0.5;">Contact Me</h4><a href="#" style=" color:'+data.text+';text-decoration: none;margin-right: 10px;font-size: 12px;">'+data.phone+'</a>')
                                  }
                                  
                                   var html1='';
                              if(data.gallery1!==null && data.gallery2!==null && data.gallery3!==null){
                       html1+='<h4 style="color:'+data.heading+';margin-right: 5px;font-size: 16px;float: right;">Gallery</h4><ul class="list-unstyled" id="list-unstyled" style="display: flex;float: right;">'
                        +'<li style="margin-right:5px;"><img class="img-fluid" src="'+data.gallery1+'" alt="ser-1" style="width:65px;height:63px;border-radius: 20px;"></li>'
                        +'<li style="margin-right:5px;"><img class="img-fluid" src="'+data.gallery2+'" alt="ser-2" style="width:65px;height:63px;border-radius: 20px;"></li>'
                        +'<li style="margin-right:5px;"><img class="img-fluid" src="'+data.gallery3+'" alt="ser-3" style="width:65px;height:63px;border-radius: 20px;"></li></ul> ';
                             
                                  }else{
                                    var img1="{{url('/web_assets/design-image/design-2/ser-1.jpg')}}";
                                    var img2="{{url('/web_assets/design-image/design-2/ser-2.jpg')}}";
                                    var img3="{{url('/web_assets/design-image/design-2/ser-3.jpg')}}";
                        html1+='<h4 style="color:'+data.heading+';margin-right: 5px;font-size: 16px;float: right;">Gallery</h4><ul class="list-unstyled" id="list-unstyled" style="display: flex;float: right;">'
                        +'<li style="margin-right:5px;"><img class="img-fluid" src="'+img1+'" alt="ser-1" style="width:65px;height:63px;border-radius: 20px;"></li>'
                        +'<li style="margin-right:5px;"><img class="img-fluid" src="'+img2+'" alt="ser-2" style="width:65px;height:63px;border-radius: 20px;"></li>'
                        +'<li style="margin-right:5px;"><img class="img-fluid" src="'+img3+'" alt="ser-3" style="width:65px;height:63px;border-radius: 20px;"></li></ul> ';
                                  }
                                  
                                     $('.galleryBoxx').html(html1);
                                
                                var i;
                                var html='';
                              if(data.social_media[1]!==null || data.social_media[2]!==null || data.social_media[3]!==null){
                                   $('.searchMe').show();
                                    $('.searchMe').css('color',data.text);
                              
                                   for(var i in data.social_media){
                                       if(data.social_media[i]!==null)
                                         html +='<tr><td><a href="#" ><div style="width: 47px;height: 48px;border-radius: 23px;border: 2px solid '+data.heading+';background:'+data.primary+';margin-top: 10px;"><i class="fa '+data.socialMediaList[i]+'" aria-hidden="true" style="font-size: 27px;color:'+data.text+';margin-top: 9px;" ></i></div></a></td></tr>';
                                           $('.socialMedia').html(html);
                                     }
                                   
                                 }
                              
                                if(data.image1!==null){
                                    $('#primary4').html('<img src="'+data.image1+'" alt="girlbg" class="img-fluid d-block" >');
                                }else{
                                  var userimg ="{{url('/web_assets/design-image/design-2/userimage.png')}}";
                                    $('#primary4').html('<img src="'+userimg+'" alt="girlbg" class="img-fluid d-block" style="height:100%;">');
                                }
                                  $("#primary").css('background',data.primary);
                                  $("#primary1").css('background',data.primary);
                                  $("#primary2").css('background',data.primary);
                                  $("#primary3").css('background',data.primary);
                                  $("#primary4").css('background',data.primary);
                                  $("#primary5").css('background',data.primary);
                                  $("#scondary").css('background',data.scondary);
                                  $("#url1").css('background',data.scondary);
                                  	
                               	$('#img').hide(); 
                               	$('#img1').hide(); 
                               	
                               	setTimeout(function(){ $('#loader').hide();  }, 4000);
                               	 	setTimeout(function(){ 	dview();  }, 3000);
                               
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                   
      
         });
           
       
            
            $('.finish-btn').click(function(){
                 $('form').attr('action', "{{route('admin.Cards.generate_pdf1')}}");
                 var pdfgen ="{{route('admin.Cards.generate_pdf1')}}";
                 $('form').attr('target','popup');
                 $('form').attr(' onclick',"window.open("+pdfgen+",'popup','width=800,height=800');");
                
            });
            
            function view(){
            
                $.ajax({
                    url:"{{route('admin.Cards.delete-image')}}",
                    method:"get",
                    success: function(data) {
                        console.log(data);
                    }
                    
                });
            }
            
            
            
// global variable
 function dview() {
         var element = $("#html-content-holder1"); // global variable
       var getCanvas; 

         html2canvas(element, {
              x: 50, y: 100,
              draggable: true,
         onrendered: function (canvas) {
                 
                $("#previewImage").html(canvas);
                getCanvas = canvas;
                console.log(canvas);
             }
         });
             
         // $('#frame').show(); 
        // $('#wrapper').hide(); 
        $('#img').hide(); 
       
    }
   // $('#frame').hide(); 
 // $('#wrapper').hide();  
 
   

    $("#Primary").change(function(){
       var color=$("#Primary").val();
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', false);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
    }) ;   
        
  $("#Secondary").change(function(){
       var color=$("#Secondary").val();
        $("#heading option[value=" + color + "]").prop('disabled', true);
       $("#Secondary option[value="+ color +"]").prop('disabled', false);
       $("#Primary option[value="+ color +"]").prop('disabled', true);
       $("#text option[value=" + color + "]").prop('disabled', true);
    }) ;   
    
    $("#text").change(function(){
       var color=$("#text").val();
         $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', false);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
         $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
    
     $("#heading").change(function(){
       var color=$("#heading").val();
        $("#heading option[value=" + color + "]").prop('disabled', false);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
         $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
        
        //  $('.sf-controls').css("display","none");
        
        $('input[type="color"]').change(function(){
             var select1=$('#Primary').val();
             var select2=$('#Secondary').val();
             var select3=$('#text').val();
             var select4=$('#heading').val();
             if(select1!=='Choose Color' && select2!=='Choose Color' && select3!=='Choose Color' && select4!=='Choose Color'){
                 $('.sf-controls').css("display","block");
                   $('#active1').attr('class','active');
             }
            
        });
     
    //  $('.next-btn').click(function(){
    //           $('.sf-controls').css("display","none");
            
    //     });   
        
        
         $('.socialLinksboxx').on("focusout","input",function(){
             
           
             var input1=$('input[name="social_media[1]"]').val();
             var input2=$('input[name="social_media[2]"]').val();
             var input3=$('input[name="social_media[3]"]').val();
             var input4=$('input[name="social_media[4]"]').val();
             var input5=$('input[name="social_media[5]"]').val();
             
             if(input1!=='' && input2!=='' && input3!=='' && input4!=='' && input5!==''){
                   $('.sf-controls').css("display","block");
                   $('#active2').attr('class','active');
             }else{
                   $('.sf-controls').css("display","none");
                   $('.col').text('*Please fill all the boxes');
             }
            
        });
        
        
        
         $('.panel-group').on("focusout","input",function(){
            
             var first_name=$('input[name="first_name"]').val();
             var last_name=$('input[name="last_name"]').val();
             var email=$('input[name="email"]').val();
             var designation=$('input[name="designation"]').val();
             var about=$('#about').val();
             var image=$('input[name="image"]').val();
             var phone=$('input[name="phone"]').val();
             var address=$('input[name="address"]').val();
             //var website=$('input[name="website"]').val();
             
                              if(first_name.length <31){
                                
                                  first_name1=first_name;
                              }else{
                                  $('.first_name').text('First Name max 30 Char');
                                    // alert('*First Name max 30 Char');
                              }
                              
                               
                              
                              if(last_name.length <31){
                                 
                                  last_name1=last_name;
                              }else{
                                   $('.last_name').text('Last Name max 30 Char');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              if(about.length <145){
                                 
                                  about1=about;
                              }else{
                                   // alert('About max 300 Char');
                                     $('.about').text('*About max 145 Char');
                              }
                              
                         
                              if(designation.length <30){
                                  
                                  designation1=designation;
                              }else{
                                  //alert('Designation max 255 Char');
                                  $('.designation').text('* Designation max 30 Char');
                              }
                           
                              if(phone!==''){
                              if(validatePhone(phone)==true){
                                 
                                  phone1=phone;
                                  $('.phone').css('color','green');
                                  $(".phone").text('*Phone Valid..!');
                              }else{
                                   //alert('Is not Valid..!');
                                     $('.phone').css('color','red');
                                   $('.phone').text('*Is not Valid Phone No... only no!');
                              }
                              }
                             
             if(first_name1!=='' && last_name1!=='' && designation1!==''  && about1!==''  && phone1!=='' ){
                 // alert(first_name+" "+last_name+" "+email+" "+designation+"  "+about+" "+phone+" "+address+" "+website);
                 $('.sf-controls').css("display","block");
             }else{
                  $('.sf-controls').css("display","none");
             }
            
        });
        
        
        function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
          if (regex.test(email)) {
                 return true;
               }
            else {
                  return false;
             }
        }
        
        
     function validatePhone(Phone) {
    
    var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (filter.test(Phone)) {
        return true;
    }
    else {
        return false;
    }
}
                    
});


    </script>
    
   
    
    
@endsection