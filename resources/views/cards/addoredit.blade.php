@extends('admin.layouts.app')
@section('content')
<div class="main-panel">
  <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            @if(isset($card))
              {{ Form::model($card, ['route' => ['admin.Cards.postEditCard', $card->id], 'method' => 'patch','files'=> true]) }}
          @else
              {{ Form::open(['route' => 'admin.Cards.postAddCard','files'=> true]) }}
          @endif
            <div class="row">
              <div class="col-md-6">
                <div class="form-group required">
                  <label for="name">Card Name</label>
                  {{ Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Enter name','id'=>'name','required'=>1]) }}
                  @if($errors->has('name'))
                    <div class="error">{{ $errors->first('name') }}</div>
                  @endif
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group " id = "cardimage">
                  <label for="image">Card Image</label>
                  <div class="file-upload">
                    <div class="image-upload-wrap" @if(isset($card)) @if(!empty($card->image)) style = "display:none" @endif @endif>
                    {{ Form::file('image',['class'=>'form-control file-upload-input','id'=>'image','onchange'=>'readURL(this,"cardimage")','accept'=>'image/*']) }}
                      @if($errors->has('image'))
                      <div class="error">{{ $errors->first('image') }}</div>
                      @endif
                    <div class="drag-text">
                      <h3>Drag and drop or select Image</h3>
                    </div>
                    </div>
                    <div class="file-upload-content" @if(isset($card)) @if(!empty($card->image)) style = "display:block" @endif @endif>
                    <img class="file-upload-image" src='@if(isset($card)) {{ url("storage/$card->image") }} @endif' alt="your image" />
                    <div class="image-title-wrap">
                      <button type="button" onclick="removeUpload('cardimage')" class="remove-image"><i class="fa fa-trash" aria-hidden="true"></i> <span class="image-title">Uploaded Image</span></button>
                    </div>
                    </div>
                  </div>
                </div>
              </div> 
            </div>
              <div class="row">
                <div class="form-group col-md-6">
                   {!! Form::submit('Submit', ['class' => 'btn btn-primary submit_btn_new']) !!}
                </div>
              </div>
          {{ Form::close() }}
          </div>
        </div>
      </div>
  </div>
</div>
@endsection

@push('scripts')
@endpush
@push('styles')
@endpush