@extends('cards.layouts.master')
@section('css')
         <link href="{{ url('web_assets/css/form-wizard.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/css/dropify.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/bootstrap.min.css')}}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/font-awesome.min.css')}}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/main.css')}}" rel="stylesheet">
         
         <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/demo/prism.css" />
         <link rel="Stylesheet" type="text/css" href="https://foliotek.github.io/Croppie/bower_components/sweetalert/dist/sweetalert.css" />
	     <link rel="Stylesheet" type="text/css" href="{{ url('web_assets/js/style1.css?abc')}}" />
	     <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
	     <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
		 <script src="https://foliotek.github.io/Croppie/croppie.js"></script>
		 <script src="{{ url('web_assets/js/crop_js/script6.js')}}"></script>
       
       
    <style>
        .text-muted1 {
     color: red;
        }
        .progressbar li {
         
        }
        
        #loader { 
      border: 12px solid #f3f3f3; 
      border-radius: 50%; 
      border-top: 12px solid #444444; 
      width: 70px; 
      height: 70px; 
      animation: spin 1s linear infinite; 
    } 
    
    @keyframes spin { 
      100% { 
        transform: rotate(360deg); 
      } 
    } 
    
    .center { 
      position: absolute; 
      top: 0; 
      bottom: 0; 
      left: 0; 
      right: 0; 
      margin: auto; 
    } 
    

    </style>     
    
       
@section('content')
    <!-- BACK BUTTON STARTS HERE -->


    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="backBtn">
                        <a class="btnCustomStyle2 btn-solid" href="index.html">
                            <span>Back</span>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- BACK BUTTON END HERE -->

    <section class="upperCircles">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="progressbar list-unstyled" style="width: 100%;">
                        <li class="active" style=" width: 33%;"></li>
                        <li id="active1"  style=" width: 33%;"></li>
                        <li id="active2"  style=" width: 33%;"></li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- FORM WIZARD STARTS HERE -->
    <section class="mainFormWizard">
        <div class="container-fluid">
            <span id="theme_name" style="display: none;">sky</span>
            <span id="trans_name" style="display: none;">slide</span>
            <span id="rtl" style="display: none;">0</span>
            <div class="row">
                <div class="col-md-9">
    <ul>
            @foreach ($errors->all() as $error)
           
              <li> <div class="alert alert-warning">
               <strong>Warning!</strong> {{ $error }}
              </div></li>
            @endforeach
        </ul>
                    <form id="wizard_example_2"  method="post" enctype="multipart/form-data" >
                       
                        @csrf
                        <fieldset>
                            <legend>Color</legend>
                            <div class="row">
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Primary Color</label>
                                             <input type="color" class="form-control" id="Primary" name="primary" style="height:80px;width:200px;" value="#ffffff">
                                           
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Secondary Color</label>
                                             <input type="color" class="form-control" id="Secondary" name="scondary" style="height:80px;width:200px;" value="#0163d2">
                                        
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Text Color</label>
                                               <input type="color" class="form-control" id="text" name="text" style="height:80px;width:200px;" value="#000333">
                                         
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Blog Color</label>
                                        <input type="color" class="form-control" id="heading" name="blog_color" style="height:80px;width:200px;" value="#eff6fc">
                                          
                                           
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                
                                
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Button Color</label>
                                             <input type="color" class="form-control" id="heading1" name="button_color" style="height:80px;width:200px;" value="#f7d807">
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                
                                
                               
                            </div>
                        </fieldset>
                      
                        <fieldset>
                            <legend>Social Links</legend>
                            @include('cards.socialmedia')
                            <!-- <div class="row">
                                <div class="col-md-12">
                                    <span class="col" style="color:red;font-size:16px;"></span>
                                    <ul class="list-unstyled socialLinksboxx">
                                        @foreach($socialMedia as $social)
                                            <li class="clearfix">
                                                 <span class="titleSocial" >
                                                     <i class="fa {{$social['icon']}}" aria-hidden="true"></i>{{$social['name']}}
                                                 </span>
                                                 @if($social['icon']=='fa-whatsapp')
                                              
                                                 <select class="form-control" name="code" style="width: 78px; height: 29px;float: left;margin-top: 0px;font-size: 12px;margin-left: 63px;" id="code">
                                                    @foreach($va as $va1)
                                                   <option value="{{$va1->callingCode}}">{{$va1->callingCode}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$va1->country}} </option>
                                                    @endforeach
                                                </select>
                                                 <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="7484564564">
                                                 </span>
                                                   @elseif($social['icon']=='fa-facebook')
                                                  <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.facebook.com">
                                                 </span>
                                                 
                                                 @elseif($social['icon']=='fa-skype')
                                                  <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.skype.com">
                                                 </span>
                                                 
                                                
                                                  
                                                   @elseif($social['icon']=='fa-linkedin')
                                                    <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://www.linkedin.com">
                                                 </span>
                                                    @elseif($social['icon']=='fa-twitter')
                                                   <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link..." value="https://twitter.com">
                                                 </span>
                                                   @endif
                                                
                                            </li>
                                             
                                        @endforeach
                                    </ul>
                                </div>
                            </div> -->
                        </fieldset>
                        
                        
                      <fieldset>
                            <legend>Profile </legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        
                                              <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne1">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne1">
                                                        Company Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne1">
                                                
                                                <div class="panel-body personalBoxxx">
                                                    <div class="row">
                                                        
                                                        
                                                          <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Logo</label>
                                                                    <!--<input type="file" name="logo" id="input-file-now" class="dropify" />-->
                                                                    
                                                                     <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload1"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="logo" id="base64image1">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap1">
                                                                         <div id="upload-demo1"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <label>Title</label>
                                                                <input type="text" class="form-control" placeholder="Enter Company Name" name="company_name" value="ELECTRONICS">
                                                                 <small class="text-muted1 website"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <label>Quotation</label>
                                                                <input type="text" class="form-control" placeholder="Enter Company Quotation" name="quotation" value="Security Industry Association">
                                                                 <small class="text-muted1 website"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        
                                                       
                                                        
                                                      
                                                        
                                                     
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        Personal Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                
                                                <div class="panel-body personalBoxxx">
                                                  @include('cards.createfor')
                                                    <div class="row">
                                                        
                                                        
                                                          
                                                        
                                                        
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" name="first_name1" placeholder="First Name 1" value="Gurpreet Singh">
                                                                <small class="text-muted1 first_name1"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Enter Last Name1" name="last_name1" value="Gill">
                                                                <small class="text-muted1 last_name1"></small>
                                                            </div>
                                                        </div>
                                                        
                                                      
                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" name="phone1" class="form-control" placeholder="Phone Number 1" maxlength="10" value="1234567890">
                                                                    <small class="text-muted1 phone1"></small>
                                                            </div>
                                                        </div>
                                                       <div class="col-md-12 col-lg-6 "></div>
                                                       
                                                       <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" name="first_name2" placeholder="Enter First Name 2"  value="Harpreet Singh">
                                                                <small class="text-muted1 first_name2"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Enter Last Name2" name="last_name2" value="Gill">
                                                                <small class="text-muted1 last_name2"></small>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" name="phone2" class="form-control" placeholder="Enter Phone Number 2" maxlength="10" value="1234567890">
                                                                    <small class="text-muted1 phone2"></small>
                                                            </div>
                                                        </div>
                                                    <div class="col-md-12 col-lg-6 ">
                                                        <div class="form-group ">
                                                          <input type="text" id="autocomplete" class="form-control" placeholder="Search your location" value="" onFocus="geolocate()">
                                                           <small class="text-muted1 address"></small>
                                                          <input type="hidden" name="location" id="latlong" value="">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-12 col-lg-6 ">
                                                      <div class="form-group ">
                                                        <input type="text" class="form-control" placeholder="Enter your Official Address" name="address">
                                                        <small class="text-muted1 address"></small>
                                                      </div>
                                                    </div>
                                                      
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
           
                       
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingfour">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                                        Service Offers
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                                <div class="panel-body businessInformation">

                                                    <div class="row">

                                                        <!--<div class="col-md-12 col-lg-6 ">-->
                                                        <!--    <div class="form-group">-->
                                                        <!--        <input type="text" name="phone" class="form-control" placeholder="Primary Number" maxlength="10">-->
                                                        <!--            <small class="text-muted1 phone"></small>-->
                                                        <!--    </div>-->
                                                        <!--</div>-->
                                                       

                                                       
                                                        
                                                        <!--<div class="col-md-12 col-lg-6 ">-->
                                                        <!--    <div class="form-group ">-->
                                                        <!--        <input type="text" class="form-control" placeholder="Address" name="address" maxlength="100">-->
                                                        <!--         <small class="text-muted1 address"></small>-->

                                                        <!--    </div>-->
                                                        <!--</div>-->                      
                                                      <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Service Image 1</label>
                                                                    <!--<input type="file" name="service-image-1" id="input-file-now1" class="dropify" />-->
                                                                    
                                                                    <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload2"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="serviceimage1" id="base64image2">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap2">
                                                                         <div id="upload-demo2"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                         <div class="col-md-12 col-lg-12 ">
                                                            <div class="form-group">
                                                                <label>Service Name 1</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 1" name="service-name-1" value="TV & Audio">
                                                                 <small class="text-muted1 service-name-1"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Service Image 2</label>
                                                                    <!--<input type="file" name="service-image-2" id="input-file-now2" class="dropify" />-->
                                                                    
                                                                     <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload3"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="serviceimage2" id="base64image3">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap3">
                                                                         <div id="upload-demo3"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                         <div class="col-md-12 col-lg-12 ">
                                                            <div class="form-group">
                                                                <label>Service Name 2</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 2" name="service-name-2" value="Laptops & Tablets">
                                                                 <small class="text-muted1 service-name-2"></small>
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Service Image 3</label>
                                                                    <!--<input type="file" name="service-image-3" id="input-file-now3" class="dropify" />-->
                                                                    
                                                                    <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload4"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="serviceimage3" id="base64image4">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap4">
                                                                         <div id="upload-demo4"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                         <div class="col-md-12 col-lg-12 ">
                                                            <div class="form-group">
                                                                <label>Service Name 3</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 3" name="service-name-3" value="Home Accessories">
                                                                 <small class="text-muted1 service-name-3"></small>
                                                            </div>
                                                        </div>
                                                       
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Service Image 4</label>
                                                                    <!--<input type="file" name="service-image-4" id="input-file-now4" class="dropify" />-->
                                                                    
                                                                     <div class="row">
                                                                      <div class="col-md-6">
                                                                        <input type="file" id="upload5"  value="Choose Picture" accept="image/*" class="dropify">
                                                                       <input type="hidden" name="serviceimage4" id="base64image5">
                                                                      </div>
                                                                      <div class="col-md-6">
                                                                         <div class="upload-demo-wrap5">
                                                                         <div id="upload-demo5"></div>
                                                                       </div>
                                                                      </div>
                                                                    </div>
                                                                    
                                                                    
                                                                </div>
                                                            </div>
                                                        </div>
                                                         <div class="col-md-12 col-lg-12 ">
                                                            <div class="form-group">
                                                                <label>Service Name 4</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service Name 4" name="service-name-4" value="Games & Consoles">
                                                                 <small class="text-muted1 service-name-4"></small>
                                                            </div>
                                                        </div>
                                                       
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                             
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                            <div class="col-md-12">
                                <p class="backBtn">
                                    <a class="btnCustomStyle2 btn-solid Preview1 Preview2 Preview3 Preview4 Preview5" href="javascript:void(0);">
                                        <span>Preview</span>
                                    </a>
                                </p>
                            </div>
                          
                       </form>
                    </div>
                <div class="col-md-3">
                    
                    <div id="frame" style="background-image: url('{{ url("web_assets")}}/img/Phone1.png');background-color: #cccccc;background-position: center;background-repeat: no-repeat;background-size: cover;
  position: relative;width: 263px;height: 535px;border-radius: 30px;"></br>
                         <div id="previewImage" ></div>
                         <div id="loader" class="center" ></div> 
                          <div id="img" style="width: 235px;border-radius: 30px;margin-left: 15px;margin-top: -9px;">
                         <img class="img-fluid mx-auto d-block "  src="{{ url('web_assets')}}/img/phone.png" alt="" style="border-radius: 15px;">
                    </div>
                  
                    </div>
                   
      
                   
                   
                </div>
                
    <style>
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #f0394d; }
::selection { color: #f1f1f1;background: #f0394d; }



body  {font-family: 'Poppins', sans-serif;}
.wrapper {margin: 0 auto;background: #fff;}

.TopBlankbox {    background: #0163d2;
    /* visibility: hidden; */
    padding: 40px;
    height: 125px;
    
}




.mainbody {
    background: #fff;
    padding: 20px 40px 0 40px;
    border-radius: 36px 36px 0 0;
    height: 100%;
        margin: -30px 0 0 0;
}


.logoMainBox {}
.logoMainBox .companyPic {    width: 250px;
    margin: -70px auto 20px auto;
    padding: 20px;
    border-radius: 20px;
    /*border: 1px solid #eff6fc;*/
    box-shadow: 0 10px 26px rgb(167 167 167 / 10%);
    background: #fff;}
.logoMainBox .companyPic img {}
.logoMainBox h1 {    font-size: 24px;
    color: #000333;
    margin: 0;
    font-weight: 700;}
.logoMainBox p {    font-size: 16px;
    color: #babdc4;
    margin: 0;}




.proprietorName {    margin: 20px 0 0 0;}
.proprietorName ul {       padding: 10px; margin: 0;
    background: #eff6fc;
    border-radius: 10px;}
.proprietorName ul li {margin: 0 0 5px 0;}
.proprietorName ul li:last-child {margin: 0;}

.proprietorName ul li .titleName {    margin: 0;
    display: inline-block;
    width: 70%;
    height: 34px;
    line-height: 36px;
    float: left;
    font-size: 18px;
    /*font-weight: 600;*/
    color: #000333;}
.proprietorName ul li .calling { margin: 0;   display: inline-block;
    width: 30%;
    float: left;     text-align: right; }
.proprietorName ul li .calling a {    display: inline-block;
    padding: 5px 12px;
    background: #f7d807;
    color: #000333;
    /*font-weight: 600;*/
    border-radius: 10px;
}
.proprietorName ul li .calling a i {}




.serviceoffersBox {    margin: 20px 0 0 0;}
.serviceoffersBox .row {    margin-right: -7.5px;
    margin-left: -7.5px;}
.serviceoffersBox .row .col-md-6 {padding-right: 7.5px;
    padding-left: 7.5px;}
.serviceoffersBox h2 {font-size: 16px;
/*font-weight: 600;*/
color: #000333;}
.serviceoffersBox .user-card {    background: #eff6fc;
    border-radius: 10px;
    padding: 15px 0;
    margin: 0 0 15px 0;}
.serviceoffersBox .user-card p {}
.serviceoffersBox .user-card p img {}
.serviceoffersBox .user-card h4 {font-size: 12px;text-align: center;
/*font-weight: 600;*/
color: #000333;}



.socialMediaIcons {}
.socialMediaIcons ul{    margin: 0 auto;
    display: table;}
.socialMediaIcons ul li {    display: inline-block;
    margin: 0 5px;
    padding: 2px;
    background: #f7d807;
    border-radius: 50px;}
.socialMediaIcons ul li a {  
    font-size: 30px;
    width: 50px;
    height: 50px;
    line-height: 50px;
    text-align: center;
    text-decoration: none;
    border-radius: 50%;
    display: inline-block;
    background: #f7d807;
    color: #000333;
    border: 2px solid #ffffff;
}
.socialMediaIcons ul li a .fa {
    font-size: 26px;
}

.socialMediaIcons ul li a .fa:hover {
    opacity: 0.7;
}



        </style>
     
    <div style="opacity:0;">      
<div id="html-content-holder">
    
    <section class="wrapper"  id="wrapper" style="width:400px;background:red;height:880px;">     
      
                      <div class="TopBlankbox"></div>
                     <div class="mainbody" align="center">
                         <div class="logoMainBox">
                                  <p class="companyPic" style="height:80px;border-bottom: 5px solid #b5acac2b;border-left: 1px solid #b5acac2b;border-right: 1px solid #b5acac2b;" align="center">
                                   
                                  </p>
                               <div id="companyPic">
                                   
                               </div>
                          </div>
                   <table   style="height:510px;width:100%;" >
                       
                   <tr>
                       <td colspan="2" style="height: 150px;">
                             <div class="proprietorName">
                                <ul class="list-unstyled" id="list-unstyled">
                                  
                                    
                                </ul>
                            </div>
                           
                       </td>
                     </tr>
                     <tr>
                         <td colspan="2" style="height:30px;" id="service">
                          
                         </td>
                     </tr>
                     <tr id="middile1" style="padding: 10px;">
                        
                     </tr>
                      
                       <tr id="middile2" style="padding: 10px;">
                         
                     </tr>
                   
                      
                     <tr>
                         <td colspan="2" style="height:150px;">      
                         <table style="width:100%" border="0" align="center" >
                        <tr id="socialMedia">
                            
                            
                        </tr>
                    </table>
                     </td>
                     </tr>
                   </table>
                       
              </div>
       </section>           
           
 
 </div> 
  </div>            
            
            
            
        </div>
        

        

<script>
demoUpload1();
demoUpload2();
demoUpload3();
demoUpload4();
demoUpload5();

</script>        
        
    </section>
    <!-- FORM WIZARD END HERE -->
@endsection

@section('scripts')
    <script src="{{ url('web_assets/js/html2canvas.js') }}"></script>
    <script src="{{ url('web_assets/js/form-wizard.js') }}"></script>
    <script src="{{ url('web_assets/js/jscolor.js') }}"></script>
    <script src="{{ url('web_assets/js/dropify.js') }}"></script>
     
<style>
    canvas {
    height: 511px;
    width: 238px;
    margin-top: -10px;
    border-radius: 27px;
    margin-left: 13px;

    }
</style>
    <script>
        jscolor.presets.default = {
            borderColor:'rgba(236,239,245,0.6)', width:200, height:100,
            buttonColor:'rgba(70,51,255,1)', shadowColor:'rgba(0,0,0,0.05)'
        };
    </script>

    <script>
        $(document).ready(function(){
            	$('#myModal').hide();
            	 $('#h1').hide();
            	 $('#h2').hide();
            	 $('.searchMe').hide();
            // 	 $('.btnCustomStyle2').hide();
            	 $('#code').val('+91');
            	 $('#loader').hide(); 
            	 $('#wrapper').hide();
             $('.companyPic').hide();
             $('.proprietorName').hide();
            view();
            $('input[type="color"]').change(function(){
              $('.btnCustomStyle2').show();
            });
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });
           
            
               
             $('.btnCustomStyle2').click(function(){
                    $('#wrapper').show();
                  	$('#loader').show();
                 
              
                var form_data = new FormData($('#wizard_example_2')[0]);
                 
                    
   
                       $.ajax({
                            type: 'post',
                            url:  "{{route('admin.Cards.cards6')}}",
                            
                            data: form_data,
                            async: false,
                            dataType:'json',
                            success: function (data1) {
                            var    data=data1.va;
                                console.log(data);
                                if(data.logo!==null){
                                    $('.companyPic').show();
                                     $('.companyPic').html('<img src="'+data.logo+'" alt="logo-pic" style="height:46px;" >');
                                 }else{
                                      $('.companyPic').show();
                                      var img1 ="{{url('/web_assets/design-image/design-6/logo-pic.png')}}";
                                     $('.companyPic').html('<img src="'+img1+'" alt="logo-pic" style="height:46px;" >')
                                 }
                                 
                                 if(data.company_name!==null && data.quotation!=null){
                                      $('.companyPic').show();
                                     $('#companyPic').html('<h1 class="text-center text-uppercase" style="color:'+data.text+'">'+data.company_name+'</h1><p class="text-center" style="color:'+data.text+'">'+data.quotation+'</p>')
                                 }
                             
                                if(data.first_name1!==null && data.last_name1!==null && data.first_name2!==null && data.last_name2!==null){
                                    $('.proprietorName').show();
                                    $('#list-unstyled').html('<li class="clearfix"><p class="titleName" style="font-size: 14px;margin-top:5px;color:'+data.text+'">'+data.first_name1+' '+data.last_name1+'</p><p class="calling" style="margin-top:10px;"><a href="#" style="color:'+data.text+';font-size:13px;background:'+data.button_color+';"> Call <i class="fa fa-phone" aria-hidden="true" style="margin-top:5px;font-size:13px;"></i></a></p></li><li class="clearfix"><p class="titleName" style="font-size: 14px;;margin-top:5px;color:'+data.text+'">'+data.first_name2+' '+data.last_name2+'</p><p class="calling" style="margin-top:10px;"><a href="#" style="color:'+data.text+';font-size:13px;background:'+data.button_color+';"> Call <i class="fa fa-phone" aria-hidden="true" style="margin-top:5px;font-size:13px;"></i></a></p></li>');
                                }
                                
                                if(data.serviceimage1!==null && data.serviceimage2!==null && data.servicename1!==null && data.servicename2!==null){
                                        $('#service').html('<h2 style="font-size: 16px;margin-top: 10px;color:'+data.text+'">Service Offers</h2>');
                                    $('#middile1').html('<td style="height:150px;width:150px;" align="center"><div><div style="background:'+data.blog_color+';height: 135px;width: 145px;"><img src="'+data.serviceimage1+'" alt="ser-1"  style="width:50px;height:50px;margin: 30px 0px 0px 0px;"> <h4 style="font-size: 12px;text-align: center;color:'+data.text+';margin-top:5px;">'+data.servicename1+'</h4></div></div></td><td style="height:150px;width:150px" align="center"><div><div style="background:'+data.blog_color+';height: 135px;width: 145px;"><img src="'+data.serviceimage2+'" alt="ser-1"  style="width:50px;height:50px;margin: 30px 0px 0px 0px;"> <h4 style="font-size: 12px;text-align: center;color:'+data.text+';margin-top:5px;">'+data.servicename2+'</h4></div> </div></td>');
                                }else{
                                    $('#service').html('<h2 style="font-size: 16px;margin-top: 10px;color:'+data.text+'">Service Offers</h2>');
                                    var img2 ="{{url('/web_assets/design-image/design-6/ser-1.png')}}";
                                    var img3 ="{{url('/web_assets/design-image/design-6/ser-2.png')}}";
                                    $('#middile1').html('<td style="height:150px;width:150px;" align="center"><div><div style="background:'+data.blog_color+';height: 135px;width: 145px;"><img src="'+img2+'" alt="ser-1"  style="width:50px;height:50px;margin: 30px 0px 0px 0px;"> <h4 style="font-size: 12px;text-align: center;color:'+data.text+';margin-top:5px;">'+data.servicename1+'</h4></div></div></td><td style="height:150px;width:150px" align="center"><div><div style="background:'+data.blog_color+';height: 135px;width: 145px;"><img src="'+img3+'" alt="ser-1"  style="width:50px;height:50px;margin: 30px 0px 0px 0px;"> <h4 style="font-size: 12px;text-align: center;color:'+data.text+';margin-top:5px;">'+data.servicename2+'</h4></div> </div></td>');
                                }
                                
                                 if(data.serviceimage3!==null && data.serviceimage4!==null && data.servicename3!==null && data.servicename4!==null){
                                    $('#middile2').html('<td style="height:150px;width:150px;" align="center"><div><div style="background:'+data.blog_color+';height: 135px;width: 145px;"><img src="'+data.serviceimage3+'" alt="ser-1"  style="width:50px;height:50px;margin: 30px 0px 0px 0px;"> <h4 style="font-size: 12px;text-align: center;color: '+data.text+';margin-top:5px;">'+data.servicename3+'</h4></div></div></td><td style="height:150px;width:150px" align="center"><div><div style="background:'+data.blog_color+';height: 135px;width: 145px;"><img src="'+data.serviceimage4+'" alt="ser-1"  style="width:50px;height:50px;margin: 30px 0px 0px 0px;"> <h4 style="font-size: 12px;text-align: center;color:'+data.text+';margin-top:5px;">'+data.servicename4+'</h4></div> </div></td>');
                                }else{
                                  var img4 ="{{url('/web_assets/design-image/design-6/ser-3.png')}}";
                                  var img5 ="{{url('/web_assets/design-image/design-6/ser-4.png')}}";

                                     $('#middile2').html('<td style="height:150px;width:150px;" align="center"><div><div style="background:'+data.blog_color+';height: 135px;width: 145px;"><img src="'+img4+'" alt="ser-1"  style="width:50px;height:50px;margin: 30px 0px 0px 0px;"> <h4 style="font-size: 12px;text-align: center;color: '+data.text+';margin-top:5px;">'+data.servicename3+'</h4></div></div></td><td style="height:150px;width:150px" align="center"><div><div style="background:'+data.blog_color+';height: 135px;width: 145px;"><img src="'+img5+'" alt="ser-1"  style="width:50px;height:50px;margin: 30px 0px 0px 0px;"> <h4 style="font-size: 12px;text-align: center;color:'+data.text+';margin-top:5px;">'+data.servicename4+'</h4></div> </div></td>');
                                }
                          
                                
                                var i;
                                var html='';
                              
                                  for(var i in data.social_media){
                                      if(data.social_media[i]!==null)
                                      
                                   if(data.socialMediaList[i]=='fa-linkedin'){
                                        html+='<td style="height: 50px;width: 50px;text-align: center;"><a href="#" style="font-size: 30px;width: 50px;height: 50px;line-height: 50px;text-align: center;text-decoration: none;border-radius: 25px;display: inline-block;background: '+data.button_color+';color:'+data.text+';border:2px solid '+data.scondary+';"><i class="fa '+data.socialMediaList[i]+'" aria-hidden="true" style="margin-top:8px;"></i></a></td>';
                                   }else if(data.socialMediaList[i]=='fa-twitter'){
                                        html+='<td style="height: 50px;width: 50px;text-align: center;"><a href="#" style="font-size: 30px;width: 50px;height: 50px;line-height: 50px;text-align: center;text-decoration: none;border-radius: 25px;display: inline-block;background: '+data.button_color+';color:'+data.text+';border:2px solid '+data.scondary+';"><i class="fa '+data.socialMediaList[i]+'" aria-hidden="true" style="margin-left:0px;margin-top:9px;"></i></a></td>';
                                   }else{
                                        html+='<td style="height: 50px;width: 50px;text-align: center;"><a href="#" style="font-size: 30px;width: 50px;height: 50px;line-height: 50px;text-align: center;text-decoration: none;border-radius: 25px;display: inline-block;background:'+data.button_color+';color:'+data.text+';border:2px solid '+data.scondary+';"><i class="fa '+data.socialMediaList[i]+'" aria-hidden="true" style="margin-top:px;"></i></a></td>';
                                   }
                                      
                                  }
                                 $('#socialMedia').html(html);
                                 $('.logoMainBox').css('background',)
                                //  $(".titleName").css("color",data.primary);
                                 $('h1').css('color',data.heading)
                                //  $("p").css("color",data.text);
                                 $(".calling").css("color",data.text);
                                 $("#list-unstyled").css("background",data.blog_color);
                                 $(".TopBlankbox").css("background",data.scondary);
                                 $(".companyPic").css("background",data.primary);
                                 $(".mainbody").css("background",data.primary);
                         
                                
                              	$('#img').hide(); 
                              		setTimeout(function(){ $('#loader').hide();  }, 4000);
                              	 	dview();
                               		//$('#myModal').show();
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                   
      
         });
           
         $('button').click(function(){
             $('#myModal').hide(); 
         });   
            
            $('.finish-btn').click(function(){
                 $('form').attr('action', "{{route('admin.Cards.generate_pdf6')}}");
                 $('form').attr('target','popup');
                 var urrl ="{{route('admin.Cards.generate_pdf6')}}";
                 $('form').attr(' onclick',"window.open("+urrl+",'width=800,height=800');");
            });
            
            function view(){
            
                $.ajax({
                    url:"{{route('admin.Cards.delete-image')}}",
                    method:"get",
                    success: function(data) {
                        console.log(data);
                    }
                    
                });
            }
            
            
            
// global variable
 
    function dview() {
         var element = $("#html-content-holder"); // global variable
       var getCanvas; 

         html2canvas(element, {
              x: 50, y: 100,
              draggable: true,
         onrendered: function (canvas) {
                 
                $("#previewImage").html(canvas);
                getCanvas = canvas;
                
             }
         });
             
         $('#frame').show(); 
        $('#wrapper').hide(); 
        $('#img').hide(); 
       
    }
    //$('#img').hide(); 
   // $('#frame').hide(); 
  //$('#wrapper').hide();  
 
 
      //("#Secondary option[value="+ color +"]").prop('disabled', true);              
    //   $("#text option[value=" + color + "]").prop('disabled', true);
    //     $("#Primary option[value="+ color +"]").prop('disabled', true);
   
    $("#Primary").change(function(){
       var color=$("#Primary").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', true);
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', false);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
    }) ;   
        
  $("#Secondary").change(function(){
       var color=$("#Secondary").val();
       $("#heading1 option[value=" + color + "]").prop('disabled', true);
       $("#heading option[value=" + color + "]").prop('disabled', true);
       $("#Secondary option[value="+ color +"]").prop('disabled', false);
       $("#Primary option[value="+ color +"]").prop('disabled', true);
       $("#text option[value=" + color + "]").prop('disabled', true);
    }) ;   
    
    $("#text").change(function(){
       var color=$("#text").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', true);
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', false);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
         $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
    
     $("#heading").change(function(){
       var color=$("#heading").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', true);
        $("#heading option[value=" + color + "]").prop('disabled', false);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
        
        
         $("#heading1").change(function(){
       var color=$("#heading1").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', false);
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
         $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
         
        
        
        // $('.sf-controls').css("display","none");
        
          $('input[type="color"]').change(function(){
             var select1=$('#Primary').val();
             var select2=$('#Secondary').val();
             var select3=$('#text').val();
             var select4=$('#heading').val();
             var select5=$('#button_color').val();
             if(select1!=='Choose Color' && select2!=='Choose Color' && select3!=='Choose Color' && select4!=='Choose Color' && select5!=='Choose Color'){
                 $('.sf-controls').css("display","block");
                   $('#active1').attr('class','active');
             }
            
        });
     
    //  $('.next-btn').click(function(){
    //           $('.sf-controls').css("display","none");
    //     });   
        
        
         $('.socialLinksboxx').on("focusout","input",function(){
             
           
             var input1=$('input[name="social_media[1]"]').val();
             var input2=$('input[name="social_media[2]"]').val();
             var input3=$('input[name="social_media[3]"]').val();
             var input4=$('input[name="social_media[4]"]').val();
             var input5=$('input[name="social_media[5]"]').val();
             
             if(input1!=='' && input2!=='' && input3!=='' && input4!=='' && input5!==''){
                   $('.sf-controls').css("display","block");
                     $('#active2').attr('class','active');
             }else{
                   $('.sf-controls').css("display","none");
                   $('.col').text('*Please fill all the boxes');
             }
            
        });
        
        
        
         $('.panel-group').on("focusout","input",function(){
            
             var first_name1=$('input[name="first_name1"]').val();
             var last_name1=$('input[name="last_name1"]').val();
             var phone1=$('input[name="phone1"]').val();
             var first_name2=$('input[name="first_name2"]').val();
             var last_name2=$('input[name="last_name2"]').val();
             var phone2=$('input[name="phone2"]').val();
             var service_name_1=$('input[name="service-name-1"]').val();
             var service_name_2=$('input[name="service-name-2"]').val();
             var service_name_3=$('input[name="service-name-3"]').val();
             var service_name_4=$('input[name="service-name-4"]').val();
             
                              if(first_name1.length <31){
                                
                                  first_name1=first_name1;
                              }else{
                                  $('.first_name1').text('First Name 1 max 30 Char');
                                    // alert('*First Name max 30 Char');
                              }
                              
                              if(first_name2.length <31){
                                
                                  first_name2=first_name2;
                              }else{
                                  $('.first_name2').text('First Name 2 max 30 Char');
                                    // alert('*First Name max 30 Char');
                              }
                              
                               
                              
                              if(last_name1.length <31){
                                 
                                  last_name1=last_name1;
                              }else{
                                   $('.last_name1').text('Last Name2 max 30 Char');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              if(last_name2.length <31){
                                 
                                  last_name2=last_name2;
                              }else{
                                   $('.last_name2').text('Last Name 2 max 30 Char');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_1.length <70){
                                 
                                   service_name_1= service_name_1;
                              }else{
                                   $('.service-name-1').text('Service Name 1 max 70 Char');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_2.length <70){
                                 
                                   service_name_2= service_name_2;
                              }else{
                                   $('.service-name-2').text('Service Name 2 max 70 Char');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_3.length <70){
                                 
                                   service_name_3= service_name_3;
                              }else{
                                   $('.service-name-3').text('Service Name 3 max 70 Char');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              if(service_name_4.length <70){
                                 
                                   service_name_4= service_name_4;
                              }else{
                                   $('.service-name-4').text('Service Name 4 max 70 Char');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              
                              
                            
                             
                              if(phone1!==''){
                              if(validatePhone(phone1)==true){
                                 
                                  phone1=phone1;
                                  $('.phone1').css('color','green');
                                  $(".phone1").text('*Phone Valid..!');
                              }else{
                                   //alert('Is not Valid..!');
                                     $('.phone1').css('color','red');
                                   $('.phone1').text('*Is not Valid Phone No... only no!');
                              }
                              }
                              
                              if(phone2!==''){
                              if(validatePhone(phone2)==true){
                                 
                                  phone2=phone2;
                                  $('.phone2').css('color','green');
                                  $(".phone2").text('*Phone Valid..!');
                              }else{
                                   //alert('Is not Valid..!');
                                     $('.phone2').css('color','red');
                                   $('.phone2').text('*Is not Valid Phone No... only no!');
                              }
                              }
                             
             if(first_name1!=='' && last_name1!==''  && phone1!==' '&& first_name2!=='' && last_name2!==''  && phone2!=='' && service_name_1!=='' && service_name_2!=='' && service_name_3!=='' && service_name_4!==''){
                 // alert(first_name+" "+last_name+" "+email+" "+designation+"  "+about+" "+phone+" "+address+" "+website);
                 $('.sf-controls').css("display","block");
             }else{
                  $('.sf-controls').css("display","none");
             }
            
        });
        
        
        function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
          if (regex.test(email)) {
                 return true;
               }
            else {
                  return false;
             }
        }
        
        
     function validatePhone(Phone) {
    
    var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (filter.test(Phone)) {
        return true;
    }
    else {
        return false;
    }
}
                    
});
    </script>
    
   
    
    
@endsection