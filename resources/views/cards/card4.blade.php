<html>
        <!--Core CSS -->
      <link href="{{ url('bootstrap-4/css/bootstrap.min.css')}}" rel="stylesheet">
        <link href="{{ url('bootstrap-4/css/font-awesome.min.css')}}" rel="stylesheet">
    
     
     
     
        <style>
            


/* Aurthor: Hardeep Singh */
@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #3055c6; }
::selection { color: #f1f1f1;background: #3055c6; }



body  {font-family: 'Poppins', sans-serif;
/*background: #2a53c1;*/
}
.wrapper {width: 400px;margin: 0 auto;height: 800px;background: #ecf2fb;}


.profileboxx {text-align: center;
/*background-image: url(web_assets/img/bgimg.jpg);*/
height: 315px;
color: #fff;
    
}
.profileboxx .innerbox {padding: 40px 0 0 0;}
.profileboxx .innerbox .companyName {    text-transform: uppercase;
    font-size: 24px;
     font-weight: normal;
    /*font-weight: 600;*/
    letter-spacing: 1px;}
.profileboxx .innerbox .userimg  {     border: 3px solid #fff;
    width: 114px;
    height: 114px;
    margin: 15px auto;
    border-radius: 100px;}
.profileboxx .innerbox .userimg img {    border-radius: 100px;
    width: 100%;}

.profileboxx .innerbox .name  {    font-size: 20px;
    /*font-weight: 600;*/
    
}



.information {margin: -40px 0 0 0;}
.information ul {
    display: table;
    margin: 0 auto;
    margin-left:10px;
/*background: #fff;*/
width: 84%;border-radius: 10px;padding: 10px;}
.information ul li {margin: 0 0 10px 0;}
.information ul li:last-child {margin: 0;}
.information ul li:after {display: block;clear: both;content: "";}
.information ul li .ledtname{float: left;width: 25%;    margin: 0;    font-size: 13px;color: #6e7179; margin-left:-20px; font-weight:none}
.information ul li .rightname{
    float: left;
width: 100%;margin: 0;font-size: 13px;color: #000;}


.servicesBoxx {margin: 15px 0 15px 0;}
.servicesBoxx h4{    width: 80%;
    display: table;
    margin: 0 auto 15px;
    font-size: 14px;
   font-weight: normal;
    color: #6e7179;}
.servicesBoxx ul {    display: table;
    margin: 0 auto;
    /* background: #fff; */
    width: 80%;
    border-radius: 10px;
    /* padding: 30px; */
    text-align: center;}
.servicesBoxx ul li {margin: 0 0 10px 0;}
.servicesBoxx ul li:last-child {margin: 0;}
.servicesBoxx ul li:after {display: block;clear: both;content: "";}
.servicesBoxx ul li .ledtname{    float: left;
    width: 47%;
    margin: 0;
    font-size: 14px;
    color: #fff;
    background: #3055c6;
    border-radius: 50px;
    padding: 10px 0;}
.servicesBoxx ul li .rightname{    float: right;
    width: 47%;
    margin: 0;
    font-size: 14px;
    color: #fff;
    /* margin-left: 10%; */
    background: #3055c6;
    padding: 10px 0;
    border-radius: 50px;}


.socialMediaIcons {    display: table;
    margin:20px auto;
    /* background: #fff; */
    width: 85%;}
.socialMediaIcons ul{margin: 0;}
.socialMediaIcons ul li {display: inline-block;margin: 0 7px;}
.socialMediaIcons ul li a {  
 
    font-size: 30px;
    width: 50px;
    height: 50px;
    line-height: 50px;
    text-align: center;
    text-decoration: none;
    border-radius: 50%;
    display: inline-block;
    background: #ecf2fb;
    color: #2d6ff7;

}
.socialMediaIcons ul li a .fa {
    font-size: 28px;
}

.socialMediaIcons ul li a .fa:hover {
    opacity: 0.7;
}

.address {color: #2b53c2;font-size: 13px;text-align: center; font-weight:none;}






















/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


}


        </style>
        
         <body  style="padding-bottom:-150px;">


       <section class="wrapper" style="background:{{$val['primary']}}">
           
           <table style="width:100%;">
               <tr>
                   <th style="height:400px" >
                       @if($val['background_image']!==null)
                           <img src="{{ $val['background_image']}}" style="width:400px;height:100%;opacity:0.8;background:black;">
                       @else
                           <img src="{{ url('web_assets/design-image/design-4/bgimg.jpg')}}" style="width:400px;height:100%;opacity:0.8;background:black;">
                       @endif
                    
                       
                        <div class="profileboxx" style="margin-top:-250px;">
                            
                            <div class="innerbox" style="margin-top:-125px;">
                             
                                <h2 class="companyName" style="color:{{$val['scondary']}};font-style:bold;">{{$val['company_name']}}</h2>
                                <p style="border: 3px solid {{$val['scondary']}};width: 98px;height: 98px;margin: 15px auto;border-radius: 50px;">
                                  
                            @if($val['profile_image']!==null)
                            <img class="" src="{{ $val['profile_image']}}" style="width:100px; height:100px;border-radius: 50px;margin-top:-1px;" alt="profilepix">
                              @else
                            <img class="" src="{{ url('web_assets/design-image/design-4/profilepix.jpg')}}" style="width:100px; height:100px;border-radius: 50px;margin-top:-1px;" alt="profilepix">
                          @endif
                                     
                                </p>
                                <p class="name" style="color:{{$val['scondary']}};">{{$val['first_name']}} {{$val['last_name']}}</p>
                            </div>
                            
                            <div style="width:100%;height:140px;margin-top:0px;">
                                 <div style="width:100%;background:transparent;height:80px;"></div>
                                  <div style="width:100%;background:{{$val['primary']}};height:80px;"></div>
                            </div>
                      
                        <div class="information" style="border-radius:20px;margin-left:30px;width:280px;margin-top:0px;margin-top:-100px;padding:20px 0px 20px 0px;padding-left:50px;background:{{$val['scondary']}}" >
                            <ul class="list-unstyled"  style="margin: 0 auto;border-radius: 10px;list-style: none;text-align:left;margin-top:-80px;" >
                                <li>
                                    <p class="ledtname" style="color:{{$val['heading']}};">Contact</p>
                                    <p class="rightname" style="color:{{$val['text']}};">&nbsp;&nbsp;<a href="tel:{{$val['phone']}}" style="color:{{$val['text']}};">{{$val['phone']}}</a> </p>
                                </li>
                                {{--
                                  <li>
                                    <p class="ledtname" style="color:{{$val['heading']}};">WhatsApp</p>
                                    <p class="rightname" >&nbsp;&nbsp;<a href="https://wa.me/{{$val['whats_no']}}" style="color:{{$val['text']}};">{{$val['code']}}-{{$val['whats_no']}}</a> </p>
                                --}}
                                </li>
                                @if($val['website3']!=='')
                                 <li>
                                    <p class="ledtname" style="color:{{$val['heading']}};">Website</p>
                                    <p class="rightname" style="color:{{$val['text']}};">&nbsp;&nbsp;<a href="{{$val['website1']}}" style="color:{{$val['text']}};">{{$val['website3']}}</a></p>
                                </li>
                                @endif
                                 <li>
                                    <p class="ledtname" style="color:{{$val['heading']}};">Email</p>
                                    <p class="rightname" style="color:{{$val['text']}};">&nbsp;&nbsp;<a href="mailto:{{$val['email']}}" style="color:{{$val['text']}};">{{$val['email']}}</a></p>
                                </li>
                               
                               
                              
                                
                            </ul>
                        </div>
                     </div>
                   </th>
               </tr>
               
               <tr>
                   <th style="height:250px;">
                       <div style="height:100px;margin-top:-150px;">
                            <div class="servicesBoxx" style="width:340px;margin-left:30px;" >
                                <h4 style="font-size: 16px;margin-top:0px;color:{{$val['heading']}};">Services</h4>
                              
                                <ul class="list-unstyled" style="padding-top:10px;">
                                     <li>
                                        <p class="ledtname" style="height:20px;width:165px;border-radius:20px;background:{{$val['blog_color']}};color:{{$val['scondary']}};">{{$val['services5']}}</p>
                                        <p class="rightname"  style="height:20px;width:165px;border-radius:20px;background:{{$val['blog_color']}};color:{{$val['scondary']}}">{{$val['services6']}}</p>
                                    </li>
                                    
                                      <li>
                                        <p class="ledtname"  style="height:20px;width:165px;border-radius:20px;background:{{$val['blog_color']}};color:{{$val['scondary']}};">{{$val['services3']}}</p>
                                        <p class="rightname"  style="height:20px;width:165px;border-radius:20px;background:{{$val['blog_color']}};color:{{$val['scondary']}};">{{$val['services4']}}</p>
                                    </li>
                                    <li>
                                        <p class="ledtname"  style="height:20px;width:165px;border-radius:20px;background:{{$val['blog_color']}};color:{{$val['scondary']}};">{{$val['services1']}}</p>
                                        <p class="rightname"  style="height:20px;width:165px;border-radius:20px;background:{{$val['blog_color']}};color:{{$val['scondary']}};">{{$val['services2']}}</p>
                                    </li>
                                   
                                  
                                </ul>
                            </div>
                           
                       </div>
                       
                   </th>
               </tr>
               
               
               <tr>
                   <th style="height:100px">
                         <div style="margin-top:-120px;">
                        <div class="socialMediaIcons" >
                            <ul class="list-unstyled" style="margin-left:30px;">
                           @if(isset($val['social_media']))   
                             @foreach($val['social_media1'] as $key=>$media)
                                <li style="margin-left:6px;background:transparent ;height:50px; width:50px;">
                                    <a href="{{$media}}" style="background:transparent;border:1px solid {{$val['scondary']}};border-radius:26px;opacity:0.9;" target="_blank">
                                        
                                         <i class="fa {{$val['socialMediaList'][$key]}}" aria-hidden="true" style="background:{{$val['primary']}}; color:{{$val['blog_color']}}; margin-left:14px;"></i>
                                       
                                    </a>
                                </li>
                            @endforeach
                           @endif
                               
                            </ul>
                        </div>
                        <div class="address" style="color:{{$val['blog_color']}}">
                           <a href="{{$val['location']}}"> {{$val['address']}} </a>
                       </div>
                        </div>
                        
                   </th>
               </tr>
               
               
               
           </table>



        </section>



       
   


    </body>
    
    </html>