@extends('frontend.layouts.master')
@section('css')
         <link href="{{ url('web_assets/css/form-wizard.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/css/dropify.css') }}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/bootstrap.min.css')}}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/font-awesome.min.css')}}" rel="stylesheet">
         <link href="{{ url('web_assets/card-css/css/main.css')}}" rel="stylesheet">
       
    <style>
        .text-muted1 {
     color: red;
        }
        .progressbar li {
         
        }
        
        #loader { 
      border: 12px solid #f3f3f3; 
      border-radius: 50%; 
      border-top: 12px solid #444444; 
      width: 70px; 
      height: 70px; 
      animation: spin 1s linear infinite; 
    } 
    
    @keyframes spin { 
      100% { 
        transform: rotate(360deg); 
      } 
    } 
    
    .center { 
      position: absolute; 
      top: 0; 
      bottom: 0; 
      left: 0; 
      right: 0; 
      margin: auto; 
    } 
    

    </style>     
    
       
@section('content')
    <!-- BACK BUTTON STARTS HERE -->


    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="backBtn">
                        <a class="btnCustomStyle2 btn-solid" href="index.html">
                            <span>Back</span>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- BACK BUTTON END HERE -->

    <section class="upperCircles">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="progressbar list-unstyled" style="width: 100%;">
                        <li class="active" style=" width: 33%;"></li>
                        <li id="active1"  style=" width: 33%;"></li>
                        <li id="active2"  style=" width: 33%;"></li>
                       
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- FORM WIZARD STARTS HERE -->
    <section class="mainFormWizard">
        <div class="container-fluid">
            <span id="theme_name" style="display: none;">sky</span>
            <span id="trans_name" style="display: none;">slide</span>
            <span id="rtl" style="display: none;">0</span>
            <div class="row">
                <div class="col-md-9">
    <ul>
            @foreach ($errors->all() as $error)
           
              <li> <div class="alert alert-warning">
               <strong>Warning!</strong> {{ $error }}
              </div></li>
            @endforeach
        </ul>
                    <form id="wizard_example_2"  method="post" enctype="multipart/form-data" >
                       
                        @csrf
                        <fieldset>
                            <legend>Color</legend>
                            <div class="row">
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Primary Color</label>
                                             <input type="color" class="form-control" id="Primary" name="primary" style="height:80px;width:200px;" value="">
                                           
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Secondary Color</label>
                                             <input type="color" class="form-control" id="Secondary" name="scondary" style="height:80px;width:200px;" value="">
                                        
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Text Color</label>
                                               <input type="color" class="form-control" id="text" name="text" style="height:80px;width:200px;" value="">
                                         
                                        </div>
                                    </div>
                                </div>
                                
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Heading Color</label>
                                        <input type="color" class="form-control" id="heading" name="heading_color" style="height:80px;width:200px;" value="">
                                          
                                           
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Blog Text Color</label>
                                             <input type="color" class="form-control" id="heading2" name="blogtext_color" style="height:80px;width:200px;" value="">
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Facebook Color</label>
                                             <input type="color" class="form-control" id="heading1" name="facebook_color" style="height:80px;width:200px;" value="">
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Skype Color</label>
                                             <input type="color" class="form-control" id="heading1" name="skepe_color" style="height:80px;width:200px;" value="">
                                        </div>
                                       
                                    </div>
                                </div>
                                
                                 <div class="col-lg-4">
                                    <div class="form-group">
                                        <div class="firstColor">
                                            <label>Linkedin Color</label>
                                             <input type="color" class="form-control" id="heading1" name="linkedin_color" style="height:80px;width:200px;" value="">
                                        </div>
                                       
                                    </div>
                                </div>
                                
                               
                                
                                
                                
                               
                            </div>
                        </fieldset>
                      
                        <fieldset>
                            <legend>Social Links</legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="col" style="color:red;font-size:16px;"></span>
                                    <ul class="list-unstyled socialLinksboxx">
                                        @foreach($socialMedia as $social)
                                         @if($social['icon']!=='fa-linkedin')
                                            <li class="clearfix">
                                                 <span class="titleSocial" >
                                                     <i class="fa {{$social['icon']}}" aria-hidden="true"></i>{{$social['name']}}
                                                 </span>
                                                 @if($social['icon']=='fa-whatsapp')
                                              
                                                 <select class="form-control" name="code" style="width: 78px; height: 29px;float: left;margin-top: 0px;font-size: 12px;margin-left: 63px;" id="code">
                                                    @foreach($va as $va1)
                                                   <option value="{{$va1->callingCode}}">{{$va1->callingCode}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$va1->country}} </option>
                                                    @endforeach
                                                </select>
                                                   @endif
                                                 <span class="inputwithSubmit"  id="textfield_{{$social['id']}}">
                                                        <input type="text" name="social_media[{{$social['id']}}]" class="form-control" placeholder="Enter profle link...">
                                                 </span>
                                            </li>
                                            @endif
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </fieldset>
                        
                        
                      <fieldset>
                            <legend>Profile </legend>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                                        
                                              <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne1">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                        Image Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne1">
                                                
                                                <div class="panel-body personalBoxxx">
                                                    <div class="row">
                                                        
                                                        
                                                          <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Profile Image</label>
                                                                    <input type="file" name="profile_image" id="input-file-now" class="dropify" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <div class="uploadBackground">
                                                                    <label>Logo Image</label>
                                                                    <input type="file" name="logo" id="input-file-now1" class="dropify" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingOne">
                                                <h4 class="panel-title">
                                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne1" aria-expanded="true" aria-controls="collapseOne">
                                                        Personal Information
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapseOne1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                                
                                                <div class="panel-body personalBoxxx">
                                                    <div class="row">
                                                        
                                                        
                                                          
                                                        
                                                        
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" name="first_name" placeholder="First Name 1">
                                                                <small class="text-muted1 first_name1"></small>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-12 col-lg-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Enter Last Name1" name="last_name">
                                                                <small class="text-muted1 last_name"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" name="email" class="form-control" placeholder="Enter Email" >
                                                                    <small class="text-muted1 email"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        <div class="col-md-6">
                                                            <div class="form-group ">
                                                                <input type="text" class="form-control" placeholder="Designation" name="designation" maxlength="40">
                                                                 <small class="text-muted1 designation"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        
                                                        <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <input type="text" name="phone" class="form-control" placeholder="Phone Number " maxlength="10">
                                                                    <small class="text-muted1 phone1"></small>
                                                            </div>
                                                        </div>
                                                        
                                                        <!-- <div class="col-md-12 col-lg-6 ">-->
                                                        <!--    <div class="form-group">-->
                                                        <!--        <input type="text" name="bottom_title" class="form-control" placeholder="Bottom Title " maxlength="12">-->
                                                        <!--            <small class="text-muted1 bottom_title"></small>-->
                                                        <!--    </div>-->
                                                        <!--</div>-->
                                                          <div class="col-md-12 col-lg-12 ">
                                                            <div class="form-group">
                                                                <textarea type="text" name="about" id="about" class="form-control" placeholder="Enter About " maxlength="100"></textarea>
                                                                    <small class="text-muted1 about"></small>
                                                            </div> 
                                                        </div>
                                                      
                                                       <div class="col-md-12 col-lg-12 ">
                                                            <div class="form-group">
                                                                <textarea type="text" name="address" id="address" class="form-control" placeholder="Enter Address " maxlength="78"></textarea>
                                                                    <small class="text-muted1 address"></small>
                                                            </div> 
                                                        </div>
                                                      
                                                     
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
           
                       
                                        <div class="panel panel-default">
                                            <div class="panel-heading" role="tab" id="headingfour">
                                                <h4 class="panel-title">
                                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                                                        Service Name
                                                    </a>
                                                </h4>
                                            </div>
                                            <div id="collapsefour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingfour">
                                                <div class="panel-body businessInformation">

                                                    <div class="row">

                                                       
                                                        
                                                     
                                                        
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <label>Service Name 1</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service, Name 1" name="service-name-1" maxlength="25">
                                                                 <small class="text-muted1 service-name-1"></small>
                                                            </div>
                                                        </div>
                                                      
                                                        
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <label>Service Name 2</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service, Name 2" name="service-name-2" maxlength="25">
                                                                 <small class="text-muted1 service-name-2"></small>
                                                            </div>
                                                        </div>
                                                       
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <label>Service Name 3</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service, Name 3" name="service-name-3" maxlength="25">
                                                                 <small class="text-muted1 service-name-3"></small>
                                                            </div>
                                                        </div>
                                                       
                                                        
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <label>Service Name 4</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service, Name 4" name="service-name-4" maxlength="25">
                                                                 <small class="text-muted1 service-name-4"></small>
                                                            </div>
                                                        </div>
                                                        
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <label>Service Name 5</label>
                                                                <input type="text" class="form-control" placeholder="Enter Service, Name 5" name="service-name-5" maxlength="25">
                                                                 <small class="text-muted1 service-name-5"></small>
                                                            </div>
                                                        </div>
                                                       
                                                        
                                                         <div class="col-md-12 col-lg-6 ">
                                                            <div class="form-group">
                                                                <label>Service Name 6</label>
                                                                <input type="text" class="form-control" placeholder="Enter  Service, Name 6" name="service-name-6" maxlength="25">
                                                                 <small class="text-muted1 service-name-6"></small>
                                                            </div>
                                                        </div>
                                                       
                                                       
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                             
                                    </div>
                                </div>
                            </div>
                        </fieldset>
                            <div class="col-md-12">
                                <p class="backBtn">
                                    <a class="btnCustomStyle2 btn-solid" href="javascript:void(0);">
                                        <span>Preview</span>
                                    </a>
                                </p>
                            </div>
                          
                       </form>
                    </div>
                <div class="col-md-3">
                    
                    <div id="frame" style="background-image: url('{{ url("web_assets")}}/img/Phone1.png');background-color: #cccccc;background-position: center;background-repeat: no-repeat;background-size: cover;
  position: relative;width: 263px;height: 528px;border-radius: 30px;"></br>
                         <div id="previewImage" ></div>
                         <div id="loader" class="center" ></div> 
                          <div id="img" style="width: 235px;border-radius: 30px;margin-left: 15px;margin-top: -9px;">
                         <img class="img-fluid mx-auto d-block "  src="{{ url('web_assets')}}/img/desine-8.png" alt="" style="border-radius: 15px;height:500px;">
                    </div>
                  
                    </div>
                   
      
                   
                   
                </div>
                
    <style>



/* Aurthor: Hardeep Singh */
/*@import url('https://fonts.googleapis.com/css2?family=Poppins:wght@100;200;300;400;500;600;700;800;900&display=swap');*/

/* GLOBAL */
::-moz-selection { color: #f1f1f1;background: #3055c6; }
::selection { color: #f1f1f1;background: #3055c6; }



body  {font-family: 'Poppins', sans-serif;background: #eae7f0;}
.wrapper {width: 400px;margin: 0 auto;height: 820px;background: #fff;}

.mainboxx {background: #317ef8;height: 100%;overflow: hidden;color: #fff;}



.logo {margin: 15px 0 0 0;}


.profileboxx {padding: 0 40px;margin: 30px 0 0 0;}
.galleryImages ul:after {display: block;clear: both;content: "";}
.galleryImages > ul > li {display: inline-block;float: left;width: 40%;margin: 0 5% 0 0;}
.galleryImages > ul > li:last-child {margin:0;width: 55%;}
.galleryImages > ul > li .pic-1{margin: 0;border-radius: 15px;}
.galleryImages > ul > li .pic-1 img {border-radius: 15px;width: 100%;height:168px;}
.galleryImages > ul > li .pic-2 {text-align: left;margin: 40px -5px 45px 8px;}
.galleryImages > ul > li .pic-2 p { margin: 0;font-size: 20px;font-weight: 600;}
.galleryImages > ul > li .pic-2 small {font-size: 12px;color: #bed6fd;}
.galleryImages > ul > li .pic-3{margin-top: -18px;}

.socialMediaIcons { }
.socialMediaIcons ul{}
.socialMediaIcons ul li {display: inline-block;margin: 0 2px;}
.socialMediaIcons ul li a {text-align: center;font-size: 30px;width: 50px;height: 50px;line-height: 50px;text-decoration: none;border-radius: 16px;display: block;}
.socialMediaIcons ul li a.yellowColor {background: #fff4e2;color: #ffbc4e;}
.socialMediaIcons ul li a.redColor {background: #ffebef;color: #ff3b62;}
.socialMediaIcons ul li a.greenColor {background: #e6fff4;color: #39de96;}
.socialMediaIcons ul li a .fa {font-size: 26px; margin-top:12px;}
.socialMediaIcons ul li a .fa:hover {opacity: 0.7;}



.abtEmailboxx {
    color: #3f455a;
    padding: 0 40px;
    margin: 25px 0;
}
.abtEmailboxx .aboutsec {}
.abtEmailboxx .aboutsec h4 {font-size: 12px;
    color: #abb1c7;}
.abtEmailboxx .aboutsec p {font-size: 14px;
    color: #3f455a;    font-weight: 600;}

.abtEmailboxx .emailsec {}
.abtEmailboxx .emailsec h4 {font-size: 12px;
    color: #abb1c7;}
.abtEmailboxx .emailsec p {font-size: 14px;
    color: #3f455a;   }


.servicesBoxx {    margin: 15px 0;
    padding: 0 40px;}
.servicesBoxx ul {}
.servicesBoxx ul li {margin: 0 0 10px 0;}
.servicesBoxx ul li:last-child {margin: 0;}
.servicesBoxx ul li:after {display: block;clear: both;content: "";}
.servicesBoxx ul li .ledtname{float: left;
    width: 47%;
    margin: 0;
    font-size: 14px;
    color: #fff;
    background: #317ef8;
    border-radius: 15px;
        padding: 10px 15px;}
.servicesBoxx ul li .ledtname h4 {font-size: 12px;font-weight: 600;    margin: 0;}
.servicesBoxx ul li .ledtname p {font-size: 18px;font-weight: 600;    margin: 0;}

.servicesBoxx ul li .rightname{    float: right;
    width: 47%;
    margin: 0;
    font-size: 14px;
    color: #fff;
    background: #317ef8;
        padding: 10px 15px;
    border-radius: 15px;}
.servicesBoxx ul li .rightname h4 {font-size: 12px;font-weight: 600;    margin: 0;}
.servicesBoxx ul li .rightname p {font-size: 18px;font-weight: 600;    margin: 0;}




.bottomIcons {padding: 0 40px;
    margin: 60px 0 0 0;}
.bottomIcons ul{margin: 0;}
.bottomIcons ul li.phone {display: inline-block;width: 20%;   float: left;}
.bottomIcons ul li.phone a {font-size: 30px;width: 60px;height: 60px;line-height: 65px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;    background: #003c9c;color: #fff;}
.bottomIcons ul li.phone a .fa {font-size: 32px; margin-top:13px;}
.bottomIcons ul li.phone a .fa:hover {opacity: 0.7;}

.bottomIcons ul li.middelLine {    float: left;     margin: 7px 0 0 0;   width: 60%;}
.bottomIcons ul li.middelLine p {     font-size: 14px; text-align: center;}

.bottomIcons ul{margin: 0;}
.bottomIcons ul:after {display: block;clear: both;content: "";}
.bottomIcons ul li.whatsApp {display: inline-block;width: 20%;      text-align: right; float: right;}
.bottomIcons ul li.whatsApp a {  font-size: 30px;width: 60px;height: 60px;line-height: 60px;text-align: center;text-decoration: none;border-radius: 30px;display: inline-block;    background: #003c9c;color: #fff;}
.bottomIcons ul li.whatsApp a .fa {font-size: 32px; margin-top:13px;}
.bottomIcons ul li.whatsApp a .fa:hover {opacity: 0.7;}














/* Media Queries */

@media (min-width: 992px) and (max-width: 1199px) {


}

@media (min-width: 768px) and (max-width: 991px) {



}



@media (min-width: 480px) and (max-width: 767px) {

}





@media (min-width: 150px) and (max-width: 479px) {


}


        </style>
     
    <div style="opacity:0;">      
<div id="html-content-holder">
    
          <section class="wrapper" id="wrapper">

            <div class="mainboxx">
                
                <div class="logo">
                    
                </div>

                <div class="profileboxx">
                    <div class="galleryImages">
                        <ul class="list-unstyled">
                            <li>
                                <p class="pic-1">
                                    
                                </p>
                            </li>
                            <li>
                                <div class="pic-2">
                                    <p></p>
                                    <small></small>
                                </div>
                                <div class="pic-3">
                                    <div class="socialMediaIcons">
                                        <ul class="list-unstyled">
                                           
                                        </ul>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="abtEmailboxx">
                    <div class="aboutsec">
                        
                    </div>
                    <div class="emailsec">
                       
                    </div>
                </div>

                <div class="servicesBoxx">
                    <ul class="list-unstyled">
                       
                       
                    </ul>
                </div>

                <div class="bottomIcons">
                    <ul class="list-unstyled">
                        <li class="phone">
                            
                        </li>
                        <li class="middelLine">
                            <p></p>
                        </li>
                        <li class="whatsApp">
                         
                        </li>
                    </ul>
                </div>



            </div>
        
        </section>
  
    </div> 
  </div>            
            
            
            
        </div>
        

        

        
        
    </section>
    <!-- FORM WIZARD END HERE -->
@endsection

@section('scripts')
    <script src="{{ url('web_assets/js/html2canvas.js') }}"></script>
    <script src="{{ url('web_assets/js/form-wizard.js') }}"></script>
    <script src="{{ url('web_assets/js/jscolor.js') }}"></script>
    <script src="{{ url('web_assets/js/dropify.js') }}"></script>
     
<style>
    canvas {
    height: 505px;
    width: 238px;
    margin-top: -13px;
    border-radius: 27px;
    margin-left: 13px;

    }
</style>
    <script>
        jscolor.presets.default = {
            borderColor:'rgba(236,239,245,0.6)', width:200, height:100,
            buttonColor:'rgba(70,51,255,1)', shadowColor:'rgba(0,0,0,0.05)'
        };
    </script>

    <script>
        $(document).ready(function(){
            	$('#myModal').hide();
            	 $('#h1').hide();
            	 $('#h2').hide();
            	 $('.searchMe').hide();
            	 $('.btnCustomStyle2').hide();
            	 $('#code').val('+91');
            	 $('#loader').hide(); 
            	 $('#wrapper').hide();
             $('.companyPic').hide();
             $('.proprietorName').hide();
            view();
            $('input[type="color"]').change(function(){
              $('.btnCustomStyle2').show();
            });
            // Basic
            $('.dropify').dropify();

            // Translated
            $('.dropify-fr').dropify({
                messages: {
                    default: 'Glissez-déposez un fichier ici ou cliquez',
                    replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                    remove:  'Supprimer',
                    error:   'Désolé, le fichier trop volumineux'
                }
            });

            // Used events
            var drEvent = $('#input-file-events').dropify();

            drEvent.on('dropify.beforeClear', function(event, element){
                return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
            });

            drEvent.on('dropify.afterClear', function(event, element){
                alert('File deleted');
            });

            drEvent.on('dropify.errors', function(event, element){
                console.log('Has Errors');
            });

            var drDestroy = $('#input-file-to-destroy').dropify();
            drDestroy = drDestroy.data('dropify')
            $('#toggleDropify').on('click', function(e){
                e.preventDefault();
                if (drDestroy.isDropified()) {
                    drDestroy.destroy();
                } else {
                    drDestroy.init();
                }
            });
           
            $("[type='file']").focusout(function(){
            
            //      var files =$('#input-file-now').prop('files')[0];
            //   if(files[0]!==''){
            //       if(files[0].size > (1024*1024)){
            //       alert('Please Insert Image Max 2MB.!');
            //       $("#input-file-now").val(null);
            //       }
            //   }
                  
            //       var files1 =$('#input-file-now1').prop('files')[0];
            //   if(files1[0]!==''){
            //       if(files1[0].size > (1024*1024)){
            //       alert('Please Insert Image Max 2MB.!');
            //       $("#input-file-now1").val(null);
            //       }
            //   }
                  
             
               });
               
             $('.btnCustomStyle2').click(function(){
                     $('#wrapper').show();
                  	$('#loader').show();
               var  files =$('#input-file-now').prop('files')[0];
            
              
                var form_data = new FormData($('#wizard_example_2')[0]);
                  form_data.append("file", files);
                    
   
                       $.ajax({
                            type: 'post',
                            url:  "{{route('admin.Cards.cards8')}}",
                            
                            data: form_data,
                            async: false,
                            dataType:'json',
                            success: function (data1) {
                            var    data=data1.va;
                                console.log(data1);
                                if(data.profile_image!=='') {
                                      $('.pic-1').html('<img src="../card/image/'+data.profile_image+'" alt="profilepic" class="img-fluid d-block">');
                                     
                                      
                                }
                                if(data.logo!==''){
                                   
                                     $('.logo').html('<img class="img-fluid d-block mx-auto" src="../card/image/'+data.logo+'" alt="logo">');
                                 }
                                 
                                
                             
                                if(data.first_name!==null && data.last_name!==null){
                                    $('.pic-2 p').css('color',data.blogtext_color);
                                   
                                    $('.pic-2 p').text(data.first_name+' '+data.last_name);
                                   
                                }
                                
                                if(data.designation!==null){
                                   $('.pic-2 small').text(data.designation);
                                   $('.pic-2 small').css('color',data.blogtext_color);
                                }
                                
                                if(data.phone!==null){
                                 
                                    $('.phone').html('<a href="'+data.code+data.phone+'"  style="background:'+data.scondary+';color:'+data.blogtext_color+';"><i class="fa fa-phone" aria-hidden="true"></i></a>');
                                }
                            var html1='';
                                if(data.servicename1[0]!==null && data.servicename1[1]!==null && data.servicename2[0]!==null && data.servicename2[1]!==null && data.servicename1!=='' && data.servicename2!==''){
                                     html1+='<li><div class="ledtname" style="background:'+data.primary+';color:'+data.blogtext_color+';"><h4>'+data.servicename1[0]+'</h4><p>'+data.servicename1[1]+'</p></div><div class="rightname" style="background:'+data.primary+';color:'+data.blogtext_color+';"><h4>'+data.servicename2[0]+'</h4><p>'+data.servicename2[1]+'</p></div></li>';
                                  
                                }
                                 if(data.servicename3[0]!==null && data.servicename3[1]!==null && data.servicename4[0]!==null && data.servicename4[1]!==null && data.servicename3!=='' && data.servicename4!==''){
                                     html1+='<li><div class="ledtname" style="background:'+data.primary+';color:'+data.blogtext_color+';"><h4>'+data.servicename3[0]+'</h4><p>'+data.servicename3[1]+'</p></div><div class="rightname" style="background:'+data.primary+';color:'+data.blogtext_color+';"><h4>'+data.servicename4[0]+'</h4><p>'+data.servicename4[1]+'</p></div></li>';
                                  
                                }
                                
                                 if(data.servicename5[0]!==null && data.servicename5[1]!==null && data.servicename6[0]!==null && data.servicename6[1]!==null && data.servicename5!=='' && data.servicename6!==''){
                                     html1+='<li><div class="ledtname" style="background:'+data.primary+';color:'+data.blogtext_color+';"><h4>'+data.servicename5[0]+'</h4><p>'+data.servicename5[1]+'</p></div><div class="rightname" style="background:'+data.primary+';color:'+data.blogtext_color+';"><h4>'+data.servicename6[0]+'</h4><p>'+data.servicename6[1]+'</p></div></li>';
                                  
                                }
                                
                                
                                $('.servicesBoxx .list-unstyled').html(html1);
                                if(data.about!==null){
                                   $('.aboutsec').html('<h4 style="color:'+data.heading_color+';">About</h4><p style="color:'+data.text+';">'+data.about+'</p>');
                                }
                                if(data.email!==null){
                                   $('.emailsec').html('<h4 style="color:'+data.heading_color+';">Email</h4><p style="color:'+data.text+';">'+data.email+'</p>');
                                }
                                 if(data.address!==null){
                                   $('.middelLine p').text(data.address);
                                   $('.middelLine p').css('color',data.blogtext_color);
                                }
                                

                               
                                  var i;
                                  var html='';
                                  var html2='';
                              
                                 
                                     
                                      
                                   if(data.socialMediaList[3]=='fa-whatsapp'){
                                        if(data.social_media[3]!==null){
                                        html2+='<a href="#" style="background:'+data.scondary+';color:'+data.blogtext_color+';"><i class="fa '+data.socialMediaList[3]+'" aria-hidden="true"></i></a>';
                                        } 
                                   }
                                       if(data.socialMediaList[1]=='fa-facebook'){
                                      if(data.social_media[1]!==null){
                                        html+='<li><a href="#" class="yellowColor" style="background-color:rgba('+data.facebook_color1+',0.1);color:'+data.facebook_color+';"><i class="fa '+data.socialMediaList[1]+'" aria-hidden="true"></i></a></li>';
                                       } 
                                       }
                                       
                                        if(data.socialMediaList[2]=='fa-skype'){
                                       if(data.social_media[2]!==null){
                                        html+='<li><a href="#" class="redColor" style="background-color:rgba('+data.skepe_color1+',0.1);color:'+data.skepe_color+';"><i class="fa '+data.socialMediaList[2]+'" aria-hidden="true"></i></a></li>';
                                        }
                                        }
                                      
                                      if(data.socialMediaList[5]=='fa-twitter'){
                               if(data.social_media[5]!==null){
                                        html+='<li><a href="#" class="greenColor" style="background-color:rgba('+data.linkedin_color1+',0.1);color:'+data.linkedin_color+';"><i class="fa '+data.socialMediaList[5]+'" aria-hidden="true"></i></a></li>';
                                       } 
                                      }
                                      
                                           
                                    
                                  
                                   $('.whatsApp').html(html2);
                               
                                $('.socialMediaIcons .list-unstyled').html(html);
                               
                                 $(".calling").css("color",data.text);
                                 $(".profilePicBoxx").css("background",data.scondary);
                                 $(".mainboxx").css("background",data.primary);
                                  $(".mainboxx").css('background-image', "{{url('/web_assets/img/bg-white.png')}}");
                         
                                
                              	$('#img').hide(); 
                              		setTimeout(function(){ $('#loader').hide();  }, 4000);
                              	 	dview();
                               		//$('#myModal').show();
                            },
                            cache: false,
                            contentType: false,
                            processData: false
                        });
                   
      
         });
           
         $('button').click(function(){
             $('#myModal').hide(); 
         });   
            
            $('.finish-btn').click(function(){
                 $('form').attr('action', "{{route('admin.Cards.generate_pdf8')}}");
                 $('form').attr('target','popup');
                 $('form').attr(' onclick',"window.open('/generate_pdf8','popup','width=800,height=800');");
            });
            
            function view(){
            
                $.ajax({
                    url:"{{route('admin.Cards.delete-image')}}",
                    method:"get",
                    success: function(data) {
                        console.log(data);
                    }
                    
                });
            }
            
            
            
// global variable
 
    function dview() {
         var element = $("#html-content-holder"); // global variable
       var getCanvas; 

         html2canvas(element, {
              x: 50, y: 100,
              draggable: true,
         onrendered: function (canvas) {
                 
                $("#previewImage").html(canvas);
                getCanvas = canvas;
             
             }
         });
             
          $('#frame').show(); 
        $('#wrapper').hide(); 
        $('#img').hide(); 
       
    }
    //$('#img').hide(); 
   // $('#frame').hide(); 
  //$('#wrapper').hide();  
 
 
      //("#Secondary option[value="+ color +"]").prop('disabled', true);              
    //   $("#text option[value=" + color + "]").prop('disabled', true);
    //     $("#Primary option[value="+ color +"]").prop('disabled', true);
   
    $("#Primary").change(function(){
       var color=$("#Primary").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', true);
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', false);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
    }) ;   
        
  $("#Secondary").change(function(){
       var color=$("#Secondary").val();
       $("#heading1 option[value=" + color + "]").prop('disabled', true);
       $("#heading option[value=" + color + "]").prop('disabled', true);
       $("#Secondary option[value="+ color +"]").prop('disabled', false);
       $("#Primary option[value="+ color +"]").prop('disabled', true);
       $("#text option[value=" + color + "]").prop('disabled', true);
    }) ;   
    
    $("#text").change(function(){
       var color=$("#text").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', true);
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', false);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
         $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
    
     $("#heading").change(function(){
       var color=$("#heading").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', true);
        $("#heading option[value=" + color + "]").prop('disabled', false);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
        $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
        
        
         $("#heading1").change(function(){
       var color=$("#heading1").val();
        $("#heading1 option[value=" + color + "]").prop('disabled', false);
        $("#heading option[value=" + color + "]").prop('disabled', true);
        $("#text option[value=" + color + "]").prop('disabled', true);
        $("#Primary option[value="+ color +"]").prop('disabled', true);
         $("#Secondary option[value="+ color +"]").prop('disabled', true);
    }) ;   
         
        
        
         $('.sf-controls').css("display","none");
        
          $('input[type="color"]').change(function(){
             var select1=$('#Primary').val();
             var select2=$('#Secondary').val();
             var select3=$('#text').val();
             var select4=$('#heading').val();
             var select5=$('#button_color').val();
             if(select1!=='Choose Color' && select2!=='Choose Color' && select3!=='Choose Color' && select4!=='Choose Color' && select5!=='Choose Color'){
                 $('.sf-controls').css("display","block");
                   $('#active1').attr('class','active');
             }
            
        });
     
     $('.next-btn').click(function(){
              $('.sf-controls').css("display","none");
        });   
        
        
         $('.socialLinksboxx').on("focusout","input",function(){
             
           
             var input1=$('input[name="social_media[1]"]').val();
             var input2=$('input[name="social_media[2]"]').val();
             var input3=$('input[name="social_media[3]"]').val();
             var input4=$('input[name="social_media[4]"]').val();
             var input5=$('input[name="social_media[5]"]').val();
             
             if(input1!=='' && input2!=='' && input3!=='' && input4!=='' && input5!==''){
                   $('.sf-controls').css("display","block");
                     $('#active2').attr('class','active');
             }else{
                   $('.sf-controls').css("display","none");
                   $('.col').text('*Please fill all the boxes');
             }
            
        });
        
        
        
         $('.panel-group').on("focusout","input",function(){
            
             var first_name=$('input[name="first_name"]').val();
             var last_name=$('input[name="last_name"]').val();
             var phone=$('input[name="phone"]').val();
             var designation=$('input[name="designation"]').val();
             var email=$('input[name="email"]').val();
             var bottom_title=$('input[name="bottom_title"]').val();
             var address=$('#address').val();
             var about=$('#about').val();
             var service_name_1=$('input[name="service-name-1"]').val();
             var service_name_2=$('input[name="service-name-2"]').val();
             var service_name_3=$('input[name="service-name-3"]').val();
             var service_name_4=$('input[name="service-name-4"]').val();
             var service_name_5=$('input[name="service-name-5"]').val();
             var service_name_6=$('input[name="service-name-6"]').val();
             
                              if(first_name.length <31){
                                
                                  first_name=first_name;
                              }else{
                                  $('.first_name').text('First Name  max 30 Char');
                                  $('.first_name').css('color','red');
                                    // alert('*First Name max 30 Char');
                              }
                              
                            
                              
                               
                              
                              if(last_name.length <31){
                                 
                                  last_name=last_name;
                              }else{
                                   $('.last_name').text('Last Name max 30 Char');
                                    $('.last_name').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              
                            
                              
                              if(designation.length <31){
                                 
                                  designation=designation;
                              }else{
                                   $('.designation').text('Designation max 30 Char');
                                    $('.designation').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              
                              if(about.length <100){
                                 
                                  about1=about;
                              }else{
                                   $('.about').text('About max 100 Char');
                                   $('.about').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                           
                              
                               if(address.length <81){
                                 
                                  address1=address;
                              }else{
                                   $('.address').text('Address max 80 Char');
                                   $('.address').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                           
                              
                               if(service_name_1.length <26){
                                 
                                   service_name_1= service_name_1;
                              }else{
                                  
                                   $('.service-name-1').text('Service Name 1 max 25 Char');
                                   $('.service-name-1').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_2.length <26){
                                 
                                   service_name_2= service_name_2;
                              }else{
                                   service_name_2='';
                                   $('.service-name-2').text('Service Name 2 max 25 Char');
                                   $('.service-name-2').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_3.length <26){
                                 
                                   service_name_3= service_name_3;
                              }else{
                                   $('.service-name-3').text('Service Name 3 max 25 Char');
                                   $('.service-name-3').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              if(service_name_4.length <26){
                                 
                                   service_name_4= service_name_4;
                              }else{
                                   $('.service-name-4').text('Service Name 4 max 25 Char');
                                   $('.service-name-4').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_5.length <26){
                                 
                                   service_name_5= service_name_5;
                              }else{
                                   $('.service-name-5').text('Service Name 5 max 25 Char');
                                   $('.service-name-5').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                               if(service_name_6.length <26){
                                 
                                   service_name_6= service_name_6;
                              }else{
                                   $('.service-name-6').text('Service Name 6 max 25 Char');
                                     $('.service-name-6').css('color','red');
                                   //alert('*Last Name max 30 Char');
                              }
                              
                              
                              
                            
                             
                              if(phone!==''){
                              if(validatePhone(phone)==true){
                                 
                                  phone=phone;
                                   $('.phone1').css('color','green');
                                   $(".phone1").text('*Phone Valid..!');
                              }else{
                                   //alert('Is not Valid..!');
                                   $('.phone1').css('color','red');
                                   $('.phone1').text('*Is not Valid Phone No... only no!');
                              }
                              }
                              
                              if(email!==''){
                              if(isEmail(email)==true){
                                 
                                  email1=email;
                                  $('.email').css('color','green');
                                  $(".email").text('* E-mail Valid..!');
                              }else{
                                   //alert('Is not Valid..!');
                                     $('.email').css('color','red');
                                   $('.email').text('* Is not Valid E-mail..!');
                              }
                              }
                             
             if(first_name!=='' && last_name!==''  && phone!=='' && designation!=='' && about1!=='' && email!=='' && address1!=='' && service_name_1!=='' && service_name_2!=='' && service_name_3!=='' && service_name_4!==''){
                   $('.sf-controls').css("display","block");
             }else{
                  $('.sf-controls').css("display","none");
             }
            
        });
        
        
        function isEmail(email) {
          var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          return regex.test(email);
          if (regex.test(email)) {
                 return true;
               }
            else {
                  return false;
             }
        }
        
        
     function validatePhone(Phone) {
    
    var filter = /^((\+[1-9]{1,4}[ \-]*)|(\([0-9]{2,3}\)[ \-]*)|([0-9]{2,4})[ \-]*)*?[0-9]{3,4}?[ \-]*[0-9]{3,4}?$/;
    if (filter.test(Phone)) {
        return true;
    }
    else {
        return false;
    }
}
                    
});
    </script>
    
   
    
    
@endsection