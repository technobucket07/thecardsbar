<!doctype html>
<html lang="en">
@include('admin.layouts.head')
<body>
	<div class="loader" style="display:none;"></div>
  	<div class="wrapper ">
  		@include('admin.layouts.sidebar')
  		@include('admin.layouts.flashmessages')
  		@yield('content')

	</div>
    @include('admin.layouts.footer')
</body>
</html>
