<div class="sidebar" data-color="purple" data-background-color="white" data-image="../assets/img/sidebar-1.jpg">
      <!--
        Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

        Tip 2: you can also add an image using data-image tag
    -->
      <div class="logo"><a href="{{url('/')}}" class="simple-text logo-normal">
          Thecardsbar
        </a></div>
      <div class="sidebar-wrapper custom_sidebar">
        <ul class="nav">
          <li class="nav-item {{ request()->is('admin') ? 'active' : '' }}  ">
            <a class="nav-link" href="{{route('admin.home')}}">
              <i class="fa fa-tachometer" aria-hidden="true"></i>
              Dashboard
            </a>
          </li>
          <li class="nav-item {{ request()->is('admin/users') || request()->is('admin/users/*') ? 'active' : '' }}">
            <a class="nav-link" href="{{route('admin.users.index')}}">
              <i class="fa fa-user" aria-hidden="true"></i>
              Users
            </a>
          </li>
          
          <li class="nav-item {{ request()->is('admin/orders') || request()->is('admin/orders/*') ? 'active' : '' }}">
            <a class="nav-link" href="{{route('admin.orderslist')}}">
              <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
              Orders
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link collapsed text-truncate" href="#submenu1" data-toggle="collapse" data-target="#submenu1">
             <i class="fa fa-caret-square-o-down" aria-hidden="true"></i>
              Cards Category
            </a>
            <div class="side_dropdown collapse {{ request()->is('admin/cardtypes') || request()->is('admin/cardtypes/*') || request()->is('admin/cardscategory') || request()->is('admin/cardscategory/*') || request()->is('admin/cardssubcategory') || request()->is('admin/cardssubcategory/*') ? 'show' : '' }}" id="submenu1" aria-expanded="false">
              <ul class="flex-column pl-2 nav">
                <li class="nav-item {{ request()->is('admin/cardscategory') || request()->is('admin/cardscategory/*') ? 'active' : '' }}">
                  <a class="nav-link py-0" href="{{route('admin.cardscategory.index')}}">
                    <span>Category</span>
                  </a>
                </li>
                <li class="nav-item {{ request()->is('admin/cardssubcategory') || request()->is('admin/cardssubcategory/*') ? 'active' : '' }}">
                  <a class="nav-link py-0" href="{{route('admin.cardssubcategory.index')}}">
                    <span>Subcategory</span>
                  </a>
                </li>
                <li class="nav-item {{ request()->is('admin/cardtypes') || request()->is('admin/cardtypes/*') ? 'active' : '' }}">
                  <a class="nav-link py-0" href="{{route('admin.cardtypes.index')}}">
                    <span>Card types</span>
                  </a>
                </li>
              </ul>
            </div>
          </li>
          <li class="nav-item {{ request()->is('admin/vistingcards') || request()->is('admin/vistingcards/*') ? 'active' : '' }}">
            <a class="nav-link" href="{{route('admin.vistingcards.index')}}">
              <i class="fa fa-id-card-o" aria-hidden="true"></i>
              Visting Card
            </a>
          </li>
          <li class="nav-item {{ request()->is('admin/pages') || request()->is('admin/pages/*') ? 'active' : '' }}">
            <a class="nav-link" href="{{route('admin.pages.index')}}">
              <i class="fa fa-paper-plane" aria-hidden="true"></i>
              Pages
            </a>
          </li>
          <li class="nav-item {{ request()->is('admin/cards') || request()->is('admin/cards/*') ? 'active' : '' }}">
            <a class="nav-link" href="{{route('admin.Cards.cardslist')}}">
              <i class="fa fa-id-badge" aria-hidden="true"></i>
              Cards Design
            </a>
          </li>
          <li class="nav-item {{ request()->is('admin/socialmedias') || request()->is('admin/socialmedias/*') ? 'active' : '' }}">
            <a class="nav-link" href="{{route('admin.socialmedias.index')}}">
              <i class="fa fa-medium" aria-hidden="true"></i>
              Social Medias
            </a>
          </li>
          <li class="nav-item {{ request()->is('admin/contacts') || request()->is('admin/contacts/*') ? 'active' : '' }}">
            <a class="nav-link" href="{{route('admin.contacts.index')}}">
              <i class="fa fa-commenting" aria-hidden="true"></i>
              Feedbacks
            </a>
          </li>
          <!-- <li class="nav-item ">
            <a class="nav-link collapsed text-truncate" href="#submenu2" data-toggle="collapse" data-target="#submenu2">
             <i class="fa fa-caret-square-o-down" aria-hidden="true"></i>
              Pages
            </a>
            <div class="collapse" id="submenu2" aria-expanded="false">
              <ul class="flex-column pl-2 nav">
                <li class="nav-item">
                  <a class="nav-link py-0" href="">
                    <span>About us</span>
                  </a>
                </li>
                <li class="nav-item">
                  <a class="nav-link py-0" href="">
                    <span>Subcategory</span>
                  </a>
                </li>
              </ul>
            </div>
          </li> -->
        </ul>
      </div>
    </div>