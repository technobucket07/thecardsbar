@extends('admin.layouts.app')
@section('content')
<div class="main-panel">
  <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            {{ Form::open(['route' => 'admin.pages.addcontentpost']) }}
            <input type="hidden" name="page_id" value="{{$page->id}}">
            @foreach($pagefields as $pagefield)
              <div class="row">
                <div class="form-group col-md-12">
                  <label for="{{$pagefield->title}}" class="col-form-label">{{$pagefield->title}}</label>
                  {{ Form::textarea($pagefield->title,$pagefield->content,['class'=>'form-control','id'=>$pagefield->title]) }}
                </div>
              </div>
            @endforeach
              <div class="row">
                <div class="form-group col-md-6">
                   {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                </div>
              </div>
          </form>
          </div>
        </div>
      </div>
  </div>
</div>
@endsection

@push('styles')
<style type="text/css">
  ul {
    list-style: none;
  }
  li {
    margin-left: -20px;
  }
</style>
@endpush
@push('scripts')
<script src="{{ asset('dashboard/assets/js/tinymce.min.js') }}"></script>
<script type="text/javascript">
  tinymce.init({
  selector: 'textarea',
  plugins: 'print preview paste importcss searchreplace autolink autosave save directionality code visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists wordcount imagetools textpattern noneditable help charmap quickbars emoticons',
});
</script>
@endpush