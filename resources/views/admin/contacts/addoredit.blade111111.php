@extends('admin.layouts.app')
@section('content')
<div class="main-panel">
  <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
<<<<<<< HEAD
            @if(isset($user))
              {{ Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'patch']) }}
          @else
              {{ Form::open(['route' => 'admin.users.store']) }}
          @endif
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="name" class="col-form-label">Name</label>
                    {{ Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Enter name','id'=>'name']) }}
                    @if($errors->has('name'))
                        <div class="error">{{ $errors->first('name') }}</div>
                    @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="email" class="col-form-label">Email</label>
                  {{ Form::text('email',old('email'),['class'=>'form-control','placeholder'=>'Enter email','id'=>'email']) }}
                  @if($errors->has('email'))
                      <div class="error">{{ $errors->first('email') }}</div>
                  @endif
                </div>

                <div class="form-group col-md-6">
                  <label for="phone" class="col-form-label">Phone</label>
                  {{ Form::text('phone',old('phone'),['class'=>'form-control','placeholder'=>'Enter phone number','id'=>'phone']) }}
                  @if($errors->has('phone'))
                      <div class="error">{{ $errors->first('phone') }}</div>
                  @endif
                </div>

              @if(isset($user))
                  <div class="form-group col-md-6">
                  <label for="password" class="col-form-label">Password</label>
                    {{ Form::password('password',['class'=>'form-control','id'=>'password', 'placeholder'=>'Enter password','disabled'=>true]) }}
                    @if($errors->has('password'))
                        <div class="error">{{ $errors->first('password') }}</div>
                    @endif
                </div>
              @else
                <div class="form-group col-md-6">
                  <label for="passowrd" class="col-form-label">Password</label>
                    {{ Form::password('password',['class'=>'form-control','placeholder'=>'Enter password','id'=>'password']) }}
                    @if($errors->has('password'))
                        <div class="error">{{ $errors->first('password') }}</div>
                    @endif
                </div>
              @endif
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                   {!! Form::submit('Submit', ['class' => 'btn btn-primary submit_btn_new']) !!}
                </div>
              </div>
          </form>
=======
            <div class="card">
              <div class="card-body">
                <h5 class="card-title">Dear Admin,</h5>
                <h6 class="mb-2 text-muted">User Feedback</h6>
                <p class="card-text">{{$feedback->message}}</p>
                <br>
                Yours sincerely,
                <br>
                {{$feedback->name}}
                <br>
                Phone: <a href="tel:{{$feedback->phone}}" class="call" target="_blank">{{$feedback->phone}}</a>
                <br>
                Email:
                <a href="mailto:{{$feedback->email}}" class="card-link" target="_blank">{{$feedback->email}}</a>
              </div>
              <button type="button" id="reply" class="process_btn" data-email="{{$feedback->email}}"data-username="{{$feedback->name}}" data-toggle="modal" data-target="#myModal">Reply <i class="fa fa-angle-double-right" aria-hidden="true"></i><div class="ripple-container"></div></button>
            </div>
>>>>>>> 81f73e1e7946f9f08991a562876e19faa8d1e2da
          </div>
        </div>
      </div>
  </div>
</div>
<<<<<<< HEAD
=======
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Sending Card</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <span class="error-msg alert alert-danger" style="display: none;"></span>
        <span class="success-msg alert alert-success" style="display: none;"></span>
        <form method="post" action="{{route('admin.feedbackReply')}}" enctype="multipart/form-data" id="feedbackReply">
        <div class="modal-body">
            @csrf
            <input type="hidden" name="orderid" id="orderid" value="">
            <input type="hidden" name="category_id"  id="category_id" value="">
          <input type="hidden" name="subcategory_id" id="subcategory_id" value="">
          <div class="form-group">
            <label for="useremail">Email address</label>
            <input type="email" class="form-control" id="useremail" name="email" >
          </div>
          <div class="form-group">
            <label for="username">user name</label>
            <input type="text" class="form-control" id="username" name="name" >
          </div>
          <!-- <div class="form-group">
            <label for="phone">Thecadbar sport number</label>
            <input type="text" class="form-control" id="phone" name="phone" >
          </div> -->

          <div class="form-group">
            <label for="massage">Message</label>
            <textarea class="form-control" id="massage" name="message" rows="3"></textarea>
          </div>
          <!-- <div class="form-group">
            <input type="submit" name="submit" class="form-control">
          </div> -->
        </div>
        <div class="modal-footer">
          <button type="button" class="close" data-dismiss="modal">Close</button>
          <button type="submit" name="submit" class="btn btn-info">Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>
>>>>>>> 81f73e1e7946f9f08991a562876e19faa8d1e2da
@endsection

@push('styles')
<style type="text/css">
  ul {
    list-style: none;
  }
  li {
    margin-left: -20px;
  }
</style>
@endpush
@push('scripts')
<<<<<<< HEAD
<!-- <script type="text/javascript">
$(function () {
    $('input:checkbox.main-checkbox').click(function () {
        var array = [];
        var parent = $(this).closest('.main-parent');
        //check or uncheck sub-checkbox
        $(parent).find('.sub-checkbox').prop("checked", $(this).prop("checked"))
        //push checked sub-checkbox value to array
        $(parent).find('.sub-checkbox:checked').each(function () {
            array.push($(this).val());
        })
    });

    $('input:checkbox.sub-checkbox').click(function () {
      var ii = $(this).closest('.main-parent').find('input:checked.sub-checkbox').length;
      var jj = $(this).closest('.main-parent').find('.sub-checkbox').length;
        if(ii==jj){
          $(this).closest('.main-parent').find('.main-checkbox').prop('checked', true);
        }else{
          $(this).closest('.main-parent').find('.main-checkbox').prop('checked', false);
        }
    });
})
</script> -->
=======
<script type="text/javascript">

  $(document.body).on('click', '.process_btn', function(event) {
        $('#useremail').val($(this).data('email'));
        $('#username').val($(this).data('username'));
    });

  $(document.body).on('submit', '#feedbackReply', function(event) {
        event.preventDefault();
        $.ajax({
            type: $(this).attr('method'),
            url: $(this).attr('action'),
            data: new FormData(this),
            processData: false,
            contentType: false,
            // beforeSend: function() {
            //     showLoader();
            // },
            success: function(data) {
              if(data.status){
                $('.error-msg').css('display','none');
                $('.success-msg').css('display','block').text(data.massage);
                $('#myModal').modal('toggle');
              }else{
                $('.success-msg').css('display','none');
                $('.error-msg').css('display','block').text(data.massage);
              }
            },
            error: function(data) {
                console.log('An error occurred.');
            }
        });
    });
</script>
>>>>>>> 81f73e1e7946f9f08991a562876e19faa8d1e2da
@endpush