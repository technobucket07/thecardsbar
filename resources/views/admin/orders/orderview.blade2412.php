@extends('admin.layouts.app')
@section('content')
<div class="main-panel">
    <!-- <div class="container-fluid my-5 d-flex justify-content-center">
        <button type="button" id="test1" class="btn btn-info btn-sm process" data-order_id="{{$order->id}}" data-email="{{$order->email}}" data-toggle="modal" data-target="#myModal">Process<div class="ripple-container"></div></button>
    </div> -->
  <div class="orderDiv">
    <div class="container">
      <div class="row">
       <div class="col-md-8">
          <div class="mainOrderView">
             <!-- <a href="my-account.html" class="btnCustomStyle2 btn-solid btnNackOrderView">Back</a> -->
             <h3>
                <i class="fa fa-address-card-o"></i> ORDER NO: #{{$order->id}}
             </h3>
             <div class="order-boxx">
                <div class="order-boxx-new">
                   <ul>
                      <!-- <li>
                         <h3>The CardBar</h3>
                         <p><i class="fa fa-map-marker"></i>Storgata 20, 4340 Bryne, Norway</p>
                      </li> -->
                      <li>
                         <h3>Date of Booking</h3>
                         <p><i class="fa fa-calendar"></i>{{ date($order->created_at)}}</p>
                      </li>
                   </ul>
                </div>
                <div class="">
                  <h3>Card Drtails</h3>
                   <div class="table-responsive">
                      <table class="table dataTable" id="CardType">
                         <tbody>
                          @if(isset($order->card_details->category_id))
                            <tr>
                               <td><strong>Category:</strong></td>
                               <td></td>
                               <td align="right">
                                <img src="{{url($order->card_details->category_id)}}">
                               </td>
                            </tr>
                            @endif
                            
                             @if(isset($order->card_details->sub_category_id))
                            <tr>
                               <td><strong>SubCategory:</strong></td>
                               <td></td>
                               <td align="right">
                                <img src="{{url($order->card_details->sub_category_id)}}">
                               </td>
                            </tr>
                            @endif
                            
                            @if(isset($order->card_details->cardtype))
                            <tr>
                               <td><strong>Card type:</strong></td>
                               <td></td>
                               <td align="right">
                                <img src="{{url($order->card_details->cardtype)}}">
                               </td>
                            </tr>
                            @endif
                            
                            @if(isset($order->card_details->backgroundimg))
                            <tr>
                               <td><strong>Background Image:</strong></td>
                               <td></td>
                               <td align="right">
                                <img src="{{url($order->card_details->backgroundimg)}}">
                               </td>
                            </tr>
                            @endif

                            @if(isset($order->card_details->Primary))
                            <tr>
                               <td><strong>Primary Color:</strong></td>
                               <td></td>
                               <td align="right">
                                <p class="price_tag">{{$order->card_details->Primary}}</p>
                               </td>
                            </tr>
                            @endif
                            @if(isset($order->card_details->Secondary))
                            <tr>
                               <td><strong>Secondary Color:</strong></td>
                               <td></td>
                               <td align="right">
                                <p class="price_tag">{{$order->card_details->Secondary}}</p>
                               </td>
                            </tr>
                            @endif
                            @if(isset($order->card_details->Blog))
                            <tr>
                               <td><strong>Blog Color:</strong></td>
                               <td></td>
                               <td align="right">
                                <p class="price_tag">{{$order->card_details->Blog}}</p>
                               </td>
                            </tr>
                            @endif
                            @if(isset($order->card_details->Text))
                            <tr>
                               <td><strong>Text Color:</strong></td>
                               <td></td>
                               <td align="right">
                                <p class="price_tag">{{$order->card_details->Text}}</p>
                               </td>
                            </tr>
                            @endif
                            @if(isset($order->card_details->Heading))
                            <tr>
                               <td><strong>Heading Color:</strong></td>
                               <td></td>
                               <td align="right">
                                <p class="price_tag">{{$order->card_details->Heading}}</p>
                               </td>
                            </tr>
                            @endif
                         </tbody>
                      </table>
                   </div>
                </div>
                <div class="">
                  <h3>Client Personal Information</h3>
                   <div class="table-responsive">
                      <table class="table dataTable" id="PersonalInformation">
                         <tbody>
                          @if(isset($order->card_details->first_name))
                            <tr>
                               <td><strong>First Name:</strong></td>
                               <td align="right">
                                  <p class="price_tag">{{$order->card_details->first_name}}</p>
                               </td>
                            </tr>
                          @endif
                          @if(isset($order->card_details->last_name))
                            <tr>
                               <td><strong>Last Name:</strong></td>
                               <td align="right">
                                  <p class="price_tag">{{$order->card_details->last_name}}</p>
                               </td>
                            </tr>
                          @endif
                          @if(isset($order->card_details->designation))
                            <tr>
                               <td><strong>Designation:</strong></td>
                               <td align="right">
                                  <p class="price_tag">{{$order->card_details->designation}}</p>
                               </td>
                            </tr>
                          @endif
                          @if(isset($order->card_details->primaryemail))
                            <tr>
                               <td><strong>Primary email:</strong></td>
                               <td align="right">
                                  <p class="price_tag">{{$order->card_details->primaryemail}}</p>
                               </td>
                            </tr>
                          @endif
                          @if(isset($order->card_details->secondaryemail))
                            <tr>
                               <td><strong>Secondary email:</strong></td>
                               <td align="right">
                                  <p class="price_tag">{{$order->card_details->secondaryemail}}</p>
                               </td>
                            </tr>
                          @endif

                          @if(isset($order->card_details->primaryphone))
                            <tr>
                               <td><strong>Primary phone:</strong></td>
                               <td align="right">
                                  <p class="price_tag">{{$order->card_details->primaryphone}}</p>
                               </td>
                            </tr>
                          @endif
                          @if(isset($order->card_details->secondaryphone))
                            <tr>
                               <td><strong>Secondary phone:</strong></td>
                               <td align="right">
                                  <p class="price_tag">{{$order->card_details->secondaryphone}}</p>
                               </td>
                            </tr>
                          @endif
                          @if(isset($order->card_details->address))
                            <tr>
                               <td><strong>Address:</strong></td>
                               <td align="right">
                                  <p class="price_tag">{{$order->card_details->address}}</p>
                               </td>
                            </tr>
                          @endif
                          @if(isset($order->card_details->website))
                            <tr>
                               <td><strong>Website:</strong></td>
                               <td align="right">
                                  <p class="price_tag">{{$order->card_details->website}}</p>
                               </td>
                            </tr>
                          @endif
                          @if(isset($order->card_details->profileimg))
                            <tr>
                               <td><strong>Profile Image:</strong></td>
                               <td align="right">
                                  <img src="{{url($order->card_details->profileimg)}}">
                               </td>
                            </tr>
                          @endif
                         </tbody>
                      </table>
                   </div>
                </div>
                @if(!empty($order->card_details->social_media))
                <div class="">
                  <h3>Social Media</h3>
                   <div class="table-responsive">
                      <table class="table dataTable" id="social_media">
                         <tbody>
                          @foreach($order->card_details->social_media as $id => $link)
                          @if(array_key_exists($id, $socials))
                            <tr>
                               <td><strong>{{$socials[$id]}}:</strong></td>
                               <td align="right">
                                  @if($socials[$id] =='WhatsApp')
                                  <p class="price_tag">{{$order->card_details->code}}  {{$link}}</p>
                                  @else
                                  <p class="price_tag">{{$link}}</p>
                                  @endif
                               </td>
                            </tr>
                          @endif
                          @endforeach
                         </tbody>
                      </table>
                   </div>
                </div>
                @endif
                <div class="">
                  <h3>Business Deatils</h3>
                   <div class="table-responsive">
                      <table class="table dataTable" id="social_media">
                         <tbody>
                            @if(isset($order->card_details->company))
                            <tr>
                               <td><strong>Company name:</strong></td>
                               <td align="right">
                                  <p class="price_tag">{{$order->card_details->company}}</p>
                               </td>
                            </tr>
                          @endif
                           @if(isset($order->card_details->companyabout))
                            <tr>
                               <td><strong>About Company:</strong></td>
                               <td align="right">
                                  <p class="price_tag">{{$order->card_details->companyabout}}</p>
                               </td>
                            </tr>
                          @endif
                          @if(isset($order->card_details->logo))
                            <tr>
                               <td><strong>Logo:</strong></td>
                               <td></td>
                               <td align="right">
                                <img src="{{url($order->card_details->logo)}}">
                               </td>
                            </tr>
                          @endif
                          @if(!empty($order->card_details->services))
                          @foreach($order->card_details->services as $id =>$service)
                            <tr>
                               <td><strong>Service {{$id}}:</strong></td>
                               <td align="right">
                                  <p class="price_tag">{{$service}}</p>
                               </td>
                               @if(!empty($order->card_details->serviceimage))
                               <td align="right">
                                  <img src="{{url($order->card_details->serviceimage->$id)}}">
                               </td>
                              @endif
                            </tr>
                            @endforeach
                          @endif
                         </tbody>
                      </table>
                   </div>
                </div>

                @if(!empty($order->card_details->gallery))
                <div class="">
                  <h3>Gallery</h3>
                   <div class="table-responsive">
                      <table class="table dataTable" id="social_media">
                         <tbody>
                          @foreach($order->card_details->gallery as $id => $link)
                            <tr>
                               <td><strong>Image {{$id}}:</strong></td>
                               <td align="right">

                               </td>
                            </tr>
                          @endforeach
                         </tbody>
                      </table>
                   </div>
                </div>
                @endif
                <div class="details-order">
                   <h3>Order Payment Details</h3>
                </div>
                <div class="">
                   <div class="table-responsive">
                      <table class="table dataTable" id="PaymentDetails">
                         <tbody>
                            <tr>
                               <td><strong>Price:</strong></td>
                               <td align="right">
                                  <p class="price_tag">{{$order->price}}</p>
                               </td>
                            </tr>
                            <tr>
                               <td><strong>Tax &amp; Charges:</strong></td>
                               <td align="right">
                                  <p class="price_tag">0.00</p>
                               </td>
                            </tr>
                            <tr>
                               <td><strong>Delivery Charges:</strong></td>
                               <td align="right">
                                  <p class="price_tag">0.00</p>
                               </td>
                            </tr>
                            <tr>
                               <td><strong>Promo Code:</strong></td>
                               <td align="right">
                                  <p class="price_tag">{{$order->coupon ? $order->coupon :'N/A'}} </p>
                               </td>
                            </tr>
                            <tr>
                               <td>
                                  <h3 class="total_list">Order Total:</h3>
                               </td>
                               <td align="right">
                                  <h3 class="total_list">
                                     <p class="price_tag">{{$order->price}}</p>
                                  </h3>
                               </td>
                            </tr>
                         </tbody>
                      </table>
                   </div>
                </div>
             </div>
          </div>
       </div>
       <div class="col-md-4">
         <img src="{{url($card->picture)}}">
       </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Send Hosting Credentials</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form method="post" action="{{route('admin.orderprocess')}}" enctype="multipart/form-data">
        <div class="modal-body">
            @csrf
            <input type="hidden" name="orderid" id="orderid" value="">
          <div class="form-group">
            <label for="useremail">Email address</label>
            <input type="email" class="form-control" id="useremail" name="useremail" >
          </div>
          <div class="form-group">
            <label for="useremail">Attachment</label>
            <input type="file" class="form-control" id="attachment" name="attachment" >
          </div>
          <div class="form-group">
            <label for="massage">Massage</label>
            <textarea class="form-control" id="massage" name="massage" rows="3"></textarea>
          </div>
          <!-- <div class="form-group">
            <input type="submit" name="submit" class="form-control">
          </div> -->
        </div>
        <div class="modal-footer">
            <button type="submit" name="submit" class="btn btn-info">Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('dashboard/assets/js/tinymce.min.js') }}" referrerpolicy="origin"></script>
<script type="text/javascript">
  tinymce.init({
        selector: 'textarea#message',
        menubar: false
      });

    $(document.body).on('click', '.process', function(event) {
        $('#useremail').val($(this).data('email'));
        $('#orderid').val($(this).data('order_id'));
        $('#massage').val($(this).data('massage'));
    });  
</script>
@endpush