@extends('admin.layouts.app')
@section('content')
<div class="main-panel">
  <div class="workspace orderDiv">
    <div class="container">
      <div class="row">
       <div class="col-md-8">
          <div class="mainOrderView">
             <!-- <a href="my-account.html" class="btnCustomStyle2 btn-solid btnNackOrderView">Back</a> -->
             <h4 class="order_num">
                <!--<i class="fa fa-address-card-o"></i>--> Order No: <span class="digits"><span class="prefix">#</span>{{$order->id}}</span>
             </h4>
             <div class="order-boxx">
                <div class="order-boxx-new d_tail_header">
                   <ul>
                      <!-- <li>
                         <h3>The CardBar</h3>
                         <p><i class="fa fa-map-marker"></i>Storgata 20, 4340 Bryne, Norway</p>
                      </li> -->
                      <li>
                         <h4>Date of Booking :</h4>
                         <p><i class="fa fa-calendar"></i>{{ date($order->created_at)}}</p>
                      </li>
					  <li class="etc_opt">
							<div class="search_set">
								<input type="text" placeholder="Search..."> <button class="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
							</div>
							<div class="more_opts">
								<div class="dropdown">
								  <a class="drop_link dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
									More
								  </a>

								  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
									<a class="dropdown-item" href="#">Action</a>
									<a class="dropdown-item" href="#">Another action</a>
									<a class="dropdown-item" href="#">Something else here</a>
								  </div>
								</div>
							</div>
					  </li>
                   </ul>
                </div>
                @if(!empty($cardfields))
                
                <div class="diff_card">
                  <h4>Card Details</h4>
                   <div class="table-responsive">
                      <table class="table dataTable" id="CardType">
                         <tbody>
                        @foreach($cardfields as $cardfield)
                          @if($cardfield['fieldname'] =='category_id')
                            <tr>
                               <td><strong>Category:</strong></td>
                               <td></td>
                               <td align="right" class="values">
                                  <p class="price_tag">
                                    {{$category[$cardfield['fieldvalue']]}}
                                  </p>
                               </td>
                            </tr>
                            @endif
                            
                             @if($cardfield['fieldname'] =='sub_category_id')
                            <tr>
                               <td><strong>SubCategory:</strong></td>
                               <td></td>
                               <td align="right"  class="values">
                                  <p class="price_tag">
                                    @php
                                    if(!isset($cardfield['fieldvalue']) || $cardfield['fieldvalue'] !=0){
                                      echo $subcategory[$cardfield['fieldvalue']];
                                    }else{
                                      echo "No Subcategory";
                                    }
                                    @endphp
                                    
                                  </p>
                               </td>
                            </tr>
                            @endif
                            
                            @if($cardfield['fieldname']== 'cardtype')
                            <tr>
                               <td><strong>Card type:</strong></td>
                               <td></td>
                               <td align="right" class="values">
                                <p class="price_tag">{{$cardfield['fieldvalue']}}</p>
                               </td>
                            </tr>
                            @endif
                            
                            @if($cardfield['fieldname']== 'backgroundimg')
                            <tr>
                               <td><strong>Background Image:</strong></td>
                               <td></td>
                               <td align="right" class="values">
                                <img class="thumbnail" src="{{url('storage/'.$cardfield['fieldvalue'])}}" style="width: 100px">
                               </td>
                            </tr>
                            @endif

                            @if($cardfield['fieldname'] == 'Primary')
                            <tr>
                               <td><strong>Primary Color:</strong></td>
                               <td></td>
                               <td align="right" class="values">
                                <p class="price_tag">{{$cardfield['fieldvalue']}}</p>
								<input type="color" id="#" value="{{$cardfield['fieldvalue']}}">
                               </td>
                            </tr>
                            @endif
                            @if($cardfield['fieldname'] == 'Secondary')
                            <tr>
                               <td><strong>Secondary Color:</strong></td>
                               <td></td>
                               <td align="right" class="values">
                                <p class="price_tag">{{$cardfield['fieldvalue']}}</p>
								<input type="color" id="#" value="{{$cardfield['fieldvalue']}}">
                               </td>
                            </tr>
                            @endif
                            @if($cardfield['fieldname'] == 'Blog')
                            <tr>
                               <td><strong>Blog Color:</strong></td>
                               <td></td>
                               <td align="right" class="values">
                                <p class="price_tag">{{$cardfield['fieldvalue']}}</p>
								<input type="color" id="#" value="{{$cardfield['fieldvalue']}}">
                               </td>
                            </tr>
                            @endif
                            @if($cardfield['fieldname'] == 'Text')
                            <tr>
                               <td><strong>Text Color:</strong></td>
                               <td></td>
                               <td align="right" class="values">
                                <p class="price_tag">{{$cardfield['fieldvalue']}}</p>
								<input type="color" id="#" value="{{$cardfield['fieldvalue']}}">
                               </td>
                            </tr>
                            @endif
                            @if($cardfield['fieldname'] == 'Heading')
                            <tr>
                               <td><strong>Heading Color:</strong></td>
                               <td></td>
                               <td align="right" class="values">
                                <p class="price_tag">{{$cardfield['fieldvalue']}}</p>
								<input type="color" id="#" value="{{$cardfield['fieldvalue']}}">
                               </td>
                            </tr>
                            @endif
                        @endforeach 
                         </tbody>
                      </table>
                   </div>
                </div>
                   
                @endif         
                <div class="diff_card">
                  <h4>Client Personal Information</h4>
                   <div class="table-responsive">
                      <table class="table dataTable" id="PersonalInformation">
                         <tbody>
                        @foreach($cardfields as $cardfield)
                          @if($cardfield['fieldname'] == 'first_name')
                            <tr>
                               <td><strong>First Name:</strong></td>
                               <td align="right" class="values">
                                  <p class="price_tag">{{$cardfield['fieldvalue']}}</p>
                               </td>
                            </tr>
                          @endif
                          @if($cardfield['fieldname'] =='last_name')
                            <tr>
                               <td><strong>Last Name:</strong></td>
                               <td align="right" class="values">
                                 <p class="price_tag">{{$cardfield['fieldvalue']}}</p>
                               </td>
                            </tr>
                          @endif
                          @if($cardfield['fieldname'] =='designation')
                            <tr>
                               <td><strong>Designation:</strong></td>
                               <td align="right" class="values">
                                  <p class="price_tag">{{$cardfield['fieldvalue']}}</p>
                               </td>
                            </tr>
                          @endif
                          @if($cardfield['fieldname'] =='primaryemail')
                            <tr>
                               <td><strong>Primary email:</strong></td>
                               <td align="right" class="values">
                                  <p class="price_tag">{{$cardfield['fieldvalue']}}</p>
                               </td>
                            </tr>
                          @endif
                          @if($cardfield['fieldname'] =='secondaryemail')
                            <tr>
                               <td><strong>Secondary email:</strong></td>
                               <td align="right" class="values">
                                   <p class="price_tag">{{$cardfield['fieldvalue']}}</p>
                               </td>
                            </tr>
                          @endif
                          @if($cardfield['fieldname'] =='primaryphone')
                            <tr>
                               <td><strong>Primary phone:</strong></td>
                               <td align="right" class="values">
                                  <p class="price_tag">{{$cardfield['fieldvalue']}}</p>
                               </td>
                            </tr>
                          @endif
                          @if($cardfield['fieldname'] =='secondaryphone')
                            <tr>
                               <td><strong>Secondary phone:</strong></td>
                               <td align="right" class="values">
                                  <p class="price_tag">{{$cardfield['fieldvalue']}}</p>
                               </td>
                            </tr>
                          @endif
                          @if($cardfield['fieldname'] =='address')
                            <tr>
                               <td><strong>Address:</strong></td>
                               <td align="right" class="values">
                                  <p class="price_tag">{{$cardfield['fieldvalue']}}</p>
                               </td>
                            </tr>
                          @endif
                          @if($cardfield['fieldname'] =='website')
                            <tr>
                               <td><strong>Website:</strong></td>
                               <td align="right" class="values">
                                 <p class="price_tag">{{$cardfield['fieldvalue']}}</p>
                               </td>
                            </tr>
                          @endif
                          @if($cardfield['fieldname'] =='profileimg')
                            <tr>
                               <td><strong>Profile Image:</strong></td>
                               <td align="right" class="values">
                                  <img class="thumbnail" src="{{url('storage/'.$cardfield['fieldvalue'])}}" style="width: 100px">
                               </td>
                            </tr>
                          @endif
                          @if($cardfield['fieldname'] =='Facebook')
                            <tr>
                               <td><strong>Facebook Link:</strong></td>
                               <td align="right" class="values">
								<span class="attache"><i class="fa fa-link" aria-hidden="true"></i></span>
                                  <a href="{{url($cardfield['fieldvalue'])}}">
                                    facebook
                                  </a>
                               </td>
                            </tr>
                          @endif
                          @if($cardfield['fieldname'] =='Skype')
                            <tr>
                               <td><strong>Skype Link:</strong></td>
                               <td align="right" class="values">
							   <span class="attache"><i class="fa fa-link" aria-hidden="true"></i></span>
                                  <a href="{{url($cardfield['fieldvalue'])}}">
                                    Skype
                                  </a>
                               </td>
                            </tr>
                          @endif
                          @if($cardfield['fieldname'] =='WhatsApp')
                            <tr>
                               <td><strong>WhatsApp :</strong></td>
                               <td align="right" class="values">
                                  <p class="price_tag">                                
                                    {{$cardfield['fieldvalue']}}
                                  </p>
                               </td>
                            </tr>
                          @endif
                          @if($cardfield['fieldname'] =='LinkedIn')
                            <tr>
                               <td><strong>LinkedIn Link:</strong></td>
                               <td align="right" class="values">
							   <span class="attache"><i class="fa fa-link" aria-hidden="true"></i></span>
                                  <a href="{{url($cardfield['fieldvalue'])}}">
                                    LinkedIn
                                  </a>
                               </td>
                            </tr>
                          @endif
                          @if($cardfield['fieldname'] =='Twitter')
                            <tr>
                               <td><strong>Twitter Link:</strong></td>
                               <td align="right" class="values">
							   <span class="attache"><i class="fa fa-link" aria-hidden="true"></i></span>
                                  <a href="{{url($cardfield['fieldvalue'])}}">
                                    Twitter
                                  </a>
                               </td>
                            </tr>
                          @endif
                        @endforeach
                         </tbody>
                      </table>
                   </div>
                </div>

                <div class="diff_card">
                  <h4>Business Deatils</h4>
                   <div class="table-responsive">
                      <table class="table dataTable" id="BusinessDeatils">
                         <tbody>
                        @foreach($cardfields as $cardfield)
                            @if($cardfield['fieldname'] == 'company')
                            <tr>
                               <td><strong>Company name:</strong></td>
                               <td></td>
                               <td align="right" class="values">
                                  <p class="price_tag">                                
                                    {{$cardfield['fieldvalue']}}
                                  </p>
                               </td>
                            </tr>
                          @endif
                          @if($cardfield['fieldname'] == 'companyabout')
                            <tr>
                               <td><strong>About Company:</strong></td>
                               <td></td>
                               <td align="right" class="values">
                                 <p class="price_tag">                                
                                    {{$cardfield['fieldvalue']}}
                                  </p>
                               </td>
                            </tr>
                          @endif
                          @if($cardfield['fieldname'] == 'logo')
                            <tr>
                               <td><strong>Logo:</strong></td>
                               <td></td>
                               <td align="right" class="values">
                                <img class="thumbnail_logo" src="{{url('storage/'.$cardfield['fieldvalue'])}}" style="width: 100px">
                               </td>
                            </tr>
                          @endif
                          @if($cardfield['fieldname'] == 'services')
                          <tr>
                               <td><strong>services:</strong></td>
                               <td></td>
                               <td align="right" class="values">
                                  <p class="price_tag">                                
                                  {{$cardfield['fieldvalue']}}
                                  </p>   
                               </td>
                            </tr>
                          @endif
                        @endforeach
                         </tbody>
                      </table>
                   </div>
                </div>
                <div class="diff_card">
                  <h4>Gallery</h4>
                   <div class="table-responsive">
                      <table class="table dataTable" id="social_media">
                         <tbody>
                          @foreach($cardfields as $cardfield)
                          @if($cardfield['fieldname'] == 'gallery')
                            <tr>
                               <td><strong>Image:</strong></td>
                               <td align="right" class="values">
                                <img class="thumbnail" src="{{url($cardfield['fieldvalue'])}}" style="width: 100px">
                               </td>
                            </tr>
                          @endif
                          @endforeach
                          
                         </tbody>
                      </table>
                   </div>
                </div>
				
			</div>
          </div>
       </div>
       <div class="col-md-4">
         <img class="img-fluid" src="{{url($card->picture)}}">
		 <button type="button" id="test1" class="process_btn" data-order_id="{{$order->id}}" data-email="{{$order->email}}" data-toggle="modal" data-target="#myModal">Process <i class="fa fa-angle-double-right" aria-hidden="true"></i><div class="ripple-container"></div></button>
    
			<div class="bill_stack">
				<div class="details-order">
                   <h4>Order Payment Details</h4>
                </div>
                <div class="">
                   <div class="table-responsive">
                      <table class="table dataTable" id="PaymentDetails">
                         <tbody>
                            <tr>
                               <td><strong>Price:</strong></td>
                               <td align="right">
                                  <p class="price_tag">{{$order->price}}</p>
                               </td>
                            </tr>
                            <tr>
                               <td><strong>Tax &amp; Charges:</strong></td>
                               <td align="right">
                                  <p class="price_tag">0.00</p>
                               </td>
                            </tr>
                            <tr>
                               <td><strong>Delivery Charges:</strong></td>
                               <td align="right">
                                  <p class="price_tag">0.00</p>
                               </td>
                            </tr>
                            <tr>
                               <td><strong>Promo Code:</strong></td>
                               <td align="right">
                                  <p class="price_tag">{{$order->coupon ? $order->coupon :'N/A'}} </p>
                               </td>
                            </tr>
                            <tr class="ttl">
                               <td>
                                  <h3 class="total_list">Order Total:</h3>
                               </td>
                               <td align="right">
                                  <h3 class="total_list">
                                     <p class="price_tag">{{$order->price}}</p>
                                  </h3>
                               </td>
                            </tr>
                         </tbody>
                      </table>
                   </div>
                </div>
            </div>
       </div>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Sending Crad</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <form method="post" action="{{route('admin.orderprocess')}}" enctype="multipart/form-data">
        <div class="modal-body">
            @csrf
            <input type="hidden" name="orderid" id="orderid" value="">
          <div class="form-group">
            <label for="useremail">Email address</label>
            <input type="email" class="form-control" id="useremail" name="useremail" >
          </div>
          <div class="form-group">
            <label for="useremail">Attachment</label>
            <input type="file" class="form-control" id="attachment" name="attachment" >
          </div>
          <div class="form-group">
            <label for="massage">Massage</label>
            <textarea class="form-control" id="massage" name="massage" rows="3"></textarea>
          </div>
          <!-- <div class="form-group">
            <input type="submit" name="submit" class="form-control">
          </div> -->
        </div>
        <div class="modal-footer">
            <button type="submit" name="submit" class="btn btn-info">Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('dashboard/assets/js/tinymce.min.js') }}" referrerpolicy="origin"></script>
<script type="text/javascript">
  tinymce.init({
        selector: 'textarea#message',
        menubar: false
      });

    $(document.body).on('click', '.process_btn', function(event) {
        $('#useremail').val($(this).data('email'));
        $('#orderid').val($(this).data('order_id'));
        $('#massage').val($(this).data('massage'));
    });  

    $( document ).ready(function() {
        var cardtype = $('#CardType tr').length;
        if(cardtype ==0){
         $('#CardType tbody').html("<tr></tr>").find('tr').text(' No Card Details');
        }
        var social = $('#social_media tr').length;
        if(social ==0){
         $('#social_media tbody').html("<tr></tr>").find('tr').text(' No Gallery');
        }
        var business = $('#BusinessDeatils tr').length;
        if(business ==0){
         $('#BusinessDeatils tbody').html("<tr></tr>").find('tr').text(' No Business Details');
        }
        var personal = $('#PersonalInformation tr').length;
        if(personal ==0){
         $('#PersonalInformation tbody').html("<tr></tr>").find('tr').text(' No Personal Information');
        }
    });
</script>
@endpush