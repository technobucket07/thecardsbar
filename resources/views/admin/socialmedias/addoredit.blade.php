@extends('admin.layouts.app')
@section('content')
<div class="main-panel">
  <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            @if(isset($media))
              {{ Form::model($media, ['route' => ['admin.socialmedias.update', $media->id], 'method' => 'patch']) }}
          @else
              {{ Form::open(['route' => 'admin.socialmedias.store']) }}
          @endif
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="name" class="col-form-label">Name</label>
                    {{ Form::text('name',old('name'),['class'=>'form-control','id'=>'name']) }}
                    @if($errors->has('name'))
                        <div class="error">{{ $errors->first('name') }}</div>
                    @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="icon" class="col-form-label">Icon</label>
                    {{ Form::text('icon',old('icon'),['class'=>'form-control','id'=>'icon']) }}
                    @if($errors->has('icon'))
                        <div class="error">{{ $errors->first('icon') }}</div>
                    @endif
                </div>
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                   {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                </div>
              </div>
          </form>
          </div>
        </div>
      </div>
  </div>
</div>
@endsection

@push('styles')
<style type="text/css">
  ul {
    list-style: none;
  }
  li {
    margin-left: -20px;
  }
</style>
@endpush
@push('scripts')
<!-- <script type="text/javascript">
$(function () {
    $('input:checkbox.main-checkbox').click(function () {
        var array = [];
        var parent = $(this).closest('.main-parent');
        //check or uncheck sub-checkbox
        $(parent).find('.sub-checkbox').prop("checked", $(this).prop("checked"))
        //push checked sub-checkbox value to array
        $(parent).find('.sub-checkbox:checked').each(function () {
            array.push($(this).val());
        })
    });

    $('input:checkbox.sub-checkbox').click(function () {
      var ii = $(this).closest('.main-parent').find('input:checked.sub-checkbox').length;
      var jj = $(this).closest('.main-parent').find('.sub-checkbox').length;
        if(ii==jj){
          $(this).closest('.main-parent').find('.main-checkbox').prop('checked', true);
        }else{
          $(this).closest('.main-parent').find('.main-checkbox').prop('checked', false);
        }
    });
})
</script> -->
@endpush