@extends('admin.layouts.app')
@section('content')
<div class="main-panel">
	<div class="content">
	    <div class="container-fluid">
	        <div class="row">
	          <div class="col-md-12">
	          	@if(isset($fields))
	              {{ Form::model($fields, ['route' => ['admin.cardfields.update', $fields->id], 'method' => 'patch']) }}
	          	@else
	              {{ Form::open(['route' => 'admin.cardfields.store']) }}
	          	@endif
	          	<div class="row">
	          		<div class="col-md-4">
		                <div class="form-group required">
		                  <label for="fieldtype" class="col-form-label">Field Type</label>
		                  {{ Form::select('fieldtype',inputfield(),old('fieldtype'),['class'=>'form-control','id'=>'fieldtype','required'=>true]) }}
		                </div>
		             </div>
	                <div class="form-group col-md-4">
	                  <label for="name" class="col-form-label">Name</label>
	                    {{ Form::text('name',old('name'),['class'=>'form-control','id'=>'name']) }}
	                    @if($errors->has('name'))
	                        <div class="error">{{ $errors->first('name') }}</div>
	                    @endif
	                </div>
	                <div class="form-group col-md-4">
	                  <label for="defaultvalue" class="col-form-label">Default Value</label>
	                    {{ Form::text('defaultvalue',old('defaultvalue'),['class'=>'form-control','id'=>'defaultvalue']) }}
	                    @if($errors->has('defaultvalue'))
	                        <div class="error">{{ $errors->first('defaultvalue') }}</div>
	                    @endif
	                </div>
	                <div class="form-group form-check form-check-inline">
		              <div class="chiller_cb">
		                {{ Form::checkbox('max_limit', 0,old('max_limit') == 1 ? 'checked' : (isset($fields->max_limit) && $fields->max_limit == 1 ? 'checked' : ''),['id'=>'max_limit', 'class'=>'form-check-input']) }}
		                <label for="max_limit">Max. Limit Field</label>
		                <span></span>
		              </div>
		            </div>
		            <div class="form-group form-check form-check-inline">
		              <div class="chiller_cb">
		                {{ Form::checkbox('min_limit', 0,old('min_limit') == 1 ? 'checked' : (isset($fields->min_limit) && $fields->min_limit == 1 ? 'checked' : ''),['id'=>'min_limit', 'class'=>'form-check-input']) }}
		                <label for="min_limit">Min. Limit Field</label>
		                <span></span>
		              </div>
		            </div>
		            <div class="form-group form-check form-check-inline">
		              <div class="chiller_cb">
		                {{ Form::checkbox('withimages', 0,old('withimages') == 1 ? 'checked' : (isset($fields->withimages) && $fields->withimages == 1 ? 'checked' : ''),['id'=>'withimages', 'class'=>'form-check-input']) }}
		                <label for="withimages">Field with images</label>
		                <span></span>
		              </div>
		            </div>
		            <div class="form-group form-check form-check-inline">
		              <div class="chiller_cb">
		                {{ Form::checkbox('sub_field', 0,old('sub_field') == 1 ? 'checked' : (isset($fields->sub_field) && $fields->sub_field == 1 ? 'checked' : ''),['id'=>'sub_field', 'class'=>'form-check-input']) }}
		                <label for="sub_field">You want Subfield</label>
		                <span></span>
		              </div>
		            </div>
            	</div>
            	<div class="row">
	                <div class="form-group col-md-6">
	                   {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
	                </div>
              	</div>
          		{{ Form::close() }}
	          </div>
	      	</div>
	  	</div>
	</div>
</div>
@endsection
@push('styles')
@endpush
@push('scripts')
@endpush