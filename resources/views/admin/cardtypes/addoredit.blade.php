@extends('admin.layouts.app')
@section('content')
<div class="main-panel">
  <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            @if(isset($category))
              {{ Form::model($category, ['route' => ['admin.cardtypes.update', $category->id], 'method' => 'patch','files'=> true]) }}
          @else
              {{ Form::open(['route' => 'admin.cardtypes.store','files'=> true]) }}
          @endif
            <div class="row">
              <div class="col-md-6">
                <div class="form-group required">
                  <label for="name">Card Type</label>
                  {{ Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Enter name','id'=>'name','required'=>1]) }}
                  @if($errors->has('name'))
                    <div class="error">{{ $errors->first('name') }}</div>
                  @endif
                </div>
              </div>
            </div>
              <div class="row">
                <div class="form-group col-md-6">
                   {!! Form::submit('Submit', ['class' => 'btn btn-primary submit_btn_new']) !!}
                </div>
              </div>
          {{ Form::close() }}
          </div>
        </div>
      </div>
  </div>
</div>
@endsection

@push('scripts')

@endpush
@push('styles')

@endpush