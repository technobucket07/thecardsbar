@extends('admin.layouts.app')
@section('content')
<div class="main-panel">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="create_header">
					<p>Create Card Theme</p>
				</div>
			</div>
		</div>
	</div>
  <div class="content creation_form_outer">
      <div class="container-fluid">		
        <div class="row">
          <div class="col-md-12">
            @if(isset($card))
              {{ Form::model($card, ['route' => ['admin.vistingcards.update', $card->id], 'method' => 'patch','files'=> true,'id'=>'vistingcards', 'class'=>'ajax-submit']) }}
          @else
              {{ Form::open(['route' => 'admin.vistingcards.store','files'=> true,'id'=>'vistingcards', 'class'=>'ajax-submit']) }}
          @endif
            <div class="row">
              <div class="col-md-12">
                <div class="form-group required">
                  <label for="cardtitle">Card Tittle</label>
                  {{ Form::text('cardtitle',old('cardtitle'),['class'=>'form-control','placeholder'=>'Add Title','id'=>'cardtitle']) }}
                  @if($errors->has('cardtitle'))
                    <div class="error">{{ $errors->first('cardtitle') }}</div>
                  @endif
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group required">
                  <label for="cardtitle">Card Description</label>
                  {{ Form::textarea('carddescription',old('carddescription'),['class'=>'form-control','placeholder'=>'Write Discription...','id'=>'carddescription','cols'=>5,'rows'=>4]) }}
                  @if($errors->has('carddescription'))
                    <div class="error">{{ $errors->first('carddescription') }}</div>
                  @endif
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group " id = "cardUploader">
        					<label for="picture">Card Picture</label>
        					<div class="file-upload">
        					  <div class="image-upload-wrap"@if(isset($card)) @if(!empty($card->picture)) style = "display:none" @endif @endif>
        						{{ Form::file('picture',['class'=>'form-control file-upload-input','id'=>'picture','onchange'=>'readURL(this,"cardUploader")','accept'=>'image/*']) }}
                    <!-- <input class="thisimage" type="hidden" name="picture" value="{{old('picture')}}"> -->
        						  @if($errors->has('picture'))
        							<div class="error">{{ $errors->first('picture') }}</div>
        						  @endif
        						<div class="drag-text">
        						  <h3>Drag and drop or select Image</h3>
        						</div>
        					  </div>
        					  <div class="file-upload-content"@if(isset($card)) @if(!empty($card->picture)) style = "display:block" @endif @endif>
        						<img class="file-upload-image" src='@if(isset($card)) {{ url("$card->picture") }} @endif' alt="your image" />
        						<div class="image-title-wrap">
        						  <button type="button" onclick="removeUpload('cardUploader')" class="remove-image"><i class="fa fa-trash" aria-hidden="true"></i> <span class="image-title">Uploaded Image</span></button>
        						</div>
        					  </div>
        					</div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="crosel_image">Carousel Image</label>
        				  <div class="file-upload" id = "carouselUploader">
        					  <div class="image-upload-wrap" @if(isset($card)) @if(!empty($card->crosel_image)) style = "display:none" @endif @endif>
        						{{ Form::file('crosel_image',['class'=>'form-control file-upload-input','id'=>'crosel_image','onchange'=>'readURL(this,"carouselUploader")','accept'=>'image/*']) }}
        						  @if($errors->has('crosel_image'))
        							<div class="error">{{ $errors->first('crosel_image') }}</div>
        						  @endif
        						<div class="drag-text">
        						  <h3>Drag and drop or select Image</h3>
        						</div>
        					  </div>
        					  <div class="file-upload-content" @if(isset($card)) @if(!empty($card->crosel_image)) style = "display:block" @endif @endif>
        						<img class="file-upload-image" src='@if(isset($card)) {{ url("$card->crosel_image") }} @endif' alt="your image" />
        						<div class="image-title-wrap">
        						  <button type="button" onclick="removeUpload('carouselUploader')" class="remove-image"><i class="fa fa-trash" aria-hidden="true"></i> <span class="image-title">Uploaded Image</span></button>
        						</div>
        					  </div>
        					</div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <button type="button" id="addTheme" class="btn btn-primary submit_btn_new">Add More Themes</button>
                <div class="themeform" style="display: @if(count($cardthemes) == 0) none; @endif">
					<div class="row">
						<div class="col-md-12">
							<button type="button" class="btn btn-primary submit_btn_new addMoretheme">Create New</button>
						</div>
				  </div>
                  @if(count($cardthemes) > 0)
                  @foreach($cardthemes as $key=>$cardtheme)
                  <div class="row themerow">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="themename{{$key}}">Theme Name</label>
                        <input type="text" class="form-control themename" name="theme[][name]" id = "themename{{$key}}"  value="{{$cardtheme->name}}">
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group" id = "cardthemes{{$key}}">
                        <label for="theme{{$key}}">Theme Image</label>
                        <div class="file-upload">
                          <div class="image-upload-wrap" @if(isset($cardtheme)) @if(!empty($cardtheme->image)) style = "display:none" @endif @endif>
                            <input type="file" class="form-control file-upload-input themefile" name="theme[][image]" id="theme{{$key}}" onchange="readURL(this,'cardthemes{{$key}}')"accept="image/*">
                            <div class="drag-text">
                              <h3>Drag and drop or select Image</h3>
                            </div>
                          </div>
                          <div class="file-upload-content"@if($cardtheme->image) style = "display:block" @endif>
                            <img class="file-upload-image" src='@if($cardtheme->image) {{ url("$cardtheme->image") }} @endif' alt="your image" />
                            <div class="image-title-wrap">
                              <button type="button" onclick="removeUpload('cardthemes{{$key}}')" class="remove-image"><i class="fa fa-trash" aria-hidden="true"></i> <span class="image-title">Uploaded Image</span></button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-2"><button type="button" class="close removetheme" aria-label="Close"><span aria-hidden="true">×</span></button></div>
                  </div>
                  @endforeach
                  @else
                  <div class="row themerow">
                    <div class="col-md-4">
                      <div class="form-group">
                        <label for="picture">Theme Name</label>
                        <input type="text" class="form-control themename" name="theme[][name]" disabled>
                      </div>
                    </div>
                    <div class="col-md-6">
                      <div class="form-group" id = "cardthemes">
                        <label for="picture">Theme Image</label>
                        <div class="file-upload">
                          <div class="image-upload-wrap">
                            <input type="file" class="form-control file-upload-input themefile" name="theme[][image]" id="" onchange="readURL(this,'cardthemes')"accept="image/*" disabled>
                            <div class="drag-text">
                              <h3>Drag and drop or select Image</h3>
                            </div>
                          </div>
                          <div class="file-upload-content">
                            <img class="file-upload-image" src='' alt="your image" />
                            <div class="image-title-wrap">
                              <button type="button" onclick="removeUpload('cardthemes')" class="remove-image"><i class="fa fa-trash" aria-hidden="true"></i> <span class="image-title">Uploaded Image</span></button>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endif
                </div>
                </div>
              </div>
              {{--
            <div class="row">
              <div class="col-md-6">
                <div class="form-group required">
                  <label for="category_id">Category</label>
                  {{ Form::select('category_id',$categories,old('category_id'),['class'=>'form-control minimal category','placeholder'=>'---Choose Category---','id'=>'category_id']) }}
                    @if($errors->has('category_id')) }}
                      <div class="error">{{ $errors->first('category_id') }}</div>
                    @endif
                </div>
              </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label for="cardtitle">Sub Category</label>
                  <input type="hidden" id="subcategoryid" value="{{(isset($card) ? $card->sub_category_id : '')}}">
                  {{ Form::select('sub_category_id',[],old('sub_category_id'),['class'=>'form-control minimal subcategory','id'=>'sub_category_id']) }}
                    @if($errors->has('sub_category_id')) }}
                      <div class="error">{{ $errors->first('sub_category_id') }}</div>
                    @endif
                </div>
              </div>
            </div> --}}
            <div class="row">
              <div class="col-md-4">
                <div class="form-group required quantity">
                  <label for="cardtitle">Card Actual price</label>
                  {{ Form::number('actualprice',old('actualprice'),['class'=>'form-control','placeholder'=>'Amount','id'=>'actualprice']) }}
                  @if($errors->has('actualprice'))
                    <div class="error">{{ $errors->first('actualprice') }}</div>
                  @endif
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group required">
                  <label for="cardtitle">Discount Type</label>
                  {{ Form::select('discounttype', [ ''=>'select', '1'=>'Percentage', '2'=>'Flat off'],old('discounttype'),['class'=>'form-control minimal','id'=>'discounttype']) }}
                    @if($errors->has('discounttype')) }}
                      <div class="error">{{ $errors->first('discounttype') }}</div>
                    @endif
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group required quantity">
                  <label for="discount">Discount</label>
                  {{ Form::number('discount',old('discount'),['class'=>'form-control','placeholder'=>'Value','id'=>'discount']) }}
                  @if($errors->has('discount'))
                    <div class="error">{{ $errors->first('discount') }}</div>
                  @endif
                </div>
              </div>
             {{-- <div class="col-md-3">
                <div class="form-group required">
                  <label for="cardtitle">Actual price</label>
                  {{ Form::text('actualprice',old('actualprice'),['class'=>'form-control','placeholder'=>'Amount','id'=>'actualprice','readonly'=>true]) }}
                  @if($errors->has('actualprice'))
                    <div class="error">{{ $errors->first('actualprice') }}</div>
                  @endif
                </div>
              </div>
              --}}
            </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group required">
                    <label for="cardtype">Type of card</label>
                   {{ Form::select('cardtype',$cardtype,old('cardtype'),['class'=>'form-control minimal','id'=>'cardtype']) }}
                    @if($errors->has('cardtype')) }}
                      <div class="error">{{ $errors->first('cardtype') }}</div>
                    @endif
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <label for="colortype">Color Type</label>
                    {{ Form::select('colortype[]',$allcolortype,old('colortype[]'),['class'=>'form-control multiselect','id'=>'colortype','multiple'=>'multiple']) }}
                    <button type="button" id="test1" class="btn" data-toggle="modal" data-target="#addColor"><i class="fa fa-plus" aria-hidden="true"></i><div class="ripple-container"></div></button>
                    @if($errors->has('colortype'))
                      <div class="error">{{ $errors->first('colortype') }}</div>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-4">
                  <div class="form-group required quantity">
                    <label for="maxsocialmedia">Mix. Number of Social Media</label>
                    {{ Form::number('maxsocialmedia',old('maxsocialmedia'),['class'=>'form-control','placeholder'=>'Choose Units','id'=>'maxsocialmedia']) }}
                    @if($errors->has('maxsocialmedia'))
                      <div class="error">{{ $errors->first('maxsocialmedia') }}</div>
                    @endif
                  </div>
                </div>
                <div class="col-md-4">
                  <div class="form-group required quantity">
                    <label for="minsocialmedia">Min. Number of Social Media</label>
                    {{ Form::number('minsocialmedia',old('minsocialmedia'),['class'=>'form-control','placeholder'=>'Choose Units','id'=>'minsocialmedia']) }}
                    @if($errors->has('minsocialmedia'))
                      <div class="error">{{ $errors->first('minsocialmedia') }}</div>
                    @endif
                  </div>
                </div>
              </div>
			  <div class="row">
				<div class="col-lg-12">
					<div class="half_part">
						<div class="form-group required">
							<div class="custom-control custom-switch">
							  {{Form::checkbox('profile', 1 , old('profile') == 1 ? 'checked' : (isset($card->profile) && $card->profile == 1 ? 'checked' : '') , ['id'=>'profileyes','class'=>'custom-control-input'])}}
							  <label class="custom-control-label" for="profileyes">Do you want profile image ?</label>
							</div>
						</div>
						<div class="form-group">
							<div class="custom-control custom-switch">
							  {{Form::checkbox('background',1, old('background') == 1 ? 'checked' : (isset($card->background) && $card->background == 1 ? 'checked' : ''),['id'=>'backgroundyes','class'=>'custom-control-input'])}}
							  <label class="custom-control-label" for="backgroundyes">Do you want background image ?</label>
							</div>
						</div>
						<div class="form-group">
							<div class="custom-control custom-switch">
							  {{Form::checkbox('services',1, old('services') == 1 ? 'checked' : (isset($card->services) && $card->services == 1 ? 'checked' : ''),['id'=>'servicesyes','class'=>'custom-control-input'])}}
							  <label class="custom-control-label" for="servicesyes">Do you want services ?</label>
							</div>
							<div class="servicesdiv">
								<div class="row">    
								  <div class="col-md-6 quantity">
									<label for="max_no_services" class="col-form-label">Max. Number of services</label>
									{{ Form::number('max_no_services',old('max_no_services'),['class'=>'form-control','placeholder'=>'Units','id'=>'max_no_services']) }}
								  </div>
								  <div class="col-md-6 quantity">
									<label for="min_no_services" class="col-form-label">Min. Number of services</label>
									{{ Form::number('min_no_services',old('min_no_services'),['class'=>'form-control','placeholder'=>'Units','id'=>'min_no_services']) }}
								  </div>						  
								</div>
								<div class="row">
									<div class="form-group col-md-12">
									<div class="custom-control custom-switch">
									  {{Form::checkbox('serviceswithimg',1,old('serviceswithimg') == 1 ? 'checked' : (isset($card->serviceswithimg) && $card->serviceswithimg == 1 ? 'checked' : ''),['id'=>'services_img','class'=>'custom-control-input'])}}
									  <label class="custom-control-label" for="services_img">Services with Images ?</label>
									</div>
								  </div>
								</div>
							</div>
						</div>
					</div>
					<div class="half_part">
						<div class="form-group">
							<div class="custom-control custom-switch">
							  {{Form::checkbox('logo',1, old('logo') == 1 ? 'checked' : (isset($card->logo) && $card->logo == 1 ? 'checked' : ''),['id'=>'logoyes','class'=>'custom-control-input'])}}
							  <label class="custom-control-label" for="logoyes">Do you want logo ?</label>
							</div>
						</div>
						<div class="form-group">
							<div class="custom-control custom-switch">
							  {{Form::checkbox('gallery',1, old('gallery') == 1 ? 'checked' : (isset($card->gallery) && $card->gallery == 1 ? 'checked' : ''),['id'=>'galleryyes','class'=>'custom-control-input'])}}
							  <label class="custom-control-label" for="galleryyes">Do you want gallery images ?</label>
							</div>
							<div class="no_gallerydiv" style="display: none;">
								<div class="row">
									<div class="col-lg-6 quantity">
										<label for="max_no_gallery" class="col-form-label">Max. Number of gallery image</label>
										{{ Form::number('max_no_gallery',old('max_no_gallery'),['class'=>'form-control','placeholder'=>'Units','id'=>'max_no_gallery']) }}
									</div>
									<div class="col-lg-6 quantity">
										<label for="max_no_gallery" class="col-form-label">Min. Number of gallery image</label>
										{{ Form::number('min_no_gallery',old('min_no_gallery'),['class'=>'form-control','placeholder'=>'Units','id'=>'min_no_gallery']) }}
									</div>
								</div>                        
							</div>
						</div>
					</div>
				</div>
			  </div>
              <div class="row">
                  <div class="col-md-12">	
					<fieldset class="">
              <legend>Required Details:</legend>
              <div class="form-group required form-check form-check-inline">
                <div class="chiller_cb">
  							 {{ Form::checkbox('username', 1, old('username') == 1 ? 'checked' : (isset($card->username) && $card->username == 1 ? 'checked' : ''),['id'=>'username', 'class'=>'form-check-input']) }}
    							<label for="username">Name</label>
    							<span></span>
    						</div>
            </div>
            <div class="form-group form-check form-check-inline">
                <div class="chiller_cb">
                 {{ Form::checkbox('username2', 1, old('username2') == 1 ? 'checked' : (isset($card->username2) && $card->username2 == 1 ? 'checked' : ''),['id'=>'username2', 'class'=>'form-check-input']) }}
                  <label for="username2">Other Name</label>
                  <span></span>
                </div>
            </div>
            <div class="form-group required form-check form-check-inline">
  						<div class="chiller_cb">
  							{{ Form::checkbox('primaryphone', 1, old('primaryphone') == 1 ? 'checked' : (isset($card->primaryphone) && $card->primaryphone == 1 ? 'checked' : ''),['id'=>'primaryphone', 'class'=>'form-check-input']) }}
  							<label for="primaryphone">Primary Phone</label>
  							<span></span>
  						</div>
            </div>
            <div class="form-group form-check form-check-inline">
  						<div class="chiller_cb">
  							{{ Form::checkbox('secondaryphone', 1,old('secondaryphone') == 1 ? 'checked' : (isset($card->secondaryphone) && $card->secondaryphone == 1 ? 'checked' : ''),['id'=>'secondaryphone', 'class'=>'form-check-input']) }}
  							<label for="secondaryphone">Secondary Phone</label>
  							<span></span>
  						</div>
            </div>
            <div class="form-group required form-check form-check-inline">
  						<div class="chiller_cb">
  							{{ Form::checkbox('primaryemail', 1,old('primaryemail') == 1 ? 'checked' : (isset($card->primaryemail) && $card->primaryemail == 1 ? 'checked' : ''),['id'=>'primaryemail', 'class'=>'form-check-input']) }}
  							<label for="primaryemail">Primary Email</label>
  							<span></span>
  						</div>
            </div>
            <div class="form-group form-check form-check-inline">
  						<div class="chiller_cb">
  							{{ Form::checkbox('secondaryemail', 1,old('secondaryemail') == 1 ? 'checked' : (isset($card->secondaryemail) && $card->secondaryemail == 1 ? 'checked' : ''),['id'=>'secondaryemail', 'class'=>'form-check-input']) }}
  							<label for="secondaryemail">Secondary Email</label>
  							<span></span>
  						</div>
            </div>
            <div class="form-group required form-check form-check-inline">
  						<div class="chiller_cb">
  							{{ Form::checkbox('address', 1,old('address') == 1 ? 'checked' : (isset($card->address) && $card->address == 1 ? 'checked' : ''),['id'=>'address', 'class'=>'form-check-input']) }}
  							<label for="address">Address</label>
  							<span></span>
  						</div>
            </div>
            <div class="form-group form-check form-check-inline">
  						<div class="chiller_cb">
  							{{ Form::checkbox('location', 1,old('location') == 1 ? 'checked' : (isset($card->location) && $card->location == 1 ? 'checked' : ''),['id'=>'location', 'class'=>'form-check-input']) }}
  							<label for="location">Location</label>
  							<span></span>
  						</div>
            </div>
            <div class="form-group form-check form-check-inline">
  						<div class="chiller_cb">
  							{{ Form::checkbox('website', 1,old('website') == 1 ? 'checked' : (isset($card->website) && $card->website == 1 ? 'checked' : ''),['id'=>'website', 'class'=>'form-check-input']) }}
  							<label for="website">Website</label>
  							<span></span>
  						</div>
            </div>
            <div class="form-group form-check form-check-inline">
  						<div class="chiller_cb">
  							{{ Form::checkbox('designation', 1,old('designation') == 1 ? 'checked' : (isset($card->designation) && $card->designation == 1 ? 'checked' : ''),['id'=>'designation', 'class'=>'form-check-input']) }}
  							<label for="designation">Designation</label>
  							<span></span>
  						</div>
            </div>
					  <div class="form-group form-check form-check-inline">
  						<div class="chiller_cb">
  							{{ Form::checkbox('company', 1,old('company') == 1 ? 'checked' : (isset($card->company) && $card->company == 1 ? 'checked' : ''),['id'=>'company', 'class'=>'form-check-input']) }}
  							<label for="company">Company name</label>
  							<span></span>
  						</div>
            </div>
            <div class="form-group form-check form-check-inline">
              <div class="chiller_cb">
                {{ Form::checkbox('bottomtitle', 1,old('bottomtitle') == 1 ? 'checked' : (isset($card->bottomtitle) && $card->bottomtitle == 1 ? 'checked' : ''),['id'=>'bottomtitle', 'class'=>'form-check-input']) }}
                <label for="bottomtitle">Bottom Title</label>
                <span></span>
              </div>
            </div>
            <div class="form-group form-check form-check-inline">
            <div class="chiller_cb">
              {{ Form::checkbox('companyabout', 1,old('companyabout') == 1 ? 'checked' : (isset($card->companyabout) && $card->companyabout == 1 ? 'checked' : ''),['id'=>'companyabout', 'class'=>'form-check-input']) }}
              <label for="companyabout">About Company</label>
              <span></span>
            </div>
            </div>
            <div class="form-group form-check form-check-inline">
              <div class="chiller_cb">
                {{ Form::checkbox('description', 1,old('description') == 1 ? 'checked' : (isset($card->description) && $card->description == 1 ? 'checked' : ''),['id'=>'description', 'class'=>'form-check-input']) }}
                <label for="description">Description</label>
                <span></span>
              </div>
              <div class="classdescription">
                <label for="min_no_services" class="col-form-label">Min. Number of Description</label>
                {{ Form::text('min_no_description',old('min_no_description'),['class'=>'form-control','id'=>'min_no_services']) }}
              </div>
            </div>
            <div class="form-group form-check form-check-inline">
              <div class="chiller_cb">
                {{ Form::checkbox('calltagline', 1,old('calltagline') == 1 ? 'checked' : (isset($card->calltagline) && $card->calltagline == 1 ? 'checked' : ''),['id'=>'calltagline', 'class'=>'form-check-input']) }}
                <label for="calltagline">Call tag line</label>
                <span></span>
              </div>
            </div>
            <div class="form-group form-check form-check-inline">
              <div class="chiller_cb">
                {{ Form::checkbox('openclosetime', 1,old('openclosetime') == 1 ? 'checked' : (isset($card->openclosetime) && $card->openclosetime == 1 ? 'checked' : ''),['id'=>'openclosetime', 'class'=>'form-check-input']) }}
                <label for="openclosetime">opening closeing time</label>
                <span></span>
              </div>
            </div>
                      
                    </fieldset>
                  </div>
                </div>
              <div class="row">
                <div class="form-group col-md-6">
                   {!! Form::submit('Submit', ['class' => 'btn btn-primary submit_btn_new']) !!}
                </div>
              </div>
          {{ Form::close() }}
          </div>
        </div>
      </div>
  </div>
</div>
<!-- Modal -->
<div class="modal fade" id="addColor" role="dialog">
    <div class="modal-dialog">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Add New Color Type</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="alert alert-success successmsg" style="display: none;" role="alert">
        </div>
        <div class="alert alert-danger errormsg"style="display: none;" role="alert">
        </div>
        <form method="post" action="{{route('admin.colortypes.store')}}" id="addColorform">
        <div class="modal-body">
            @csrf
          <div class="form-group">
            <label for="Typename">Color Type Name</label>
            <input type="text" class="form-control" id="Type" name="name" >
          </div>
          <!-- <div class="form-group">
            <label for="Typename">Color Type Title</label>
            <input type="text" class="form-control" id="TypeTitle" name="title" >
          </div> -->
        </div>
        <div class="modal-footer">
            <button type="submit" name="submit" class="btn btn-info">Submit</button>
        </div>
        </form>
      </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('dashboard/assets/js/select2.min.js') }}"></script>
<script type="text/javascript">
  if ($('#description').is(':checked')) {

    $('.classdescription').css('display','block');
  }else{
    $('.classdescription').css('display','none');
  }

  $('#description').change(function() {
      if ($(this).is(':checked')) {
          $('.classdescription').css('display','block');
      }
      else {
        $('.classdescription').css('display','none');
      }
  });

  if ($('#galleryyes').is(':checked')) {

    $('.no_gallerydiv').css('display','block');
  }else{
    $('.no_gallerydiv').css('display','none');
  }


  $('input[type=checkbox][name=gallery]').change(function() {
      if ($(this).is(':checked')) {
          $('.no_gallerydiv').css('display','block');
      }
      else {
        $('.no_gallerydiv').css('display','none');
      }
  });

  if ($('#servicesyes').is(':checked')) {

    $('.servicesdiv').css('display','block');
  }else{
    $('.servicesdiv').css('display','none');
  }

  $('input[type=checkbox][name=services]').change(function() {
      if ($(this).is(':checked')) {
          $('.servicesdiv').css('display','block');
      }
      else {
        $('.servicesdiv').css('display','none');
      }
  });

  $( ".multiselect" ).select2( {
    } );

  $('.category').change(function() {
    var cate = $(this).val();
      $.ajax({
        url:"{{route('admin.getSubcategory')}}",
        method:'get',
        data:{
          category_id:cate
        },
        success: function(res){
          var html = "";
          $.each(res, function (key, value) {
            html += "<option value="+key+">"+value+"</option>";
          });
          $(".subcategory").html(html);
        }
      });
  });

  $( document ).ready(function() {
    var cate = $('.category').val();
    var subcate = $('#subcategoryid').val();
    $.ajax({
        url:"{{route('admin.getSubcategory')}}",
        method:'get',
        data:{
          category_id:cate
        },
        success: function(res){
          var html = "";
          $.each(res, function (key, value) {
            var sect ='';
            if(subcate == key){
              sect='selected';
            }
            html += "<option value="+key+" "+sect+">"+value+"</option>";
          });
          $(".subcategory").html(html);
        }
      });

    $("#addTheme").click(function() {
      $(".themeform").toggle();

      if ($('.themename').attr('disabled')) {
         $('.themename').removeAttr('disabled');
      }else{
        $('.themename').attr('disabled','disabled');
      }
      if ($('.themefile').attr('disabled')) {
         $('.themefile').removeAttr('disabled');
      }else{
        $('.themefile').attr('disabled','disabled');
      }
      
    });

    $(".addMoretheme").click(function() {
      var the = $('.themerow').length;
      //alert(the);
      var theid  = 'cardthemes'+(the+1);
      $('.themeform').append('<div class="row themerow"><div class="col-md-4"><div class="form-group"><label for="picture">Add Card Theme</label><input type="text" class="form-control themename" name="theme[][name]"></div></div><div class="col-md-6"><div class="form-group" id = "'+theid+'"><label for="picture">Add Card Theme</label><div class="file-upload"><div class="image-upload-wrap"><input type="file" class="form-control file-upload-input themefile" name="theme[][image]" id="" onchange="readURL(this,`'+theid+'`)"accept="image/*"><div class="drag-text"><h3>Drag and drop or select Image</h3></div></div><div class="file-upload-content"><img class="file-upload-image" src="" alt="your image" /><div class="image-title-wrap"><button type="button" onclick="removeUpload(`'+theid+'`)" class="remove-image"><i class="fa fa-trash" aria-hidden="true"></i> <span class="image-title">Uploaded Image</span></button></div></div></div></div></div><div class="col-md-2"><button type="button" class="close removetheme" aria-label="Close"><span aria-hidden="true">&times;</span></button></div></div>');
    });
  });

  $(document).on('click', '.removetheme', function(e) {
      $(this).parents('.themerow').remove();
  });

  $(document).on('submit', '#addColorform', function(e) {
    e.preventDefault();
    var formdata = new FormData($('#addColorform')[0]);
    $.ajax({
     method: $(this).attr('method'),
     url:   $(this).attr('action'),
     data: formdata,
     processData: false,
     contentType: false,
         success: function(response) {
          console.log(response);
          if(response.status){
            $('.successmsg').text(response.massage).css('display','block');
            $('.errormsg').css('display','none');
            $('#colortype').append(new Option(response.title,response.name,true, true));
          }else{
            $('.errormsg').text(response.massage).css('display','block');
            $('.successmsg').css('display','none');
          }

         }
    });
});

$('body').on('submit', '#vistingcards', function(e) {
  e.preventDefault();
  var formId = $(this).attr('id');
  var formdataId = $(this).data("id");
  $('#'+formId).removeClass('border-red-error');
  $('#'+formId).find(".error").remove();
  $('#'+formId).find(".border-red").removeClass('border-red');
  var form = $('#'+formId);
  $.ajax({
      type: $(this).attr('method'),
      url: $(this).attr('action'),
      data: new FormData(this),
      processData: false,
      contentType: false,
      // beforeSend: function() {
      //     showLoader();
      // },
      success: function(data) {
          if (data.success)
          {
              $(this).find("button[type='submit']").prop('disabled', true);
              //hideLoader();
              if (data.message != '') {
                $('.ajaxtoast').css('display','block');
                $('.ajaxtoast').find('.toast-body').text(data.message);
                // $('.image-upload-wrap').css('display','block');
                // $('.file-upload-content').css('display','none');
                // $('.themerow').html();
              }
              //$("#vistingcards").trigger("reset");
              //refreshDiv();
               if (data.extra.redirect)
               {
                setTimeout(function(){ window.location.href = data.extra.redirect; }, 1500);
               }
          } else {
             // hideLoader();
              $.each(data.errors, function(i, v) {
                  var error = '<div class="error">' + v + '</div>';
                  var split = i.split('.');
                  if (split[2]) {
                      var ind = split[0] + '[' + split[1] + ']' + '[' + split[2] + ']';
                      form.find("[name='" + ind + "']").addClass('border-red');
                      form.find("[name='" + ind + "']").parent().append(error);
                  } else if (split[1]) {
                      var ind = split[0] + '[' + split[1] + ']';
                      form.find("[name='" + ind + "']").addClass('border-red');
                      form.find("[name='" + ind + "']").parent().append(error);
                  } else {
                      form.find("[name='" + i + "']").addClass('border-red');
                      form.find("[name='" + i + "']").parent().append(error);
                  }
              });
              $('#'+formId).addClass('border-red-error');
              $(".border-red").first().focus();
          }
      },
      error: function(data) {
          console.log('An error occurred.');
      }
  });
});

</script>
@endpush
@push('styles')
 <link href=" {{ asset('dashboard/assets/css/select2.min.css') }}" rel="stylesheet">
@endpush