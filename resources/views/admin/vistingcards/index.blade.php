@extends('admin.layouts.app')
@section('content')
<div class="main-panel">
  <div class="content right_body">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header card-header-primary custom_header_4">
                <h4 class="card-title ">Visiting Cards</h4>
                <p class="card-category">List of visiting cards</p>
              </div>
              <div class="card-body">
                <div class="table-responsive">

                  <a href="{{ route('admin.vistingcards.create') }}" class="btn btn-primary add_new_data">Add New <i class="fa fa-plus-circle" aria-hidden="true"></i></a>
                  <table class="tableHosting table-striped custom_table">

                    <thead class=" text-primary">
                      <th>ID</th>
                      <th>Title</th>
                      <th>Image</th>
                      <th>Description</th>
                      <th>Price</th>
                      <th>Discount Type</th>
                      <th>Discount</th>
                      <th>Action</th>
                    </thead>
                    <tbody>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
  $(document).ready(function() {
    $('.tableHosting').DataTable({
      "order": [[ 0, "desc" ]],
        processing: true,
        serverSide: true,
        ajax: "{{ route('admin.vistingcards.index') }}",
        columns: [
            {data: 'id', name: 'id'},
            {data: 'cardtitle', name: 'cardtitle'},
            {data: 'picture', name: 'picture',
              render: function( data, type, full, meta ) {
                return "<img src='" + data + "' height='50'/>";
              },
              orderable: false,
              searchable: false
            },
            {data: 'carddescription', name: 'carddescription'},
            {data: 'price', name: 'price'},
            {data: 'discounttype', name: 'discounttype'},
            {data: 'discount', name: 'discount'},
            {
              data: 'action', 
              name: 'action', 
              orderable: true, 
              searchable: true
            },
        ]
    });
  } );

  $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': "{{ csrf_token() }}"
          }
      });
  function deleteData(e){
    var table = $('.tableHosting').DataTable();
    var id = e.getAttribute('data-id');
    var url = e.getAttribute('data-url');
      swal({
          title: "Are you sure?",
          text: "Once deleted, you will not be able to recover this imaginary file!",
          icon: "warning",
          buttons: true,
          dangerMode: true,
      })
      .then((willDelete) => {
          if (willDelete) {
              $.ajax({
                  url : url,
                  type : "POST",
                  data : {'_method' : 'DELETE'},
                  success: function(){
                      swal({
                          title: "Success!",
                          text : "Post has been deleted \n Click OK to refresh the page",
                          icon : "success",
                      }).then(function(){
                          $(e).closest("tr").remove();
                      });
                  },
                  error : function(){
                      swal({
                          title: 'Opps...',
                          text : "Something Wrong",
                          type : 'error',
                          timer : '1500'
                      })
                  }
              })
          } else {
          swal("Your imaginary file is safe!");
          }
      });
  }

  function Isenabled(e){
    var eid = e.getAttribute('data-id');
    $.ajax({
        url: "{{route('admin.EnableCard')}}",
        type : "POST",
        data:{
          'id':eid
        },
        success: function(data) {
          location.reload();
          // if(data.is_enabled==1){
          //   e.text('Save').button("Enabled");
          // }else{
          //   e.text('Save').button("Disabled");
          // }
        },
    });
  }
</script>
@endpush