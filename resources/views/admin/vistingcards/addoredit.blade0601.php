@extends('admin.layouts.app')
@section('content')
<div class="main-panel">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="create_header">
					<p>Create Card Theme</p>
				</div>
			</div>
		</div>
	</div>
  <div class="content creation_form_outer">
      <div class="container-fluid">		
        <div class="row">
          <div class="col-md-12">
            @if(isset($card))
              {{ Form::model($card, ['route' => ['admin.vistingcards.update', $card->id], 'method' => 'patch','files'=> true]) }}
          @else
              {{ Form::open(['route' => 'admin.vistingcards.store','files'=> true]) }}
          @endif
            <div class="row">
              <div class="col-md-12">
                <div class="form-group required">
                  <label for="cardtitle">Card Tittle</label>
                  {{ Form::text('cardtitle',old('cardtitle'),['class'=>'form-control','placeholder'=>'Add Title','id'=>'cardtitle','required'=>1]) }}
                  @if($errors->has('cardtitle'))
                    <div class="error">{{ $errors->first('cardtitle') }}</div>
                  @endif
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group required">
                  <label for="cardtitle">Card Description</label>
                  {{ Form::textarea('carddescription',old('carddescription'),['class'=>'form-control','placeholder'=>'Write Discription...','id'=>'carddescription','cols'=>5,'rows'=>4,'required'=>1]) }}
                  @if($errors->has('carddescription'))
                    <div class="error">{{ $errors->first('carddescription') }}</div>
                  @endif
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group " id = "cardUploader">
					<label for="picture">Card Picture</label>
					<div class="file-upload">
					  <div class="image-upload-wrap" @if(!empty($card->picture)) style = "display:none" @endif>
						{{ Form::file('picture',['class'=>'form-control file-upload-input','id'=>'picture','onchange'=>'readURL(this,"cardUploader")','accept'=>'image/*']) }}
						  @if($errors->has('picture'))
							<div class="error">{{ $errors->first('picture') }}</div>
						  @endif
						<div class="drag-text">
						  <h3>Drag and drop or select Image</h3>
						</div>
					  </div>
					  <div class="file-upload-content" @if(!empty($card->picture)) style = "display:block" @endif>
						<img class="file-upload-image" src='{{ url("$card->picture") }}' alt="your image" />
						<div class="image-title-wrap">
						  <button type="button" onclick="removeUpload('cardUploader')" class="remove-image"><i class="fa fa-trash" aria-hidden="true"></i> <span class="image-title">Uploaded Image</span></button>
						</div>
					  </div>
					</div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="crosel_image">Carousel Image</label>
				  <div class="file-upload" id = "carouselUploader">
					  <div class="image-upload-wrap" @if(!empty($card->crosel_image)) style = "display:none" @endif>
						{{ Form::file('crosel_image',['class'=>'form-control file-upload-input','id'=>'crosel_image','onchange'=>'readURL(this,"carouselUploader")','accept'=>'image/*']) }}
						  @if($errors->has('crosel_image'))
							<div class="error">{{ $errors->first('crosel_image') }}</div>
						  @endif
						<div class="drag-text">
						  <h3>Drag and drop or select Image</h3>
						</div>
					  </div>
					  <div class="file-upload-content" @if(!empty($card->crosel_image)) style = "display:block" @endif>
						<img class="file-upload-image" src='{{ url("$card->crosel_image") }}' alt="your image" />
						<div class="image-title-wrap">
						  <button type="button" onclick="removeUpload('carouselUploader')" class="remove-image"><i class="fa fa-trash" aria-hidden="true"></i> <span class="image-title">Uploaded Image</span></button>
						</div>
					  </div>
					</div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group required">
                  <label for="cardtitle">Category</label>
                  {{ Form::select('category_id',$categories,old('category_id'),['class'=>'form-control category','placeholder'=>'---Choose Category---','id'=>'category_id','required'=>true]) }}
                    @if($errors->has('category_id')) }}
                      <div class="error">{{ $errors->first('category_id') }}</div>
                    @endif
                </div>
              </div>
               <div class="col-md-6">
                <div class="form-group">
                  <label for="cardtitle">Sub Category</label>
                  <input type="hidden" id="subcategoryid" value="{{(isset($card) ? $card->sub_category_id : '')}}">
                  {{ Form::select('sub_category_id',[],old('sub_category_id'),['class'=>'form-control subcategory','id'=>'sub_category_id']) }}
                    @if($errors->has('sub_category_id')) }}
                      <div class="error">{{ $errors->first('sub_category_id') }}</div>
                    @endif
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="form-group required">
                  <label for="cardtitle">Card Actual price</label>
                  {{ Form::number('actualprice',old('actualprice'),['class'=>'form-control','placeholder'=>'Amount','id'=>'actualprice','required'=>1]) }}
                  @if($errors->has('actualprice'))
                    <div class="error">{{ $errors->first('actualprice') }}</div>
                  @endif
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group required">
                  <label for="cardtitle">Discount Type</label>
                  {{ Form::select('discounttype', [ ''=>'select', '1'=>'Percentage', '2'=>'Flat off'],old('discounttype'),['class'=>'form-control','id'=>'discounttype','required'=>true]) }}
                    @if($errors->has('discounttype')) }}
                      <div class="error">{{ $errors->first('discounttype') }}</div>
                    @endif
                </div>
              </div>
              <div class="col-md-4">
                <div class="form-group required">
                  <label for="cardtitle">Discount</label>
                  {{ Form::number('discount',old('discount'),['class'=>'form-control','placeholder'=>'Value','id'=>'discount','required'=>1]) }}
                  @if($errors->has('discount'))
                    <div class="error">{{ $errors->first('discount') }}</div>
                  @endif
                </div>
              </div>
             {{-- <div class="col-md-3">
                <div class="form-group required">
                  <label for="cardtitle">Actual price</label>
                  {{ Form::text('actualprice',old('actualprice'),['class'=>'form-control','placeholder'=>'Amount','id'=>'actualprice','readonly'=>true]) }}
                  @if($errors->has('actualprice'))
                    <div class="error">{{ $errors->first('actualprice') }}</div>
                  @endif
                </div>
              </div>
              --}}
            </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group required">
                    <label for="cardtype">Type of card</label>
                   {{ Form::select('cardtype', [ ''=>'select', '1'=>'Business', '2'=>'Personal'],old('cardtype'),['class'=>'form-control','id'=>'cardtype','required'=>true]) }}
                    @if($errors->has('cardtype')) }}
                      <div class="error">{{ $errors->first('cardtype') }}</div>
                    @endif
                  </div>
                </div>
                <div class="col-md-12">
                  <div class="form-group required">
                    <label for="colortype">Color Type</label>
                    {{ Form::select('colortype[]',colorTypes(),old('colortype[]'),['class'=>'form-control multiselect','id'=>'colortype','multiple'=>'multiple','required'=>1]) }}
                    @if($errors->has('colortype'))
                      <div class="error">{{ $errors->first('colortype') }}</div>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group required">
                    <label for="maxsocialmedia">Mix. Number of Social Media</label>
                    {{ Form::number('maxsocialmedia',old('maxsocialmedia'),['class'=>'form-control','placeholder'=>'Choose Units','id'=>'maxsocialmedia','required'=>1]) }}
                    @if($errors->has('maxsocialmedia'))
                      <div class="error">{{ $errors->first('maxsocialmedia') }}</div>
                    @endif
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group required">
                    <label for="minsocialmedia">Min. Number of Social Media</label>
                    {{ Form::number('minsocialmedia',old('minsocialmedia'),['class'=>'form-control','placeholder'=>'Choose Units','id'=>'minsocialmedia','required'=>1]) }}
                    @if($errors->has('minsocialmedia'))
                      <div class="error">{{ $errors->first('minsocialmedia') }}</div>
                    @endif
                  </div>
                </div>
              </div>
              <div class="row">
                  <div class="form-group col-md-4 required">
                    <label for="profile">Do you want profile image ?</label>
					<div class="custom_swicth">
						{{Form::radio('profile', 1 , old('profile') == 1 ? 'checked' : (isset($card->profile) && $card->profile == 1 ? 'checked' : '') , ['id'=>'profileyes','class'=>'input_yes'])}}
						{{Form::radio('profile', 0 , old('profile') == 0 ? 'checked' : (isset($card->profile) && $card->profile == 0 ? 'checked' : '') ,['id'=>'profileno','class'=>'input_no'])}}
						<div class="switch">
							<label class="yess" for="profileyes">Yes</label>
							<label class="nope" for="profileno">No</label>
							<span></span>
						</div>
					</div>
				  </div>
                  <div class="form-group col-md-4">
                    <label for="logo">Do you want logo ?</label>
					<div class="custom_swicth">
						{{Form::radio('logo',1, old('logo') == 1 ? 'checked' : (isset($card->logo) && $card->logo == 1 ? 'checked' : ''),['id'=>'logoyes','class'=>'input_yes'])}}
						{{Form::radio('logo',0,old('logo') == 0 ? 'checked' : (isset($card->logo) && $card->logo == 0 ? 'checked' : ''),['id'=>'logono','class'=>'input_no'])}}
						<div class="switch">
							<label class="yess" for="logoyes">Yes</label>
							<label class="nope" for="logono">No</label>
							<span></span>
						</div>
					</div>
                  </div>
                  <div class="form-group col-md-4">
                    <label for="background">Do you want background image ?</label>
					
					<div class="custom_swicth">
						{{Form::radio('background',1, old('background') == 1 ? 'checked' : (isset($card->background) && $card->background == 1 ? 'checked' : ''),['id'=>'backgroundyes','class'=>'input_yes'])}}
						{{Form::radio('background',0,old('background') == 0 ? 'checked' : (isset($card->background) && $card->background == 0 ? 'checked' : ''),['id'=>'backgroundno','class'=>'input_no'])}}
						<div class="switch">
							<label class="yess" for="backgroundyes">Yes</label>
							<label class="nope" for="backgroundno">No</label>
							<span></span>
						</div>
					</div>
                  </div>
                </div>
                <div class="row">
                  <div class="form-group col-md-12">
                    <label for="gallery">Do you want gallery image ?</label>
                    <div class="custom_swicth">
						{{Form::radio('gallery',1, old('gallery') == 1 ? 'checked' : (isset($card->gallery) && $card->gallery == 1 ? 'checked' : ''),['id'=>'galleryyes','class'=>'input_yes'])}}
						{{Form::radio('gallery',0,old('gallery') == 0 ? 'checked' : (isset($card->gallery) && $card->gallery == 0 ? 'checked' : ''),['id'=>'galleryno','class'=>'input_no'])}}
						<div class="switch">
							<label class="yess" for="galleryyes">Yes</label>
							<label class="nope" for="galleryno">No</label>
							<span></span>
						</div>
					</div>
					<div class="no_gallerydiv" style="display: none;">
						<div class="row">
							<div class="col-lg-6">
								<label for="max_no_gallery" class="col-form-label">Max. Number of gallery image</label>
								{{ Form::number('max_no_gallery',old('max_no_gallery'),['class'=>'form-control','placeholder'=>'Units','id'=>'max_no_gallery']) }}
							</div>
							<div class="col-lg-6">
								<label for="max_no_gallery" class="col-form-label">Min. Number of gallery image</label>
								{{ Form::number('min_no_gallery',old('min_no_gallery'),['class'=>'form-control','placeholder'=>'Units','id'=>'min_no_gallery']) }}
							</div>
						</div>                        
                    </div>
                  </div> 
                </div>
                <div class="row">
                  <div class="form-group col-md-12">
                    <label for="services">Do you want services ?</label>
					<div class="custom_swicth">
						{{Form::radio('services',1, old('services') == 1 ? 'checked' : (isset($card->services) && $card->services == 1 ? 'checked' : ''),['id'=>'servicesyes','class'=>'input_yes'])}}
						{{Form::radio('services',0,old('services') == 0 ? 'checked' : (isset($card->services) && $card->services == 0 ? 'checked' : ''),['id'=>'servicesno','class'=>'input_no'])}}
						<div class="switch">
							<label class="yess" for="servicesyes">Yes</label>
							<label class="nope" for="servicesno">No</label>
							<span></span>
						</div>
					</div>
					<div class="servicesdiv">
						<div class="row">
						  <div class="form-group col-md-6">
							<label for="no_gallery" class="col-form-label">Max. Number of services</label>
							{{ Form::number('max_no_services',old('max_no_services'),['class'=>'form-control','placeholder'=>'Units','id'=>'max_no_services']) }}
						  </div>
						  <div class="form-group col-md-6">
							<label for="no_gallery" class="col-form-label">Min. Number of services</label>
							{{ Form::number('min_no_services',old('min_no_services'),['class'=>'form-control','placeholder'=>'Units','id'=>'min_no_services']) }}
						  </div>						  
						</div>
						<div class="row">
							<div class="form-group col-md-12">
							<label for="serviceswithimg">Services with Images ?</label>
							<div class="custom_swicth">
								{{Form::radio('serviceswithimg',1,old('serviceswithimg') == 1 ? 'checked' : (isset($card->serviceswithimg) && $card->serviceswithimg == 1 ? 'checked' : ''),['id'=>'servicesimgyes','class'=>'input_yes'])}}
								{{Form::radio('serviceswithimg',0,old('serviceswithimg') == 0 ? 'checked' : (isset($card->serviceswithimg) && $card->serviceswithimg == 0 ? 'checked' : ''),['id'=>'servicesimgno','class'=>'input_no'])}}
								<div class="switch">
									<label class="yess" for="servicesimgyes">Yes</label>
									<label class="nope" for="servicesimgno">No</label>
									<span></span>
								</div>
							</div>
						  </div>
						</div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <fieldset class="">
                      <legend>Required Details:</legend>
                      <div class="form-group required form-check form-check-inline">
						<div class="chiller_cb">
							{{ Form::checkbox('username', 1, old('username') == 1 ? 'checked' : (isset($card->username) && $card->username == 1 ? 'checked' : ''),['id'=>'username', 'class'=>'form-check-input','required'=>1]) }}
							<label class="form-check-label" for="username">Name</label>	
							<span></span>							
                        </div>
                      </div>

                      <div class="form-group required form-check form-check-inline">
						<div class="chiller_cb">
							{{ Form::checkbox('primaryphone', 1, old('primaryphone') == 1 ? 'checked' : (isset($card->primaryphone) && $card->primaryphone == 1 ? 'checked' : ''),['id'=>'primaryphone', 'class'=>'form-check-input','required'=>1]) }}
							<label class="form-check-label" for="primaryphone">Primary Phone</label>
							<span></span>
						</div>
                      </div>
					  
                      <div class="form-group form-check form-check-inline">
                        <div class="chiller_cb">
							{{ Form::checkbox('secondaryphone', 1,old('secondaryphone') == 1 ? 'checked' : (isset($card->secondaryphone) && $card->secondaryphone == 1 ? 'checked' : ''),['id'=>'secondaryphone', 'class'=>'form-check-input']) }}
							<label class="form-check-label" for="secondaryphone">Secondary Phone</label>
							<span></span>
						</div>
                      </div>
					  
                      <div class="form-group required form-check form-check-inline">
                        <div class="chiller_cb">
							{{ Form::checkbox('primaryemail', 1,old('primaryemail') == 1 ? 'checked' : (isset($card->primaryemail) && $card->primaryemail == 1 ? 'checked' : ''),['id'=>'primaryemail', 'class'=>'form-check-input','required'=>1]) }}
							<label class="form-check-label" for="primaryemail">Primary Email</label>
							<span></span>
						</div>
                      </div>
					  
                      <div class="form-group form-check form-check-inline">
                        <div class="chiller_cb">
							{{ Form::checkbox('secondaryemail', 1,old('secondaryemail') == 1 ? 'checked' : (isset($card->secondaryemail) && $card->secondaryemail == 1 ? 'checked' : ''),['id'=>'secondaryemail', 'class'=>'form-check-input']) }}
							<label class="form-check-label" for="secondaryemail">Secondary Email</label>
							<span></span>
						</div>
                      </div>
					  
                      <div class="form-group required form-check form-check-inline">
                        <div class="chiller_cb">
							{{ Form::checkbox('address', 1,old('address') == 1 ? 'checked' : (isset($card->address) && $card->address == 1 ? 'checked' : ''),['id'=>'address', 'class'=>'form-check-input','required'=>1]) }}
							<label class="form-check-label" for="address">Address</label>
							<span></span>
						</div>
                      </div>
					  
                      <div class="form-group form-check form-check-inline">
                        <div class="chiller_cb">
							{{ Form::checkbox('location', 1,old('location') == 1 ? 'checked' : (isset($card->location) && $card->location == 1 ? 'checked' : ''),['id'=>'location', 'class'=>'form-check-input']) }}
							<label class="form-check-label" for="location">Location</label>
							<span></span>
						</div>
                      </div>
					  
                      <div class="form-group form-check form-check-inline">
                        <div class="chiller_cb">
							{{ Form::checkbox('website', 1,old('website') == 1 ? 'checked' : (isset($card->website) && $card->website == 1 ? 'checked' : ''),['id'=>'website', 'class'=>'form-check-input']) }}
							<label class="form-check-label" for="website">Website</label>
							<span></span>
						</div>
                      </div>
					  
                      <div class="form-group form-check form-check-inline">
						<div class="chiller_cb">
							{{ Form::checkbox('designation', 1,old('designation') == 1 ? 'checked' : (isset($card->designation) && $card->designation == 1 ? 'checked' : ''),['id'=>'designation', 'class'=>'form-check-input']) }}
							<label class="form-check-label" for="designation">Designation</label>
							<span></span>
						</div>
                      </div>
					  
                      <div class="form-group form-check form-check-inline">
                        <div class="chiller_cb">
							{{ Form::checkbox('description', 1,old('description') == 1 ? 'checked' : (isset($card->description) && $card->description == 1 ? 'checked' : ''),['id'=>'description', 'class'=>'form-check-input']) }}
							<label class="form-check-label" for="description">Description</label>
							<span></span>
						</div>
							<div class="classdescription">
							  <label for="no_gallery" class="col-form-label">Min. Number of Description</label>
							  {{ Form::text('min_no_description',old('min_no_description'),['class'=>'form-control','id'=>'min_no_services']) }}
							</div>
                      </div>
					  
					  <div class="form-group form-check form-check-inline">
                        <div class="chiller_cb"> 
							{{ Form::checkbox('company', 1,old('company') == 1 ? 'checked' : (isset($card->company) && $card->company == 1 ? 'checked' : ''),['id'=>'company', 'class'=>'form-check-input']) }}
							<label class="form-check-label" for="company">Company name</label>
							<span></span>
						</div>
                      </div>
					  
					  <div class="form-group form-check form-check-inline">
						<div class="chiller_cb">
                          {{ Form::checkbox('companyabout', 1,old('companyabout') == 1 ? 'checked' : (isset($card->companyabout) && $card->companyabout == 1 ? 'checked' : ''),['id'=>'companyabout', 'class'=>'form-check-input']) }}
						  <label class="form-check-label" for="companyabout">About Company</label>
						  <span></span>
						</div>
                      </div>
                      
                    </fieldset>
                  </div>
                </div>
              <div class="row">
                <div class="form-group col-md-6">
                   {!! Form::submit('Submit', ['class' => 'btn btn-primary']) !!}
                </div>
              </div>
          {{ Form::close() }}
          </div>
        </div>
      </div>
  </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('dashboard/assets/js/select2.min.js') }}"></script>
<script type="text/javascript">
  if ($('#description').is(':checked')) {

    $('.classdescription').css('display','block');
  }else{
    $('.classdescription').css('display','none');
  }

  $('#description').change(function() {
      if ($(this).is(':checked')) {
          $('.classdescription').css('display','block');
      }
      else {
        $('.classdescription').css('display','none');
      }
  });

  if ($('#galleryyes').is(':checked')) {

    $('.no_gallerydiv').css('display','block');
  }else{
    $('.no_gallerydiv').css('display','none');
  }


  $('input[type=radio][name=gallery]').change(function() {
      if (this.value == 1) {
          $('.no_gallerydiv').css('display','block');
      }
      else {
        $('.no_gallerydiv').css('display','none');
      }
  });

  if ($('#servicesyes').is(':checked')) {

    $('.servicesdiv').css('display','block');
  }else{
    $('.servicesdiv').css('display','none');
  }

  $('input[type=radio][name=services]').change(function() {
      if (this.value == 1) {
          $('.servicesdiv').css('display','block');
      }
      else {
        $('.servicesdiv').css('display','none');
      }
  });

  $( ".multiselect" ).select2( {
    } );

  $('.category').change(function() {
    var cate = $(this).val();
      $.ajax({
        url:"{{route('admin.getSubcategory')}}",
        method:'get',
        data:{
          category_id:cate
        },
        success: function(res){
          var html = "";
          $.each(res, function (key, value) {
            html += "<option value="+key+">"+value+"</option>";
          });
          $(".subcategory").html(html);
        }
      });
  });

  $( document ).ready(function() {
    var cate = $('.category').val();
    var subcate = $('#subcategoryid').val();
    $.ajax({
        url:"{{route('admin.getSubcategory')}}",
        method:'get',
        data:{
          category_id:cate
        },
        success: function(res){
          var html = "";
          $.each(res, function (key, value) {
            var sect ='';
            if(subcate == key){
              sect='selected';
            }
            html += "<option value="+key+" "+sect+">"+value+"</option>";
          });
          $(".subcategory").html(html);
        }
      });
  });
</script>
@endpush
@push('styles')
 <link href=" {{ asset('dashboard/assets/css/select2.min.css') }}" rel="stylesheet">
@endpush