@extends('admin.layouts.app')
   
@section('content')
<div class="main-panel">
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-warning card-header-icon">
              <div class="card-icon">
                <i class="fa fa-users" aria-hidden="true"></i>
              </div>
              <p class="card-category">Total Users</p>
              <h3 class="card-title">
                {{$totalusers}}
              </h3>
            </div>
            <div class="card-footer">
              <div class="stats">
               <i class="fa fa-users" aria-hidden="true"></i>  &nbsp; &nbsp; Total Users
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-success card-header-icon">
              <div class="card-icon">
               <i class="fa fa-user-plus" aria-hidden="true"></i>
              </div>
              <p class="card-category">New Users</p>
              <h3 class="card-title">{{$newusers}}</h3>
            </div>
            <div class="card-footer">
              <div class="stats">
               <i class="fa fa-calendar-o" aria-hidden="true"></i> &nbsp; &nbsp; This Month New Users 
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-danger card-header-icon">
              <div class="card-icon">
                <i class="fa fa-cart-arrow-down" aria-hidden="true"></i>
              </div>
              <p class="card-category">Total Orders</p>
              <h3 class="card-title">{{$totalorder}}</h3>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="fa fa-cart-arrow-down" aria-hidden="true"></i> &nbsp; &nbsp; Total Orders
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
              <div class="card-icon">
                <i class="fa fa-cart-plus" aria-hidden="true"></i>
              </div>
              <p class="card-category">Today's Oders </p>
              <h3 class="card-title">{{$todayorder}}</h3>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="fa fa-clock-o" aria-hidden="true"></i> &nbsp; &nbsp; Today's Oders
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6">
          <div class="card card-stats">
            <div class="card-header card-header-info card-header-icon">
              <div class="card-icon">
                <i class="fa fa-product-hunt" aria-hidden="true"></i>
              </div>
              <p class="card-category">Pending Oders </p>
              <h3 class="card-title">{{$pendingorder}}</h3>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="fa fa-clock-o" aria-hidden="true"></i> &nbsp; &nbsp; Pending Oders
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- <div class="row">
        <div class="col-md-4">
          <div class="card card-chart">
            <div class="card-header card-header-success">
              <div class="ct-chart" id="dailySalesChart"></div>
            </div>
            <div class="card-body">
              <h4 class="card-title">Daily Sales</h4>
              <p class="card-category">
                <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> increase in today sales.</p>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons">access_time</i> updated 4 minutes ago
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card card-chart">
            <div class="card-header card-header-warning">
              <div class="ct-chart" id="websiteViewsChart"></div>
            </div>
            <div class="card-body">
              <h4 class="card-title">Email Subscriptions</h4>
              <p class="card-category">Last Campaign Performance</p>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons">access_time</i> campaign sent 2 days ago
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-4">
          <div class="card card-chart">
            <div class="card-header card-header-danger">
              <div class="ct-chart" id="completedTasksChart"></div>
            </div>
            <div class="card-body">
              <h4 class="card-title">Completed Tasks</h4>
              <p class="card-category">Last Campaign Performance</p>
            </div>
            <div class="card-footer">
              <div class="stats">
                <i class="material-icons">access_time</i> campaign sent 2 days ago
              </div>
            </div>
          </div>
        </div>
      </div> -->
    </div>
  </div>
</div>
@endsection

@push('scripts')
  <script>
    $(document).ready(function() {
      // Javascript method's body can be found in assets/js/demos.js
      md.initDashboardPageCharts();

    });
  </script>
@endpush