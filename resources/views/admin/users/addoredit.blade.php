@extends('admin.layouts.app')
@section('content')
<div class="main-panel">
  <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            @if(isset($user))
              {{ Form::model($user, ['route' => ['admin.users.update', $user->id], 'method' => 'patch']) }}
          @else
              {{ Form::open(['route' => 'admin.users.store']) }}
          @endif
              <div class="row">
                <div class="form-group col-md-6">
                  <label for="name" class="col-form-label">Name</label>
                    {{ Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Enter name','id'=>'name']) }}
                    @if($errors->has('name'))
                        <div class="error">{{ $errors->first('name') }}</div>
                    @endif
                </div>
                <div class="form-group col-md-6">
                  <label for="email" class="col-form-label">Email</label>
                  {{ Form::text('email',old('email'),['class'=>'form-control','placeholder'=>'Enter email','id'=>'email']) }}
                  @if($errors->has('email'))
                      <div class="error">{{ $errors->first('email') }}</div>
                  @endif
                </div>

                <div class="form-group col-md-6">
                  <label for="phone" class="col-form-label">Phone</label>
                  {{ Form::text('phone',old('phone'),['class'=>'form-control','placeholder'=>'Enter phone number','id'=>'phone']) }}
                  @if($errors->has('phone'))
                      <div class="error">{{ $errors->first('phone') }}</div>
                  @endif
                </div>

              @if(isset($user))
                  <div class="form-group col-md-6">
                  <label for="password" class="col-form-label">Password</label>
                    {{ Form::password('password',['class'=>'form-control','id'=>'password', 'placeholder'=>'Enter password','disabled'=>true]) }}
                    @if($errors->has('password'))
                        <div class="error">{{ $errors->first('password') }}</div>
                    @endif
                </div>
              @else
                <div class="form-group col-md-6">
                  <label for="passowrd" class="col-form-label">Password</label>
                    {{ Form::password('password',['class'=>'form-control','placeholder'=>'Enter password','id'=>'password']) }}
                    @if($errors->has('password'))
                        <div class="error">{{ $errors->first('password') }}</div>
                    @endif
                </div>
              @endif
              </div>
              <div class="row">
                <div class="form-group col-md-6">
                   {!! Form::submit('Submit', ['class' => 'btn btn-primary submit_btn_new']) !!}
                </div>
              </div>
          </form>
          </div>
        </div>
      </div>
  </div>
</div>
@endsection

@push('styles')
<style type="text/css">
  ul {
    list-style: none;
  }
  li {
    margin-left: -20px;
  }
</style>
@endpush
@push('scripts')
<!-- <script type="text/javascript">
$(function () {
    $('input:checkbox.main-checkbox').click(function () {
        var array = [];
        var parent = $(this).closest('.main-parent');
        //check or uncheck sub-checkbox
        $(parent).find('.sub-checkbox').prop("checked", $(this).prop("checked"))
        //push checked sub-checkbox value to array
        $(parent).find('.sub-checkbox:checked').each(function () {
            array.push($(this).val());
        })
    });

    $('input:checkbox.sub-checkbox').click(function () {
      var ii = $(this).closest('.main-parent').find('input:checked.sub-checkbox').length;
      var jj = $(this).closest('.main-parent').find('.sub-checkbox').length;
        if(ii==jj){
          $(this).closest('.main-parent').find('.main-checkbox').prop('checked', true);
        }else{
          $(this).closest('.main-parent').find('.main-checkbox').prop('checked', false);
        }
    });
})
</script> -->
@endpush