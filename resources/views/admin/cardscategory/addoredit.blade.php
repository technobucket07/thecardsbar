@extends('admin.layouts.app')
@section('content')
<div class="main-panel">
  <div class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            @if(isset($category))
              {{ Form::model($category, ['route' => ['admin.cardscategory.update', $category->id], 'method' => 'patch','files'=> true]) }}
          @else
              {{ Form::open(['route' => 'admin.cardscategory.store','files'=> true]) }}
          @endif
            <div class="row">
              <div class="col-md-6">
                <div class="form-group required">
                  <label for="name">Category Name</label>
                  {{ Form::text('name',old('name'),['class'=>'form-control','placeholder'=>'Enter name','id'=>'name','required'=>1]) }}
                  @if($errors->has('name'))
                    <div class="error">{{ $errors->first('name') }}</div>
                  @endif
                </div>
              </div>
              <div class="col-md-6">
                <!-- <div class="form-group required">
                  <label for="name">Icon</label>
                  {{ Form::text('icon',old('icon'),['class'=>'form-control','placeholder'=>'Choose Icon','id'=>'icon']) }}
                  @if($errors->has('icon'))
                    <div class="error">{{ $errors->first('icon') }}</div>
                  @endif
                </div> -->
                <div class="form-group " id = "categoryimage">
                  <label for="image">Category Image</label>
                  <div class="file-upload">
                    <div class="image-upload-wrap" @if(isset($category)) @if(!empty($category->image)) style = "display:none" @endif @endif>
                    {{ Form::file('image',['class'=>'form-control file-upload-input','id'=>'image','onchange'=>'readURL(this,"categoryimage")','accept'=>'image/*']) }}
                      @if($errors->has('image'))
                      <div class="error">{{ $errors->first('image') }}</div>
                      @endif
                    <div class="drag-text">
                      <h3>Drag and drop or select Image</h3>
                    </div>
                    </div>
                    <div class="file-upload-content" @if(isset($category)) @if(!empty($category->image)) style = "display:block" @endif @endif>
                    <img class="file-upload-image" src='@if(isset($category)) {{ url("storage/$category->image") }} @endif' alt="your image" />
                    <div class="image-title-wrap">
                      <button type="button" onclick="removeUpload('categoryimage')" class="remove-image"><i class="fa fa-trash" aria-hidden="true"></i> <span class="image-title">Uploaded Image</span></button>
                    </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label for="description">Description</label>
                  {{ Form::textarea('description',old('description'),['class'=>'form-control','id'=>'description','cols'=>5,'rows'=>4]) }}
                  @if($errors->has('description'))
                    <div class="error">{{ $errors->first('description') }}</div>
                  @endif
                </div>
              </div> 
            </div>
              <div class="row">
                <div class="form-group col-md-6">
                   {!! Form::submit('Submit', ['class' => 'btn btn-primary submit_btn_new']) !!}
                </div>
              </div>
          {{ Form::close() }}
          </div>
        </div>
      </div>
  </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('dashboard/assets/js/select2.min.js') }}"></script>
<script type="text/javascript">
  if ($('#description').is(':checked')) {

    $('.classdescription').css('display','block');
  }else{
    $('.classdescription').css('display','none');
  }

  $('#description').change(function() {
      if ($(this).is(':checked')) {
          $('.classdescription').css('display','block');
      }
      else {
        $('.classdescription').css('display','none');
      }
  });

  if ($('#galleryyes').is(':checked')) {

    $('.no_gallerydiv').css('display','block');
  }else{
    $('.no_gallerydiv').css('display','none');
  }


  $('input[type=radio][name=gallery]').change(function() {
      if (this.value == 1) {
          $('.no_gallerydiv').css('display','block');
      }
      else {
        $('.no_gallerydiv').css('display','none');
      }
  });

  if ($('#servicesyes').is(':checked')) {

    $('.servicesdiv').css('display','block');
  }else{
    $('.servicesdiv').css('display','none');
  }

  $('input[type=radio][name=services]').change(function() {
      if (this.value == 1) {
          $('.servicesdiv').css('display','block');
      }
      else {
        $('.servicesdiv').css('display','none');
      }
  });

  $( ".multiselect" ).select2( {
    } );
</script>
@endpush
@push('styles')
 <link href=" {{ asset('dashboard/assets/css/select2.min.css') }}" rel="stylesheet">
@endpush