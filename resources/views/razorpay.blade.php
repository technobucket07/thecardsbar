<!-- <button id="rzp-button1">Pay</button> -->
<!-- <script src="https://checkout.razorpay.com/v1/checkout.js"></script> -->
<!-- <script> -->
var options = {
    "key": "{{ env('RAZOR_KEY') }}",
    "amount": "{{$pay['amount']}}",
    "currency": "{{$pay['currency']}}",
    "name": "{{$pay['ordername']}}",
    "description": "{{$pay['description']}}",
    "image": "{{ env('RAZOR_IMAGE') }}",
    "order_id": "{{$pay['razor_order_id']}}",
    "handler": function (response){
         $.ajax({
           url: "{{route('razorpaysuccess')}}",
           type: 'post',
           data: {
            razorpay_payment_id: response.razorpay_payment_id ,
            order_id: "{{$pay['order_id']}}" ,
            razorpay_order_id: response.razorpay_order_id ,
            razorpay_signature: response.razorpay_signature ,
            _token:"{{ csrf_token() }}",
           }, 
           success: function (msg) {
            if(msg=='success'){
              window.location.href = "{{route('ThanksYou',1)}}";  
            }
           }
       });
    },
    "prefill": {
        "name": "{{$pay['username']}}",
        "email": "{{$pay['useremail']}}",
    },
    "notes": {
        "address": "{{$pay['address']}}"
    },
    "theme": {
        "color": "{{ env('RAZOR_THEME') }}"
    }
};
var rzp1 = new Razorpay(options);

 document.getElementById('rzp-button1').onclick = function(e){
    rzp1.open();
    e.preventDefault();
 }
<!-- </script>