<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Profile</title>
    <!--Core CSS -->
    <link href="{{url('asset/css/bootstrap.min.css?v=1')}}" rel="stylesheet" media="all">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="{{url('asset/css/main.css?v=61')}}" rel="stylesheet" media="all">




</head>
<body>

    <header>
        <section class="topHeader">
            <div class="firstName">James</div>
            <div class="lastName">Anderson</div>
            <div class="designation">Sales Manager</div>
        </section>
    </header>
    <!-- HEADER END HERE -->

    <!-- ABOUT STARTS HERE -->
    <section class="abtSec">
        <p class="userImg"><img class="img-fluid" src="{{url('asset/img/userimage.png')}}" alt="userimage"></p>
        <h1>About Me</h1>
        <p class="content">Lorem Ipsum is simply dummy text of the printing and typesetting.</p>
    </section>
    <!-- ABOUT END HERE -->

    <!-- CONTACT STARTS HERE -->
    <section class="infoSection">
        <h2>contact Me</h2>
        <ul class="list-unstyled">
            <li class="clearfix">
                <p class="contactIcon">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                </p>
                <div class="contactInfo">
                    <h6>Location</h6>
                    <p>14 Sti Kilda Road, USA</p>
                </div>
            </li>
            <li class="clearfix">
                <p class="contactIcon">
                    <i class="fa fa-mobile" aria-hidden="true"></i>
                </p>
                <div class="contactInfo">
                    <h6>Phone Number</h6>
                    <p><a href="#">+12 125 124 8574</a></p>
                </div>
            </li>
            <li class="clearfix">
                <p class="contactIcon">
                    <i class="fa fa-envelope messageIcon" aria-hidden="true"></i>
                </p>
                <div class="contactInfo">
                    <h6>Email Address</h6>
                    <p><a href="#">info@example.com</a></p>
                </div>
            </li>
        </ul>
    </section>
    <!-- CONTACT END HERE -->

    <!-- CONTACT STARTS HERE -->
    <section class="searchMe">
        <h2>search Me @</h2>
        <!--<ul class="list-unstyled socialMedia d-flex">-->
        <ul class="list-unstyled socialMedia">
            <li><a href="#" class="fa fa-facebook"></a></li>
            <li><a href="#" class="fa fa-skype"></a></li>
            <li><a href="#" class="fa fa-linkedin"></a></li>
            <li><a href="#" class="fa fa-instagram"></a></li>
            <li><a href="#" class="fa fa-twitter"></a></li>
        </ul>
    </section>
    <!-- CONTACT END HERE -->

    <!-- FOOTER STARTS HERE -->
    <footer>
        <section class="footer">
            <p class="webLink">
                <a href="#">www.information.com</a>
            </p>
            <p class="nameUser">James Anderson</p>
        </section>
    </footer>
    <!-- FOOTER END HERE -->



<!-- FOOTER END HERE -->
<!-- jQuery -->
<script src="{{url('asset/js/jquery.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{url('asset/js/bootstrap.min.js')}}"></script>
<script src="{{url('asset/js/proper.js')}}"></script>




</body>
</html>