@extends('frontend.layouts.master')
@section('content')
<!-- BACK BUTTON STARTS HERE -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <p class="backBtn">
                        <a class="btnCustomStyle2 btn-solid" href="index.html">
                            <span>Back to Sign in</span>
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </section>
    <!-- BACK BUTTON END HERE -->


    <!-- FORGOT PASSWORD STARTS HERE -->
    <section class="mainForgot">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 col-sm-12">
                   <div class="forgotPaword">
                     @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                        <h3>Forgot your password</h3>
                        <p>Please enter your email address and we'll send you instructions on how to reset your password.</p>
                        <form method="POST" action="{{ route('password.email') }}" aria-label="{{ __('Reset Password') }}">
                            @csrf
                            <div class="form-group">
                                <input  id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required placeholder="Enter you email...">
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <button type="submit" class="btnCustomStyle2 btn-solid">Recover Password</button>
                        </form>
                   </div>
                </div>
            </div>
        </div>
    </section>
    <!-- FORGOT PASSWORD END HERE -->
@endsection