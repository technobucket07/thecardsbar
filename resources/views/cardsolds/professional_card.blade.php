@extends('cards.layouts.master')
@section('content')
    <link href="{{ url('card/css/professional_card.css') }}" rel="stylesheet" media="all">
    <style>
        .TopBlankbox {
            background: {{$input['color1']}};
        }
        .mainbody {
            background: {{$input['color2']}};
        }

    </style>
    <section class="wrapper">
    <div class="TopBlankbox"></div>
    <div class="mainbody">
        <div class="logoMainBox">
            <p class="companyPic">
                <img class="img-fluid d-block mx-auto" src="{{url('card/logo/'.$input['logo'])}}" alt="userimage">
            </p>
            <h1 class="text-center text-uppercase">@if(isset($input['business_name'])){{$input['business_name']}}@endif</h1>
            <p class="text-center">@if(isset($input['tagline'])){{$input['tagline']}}@endif</p>
        </div>
        <div class="proprietorName">
            <ul class="list-unstyled">
                <li class="clearfix">
                    <p class="titleName">@if(isset($input['first_name'])){{$input['first_name']}}@endif  @if(isset($input['last_name'])){{$input['last_name']}}@endif</p>
                    <p class="calling">
                        <a href="tel:@if(isset($input['phone'])){{$input['phone']}}@endif"> Call <i class="fa fa-phone" aria-hidden="true"></i></a>
                    </p>
                </li>
                <li class="clearfix">
                    <p class="titleName">Harpreet Singh Gill</p>
                    <p class="calling">
                        <a href="#"> Call <i class="fa fa-phone" aria-hidden="true"></i></a>
                    </p>
                </li>
            </ul>
        </div>


        <div class="serviceoffersBox">
            <h2>Service Offers</h2>
            <div class="row">
                @if(isset($input['servicelogo']))
                    @foreach($input['servicelogo'] as $key=>$service_logo)
                        <div class="col-md-6">
                            <div class="user-card">
                                <p>
                                    <img src="{{url('card/services/'.$service_logo)}}" alt="ser-1" class="img-fluid d-block mx-auto">
                                </p>
                                <h4>@if(isset($input['service_name'][$key])){{$input['service_name'][$key]}}@endif </h4>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>

        <div class="socialMediaIcons">
            <ul class="list-unstyled">
                @foreach($input['social_media'] as $key=>$media)
                    <li><a href="{{$media}}" class="fa {{$socialMediaList[$key]}}"></a></li>
                @endforeach
            </ul>
        </div>



    </div>




</section>
@endsection