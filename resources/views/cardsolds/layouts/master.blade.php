<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Profile</title>
    <!--Core CSS -->
    <link href="{{ url('web_assets/css/bootstrap.min.css') }}" rel="stylesheet"  media="all">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">

</head>
<body>

@yield('content')

<!-- jQuery -->
<script src="{{ url('web_assets/js/jquery.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ url('web_assets/js/bootstrap.min.js') }}"></script>
<script src="{{ url('web_assets/js/proper.js') }}"></script>

</body>
</html>