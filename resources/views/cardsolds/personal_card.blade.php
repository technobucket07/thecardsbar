@extends('cards.layouts.master')
@section('content')
    <link href="{{ url('card/css/personal_card.css') }}" rel="stylesheet" media="all">
    <style>
        .abtSec{
            background:{{$input['color2']}}
        }
        .topHeader {
            background: {{$input['color1']}};
        }
        .infoSection ul li .contactIcon{
            background:{{$input['color2']}}
        }
        body  {font-family: 'Poppins', sans-serif;background: {{$input['color2']}};}
    </style>
    <header>
        <section class="topHeader">
            <div class="firstName">@if(isset($input['first_name'])){{$input['first_name']}}@endif</div>
            <div class="lastName">@if(isset($input['last_name'])){{$input['last_name']}}@endif</div>
            <div class="designation">@if(isset($input['designation'])){{$input['designation']}}@endif</div>
        </section>
    </header>
    <!-- HEADER END HERE -->

    <!-- ABOUT STARTS HERE -->
    <section class="abtSec">
        <p class="userImg"><img class="img-fluid" src="{{url('card/logo/'.$input['logo'])}}" alt="userimage"></p>
        <h1>About Me</h1>
        <p class="content">@if(isset($input['about'])){{$input['about']}}@endif</p>
    </section>
    <!-- ABOUT END HERE -->

    <!-- CONTACT STARTS HERE -->
    <section class="infoSection">
        <h2>contact Me</h2>
        <ul class="list-unstyled">
            <li class="clearfix">
                <p class="contactIcon">
                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                </p>
                <div class="contactInfo">
                    <h6>Location</h6>
                    <p>@if(isset($input['address'])){{$input['address']}}@endif</p>
                </div>
            </li>
            <li class="clearfix">
                <p class="contactIcon">
                    <i class="fa fa-mobile" aria-hidden="true"></i>
                </p>
                <div class="contactInfo">
                    <h6>Phone Number</h6>
                    <p><a href="tel:@if(isset($input['phone'])){{$input['phone']}}@endif">@if(isset($input['phone'])){{$input['phone']}}@endif</a></p>
                </div>
            </li>
            <li class="clearfix">
                <p class="contactIcon">
                    <i class="fa fa-envelope messageIcon" aria-hidden="true"></i>
                </p>
                <div class="contactInfo">
                    <h6>Email Address</h6>
                    <p><a href="#">@if(isset($input['email'])){{$input['email']}}@endif</a></p>
                </div>
            </li>
        </ul>
    </section>
    <!-- CONTACT END HERE -->

    <!-- CONTACT STARTS HERE -->
    @if(isset($input['social_media']))
    <section class="searchMe">
        <h2>search Me @</h2>
        <!--<ul class="list-unstyled socialMedia d-flex">-->
        <ul class="list-unstyled socialMedia">
            @foreach($input['social_media'] as $key=>$media)

                <li><a href="{{$media}}" class="fa {{$socialMediaList[$key]}}"></a></li>
            @endforeach

        </ul>
    </section>
    @endif
    <!-- CONTACT END HERE -->

    <!-- FOOTER STARTS HERE -->
    <footer>
        <section class="footer">
            <p class="webLink">
                <a href="#">@if(isset($input['website'])){{$input['website']}}@endif</a>
            </p>
            <p class="nameUser">@if(isset($input['first_name'])){{$input['first_name']}}@endif @if(isset($input['last_name'])){{$input['last_name']}} @endif</p>
        </section>
    </footer>
   @endsection