<?php

return [

   'server_error' => 'Something went wrong, please try again later.',
   'status' => 'Status updated.',
   'invalid_detail' => 'Invalid Details.',
   'deleted' => 'Record deleted.',

];
