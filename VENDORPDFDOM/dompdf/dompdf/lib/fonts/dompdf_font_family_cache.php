<?php return array (
  'sans-serif' => array(
    'normal' => $fontDir . '/Helvetica',
    'bold' => $fontDir . '/Helvetica-Bold',
    'italic' => $fontDir . '/Helvetica-Oblique',
    'bold_italic' => $fontDir . '/Helvetica-BoldOblique',
  ),
  'times' => array(
    'normal' => $fontDir . '/Times-Roman',
    'bold' => $fontDir . '/Times-Bold',
    'italic' => $fontDir . '/Times-Italic',
    'bold_italic' => $fontDir . '/Times-BoldItalic',
  ),
  'times-roman' => array(
    'normal' => $fontDir . '/Times-Roman',
    'bold' => $fontDir . '/Times-Bold',
    'italic' => $fontDir . '/Times-Italic',
    'bold_italic' => $fontDir . '/Times-BoldItalic',
  ),
  'courier' => array(
    'normal' => $fontDir . '/Courier',
    'bold' => $fontDir . '/Courier-Bold',
    'italic' => $fontDir . '/Courier-Oblique',
    'bold_italic' => $fontDir . '/Courier-BoldOblique',
  ),
  'helvetica' => array(
    'normal' => $fontDir . '/Helvetica',
    'bold' => $fontDir . '/Helvetica-Bold',
    'italic' => $fontDir . '/Helvetica-Oblique',
    'bold_italic' => $fontDir . '/Helvetica-BoldOblique',
  ),
  'zapfdingbats' => array(
    'normal' => $fontDir . '/ZapfDingbats',
    'bold' => $fontDir . '/ZapfDingbats',
    'italic' => $fontDir . '/ZapfDingbats',
    'bold_italic' => $fontDir . '/ZapfDingbats',
  ),
  'symbol' => array(
    'normal' => $fontDir . '/Symbol',
    'bold' => $fontDir . '/Symbol',
    'italic' => $fontDir . '/Symbol',
    'bold_italic' => $fontDir . '/Symbol',
  ),
  'serif' => array(
    'normal' => $fontDir . '/Times-Roman',
    'bold' => $fontDir . '/Times-Bold',
    'italic' => $fontDir . '/Times-Italic',
    'bold_italic' => $fontDir . '/Times-BoldItalic',
  ),
  'monospace' => array(
    'normal' => $fontDir . '/Courier',
    'bold' => $fontDir . '/Courier-Bold',
    'italic' => $fontDir . '/Courier-Oblique',
    'bold_italic' => $fontDir . '/Courier-BoldOblique',
  ),
  'fixed' => array(
    'normal' => $fontDir . '/Courier',
    'bold' => $fontDir . '/Courier-Bold',
    'italic' => $fontDir . '/Courier-Oblique',
    'bold_italic' => $fontDir . '/Courier-BoldOblique',
  ),
  'dejavu sans' => array(
    'bold' => $fontDir . '/DejaVuSans-Bold',
    'bold_italic' => $fontDir . '/DejaVuSans-BoldOblique',
    'italic' => $fontDir . '/DejaVuSans-Oblique',
    'normal' => $fontDir . '/DejaVuSans',
  ),
  'dejavu sans mono' => array(
    'bold' => $fontDir . '/DejaVuSansMono-Bold',
    'bold_italic' => $fontDir . '/DejaVuSansMono-BoldOblique',
    'italic' => $fontDir . '/DejaVuSansMono-Oblique',
    'normal' => $fontDir . '/DejaVuSansMono',
  ),
  'dejavu serif' => array(
    'bold' => $fontDir . '/DejaVuSerif-Bold',
    'bold_italic' => $fontDir . '/DejaVuSerif-BoldItalic',
    'italic' => $fontDir . '/DejaVuSerif-Italic',
    'normal' => $fontDir . '/DejaVuSerif',
  ),
  'fontawesome' => array(
    'normal' => $fontDir . '/fontawesome-normal_e4d3631d07307ec9e28bc0d3b1dfccb0',
  ),
  'oswald-medium' => array(
    '500' => '/home/seller/public_html/cardbar/storage/fonts//oswald-medium-500_224f32ac63c926c740d2ce1473baf35c',
  ),
  'quicksand' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//quicksand-normal_928a43fb6528bd1166b64aec4b48b4b9',
    '500' => '/home/seller/public_html/cardbar/storage/fonts//quicksand-500_879de72dfb531b04977dc8ef23f6b7c8',
    'bold' => '/home/seller/public_html/cardbar/storage/fonts//quicksand-bold_354df12367be64121ebc2c68355fe1dc',
  ),
  'roboto condensed' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//roboto-condensed-normal_d726c38c34e69837b214367d52a08a73',
  ),
  'slabo 27px' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//slabo-27px-normal_866a52c5d08b327d451c39553806bc66',
  ),
  'lg' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//lg-normal_65187b127909c47f3eec198ea5e6cd8b',
  ),
  'ionicons' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//ionicons-normal_238edc44f4119d799e3d64a5072da2bf',
  ),
  'simple-line-icons' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//simple-line-icons-normal_39032b07537c592dc01c19dbe0f94b9b',
  ),
  'cristmas' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//cristmas-normal_407832487ac4a79e84cd1b3aeb2f7efb',
  ),
  'clothes' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//clothes-normal_3db7c4fd8c1267efca66dba24c7c139b',
  ),
  'communication-48-x-48' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//communication-48-x-48-normal_a157768a26c6b34c38525a199e077195',
  ),
  'education-48' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//education-48-normal_2bafaeff7931cb69bb98d2cb2db79d96',
  ),
  'electronics' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//electronics-normal_d8a83dec57f124e0890768df052d82c8',
  ),
  'finance' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//finance-normal_a6f0774d4a79b76d9c42d8a577dc6808',
  ),
  'food-48' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//food-48-normal_0a174be12fdd7bc88c1cc9a48438ed50',
  ),
  'furniture' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//furniture-normal_88a4b290740bf0b9e5da4b7798f5e4e1',
  ),
  'hotel-restaurant' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//hotel-restaurant-normal_1c28b36d68eab525cb4733fbd929b451',
  ),
  'media' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//media-normal_5a0ea3dc2f78e52ef35e0b0cae5394c1',
  ),
  'medical-and-health' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//medical-and-health-normal_4baea31b75fe0b0f45150b74b81166df',
  ),
  'music' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//music-normal_3433413ca2ef686afad695720232fde9',
  ),
  'real-estate' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//real-estate-normal_b9e260a6b05e4a35dcee00d911d217d5',
  ),
  'science' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//science-normal_99e8642c357bccc60ca7433242ad491e',
  ),
  'sports-48-x-48' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//sports-48-x-48-normal_279d9b8a9ca14318be586c6435636fc1',
  ),
  'travel' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//travel-normal_d80a5795c5c46245363b01cc9ca3c00c',
  ),
  'weather' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//weather-normal_c35019985d3786fe2e0fae74cc1a2782',
  ),
  'transport' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//transport-normal_bfb43573682eb665714e553cb9c2f511',
  ),
  'linea-arrows-10' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//linea-arrows-10-normal_6349f0408af2bdf606dea4049ef1783d',
  ),
  'linea-basic-10' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//linea-basic-10-normal_8f283e695468b9c69a11f6428ff48452',
  ),
  'linea-ecommerce-10' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//linea-ecommerce-10-normal_0e312e783df662446cb8e21356b3b6af',
  ),
  'linea-software-10' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//linea-software-10-normal_797b66ee092af1ccae1625e62006443f',
  ),
  'blzee' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//blzee-normal_1455a2530f7eef0104bea266b9282f0c',
  ),
  'sue ellen francisco' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//sue-ellen-francisco-normal_297445dfb06047f4cf92a1ee27c8b4eb',
  ),
  'pacifico' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//pacifico-normal_7fb5df80ddc72cad3269857f0b21f729',
  ),
  'yellowtail' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//yellowtail-normal_b44c42ea1603da919150fce1a76356c2',
  ),
  'shadows into light' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//shadows-into-light-normal_63430133d9df206313b538c9c54bda22',
  ),
  'nunito' => array(
    'normal' => '/home/seller/public_html/cardbar/storage/fonts//nunito-normal_e93b6d41bf8c134cda18812cdd2254ab',
  ),
) ?>