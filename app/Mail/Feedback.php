<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class Feedback extends Mailable
{
    use Queueable, SerializesModels;
    public $data;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return  $this->subject('Vising card')
                    // if(isset($this->data['attachment'])){
                    //     ->attach($this->data['attachment']->getRealPath(),
                    //         [
                    //         'as' => $this->data['attachment']->getClientOriginalName(),
                    //         'mime' => $this->data['attachment']->getClientMimeType(),
                    //         ]
                    //     )
                    // }
                ->view('emails.feedback');
    }
}
