<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class UsersCard extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	
    //
    protected $fillable = [
        'user_id',
        'card_template_id',
        'card_theme_id',
        'order_id',
        'cardpdf',
        'cardimage',
        'cardqr',
        'vcard',
        'view'
    ];
}
