<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class SocialMedia extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	
    protected $table = "social_media";
    protected $guarded = ["id"];
    protected $primaryKey = "id";
}
