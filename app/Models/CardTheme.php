<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class CardTheme extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	
    protected $fillable = [
        'name',
        'image',
        'card_template_id',
        'default_image'
    ];

     /**
     * Get the card that owns the theme.
     */
    public function cardtemplate()
    {
        return $this->belongsTo(CardTemplate::class);
    }
}
