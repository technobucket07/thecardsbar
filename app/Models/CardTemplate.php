<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CardTemplate extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'picture',
        'category_id',
        'sub_category_id',
         'price',
         'cardtitle',
         'discount',
         'discounttype',
         'actualprice',
         'carddescription',
         'colortype',
         'cardtype',
         'maxsocialmedia',
         'minsocialmedia',
         'profile',
         'logo',
         'background',
         'gallery',
         'max_no_gallery',
         'min_no_gallery',
         'services',
         'max_no_services',
         'min_no_services',
         'serviceswithimg',
         'servicedescription',
         'username',
         'primaryphone',
         'secondaryphone',
         'primaryemail',
         'secondaryemail',
         'address',
         'location',
         'website',
         'designation',
         'description',
         'min_no_description',
         'company',
         'companyabout',
         'crosel_image',
         'calltagline',
         'openclosetime',
         'bottomtitle',
         'is_enabled',
    ];

    /**
     * Get the themes for card.
     */
    public function themes()
    {
        return $this->hasMany(CardTheme::class);
    }

    // public function likes()
    // {
    //     return $this->hasMany(CardLikes::class);
    // }

    //get user is liked
    public function getUserIsLikedAttribute()
    {
        $liked = $this->hasMany(UserCardLikes::class)->where('user_id','=',auth()->id())->first();
        if($liked && $liked->likes){
            return $liked->user_id;
        }else{
            return 0;
        }

    }

    public function getIsLikedAttribute()
    {
        $liked = $this->hasMany(CardLikes::class)->where('user_id','=',auth()->id())->first();
        if($liked && $liked->likes){
            return $liked->user_id;
        }else{
            return 0;
        }

    }

    //total view
    public function getUserTotalViewsAttribute()
    {
        return $this->hasMany(UserCardViews::class)->where('views','=',1)->count();

    }

    public function getTotalViewsAttribute()
    {
        return $this->hasMany(CardViews::class)->where('views','=',1)->count();

    }

    //total likes
    public function getUserTotalLikesAttribute()
    {
        return $this->hasMany(UserCardViews::class)->where('likes','=',1)->count();

    }

    public function getTotalLikesAttribute()
    {
        return $this->hasMany(CardLikes::class)->where('likes','=',1)->count();

    }

    // this is a recommended way to declare event handlers
    public static function boot() {
        parent::boot();
        self::deleting(function($card) { // before delete() method call this
             $card->themes()->each(function($theme) {
                $theme->delete(); // <-- direct deletion
             });
             // do the rest of the cleanup...
        });
    }
}
