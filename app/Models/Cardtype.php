<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Cardtype extends Model
{
	use SoftDeletes;
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
	protected $dates = ['deleted_at'];
    protected $fillable=[
        'name',
    ];
}
