<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SubCategory extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    protected $fillable=[
        'name',
        'image',
        'description',
        'category_id',
    ];
}
