<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserCardViews extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	
    //
    protected $fillable = [
        'user_id',
        'user_card_id',
        'card_template_id',
        'views',
    ];
}
