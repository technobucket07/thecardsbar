<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class ColorType extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	
    protected $fillable = [
        'name',
        'title'
    ];


   public function getall(){
   		$color =ColorType::all();
   		$types = $color->pluck('title','name')->toArray();
   		return $types;
   }
}
