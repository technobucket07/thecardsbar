<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cardfield extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable=[
        'fieldtype',
        'name',
        'isrequired',
        'label',
        'defaultvalue',
        'max_limit',
        'min_limit',
        'sub_field',
        'withimages',
    ];
}
