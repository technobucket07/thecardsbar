<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Orderfield extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'user_id',
        'fieldname',
        'subfieldname',
        'fieldvalue',
        'field',
    ];

    public function deleteDirectory($folderPath)
    {
        //$folderPath = public_path('test');
        $dd = \File::deleteDirectory($folderPath);
        //$dd = rmdir($folderPath);
        return $dd;
    }
}
