<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;
class Order extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable=[
        'razorpay_order_id',
        'razorpay_signature',
        'user_id',
        'card_template_id',
        'card_theme_id',
        'price',
        'payment_id',
        'coupon',
        'payment_method',
        'payment_status',
        'card_details',
        'first_name',
        'last_name',
        'email',
        'address',
        'status',
        'purchased_at',
        'cardqr',
        'cardimage',
        'vcard',
        'official_location',
        // 'primary',
        // 'scondary',
        // 'text',
        // 'code',
        // 'designation',
        // 'about',
        // 'phone',
        // 'social_media',
        // 'website',
        // 'image',
    ];

    public function countOrders($status=NULL,$createat=NULL) {
        $order = new Order;
        if(!is_null($status)){
            $order = $order->where('status','=',$status);
        }
        if(!is_null($createat)){
            if($createat == 'week'){
                $order = $order->where('created_at', '>', Carbon::now()->startOfWeek())
                ->where('created_at', '<', Carbon::now()->endOfWeek());            
            }elseif($createat == 'month'){
                $order = $order->whereMonth('created_at', Carbon::now()->month);
            }elseif($createat == 'year'){
                $order = $order->whereYear('created_at', Carbon::now()->year);
            }else{
                $order = $order->whereDate('created_at', Carbon::today());
            }
        }
      return $order->count();
    } 
}
