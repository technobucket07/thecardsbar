<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class TempCardDetails extends Model
{
	use SoftDeletes;
	protected $dates = ['deleted_at'];
    //
    protected $fillable = [
        'user_id',
        'temp_card_id',
        'card_details',
    ];
}
