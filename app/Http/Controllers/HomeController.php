<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\CardReviews;
use App\Models\CardLikes;
use App\Models\CardViews;
use App\Models\UserCardLikes;
use App\Models\UserCardViews;
use App\Models\UsersCard;
use App\Models\SharedCards;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('checkuser');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function UserAccount()
    {   $user =auth()->user();
        return view('frontend.account',compact('user'));
    }

    public function UpdateAccount(User $user, Request $request)
    {   
        $user =auth()->user();
        $rules = array(
            'name' => 'required',
            'about' => 'required',
            'gender' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'phone' => 'required|unique:users,phone,'.$user->id,
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with(['editprofile'=>true,'errors'=>$validator->messages()]);
        }else{
            $data = $request->except(['_token']);

            if($request->hasFile('profile')){
                $filepath = 'users/'.endeCrypt($user->id).'/profile';
                $file = $request->file('profile');
                $data['profile']=uploadImage($file,$filepath);
            }
            // echo"<pre>";print_r($data);die;
            $user->fill($data);
            $user->save();
            //Flash::message('Your account has been updated!');
            return back()->with(['editprofile'=>true,'success'=>'Successfully updated']);
        }
    }


     public function changePassword(Request $request)
     {
        $rules = array(
            'current-password' => 'required',
            'new-password' => 'required|string|min:8|confirmed',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->with(['changepass'=>true,'errors'=>$validator->messages()]);
        }else{

            if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
                // The passwords matches
                $validator->getMessageBag()->add('password', 'Your current password does not matches with the password you provided. Please try again.');
                return redirect()->back()->with(['changepass'=>true,'errors'=>$validator->messages()]);
            }

            if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
                //Current password and new password are same
                $validator->getMessageBag()->add('password', 'New Password cannot be same as your current password. Please choose a different password.');
                return redirect()->back()->with(['changepass'=>true,'errors'=>$validator->messages()]);
            }

            $user = Auth::user();
            $user->password = Hash::make($request->get('new-password'));
            //$user->password = bcrypt($request->get('new-password'));
            $user->save();
            return back()->with(['changepass'=>true,'success'=>'Password changed successfully !']);
        }
    }

    public function CardReviews(Request $request)
    {//echo"<pre>";print_r($request->all());die;
        $user =auth()->user();
        $data['user_id']=$user->id;
        $data['card_template_id']=$request->templateid;
        $like['likes']= $request->like;
        CardLikes::updateOrCreate($data,$like);
        return 1;
    }

    public function UserCardReviews(Request $request)
    {//echo"<pre>";print_r($request->all());die;
        $user =auth()->user();
        if($user){
            $data['user_id']=$user->id;
            $data['card_template_id']=$request->templateid;
            if($request->action =='like'){
                $like['likes']= $request->like;
                CardLikes::updateOrCreate($data,$like);
                $data['user_card_id']=$request->usercardid;
                UserCardLikes::updateOrCreate($data,$like);
                return 1;
            }
        }else{
            return 0;
        }
    }

    public function DownloadCardWithQR($id){
        $id = endeCrypt($id,'decrypt');
        $card = UsersCard::find($id);
        $card->update(['view'=>$card->view++]);
        // $user =auth()->user();
        // $data['user_id']=$user->id;
        // $data['user_card_id']=$id;
        // $data['card_template_id']=$card->card_template_id;
        // $view['views']=1;
        // UserCardViews::updateOrCreate($data,$view);
        $headers = ['Content-Type: application/force-download'];
        $basename = basename(storage_path("app/public/{$card->cardpdf}"));
        return response()->download(storage_path("app/public/{$card->cardpdf}"),$basename,$headers);
        //return redirect(url('storage/'.$card->cardpdf));
    }

    public function DownloadCard($id,$type){
        $id = endeCrypt($id,'decrypt');
        $card = UsersCard::find($id);
        //echo"<pre>";print_r($card);die;
        $user =auth()->user();
        if($type=='pdf'){
            return redirect(url('storage/'.$card->cardpdf));
        }else{
            return redirect(url('storage/'.$card->vcard));
        }
    }

    public function GetQRcode($userid,$orderid){
        $userid = endeCrypt($userid,'decrypt');
        $orderid = endeCrypt($orderid,'decrypt');
        $user =auth()->user();
        $card = UsersCard::leftJoin('card_templates', 'card_templates.id',  '=', 'users_cards.card_template_id')
            ->leftJoin('card_categories', 'card_categories.id',  '=', 'card_templates.category_id')
            ->leftJoin('users', 'users.id',  '=', 'users_cards.user_id')
            ->where('users_cards.user_id', '=',$userid)
            ->where('users_cards.order_id', '=',$orderid)
            ->select('users_cards.*','card_templates.cardtype','users.name AS serviceMan','users.profile AS serviceManProfile','card_categories.name AS categoryName','users_cards.id AS cardid')
            ->first();
//        echo"<pre>";print_r($card);die;
        if(!$card){
            return "Card not found";
        }  
        return view('frontend.qrcodeview',compact('card'));
    }

    public function YourCollection()
    {
        $user = auth()->user();
        $cards = SharedCards::leftJoin('users_cards', 'users_cards.id',  '=', 'shared_cards.user_card_id')
        ->leftJoin('card_templates', 'card_templates.id',  '=', 'users_cards.card_template_id')
        ->leftJoin('users', 'users.id',  '=', 'users_cards.user_id')
        ->leftJoin('card_categories', 'card_categories.id',  '=', 'card_templates.category_id')
        ->select('users_cards.*','card_templates.cardtype','shared_cards.shared_with','users.name AS serviceMan','users.profile AS serviceManProfile','card_categories.name AS categoryName')
        ->selectSub(function ($q) {
                           $q->from('user_card_likes')
                             ->whereRaw('user_card_likes.user_card_id = users_cards.id')
                             ->where('likes', 1)
                             ->selectRaw('count(*)');
                         }, 
                    'likes_count')
        ->selectSub(function ($q) {
                           $q->from('user_card_views')
                             ->whereRaw('user_card_views.user_card_id = users_cards.id')
                             ->where('views', 1)
                             ->selectRaw('count(*)');
                         }, 
                    'views_count')
        ->selectSub(function ($q) use($user) {
                             $q->from('user_card_likes')
                             ->whereRaw('user_card_likes.user_card_id = users_cards.id')
                             ->where('user_id', $user->id)
                             ->where('likes', 1)
                             ->selectRaw('user_id');
                         }, 
                    'userisliked')
        ->where('shared_cards.shared_with', '=', $user->id)->get();
        //echo"<pre>";print_r($cards);die;
        $empty ="You Have No Cards Please Download Our App For This Feature";
        $page ="Friends Cards";
        return view('frontend.yourcollection',compact('cards','page','empty'));
    }
}
