<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Models\Pages;
use App\Models\PageContent;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Pages::latest()->get();
            return DataTables::of($data)
                    ->addColumn('action', function($data){
                        $edit = route('admin.pages.edit',$data->id);
                        $delete = route('admin.pages.destroy',$data->id);
                        $addcontent =route('admin.pages.addcontent',$data->id);

                        // $button = '<a name="edit" id="'.$data->id.'" class="edit btn btn-primary btn-sm" href="'.$edit.'">Edit</a>';
                        $button='';
                        $button .= '&nbsp;&nbsp;&nbsp;<button type="button" class="delete btn btn-danger btn-sm" onclick="deleteData(this)" data-id="'.$data->id.'" data-url="'.$delete.'">Delete</button>';
                        $button .= '<a name="addcontent" class="edit btn btn-warning btn-sm" href="'.$addcontent.'">Add Content</a>';
                        return $button;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('admin.pages.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.addoredit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //echo"<pre>";print_r($request->all());die;
        $rules = array(
            'name' => 'required|unique:pages,name',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }else{

            $data = $request->except(['_token']);
            $page = Pages::create($data);

            if($page){
                return back()->with('success','Added successfully');
            }else{
                return back()->with('error','Something Wrong');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page = Pages::find($id);
        return view('admin.pages.addoredit',compact('page'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name' => 'required|unique:pages,name,'.$id,
        );
        $validator = Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
           return back()->withInput()->withErrors($validator);
        } else {
            $data = $request->except(['_token','_method']);
            $plan = Pages::where('id', $id)->update($data);

            if($plan){
            return back()->with('success','Successfully updated');
            }else{
                return back()->with('error','Something Wrong');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       // delete
        $plan = Pages::find($id);
        if($plan->delete()){
            return 1;
        }else{
            return 0;

        }
    }

    public function AddContent($id)
    {
        $page = Pages::find($id);
        $pagefields = PageContent::where('page_id', '=', $id)->get();
        return view('admin.pages.addoreditcontent',compact('pagefields','page'));
    }

    public function AddContentPost(Request $request)
    {
        //echo"<pre>";print_r($request->all());die;
        $pageid = $request->page_id;
        $data = $request->except(['_token','page_id']);
        foreach ($data as $key => $value) {
            $page=[
                'page_id'=>$pageid,
                'title'=>$key
            ];
            $content =[
                'content'=>$value
            ];
           PageContent::updateOrCreate($page,$content);
        }
        return back()->with('success','Successfully updated');
    }
}
