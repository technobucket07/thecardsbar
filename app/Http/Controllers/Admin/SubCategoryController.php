<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CardCategories;
use App\Models\SubCategory;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if ($request->ajax()) {
            $data = SubCategory::latest()->get();
            return DataTables::of($data)
                    ->editColumn('category_id',function($data){
                        $cat = CardCategories::find($data->category_id);
                        return $cat ? $cat->name : 'none';
                    })
                    ->addColumn('action', function($data){
                        $edit = route('admin.cardssubcategory.edit',$data->id);
                        $delete = route('admin.cardssubcategory.destroy',$data->id);
                        $button = '<a name="edit" id="'.$data->id.'" class="edit btn btn-primary btn-sm" href="'.$edit.'">Edit</a>';
                        $button .= '&nbsp;&nbsp;&nbsp;<button type="button" class="delete btn btn-danger btn-sm" onclick="deleteData(this)" data-id="'.$data->id.'" data-url="'.$delete.'">Delete</button>';
                        return $button;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('admin.cardssubcategory.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $categories = CardCategories::all()->pluck('name','id');
        return view('admin.cardssubcategory.addoredit',compact('categories'))->withErrors([]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $rules = array(
            'name' => 'required',
            'category_id' => 'required',
            //'icon' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }else{
            $data = $request->except(['_token']);

            if($request->hasFile('image')){
              $file = $request->file('image');
              $data['image']=uploadImage($file,'categories/image');
            }

            $template = SubCategory::create($data);
            if($template){
                return back()->with('success','Added successfully');
            }else{
                return back()->with('error','Something Wrong');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $categories = CardCategories::all()->pluck('name','id');
        $subcategory = SubCategory::find($id);
        return view('admin.cardssubcategory.addoredit',compact('categories','subcategory'))->withErrors([]);;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules = array(
            'name' => 'required',
            'category_id' => 'required',
            //'icon' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        } else {
            $data = $request->except(['_token','_method']);

            if($request->hasFile('image')){
              $file = $request->file('image');
              $data['image']=uploadImage($file,'categories/image');
            }
            
            $subcategory = SubCategory::where('id', $id)->update($data);
            if($subcategory){
            return back()->with('success','Successfully updated');
            }else{
                return back()->with('error','Something Wrong');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat = SubCategory::find($id);
        if($cat->delete()){
            return 1;
        }else{
            return 0;

        }
    }
}
