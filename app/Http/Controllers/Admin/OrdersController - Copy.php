<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\CardTemplate;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;
use App\Models\SocialMedia;
use Mail;
use App\Mail\ProcessEmail;
use App\Models\User;
use Carbon\Carbon;
use DB;
class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type ='all',Request $request)
    {
        
        if ($request->ajax()) {
            $data = Order::leftJoin('card_templates', 'card_templates.id',  '=', 'orders.card_template_id')->select('*','orders.id as order_id')->get();
            return DataTables::of($data)
                    ->editColumn('payment_status', function ($data) {
                        if($data->payment_status ==2){
                            return 'Paid';
                        }else{
                            return 'Unpaid';
                        }
                    })
                    ->editColumn('status', function ($data) {
                        if($data->status ==2){
                            return 'Cencal';
                        }else if($data->status ==1){
                            return 'Process';
                        }else{
                            return 'Completed';
                        }
                    })
                    ->addColumn('action', function($data){
                        $orderview = route('admin.orderview',$data->order_id);
                        $delete = '';
                        $sendcard = '';
                        $button = '<a name="view" id="'.$data->order_id.'" class="edit btn btn-primary btn-sm" href="'.$orderview.'">View</a>';
                        // $button .='<button name="view" id="'.$data->order_id.'" class="edit btn btn-primary btn-sm SendCard" data-orderid="'.$data->order_id.'" data-email="'.$data->email.'">Send Card</button>'; 
                        return $button;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('admin.orders.index', compact('type'));
    }


    public function orderview($id, Request $request)
    {
        $order = Order::find($id);
        $order->card_details = json_decode($order->card_details);
        $card  = CardTemplate::find($order->card_template_id);
        $socials = SocialMedia::all()->pluck('name','id')->toArray();
        //echo"<pre>";print_r($order);die;
        return view('admin.orders.orderview',compact('order','card','socials'));
    }

    public function orderprocess(Request $request)
    {
        $details = [
        'title' => 'Your visting card',
        'body' =>   $request->massage,
        ];

        if($request->hasFile('attachment'))
        {
           $details['attachment'] = $request->file('attachment'); 
        }
        //echo"<pre>";print_r($details);die;
        $email = Mail::to($request->useremail)->send(new ProcessEmail($details));

        // $order = Order::find($request->orderid);
        // $data = array(
        //     'status'=>'completed',
        // );
        // $order->update($data);
        return back()->with('success','Email successfully sent');
        
    }

}
