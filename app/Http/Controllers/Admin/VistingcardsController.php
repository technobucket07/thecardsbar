<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\CardTemplate;
use App\Models\CardCategories;
use App\Models\SubCategory;
use App\Models\SocialMedia;
use App\Models\ColorType;
use App\Models\Order;
use App\Models\CardTheme;
use App\Models\Cardtype;
use App\Models\Cards;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;
use Image;
class VistingcardsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = CardTemplate::latest()->get();
            return DataTables::of($data)
                    ->editColumn('discounttype', function ($data) {
                        return $data->discounttype == 1 ? 'Percentage' :'Flat off';
                    })
                    ->editColumn('picture', function ($data) {
                        return url('/').'/'.$data->picture;
                    })
                    ->addColumn('action', function($data){
                        // $enable = route('admin.vistingcards.edit',$data->id);
                        $edit = route('admin.vistingcards.edit',$data->id);
                        $delete = route('admin.vistingcards.destroy',$data->id);
                        if($data->is_enabled==1){
                            $isenabled='Enabled';
                        }else{
                            $isenabled='Disabled';
                        }

                        $button = '<a name="edit" id="'.$data->id.'" class="edit btn btn-primary btn-sm" href="'.$edit.'">Edit</a>';
                        
                        $button .= '<button name="isenabled" data-status="'.$data->is_enabled.'" data-id="'.$data->id.'" onclick="Isenabled(this)" class="btn btn-secondary btn-sm isenabled" href="javascript:void(0);">'.$isenabled.'</button>';

                        $button .= '&nbsp;&nbsp;&nbsp;<button type="button" class="delete btn btn-danger btn-sm" onclick="deleteData(this)" data-id="'.$data->id.'" data-url="'.$delete.'">Delete</button>';
                        return $button;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('admin.vistingcards.index');
    }

    public function getSubcategory(Request $request){
       
        $subcategories = SubCategory::where('category_id','=',$request->category_id)->pluck('name','id')->toArray();
       // array_unshift($subcategories, "Select");
        return response()->json($subcategories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $colors = new ColorType;
        $allcolortype =$colors->getall();
        $categories = CardCategories::all()->pluck('name','id')->toArray();
        // array_unshift($categories, "Select");
        $cardtype = Cardtype::all()->pluck('name','id')->toArray();
        $design = Cards::all()->pluck('name','id')->toArray();
        $cardthemes = array();
        return view('admin.vistingcards.addoredit',compact('categories','allcolortype','cardthemes','cardtype','design'))->withErrors([]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {//echo "<pre>";print_r($request->all());die;
        $rules = array(
            'cardtitle' => 'required',
            'carddescription' => 'required',
            'discounttype' => 'required',
            'discount' => 'required|numeric',
            'picture' => 'required',
            'actualprice' => 'required|numeric',
            'cardtype' => 'required',
            // 'category_id' => 'required',
            //'colortype' => 'required',
            'maxsocialmedia' => 'required',
            'minsocialmedia' => 'required',
            'profile' => 'required',
            'username' => 'required',
            'primaryphone' => 'required',
            'primaryemail' => 'required',
            'address' => 'required',
        );

        $messages =[
            'cardtitle.required' => 'This field is required.',
            'carddescription.required' => 'This field is required.',
            'discounttype.required' => 'This field is required.',
            'discount.required' => 'This field is required.',
            'picture.required' => 'This field is required.',
            'actualprice.required' => 'This field is required.',
            'cardtype.required' => 'This field is required.',
            // 'category_id.required' => 'This field is required.',
            //'colortype.required' => 'This field is required.',
            'maxsocialmedia.required' => 'This field is required.',
            'minsocialmedia.required' => 'This field is required.',
            'profile.required' => 'This field is required.',
            'username.required' => 'This field is required.',
            'primaryphone.required' => 'This field is required.',
            'primaryemail.required' => 'This field is required.',
            'address.required' => 'This field is required.',
        ];
        $validator = Validator::make($request->all(), $rules,$messages);

        
        if ($validator->fails()) {
            // return back()->withInput()->withErrors($validator);
            return webResponse(false, 206, $validator->getMessageBag());
        }else{
            $data = $request->except(['_token','theme']);
            if($request->discounttype =='1'){
                $discount = ($request->actualprice * $request->discount)/100;
                $data['price'] = $request->actualprice-$discount;
            }else{
                $data['price'] = $request->actualprice-$request->discount;
            }

            if ($request->hasFile('crosel_image')) {
                $croselimage = $request->file('crosel_image');
                $data['crosel_image'] ='storage/'.uploadImage($croselimage,'card/image/');
            }
            if ($request->hasFile('picture')) {
                $picture = $request->file('picture');
                $data['picture'] ='storage/'.uploadImage($picture,'card/image/');
            }
            //echo "<pre>";print_r($data);die;
            // $data['colortype'] = serialize($data['colortype']);
            unset($data['colortype']);
            $template = CardTemplate::create($data);

            // if ($request->hasFile('picture')) {
            //     $picture = $request->file('picture');
            //     $defaulttheme ='card/image/'.$this->image_upload($picture);
            //     CardTheme::create([
            //         'card_template_id'=>$template->id,
            //         'name'=>'Default',
            //         'image'=>$defaulttheme,
            //         'default_image'=>1
            //     ]);
            // }

            if(isset($request->theme)){
                foreach ($request->theme as  $theme) {
                    $theimg='';
                    if(!empty($theme['image'])){
                        $theimg ='storage/'.uploadImage($theme['image'],'card/image/');
                    } 
                    CardTheme::create([
                        'card_template_id'=>$template->id,
                        'name'=>$theme['name'],
                        'image'=>$theimg
                    ]);
                }
            }
            if($template){
                //return back()->with('success','Added successfully');
                return webResponse(true, 200,'Added successfully',['redirect'=>route('admin.vistingcards.create')]);
            }else{
                return back()->with('error','Something Wrong');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $card = CardTemplate::find($id);
        $card->colortype = unserialize($card->colortype);
        $categories = CardCategories::all()->pluck('name','id')->toArray();
        $cardthemes = CardTheme::where('card_template_id','=',$id)->get();
        $cardtype = Cardtype::all()->pluck('name','id')->toArray();
        $design = Cards::all()->pluck('name','id')->toArray();
        // array_unshift($categories, "Select");
        $colors = new ColorType;
        $allcolortype =$colors->getall();
        //echo"<pre>";print_r($cardthemes);die;
        return view('admin.vistingcards.addoredit',compact('card','categories','allcolortype','cardthemes','cardtype','design'))->withErrors([]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //echo"<pre>";print_r($request->all());die;
        $rules = array(
            'cardtitle' => 'required',
            'carddescription' => 'required',
            'discounttype' => 'required',
            'discount' => 'required|numeric',
            //'picture' => 'required',
            //'colortype' => 'required',
            'actualprice' => 'required|numeric',
            'cardtype' => 'required',
            'maxsocialmedia' => 'required',
            'minsocialmedia' => 'required',
            'profile' => 'required',
            'username' => 'required',
            'primaryphone' => 'required',
            'primaryemail' => 'required',
            'address' => 'required',
        );
        $messages =[
            'cardtitle.required' => 'This field is required.',
            'carddescription.required' => 'This field is required.',
            'discounttype.required' => 'This field is required.',
            'discount.required' => 'This field is required.',
            //'picture.required' => 'This field is required.',
            'actualprice.required' => 'This field is required.',
            'cardtype.required' => 'This field is required.',
            // 'category_id.required' => 'This field is required.',
            //'colortype.required' => 'This field is required.',
            'maxsocialmedia.required' => 'This field is required.',
            'minsocialmedia.required' => 'This field is required.',
            'profile.required' => 'This field is required.',
            'username.required' => 'This field is required.',
            'primaryphone.required' => 'This field is required.',
            'primaryemail.required' => 'This field is required.',
            'address.required' => 'This field is required.',
        ];

        $validator = Validator::make($request->all(), $rules,$messages);

        if ($validator->fails()) {
            return webResponse(false, 206, $validator->getMessageBag());
           //return back()->withInput()->withErrors($validator);
        } else {
            $data = $request->except(['_token','_method','theme']);

            if($request->discounttype =='1'){
                $discount = ($request->actualprice * $request->discount)/100;
                $data['price'] = $request->actualprice-$discount;
            }else{
                $data['price'] = $request->actualprice-$request->discount;
            }

            $orders = Order::all();
            if(!empty($orders)){
                foreach ($orders as $order){
                    Order::where('card_template_id',$id)->update(['price'=>$data['price']]);
                }
            }

            if ($request->hasFile('picture')) {
                $picture = $request->file('picture');
                $data['picture'] ='storage/'.uploadImage($picture,'card/image/');
            }

            if ($request->hasFile('crosel_image')) {
                $croselimage = $request->file('crosel_image');
                $data['crosel_image'] ='storage/'.uploadImage($croselimage,'card/image/');
            }

			if(!isset($request->profile)){
                $data['profile'] =0;
            }
			if(!isset($request->serviceswithimg)){
                $data['serviceswithimg'] =0;
            }
            if(!isset($request->servicedescription)){
                $data['servicedescription'] =0;
            }
			if(!isset($request->background)){
                $data['background'] =0;
            }
			
			if(!isset($request->services)){
                $data['services'] =0;
            }
			
			if(!isset($request->logo)){
                $data['logo'] =0;
            }
			
			if(!isset($request->gallery)){
                $data['gallery'] =0;
            }
			
            if(!isset($request->username)){
                $data['username'] =0;
            }
            if(!isset($request->primaryphone)){
                $data['primaryphone'] =0;
            }
            if(!isset($request->secondaryphone)){
                $data['secondaryphone'] =0;
            }
            if(!isset($request->primaryemail)){
                $data['primaryemail'] =0;
            }
            if(!isset($request->secondaryemail)){
                $data['secondaryemail'] =0;
            }
            if(!isset($request->address)){
                $data['address'] =0;
            }
            if(!isset($request->location)){
                $data['location'] =0;
            }
            if(!isset($request->website)){
                $data['website'] =0;
            }
            if(!isset($request->designation)){
                $data['designation'] =0;
            }
            if(!isset($request->description)){
                $data['description'] =0;
            }

            if(!isset($request->company)){
                $data['company'] =0;
            }
            if(!isset($request->companyabout)){
                $data['companyabout'] =0;
            }

            // $data['colortype'] = serialize($data['colortype']);
            unset($data['colortype']);
            $template = CardTemplate::where('id', $id)->update($data);

            // if ($request->hasFile('picture')) {
            //     $picture = $request->file('picture');
            //     $defaulttheme ='card/image/'.$this->image_upload($picture);
            //     CardTheme::updateOrCreate([
            //         'card_template_id'=>$id,
            //         'name'=>'Default'
            //         ],
            //         [
            //         'image'=>$defaulttheme,
            //         'default_image'=>1
            //         ]);
            // }

            if(isset($request->theme)){
                foreach ($request->theme as  $theme) {
                    $theimg='';
                    if(!empty($theme['image'])){
                        $theimg ='storage/'.uploadImage($theme['image'],'card/image/');
                        CardTheme::updateOrCreate([
                        'card_template_id'=>$id,
                        'name'=>$theme['name'] 
                        ],
                        [
                        'image'=>$theimg
                        ]);
                    } 
                }
            }

            if($template){
            return webResponse(true, 200,'Successfully updated',['redirect'=>route('admin.vistingcards.edit',$id)]);
            // return back()->with('success','Successfully updated');
            }else{
                return back()->with('error','Something Wrong');
            }
        }
    }

    public function EnableCard(Request $request)
    {//echo $request->id;die;
        $card = CardTemplate::find($request->id);
        $data=[];
        $msg='';
        if($card->is_enabled==0){
            $data['is_enabled']=1;
            $msg="is_enabled";
        }else{
            $data['is_enabled']=0;
            $msg="is_disabled";
        }
        $card->update($data);
        return response()->json(['status'=>true,
            'is_enabled'=>$data['is_enabled'],
          'massage' =>$msg], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
       // delete
        $card = CardTemplate::find($id);
        if($card->delete()){
            return 1;
        }else{
            return 0;

        }
    }

    public function Deletetheme($id)
    {
       // delete
        $card = CardTheme::find($id);
        if($card->delete()){
            //unlink(url('/'.$card->image));
            return 1;
        }else{
            return 0;

        }
    }

    public function image_upload($file,$height =0,$width =0)
    {
        $input = rand(10,10000).time().'.'.$file->getClientOriginalExtension();
        $destinationPath = public_path('card/image');
        $img = Image::make($file->getRealPath());
        // $img->resize($width,$height, function ($constraint) {
        // $constraint->aspectRatio();
        // })->save($destinationPath.'/'.$input);
        $img->save($destinationPath.'/'.$input);
        return $input;
    }

}
