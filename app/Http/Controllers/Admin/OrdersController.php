<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\Orderfield;
use App\Models\CardTemplate;
use App\Models\UsersCard;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;
use App\Models\SocialMedia;
use App\Models\CardCategories;
use App\Models\SubCategory;
use App\Models\CardTheme;
use JeroenDesloovere\VCard\VCard;
use Mail;
use App\Mail\ProcessEmail;
use App\User;
use Carbon\Carbon;
use DB;
use File;
use Image;
use Auth;
class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($type ='all',Request $request)
    {
      if ($request->ajax()) {
          $data = Order::leftJoin('card_templates', 'card_templates.id',  '=', 'orders.card_template_id')
          ->where('orders.payment_status','=','2')
          ->select('*','orders.id as order_id')->orderBy('orders.id', 'DESC')->get();

          return DataTables::of($data)
                  ->editColumn('payment_status', function ($data) {
                      if($data->payment_status ==2){
                          return 'Paid';
                      }else{
                          return 'Unpaid';
                      }
                  })
                  ->editColumn('purchased_at', function ($data) {
                      return date("d M Y", strtotime($data->created_at));
                  })
                  ->editColumn('status', function ($data) {
                      if($data->status ==2){
                          return 'Completed';
                      }else if($data->status ==1){
                          return 'Process';
                      }else{
                          return 'Cancel';
                      }
                  })
                  ->addColumn('username', function($data){
                      $user = User::find($data->user_id);
                      return $user ? $user->name:"N/A";
                  })
                  ->addColumn('useremail', function($data){
                      $user = User::find($data->user_id);
                      return $user ? $user->email:"N/A";
                  })
                  ->addColumn('action', function($data){
                      $orderview = route('admin.orderview',$data->order_id);
                      $delete = '';
                      $sendcard = '';
                      $button = '<a name="view" id="'.$data->order_id.'" class="edit btn btn-primary btn-sm" href="'.$orderview.'">View</a>';
                      // $button .='<button name="view" id="'.$data->order_id.'" class="edit btn btn-primary btn-sm SendCard" data-orderid="'.$data->order_id.'" data-email="'.$data->email.'">Send Card</button>'; 
                      return $button;
                  })
                  ->rawColumns(['action'])
                  ->make(true);
      }
      return view('admin.orders.index', compact('type'));
    }


    public function orderview($id, Request $request)
    {
        $order = Order::find($id);
        $cardfields = Orderfield::where('order_id', '=', $order->id)->get();
        $card  = CardTemplate::find($order->card_template_id);
        $category = CardCategories::all()->pluck('name','id')->toArray();
        $subcategory = SubCategory::all()->pluck('name','id')->toArray();
        //echo"<pre>";print_r($themeid);die;
        return view('admin.orders.orderview',compact('order','card','category','subcategory','cardfields'));
    }

    public function orderprocess(Request $request)
    {

      $rules = array(
        'useremail' => 'required',
        'cardimage' => 'mimes:jpeg,jpg,png,gif|required',
        'cardpdf' => 'required|mimetypes:application/pdf',
      );
      $validator = Validator::make($request->all(), $rules);

      if ($validator->fails()) {
        return response()->json(['status'=>false,
          'massage' => $validator->errors()->first()], 200);
      }
        $details = [
        'title' => 'Your visting card',
        'body' =>   $request->massage,
        ];
        $order = Order::find($request->orderid);
        $data=[];
        $data['category_id']=$request->category_id;
        $data['subcategory_id']=$request->subcategory_id;
        if($request->hasFile('cardpdf') && $request->hasFile('cardimage'))
        {
          $pdf = $request->file('cardpdf');
          $image = $request->file('cardimage');

          $path = 'users/'.endeCrypt($order->user_id).'/orders/'.endeCrypt($order->id);

          $data['cardpdf'] = uploadImage($pdf,$path);
          $data['cardimage'] = uploadImage($image,$path);

          $qrdata = url('/storage/'.$data['cardpdf']);
          generateQr($qrdata,$path."/qrcode.png");
          $data['cardqr'] = $path."/qrcode.png";
          $vcard = $this->vcard($order->user_id,$order->id,$path);
          $data['vcard']  = $path.'/'.$vcard;

          $details['cardpdf'] =url('/storage/'.$data['cardpdf']);
          $details['cardqr']  =url('/storage/'.$data['cardqr']);
          $details['vcard']   =url('/storage/'.$data['vcard']);
        }
        $email = Mail::to($request->useremail)->send(new ProcessEmail($details));
        $this->sendSuccessMail($request->orderid,$details);

        $pro=[
              'user_id'=>$order->user_id,
              'order_id'=>$order->id,
              'card_template_id'=>$order->card_template_id,
              'card_theme_id'=>$order->card_theme_id,
            ];

        UsersCard::updateOrCreate($pro,$data);
        $odata['status'] = '2';
        $order->update($odata);

        return response()->json(['status'=>true,
          'massage' =>'Email successfully sent'], 200);
    }

    public function sendSuccessMail($orderID = null,$details=array()){

        $orderDetail = Order::where('id',$orderID)->first();
        $cardDetail = CardTemplate::where('id',$orderDetail->card_template_id)->first();
        $data = array(
            'name'      => Auth::user()->name,
            'user_id'   => Auth::user()->id,
            'card_name' => $cardDetail->cardtitle,
            'card_id'   => $cardDetail->card_id,
            'date'      => Carbon::parse(Carbon::now())->format('d-M-Y'),
            'price'     => $orderDetail->price,
            'cardqr'    =>$details['cardqr'],
            'cardpdf'   =>$details['cardpdf'],
        );
        $res = Mail::send('emails.card_completed', $data, function ($message) {
            $message->from(env('MAIL_FROM_ADDRESS'), 'Cardsbar');
            $message->to(env('ADMIN_EMAIL'))->cc('bar@example.com')->subject('Card Completion');
        });

        return true;
    }


    function vcard($userid, $orderid,$savepath=''){

        $additional = '';
        $prefix = '';
        $suffix = '';
        $lastname = '';
        $firstname = '';
        $vcard = new VCard();
        $userinfo = Orderfield::where('user_id','=',$userid)
                  ->where('order_id','=',$orderid)->get();
        $path = storage_path('app/public/').$savepath;
        //$filename = uniqid();
        //$vcard->setFilename($filename);

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
        }          
        $vcard->setSavePath($path);

        foreach ($userinfo as $value) {
          if($value->fieldname =='last_name'){
            $lastname =$value->fieldvalue;
          }

          if($value->fieldname =='first_name'){
            $firstname =$value->fieldvalue;
          }

          if($value->fieldname =='company'){
            $vcard->addCompany($value->fieldvalue);
          }

          if($value->fieldname =='designation'){
            $vcard->addJobtitle($value->fieldvalue);
            //$vcard->addCompany($value->fieldvalue);
            $vcard->addRole($value->fieldvalue);
          }

          if($value->fieldname =='primaryemail'){
            $vcard->addEmail($value->fieldvalue,'Primary');
          }

          if($value->fieldname =='scondaryemail'){
            $vcard->addEmail($value->fieldvalue,'Scondary');
          }

          if($value->fieldname =='primaryphone'){
            $vcard->addPhoneNumber($value->fieldvalue, 'PREF;WORK');
          }

          if($value->fieldname =='scondaryphone'){
            $vcard->addPhoneNumber($value->fieldvalue, 'HOME');
          }

          if($value->fieldname =='address'){
            $vcard->addAddress($value->fieldvalue);
          }

          if($value->fieldname =='website'){
            $vcard->addURL($value->fieldvalue);
          }

          if($value->fieldname =='profileimg'){
            //echo url('/storage/'.$value->fieldvalue);die;
            $vcard->addPhoto(url('/storage/'.$value->fieldvalue));
          }
          if($value->fieldname =='logo'){
            $vcard->addLogo(url('/storage/'.$value->fieldvalue));
          }

    }
    //$vcard->addLabel('street, worktown, workpostcode Belgium');
    $vcard->setFilename('contact');
    $vcard->save();
    return 'contact.vcf';
  }
}
