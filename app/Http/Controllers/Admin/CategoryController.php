<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CardCategories;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Validator;
class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
      if ($request->ajax()) {
            $data = CardCategories::latest()->get();
            return DataTables::of($data)
                    ->addColumn('action', function($data){
                        $edit = route('admin.cardscategory.edit',$data->id);
                        $delete = route('admin.cardscategory.destroy',$data->id);
                        $button = '<a name="edit" id="'.$data->id.'" class="edit btn btn-primary btn-sm" href="'.$edit.'">Edit</a>';
                        $button .= '&nbsp;&nbsp;&nbsp;<button type="button" class="delete btn btn-danger btn-sm" onclick="deleteData(this)" data-id="'.$data->id.'" data-url="'.$delete.'">Delete</button>';
                        return $button;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('admin.cardscategory.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.cardscategory.addoredit')->withErrors([]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $rules = array(
            'name' => 'required',
            'image' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }else{
            $data = $request->except(['_token']);

            if($request->hasFile('image')){
              $file = $request->file('image');
              $data['image']=uploadImage($file,'categories/image');
            }
            
            $template = CardCategories::create($data);
            if($template){
                return back()->with('success','Added successfully');
            }else{
                return back()->with('error','Something Wrong');
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = CardCategories::find($id);
        return view('admin.cardscategory.addoredit',compact('category'))->withErrors([]);;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       // echo"<pre>";print_r($request->all());die;
        $rules = array(
            'name' => 'required',
            //'image' => 'required',
        );
        $validator = Validator::make($request->all(), $rules);

        // process the login
        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        } else {
            $data = $request->except(['_token','_method']);

            if($request->hasFile('image')){
              $file = $request->file('image');
              $data['image']=uploadImage($file,'categories/image');
            }
            $category = CardCategories::where('id', $id)->update($data);
            if($category){
            return back()->with('success','Successfully updated');
            }else{
                return back()->with('error','Something Wrong');
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cat = CardCategories::find($id);
        if($cat->delete()){
            return 1;
        }else{
            return 0;

        }
    }
}
