<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Models\Order;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(User $user, Order $order)
    {   
        $totalusers = $user->countUsers('0');
        $newusers   = $user->countUsers('0','month');
        $totalorder = $order->countOrders();
        $pendingorder = $order->countOrders('1');
        $cancelorder = $order->countOrders('0');
        $todayorder = $order->countOrders('1','today');
        return view('admin.home',compact('totalusers','newusers','totalorder','todayorder','pendingorder'));
    }

}
