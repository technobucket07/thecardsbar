<?php
namespace App\Http\Controllers\Cards;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SocialMedia;
use App\Models\Cards;
use App\User;
use Yajra\DataTables\DataTables;
use Illuminate\Support\Facades\Input;
use Image;
use Auth;
use Session;
class HomeController extends Controller
{   
  public function __construct(){
    date_default_timezone_set('Asia/Kolkata');
  }
  public function index(Request $request)
  {
    if ($request->ajax()) {
      $data = Cards::latest()->get();
      return DataTables::of($data)
      ->editColumn('image', function($data){
        return url('storage').'/'.$data->image;
      })
      ->addColumn('action', function($data){
        $makecad = route('admin.Cards.steps',$data->name);
        $edit = route('admin.Cards.getEditCard',$data->id);
        $delete = route('admin.Cards.DeleteCard',$data->id);
        $button = '<a name="edit"class="btn btn-primary btn-sm" href="'.$makecad.'">Make Card</a>';
        $button .= '&nbsp;&nbsp;&nbsp;<a name="edit"class="edit btn btn-warning btn-sm" href="'.$edit.'">Edit</a>';
        $button .= '&nbsp;&nbsp;&nbsp;<button type="button" class="delete btn btn-danger btn-sm" onclick="deleteData(this)" data-id="'.$data->id.'" data-url="'.$delete.'">Delete</button>';
        return $button;
      })
      ->rawColumns(['action'])
      ->make(true);
    }
    return view('cards.index');
  }

  public function AddCard(Request $request)
  {
    if($request->isMethod('post')){
      $rules = array(
            'name' => 'required|unique:cards,name',
            'image' => 'required',
        );
      $validator = Validator::make($request->all(), $rules);
      if($validator->fails())
      {
        return back()->withInput()->withErrors($validator);
      }else{
        $data = $request->except(['_token']);
        $data['name'] = str_slug($data['name']);
        if($request->hasFile('image')){
          $file = $request->file('image');
          $data['image']=uploadImage($file,'cards/image');
        }
        
        $card = Cards::create($data);
        if($card){
            return back()->with('success','Added successfully');
        }else{
            return back()->with('error','Something Wrong');
        }
      }
    }else{
      return view('cards.addoredit');
    }
  }

  public function EditCard($id, Request $request)
  {
    $card = Cards::find($id);
    if($request->isMethod('patch')){
      $rules = array(
            'name' => 'required|unique:cards,name,'.$id,
            //'image' => 'required',
        );
      $validator = Validator::make($request->all(), $rules);
      if($validator->fails())
      {
        return back()->withInput()->withErrors($validator);
      }else{
        $data = $request->except(['_token']);
        $data['name'] = str_slug($data['name']);
        if($request->hasFile('image')){
          $file = $request->file('image');
          $data['image']=uploadImage($file,'cards/image');
        }
        
        $card = $card->update($data);
        if($card){
            return back()->with('success','Added successfully');
        }else{
            return back()->with('error','Something Wrong');
        }
      }
    }else{
      return view('cards.addoredit',compact('card'));
    }
  }

  /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
  */
  public function destroy($id)
  {
      $cat = Cards::find($id);
      if($cat->delete()){
          return 1;
      }else{
          return 0;

      }
  }


    public function cardSteps($id)
    {  
        
        $data['image']=scandir('web_assets/design-image/'.$id.'/',1);
        $url=file_get_contents("http://robottxt.tdclients.work/text_frequency/country.json");
        $data['va']=json_decode($url);
    
        $data['socialMedia'] = SocialMedia::all();
        if($id=='design-1'){
            return view('cards.card_steps', $data);
        }elseif($id=='design-2'){
            return view('cards.card_steps1', $data);
        }elseif($id=='design-3'){
            return view('cards.card_steps3', $data);
        }elseif($id=='design-4'){
            return view('cards.card_steps4', $data);
        }elseif($id=='design-5'){
            return view('cards.card_steps5', $data);
        }elseif($id=='design-6'){
            return view('cards.card_steps6', $data);
        }elseif($id=='design-7'){
            return view('cards.card_steps7', $data);
        }elseif($id=='design-8'){
            return view('cards.card_steps8', $data);
        }elseif($id=='design-9'){
             return view('cards.card_steps9', $data);
        }elseif($id=='design-10'){
             return view('cards.card_steps10', $data);
        }elseif($id=='design-11'){
             return view('cards.card_steps11', $data);
        }elseif($id=='croppie'){
             return view('cards.croppie');
        }else{
            echo "this page is not avalbile!";
        }
        
    }
    
  public function card(Request $request){
     $val=$request->all();
    if(isset($request->userid) && isset($request->orderid)){
      $val['social_media']['info'] = $this->infolink($request->userid,$request->orderid);
    }else{
      $val['social_media']['info'] = '#';
    } 

   
    
     $bah=substr($val['website'],0,4);
        if($bah=='http'){
         
        $val['website1']=$val['website'];
        }else{
        
        $val['website1']='https://'.$val['website'];  
        }
   
    //   if($request->hasFile('image')){
    //         $file = $request->file('image');
    //         $height=237; $width=237;
    //         $val['img']=$this->image_upload($file,$height,$width);
    //     }
   
    //echo"<pre>";print_r($val);//die;
    $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
    $val['socialMediaList']['info']='fa-info';
    echo json_encode(array('va'=>$val,'count'=> count($val['social_media'])));
   // return view('cards.card2',compact('val'));
}
    
     public function cards1(Request $request){
       
      $val=$request->all();
      if(isset($request->userid) && isset($request->orderid)){
        $val['social_media']['info'] = $this->infolink($request->userid,$request->orderid);
      }else{
        $val['social_media']['info'] = '#';
      } 
        $bah=substr($val['website'],0,4);
        if($bah=='http'){
            $val3=explode('://',$val['website']);
            $val['website']=$val3[1];
          $val['website1']=$val['website'];
        }else{
          
          $val['website1']='https://'.$val['website'];  
        }
           foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
         //dd($val['social_media1']);
         
        //   if($request->hasFile('image1')){
        //     $file = $request->file('image1');
        //     $height=418; $width=294;
        //     $val['img1']=$this->image_upload($file,$height,$width);
            
            
        // }else{
        //      $val['img1']='';
        // }
        
        //  if($request->hasFile('image2')){
        //     $file = $request->file('image2');
        //     $height=112; $width=112;
        //     $val['img2']=$this->image_upload($file,$height,$width);
           
        // }else{
        //     $val['img2']='';
        // }
        
        // if($request->hasFile('gallery1')){
        //     $file = $request->file('gallery1');
        //      $height=62; $width=78;
        //     $val['gallery1']=$this->image_upload($file,$height,$width);
           
           
        // }else{
        //     $val['gallery1']='';
        // }
        
        // if($request->hasFile('gallery2')){
        //      $file = $request->file('gallery2');
        //       $height=62; $width=78;
        //      $val['gallery2']=$this->image_upload($file,$height,$width);
           
        // }else{
        //     $val['gallery2']='';
        // }
        //  if($request->hasFile('gallery3')){
        //      $file = $request->file('gallery3');
        //       $height=62; $width=78;
        //      $val['gallery3']=$this->image_upload($file,$height,$width);
            
        // }else{
        //      $val['gallery3']='';
        // }
          $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
        $val['socialMediaList']['info']='fa-info';
       echo json_encode(array('va'=>$val,'count'=> count($val['social_media'])));
   
    }
    
   
   
   
       
    public function cards3(Request $request){
      
      $val=$request->all();
      if(isset($request->userid) && isset($request->orderid)){
        $val['social_media']['info'] = $this->infolink($request->userid,$request->orderid);
      }else{
        $val['social_media']['info'] = '#';
      }
        foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
         
         if(!empty($val['website'])){
               $bah=substr($val['website'],0,4);
               if($bah=='http'){
                 $val['website2']=explode('://',$val['website']);
                 $val['website1']=$val['website'];
                 $val['website3']= $val['website2'][1];
          
          }else{
                 $val['website2']=$val['website'];
                 $val['website1']='https://'.$val['website']; 
                 $val['website3']= $val['website2'];
           }
          }else{
                $val['website1']='';
                $val['website3']='';
          }
      
         
         
        //   if($request->hasFile('background_image1')){
        //       $file = $request->file('background_image1');
        //       $height=400; $width=350;
        //       $val['background_image1']=$this->image_upload($file,$height,$width); 
              
        // }else{
        //     $val['background_image1']='';
        // }
        
        // if($request->hasFile('background_image2')){
        //       $file = $request->file('background_image2');
        //       $height=400; $width=400;
        //       $val['background_image2']=$this->image_upload($file,$height,$width); 
              
        // }else{
        //     $val['background_image2']='';
        // }
        
        
        // if($request->hasFile('profile_image')){
        //       $file = $request->file('profile_image');
        //       $height=100; $width=100;
        //       $val['profile_image']=$this->image_upload($file,$height,$width); 
              
        // }else{
        //     $val['profile_image']='';
        // }
        
        
        
      $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
      $val['socialMediaList']['info']='fa-info';
         
          echo json_encode(array('va'=>$val,'count'=> count($val['social_media'])));
    }
    
   
   
    
    
    public function cards4(Request $request){
       
      $val=$request->all();
      if(isset($request->userid) && isset($request->orderid)){
        $val['social_media']['info'] = $this->infolink($request->userid,$request->orderid);
      }else{
        $val['social_media']['info'] = '#';
      }

        foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
         
         if(!empty($val['website'])){
               $bah=substr($val['website'],0,4);
               if($bah=='http'){
                 $val['website2']=explode('://',$val['website']);
                 $val['website1']=$val['website'];
                 $val['website3']= $val['website2'][1];
          
          }else{
                 $val['website2']=$val['website'];
                 $val['website1']='https://'.$val['website']; 
                 $val['website3']= $val['website2'];
           }
          }else{
                $val['website1']='';
                $val['website3']='';
          }
      
         
         
        //   if($request->hasFile('background_image')){
        //       $file = $request->file('background_image');
        //       $height=400; $width=350;
        //       $val['background_image']=$this->image_upload($file,$height,$width); 
              
        // }else{
        //     $val['background_image']='';
        // }
        
        
        
        // if($request->hasFile('profile_image')){
        //       $file = $request->file('profile_image');
        //       $height=100; $width=100;
        //       $val['profile_image']=$this->image_upload($file,$height,$width); 
              
        // }else{
        //     $val['profile_image']='';
        // }
        
        
        
      $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
      $val['socialMediaList']['info']='fa-info';
         
          echo json_encode(array('va'=>$val,'count'=> count($val['social_media'])));
    }
    
    
    
     public function cards5(Request $request){
       
      $val=$request->all();
      if(isset($request->userid) && isset($request->orderid)){
        $val['social_media']['info'] = $this->infolink($request->userid,$request->orderid);
      }else{
        $val['social_media']['info'] = '#';
      }
        foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
         
        //  if(!empty($val['website'])){
        //       $bah=substr($val['website'],0,4);
        //       if($bah=='http'){
        //          $val['website2']=explode('://',$val['website']);
        //          $val['website1']=$val['website'];
        //          $val['website3']= $val['website2'][1];
          
        //   }else{
        //          $val['website2']=$val['website'];
        //          $val['website1']='https://'.$val['website']; 
        //          $val['website3']= $val['website2'];
        //   }
        //   }else{
        //         $val['website1']='';
        //         $val['website3']='';
        //   }
      
         
         
        //   if($request->hasFile('image1')){
        //       $file = $request->file('image1');
        //       $height=283; $width=156;
        //       $val['image1']=$this->image_upload($file,$height,$width); 
              
        // }else{
        //     $val['image1']='';
        // }
        
        
        //  if($request->hasFile('image2')){
        //       $file = $request->file('image2');
        //       $height=132; $width=156;
        //       $val['image2']=$this->image_upload($file,$height,$width); 
              
        // }else{
        //     $val['image2']='';
        // }
        // if($request->hasFile('image3')){
        //       $file = $request->file('image3');
        //       $height=132; $width=156;
        //       $val['image3']=$this->image_upload($file,$height,$width); 
              
        // }else{
        //     $val['image3']='';
        // }
        
        
        
      $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
      $val['socialMediaList']['info']='fa-info';
         
          echo json_encode(array('va'=>$val,'count'=> count($val['social_media'])));
    }
    
    
    
    
  public function cards6(Request $request){
       
      $val=$request->all();
      if(isset($request->userid) && isset($request->orderid)){
        $val['social_media']['info'] = $this->infolink($request->userid,$request->orderid);
      }else{
        $val['social_media']['info'] = '#';
      }
        foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
         //dd($val['social_media1']);
         
         
        
             $val['servicename1']=$val['service-name-1'];
             $val['servicename2']=$val['service-name-2'];
             $val['servicename3']=$val['service-name-3'];
             $val['servicename4']=$val['service-name-4'];
          $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
          $val['socialMediaList']['info']='fa-info';
           echo json_encode(array('va'=>$val,'count'=> count($val['social_media'])));
    }
    
    
    
     public function cards7(Request $request){
       
      $val=$request->all();
      if(isset($request->userid) && isset($request->orderid)){
        $val['social_media']['info'] = $this->infolink($request->userid,$request->orderid);
      }else{
        $val['social_media']['info'] = '#';
      }
       foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
        
         
         
         
             $val['servicename1']=$val['service-name-1'];
             $val['servicename2']=$val['service-name-2'];
             $val['servicename3']=$val['service-name-3'];
             $val['servicename4']=$val['service-name-4'];
             $val['servicename5']=$val['service-name-5'];
             $val['servicename6']=$val['service-name-6'];
          $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
          $val['socialMediaList']['info']='fa-info';
           echo json_encode(array('va'=>$val,'count'=> count($val['social_media'])));
    }
    
    
    public function cards8(Request $request){
        
          $val=$request->all();
      if(isset($request->userid) && isset($request->orderid)){
        $val['social_media']['info'] = $this->infolink($request->userid,$request->orderid);
      }else{
        $val['social_media']['info'] = '#';
      }   
          foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
         
        //  if($request->hasFile('profile_image')){
        //     $file = $request->file('profile_image');
        //         $height=178; $width=131;
        //         $val['profile_image']=$this->image_upload($file,$height,$width);
                
        // }else{
        //      $val['profile_image']='';
        // }
        
        //  if($request->hasFile('logo')){
        //     $file = $request->file('logo');
        //         $height=61; $width=167;
        //         $val['logo']=$this->image_upload($file,$height,$width);
                
        // }else{
        //      $val['logo']='';
        // }
        if($val['service-name-1']!==null){
            $val['servicename1']=explode(",",$val['service-name-1']);
        }else{
             $val['servicename1']='';
        }
        
         if($val['service-name-2']!==null){
            $val['servicename2']=explode(",",$val['service-name-2']);
        }else{
             $val['servicename2']='';
        }
        
         if($val['service-name-3']!==null){
            $val['servicename3']=explode(",",$val['service-name-3']);
        }else{
             $val['servicename3']='';
        }
        
         if($val['service-name-4']!==null){
            $val['servicename4']=explode(",",$val['service-name-4']);
        }else{
             $val['servicename4']='';
        }
        
         if($val['service-name-5']!==null){
            $val['servicename5']=explode(",",$val['service-name-5']);
        }else{
             $val['servicename5']='';
        }
        
         if($val['service-name-6']!==null){
            $val['servicename6']=explode(",",$val['service-name-6']);
        }else{
             $val['servicename6']='';
        }
          $val['facebook_color1']=$this->hexToHsl($val['facebook_color']);
          $val['skepe_color1']=$this->hexToHsl($val['skepe_color']);
          $val['linkedin_color1']=$this->hexToHsl($val['linkedin_color']);
         $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
         $val['socialMediaList']['info']='fa-info';
        echo json_encode(array('va'=>$val,'count'=> count($val['social_media'])));
        
    }
    
    public function cards9(Request $request){
         $val=$request->all();
      if(isset($request->userid) && isset($request->orderid)){
        $val['social_media']['info'] = $this->infolink($request->userid,$request->orderid);
      }else{
        $val['social_media']['info'] = '#';
      }   
         foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
          
     
          if($val['service-name-1']!==null){
            $val['servicename1']=$val['service-name-1'];
        }else{
             $val['servicename1']='';
        }
        
         if($val['service-name-2']!==null){
            $val['servicename2']=$val['service-name-2'];
        }else{
             $val['servicename2']='';
        }
        
         if($val['service-name-3']!==null){
            $val['servicename3']=$val['service-name-3'];
        }else{
             $val['servicename3']='';
        }
        
         if($val['service-name-4']!==null){
            $val['servicename4']=$val['service-name-4'];
        }else{
             $val['servicename4']='';
        }
        
         
          $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
          $val['socialMediaList']['info']='fa-info';
      echo json_encode(array('va'=>$val,'count'=> count($val['socialMediaList'])));
    }
    
     public function cards10(Request $request){ 
         
                      $val=$request->all();
      if(isset($request->userid) && isset($request->orderid)){
        $val['social_media']['info'] = $this->infolink($request->userid,$request->orderid);
      }else{
        $val['social_media']['info'] = '#';
      }       
            foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
          
       
        
         if($val['service-name-1']!==null){
            $val['servicename1']=$val['service-name-1'];
        }else{
             $val['servicename1']='';
        }
        
         if($val['service-name-2']!==null){
            $val['servicename2']=$val['service-name-2'];
        }else{
             $val['servicename2']='';
        }
        
         if($val['service-name-3']!==null){
            $val['servicename3']=$val['service-name-3'];
        }else{
             $val['servicename3']='';
        }
        
         if($val['service-name-4']!==null){
            $val['servicename4']=$val['service-name-4'];
        }else{
             $val['servicename4']='';
        }
        
          if($val['service-name-5']!==null){
            $val['servicename5']=$val['service-name-5'];
          }else{
             $val['servicename5']='';
          }
           
            if($val['service-name-6']!==null){
            $val['servicename6']=$val['service-name-6'];
          }else{
             $val['servicename6']='';
          }
          $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
          $val['socialMediaList']['info']='fa-info';
            echo json_encode(array('va'=>$val,'count'=> count($val['social_media'])));
     }
    
    
    
    public function cards11(Request $request) { 
        $val=$request->all();
      if(isset($request->userid) && isset($request->orderid)){
        $val['social_media']['info'] = $this->infolink($request->userid,$request->orderid);
      }else{
        $val['social_media']['info'] = '#';
      } 
       foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
         //dd($val['social_media1']);
        
       
        
            if(!empty($val['website'])){
              $bah=substr($val['website'],0,4);
              if($bah=='http'){
              $val['website2']=explode('://',$val['website']);
              $val['website1']=$val['website'];
              $val['website3']= $val['website2'][1];
          
          }else{
              $val['website2']=$val['website'];
              $val['website1']='https://'.$val['website']; 
              $val['website3']= $val['website2'];
          }
          }else{
              $val['website1']='';
                $val['website3']='';
          }
      
        
         
    
          $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
          $val['socialMediaList']['info']='fa-info';
            echo json_encode(array('va'=>$val,'count'=> count($val['social_media'])));
    }
    
    
    
    
    
    
    
    
   //pdf generate function 
    
    
    
    public function generate_pdf(Request $request){
    
       $validatedData = $request->validate([
                'primary' =>'required',
                'scondary' =>'required',
                //'code' =>'required',
                'text' => 'required',
                'heading' => 'required',
                'social_media' =>'required',
                'first_name' =>'required|max:30',
                'last_name' =>'required|max:30',
                'email' => 'required|max:100',
                'designation' =>'required|max:50',
                'about' => 'required|max:100',
                'phone' => 'required|max:13|min:10',
                //'whats_no' => 'required|max:10|min:10',
                'address' => 'required|max:100',
              //  'website' => 'required|max:30',
               // 'image' => 'required',
   
]);
    // dd($validatedData);   
          $val=$request->all();
        //   if($request->hasFile('image')){
        //       $height=237; $width=237;
        //       $image = $request->file('image');
        //       $val['img']=$this->image_upload($image,$height,$width);
        //      }
        
          $bah=substr($val['website'],0,4);
          if($bah=='http'){
           
          $val['website1']=$val['website'];
          }else{
          
          $val['website1']='https://'.$val['website'];  
          }
      
        foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
          if(isset($request->location)){
            $location = json_decode($request->location);
            $val['location']="http://maps.google.co.uk/maps?q=$location->lat,$location->lng"; 
          }
          

          if(isset($request->userid) && isset($request->orderid)){
            $val['social_media1']['info'] = $this->infolink($request->userid,$request->orderid);
          }else{
            $val['social_media1']['info']="#";
          }

          $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
          $val['socialMediaList']['info']='fa-info';
           $pdf = \App::make('dompdf.wrapper');
           $customPaper = array(40,-34,330.80,570.80);
           $pdf->loadView('cards.card',compact('val'))->setPaper($customPaper, 'portrait');
           $output = $pdf->output();
           $filename=$val['first_name']." ".$val['last_name']."_".date('d-m-Y')."_".date('H')."_".".pdf";
           $design='design-1';
           $url=$this->pdf_upload($output,$filename,$design);
          // echo $url;
         //return $pdf->stream($val['first_name']." ".$val['last_name'].".pdf");
      
}
    
     public function generate_pdf1(Request $request){
    // dd($request->all());
       $validatedData = $request->validate([
                'primary' =>'required',
                'scondary' =>'required',
                //'code' =>'required',
                'heading' =>'required',
                'text' => 'required',
                'social_media' =>'required',
                'first_name' =>'required|max:30',
                'last_name' =>'required|max:30',
                'designation' =>'required|max:100',
                'about' => 'required|max:145',
                'phone' => 'required|max:10|min:10',
                //'website' => 'required|max:100',
                // 'image1' => 'required',
                // 'image2' => 'required',
                // 'gallery1' => 'required',
                // 'gallery2' => 'required',
                // 'gallery3' => 'required',
   
]);
        $val=$request->all();
        $bah=substr($val['website'],0,4);
        if($bah=='http'){
           
          $val['website1']=$val['website'];
        }else{
          
          $val['website1']='https://'.$val['website'];  
        }
      
            foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
          
          if(isset($request->userid) && isset($request->orderid)){
            $val['social_media1']['info'] = $this->infolink($request->userid,$request->orderid);
          }else{
            $val['social_media1']['info']="#";
          }

          $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
          $val['socialMediaList']['info']='fa-info';
          $pdf = \App::make('dompdf.wrapper');
        
           $customPaper = array(45,-50,340.80,625.80);
             // return view('cards.index2',compact('val'));
           $pdf->loadView('cards.index2',compact('val'))->setPaper($customPaper, 'portrait');
           $output = $pdf->output();
           $filename=$val['first_name']." ".$val['last_name']."_".date('d-m-Y')."_".date('H')."_".".pdf";
           $design='design-2';
           $url=$this->pdf_upload($output,$filename,$design);
        
        }
        
        
        
         public function generate_pdf3(Request $request) {
             //dd($request->all());
             $validatedData = $request->validate([
                'linkedin_color' =>'required',
                'facebook_color' =>'required',
                'whatsapp_color' =>'required',
                'twitter_color' =>'required',
                'skype_color' =>'required',
                'heading_color' =>'required',
                'country_code' =>'required',
                'text' => 'required',
                'social_media' =>'required',
                //'code' =>'required',
                'first_name' =>'required|max:30',
                'last_name' =>'required|max:30',
                'designation' =>'required|max:35',
                'about' =>'required|max:100',
                'phone' => 'required|max:10|min:10',
                // 'profile_image' => 'required',
                // 'background_image1' => 'required',
                // 'background_image2' => 'required',
               ]);
               
                        $val=$request->all();
        foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
         
         if(isset($request->userid) && isset($request->orderid)){
            $val['social_media1']['info'] = $this->infolink($request->userid,$request->orderid);
          }else{
            $val['social_media1']['info']="#";
          }
          
          if(!empty($val['website'])){
                $bah=substr($val['website'],0,4);
               if($bah=='http'){
                $val['website2']=explode('://',$val['website']);
                $val['website1']=$val['website'];
                $val['website3']= $val['website2'][1];
          
          }else{
                $val['website2']=$val['website'];
                $val['website1']='https://'.$val['website']; 
                $val['website3']= $val['website2'];
           }
          }else{
                $val['website1']='';
                $val['website3']='';
          }
      
         
         
         
         if($request->hasFile('background_image1')){
              $file = $request->file('background_image1');
              $height=400; $width=350;
              $val['background_image1']=$this->image_upload($file,$height,$width); 
              
        }
        
        if($request->hasFile('background_image2')){
              $file = $request->file('background_image2');
              $height=400; $width=350;
              $val['background_image2']=$this->image_upload($file,$height,$width); 
              
        }
        
        if($request->hasFile('profile_image')){
              $file = $request->file('profile_image');
              $height=100; $width=100;
              $val['profile_image']=$this->image_upload($file,$height,$width); 
              
        }
          $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
          $val['socialMediaList']['info']='fa-info';
          $pdf = \App::make('dompdf.wrapper');
          $customPaper = array(35,-35,333,633);
         //return view('cards.card3',compact('val'));
          $pdf->loadView('cards.card3',compact('val'))->setPaper($customPaper, 'portrait');
           $output = $pdf->output();
           $filename=$val['first_name']." ".$val['last_name']."_".date('d-m-Y')."_".date('H')."_".".pdf";
           $design='design-3';
           $url=$this->pdf_upload($output,$filename,$design);
       
          //return $pdf->stream('invoice.pdf');
           //echo  $url;
   
    } 
        
        
        
        
    

    
    
     public function generate_pdf4(Request $request){
        
        
          $validatedData = $request->validate([
                'primary' =>'required',
                'scondary' =>'required',
               // 'website' =>'required',
                'text' => 'required',
                'blog_color' => 'required',
                'heading' => 'required',
                'social_media' =>'required',
                'first_name' =>'required|max:30',
                'last_name' =>'required|max:30',
                'company_name' =>'required|max:35',
                'phone' => 'required|max:10|min:10',
                //'whats_no' => 'required|max:10|min:10',
                // 'profile_image' => 'required',
                // 'background_image' => 'required',
               
   
        ]);
          $val=$request->all();
           foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
         
         if(isset($request->userid) && isset($request->orderid)){
            $val['social_media1']['info'] = $this->infolink($request->userid,$request->orderid);
          }else{
            $val['social_media1']['info']="#";
          }
          
          if(!empty($val['website'])){
               $bah=substr($val['website'],0,4);
               if($bah=='http'){
               $val['website2']=explode('://',$val['website']);
               $val['website1']=$val['website'];
               $val['website3']= $val['website2'][1];
          
          }else{
              $val['website2']=$val['website'];
              $val['website1']='https://'.$val['website']; 
              $val['website3']= $val['website2'];
           }
          }else{
               $val['website1']='';
                $val['website3']='';
          }
      
         
         
         
         if($request->hasFile('background_image')){
              $file = $request->file('background_image');
              $height=400; $width=350;
              $val['background_image']=$this->image_upload($file,$height,$width); 
              
        }
        
        if($request->hasFile('profile_image')){
              $file = $request->file('profile_image');
              $height=100; $width=100;
              $val['profile_image']=$this->image_upload($file,$height,$width); 
              
        }
        
        
        
      $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
      $val['socialMediaList']['info']='fa-info';
     //return view('cards.card4',compact('val'));
           $pdf = \App::make('dompdf.wrapper');
           $customPaper = array(35,-35,333,560.80);
           $pdf->loadView('cards.card4',compact('val'))->setPaper($customPaper, 'portrait');
           $output = $pdf->output();
           $filename=$val['first_name']." ".$val['last_name']."_".date('d-m-Y')."_".date('H')."_".".pdf";
           $design='design-4';
           $url=$this->pdf_upload($output,$filename,$design);
          // echo $url;
        //   return $pdf->stream($val['first_name']." ".$val['last_name'].".pdf");
        //  return view('cards.card4');
     }
     
     
     
          public function generate_pdf5(Request $request){
       // dd($request->all());
        
          $validatedData = $request->validate([
                'primary' =>'required',
                'scondary' =>'required',
                'text' => 'required',
                'blog_color' =>'required',
                'social_media' =>'required',
                'first_name' =>'required|max:30',
                'last_name' =>'required|max:30',
                'study' =>'required|max:100',
                'hobby' =>'required|max:100',
                'heading1' =>'required|max:8',
                'heading2' =>'required|max:8',
                'heading3' =>'required|max:8',
                'about' =>'required|max:100',
                'phone' => 'required|max:10|min:10',
                // 'image1' => 'required',
                // 'image2' => 'required',
                // 'image3' => 'required',
                
               
   
        ]);
        $val=$request->all();
        if(!empty($val['study'])){
           
           $val['study1']=explode(",",$val['study']);
        }
         if(!empty($val['hobby'])){
           $val['hobby1']=explode(",",$val['hobby']);
        }
          
          foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
         
         if(isset($request->userid) && isset($request->orderid)){
            $val['social_media1']['info'] = $this->infolink($request->userid,$request->orderid);
          }else{
            $val['social_media1']['info']="#";
          }
          
        //   if(!empty($val['website'])){
        //       $bah=substr($val['website'],0,4);
        //       if($bah=='http'){
        //       $val['website2']=explode('://',$val['website']);
        //       $val['website1']=$val['website'];
        //       $val['website3']= $val['website2'][1];
          
        //   }else{
        //       $val['website2']=$val['website'];
        //       $val['website1']='https://'.$val['website']; 
        //       $val['website3']= $val['website2'];
        //   }
        //   }else{
        //       $val['website1']='';
        //         $val['website3']='';
        //   }
      
         
         
         
        //  if($request->hasFile('image1')){
        //       $file = $request->file('image1');
        //       $height=283; $width=156;
        //       $val['image1']=$this->image_upload($file,$height,$width); 
              
        // }
        
        // if($request->hasFile('image2')){
        //       $file = $request->file('image2');
        //       $height=132; $width=156;
        //       $val['image2']=$this->image_upload($file,$height,$width); 
              
        // }
        
        //  if($request->hasFile('image3')){
        //       $file = $request->file('image3');
        //       $height=132; $width=156;
        //       $val['image3']=$this->image_upload($file,$height,$width); 
              
        // }
        
        
        
           $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
           $val['socialMediaList']['info']='fa-info';
           //dd($val);
           $pdf = \App::make('dompdf.wrapper');
           $customPaper = array(35,-35,333,600.80);
           $pdf->loadView('cards.card5',compact('val'))->setPaper($customPaper, 'portrait');
           $output = $pdf->output();
           $filename=$val['first_name']." ".$val['last_name']."_".date('d-m-Y')."_".date('H')."_".".pdf";
           $design='design-5';
           $url=$this->pdf_upload($output,$filename,$design);
       
     }
     
    
    
    public function generate_pdf6(Request $request){
    
      $validatedData = $request->validate([
                'primary' =>'required',
                'scondary' =>'required',
                'blog_color' =>'required',
                'button_color' =>'required',
                'code' =>'required',
                'text' => 'required',
                'social_media' =>'required',
                'first_name1' =>'required|max:30',
                'last_name1' =>'required|max:30',
                'phone1' => 'required|max:10|min:10',
                'first_name2' =>'required|max:30',
                'last_name2' =>'required|max:30',
                'phone2' => 'required|max:10|min:10',
                // 'logo' => 'required',
                // 'serviceimage1' => 'required',
                // 'serviceimage2' => 'required',
                // 'serviceimage3' => 'required',
                // 'serviceimage4' => 'required'
   
         ]);
        $val=$request->all();
         foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }

          if(isset($request->userid) && isset($request->orderid)){
            $val['social_media1']['info'] = $this->infolink($request->userid,$request->orderid);
          }else{
            $val['social_media1']['info']="#";
          }
         //dd($val['social_media1']);
         
          
          $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
          $val['socialMediaList']['info']='fa-info';
          $pdf = \App::make('dompdf.wrapper');
          $customPaper = array(35,-35,360.80,730.80);
          $pdf->loadView('cards.card6',compact('val'))->setPaper($customPaper, 'portrait');
          $output = $pdf->output();
         
          $filename=$val['first_name1']." ".$val['last_name1']."_".date('d-m-Y')."_".date('H').".pdf";
          $design='design-6';
          $url=$this->pdf_upload($output,$filename,$design);
          //echo $url;
          //return $pdf->stream($val['first_name1']." ".$val['last_name1'].".pdf");

        }
        
        
        
        
        public function generate_pdf7(Request $request) {
   
     $validatedData = $request->validate([
                'primary' =>'required',
                'scondary' =>'required',
                'blog_color' =>'required',
                'button_color' =>'required',
                'text' => 'required',
                'code' =>'required',
                'address' =>'required|max:90',
                'social_media' =>'required',
                'email' =>'required',
                'first_name' =>'required|max:30',
                'last_name' =>'required|max:30',
                'bottom_title'=>'required|max:30',
                'phone' => 'required|max:10|min:10',
                'designation'=>'required|max:30',
                // 'profile_image' => 'required',
                // 'service_image' => 'required',
                
   
         ]);
          
        $val=$request->all();
          foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
         if(isset($request->userid) && isset($request->orderid)){
            $val['social_media1']['info'] = $this->infolink($request->userid,$request->orderid);
          }else{
            $val['social_media1']['info']="#";
          }
         
         $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
         $val['socialMediaList']['info']='fa-info';
        $pdf = \App::make('dompdf.wrapper');
       
          
             $customPaper = array(35,-35,334,630.80);
         $pdf->loadView('cards.card7',compact('val'))->setPaper($customPaper, 'portrait');
          $output = $pdf->output();
          $filename=$val['first_name']." ".$val['last_name']."_".date('d-m-Y')."_".date('H').".pdf";
          $design='design-7';
          $url=$this->pdf_upload($output,$filename,$design);
  }
        
        
      
      
      
      public function generate_pdf8(Request $request) {
          
           $validatedData = $request->validate([
                'primary' =>'required',
                'scondary' =>'required',
                'heading_color' =>'required',
                'blogtext_color' =>'required',
                'facebook_color' =>'required',
                'skepe_color' =>'required',
                'linkedin_color' =>'required',
                'text' => 'required',
                'code' =>'required',
                'address' =>'required|max:90',
                'about' =>'required|max:100',
                'social_media' =>'required',
                'email' =>'required',
                'first_name' =>'required|max:30',
                'last_name' =>'required|max:30',
                'phone' => 'required|max:10|min:10',
                'designation'=>'required|max:30',
                // 'profile_image' => 'required',
                // 'logo' => 'required',
                
   
         ]);
          
        $val=$request->all();
        //  dd($val);
        
          foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
          
       if(isset($request->userid) && isset($request->orderid)){
            $val['social_media1']['info'] = $this->infolink($request->userid,$request->orderid);
          }else{
            $val['social_media1']['info']="#";
          }
      
          $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
          $val['socialMediaList']['info']='fa-info';
          $val['facebook_color1']=$this->hexToHsl($val['facebook_color']);
          $val['skepe_color1']=$this->hexToHsl($val['skepe_color']);
          $val['linkedin_color1']=$this->hexToHsl($val['linkedin_color']);
       // dd($val);
        $pdf = \App::make('dompdf.wrapper');
      
          $customPaper = array(35,-35,333,590.80);
          $pdf->loadView('cards.card8',compact('val'))->setPaper($customPaper, 'portrait');
          $output = $pdf->output();
          $filename=$val['first_name']." ".$val['last_name']."_".date('d-m-Y')."_".date('H')."_".".pdf";
          $design='design-8';
          $url=$this->pdf_upload($output,$filename,$design);
    // return view('cards.test');
  }
      
      
      
      
       public function generate_pdf9(Request $request) {
           //dd($request->all());
            $validatedData = $request->validate([
                'primary' =>'required',
                'scondary' =>'required',
                'heading_color' =>'required',
                'blogtext_color' =>'required',
                'social_media_color' =>'required',
                'text' => 'required',
                'code' =>'required',
                'time' =>'required',
                'days' =>'required',
                'call_us' =>'required|max:30',
                'address' =>'required|max:90',
                'social_media' =>'required',
                'first_name' =>'required|max:30',
                'last_name' =>'required|max:30',
                'phone' => 'required|max:10|min:10',
                'designation'=>'required|max:30',
                'bottom_title'=>'required|max:40',
                'service-name-1'=>'required|max:15',
                'service-name-2'=>'required|max:15',
                'service-name-3'=>'required|max:15',
                'service-name-4'=>'required|max:20',
                'case1'=>'required|max:15',
                'case2'=>'required|max:15',
                'case3'=>'required|max:15',
                'case4'=>'required|max:15',
                // 'profile_image' => 'required',
                // 'bg_image' => 'required',
                // 'serviceimage1'=>'required',
                // 'serviceimage2'=>'required',
                // 'serviceimage3'=>'required',
                // 'serviceimage4'=>'required',
   
         ]);
          
           
             $val=$request->all();
             
             foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }

          if(isset($request->userid) && isset($request->orderid)){
            $val['social_media1']['info'] = $this->infolink($request->userid,$request->orderid);
          }else{
            $val['social_media1']['info']="#";
          }
          
        //  if($request->hasFile('profile_image')){
        //     $file = $request->file('profile_image');
        //         $height=114; $width=115;
        //         $val['profile_image']=$this->image_upload($file,$height,$width);
                
        // }
        
        //  if($request->hasFile('bg_image')){
        //     $file = $request->file('bg_image');
        //         $height=293; $width=400;
        //         $val['bg_image']=$this->image_upload($file,$height,$width);
                
        // }
      
      
        // if($request->hasFile('service-image-1')){
        //         $file = $request->file('service-image-1');
        //         $height=106; $width=154;
        //         $val['service-img-1']=$this->image_upload($file,$height,$width);
                
                
        // }
        
        //   if($request->hasFile('service-image-2')){
        //         $file = $request->file('service-image-2');
        //         $height=106; $width=154;
        //         $val['service-img-2']=$this->image_upload($file,$height,$width);
                
                
        // }
        
        //   if($request->hasFile('service-image-3')){
        //         $file = $request->file('service-image-3');
        //         $height=106; $width=154;
        //         $val['service-img-3']=$this->image_upload($file,$height,$width);
                
                
        // }
        
        //   if($request->hasFile('service-image-4')){
        //         $file = $request->file('service-image-4');
        //         $height=106; $width=154;
        //         $val['service-img-4']=$this->image_upload($file,$height,$width);
                
                
        // }
        
        //  if($val['service-name-1']!==null){
        //     $val['service-name-1']=explode(",",$val['service-name-1']);
        // }else{
        //      $val['service-name-1']='';
        // }
        
        //  if($val['service-name-2']!==null){
        //     $val['service-name-2']=explode(",",$val['service-name-2']);
        // }else{
        //      $val['service-name-2']='';
        // }
        
        //  if($val['service-name-3']!==null){
        //     $val['service-name-3']=explode(",",$val['service-name-3']);
        // }else{
        //      $val['service-name-3']='';
        // }
        
        //  if($val['service-name-4']!==null){
        //     $val['service-name-4']=explode(",",$val['service-name-4']);
        // }else{
        //      $val['service-name-4']='';
        // }
          $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
        $val['socialMediaList']['info']='fa-info';
        $pdf = \App::make('dompdf.wrapper');
        //$pdf->setPaper('A4', 'portrait'); 
           //$customPaper = array(40,-34,630.80,800.80);
              $customPaper = array(38,-35,328,580.80);
         $pdf->loadView('cards.card9',compact('val'))->setPaper($customPaper, 'portrait');
         $output = $pdf->output();
          $filename=$val['first_name']." ".$val['last_name']."_".date('d-m-Y')."_".date('H').".pdf";
          $design='design-9';
          $url=$this->pdf_upload($output,$filename,$design);
   
  }
        
  public function generate_pdf10(Request $request) {

     
                 $validatedData = $request->validate([
                'color1' =>'required',
                'color2' =>'required',
                'color3' =>'required',
                'color4' =>'required',
                'color5' => 'required',
                'color6' =>'required',
                'color7' =>'required',
                'color8' => 'required',
                'code' =>'required',
                'address' =>'required|max:90',
                'social_media' =>'required',
                'first_name' =>'required|max:30',
                'last_name' =>'required|max:30',
                'phone' => 'required|max:10|min:10',
                'designation'=>'required|max:30',
                'service-name-1'=>'required|max:40',
                'service-name-2'=>'required|max:40',
                'service-name-3'=>'required|max:40',
                'service-name-4'=>'required|max:40',
                // 'profile_image' => 'required',
                // 'logo' => 'required',
                // 'serviceimage1'=>'required',
                // 'serviceimage2'=>'required',
                // 'serviceimage3'=>'required',
                // 'serviceimage4'=>'required',
   
         ]);
          
           
             $val=$request->all();
             
        foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
          
        if(isset($request->userid) && isset($request->orderid)){
            $val['social_media1']['info'] = $this->infolink($request->userid,$request->orderid);
          }else{
            $val['social_media1']['info']="#";
          }
        
         if($val['service-name-1']!==null){
            $val['service-name-1']=$val['service-name-1'];
        }else{
             $val['service-name-1']='';
        }
        
         if($val['service-name-2']!==null){
            $val['service-name-2']=$val['service-name-2'];
        }else{
             $val['service-name-2']='';
        }
        
         if($val['service-name-3']!==null){
            $val['service-name-3']=$val['service-name-3'];
        }else{
             $val['service-name-3']='';
        }
        
         if($val['service-name-4']!==null){
            $val['service-name-4']=$val['service-name-4'];
        }else{
             $val['service-name-4']='';
        }
        
          if($val['service-name-5']!==null){
            $val['service-name-5']=$val['service-name-5'];
          }else{
             $val['service-name-5']='';
          }
           
            if($val['service-name-6']!==null){
            $val['service-name-6']=$val['service-name-6'];
          }else{
             $val['service-name-6']='';
          }
          $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
          $val['socialMediaList']['info']='fa-info';
          $pdf = \App::make('dompdf.wrapper');
          $customPaper = array(34,-35,334,593.80);
          $pdf->loadView('cards.card10',compact('val'))->setPaper($customPaper, 'portrait');
          $output = $pdf->output();
          $filename=$val['first_name']." ".$val['last_name']."_".date('d-m-Y')."_".date('H').".pdf";
          $design='design-10';
          $url=$this->pdf_upload($output,$filename,$design);
    
    
  }
         
       public function generate_pdf11(Request $request) {
          
     $validatedData = $request->validate([
                'primary' =>'required',
                'scondary' =>'required',
                'blog_color' =>'required',
                'button_color' =>'required',
                'code' =>'required',
                'text' => 'required',
                'c_email' => 'required',
                'social_media' =>'required',
                'first_name1' =>'required|max:30',
                'c_phone'=>'required|max:10|min:10',
                'last_name1' =>'required|max:30',
                'phone1' => 'required|max:10|min:10',
                'first_name2' =>'required|max:30',
                'last_name2' =>'required|max:30',
                'phone2' => 'required|max:10|min:10',
                'address' =>'required|max:90',
                'website'=>'required|max:50',
               
                
   
         ]);
        $val=$request->all();
         foreach($val['social_media'] as $key=>$value){
            $bah1=substr($val['social_media'][$key],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$key]=$val['social_media'][$key];
                    } elseif($key==3){
                        $val['social_media1'][$key]='https://wa.me/'.$val['code'].$val['social_media'][$key];
                    }else {
                $val['social_media1'][$key]='https://'.$val['social_media'][$key];  
             }
              
          }
        
         
        if(isset($request->userid) && isset($request->orderid)){
            $val['social_media1']['info'] = $this->infolink($request->userid,$request->orderid);
          }else{
            $val['social_media1']['info']="#";
          }
       
        
            if(!empty($val['website'])){
              $bah=substr($val['website'],0,4);
              if($bah=='http'){
              $val['website2']=explode('://',$val['website']);
              $val['website1']=$val['website'];
              $val['website3']= $val['website2'][1];
          
          }else{
              $val['website2']=$val['website'];
              $val['website1']='https://'.$val['website']; 
              $val['website3']= $val['website2'];
          }
          }else{
              $val['website1']='';
                $val['website3']='';
          }
      
        
        
         $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
         $val['socialMediaList']['info']='fa-info';
        
          $pdf = \App::make('dompdf.wrapper');
          $customPaper = array(35,-35,334,640.80);
          $pdf->loadView('cards.card11',compact('val'))->setPaper($customPaper, 'portrait');
          $output = $pdf->output();
          $filename=$val['first_name1']." ".$val['last_name1']."_".date('d-m-Y')."_".date('H').".pdf";
          $design='design-11';
          $url=$this->pdf_upload($output,$filename,$design);
   
  }
    
        
        
        //pdf upload file   
    
       public function pdf_upload($output,$filename,$design){
          
             $user=Auth()->user();
             //$todaydate = date('d-m-Y');
             $destinationPath = public_path('card/pdf/'.$user->name.'/'.$design);
           if (!is_dir($destinationPath)) {
              mkdir($destinationPath, 0777, true);
            }
          
          file_put_contents($destinationPath.'/'.$filename, $output);
          $url="/card/pdf/".$user->name.'/'.$design.'/'.$filename;
          echo '<script type="text/javascript">window.location.href = "'.url($url).'";</script>';
         
        }
        
    
   //delete image 
    public function dele_img() {
       $imh= glob('card/image/*');
       foreach($imh as $imj){
        unlink($imj);
        }
        
    // $imh1= glob('card/img/*');
    //   foreach($imh1 as $imj1){
    //     unlink($imj1);
    //     }
        
        
       echo "all delete..!";
       }
       
  //image upload system       
  public function image_upload($file,$height,$width){
      
                  $input = rand(10,10000).time().'.'.$file->getClientOriginalExtension();
                  $destinationPath = public_path('card/image');
                  $img = Image::make($file->getRealPath());
                  $img->resize($width,$height, function ($constraint) {
		          $constraint->aspectRatio();
		          })->save($destinationPath.'/'.$input);
                  return $input;
  }
  
   public function croppie()
    {
      return view('cards.croppie');
    }
   
 public function crop_image(Request $request){
    //  if($request->hasFile('crop_image')){
    //           $file = $request->file('crop_image');
    //           $input = rand(10,10000).time().'.'.$file->getClientOriginalExtension();
    //           $destinationPath = public_path('card/crop_image');
    //           file_put_contents($destinationPath, $input);
    //         //   $file->move($destinationPath,$input);
    //           echo $destinationPath.'/'.$input;
               
              
    //     }
        
         $image = $request->image;

        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);
        $image_name= time().'.png';
        $path = public_path('card/crop_image/'.$image_name);

        file_put_contents($path, $image);
        return response()->json(['status'=>true]);
              
        }
 
  function infolink($userid,$orderid) {
    return route('GetQRcode',[endeCrypt($userid),endeCrypt($orderid)]);
  }
 function hexToHsl($hex) {
    $r = "";
    $g = "";
    $b = "";

    $hex = str_replace('#', '', $hex);

    if (strlen($hex) == 3) {
        $r = substr($hex, 0, 1);
        $r = $r . $r;
        $g = substr($hex, 1, 1);
        $g = $g . $g;
        $b = substr($hex, 2, 1);
        $b = $b . $b;
    } elseif (strlen($hex) == 6) {
        $r = substr($hex, 0, 2);
        $g = substr($hex, 2, 2);
        $b = substr($hex, 4, 2);
    } else {
        return false;
    }

    $r = hexdec($r);
    $g = hexdec($g);
    $b = hexdec($b);
    $rgb=$r.','.$g.','.$b;
    return $rgb;
}


}
