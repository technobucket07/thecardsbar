<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\User;
use Validator;
use Carbon\Carbon;
use App\Mail\Sendotp;
class JwtAuthController extends Controller
{
  /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct() {
        //$this->middleware('auth:api', ['except' => ['login', 'register','userverify']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request){
    	$validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:8',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }
        $data = $request->only(['email','password']);

        $user =  User::where('email','=',$data['email'])->first();
        if($user && is_null($user->email_verified_at)){
            return response()->json([
            'status'=>false,
            'notverified'=>true,
            'email'=>$data['email'],
            'massage'=> "Please verify your email first with otp".$user->otp
            ], 200);
        }

        if (! $token = auth('api')->attempt($data)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->createNewToken($token);
    }

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'phone' => 'required|string|min:10|unique:users',
            'password' => 'required|string|confirmed|min:8',
        ]);

        if($validator->fails()){
            return response()->json($validator->errors(), 400);
        }
        //echo"<pre>";print_r($request->all());die;
        $data = $request->except(['password_confirmation']);
        $otp = rand(100000, 999999);
        $user = User::create(array_merge(
                    $data,
                    [
                        'password' => Hash::make($request->password),
                        'otp' => $otp
                    ]
                ));

        return response()->json([
            'message' => 'User successfully registered',
            'user' => $user
        ], 201);
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout() {
        auth('api')->logout();

        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh() {
        return $this->createNewToken(auth('api')->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile() {
        return response()->json(auth('api')->user());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token){
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60,
            'user' => auth('api')->user()
        ]);
    }


    public function UserVerify(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email'     => 'required',
            'otp'  => 'required'
        ]);
        if( $validate->fails()){
            return response()->json(['status'=>false,
                'massage' => $validate->errors()->first()], 200);
        }

        $user = User::where('email','=',$request->email)
                ->where('otp','=',$request->otp)->first();
        if($user){
           $user->update(['otp'=>'','email_verified_at'=>Carbon::now()]);
           return response()->json([
            'status'=>true,
            'massage'=> "Successfully Verified Please Login"
        ], 200);
        }else{
            return response()->json([
            'status'=>false,
            'massage'=> "Please Enter Correct Otp"
        ], 200);
        }
        
    }
}
