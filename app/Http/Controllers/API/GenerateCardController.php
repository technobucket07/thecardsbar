<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SocialMedia;


class GenerateCardController extends Controller
{
    public function generateCard1(Request $request)
    {
        $input = $request->input();
        $data['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();

        if($request->hasFile('logo')){
            $file = $request->file('logo');
            $input['logo'] = rand() .'.'. $file->getClientOriginalExtension();
            $file->move('card/logo',$input['logo']);
        }
        $data['input'] = $input;

         $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('cards.personal_card', $data);
        return $pdf->stream();
    }

    public function generateCard(Request $request)
    {
        $input = $request->input();
        $data['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();

        if($request->hasFile('logo')){
            $file = $request->file('logo');
            $input['logo'] = time().rand() .'.'. $file->getClientOriginalExtension();
            $file->move('card/logo',$input['logo']);
        }

        if($request->hasFile('service_logo')){
            $files = $request->file('service_logo');
            foreach($files as $key=>$file){
                $service_logo = $key.rand() .'.'. $file->getClientOriginalExtension();
                $file->move('card/services',$service_logo);
                $input['servicelogo'][] = $service_logo;
            }
        }
        $data['input'] = $input;
       // return view('cards.professional_card', $data);

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('cards.professional_card', $data);
        return $pdf->stream();
    }
}
