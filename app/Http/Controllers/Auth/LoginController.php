<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Auth;
Use Mail;
use App\User;
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function UserLoginajax(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);

        if( $validate->fails()){
             return response()->json(['status'=>false,
                'massage' =>$validate->errors()->all()[0]], 200);
        }

        $credentials = $request->only('email', 'password');
        $user =  User::where('email','=',$credentials['email'])->first();
        if($user && is_null($user->email_verified_at)){
            return response()->json([
            'status'=>false,
            'notverified'=>true,
            'email'=>$credentials['email'],
            'massage'=> "Please verify your email first with otp".$user->otp
            ], 200);
        }
        if (Auth::attempt($credentials)) {

            $data = [
                'name' => Auth::user()->name,
            ];
//            $res = Mail::send('emails.login', $data, function ($message) {
//                $message->from(env('MAIL_FROM_ADDRESS'), 'Cardsbar');
//                $message->to(Auth::user()->email)->cc('bar@example.com')->subject('Login Notice');
//            });

            return response()->json([
            'status'=>true,
            'isadmin'=>$user->user_role,
            'massage'=> "Successfully Logged"
            ], 200);
        }

        return response()->json([
            'status'=>false,
            'massage'=> "You have entered invalid credentials"
            ], 200);
    }
}
