<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Mail\Sendotp;
use Illuminate\Foundation\Auth\RegistersUsers;
use Carbon\Carbon;
Use Mail;
use Auth;
use Socialite;
class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/login';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }


    protected $providers = [
        'github','facebook','google','twitter'
    ];

    private function isProviderAllowed($driver)
    {
        return in_array($driver, $this->providers) && config()->has("services.{$driver}");
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }


    public function registerUser(Request $request)
    {
        //echo"<pre>";print_r($request->all()); die;
        $validate = Validator::make($request->all(), [
            'name'      => 'required',
            'email'     => 'required|email|unique:users|max:255',
            'phone'     => 'required|min:10|numeric',
            'password'  => 'required|confirmed|max:255'
        ]);
        if( $validate->fails()){
             return response()->json(['status'=>false,
                'errors' => $validate->messages()], 200);
        }
        $otp = rand(100000, 999999);
        $create = User::create([
            'password'   => Hash::make($request->password),
            'email'      => $request->email,
            'name'       => $request->name,
            'phone'      => $request->phone,
            'otp'        => $otp
        ]);
            $details = [
                'otp' => $otp,
            ];
            Mail::to($request->email)->send(new Sendotp($details));
        return response()->json([
            'status'=>true,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'otp'=>'',
            'massage'=> "Please Verify Your Email"
        ], 200);
    }

    public function UserVerify(Request $request)
    {
        $validate = Validator::make($request->all(), [
            'email'     => 'required',
            'otp'  => 'required'
        ]);
        if( $validate->fails()){
            return response()->json(['status'=>false,
                'massage' => $validate->errors()->first()], 200);
        }

        $user = User::where('email','=',$request->email)
                ->where('otp','=',$request->otp)->first();
        if($user){
           $user->update(['otp'=>'','email_verified_at'=>Carbon::now()]);
           Auth::login($user, true);
           return response()->json([
            'status'=>true,
            'massage'=> "Successfully Verified Please Login"
        ], 200);
        }else{
            return response()->json([
            'status'=>false,
            'massage'=> "User Not Found. Please Enter Correct Otp"
        ], 200);
        }
        
    }

    public function reSendOtp(Request $request){
        $email =$request->email;
        $user = User::where('email','=',$email)->first();
        if(is_null($user->otp)){
            $otp = rand(100000, 999999);
            $user->update(['otp'=>$otp]);
        }
        $details = [
                'otp' =>$user->otp,
            ];
        Mail::to($request->email)->send(new Sendotp($details));
        return response()->json([
            'status'=>true,
            'otp'=>'',
            'massage'=> "Successfully sent"
        ], 200);
    }

    public function redirectToProvider($driver)
    {
        if( ! $this->isProviderAllowed($driver) ) {
            return "{$driver} is not currently supported";
        }

        try {
            return Socialite::driver($driver)->redirect();
           // return response()->json($hh);
        } catch (Exception $e) {
            // You should show something simple fail message
            return $e->getMessage();
        }
    }

    public function handleProviderCallback($provider)
    {  
      //   $data  = array(
      //               'name' => 'gdfg',
      //               'email'=>'horero9730@leonvero.com',
      //               'avatar'=>'fdgfdg',
      //               'provider'=>'dfgfdg',
      //               'provider_id'=>'dfgdf',
      //               'access_token'=>'fdgfdgfd',
      //               );
      // return redirect()->route('home')->with(['socialLogin' =>'Unable to login, Please add your email Or try with another provider to login.','loginData'=>$data]);
        try
        {
            $providerUser = Socialite::driver($provider)->user();
        }
        catch(\Exception $e)
        {   
             return $e->getMessage();
        }
        $user = User::where('email', $providerUser->getEmail())->first();
        if($user) {
            $user->update([
                'avatar' => $providerUser->avatar, // update the avatar and provider that might have changed
                'provider' => $provider,
                'provider_id' => $providerUser->id,
                'access_token' => $providerUser->token
            ]);
        } else {
            if($providerUser->getEmail()){
                $user = User::create([
                    'name' => $providerUser->getName(),
                    'email' => $providerUser->getEmail(),
                    'avatar' => $providerUser->getAvatar(),
                    'provider' => $provider,
                    'provider_id' => $providerUser->getId(),
                    'access_token' => $providerUser->token,
                    'password' => ''  // user can use reset password to create a password
                ]);
            }else{
                $data  = array(
                    'name' => $providerUser->getName(),
                    // 'email'=>$providerUser->getEmail(),
                    'avatar'=>$providerUser->getAvatar(),
                    'provider'=>$provider,
                    'provider_id'=>$providerUser->getId(),
                    'access_token'=>$providerUser->token,
                    );
               return redirect()->route('home')->with(['socialLogin' =>'Unable to login, Please add your email Or try with another provider to login.','loginData'=>$data]);
            }
        }
        //echo"<pre>"; print_r($user);die;
        Auth::login($user, true);
        return redirect()->route('home');
    }

    public function socialAddEnail(Request $request){
        //echo "<pre>";print_r($request->all());die;
        $validate = Validator::make($request->all(), [
            'email'     => 'required|email|unique:users|max:255',
        ]);
        if( $validate->fails()){
            return response()->json(['status'=>false,
                'errors' => $validate->messages()], 200);
        }else{
            $otp = rand(100000, 999999);
            $data = $request->except(['_token']);
            $data['otp']= $otp;
            User::create($data);
            $details = [
                'otp' => $otp,
            ];
            Mail::to($request->email)->send(new Sendotp($details));
        return response()->json([
            'status'=>true,
            'email'=>$request->email,
            'phone'=>$request->phone,
            'otp'=>'',
            'massage'=> "Please Verify Your Email"
        ], 200);
        }
    }

}
