<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class PDFController extends Controller
{

    public function convertToPDF()
    {

        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadView('pdf.card');
        return $pdf->stream();

    }


}
