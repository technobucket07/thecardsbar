<?php

namespace App\Http\Controllers\Frontend;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SocialMedia;
use App\Models\Order;
use App\Models\CardTemplate;
use App\Models\TempCardDetails;
use App\Models\CardCategories;
use App\Models\SubCategory;
use App\Models\Orderfield;
use App\Models\OrderCardDetails;
use App\Models\Contactus;
use App\Models\Pages;
use App\Models\PageContent;
use App\Models\CardLikes;
use App\Models\CardViews;
use App\Models\UsersCard;
use App\Models\UserCardViews;
use App\Models\Cardtype;
use Illuminate\Support\Facades\Input;
use JeroenDesloovere\VCard\VCard;
use Razorpay\Api\Api;
use Carbon\Carbon;
use App\Mail\Feedback;
use App\Mail\SuccessPayment;
use Image;
use Auth;
use DB;
use Mail;
class HomeController extends Controller
{
    public function index()
    {

        $categories = Cardtype::all()->pluck('name','id')->toArray();
        $cards = CardTemplate::orderBy('created_at', 'desc')->where('is_enabled','=',1)->paginate(6);

        $totalCrads = 0;

        return view('frontend.home',compact('cards','categories','totalCrads'));
    }


    public function Ecards(Request $request)
    {
      if(isset($request->category)){
        $cards = CardTemplate::where('cardtype','=',$request->category)
        ->where('is_enabled','=',1)
        ->orderBy('created_at', 'desc')->paginate(20);
      }else{
        $cards = CardTemplate::orderBy('created_at', 'desc')
        ->where('is_enabled','=',1)
        ->paginate(20);
      }
      $categories = Cardtype::all()->pluck('name','id')->toArray();
      return view('frontend.ecards',compact('cards','categories'));
    }

    public function pagesContent($pagename){
      $pages = Pages::where('name',$pagename)
      ->leftJoin('page_contents', 'page_contents.page_id', '=', 'pages.id')
      ->select('*')->get();
      $content = array();
      foreach ($pages as $key => $value) {
        $content[$value->title] = $value->content;
      }

      return $content;
    }

    public function aboutUs()
    {
      $content = $this->pagesContent('about-us');
      return view('frontend.about',compact('content'));
    }

    public function refundPolicy()
    {
        $content = $this->pagesContent('refund-policy');
        return view('frontend.refund',compact('content'));
    }

    public function ContactUs()
    {
      $content = $this->pagesContent('contact-us');
      return view('frontend.contact',compact('content'));
    }

    public function privacyPolicy()
    {
      $content = $this->pagesContent('Policy');
      return view('frontend.privacy',compact('content'));
    }

    public function termsConditions()
    {
      $content = $this->pagesContent('Terms');
      return view('frontend.terms',compact('content'));
    }

    public function PostContactUs(Request $request)
    {
      $request->validate([
        // 'name' => 'required',
        'email' => 'required|email',
        // 'phone' => 'required'
      ]);
     $details= array(
           'name' => $request->get('name'),
           'email' => $request->get('email'),
           'phone_number' => $request->get('phone'),
           'user_message' => $request->get('message'),
       );
      $email = Mail::to(env('MAIL_FROM_ADDRESS'))->send(new Feedback($details));

      $data =$request->except('_token');
      Contactus::create($data);
      
      return back()->with('success','Message Successfully Sended');
    }

  public function PlaceOrder(Request $request){
//    echo"<pre>";print_r( $request->all());die;
    $rules =[];
    if($request->temp_card_id){
      $card = CardTemplate::find($request->temp_card_id);
      $customMessages = [];

      if($card->minsocialmedia !=''){
        $min = $card->minsocialmedia;
        $customMessages['social_media.*.required']="social media at least $min  is required";
        $rules["social_media"] = "required|array|min:$min";
        $rules["social_media.*"]  = "required";
      }
      $rules["location"] ='required';
      if($card->profile == '1'){
        $rules["profileimg"] ='required';
      }
      if($card->logo == '1'){
        $rules["logo"] ='required';
      }

      if($card->background == '1'){
        $rules["backgroundimg"] ='required';
      }
      if($card->gallery == '1'){
        $min = $card->min_no_gallery;
        $customMessages['gallery.*.required']="gallery image at least $min  is required";
        $rules["gallery"] = "required|array|min:$min";
        $rules["gallery.*"]  = "required";
      }
      if($card->services == '1'){
        $min = $card->min_no_services;
        $customMessages['services.*.required']="services at least $min  is required";
        $rules["services"] = "required|array|min:$min";
        $rules["services.*.title"]  = "required";

        if($card->serviceswithimg =='1'){
          $customMessages['services.*.image.required']="services Image  is required";
          $rules["services.*.image"]  = "required";
        }

        if($card->servicedescription =='1'){
          $rules["services.*.description.required"]  = "services Description  is required";
          $rules["services.*.description"]  = "required";
        }
      }
      if($card->username == '1'){
        $rules["first_name"] ='required';
        $rules["last_name"] ='required';
      }
      if($card->primaryphone == '1'){
        $rules["primaryphone"] ='required';
      }
      if($card->secondaryphone == '1'){
        $rules["secondaryphone"] ='required';
      }
      if($card->primaryemail == '1'){
        $rules["primaryemail"] ='required';
      }
      if($card->secondaryemail == '1'){
        $rules["secondaryemail"] ='required';
      }
      if($card->address == '1'){
        $rules["address"] ='required';
      }
      if($card->website == '1'){
        $rules["website"] ='required';
      }
      if($card->designation == '1'){
        $rules["designation"] ='required';
      }
        $rules["official_location"] ='required';

      if($card->description == '1'){
         $min = $card->min_no_description;
         $customMessages['description.required']="description at least $min  is required";
        $rules["description"] = "required|array|min:$min";
          $rules["description.*"]  = "required";
      }
      $validator = Validator::make($request->all(), $rules,$customMessages);
      if ($validator->fails()) {
        //echo"<pre>";print_r($validator);die;
          // return back()->withErrors($validator);
        return webResponse(false, 206, $validator->getMessageBag());
      }else{
        //echo"<pre>";print_r($request->all());die;
        // if(!empty($request->social_media)){
        //   $request->merge($request->social_media);
        //   unset($request['social_media']);
        // }
        TempCardDetails::where('user_id','=',auth()->id())
        ->where('temp_card_id','=',$request->temp_card_id)->delete();

        $data = $request->except(['_token','temp_card_id']);
        $user = auth()->user();

        //create order
        $insertdata['card_template_id']   = $request->temp_card_id;
        $insertdata['user_id'] = $user->id;

        if (isset($request->first_name)) {
          $insertdata['first_name'] = $request->first_name;
        }

        if (isset($request->last_name)) {
          $insertdata['last_name'] = $request->last_name;
        }

        if (isset($request->address)) {
          $insertdata['address'] = $request->address;
        }
        if (isset($request->primaryemail)) {
          $insertdata['email'] = $request->primaryemail;
        }

        if( isset($request->official_location) ){
            $insertdata['official_location'] = $request->official_location;
        }

        $insertdata['price']   = $card ? $card->price :0;
        $insertdata['card_theme_id'] =$data['card_theme_id'];
        $order = Order::create($insertdata);
//        dd($order);

        $filepath = 'users/'.endeCrypt($user->id).'/orders/'.endeCrypt($order->id);
        if($request->hasFile('profileimg')){
          $file = $request->file('profileimg');
          $data['profileimg']=uploadImage($file,$filepath."/profile");
        }
        if($request->hasFile('backgroundimg')){
          $file = $request->file('backgroundimg');
          $data['backgroundimg']=uploadImage($file,$filepath."/background");
        }
        if($request->hasFile('logo')){
          $file = $request->file('logo');
          $data['logo']=uploadImage($file,$filepath."/logo");
        }
        if($galleries=$request->file('gallery')){
          foreach($galleries as $key => $gallery){
            $data['gallery'][$key]=uploadImage($file,$filepath."/gallery");
          }
        }
        // if($serviceimages=$request->file('serviceimage')){
        //   foreach($serviceimages as $key => $serviceimage){
        //     $data['serviceimage'][$key] = uploadImage($file,$filepath."/services");
        //   }
        // }
        // if(!empty($data['services'])){
        //   if (!empty($data['serviceimage'])) {
        //     $data['services']= array_combine($data['services'],$data['serviceimage']);
        //     unset($data['serviceimage']);
        //   }
        // }
       
       // echo "<pre>";print_r($data);die;
       $i=1;
        foreach ($data as $fieldname => $fieldvalue) {
          if(is_array($fieldvalue)){
            foreach ($fieldvalue as $key => $value) {
              //echo"$key";die;
              if($fieldname=='services'){
                $servicedata['order_id'] = $order->id;
                $servicedata['user_id']  = $user->id;
                $servicedata['fieldname']= $fieldname;
                $servicedata['official_location']= $request->official_location;
                $servicedata['field']    = $key;
                foreach ($value as $key2 => $value2) {
                  $servicedata['subfieldname']= $key2;
                  if($key2=='image'){
                    $servicedata['fieldvalue']= uploadImage($value2,$filepath."/services");
                  }else{
                    $servicedata['fieldvalue']= $value2;
                  }
                  Orderfield::create($servicedata);
                }
              }else{
                Orderfield::create(['order_id'=>$order->id,'user_id'=>$user->id,'fieldname'=>$fieldname,'fieldvalue'=>$value,'subfieldname'=>$key]);
              }
            }
          }else{
            Orderfield::create(['order_id'=>$order->id,'user_id'=>$user->id,'fieldname'=>$fieldname,'fieldvalue'=>$fieldvalue]);
          }
        }

         return webResponse(true, 200,'Successfully Added in Order Lists, Please Pay Amount',['redirect'=>route('checkout',[$order->id])]);
      }
    }
  }

  public function getTempForm(Request $request)
  {
    $tempcard = TempCardDetails::where('user_id','=',auth()->id())
      ->where('temp_card_id','=',$request->temp_card_id)->first();
      if($tempcard){
        //$tempcard->card_details= unserialize($tempcard->card_details);
        // echo"<pre>";print_r();die;
        return response()->json(['status'=>true,'data'=>$tempcard]); 
      }else{
        return response()->json(['status'=>false]);
      }
  }

  public function TempCardDetails(Request $request){
    $rules = array(
      'temp_card_id' => 'required'
    );
    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
      die('validation errror');
    } else {
      $data = $request->except(['_token']);
      
      $data1['card_details'] =json_encode($data);
      $data1['user_id'] = auth()->id();
      $data1['temp_card_id'] = $data['temp_card_id'];
      //echo "<pre>";print_r($data1);die;
      $tempcard = TempCardDetails::where('user_id','=',auth()->id())
      ->where('temp_card_id','=',$data['temp_card_id'])->first();
      if($tempcard){
        $tempcard->update($data1);
      }else{
        TempCardDetails::create($data1);
      }
      die('update');
    }
  }

   public function PayNow(Request $request){
    $user = auth()->user();
    $youorder = Order::where('id', '=', $request->order_id)->where('user_id', '=', $user->id)->first();
    $razorderamount = $youorder->price*100;
    $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
    $razorder  = $api->order->create(array('receipt' => $youorder->id, 'amount' =>$razorderamount, 'currency' => env('RAZOR_CURRENCY')));
    $youorder->update(['razorpay_order_id'=>$razorder->id]);
    $pay['order_id']    = $youorder->id;
    $pay['razor_order_id'] = $razorder->id;
    $pay['amount']      = $razorderamount;
    $pay['username']    = $youorder->first_name.' '.$youorder->last_name;
    $pay['useremail']   = $youorder->email;
    $pay['ordername']   = "Thecardsbar";
    $pay['description'] = "Buy Your Visting Cards";
    $pay['currency']    = env('RAZOR_CURRENCY');
    $pay['address']     = $youorder->address;

    return view('razorpay', compact('pay'));

   }
   public function sendSuccessMail($orderID = null){

       $orderDetail = Order::where('id',$orderID)->first();
       $cardDetail = CardTemplate::where('id',$orderDetail->card_template_id)->first();

       $data = array(
           'name'      => Auth::user()->name,
           'user_id'   => Auth::user()->id,
           'card_name' => $cardDetail->cardtitle,
           'card_id'   => $cardDetail->card_id,
           'date'      => Carbon::parse(Carbon::now())->format('d-M-Y'),
           'price'     => $orderDetail->price,
           'pdf'       => "http://thecardsbar.com/qvSudby7MKkSwUIGQhds0TyzAcrDxjT24RIFFfF1.pdf",
       );
       $res = Mail::send('emails.success_payment', $data, function ($message) {
           $message->from(env('MAIL_FROM_ADDRESS'), 'Cardsbar');
           $message->to(env('ADMIN_EMAIL'))->cc('bar@example.com')->subject('Order Alert');;
       });

       return true;
   }

  public function razorpaySuccess(Request $request){

    $this->sendSuccessMail($request->order_id);

    if(isset($request->razorpay_payment_id)){
        $order = Order::where('razorpay_order_id',$request->razorpay_order_id)->first();
        $data= [
            'payment_status'=>'2',
            'payment_id'=>$request->razorpay_payment_id,
            'razorpay_signature'=>$request->razorpay_signature,
            'purchased_at'=>Carbon::now(),
            ];
        $order->update($data);
        return 'success';
    }else{
        return 'failed';
    }
  }

  public function ThanksYou($id){
    return view('frontend.thankspage');
  }


    public function cardSteps($id)
    {  
      $url=file_get_contents("http://robottxt.tdclients.work/text_frequency/country.json");
      $data['va']=json_decode($url);
       
      $url1=file_get_contents("https://jonasjacek.github.io/colors/data.json");
      $data['color']=json_decode($url1);
      //dd($data['color']);
        $data['socialMedia'] = SocialMedia::all();
        $data['card_id'] =$id;
        if($id=='1'){
            return view('frontend.card_steps', $data);
        }
        
         if($id=='2'){
            return view('frontend.card_steps1', $data);
        }
         if($id=='4'){
            return view('frontend.card_steps4', $data);
        }
        
         if($id=='6'){
            return view('frontend.card_steps6', $data);
        }
        
    }

  public function getSubcategory(Request $request){
     
      $subcategories = SubCategory::where('category_id','=',$request->category_id)->pluck('name','id')->toArray();
      //array_unshift($subcategories, "Select");
      return response()->json($subcategories);
  }


  public function dynamicCardSteps($id)
  {  
      $url   = file_get_contents(url('country.json'));
      $va    = json_decode($url);
      $url1  = file_get_contents(url('color.json'));
      $color = json_decode($url1);
      $socialMedia = SocialMedia::all();
      $categories = CardCategories::all()->pluck('name','id')->toArray();
       //array_unshift($categories, "Select");
      $card   = CardTemplate::find($id);
      $tempcard = TempCardDetails::where('user_id','=',auth()->id())
      ->where('temp_card_id','=',$id)->first();
      $cardtempdetail =[];
      if($tempcard){
        $cardtempdetail = (array)json_decode($tempcard->card_details);
      }
      $data['user_id']=auth()->id();
      $data['card_template_id']=$id;
      $like['views']=1;
      CardViews::updateOrCreate($data,$like);
      //$cardtempdetail = $cardetail;
      // if(!empty($card)){
      //   foreach ($cardetail as $field) {
      //     $cardtempdetail[$field->name] = $field->value;
      //   }
      // }
      // if(array_key_exists('social_media[Facebook]',$cardtempdetail)){
      //   echo "yes";
      // }else{
      //   echo "no";
      // }
      //echo"<pre>";print_r($cardtempdetail);die;
    return view('frontend.dynamic_card_steps',compact('url','va','color','socialMedia','card','cardtempdetail','categories'));
     
  }
    
  public function imageUpload(Request $request){
    $img="";
    if ($request->hasFile('image')) {
      $image = $request->file('image');
      $img ='storage/'.uploadImage($image,'card/image/');
      //$img=url($img2);
    }
    echo $img;
  }

  public function imageDelete(Request $request){
    //echo"<pre>";print_r($request->all());die;
    unlink($request->imagepath);
    echo 1;
  }
  
  function vcardDownload(){
    $path = storage_path('app/public/')."users/28/orders/35/vcf";
    $this->vcard(28,32,$path);
  }

  function vcard($userid, $orderid,$savepath=''){
    $additional = '';
    $prefix = '';
    $suffix = '';
    $lastname = '';
    $firstname = '';
    $vcard = new VCard();
    $userinfo = Orderfield::where('user_id','=',$userid)
              ->where('order_id','=',$orderid)->get();
    $vcard->setSavePath($savepath);
    foreach ($userinfo as $value) {
      if($value->fieldname =='last_name'){
        $lastname =$value->fieldvalue;
      }

      if($value->fieldname =='first_name'){
        $firstname =$value->fieldvalue;
      }

      if($value->fieldname =='company'){
        $vcard->addCompany($value->fieldvalue);
      }

      if($value->fieldname =='designation'){
        $vcard->addJobtitle($value->fieldvalue);
      }

      if($value->fieldname =='designation'){
        $vcard->addCompany($value->fieldvalue);
        $vcard->addRole($value->fieldvalue);
      }

      if($value->fieldname =='designation'){
        $vcard->addCompany($value->fieldvalue);
        $vcard->addRole($value->fieldvalue);
      }

      if($value->fieldname =='primaryemail'){
        $vcard->addEmail($value->fieldvalue,'Primary');
      }

      if($value->fieldname =='scondaryemail'){
        $vcard->addEmail($value->fieldvalue,'Scondary');
      }

      if($value->fieldname =='primaryphone'){
        $vcard->addPhoneNumber($value->fieldvalue, 'PREF;WORK');
      }

      if($value->fieldname =='scondaryphone'){
        $vcard->addPhoneNumber($value->fieldvalue, 'HOME');
      }

      if($value->fieldname =='address'){
        $vcard->addAddress($value->fieldvalue);
      }

      if($value->fieldname =='website'){
        $vcard->addURL($value->fieldvalue);
      }
      if($value->fieldname =='profileimg'){
        $vcard->addPhoto(url($value->fieldvalue));
      }
      if($value->fieldname =='logo'){
        $vcard->addLogo(url($value->fieldvalue));
      }

    }
    //$vcard->addLabel('street, worktown, workpostcode Belgium');

    return  $vcard->save();
  }
}
