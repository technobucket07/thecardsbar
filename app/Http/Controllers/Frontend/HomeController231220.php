<?php

namespace App\Http\Controllers\Frontend;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SocialMedia;
use App\Models\Order;
use App\Models\CardTemplate;
use App\Models\TempCardDetails;
use App\Models\CardCategories;
use App\Models\SubCategory;
use Illuminate\Support\Facades\Input;
use Razorpay\Api\Api;
use Image;
use Auth;
use DB;
class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.home');
    }

  public function PlaceOrder(Request $request){

    $rules =[];
    if($request->card_id){
      $card = CardTemplate::find($request->card_id);
      $colortype = unserialize($card->colortype);

      if(!empty($colortype)){
       foreach ($colortype as $type) {
        $rules["$type"] ='required';
       }
      }

      if($card->minsocialmedia !=''){
        $min = $card->minsocialmedia;
        $rules["social_media"] = "required|array|min:$min";
        $rules["social_media.*"]  = "required|min:$min";
      }
      if($card->profile == '1'){
        $rules["profileimg"] ='required';
      }
      if($card->logo == '1'){
        $rules["logo"] ='required';
      }

      if($card->background == '1'){
        $rules["backgroundimg"] ='required';
      }
      if($card->gallery == '1'){
        $min = $card->min_no_gallery;
        $rules["gallery"] = "required|array|min:$min";
        $rules["social_media.*"]  = "required|min:$min";
      }
      if($card->services == '1'){
        $min = $card->min_no_services;
        $rules["services"] = "required|array|min:$min";
        $rules["services.*"]  = "required|min:$min";
        if($card->serviceswithimg =='1'){
          $rules["serviceimage"] = "required|array|min:$min";
          $rules["serviceimage.*"]  = "required|min:$min";
        }
      }
      if($card->username == '1'){
        $rules["first_name"] ='required';
        $rules["last_name"] ='required';
      }
      if($card->primaryphone == '1'){
        $rules["primaryphone"] ='required';
      }
      if($card->secondaryphone == '1'){
        $rules["secondaryphone"] ='required';
      }
      if($card->primaryemail == '1'){
        $rules["primaryemail"] ='required';
      }
      if($card->secondaryemail == '1'){
        $rules["secondaryemail"] ='required';
      }
      if($card->address == '1'){
        $rules["address"] ='required';
      }
      if($card->website == '1'){
        $rules["website"] ='required';
      }
      if($card->designation == '1'){
        $rules["designation"] ='required';
      }
      if($card->description == '1'){
         $min = $card->min_no_description;
        $rules["description"] = "required|array|min:$min";
          $rules["description.*"]  = "required|min:$min";
      }

      $validator = Validator::make($request->all(), $rules);
      if ($validator->fails()) {
           return back()->withErrors($validator);
      }else{
        $data = $request->except(['_token','card_id']);
        $user = auth()->user();
        $filepath =  'card/image/'.$user->id.'/'.$request->card_id.'/'.uniqid();
        if($request->hasFile('profileimg')){
          $file = $request->file('profileimg');
          $data['profileimg']=$this->cardimage_upload($file,$filepath."/profile");
        }
        if($request->hasFile('backgroundimg')){
          $file = $request->file('backgroundimg');
          $data['backgroundimg']=$this->cardimage_upload($file,$filepath."/background");
        }
        if($request->hasFile('logo')){
          $file = $request->file('logo');
          $data['logo']=$this->cardimage_upload($file,$filepath."/logo");
        }
        if($galleries=$request->file('gallery')){
          foreach($galleries as $key => $gallery){
            $data['logo']=$this->cardimage_upload($file,$filepath."/gallery");
          }
        }
        if($serviceimages=$request->file('serviceimage')){
          foreach($serviceimages as $key => $serviceimage){
            $data['serviceimage'][$key] = $this->cardimage_upload($file,$filepath."/services");
          }
        }

        $usercard = json_encode($data);
        $insertdata['card_details'] =$usercard;
        $insertdata['card_template_id']   = $request->card_id;
        $insertdata['user_id'] = $user->id;
        $insertdata['price']   = $card ? $card->price :0;
        Order::create($insertdata);
        return back()->with('success','Successfully Added in Order Lists, Please Pay Amount');
      }
    }
  }

  public function getTempForm(Request $request)
  {
    $tempcard = TempCardDetails::where('user_id','=',auth()->id())
      ->where('card_id','=',$request->card_id)->first();
      if($tempcard){
        //$tempcard->card_details= unserialize($tempcard->card_details);
        // echo"<pre>";print_r();die;
        return response()->json(['status'=>true,'data'=>$tempcard]); 
      }else{
        return response()->json(['status'=>false]);
      }
  }

  public function TempCardDetails(Request $request){
    $rules = array(
      'card_id' => 'required',
      'card_details' => 'required',
    );
    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
      die('validation errror');
    } else {
      //echo "<pre>";print_r($request->all());die;
      $data = $request->except(['_token']);
      $data1['card_details'] =$data['card_details'];
      $data1['user_id'] = auth()->id();
      $data1['card_id'] = $data['card_id'];
      $tempcard = TempCardDetails::where('user_id','=',auth()->id())
      ->where('card_id','=',$data['card_id'])->first();
      if($tempcard){
        $tempcard->update($data1);
      }else{
        TempCardDetails::create($data1);
      }
      die('update');
    }
  }


   public function YouOrder(Request $request){
    $user = auth()->user();
    // $orders = Order::where('user_id','=',$user->id)->get();
    $orders = DB::table('card_templates')
      ->join('orders', function ($join) use ($user) {
      $join->on('orders.card_template_id', '=', 'card_templates.id')
      ->select('orders.*','card_templates.picture')
      ->where('user_id', '=', $user->id);
      })->get();
    //echo "<pre>";print_r($orders);die;
    return view('frontend.yourorder',compact('orders'));
   }

   public function PayNow(Request $request){
    $user = auth()->user();
    $youorder = Order::where('id', '=', $request->order_id)->where('user_id', '=', $user->id)->first();
    $razorderamount = $youorder->price*100;
    $api = new Api(env('RAZOR_KEY'), env('RAZOR_SECRET'));
    $razorder  = $api->order->create(array('receipt' => $youorder->id, 'amount' =>$razorderamount, 'currency' => env('RAZOR_CURRENCY')));
    $youorder->update(['razorpay_order_id'=>$razorder->id]);
    $pay['order_id']    = $youorder->id;
    $pay['razor_order_id'] = $razorder->id;
    $pay['amount']      = $razorderamount;
    $pay['username']    = $youorder->first_name.' '.$youorder->last_name;
    $pay['useremail']   = $youorder->email;
    $pay['ordername']   = "Visting Card";
    $pay['description'] = "Buy Visting Card";
    $pay['currency']    = env('RAZOR_CURRENCY');
    $pay['address']     = $youorder->address;

    return view('razorpay', compact('pay'));

   }

  public function razorpaySuccess(Request $request){
    //echo"<pre>";print_r($request->all());die;
    if(isset($request->razorpay_payment_id)){
        $order = Order::where('razorpay_order_id',$request->razorpay_order_id)->first();
        $data= [
            'payment_status'=>'2',
            'payment_id'=>$request->razorpay_payment_id,
            'razorpay_signature'=>$request->razorpay_signature,
            ];
        $order->update($data);
        return 'success';
    }else{
        return 'failed';
    }
  }

  public function ThanksYou($id){
    return view('frontend.thankspage');
  }


    public function cardSteps($id)
    {  
      $url=file_get_contents("http://robottxt.tdclients.work/text_frequency/country.json");
      $data['va']=json_decode($url);
       
      $url1=file_get_contents("https://jonasjacek.github.io/colors/data.json");
      $data['color']=json_decode($url1);
      //dd($data['color']);
        $data['socialMedia'] = SocialMedia::all();
        $data['card_id'] =$id;
        if($id=='1'){
            return view('frontend.card_steps', $data);
        }
        
         if($id=='2'){
            return view('frontend.card_steps1', $data);
        }
         if($id=='4'){
            return view('frontend.card_steps4', $data);
        }
        
         if($id=='6'){
            return view('frontend.card_steps6', $data);
        }
        
    }

    public function getSubcategory(Request $request){
       
        $subcategories = SubCategory::where('category_id','=',$request->category_id)->pluck('name','id')->toArray();
        array_unshift($subcategories, "Select");
        return response()->json($subcategories);
    }


  public function dynamicCardSteps($id)
  {  
      $url   = file_get_contents(url('country.json'));
      $va    = json_decode($url);
      $url1  = file_get_contents(url('color.json'));
      $color = json_decode($url1);
      $socialMedia = SocialMedia::all();
      $categories = CardCategories::all()->pluck('name','id')->toArray();
       array_unshift($categories, "Select");
      $card   = CardTemplate::find($id);
      $tempcard = TempCardDetails::where('user_id','=',auth()->id())
      ->where('card_id','=',$id)->first();
      $cardetail = json_decode($tempcard->card_details);
      $cardtempdetail = array();
      if(!empty($card)){
        foreach ($cardetail as $field) {
          $cardtempdetail[$field->name] = $field->value;
        }
      }
      //echo"<pre>";print_r($cardtempdetail);die;
    return view('frontend.dynamic_card_steps',compact('url','va','color','socialMedia','card','cardtempdetail','categories'));
      
  }
    
   public function card(Request $request){
       
      $val=$request->all();
      
       $bah=substr($val['website'],0,4);
          if($bah=='http'){
           
          $val['website1']=$val['website'];
          }else{
          
          $val['website1']='https://'.$val['website'];  
          }
     
       if($request->hasFile('image')){
            $file = $request->file('image');
            $height=237; $width=237;
            $val['img']=$this->image_upload($file,$height,$width);
        }
        
       $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
       
       echo json_encode(array('va'=>$val,'count'=> count($val['socialMediaList'])));
     // return view('frontend.card2',compact('val'));
    }
    
     public function cards1(Request $request){
       
      $val=$request->all();
        
            $count=count($val['social_media']);
         for($i=1; $i <= $count; $i++){
               $bah1=substr($val['social_media'][$i],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$i]=$val['social_media'][$i];
                    } elseif($i==3){
                        $val['social_media1'][$i]='https://wa.me/'.$val['code'].$val['social_media'][$i];
                    }else {
                $val['social_media1'][$i]='https://'.$val['social_media'][$i];  
             }
         }  
         //dd($val['social_media1']);
         
          if($request->hasFile('image1')){
            $file = $request->file('image1');
            $height=418; $width=294;
            $val['img1']=$this->image_upload($file,$height,$width);
            
            
        }else{
             $val['img1']='';
        }
        
         if($request->hasFile('image2')){
            $file = $request->file('image2');
            $height=112; $width=112;
            $val['img2']=$this->image_upload($file,$height,$width);
           
        }else{
            $val['img2']='';
        }
        
        if($request->hasFile('gallery1')){
            $file = $request->file('gallery1');
             $height=62; $width=78;
            $val['gallery1']=$this->image_upload($file,$height,$width);
           
           
        }else{
            $val['gallery1']='';
        }
        
        if($request->hasFile('gallery2')){
             $file = $request->file('gallery2');
              $height=62; $width=78;
             $val['gallery2']=$this->image_upload($file,$height,$width);
           
        }else{
            $val['gallery2']='';
        }
         if($request->hasFile('gallery3')){
             $file = $request->file('gallery3');
              $height=62; $width=78;
             $val['gallery3']=$this->image_upload($file,$height,$width);
            
        }else{
             $val['gallery3']='';
        }
          $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
       
       echo json_encode(array('va'=>$val,'count'=> count($val['socialMediaList'])));
   
    }
    
    
    
    public function cards4(Request $request){
       
      $val=$request->all();
         $count=count($val['social_media']);
         for($i=1; $i <= $count; $i++){
              $bah1=substr($val['social_media'][$i],0,4);
                if($bah1=='http'){
                  $val['social_media1'][$i]=$val['social_media'][$i];
                    } elseif($i==3){
                        $val['social_media1'][$i]='https://wa.me/'.$val['code'].$val['social_media'][$i];
                    }else {
                $val['social_media1'][$i]='https://'.$val['social_media'][$i];  
             }
         }  
         
         $bah=substr($val['website'],0,4);
          if($bah=='http'){
          $val['website2']=explode('://',$val['website']);
          $val['website1']=$val['website'];
        }else{
          $val['website2']=explode('://',$val['website']);
          $val['website1']='https://'.$val['website'];  
        }
      
         
         
          if($request->hasFile('background_image')){
              $file = $request->file('background_image');
              $height=400; $width=350;
              $val['background_image']=$this->image_upload($file,$height,$width); 
              
        }else{
            $val['background_image']='';
        }
        
        if($request->hasFile('profile_image')){
              $file = $request->file('profile_image');
              $height=100; $width=100;
              $val['profile_image']=$this->image_upload($file,$height,$width); 
              
        }else{
            $val['profile_image']='';
        }
        
        
        
      $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
    
         
          echo json_encode(array('va'=>$val,'count'=> count($val['socialMediaList'])));
    }
    
    
    
    
    
    
    
  public function cards6(Request $request){
       
      $val=$request->all();
         $count=count($val['social_media']);
         for($i=1; $i <= $count; $i++){
               $bah1=substr($val['social_media'][$i],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$i]=$val['social_media'][$i];
                    } elseif($i==3){
                        $val['social_media1'][$i]='https://wa.me/'.$val['code'].$val['social_media'][$i];
                    }else {
                $val['social_media1'][$i]='https://'.$val['social_media'][$i];  
             }
         }  
         //dd($val['social_media1']);
         
          if($request->hasFile('logo')){
            $file = $request->file('logo');
            $height=58; $width=187;
            $val['logo1']=$this->image_upload($file,$height,$width);
        
        }else{
             $val['logo1']='';
        }
        
         if($request->hasFile('service-image-1')){
            $file = $request->file('service-image-1');
             $height=56; $width=56;
            $val['serviceimg1']=$this->image_upload($file,$height,$width);
           
        }else{
             $val['serviceimg1']='';
        }
        
        if($request->hasFile('service-image-2')){
            $file = $request->file('service-image-2');
             $height=56; $width=56;
            $val['serviceimg2']=$this->image_upload($file,$height,$width);
        
        }else{
             $val['serviceimg2']='';
        }
        
        if($request->hasFile('service-image-3')){
            $file = $request->file('service-image-3');
            $height=56; $width=56;
            $val['serviceimg3']=$this->image_upload($file,$height,$width);
        
        }else{
             $val['serviceimg3']='';
        }
         if($request->hasFile('service-image-4')){
            $file = $request->file('service-image-4');
             $height=56; $width=56;
            $val['serviceimg4']=$this->image_upload($file,$height,$width);
         
        }else{
             $val['serviceimg4']='';
        }
             $val['servicename1']=$val['service-name-1'];
              $val['servicename2']=$val['service-name-2'];
               $val['servicename3']=$val['service-name-3'];
                $val['servicename4']=$val['service-name-4'];
          $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
           echo json_encode(array('va'=>$val,'count'=> count($val['socialMediaList'])));
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
   //pdf generate function 
    
    
    
    public function generate_pdf(Request $request){
    // dd($request->all());
       $validatedData = $request->validate([
                'primary' =>'required',
                'scondary' =>'required',
                'code' =>'required',
                'text' => 'required',
                'social_media' =>'required',
                'first_name' =>'required|max:30',
                'last_name' =>'required|max:30',
                'email' => 'required|max:100',
                'designation' =>'required|max:50',
                'about' => 'required|max:100',
                'phone' => 'required|max:10|min:10',
                'whats_no' => 'required|max:10|min:10',
                'address' => 'required|max:100',
              //  'website' => 'required|max:30',
                'image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2000000',
   
]);
    // dd($validatedData);   
          $val=$request->all();
          if($request->hasFile('image')){
              $height=237; $width=237;
              $image = $request->file('image');
              $val['img']=$this->image_upload($image,$height,$width);
             }
        
          $bah=substr($val['website'],0,4);
          if($bah=='http'){
           
          $val['website1']=$val['website'];
          }else{
          
          $val['website1']='https://'.$val['website'];  
          }
      
            $count=count($val['social_media']);
         for($i=1; $i <= $count; $i++){
               $bah1=substr($val['social_media'][$i],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$i]=$val['social_media'][$i];
                    } elseif($i==3){
                        $val['social_media1'][$i]='https://wa.me/'.$val['code'].$val['social_media'][$i];
                    }else {
                $val['social_media1'][$i]='https://'.$val['social_media'][$i];  
             }
         }  
         $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
       
         $pdf = \App::make('dompdf.wrapper');
         $customPaper = array(40,-34,330.80,570.80);
         $pdf->loadView('frontend.card',compact('val'))->setPaper($customPaper, 'portrait');
       
        return $pdf->stream($val['first_name']." ".$val['last_name'].".pdf");
//return $pdf->download($val['first_name']." ".$val['last_name'].".pdf");
}
    
     public function generate_pdf1(Request $request){
    // dd($request->all());
       $validatedData = $request->validate([
                'primary' =>'required',
                'scondary' =>'required',
                'code' =>'required',
                'heading' =>'required',
                'text' => 'required',
                'social_media' =>'required',
                'first_name' =>'required|max:30',
                'last_name' =>'required|max:30',
                'designation' =>'required|max:100',
                'about' => 'required|max:145',
                'phone' => 'required|max:10|min:10',
                //'website' => 'required|max:100',
                'image1' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2000000',
                'image2' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2000000',
                'gallery1' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2000000',
                'gallery2' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2000000',
                'gallery3' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2000000',
   
]);
        $val=$request->all();
        $bah=substr($val['website'],0,4);
        if($bah=='http'){
           
          $val['website1']=$val['website'];
        }else{
          
          $val['website1']='https://'.$val['website'];  
        }
      
            $count=count($val['social_media']);
         for($i=1; $i <= $count; $i++){
               $bah1=substr($val['social_media'][$i],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$i]=$val['social_media'][$i];
                    } elseif($i==3){
                        $val['social_media1'][$i]='https://wa.me/'.$val['code'].$val['social_media'][$i];
                    }else {
                $val['social_media1'][$i]='https://'.$val['social_media'][$i];  
             }
         }  
         //dd($val['social_media1']);
         
          if($request->hasFile('image1')){
            $file = $request->file('image1');
             $height=418; $width=294;
              $val['img1']=$this->image_upload($file,$height,$width);
            
            
            
            //$input= rand() .'.'. $file->getClientOriginalExtension();
            // $file->move('card/image',$input);
            // $val['img1']=$input;
        }
        
         if($request->hasFile('image2')){
               $file = $request->file('image2');
               $height=112; $width=112;
               $val['img2']=$this->image_upload($file,$height,$width);
               
        }
        
        if($request->hasFile('gallery1')){
              $file = $request->file('gallery1');
              $height=62; $width=78;
              $val['gallery1']=$this->image_upload($file,$height,$width);
              
            
        }
        
        if($request->hasFile('gallery2')){
              $file = $request->file('gallery2');
              
              $height=62; $width=78;
              $val['gallery2']=$this->image_upload($file,$height,$width);
              
        }
         if($request->hasFile('gallery3')){
              $file = $request->file('gallery3');
              $height=62; $width=78;
              $val['gallery3']=$this->image_upload($file,$height,$width);
              
        }
          $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
          $pdf = \App::make('dompdf.wrapper');
        
           $customPaper = array(45,-50,340.80,625.80);
             // return view('frontend.index2',compact('val'));
          $pdf->loadView('frontend.index2',compact('val'))->setPaper($customPaper, 'portrait');
          return $pdf->stream($val['first_name']." ".$val['last_name'].".pdf");
//return $pdf->download($val['first_name']." ".$val['last_name'].".pdf");
        }
    
     public function generate_pdf4(Request $request){
        
        
          $validatedData = $request->validate([
                'primary' =>'required',
                'scondary' =>'required',
                'text' => 'required',
                'blog_color' => 'required',
                'heading' => 'required',
                'social_media' =>'required',
                'first_name' =>'required|max:30',
                'last_name' =>'required|max:30',
                'company_name' =>'required|max:35',
                'phone' => 'required|max:10|min:10',
                //'whats_no' => 'required|max:10|min:10',
                'profile_image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2000000',
                'background_image' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2000000',
               
   
        ]);
          $val=$request->all();
           $count=count($val['social_media']);
         for($i=1; $i <= $count; $i++){
               $bah1=substr($val['social_media'][$i],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$i]=$val['social_media'][$i];
                    } elseif($i==3){
                        $val['social_media1'][$i]='https://wa.me/'.$val['code'].$val['social_media'][$i];
                    }else {
                $val['social_media1'][$i]='https://'.$val['social_media'][$i];  
             }
         }  
         
         
          $bah=substr($val['website'],0,4);
          if($bah=='http'){
          $val['website2']=explode('://',$val['website']);
          $val['website1']=$val['website'];
        }else{
          $val['website2']=explode('://',$val['website']);
          $val['website1']='https://'.$val['website'];  
        }
      
         
         
         
         if($request->hasFile('background_image')){
              $file = $request->file('background_image');
              $height=400; $width=350;
              $val['background_image']=$this->image_upload($file,$height,$width); 
              
        }
        
        if($request->hasFile('profile_image')){
              $file = $request->file('profile_image');
              $height=100; $width=100;
              $val['profile_image']=$this->image_upload($file,$height,$width); 
              
        }
        
        
        
      $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
      
     //return view('frontend.card4',compact('val'));
       $pdf = \App::make('dompdf.wrapper');
         $customPaper = array(35,-35,333,560.80);
         $pdf->loadView('frontend.card4',compact('val'))->setPaper($customPaper, 'portrait');
          
          return $pdf->stream($val['first_name']." ".$val['last_name'].".pdf");
         return view('frontend.card4');
     }
    
    
    public function generate_pdf6(Request $request){
    
      $validatedData = $request->validate([
                'primary' =>'required',
                'scondary' =>'required',
                'blog_color' =>'required',
                'button_color' =>'required',
                'code' =>'required',
                'text' => 'required',
                'social_media' =>'required',
                'first_name1' =>'required|max:30',
                'last_name1' =>'required|max:30',
                'phone1' => 'required|max:10|min:10',
                'first_name2' =>'required|max:30',
                'last_name2' =>'required|max:30',
                'phone2' => 'required|max:10|min:10',
                'logo' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2000000',
                'service-image-1' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2000000',
                'service-image-2' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2000000',
                'service-image-3' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2000000',
                'service-image-4' => 'required|image|mimes:jpg,png,jpeg,gif,svg|max:2000000',
   
         ]);
        $val=$request->all();
         $count=count($val['social_media']);
         for($i=1; $i <= $count; $i++){
               $bah1=substr($val['social_media'][$i],0,4);
                if($bah1=='http'){
                   $val['social_media1'][$i]=$val['social_media'][$i];
                    } elseif($i==3){
                        $val['social_media1'][$i]='https://wa.me/'.$val['code'].$val['social_media'][$i];
                    }else {
                $val['social_media1'][$i]='https://'.$val['social_media'][$i];  
             }
         }  
         //dd($val['social_media1']);
         
          if($request->hasFile('logo')){
               $file = $request->file('logo');
               $height=58; $width=187;
               $val['logo1']=$this->image_upload($file,$height,$width); 
              
        }
        
         if($request->hasFile('service-image-1')){
                $file = $request->file('service-image-1');
                $height=100; $width=100;
                $val['service-img-1']=$this->image_upload($file,$height,$width);
                
                
        }
        
        if($request->hasFile('service-image-2')){
                $file = $request->file('service-image-2');
                 $height=100; $width=100;
                $val['service-img-2']=$this->image_upload($file,$height,$width);
                
               
        }
        
        if($request->hasFile('service-image-3')){
            $file = $request->file('service-image-3');
                $height=100; $width=100;
                $val['service-img-3']=$this->image_upload($file,$height,$width);
                
        }
         if($request->hasFile('service-image-4')){
            $file = $request->file('service-image-4');
               $height=100; $width=100;
                $val['service-img-4']=$this->image_upload($file,$height,$width);
                
        }
          $val['socialMediaList'] = SocialMedia::pluck('icon', 'id')->toArray();
         // dd($val);
          $pdf = \App::make('dompdf.wrapper');
         // $pdf->setPaper('A4', 'portrait'); 
          //return view('frontend.card5',compact('val'));
          $customPaper = array(35,-35,360.80,730.80);
         $pdf->loadView('frontend.card6',compact('val'))->setPaper($customPaper, 'portrait');
          return $pdf->stream($val['first_name1']." ".$val['last_name1'].".pdf");
//return $pdf->download($val['first_name']." ".$val['last_name'].".pdf");
        }
    
    
    
    
    public function dele_img() {
       $imh= glob('card/image/*');
       foreach($imh as $imj){
        unlink($imj);
        }
        
    $imh1= glob('card/img/*');
       foreach($imh1 as $imj1){
        unlink($imj1);
        }
        
        
       echo "all delete..!";
       }
  public function image_upload($file,$height,$width){
      
                  $input = rand(10,10000).time().'.'.$file->getClientOriginalExtension();
                  $destinationPath = public_path('card/image');
                  $img = Image::make($file->getRealPath());
                  $img->resize($width,$height, function ($constraint) {
		          $constraint->aspectRatio();
		          })->save($destinationPath.'/'.$input);
                  return $input;
  }
  
  public function pdf() {
     
      
        $pdf = \App::make('dompdf.wrapper');
        //$pdf->setPaper('A4', 'portrait'); 
           //$customPaper = array(40,-34,630.80,800.80);
              $customPaper = array(35,-35,335,560.80);
         $pdf->loadView('frontend.test')->setPaper($customPaper, 'portrait');
      return $pdf->stream('invoice.pdf');
    return view('frontend.test');
  }

  public function cardimage_upload($file,$path)
  {
    if(!\File::exists($path)) {
      \File::makeDirectory($path, 0777, true, true);
    }
    $input = rand(10,10000).time().'.'.$file->getClientOriginalExtension();
    $destinationPath = public_path($path);
    $img = Image::make($file->getRealPath());
    $img->save($destinationPath.'/'.$input);
    return $path.'/'.$input;
  }
}
