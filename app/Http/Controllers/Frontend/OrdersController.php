<?php

namespace App\Http\Controllers\Frontend;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\SocialMedia;
use App\Models\Order;
use App\Models\CardTemplate;
use App\Models\TempCardDetails;
use App\Models\CardCategories;
use App\Models\SubCategory;
use App\Models\Orderfield;
use App\Models\OrderCardDetails;
use App\Models\Contactus;
use App\Models\Pages;
use App\Models\PageContent;
use App\Models\UsersCard;
use Illuminate\Support\Facades\Input;
use JeroenDesloovere\VCard\VCard;
use Razorpay\Api\Api;
use Image;
use Auth;
use DB;
class OrdersController extends Controller
{
  public function Checkout($id){
    $user = auth()->user();
    $order = Order::leftJoin('card_templates', 'card_templates.id',  '=', 'orders.card_template_id')
    ->leftJoin('card_themes', 'card_themes.id',  '=', 'orders.card_theme_id')
    ->select('orders.*','card_templates.picture','card_templates.cardtitle','card_templates.carddescription','card_templates.cardtype','card_templates.actualprice','card_templates.discounttype','card_templates.discount','card_templates.price AS price','card_themes.image AS themeimage','card_themes.name AS themename')
      ->where('orders.user_id', '=', $user->id)
      ->where('orders.id', '=', $id)->first();
    Order::where('id',$id)->update(['price'=>$order->price]);
    
    return view('frontend.checkout',compact('order'));
  }

  public function MyCart(){
    $user = auth()->user();
    // $orders = Order::where('user_id','=',$user->id)->get();
    $orders = Order::leftJoin('card_templates', 'card_templates.id',  '=', 'orders.card_template_id')
    ->leftJoin('card_themes', 'card_themes.id',  '=', 'orders.card_theme_id')
    ->select('orders.*','card_templates.picture','card_templates.cardtitle','card_templates.carddescription','card_templates.cardtype','card_themes.image AS themeimage','card_themes.name AS themename')
      ->where('orders.user_id', '=', $user->id)
      ->where('orders.payment_status', '=', '1')->orderBy('orders.id', 'DESC')->get();
    //echo"<pre>";print_r($orders);die;
    $page ="My Cart";
    $empty ="Your Cart Is Empty";
    return view('frontend.yourorder',compact('orders','page','empty'));
   }

   public function MyOrders(){
    $user = auth()->user();
    // $orders = Order::where('user_id','=',$user->id)->get();
    $orders = Order::leftJoin('card_templates', 'card_templates.id',  '=', 'orders.card_template_id')
    ->leftJoin('card_themes', 'card_themes.id',  '=', 'orders.card_theme_id')
    ->select('orders.*','card_templates.picture','card_templates.cardtitle','card_templates.carddescription','card_templates.cardtype','card_themes.image AS themeimage','card_themes.name AS themename')
      ->where('orders.user_id', '=', $user->id)
      ->where('orders.payment_status', '=', '2')->orderBy('orders.id', 'DESC')->get();
    //echo"<pre>";print_r($orders);die;
    $page ="Orders";
    $empty ="Your Orders Is Empty";
    return view('frontend.yourorder',compact('orders','page','empty'));
   }


   public function MyCards(Request $request){
    $user = auth()->user();
    $orders = UsersCard::leftJoin('card_templates', 'card_templates.id',  '=', 'users_cards.card_template_id')->leftJoin('card_themes', 'card_themes.id',  '=', 'users_cards.card_theme_id')
    ->leftJoin('orders', 'orders.id',  '=', 'users_cards.order_id')
    ->select('orders.*','card_templates.picture','card_templates.cardtitle','card_templates.carddescription','card_templates.cardtype','card_themes.image AS themeimage','card_themes.name AS themename','users_cards.cardimage','users_cards.id AS cardid')
    ->where('users_cards.user_id', '=', $user->id)->orderBy('users_cards.id', 'DESC')->get();
    //echo"<pre>";print_r($orders);die;
    $page ="My Cards";
    $empty ="You Have No Cards";
    return view('frontend.mycards',compact('orders','page','empty'));
   }

    public function Mycard($id){
	    $user = auth()->user();
      $order = UsersCard::leftJoin('card_templates', 'card_templates.id',  '=', 'users_cards.card_template_id')->leftJoin('card_themes', 'card_themes.id',  '=', 'users_cards.card_theme_id')
      ->leftJoin('orders', 'orders.id',  '=', 'users_cards.order_id')
      ->select('orders.*','card_templates.picture','card_templates.cardtitle','card_templates.carddescription','card_templates.cardtype','card_themes.image AS themeimage','card_themes.name AS themename','users_cards.cardimage','users_cards.id AS cardid')
      ->where('users_cards.user_id', '=', $user->id)
      ->where('users_cards.id', '=', $id)->first();
      //echo"<pre>";print_r($order);die;
	    $page ="YOUR CARDS";
	    $empty ="You Have No Cards";
	    return view('frontend.mycard',compact('order'));
   }
}
