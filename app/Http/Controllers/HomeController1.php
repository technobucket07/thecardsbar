<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Models\CardReviews;
use App\Models\CardLikes;
use Illuminate\Support\Facades\Hash;
use Auth;
use Validator;
require base_path() . '/vendor/autoload.php';
class HomeController1 extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function UserAccount()
    {   $user =auth()->user();
        return view('frontend.account',compact('user'));
    }

    public function UpdateAccount(User $user, Request $request)
    {   //echo "<pre>";print_r($request->all());die;
        $user =auth()->user();
        $rules = array(
            'name' => 'required',
            'about' => 'required',
            'gender' => 'required',
            'email' => 'required|email|unique:users,email,'.$user->id,
            'phone' => 'required|unique:users,phone,'.$user->id,
        );
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
           return back()->withErrors($validator);
        }else{
            $data = $request->except(['_token']);
            if($request->hasFile('profile')){
                $filepath = 'users/'.endeCrypt($user->id).'/profile';
                $file = $request->file('profile');
                $data['profile']=uploadImage($file,$filepath);
            }
            
            $user->fill($data);
            $user->save();
            //Flash::message('Your account has been updated!');
            return back();
        }
    }


     public function changePassword(Request $request)
     {
        //echo"<pre>";print_r($request->all());die;
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }

        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);

        //Change Password
        $user = Auth::user();
        $user->password = Hash::make($request->get('new-password'));
        //$user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("success","Password changed successfully !");

    }

    public function CardReviews(Request $request)
    {
        $user =auth()->user();
        if($user){
            $data['user_id']=$user->id;
            $data['card_template_id']=$request->card_id;
            if($request->action =='like'){
                $like['likes']= $request->like;
                CardLikes::updateOrCreate($data,$like);
                return 1;
            }
        }else{
            return 0;
        }
    }


    public function getAnalyticsSummary(Request $request){
        $from_date = date("Y-m-d", strtotime($request->get('from_date',"7 days ago")));
        $to_date = date("Y-m-d",strtotime($request->get('to_date',$request->get('from_date','today')))) ;
        $gAData = $this->gASummary($from_date,$to_date) ;
        return $gAData;
    }

    //to get the summary of google analytics.
    private function gASummary($date_from,$date_to) {
        $service_account_email = 'technodeviser04@gmail.com';
        // Create and configure a new client object.
        $client = new \Google_Client();
        $client->setApplicationName("Thecardbar");
        $client->setDeveloperKey("AIzaSyD1rChVYBNqXBE0baQvh7GrBPV6XPpw6PA");
        $analytics = new \Google_Service_Analytics($client);
        $cred = new \Google_Auth_AssertionCredentials(
        $service_account_email,
        array(\Google_Service_Analytics::ANALYTICS_READONLY),
        "AIzaSyD1rChVYBNqXBE0baQvh7GrBPV6XPpw6PA"
        );
        $client->setAssertionCredentials($cred);
        if($client->getAuth()->isAccessTokenExpired()) {
            $client->getAuth()->refreshTokenWithAssertion($cred);
        }
        $optParams = [
            'dimensions' => 'ga:date',
            'sort'=>'-ga:date'
        ] ;
        $results = $analytics->data_ga->get(
            'ga:{View ID}',
            $date_from,
            $date_to,
            'ga:sessions,ga:users,ga:pageviews,ga:bounceRate,ga:hits,ga:avgSessionDuration',
            $optParams
        );
        $rows = $results->getRows();
        $rows_re_align = [] ;
        foreach($rows as $key=>$row) {
            foreach($row as $k=>$d) {
                $rows_re_align[$k][$key] = $d;
            }
        }
        $optParams = array(
            'dimensions' => 'rt:medium'
        );
        try {
        $results1 = $analytics->data_realtime->get(
            'ga:{View ID}',
            'rt:activeUsers',
        $optParams);
        // Success.
        } catch (apiServiceException $e) {
        // Handle API service exceptions.
        $error = $e->getMessage();
        }
        $active_users = $results1->totalsForAllResults;
        return [
            'data'=> $rows_re_align ,
            'summary'=>$results->getTotalsForAllResults(),
            'active_users'=>$active_users['rt:activeUsers']
        ] ;
    }

}
