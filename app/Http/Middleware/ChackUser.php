<?php

namespace App\Http\Middleware;

use Closure;

class ChackUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = auth()->user();
        if($user){
            return $next($request);
        } 
        return redirect('home')->with('Notlogin',"Please Login To Continue");
    }
}
