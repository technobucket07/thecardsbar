<?php

function jsonResponse($success, $code, $reply, $extra = [])
{
    $response = [
        'status' => $code,
        'success' => $success,
        'message' => '',
        'errors' => [],
        'result' => [],
        'result_obj' => new ArrayObject(),
        'extra' => $extra ? $extra : new ArrayObject(),
    ];

    if ($code == 200) {
        $response['result_obj'] = $reply;
    } elseif ($code == 404) {
        $response['result'] = $reply;
    } elseif ($code == 406) {
        $response['errors'] = apiErrors($reply);
    } else {
        $response['message'] = $reply;
    }
    return response()->json($response);
}

function fileUploader($file, $id, $oldImage = null, $path = null)
{
    $folder = ($path != "") ? $path : ROOT . env('UPLOAD_PATH');
    $fileName = $_FILES[$file]['name'];
    $newFile = str_replace(' ', '_', str_random(6) . " " . $fileName);
    $fileTempName = $_FILES[$file]['tmp_name'];

    if ($oldImage != "" && file_exists($folder . $oldImage)) {
        unlink($folder . $oldImage);
    }

    if(move_uploaded_file($fileTempName, $folder . $id  . '__'. string_manip($newFile))) {
        return $id  . '__'. string_manip($newFile);
    }
}

function generateUrl($name){
    $url = str_replace(' ', '-', $name);
    return $url;
}

function string_manip($string = null, $type = 'L')
{
    switch ($type) {
        case 'U':
            return strtoupper($string);
            break;
        case 'UC':
            return ucfirst(strtolower($string));
            break;
        case 'UCW':
            return ucwords(strtolower($string));
            break;
        default:
            return strtolower($string);
            break;
    }
}

function statusRadio($heading = false)
{
    $types = [
        1 => 'Active',
        0 => 'Inactive',
    ];

    if($heading) {
        $types = ['' => 'All'] + $types;
    }
    return $types;
}

function radio($name = '', $inputs = [], $selected = null, $class = '')
{
    $html = '';
    if(isset($inputs) && count($inputs) > 0) {
        foreach($inputs as $value => $label) {
            $checked = ($value === $selected) ? "checked='checked'" : '';
            $html .= "<label><input type='radio' value='$value' name='$name' class='$class' $checked /> $label</label>";
        }
    }
    return $html;
}

function renderImage($image, $width = 150, $url = false, $path=null)
{
    if($path == null){
        $folder = ROOT . env('UPLOAD_PATH');
    }else{
        $folder = $path;
    }

    if (file_exists($folder . '/' . $image) && $image != "")
    {
        $file = asset($folder) . '/' . $image;
        if ($url) {
            return $file;
        } else {
            return \Html::image($file, 'No Image', ['width' => $width, 'class' => 'img-thumbnail']);
        }
    } else {
        $file = asset('admin/images/dummy.png');
        if ($url) {
            return $file;
        } else {
            return \Html::image(asset('admin/images/dummy.png'), 'No Image', ['width' => $width, 'class' => 'img-thumbnail']);
        }
    }
}

function webResponse($success, $code, $reply, $extra = []){
    $response = [
        'status' => $code,
        'success' => $success,
        'message' => '',
        'errors' => [],
        'result' => [],
        'extra' => $extra ? $extra : new ArrayObject(),
    ];
    if ($code == 201) {
        $response['result'] = $reply;
    } elseif ($code == 406 ||  $code == 206 ) {
        $response['errors'] = webErrors($reply);
    } else {
        $response['message'] = $reply;
    }
    return response()->json($response);
}
function webErrors($errors = [])
{
    $error = [];
    if(!is_array($errors)){
        $errors = $errors->toArray();
    }
    foreach ($errors as $key => $value)
    {
        $error[$key] = $value[0];
    }
    return $error;
}

function statusSlider($route, $id, $check) {
    $checked = ($check) ? 'checked="checked"' : '';
    return '<label class="switch">'.
    '<input type="checkbox" class="__status" '.$checked.' data-route="'.route($route, $id).'">'.
    '<span class="slider round"></span>'.
    '</label>';
}


    function colorTypes($selected='') {
        $cotypes = array(
            'Primary'=>'Primary Color',
            'Secondary'=>'Secondary Color',
            'Blog'=>'Blog Color',
            'Text'=>'Text Color',
            'Heading'=>'Heading Color'
        );
        return $cotypes;
    }

    function uploadImage($image, $folder) {
        //$random_number = mt_rand(100000, 999999);
        Storage::disk('local')->put('public/'.$folder, $image);
        $name = $folder.'/'.$image->hashName();
        return $name;
    }

    function generateQr($data,$path) {
        $image = \QrCode::format('png')
        ->merge(url('/').'/web_assets/img/new_logo.png', 0.5, true)
        ->size(200)->errorCorrection('H')
        ->generate($data);
        Storage::disk('local')->put('public/'.$path, $image);
    }

    function customerCount(){
        return App\User::where('user_role','!=',1)->count();
    }

    function customerOrderTheme($themeid,$cardid){
        if($themeid !='0'){
            $theme = App\Models\CardTheme::find($themeid);
            $img = $theme->image;
        }else{
            $card = App\Models\CardTemplate::find($cardid);
            $img = $card->picture;
        }
        
        return $img;
    }

    function inputfield(){
       return array(
                ''=>'--Select--',
                'button'=>'button',
                'select'=>'select',
                'checkbox'=>'checkbox',
                'color'=>'color',
                'date'=>'date',
                'datetime-local'=>'datetime-local',
                'email'=>'email',
                'file'=>'file',
                'hidden'=>'hidden',
                'image'=>'image',
                'month'=>'month',
                'number'=>'number',
                'password'=>'password',
                'radio'=>'radio',
                'range'=>'range',
                'reset'=>'reset',
                'search'=>'search',
                'submit'=>'submit',
                'tel'=>'tel',
                'text'=>'text',
                'textarea'=>'textarea',
                'time'=>'time',
                'url'=>'url',
                'week'=>'week',
                );
    }

    function generateVcf(){
        $vcard = new \VCard();

        // define variables
        $lastname = 'Desloovere';
        $firstname = 'Jeroen';
        $additional = '';
        $prefix = '';
        $suffix = '';
        $vcard->addName($lastname, $firstname, $additional, $prefix, $suffix);
        $vcard->addCompany('Siesqo');
        $vcard->addJobtitle('Web Developer');
        $vcard->addRole('Data Protection Officer');
        $vcard->addEmail('info@jeroendesloovere.be');
        $vcard->addPhoneNumber(1234121212, 'PREF;WORK');
        $vcard->addPhoneNumber(123456789, 'WORK');
        $vcard->addAddress(null, null, 'street', 'worktown', null, 'workpostcode', 'Belgium');
        $vcard->addLabel('street, worktown, workpostcode Belgium');
        $vcard->addURL('http://www.jeroendesloovere.be');
        //$vcard->addPhoto(__DIR__ . '/landscape.jpeg');
        return $vcard->download();
    }

    function endeCrypt($string, $action = 'encrypt')
    {
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'AA74CDCC2BBRT935136HH7B63C27'; // user define private key
        $secret_iv = '5fgf5HJ5g27'; // user define secret key
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16); // sha256 is hash_hmac_algo
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        return $output;
    }

    function countUserCardLikes($cardid){
       return App\Models\UserCardLikes::where('user_card_id','=',$cardid)->sum('likes');
    }

    function countUserCardViews($cardid){
       return App\Models\UserCardViews::where('user_card_id','=',$cardid)->sum('views');
    }

    function Isuserlike($cardid){
        $liked =  App\Models\UserCardLikes::where('user_card_id','=',$cardid)
            ->where('user_id', '=',auth()->id())->first();
            
        if($liked && $liked->likes){
            return $liked->user_id;
        }else{
            return 0;
        }
    }