var pageContainer = $('#pagination');
var url = pageContainer.data('url');
var token = $('meta[name="_token"]').attr('content');
var keyword = '';
var ajaxReq = null;
var formData = '';

$('body').on('keyup', 'input[name=keyword]', function() {
    if (ajaxReq != null) ajaxReq.abort();
    keyword = $(this).val();
    pagination();
});

$('body').on('change', '.__status', function(e) {
    e.preventDefault();
    var option = { _token: token, _method: 'post' };
    var route = $(this).attr('data-route');

    $.ajax({
        type: 'post',
        url: route,
        data: option,
        success: function(data) {
            alert(data.message);
        },
        error: function(data) {
            alert('An error occurred.');
        }
    });
});

$('#form-search').submit(function(e) {
    e.preventDefault();
    $('#form-search').find(".error").remove();
    formData = $(this).serialize();
    url = pageContainer.data('url');
    window.history.pushState('', '', url);
    pagination();
});

$('#ajax-submit').submit(function(e) {
    e.preventDefault();
    try {
        for (instance in CKEDITOR.instances) {
            CKEDITOR.instances[instance].updateElement();
        }
    } catch (e) {
        // print error
    }

    $('#ajax-submit').find(".error").remove();
    $('#ajax-submit').find(".border-red").removeClass('border-red');
    var form = $('#ajax-submit');

    $.ajax({
        type: $(this).attr('method'),
        url: $(this).attr('action'),
        data: new FormData(this),
        processData: false,
        contentType: false,
        beforeSend: function() {
            showLoader();
        },
        success: function(data) {
            if (data.success)
            {
                $(this).find("button[type='submit']").prop('disabled', true);
                hideLoader();

                if (data.message != '') {
                    $('.__modal_message').html(data.message);
                    $('.__modal').modal('show');
                }

                if (data.extra.reload) {
                    $('.__modal').on('hidden.bs.modal', function(e) {
                        window.location.reload();
                    });
                }

                if (data.extra.redirect)
                {
                    if (data.message != '') {
                        $('.__modal').on('hidden.bs.modal', function(e) {
                            window.location.href = data.extra.redirect;
                        });
                    } else {
                        window.location.href = data.extra.redirect;
                    }
                }
            } else {
                if (data.status == 206) {
                    hideLoader();
                    $.each(data.message, function(i, v) {
                        var error = '<div class="error">' + v + '</div>';
                        var split = i.split('.');
                        if (split[2]) {
                            var ind = split[0] + '[' + split[1] + ']' + '[' + split[2] + ']';
                            form.find("[name='" + ind + "']").addClass('border-red');
                            form.find("[name='" + ind + "']").parent().append(error);
                        } else if (split[1]) {
                            var ind = split[0] + '[' + split[1] + ']';
                            form.find("[name='" + ind + "']").addClass('border-red');
                            form.find("[name='" + ind + "']").parent().append(error);
                        } else {
                            form.find("[name='" + i + "']").addClass('border-red');
                            form.find("[name='" + i + "']").parent().append(error);
                        }
                    });
                    // $('.__modal_message').html('Some fields are required.');
                    // $('.__modal').modal('show');
                } else if (data.status == 207) {
                    hideLoader();

                    if (data.message != '') {
                        $('.__modal_message').html(data.message);
                        $('.__modal').modal('show');
                    }

                    if (data.extra.reload) {
                        $('.__modal').on('hidden.bs.modal', function(e) {
                            window.location.reload();
                        });
                    }

                    if (data.extra.redirect) {
                        if (data.message != '') {
                            $('.__modal').on('hidden.bs.modal', function(e) {
                                window.location.href = data.extra.redirect;
                            });
                        } else {
                            window.location.href = data.extra.redirect;
                        }
                    }
                }
            }
        },
        error: function(data) {
            console.log('An error occurred.');
        }
    });
});


function showLoader() {
    $('.loading').show();
}

function hideLoader() {
    $('.loading').hide();
}

function pagination()
{
    if (formData != '') {
        formData = formData + '&';
    }

    var option = formData +
        '&keyword=' + keyword +
        '&_token=' + token;

    ajaxReq = $.ajax({
        type: 'GET',
        url: url,
        data: option,
        beforeSend: function() {
            showLoader();
        },
        success: function(data) {
            ajaxReq = null;
            if ((data.success == false) && (data.status == 206)) {
                $.each(data.message, function(i, v) {
                    var error = '<div class="error">' + v + '</div>';
                    $('#form-search').find("[name='" + i + "']").parent().append(error);
                });
            } else {
                pageContainer.html(data);
            }
            hideLoader();
        },
        error: function(data) {
            console.log('An error occurred.');
        }
    });
}

$('body').on('change', '.__check_all', function() {
    if ($(this).is(':checked')) {
        $('.__check').prop('checked', true);
        $('.__check').parent().parent().addClass('bg-warning text-white');
    } else {
        $('.__check').prop('checked', false);
        $('.__check').parent().parent().removeClass('bg-warning text-white');
    }
});

$('body').on('click', '.__toggle_all', function() {
    if ($('.__check:checked').length <= 0) {
        alert('Please select alteast one status');
        return false;
    }

    var ids = [];
    $(".__check:checked").each(function() {
        ids.push($(this).val());
    });

    var option = {
        _token: token,
        _method: 'post',
        ids: ids
    };
    var route = $(this).attr('data-route');
    showLoader();

    $.ajax({
        type: 'post',
        url: route,
        data: option,
        success: function(data) {
            if (data.success && data.status == 201) {
                showLoader();
                alert(data.message);
                window.location.reload();
            }
        },
        error: function(data) {
            console.log('An error occurred.');
        }
    });
});

$('body').on('click', '.__drop', function(e) {
    e.preventDefault();
    var conf = confirm('Are you sure?');
    if (conf) {
        var option = { _token: token, _method: 'delete' };
        var route = $(this).attr('data-url');
        showLoader();
        $.ajax({
            type: 'post',
            url: route,
            data: option,
            success: function(data) {
                if (data.success && data.status == 201) {
                    showLoader();
                    if (data.message != '')
                    {
                        hideLoader();
                        $('.__modal_message').html(data.message);
                        $('.__modal').modal('show');
                        $('.__modal').on('hidden.bs.modal', function(e) {
                            window.location.reload();
                        });
                    }
                }
            },
            error: function(data) {
                console.log('An error occurred.');
            }
        });
    }
}).on('click', '._back', function(event) {
    history.back(1);
});
