

function demoUpload1() {
  $('.upload-demo-wrap1').hide();
    var $uploadCrop;

    function readFile(input) {
      
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            $('.upload-demo-wrap1').show();
            reader.onload = function (e) {
                $('#upload1').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    //console.log('jQuery bind complete');
                });

            }

            reader.readAsDataURL(input.files[0]);
        }
        else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $uploadCrop = $('#upload-demo1').croppie({
       viewport: {
        width: 120,
        height: 120,
        type: 'circle'
    },
    boundary: {
        width: 250,
        height: 200
    },
        enforceBoundary:true,
        enableResize:false,
        enableZoom:true,
        showZoomer:true,
        enableExif: true,
       mouseWheelZoom: 'ctrl'
    });

    $('#upload1').on('change', function () {
       
     
        readFile(this);
    });

   
    $('.dropify,.cr-boundary,.finish-btn,.cr-slider,.uploadBackground').on('mouseover', function (ev) {
      
       if($('#upload1').val() != ''){
           $uploadCrop.croppie('result', {
               type: 'canvas',
               size: 'viewport'
           }).then(function (resp) {
              
               /*popupResult({
                src: resp
                });*/
               $('#base64image1').val(resp);
           });
       }

    });

}







