

function demoUpload1() {
  $('.upload-demo-wrap1').hide();
    var $uploadCrop;

    function readFile(input) {
      
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            $('.upload-demo-wrap1').show();
            reader.onload = function (e) {
                $('#upload1').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    //console.log('jQuery bind complete');
                });

            }

            reader.readAsDataURL(input.files[0]);
        }
        else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $uploadCrop = $('#upload-demo1').croppie({
       viewport: {
        width: 156,
        height: 283,
        type: 'square'
    },
    boundary: {
        width: 250,
        height: 300
    },
        enforceBoundary:true,
        enableResize:false,
        enableZoom:true,
        showZoomer:true,
        enableExif: true,
       mouseWheelZoom: 'ctrl'
    });

    $('#upload1').on('change', function () {
       
     
        readFile(this);
    });

    //$('#cropped-image-btn').on('click', function (ev) {
    $('form').on('mouseover',function (ev) {
      
       if($('#upload1').val() != ''){
           $uploadCrop.croppie('result', {
               type: 'canvas',
               size: 'viewport'
           }).then(function (resp) {
              
               /*popupResult({
                src: resp
                });*/
               $('#base64image1').val(resp);
           });
       }

    });

}







function demoUpload2() {
   $('.upload-demo-wrap2').hide();
        var $uploadCrop;
        var height=$('.basic-height2').val();
        var width=$('.basic-width2').val();
      

    function readFile(input) {
        
        
        
        $('.upload-demo-wrap2').show();
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#upload2').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result,
                   
                }).then(function(){
                    //console.log('jQuery bind complete');
                });

            }

            reader.readAsDataURL(input.files[0]);
            
            
        
   
        }
        else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }
    
   
    
    $uploadCrop = $('#upload-demo2').croppie({
        viewport: {
            width: 156,
            height: 132,
            type: 'square'
        },
      boundary: {
        width: 250,
        height: 220
      },
        enforceBoundary:true,
        enableResize:false,
        enableZoom:true,
        showZoomer:true,
        enableExif: true,
        mouseWheelZoom: true
    });
    
    
    
    
    $('#upload2').on('change', function () {
     
        readFile(this);
       
    });
   
   
    
   
    

    

  
     $('form').on('mouseover',function (ev) {
   
       if($('#upload2').val() != ''){
           $uploadCrop.croppie('result', {
               type: 'canvas',
               size: 'viewport'
           }).then(function (resp) {
               /*popupResult({
                src: resp
                });*/
               
               $('#base64image2').val(resp);
           });
       }

    });
    
    

}



function demoUpload3() {
   $('.upload-demo-wrap3').hide();
    var $uploadCrop;

    function readFile(input) {
        $('.upload-demo-wrap3').show();
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#upload3').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    //console.log('jQuery bind complete');
                });

            }

            reader.readAsDataURL(input.files[0]);
        }
        else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

     $uploadCrop = $('#upload-demo3').croppie({
        viewport: {
            width: 156,
            height: 132,
            type: 'square'
        },
         boundary: {
        width: 250,
        height: 220
      },
        enforceBoundary:true,
       enableResize:false,
        enableZoom:true,
        showZoomer:true,
        enableExif: true
    });

    $('#upload3').on('change', function () {
       
       
        readFile(this);
    });

    //$('#cropped-image-btn').on('click', function (ev) {
    $('form').on('mouseover',function (ev) {
      
       if($('#upload3').val() != ''){
           $uploadCrop.croppie('result', {
               type: 'canvas',
               size: 'viewport'
           }).then(function (resp) {
               /*popupResult({
                src: resp
                });*/
               
               $('#base64image3').val(resp);
           });
       }

    });

}



function demoUpload4() {
   $('.upload-demo-wrap4').hide();
    var $uploadCrop;

    function readFile(input) {
       $('.upload-demo-wrap4').show();
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#upload4').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    //console.log('jQuery bind complete');
                });

            }

            reader.readAsDataURL(input.files[0]);
        }
        else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $uploadCrop = $('#upload-demo4').croppie({
        viewport: {
            width: 200,
            height: 200,
            type: 'square'
        },
        
    boundary: {
        width: 250,
        height: 220
    },
        enforceBoundary:true,
        enableResize:false,
        enableZoom:true,
        showZoomer:true,
        enableExif: true
    });

    $('#upload4').on('change',function () {
       
       
        readFile(this);
    });

    //$('#cropped-image-btn').on('click', function (ev) {
    $('form').on('mouseover', function (ev) {
      
       if($('#upload4').val() != ''){
           $uploadCrop.croppie('result', {
               type: 'canvas',
               size: 'viewport'
           }).then(function (resp) {
               /*popupResult({
                src: resp
                });*/
                
               $('#base64image4').val(resp);
           });
       }

    });

}






function demoUpload5() {
   $('.upload-demo-wrap5').hide();
    var $uploadCrop;

    function readFile(input) {
      $('.upload-demo-wrap5').show();
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#upload5').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    //console.log('jQuery bind complete');
                });

            }

            reader.readAsDataURL(input.files[0]);
        }
        else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

     $uploadCrop = $('#upload-demo5').croppie({
        viewport: {
            width: 200,
            height: 200,
            type: 'square'
        },
        
    boundary: {
        width: 250,
        height: 220
    },
        enforceBoundary:true,
        enableResize:false,
        enableZoom:true,
        showZoomer:true,
        enableExif: true
    });

    $('#upload5').on('change', function () {
       
       
        readFile(this);
    });

    //$('#cropped-image-btn').on('click', function (ev) {
     $('form').on('mouseover',function (ev) {
       
       if($('#upload5').val() != ''){
           $uploadCrop.croppie('result', {
               type: 'canvas',
               size: 'viewport'
           }).then(function (resp) {
               /*popupResult({
                src: resp
                });*/
                 
               $('#base64image5').val(resp);
           });
       }

    });

}






function demoUpload6() {
   $('.upload-demo-wrap6').hide();
    var $uploadCrop;

    function readFile(input) {
      $('.upload-demo-wrap6').show();
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#upload6').addClass('ready');
                $uploadCrop.croppie('bind', {
                    url: e.target.result
                }).then(function(){
                    //console.log('jQuery bind complete');
                });

            }

            reader.readAsDataURL(input.files[0]);
        }
        else {
            swal("Sorry - you're browser doesn't support the FileReader API");
        }
    }

    $uploadCrop = $('#upload-demo5').croppie({
        viewport: {
            width: 200,
            height: 200,
            type: 'square'
        },
        
    boundary: {
        width: 250,
        height: 200
    },
        enforceBoundary:true,
        enableResize:false,
        enableZoom:true,
        showZoomer:true,
        enableExif: true
    });

    $('#upload6').on('change', function () {
       
       
        readFile(this);
    });

    //$('#cropped-image-btn').on('click', function (ev) {
    $('form').on('mouseover',function (ev) {
      
       if($('#upload6').val() != ''){
           $uploadCrop.croppie('result', {
               type: 'canvas',
               size: 'viewport'
           }).then(function (resp) {
               /*popupResult({
                src: resp
                });*/
                 
               $('#base64image6').val(resp);
           });
       }

    });

}
