<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardfieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cardfields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fieldtype')->nullable();
            $table->string('name');
            $table->string('isrequired')->default(0);
            $table->string('label');
            $table->string('defaultvalue')->nullable();
            $table->string('withimages')->default(0);
            $table->string('max_limit')->default(0);
            $table->string('min_limit')->default(0);
            $table->string('sub_field')->default(0);
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cardfields');
    }
}
