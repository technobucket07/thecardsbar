<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersCardsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users_cards', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('card_template_id');
            $table->string('card_theme_id');
            $table->string('order_id');
            // $table->string('firstname');
            // $table->string('lastname');
            // $table->string('primaryemail')->nullable();
            // $table->string('secondaryemail')->nullable();
            // $table->string('primaryphone')->nullable();
            // $table->string('secondaryphone')->nullable();
            // $table->string('whatsappnumber')->nullable();
            // $table->string('company')->nullable();
            // $table->string('designation')->nullable();
            // $table->string('address')->nullable();
            // $table->string('website')->nullable();
            $table->string('cardpdf')->nullable();
            $table->string('cardimage')->nullable();
            $table->string('cardqr')->nullable();
            $table->string('vcard')->nullable();
            $table->string('view')->default(0);
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users_cards');
    }
}
