<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderfieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orderfields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fieldname');
            $table->string('order_id');
            $table->string('user_id');
            $table->string('fieldvalue')->nullable();
            $table->string('subfieldname')->nullable();
            $table->string('field')->nullable();
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orderfields');
    }
}
