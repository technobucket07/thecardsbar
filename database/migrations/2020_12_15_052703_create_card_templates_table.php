<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('picture');
            $table->string('price');
            $table->string('card_id');
            $table->string('category_id');
            $table->string('sub_category_id')->nullable();
            $table->string('cardtitle');
            $table->string('cardtype');
            //$table->string('colortype');
            $table->string('carddescription');
            $table->string('actualprice');
            $table->enum('discounttype',['1','2'])->nullable()->comment('1=Percentage,2 Flat off');
            $table->string('discount');
            $table->string('maxsocialmedia');
            $table->string('minsocialmedia');
            $table->string('profile')->default(0);
            $table->string('logo')->default(0);
            $table->string('background')->default(0);
            $table->string('gallery')->default(0);
            $table->string('max_no_gallery')->nullable();
            $table->string('min_no_gallery')->nullable();
            $table->string('services')->default(0);
            $table->string('max_no_services')->nullable();
            $table->string('min_no_services')->nullable();
            $table->string('serviceswithimg')->default(0);
            $table->string('servicedescription')->default(0);
            $table->string('username');
            $table->string('primaryphone');
            $table->string('username2')->nullable();
            $table->string('secondaryphone')->default(0);
            $table->string('primaryemail');
            $table->string('secondaryemail')->default(0);
            $table->string('address');
            $table->string('location')->default(0);
            $table->string('website')->default(0);
            $table->string('designation')->default(0);
            $table->string('description')->default(0);
            $table->string('min_no_description')->nullable();
            $table->string('company')->default(0);
            $table->string('companyabout')->default(0);
            $table->string('calltagline')->default(0);
            $table->string('openclosetime')->default(0);
            $table->string('bottomtitle')->default(0);
            $table->string('is_enabled')->default(0);
            $table->string('crosel_image')->nullable();
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_templates');
    }
}
