<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('razorpay_order_id')->nullable();
            $table->string('razorpay_signature')->nullable();
            $table->string('user_id');
            $table->float('price');
            $table->string('card_template_id');
            $table->string('card_theme_id');
            $table->string('payment_id')->nullable();
            $table->string('coupon')->nullable();
            $table->enum('payment_method',['1'])->nullable()->comment('1=razorpay');
            $table->enum('payment_status',['1','2'])->default('1')->comment('1=unpaid');
            $table->enum('status',['0','1','2'])->default('1')->comment('0 =cancel , 1=process, 2=completed');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->string('address')->nullable();
            // $table->string('cardqr')->nullable();
            // $table->string('vcard')->nullable();
            // $table->text('cardimage')->nullable();
            //$table->text('card_details')->nullable();
            // $table->string('primary');
            // $table->string('scondary');
            // $table->string('text');
            // $table->string('code');
            // $table->string('designation');
            // $table->string('about');
            // $table->string('phone');           
            // $table->string('social_media');
            // $table->string('website')->nullable();
            // $table->text('image');
            $table->timestamp('purchased_at')->nullable();
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
