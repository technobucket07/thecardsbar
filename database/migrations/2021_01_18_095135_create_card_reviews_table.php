<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCardReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('card_reviews', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_id');
            $table->string('card_template_id');
            $table->bigInteger('likes')->nullable();
            $table->bigInteger('views')->nullable();
            $table->bigInteger('rating')->nullable();
			$table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('card_reviews');
    }
}
