<?php

use Illuminate\Database\Seeder;

class PageContact extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = App\Models\Pages::create([
            'name' => str_slug('contact us'),
        ]);

        $fields=[
        	str_slug('Address') => '<p>The Poppy Factory, 20 Petersham Road, Richmond, Surrey, TW10 6UW</p>',
            str_slug('Call us') => '<p><a href="#">T: +44 (0) 208 652 7787</a></p>
<p><a href="#">M: +44 (0) 7554 424167</a></p>',
            str_slug('Mail us') => '<p><a href="mailto:admin@gmail.com">info@streetlife-uk.com</a></p>
<p><a href="mailto:example@gmail.com">example@gmail.com</a></p>',
        ];

        foreach ($fields as $key => $field) {
        	App\Models\PageContent::create([
            'page_id'=>$page->id,
            'title'=> $key,
            'content'=>$field
        	]);
        }
    }
}
