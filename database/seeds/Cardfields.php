<?php

use Illuminate\Database\Seeder;

class Cardfields extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //Card Tittle
        App\Models\Cardfield::create([
            'name' => 'cardtitle',
            'fieldtype' => 'text',
            'defaultvalue' => '',
            'label' => 'Card Tittle',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'isrequired' =>1,
        ]);
        //Card Description
        App\Models\Cardfield::create([
            'name' => 'carddescription',
            'fieldtype' => 'textarea',
            'defaultvalue' => '',
            'label' => 'Card Description',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'isrequired' => 1,
        ]);
        //Card Picture
         App\Models\Cardfield::create([
            'name' => 'picture',
            'fieldtype' => 'file',
            'defaultvalue' => '',
            'label' => 'Card Picture',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'isrequired' => 0,
        ]);

         //Carousel Image
         App\Models\Cardfield::create([
            'name' => 'crosel_image',
            'fieldtype' => 'file',
            'defaultvalue' => '',
            'label' => 'Carousel Image',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'isrequired' => 0,
        ]);

         //Category
        App\Models\Cardfield::create([
            'name' => 'category_id',
            'fieldtype' => 'select',
            'defaultvalue' => '',
            'label' => 'Category',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'isrequired' => 1,
        ]);

         //Sub Category
        App\Models\Cardfield::create([
            'name' => 'sub_category_id',
            'fieldtype' => 'select',
            'defaultvalue' => '',
            'label' => 'Sub Category',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'isrequired' => 0,
        ]);

         //Card Actual Price
        App\Models\Cardfield::create([
            'name' => 'actualprice',
            'fieldtype' => 'number',
            'defaultvalue' => '',
            'label' => 'Card Actual Price',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'isrequired' => 1,
        ]);
        //Discount Type
        App\Models\Cardfield::create([
            'name' => 'discounttype',
            'fieldtype' => 'select',
            'defaultvalue' => '',
            'label' => 'Discount Type',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'isrequired' => 1,
        ]);
        //Discount
        App\Models\Cardfield::create([
            'name' => 'discount',
            'fieldtype' => 'number',
            'defaultvalue' => '',
            'label' => 'Discount',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'isrequired' => 1,
        ]);

        //Type Of Card
        App\Models\Cardfield::create([
            'name' => 'cardtype',
            'fieldtype' => 'select',
            'defaultvalue' => '',
            'label' => 'Type Of Card',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'isrequired' => 1,
        ]);
        //Social Media
        App\Models\Cardfield::create([
            'name' => 'socialmedia',
            'fieldtype' => 'radio',
            'defaultvalue' => '',
            'label' => 'Social Media',
            'max_limit' => 1,
            'min_limit' => 1,
            'sub_field' => 0,
            'isrequired' => 1,
        ]);
        //Profile
        App\Models\Cardfield::create([
            'name' => 'profile',
            'fieldtype' => 'radio',
            'defaultvalue' => '',
            'label' => 'Do You Want Profile Image ?',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'isrequired' => 1,
        ]);

        //Logo
        App\Models\Cardfield::create([
            'name' => 'logo',
            'fieldtype' => 'radio',
            'defaultvalue' => '',
            'label' => 'Do You Want Logo ?',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'isrequired'=> 0,
        ]);

        //Background
        App\Models\Cardfield::create([
            'name' => 'background',
            'fieldtype' => 'radio',
            'defaultvalue' => '',
            'label' => 'Do You Want Background Image ?',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'isrequired'=> 0,
        ]);

        //Gallery
        App\Models\Cardfield::create([
            'name' => 'gallery',
            'fieldtype' => 'radio',
            'defaultvalue' => '',
            'label' => 'Do You Want Gallery Images ?',
            'max_limit' => 1,
            'min_limit' => 1,
            'sub_field' => 0,
            'isrequired'=> 0,
        ]);

        //Services 
        App\Models\Cardfield::create([
            'name' => 'services',
            'fieldtype' => 'radio',
            'defaultvalue' => '',
            'label' => 'Do You Want Services ?',
            'max_limit' => 1,
            'min_limit' => 1,
            'sub_field' => 0,
            'withimages' => 1,
            'isrequired'=> 0,
        ]);

        //Name
        App\Models\Cardfield::create([
            'name' => 'username',
            'fieldtype' => 'checkbox',
            'defaultvalue' => '',
            'label' => 'Name',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'withimages' => 0,
            'isrequired'=> 1,
        ]);
        //Second name
        App\Models\Cardfield::create([
            'name' => 'secondname',
            'fieldtype' => 'checkbox',
            'defaultvalue' => '',
            'label' => 'Second name',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'withimages' => 0,
            'isrequired'=> 0,
        ]);

        //Primary Phone
        App\Models\Cardfield::create([
            'name' => 'primaryphone',
            'fieldtype' => 'checkbox',
            'defaultvalue' => '',
            'label' => 'Primary Phone',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'withimages' => 0,
            'isrequired'=> 1,
        ]);

        //Primary Phone
        App\Models\Cardfield::create([
            'name' => 'secondaryphone',
            'fieldtype' => 'checkbox',
            'defaultvalue' => '',
            'label' => 'Secondary Phone',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'withimages' => 0,
            'isrequired'=> 0,
        ]);

        //Primary Email
        App\Models\Cardfield::create([
            'name' => 'primaryemail',
            'fieldtype' => 'checkbox',
            'defaultvalue' => '',
            'label' => 'Primary Email',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'withimages' => 0,
            'isrequired'=> 1,
        ]);

         //Secondary Email
        App\Models\Cardfield::create([
            'name' => 'secondaryemail',
            'fieldtype' => 'checkbox',
            'defaultvalue' => '',
            'label' => 'Secondary Email',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'withimages' => 0,
            'isrequired'=> 0,
        ]);

         //Address
        App\Models\Cardfield::create([
            'name' => 'address',
            'fieldtype' => 'checkbox',
            'defaultvalue' => '',
            'label' => 'Address',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'withimages' => 0,
            'isrequired'=> 1,
        ]);

         //Location
        App\Models\Cardfield::create([
            'name' => 'location',
            'fieldtype' => 'checkbox',
            'defaultvalue' => '',
            'label' => 'Location',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'withimages' => 0,
            'isrequired'=> 0,
        ]);

         //Website
        App\Models\Cardfield::create([
            'name' => 'website',
            'fieldtype' => 'checkbox',
            'defaultvalue' => '',
            'label' => 'Website',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'withimages' => 0,
            'isrequired'=> 0,
        ]);

         //Designation
        App\Models\Cardfield::create([
            'name' => 'designation',
            'fieldtype' => 'checkbox',
            'defaultvalue' => '',
            'label' => 'Designation',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'withimages' => 0,
            'isrequired'=> 0,
        ]);
         //Company Name
        App\Models\Cardfield::create([
            'name' => 'company',
            'fieldtype' => 'checkbox',
            'defaultvalue' => '',
            'label' => 'Company Name',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'withimages' => 0,
            'isrequired'=> 0,
        ]);

         //Bottom Title
        App\Models\Cardfield::create([
            'name' => 'bottomtitle',
            'fieldtype' => 'checkbox',
            'defaultvalue' => '',
            'label' => 'Bottom Title',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'withimages' => 0,
            'isrequired'=> 0,
        ]);

        //Description
        App\Models\Cardfield::create([
            'name' => 'description',
            'fieldtype' => 'checkbox',
            'defaultvalue' => '',
            'label' => 'Description',
            'max_limit' => 0,
            'min_limit' => 1,
            'sub_field' => 0,
            'withimages' => 0,
            'isrequired'=> 0,
        ]);
        //About Company
        App\Models\Cardfield::create([
            'name' => 'companyabout',
            'fieldtype' => 'checkbox',
            'defaultvalue' => '',
            'label' => 'About Company',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'withimages' => 0,
            'isrequired'=> 0,
        ]);

        //Call Tag Line
        App\Models\Cardfield::create([
            'name' => 'calltagline',
            'fieldtype' => 'checkbox',
            'defaultvalue' => '',
            'label' => 'Call Tag Line',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'withimages' => 0,
            'isrequired'=> 0,
        ]);

        //Call Tag Line
        App\Models\Cardfield::create([
            'name' => 'openclosetime',
            'fieldtype' => 'checkbox',
            'defaultvalue' => '',
            'label' => 'Opening Closeing Time',
            'max_limit' => 0,
            'min_limit' => 0,
            'sub_field' => 0,
            'withimages' => 0,
            'isrequired'=> 0,
        ]);
    }
}
