<?php

use Illuminate\Database\Seeder;

class PageAbout extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = App\Models\Pages::create([
            'name' => str_slug('about us'),
        ]);


        $fields=[
            str_slug('About Us') => '<p><span style="color: #1d2228; font-family: Poppins, sans-serif; font-size: 16px; background-color: #ffffff;">luctus massa ipsum at tempus eleifend congue lectus bibendum porttito praesent elit sapien venenatis nec urna vitae pulvinar an</span></p>',
            str_slug('Our Goals') => '<p><span style="color: #1d2228; font-family: Poppins, sans-serif; font-size: 16px; background-color: #ffffff;">luctus massa ipsum at tempus eleifend congue lectus bibendum porttito praesent elit sapien venenatis nec urna vitae pulvinar an</span></p>',
            str_slug('Our Values') => '<p><span style="color: #1d2228; font-family: Poppins, sans-serif; font-size: 16px; background-color: #ffffff;">luctus massa ipsum at tempus eleifend congue lectus bibendum porttito praesent elit sapien venenatis nec urna vitae pulvinar an</span></p>',
            str_slug('Our Features') => '<p class="wow txt" style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; color: #1d2228; font-family: Poppins, sans-serif; font-size: 16px; background-color: #ffffff;">Our creative Ad agency is ranked among the finest in the US. We cultivate smart ideas for start-ups and seasoned players.</p>
<ul class="feat list-unstyled" style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem; padding-left: 0px; list-style: none; color: #1d2228; font-family: Poppins, sans-serif; font-size: 16px; background-color: #ffffff;">
<li class="wow fadeInUp" style="box-sizing: border-box; animation-name: fadeInUp;">
<h6 style="box-sizing: border-box; margin-top: 0px; margin-bottom: 0.5rem; font-weight: 500; line-height: 1.2; font-size: 1rem;"><span style="box-sizing: border-box;">1</span>&nbsp;Modern Design</h6>
<p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;">luctus massa ipsum at tempus eleifend congue lectus bibendum</p>
</li>
<li class="wow fadeInUp" style="box-sizing: border-box; animation-name: fadeInUp;">
<h6 style="box-sizing: border-box; margin-top: 0px; margin-bottom: 0.5rem; font-weight: 500; line-height: 1.2; font-size: 1rem;"><span style="box-sizing: border-box;">2</span>&nbsp;Fully Customization</h6>
<p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;">luctus massa ipsum at tempus eleifend congue lectus bibendum</p>
</li>
<li class="wow fadeInUp" style="box-sizing: border-box; animation-name: fadeInUp;">
<h6 style="box-sizing: border-box; margin-top: 0px; margin-bottom: 0.5rem; font-weight: 500; line-height: 1.2; font-size: 1rem;"><span style="box-sizing: border-box;">3</span>&nbsp;Retina ready</h6>
<p style="box-sizing: border-box; margin-top: 0px; margin-bottom: 1rem;">luctus massa ipsum at tempus eleifend congue lectus bibendum</p>
</li>
</ul>',
            str_slug('We Are') => '<p><span style="color: #1d2228; font-family: Poppins, sans-serif; font-size: 16px; background-color: #ffffff;">We develop creative solutions for small and big brands alike, build authentic product identities and much more.Lorem ipsum dolor sit amet, consectetur adipiscing elit sit non facilisis vitae eu. Ultrices ut diam morbi risus dui, nec eget at lorem in id tristique in elementum leo nisi eleifend placerat magna lacus elementum ornare vehicula odio posuere quisque ultrices tempus cras id blandit maecenas in ornare quis dolor tempus risus vitae feugiat fames aliquet sed.</span></p>',
        ];

        foreach ($fields as $key => $field) {
            App\Models\PageContent::create([
            'page_id' =>$page->id,
            'title' => $key,
            'content' =>$field
            ]);
        }
    }
}
