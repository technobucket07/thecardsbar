<?php

use Illuminate\Database\Seeder;

class Cards extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     
        for($i=1;$i<=11; $i++) {
        	App\Models\Cards::create([
            'name'=>"design-$i"
        	]);
        }
    }
}
