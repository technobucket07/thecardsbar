<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::namespace('API')->group(function() {
        Route::post('convert/card', 'GenerateCardController@generateCard');
        Route::post('convert/card2', 'GenerateCardController@generateCard2');
});

Route::post('/userverify2', 'JwtAuthController@UserVerify')->name('userverify2');

Route::group(['middleware' => 'api','namespace'=>'API'], function () {
    Route::post('/login', 'JwtAuthController@login');
    Route::post('/register', 'JwtAuthController@register');
    Route::post('/logout', 'JwtAuthController@logout');
    Route::post('/refresh', 'JwtAuthController@refresh');
    Route::get('/user-profile', 'JwtAuthController@userProfile');    
});