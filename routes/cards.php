<?php

/*
|--------------------------------------------------------------------------
| Cards Routes
|--------------------------------------------------------------------------
|
| Here is where you can register Cards routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/
    Route::group(['middleware' =>'admin','namespace' => 'Cards','as' => 'admin.Cards.'], function () {
         //Route::get('/', 'HomeController@index')->name('');
        Route::get('/addcard', 'HomeController@AddCard')->name('getAddCard');
        Route::post('/addcard', 'HomeController@AddCard')->name('postAddCard');

        Route::get('/editcard/{id}', 'HomeController@EditCard')->name('getEditCard');
        Route::patch('/editcard/{id}', 'HomeController@EditCard')->name('postEditCard');

        Route::delete('/deletecard/{id}', 'HomeController@destroy')->name('DeleteCard');

        Route::get('/cards', 'HomeController@index')->name('cardslist');
        Route::get('/cards/{id}', 'HomeController@cardSteps')->name('steps');
        //card
        Route::get('/firstcard', 'HomeController@card')->name('cards');
        Route::post('/firstcard', 'HomeController@card')->name('card');
        Route::post('/cards1', 'HomeController@cards1')->name('cards1');
        Route::post('/cards3', 'HomeController@cards3')->name('cards3');
        Route::post('/cards4', 'HomeController@cards4')->name('cards4');
        Route::post('/cards5', 'HomeController@cards5')->name('cards5');
        Route::post('/cards6', 'HomeController@cards6')->name('cards6');
        Route::post('/cards7', 'HomeController@cards7')->name('cards7');
        Route::post('/cards8', 'HomeController@cards8')->name('cards8');
        Route::post('/cards9', 'HomeController@cards9')->name('cards9');
        Route::post('/cards10', 'HomeController@cards10')->name('cards10');
        Route::post('/cards11', 'HomeController@cards11')->name('cards11');
        Route::post('/croppie', 'HomeController@croppie')->name('croppie');
        Route::post('/crop-image', 'HomeController@crop_image')->name('crop-images');
       
        //pdf
        Route::post('/generate_pdf', 'HomeController@generate_pdf')->name('generate_pdf');
        Route::post('/generate_pdf1', 'HomeController@generate_pdf1')->name('generate_pdf1');
        Route::post('/generate_pdf3', 'HomeController@generate_pdf3')->name('generate_pdf3');
        Route::post('/generate_pdf4', 'HomeController@generate_pdf4')->name('generate_pdf4');
        Route::post('/generate_pdf5', 'HomeController@generate_pdf5')->name('generate_pdf5');
        Route::post('/generate_pdf6', 'HomeController@generate_pdf6')->name('generate_pdf6');
        Route::post('/generate_pdf7', 'HomeController@generate_pdf7')->name('generate_pdf7');
        Route::post('/generate_pdf8', 'HomeController@generate_pdf8')->name('generate_pdf8');
        Route::post('/generate_pdf9', 'HomeController@generate_pdf9')->name('generate_pdf9');
        Route::post('/generate_pdf10', 'HomeController@generate_pdf10')->name('generate_pdf10');
        Route::post('/generate_pdf11', 'HomeController@generate_pdf11')->name('generate_pdf11');
      
        Route::get('/delete-image', 'HomeController@dele_img')->name('delete-image');
    });
     // Route::get('/cards', 'HomeController@card');

    /*Route::get('test', function () {
        return view('cards/personal_card');
    });
    Route::get('pcard', 'API\GenerateCardController@generateCard');
    Route::get('/convert_pdf', 'PDFController@convertToPDF');*/

    Auth::routes();
       
    Route::get('/pdfs', 'Auth\LoginController@pdf')->name('pdf');
    Route::get('/pdfdemo','Auth\LoginController@pdfdemo')->name('pdfdemo');
    Route::get('/pdfs1', 'Auth\LoginController@pdf1')->name('pdf1');
    Route::get('/pdfs2', 'Auth\LoginController@pdf2')->name('pdf2');
    Route::get('/pdfs3', 'Auth\LoginController@pdf3')->name('pdf3');
    Route::get('/pdfs4', 'Auth\LoginController@pdf4')->name('pdf4');
    Route::get('/pdfs5', 'Auth\LoginController@pdf5')->name('pdf5');
    Route::get('/pdfs6', 'Auth\LoginController@pdf6')->name('pdf6');
    Route::get('/pdfs7', 'Auth\LoginController@pdf7')->name('pdf7');