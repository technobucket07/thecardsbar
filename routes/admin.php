<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register admin routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "admin" middleware group. Now create something great!
|
*/
//'middleware' =>'admin',
Route::group(['middleware' =>'admin','namespace' => 'Admin','as' => 'admin.'], function () {
	Route::get('/', 'HomeController@index')->name('home');
	//orders routes
	Route::get('/orders/{type?}', 'OrdersController@index')->name('orderslist');
	Route::get('/orders/orderview/{oid}', 'OrdersController@orderview')->name('orderview');
	Route::post('orders/orderprocess', 'OrdersController@orderprocess')->name('orderprocess');
	Route::get('/makecard/{cid}/{oid}/{uid}', 'OrdersController@MakeCard')->name('MakeCard');
	//visting card routes
	Route::resource('cardfields', 'CardfieldController');
	Route::resource('vistingcards', 'VistingcardsController');
	Route::post('enablecard', 'VistingcardsController@EnableCard')->name('EnableCard');
	Route::delete('deletetheme/{id}', 'VistingcardsController@Deletetheme')->name('Deletetheme');
	Route::resource('colortypes', 'ColortypesController');
	Route::get('/getsubcategory', 'VistingcardsController@getSubcategory')->name('getSubcategory');
	//visting card category routes
	Route::resource('cardtypes', 'CardtypesController');
	Route::resource('cardscategory', 'CategoryController');
	Route::resource('cardssubcategory', 'SubCategoryController');
	//users routes
	Route::resource('users', 'UsersController');
	Route::resource('pages', 'PagesController');
	Route::resource('socialmedias', 'SocialmediaController');
	Route::get('addcontent/{pageid}', 'PagesController@AddContent')->name('pages.addcontent');
	Route::post('addcontent', 'PagesController@AddContentPost')->name('pages.addcontentpost');
	//contact us
	Route::resource('contacts', 'ContactsController');
	Route::post('contacts/reply', 'ContactsController@feedbackReply')->name('feedbackReply');
});
