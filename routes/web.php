<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('google-analytics-summary',array('as'=>'google-analytics-summary','uses'=>'HomeController1@getAnalyticsSummary'));

Route::namespace('Frontend')->group(function()
{
    Route::get('generateVcf2', 'HomeController@vcardDownload');
    Route::get('/', 'HomeController@index');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('about-us','HomeController@aboutUs')->name('about-us');
    Route::get('/allcards','HomeController@Ecards')->name('ecards');
    Route::get('contact-us','HomeController@ContactUs')->name('contact-us');
    Route::get('refund-policy','HomeController@refundPolicy')->name('refund-policy');
    Route::post('contact-us','HomeController@PostContactUs')->name('PostContactUs');
    Route::get('privacy-policy','HomeController@privacyPolicy')->name('privacy-policy');
    Route::get('/terms-conditions','HomeController@termsConditions')->name('termsConditions');
    Route::get('/image-upload', 'HomeController@imageUpload')->name('imageUpload');
    Route::post('/image-upload', 'HomeController@imageUpload')->name('imageUpload');
    Route::post('/image-delete', 'HomeController@imageDelete')->name('imageDelete');

    Route::group(['middleware'=>'checkuser'],function() {
    //Added by Parmod
    Route::post('/tempcarddetails', 'HomeController@TempCardDetails')->name('TempCardDetails');
    Route::get('/gettempform', 'HomeController@getTempForm')->name('getTempForm');
    Route::get('/getsubcategory', 'HomeController@getSubcategory')->name('getSubcategory');
    Route::post('/placeorder', 'HomeController@PlaceOrder')->name('PlaceOrder');

    Route::get('/cart', 'OrdersController@MyCart')->name('MyCart');
    Route::get('/myorders', 'OrdersController@MyOrders')->name('MyOrders');
    Route::get('/mycards', 'OrdersController@MyCards')->name('MyCards');
    Route::get('mycard/{id}', 'OrdersController@Mycard')->name('Mycard');
    Route::get('checkout/{oid}', 'OrdersController@Checkout')->name('checkout');

    Route::post('/paynow', 'HomeController@PayNow')->name('PayNow');
    Route::post('/razorpaysuccess', 'HomeController@razorpaySuccess')->name('razorpaysuccess');
    Route::get('/thanks-you/{id}', 'HomeController@ThanksYou')->name('ThanksYou');
    // Dynamic Card Steps
    Route::get('/cardsteps/{id}', 'HomeController@dynamicCardSteps')->name('dynamicCardSteps');
    //end  
    });
});

Route::group(['middleware'=>'checkuser'],function() {
    Route::get('/account', 'HomeController@UserAccount')->name('account');
    Route::post('/account', 'HomeController@UpdateAccount')->name('UpdateAccount');
    Route::post('/changePassword', 'HomeController@changePassword')->name('changePassword');
    Route::post('/reviewcards', 'HomeController@CardReviews')->name('CardReviews');
    Route::post('/userreviewcards', 'HomeController@UserCardReviews')->name('UserCardReviews');
    Route::get('/yourcollection', 'HomeController@YourCollection')->name('YourCollection');
});
Route::get('downloadcardwithqr/{id}','HomeController@DownloadCardWithQR')->name('DownloadCardWithQR');
Route::get('downloadcard/{id}/{type}', 'HomeController@DownloadCard')->name('DownloadCard');
Route::get('getqrcode/{userid}/{orderid}', 'HomeController@GetQRcode')->name('GetQRcode');

Route::post('/registerUser', 'Auth\RegisterController@registerUser')->name('registerUser');
Route::post('/resendotp', 'Auth\RegisterController@reSendOtp')->name('reSendOtp');
Route::post('/newuserverify', 'Auth\RegisterController@UserVerify')->name('UserVerify');
Route::post('/userlogin', 'Auth\LoginController@UserLoginajax')->name('UserLoginajax');
Route::get('auth/{provider}', 'Auth\RegisterController@redirectToProvider')->name('social.oauth');
Route::get('auth/{provider}/callback', 'Auth\RegisterController@handleProviderCallback')->name('social.callback');
Route::post('socialaddemail', 'Auth\RegisterController@socialAddEnail')->name('socialAddEnail');
Auth::routes();

Route::get('/clear-cache', function() {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');
    Artisan::call('view:clear');
    Artisan::call('config:clear');
    //Artisan::call('storage:link');
    return "Application cache flushed";
});

Route::get('/seeders', function() {
    //Artisan::call('db:seed');
    Artisan::call('db:seed');
    return "seed done";
});
